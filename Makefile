# SPDX-License-Identifier: MIT
# Copyright 2013, 2014, 2017, 2021 Alexander Zuepke
#
# Top level Makefile
#
# azuepke, 2013-04-06: split from Kernel/Makefile
# azuepke, 2013-08-08: now in 64-bit
# azuepke, 2014-08-07: moved libs to top level Makefile
# azuepke, 2017-10-03: imported and adapted

TOP := .

include $(TOP)/rules.mk

ifeq ("$(ARCH)", "arm")
	ifeq ("$(BSP)", "qemu-arm")
		# Versatile Express Cortex A15 with four UARTs
		QCMD = $(shell which ~/.qemu/bin/qemu-system-arm || echo qemu-system-arm)
		QARGS = -M vexpress-a15 -m 64 -nographic -no-reboot -net none \
			-serial mon:stdio \
			-serial telnet:localhost:1235,server,nowait \
			-serial telnet:localhost:1236,server,nowait \
			-serial telnet:localhost:1237,server,nowait
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 2
		endif
		ELF_ARCH = arm
		ELF_LOADADDR = 0x80008000
		BOOT_CMD = echo type: make run
		BOOTFILE = bootfile.elf
	endif
	ifeq ("$(BSP)", "qemu-aarch64")
		# Cortex-A57 on QEMU ARM "virt" board
		QCMD = $(shell which ~/.qemu/bin/qemu-system-aarch64 || echo qemu-system-aarch64)
		QARGS = -M virt,virtualization=on -cpu cortex-a57 -m 64 -nographic -no-reboot -net none \
			-serial mon:stdio
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 2
		endif
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x40080000
		BOOT_CMD = echo type: make run
		BOOTFILE = bootfile.elf
	endif
	ifeq ("$(BSP)", "am335x")
		# Beaglebone Black
		ELF_ARCH = arm
		ELF_LOADADDR = 0x80008000
		BOOT_CMD = cp bootfile.bin /tftpboot/beagleboneblack
		BOOTFILE = bootfile.bin
	endif
	ifeq ("$(BSP)", "am57xx")
		# BeagleBoard-X15
		ELF_ARCH = arm
		ELF_LOADADDR = 0x80008000
		BOOT_CMD = cp bootfile.img /tftpboot/am57xx
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "imx6")
		# Sabrelite on second UART
		QCMD = $(shell which ~/.qemu/bin/qemu-system-arm || echo qemu-system-arm)
		QARGS = -M sabrelite -m 64 -nographic -no-reboot -net none \
			-serial telnet:localhost:1235,server,nowait \
			-serial mon:stdio \
			-serial telnet:localhost:1236,server,nowait \
			-serial telnet:localhost:1237,server,nowait
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 4
		endif
		ELF_ARCH = arm
		ELF_LOADADDR = 0x10008000
		BOOT_CMD = cp bootfile.bin /tftpboot/imx6
		BOOTFILE = bootfile.elf bootfile.bin
	endif
	ifeq ("$(BSP)", "zynq")
		# Zynq-7000 on Zybo Z7 board using second UART
		QCMD = $(shell which ~/.qemu/bin/qemu-system-arm || echo qemu-system-arm)
		QARGS = -M xilinx-zynq-a9 -m 64 -nographic -no-reboot -net none \
			-serial telnet:localhost:1235,server,nowait \
			-serial mon:stdio
		ELF_ARCH = arm
		ELF_LOADADDR = 0x00108000
		BOOT_CMD = cp bootfile.img /tftpboot/zynq
		BOOTFILE = bootfile.elf bootfile.img
	endif
	ifeq ("$(BSP)", "bcm2836")
		# Raspberry Pi 2B
		QCMD = $(shell which ~/.qemu/bin/qemu-system-arm || echo qemu-system-arm)
		QARGS = -M raspi2b -m 1024 -nographic -no-reboot -net none \
			-serial mon:stdio -smp 4
		ELF_ARCH = arm
		ELF_LOADADDR = 0x00008000
		BOOT_CMD = cp bootfile.img /tftpboot/rpi2
		BOOTFILE = bootfile.elf bootfile.img
	endif
	ifeq ("$(BSP)", "bcm2837")
		# Raspberry Pi 3B in 32-bit mode
		QCMD = $(shell which ~/.qemu/bin/qemu-system-arm || echo qemu-system-arm)
		QARGS = -M raspi2b -m 1024 -nographic -no-reboot -net none \
			-serial telnet:localhost:1235,server,nowait \
			-serial mon:stdio -smp 4
		ELF_ARCH = arm
		ELF_LOADADDR = 0x00008000
		BOOT_CMD = cp bootfile.img /tftpboot/rpi3
		BOOTFILE = bootfile.elf bootfile.img
	endif
	ifeq ("$(BSP)", "bcm2837-64bit")
		# Raspberry Pi 3B in 64-bit mode
		QCMD = $(shell which ~/.qemu/bin/qemu-system-aarch64 || echo qemu-system-aarch64)
		QARGS = -M raspi3b -m 1024 -nographic -no-reboot -net none \
			-serial telnet:localhost:1235,server,nowait \
			-serial mon:stdio -smp 4
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x00080000
		BOOT_CMD = cp bootfile.img /tftpboot/rpi3
		BOOTFILE = bootfile.elf bootfile.img
	endif
	ifeq ("$(BSP)", "bcm2711")
		# Raspberry Pi 4B in 32-bit mode
		ELF_ARCH = arm
		ELF_LOADADDR = 0x00008000
		BOOT_CMD = cp bootfile.img /tftpboot/rpi4
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "bcm2711-64bit")
		# Raspberry Pi 4B in 64-bit mode
		QCMD = $(shell which ~/.qemu/bin/qemu-system-aarch64 || echo qemu-system-aarch64)
		QARGS = -M raspi4b -m 2048 -nographic -no-reboot -net none \
			-serial telnet:localhost:1235,server,nowait \
			-serial mon:stdio -smp 4
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x00080000
		BOOT_CMD = cp bootfile.img /tftpboot/rpi4
		BOOTFILE = bootfile.elf bootfile.img
	endif
	ifeq ("$(BSP)", "bcm2712")
		# Raspberry Pi 5 in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x00080000
		BOOT_CMD = cp bootfile.bin.gz /tftpboot/rpi5
		BOOTFILE = bootfile.bin.gz
	endif
	ifeq ("$(BSP)", "zcu102")
		# Xilinx Zynq UltraScale+ in 64-bit mode
		QCMD = $(shell which ~/.qemu/bin/qemu-system-aarch64 || echo qemu-system-aarch64)
		QARGS = -M xlnx-zcu102 -m 1024 -nographic -no-reboot -net none \
			-serial mon:stdio -smp 4
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x00080000
		BOOT_CMD = cp bootfile.img /tftpboot/zcu102
		BOOTFILE = bootfile.elf bootfile.img
	endif
	ifeq ("$(BSP)", "tegra186")
		# NVIDIA Jetson TX2 in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.img /tftpboot/tx2
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "tegra194")
		# NVIDIA Xavier in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfilec.img /tftpboot/boot.img
		BOOTFILE = bootfilec.img
	endif
	ifeq ("$(BSP)", "tegra234")
		# NVIDIA Orin in 64-bit mode (UEFI)
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.bin /tftpboot/orin
		BOOTFILE = bootfile.bin
	endif
	ifeq ("$(BSP)", "s32v234")
		# NXP S23V234 in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.img /tftpboot/s32
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "s32g274")
		# NXP S23G274 in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.img /tftpboot/s32
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "lx2160a")
		# NXP LX2160A in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.img /tftpboot/lx2160a
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "imx8m")
		# i.MX8M in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x40480000
		BOOT_CMD = cp bootfile.img /tftpboot/imx8m
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "imx8mp")
		# i.MX8M Plus in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x40480000
		BOOT_CMD = cp bootfile.img /tftpboot/imx8mp
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "rk3568")
		# RK3568 in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x00280000
		BOOT_CMD = cp bootfile.img /tftpboot/rk3568
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "rk3588")
		# RK3588 in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x00480000
		BOOT_CMD = cp bootfile.img /tftpboot/rk3588
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "fvp")
		# Fixed Virtual Platform simulator
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = echo ready
		BOOTFILE = bootfile.axf
	endif
	ifeq ("$(BSP)", "am62x")
		# AM62x in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.img /tftpboot/am62x
		BOOTFILE = bootfile.img
	endif
	ifeq ("$(BSP)", "am67x")
		# AM67x in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.bin /tftpboot/am67x
		BOOTFILE = bootfile.bin
	endif
	ifeq ("$(BSP)", "tda4vm")
		# TDA4VM in 64-bit mode
		ELF_ARCH = aarch64
		ELF_LOADADDR = 0x80080000
		BOOT_CMD = cp bootfile.bin /tftpboot/tda4vm
		BOOTFILE = bootfile.bin
	endif

	ifeq ("$(ELF_ARCH)", "arm")
		MKIMAGE_ARCH = arm
	else
		MKIMAGE_ARCH = arm64
	endif
endif
ifeq ("$(ARCH)", "riscv")
	ifeq ("$(BSP)", "qemu-riscv32")
		# QEMU RISC-V "virt" board
		QCMD = $(shell which ~/.qemu/bin/qemu-system-riscv32 || echo qemu-system-riscv32)
		QARGS = -M virt -m 64 -nographic -no-reboot -net none -serial mon:stdio
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 4
		endif
		ELF_ARCH = riscv32
		ELF_LOADADDR = 0x80400000
		BOOT_CMD = echo type: make run
		BOOTFILE = bootfile.elf
	endif
	ifeq ("$(BSP)", "qemu-riscv64")
		# QEMU RISC-V "virt" board
		QCMD = $(shell which ~/.qemu/bin/qemu-system-riscv64 || echo qemu-system-riscv64)
		QARGS = -M virt -m 64 -nographic -no-reboot -net none -serial mon:stdio
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 4
		endif
		ELF_ARCH = riscv64
		ELF_LOADADDR = 0x80200000
		BOOT_CMD = echo type: make run
		BOOTFILE = bootfile.elf
	endif
	ifeq ("$(BSP)", "polarfire")
		# QEMU RISC-V "microchip-icicle-kit" board
		QCMD = $(shell which ~/.qemu/bin/qemu-system-riscv64 || echo qemu-system-riscv64)
		QARGS = -M microchip-icicle-kit -m 1537 -nographic -no-reboot -net none \
			-serial mon:stdio \
			-serial telnet:localhost:1235,server,nowait
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 4
		endif
		ELF_ARCH = riscv64
		ELF_LOADADDR = 0x80200000
		BOOT_CMD = cp bootfile.img /tftpboot/polarfire
		BOOTFILE = bootfile.dtb bootfile.img
	endif

	MKIMAGE_ARCH = riscv
endif

ifneq ("$(AUTOBUILD)", "true")
ifneq ("$(COVBUILD)", "true")
	XTERM=xterm -geom 80x60 -hold -e
endif
endif

# extra QEMU arguments for coverage run
EXECLOG = /tmp/elog
QCOVARGS = -etrace $(EXECLOG) -etrace-flags exec -d nochain -accel tcg,thread=single
COVINFO = cov.info
COVDIR = html

# APPS CONFIG and LIBS is specified by BUILDENVs
apps = $(APPS)
libs = $(LIBS) kernel $(KLDDS)
alldirs = $(LIBS) kernel $(KLDDS) bsp/$(BSP) $(APPS)

all: $(BOOTFILE)

run: all
	# Normal run
	$(XTERM) $(QCMD) $(QARGS) -kernel bootfile.elf -s

debug: all
	# Debug session with QEMU
	$(XTERM) $(QCMD) $(QARGS) -kernel bootfile.elf -s -S

cov: all
	# Coverage run
	$(XTERM) $(QCMD) $(QARGS) $(QCOVARGS) -kernel bootfile.elf -s
	@echo "  COV   $(COVINFO)"
	$(Q)LANG=C qemu-etrace --trace $(EXECLOG) --elf bsp/$(BSP)/kernel.elf \
	    --coverage-output $(COVINFO) --coverage-format lcov --trace-output none
	$(Q)mkdir -p $(COVDIR)
	$(Q)genhtml -o $(COVDIR) $(COVINFO)
	@echo "View coverage data here: $$ firefox file:$(COVDIR)/index.html"

boot: all
	$(BOOT_CMD)

bootfile.elf: bootfile.bin tools
	$(Q)tools/mkelf/mkelf $< $(ELF_ARCH) $(ELF_LOADADDR) $@

# the .afx is just another ELF variant, but with a section header table
bootfile.axf: bootfile.bin
	@echo "  AXF   $@"
	$(Q)$(CROSS)objcopy -I binary -O elf64-littleaarch64 --change-addresses $(ELF_LOADADDR) $< $@

# binary
bootfile.bin: bsp
	@echo "  IMAGE $@"
	$(Q)$(TOP)/scripts/gen_image.py -hw kernel/gen_hardware.xml -c kernel/gen_config.xml $@

# compressed bin file
bootfile.bin.gz: bootfile.bin
	@echo "  GZIP  $@"
	$(Q)gzip -c -f -9 $^ > $@

# U-Boot boot image
bootfile.img: bootfile.bin.gz
	@echo "  BOOTU $@"
	$(Q)mkimage -A $(MKIMAGE_ARCH) -O linux -T kernel -a $(ELF_LOADADDR) -e $(ELF_LOADADDR) -n Marron -d $^ $@

# empty DTB
bootfile.dtb:
	@echo "  DTC   $@"
	$(Q)/bin/echo -e "/dts-v1/;\n/ {\n};" | dtc - -o $@

# Android boot image
bootfilea.img: bootfile.bin.gz
	@echo "  BOOTA $@"
	$(Q)mkbootimg --kernel $^ -o $@

# NVIDIA CBOOT boot image
bootfilec.img: bootfilea.img
	@echo "  BOOTC $@"
	$(Q)scripts/nvboothdr.py $^ $@

# generate kernel config
# NOTE: the kernel config pulls in the final application binaries
kernel/gen_config.c: kernel/gen_hardware.xml kernel/gen_config.xml $(apps)
	@echo "  GEN   $@"
	$(Q)$(TOP)/scripts/gen_config.py -hw kernel/gen_hardware.xml -c kernel/gen_config.xml --semi $@

kernel/gen_config.o: kernel/gen_config.c
	@echo "  CC    $@"
	$(MAKE) -C kernel gen_config.o

# generate XML config files
kernel/gen_hardware.xml: $(TOP)/$(MARRON_HARDWARE_XML) $(KLDD_XMLS)
	@echo "  GEN   $@"
	$(Q)$(TOP)/scripts/merge_config.py -o $@ $^

kernel/gen_config.xml: $(TOP)/$(CONFIG_XML) $(KLDD_XMLS)
	@echo "  GEN   $@"
	$(Q)$(TOP)/scripts/merge_config.py -o $@ $^

# generate headers
# NOTE: we only generate a config.h for the BSP, and this effectively only depends on the hardware.xml
bsp/$(BSP)/config.h: kernel/gen_hardware.xml kernel/gen_config.xml
	@echo "  HDR   $@"
	$(Q)$(TOP)/scripts/gen_header.py -hw kernel/gen_hardware.xml -c kernel/gen_config.xml --config $@

# libs + kernel
$(libs): bsp/$(BSP)/config.h
	$(MAKE) -C $@ all

# apps
$(apps): $(libs)
	$(MAKE) -C $@ all

# bsp
bsp: kernel kernel/gen_config.o bsp/$(BSP)/config.h $(KLDD_OBJS)
	$(MAKE) -C bsp/$(BSP)

# tools
tools: tools/mkelf/mkelf

HOST_CFLAGS := -W -Wall -Wextra -Werror

tools/mkelf/mkelf: tools/mkelf/mkelf.c
	@echo "  CCHOST $<"
	$(Q)$(HOSTCC) $(HOST_CFLAGS) -o $@ $<

# cleanup
thisclean:
	$(Q)rm -f kernel/gen_config.c kernel/gen_config.o \
	          kernel/gen_hardware.xml kernel/gen_config.xml
	$(Q)rm -f bootfile.bin bootfile.bin.gz bootfile.elf bootfile.axf \
	          bootfile.img bootfilea.img bootfilec.img bootfile.dtb
	$(Q)rm -f $(EXECLOG) $(COVINFO) -r $(COVDIR)


# prevent recursive build loop ;)
clean-dirs = $(addprefix _clean_, $(alldirs))
distclean-dirs = $(addprefix _distclean_, $(alldirs))

$(clean-dirs):
	-make -C $(patsubst _clean_%,%,$@) clean

$(distclean-dirs):
	-make -C $(patsubst _distclean_%,%,$@) distclean

clean: thisclean $(clean-dirs)

distclean: thisclean $(distclean-dirs)
	$(Q)rm -rf scripts/__pycache__
	$(Q)rm -f tools/mkelf/mkelf

zap: distclean
	# no extra steps right now

print-%  : ; @echo $* = $($*)

.PHONY: all clean distclean $(apps) $(libs) bsp $(clean-dirs) $(distclean-dirs)
