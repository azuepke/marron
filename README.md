# The Marron Kernel

"Marron" is a small, statically configured operating system targeting MMU
based systems. The implementation targets MMU-based 32-bit and 64-bit ARM
Cortex-A multicore processors, e.g. Cortex A9, A15, A53, A57 or A72,
as well as RISC-V cores with MMU and user and supervisor mode.

Marron is licensed under MIT license, see [LICENSE.TXT](LICENSE.TXT).

See the [README.TXT](README.TXT) for more information.

## Marron Features

- Static system configuration: all resources are known and partitioned
  at compile time, so no memory allocation is required in the kernel.
- Static process model. All processes are known at compile time.
- Strict partitioning: processes can use their assigned resources, but not more.
  (Right now, there is a limitation of one process per partition).
- Multithreading: dynamic creation of threads at runtime (fork-join model).
- Futex-based thread synchronization: low-overhead mutexes and condition
  variables like in Linux.
- User-level interrupt handling: Interrupt handlers are implemented
  in user space, i.e. a user space thread synchronously waits for an interrupt.
- POSIX-like timeout and clock API with relative and absolute timeouts
  with nanosecond resolution and support for wall and monotonic clocks.
- POSIX-like timers.
- POSIX-like signal handling for exception handling, and minimal signal
  handling support for asynchronous notifications for expired timers.
- Per-thread control of FPU usage.
- Scheduling:
  - Partitioned fixed-priority scheduling with 256 priorities (0..255)
  - Configurable time slices per thread (for FIFO or round-robin scheduling)
  - Threads do not migrate between cores automatically (no load balancing)
- Cache management API.
- Memory allocation API to allocate/free memory from assigned memory pools
  at runtime
- POSIX-like virtual memory management (map, unmap, protect)

- Single-stack kernel model.
- Fine-grained locking model in the kernel.
- The kernel memory layout is statically defined by the system configuration.
- Board-support-package (BSP) model.

- Supported architectures:
  - 32-bit ARM: Cortex A7, A8, A9, A15 (MMU in non-LPAE mode)
  - 64-bit ARM: Cortex A53, A57, A72 (MMU using 3-level page tables)
  - 32-bit RISC-V: IMAFDC in S-mode (MMU using Sv32 2-level page tables)
  - 64-bit RISC-V: IMAFDC in S-mode (MMU using Sv39 3-level page tables)
