# SPDX-License-Identifier: MIT
# Copyright 2013, 2017, 2021 Alexander Zuepke
# rules.mk
#
# Common build rules
#
# azuepke, 2013-03-22: initial
# azuepke, 2013-05-26: fixed .d: striped directories
# azuepke, 2017-10-03: imported and adapted

# overrides from shell environment
ifneq ("$(MARRON_ARCH)", "")
ARCH := $(MARRON_ARCH)
endif
ifneq ("$(MARRON_SUBARCH)", "")
SUBARCH := $(MARRON_SUBARCH)
endif
ifneq ("$(MARRON_BSP)", "")
BSP := $(MARRON_BSP)
endif
ifneq ("$(MARRON_CROSS_PREFIX)", "")
CROSS := $(MARRON_CROSS_PREFIX)
endif
ifneq ("$(MARRON_DEBUG)", "")
DEBUG := $(MARRON_DEBUG)
endif
ifneq ("$(MARRON_SMP)", "")
SMP := $(MARRON_SMP)
endif
ifneq ("$(MARRON_APPS)", "")
APPS := $(MARRON_APPS)
else
$(error APPs not specifies)
endif
ifneq ("$(MARRON_CONFIG_XML)", "")
CONFIG_XML := $(MARRON_CONFIG_XML)
else
$(error CONFIG_XML not specifies)
endif
ifneq ("$(MARRON_HARDWARE_XML)", "")
HARDWARE_XML := $(MARRON_HARDWARE_XML)
else
$(error HARDWARE_XML not specifies)
endif
ifneq ("$(MARRON_LIBS)", "")
LIBS := $(MARRON_LIBS)
else
$(error LIBS not specifies)
endif
ifneq ("$(MARRON_KLDDS)", "")
KLDDS := $(MARRON_KLDDS)
endif
KLDD_XMLS := $(foreach kldd,$(KLDDS),$(kldd)/kldd.xml)
KLDD_OBJS := $(foreach kldd,$(KLDDS),$(kldd)/kldd.o)

# Default rules for DEBUG and SMP
ifeq ("$(DEBUG)", "")
DEBUG := yes
endif
ifeq ("$(SMP)", "")
SMP := yes
endif

# Catch errors
ifeq ("$(MARRON_ARCH)", "")
$(error Please source BUILDENV.sh!)
endif
ifeq ("$(MARRON_SUBARCH)", "")
$(error Please source BUILDENV.sh!)
endif
ifeq ("$(MARRON_BSP)", "")
$(error Please source BUILDENV.sh!)
endif

# NOTE: set VERBOSE to disable suppressing of compiler commandlines
ifeq ("$(VERBOSE)", "")
Q=@
endif

# cross toolchain
LD = $(CROSS)ld
AS = $(CROSS)gcc -c
CC = $(CROSS)gcc -c
OBJCOPY = $(CROSS)objcopy
OBJDUMP = $(CROSS)objdump
DEPCC = $(CROSS)gcc -M
CPP = $(CROSS)gcc -E
NM = $(CROSS)nm
STRIP = $(CROSS)strip
AR = $(CROSS)ar

HOSTCC = gcc -O2

# {cc|as}-option for CFLAGS/AFLAGS
# Usage: CFLAGS += $(call cc-option,-ffeature-to-test,-ffallback-flag-instead)
cc-option = $(if $(shell $(CC) $1 -S -o /dev/null -xc /dev/null \
              >/dev/null 2>&1 && echo OK), $1, $2)
as-option = $(if $(shell $(AS) $1 -S -o /dev/null -xc /dev/null \
              >/dev/null 2>&1 && echo OK), $1, $2)
ld-option = $(if $(shell $(LD) $1 --version \
              >/dev/null 2>&1 && echo OK), $1, $2)


# default build ID
ifeq ("$(BUILDID)", "")
BUILDID := $(USER)@$(shell hostname) $(shell date +'%Y-%m-%d %H:%M:%S')
endif

WARNFLAGS := -W -Wall -Wextra -Wshadow -Wpointer-arith -Wcast-qual \
             -Wmissing-prototypes -Wstrict-prototypes \
             -Wmissing-declarations -Wdeclaration-after-statement \
             -Wformat=2 -Wformat-overflow=2 -Wformat-truncation=2 \
             -Wformat-security -Wformat-signedness \
             -Wnull-dereference -Wstack-protector -Wtrampolines \
             -Wvarargs \
             -Walloca -Wvla \
             -Warray-bounds=2 \
             -Wunused-result \
             -Wmissing-braces \
             -Wmisleading-indentation \
             -Wundef \
             -Wimplicit-fallthrough=3 -Wshift-overflow=2 -Wstringop-overflow=4 \
             -Wlogical-op -Wduplicated-cond -Wduplicated-branches -Wstrict-overflow=4 \
             -Wswitch-default -Wswitch-enum  \
             -Werror

# Troubling:
#            -Wconversion \
#            -Wsign-conversion \
#            -Wtraditional-conversion \
#            -Wcast-align=strict \


CFLAGS := -std=c11 -ffreestanding -fno-pic \
          $(call cc-option,-fno-delete-null-pointer-checks,) \
          -fno-stack-protector \
          $(call cc-option,-fconserve-stack,) \
          -fno-strict-aliasing \
          -fno-strict-overflow \
          -fno-common \
          $(call cc-option,-mgeneral-regs-only,) \
          $(WARNFLAGS)
CPPFLAGS := -ffreestanding -fno-pic
AFLAGS := -D__ASSEMBLER__ -fno-pic
ifeq ("$(AUTOBUILD)", "true")
	CFLAGS += -DAUTOBUILD
	CPPFLAGS += -DAUTOBUILD
endif
ifeq ("$(COVBUILD)", "true")
	CFLAGS += -DCOVBUILD
	CPPFLAGS += -DCOVBUILD
endif

.SUFFIXES: .c .cpp .S .o .d

%.o: %.S
	@echo "  AS    $<"
	$(Q)$(AS) $(AFLAGS) -o $@ $<

%.o: %.c
	@echo "  CC    $<"
	$(Q)$(CC) $(CFLAGS) -o $@ $<

%.o: %.cpp
	$(Q)$(CC) $(CPPFLAGS) -o $@ $<

%.d: %.S
	@echo "  DEPAS $<"
	$(Q)$(DEPCC) $(AFLAGS) -MT $(notdir $(patsubst %.S,%.o,$<)) -MF $@ $<

%.d: %.c
	@echo "  DEPCC $<"
	$(Q)$(DEPCC) $(CFLAGS) -MT $(notdir $(patsubst %.c,%.o,$<)) -MF $@ $<
