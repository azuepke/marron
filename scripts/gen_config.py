#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2017 Alexander Zuepke
#
# gen_config.py -- Generate config.c with full kernel configuration
#
# azuepke, 2017-10-27: initial from gen_memrqs.xml
#
#
# What does this tool do?
#
# This tool generates the config.c of the kernel, based on both hardware.xml
# and config.xml
#
# example for dummy run:
# $ ./gen_config.py -c config.xml -hw hardware.xml config.c
#
# example for final run:
# $ ./gen_config.py -c config.xml -hw hardware.xml --final config.c

import gen_core as gen
import argparse


# --- main ---

# main
def main():
    prog = 'gen_config.py'
    gen.set_progname(prog)

    parser = argparse.ArgumentParser(prog=prog,
                                     description='Generate config.c from XML config and dummy ELF files.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity')
    parser.add_argument('--deps', action='store_true',
                        help='generate dependencies')
    parser.add_argument('--final', action='store_true',
                        help='final run (resolve addresses from ELF files)')
    parser.add_argument('--semi', action='store_true',
                        help='semi-final run (resolve addresses from application ELF files only)')
    parser.add_argument('-hw', metavar='hardware.xml', nargs=1, type=str,
                        help='hardware description (XML)')
    parser.add_argument('-c', metavar='config.xml', nargs=1, type=str,
                        help='configuration (XML)')
    parser.add_argument('-nm', metavar='nm tool', nargs=1, type=str,
                        help='name mangling tool to use, e.g. arm-linux-gnueabihf-nm')
    parser.add_argument('cfg', metavar='config.c', nargs=1, type=str,
                        help='kernel configuration (C)')

    args = parser.parse_args()
    gen.set_verbose(args.verbose)

    # default argument for nm
    if args.nm is not None:
        gen.set_nm_tool(args.nm[0])

    # parse hardware.xml
    if args.hw is None:
        gen.die("need hardware.xml")
    gen.parse_hardware_xml(args.hw[0])

    # parse config.xml
    if args.c is None:
        gen.die("need config.xml")
    gen.parse_config_xml(args.c[0])

    # dependency generator
    if args.deps is not None:
        if args.deps:
            gen.gen_config_d(args.cfg[0], args.hw[0], args.c[0])
            return

    # dummy, semi-final or final run
    semi = False
    final = False
    if args.final is not None:
        final = args.final
    if args.semi is not None:
        semi = args.semi

    if final:
       semi = False

    # resolve addresses
    if final:
        gen.Partition.resolve_all(True, True)
        gen.gen_allocate_all()
    elif semi:
        gen.Partition.resolve_all(False, True)
        gen.gen_allocate_all()

    gen.gen_config_c(args.cfg[0], semi or final)


if __name__ == '__main__':
    main()
