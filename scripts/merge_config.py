#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2023 Alexander Zuepke
#
# merge_config.py -- Merge kernel configuration files
#
# azuepke, 2023-08-07: initial
#
#
# What does this tool do?
#
# This tool merges content of merge.xml into config.xml or hardware.xml
#
# Usage:
# $ ./merge_config.py -o output.xml config_or_hardware.xml [merge1.xml [merge2.xml]]
#
# For merging into a config.xml file, the target partition is needed as well:
# $ ./merge_config.py -o output.xml -p part1 config.xml [merge1.xml [merge2.xml]]

#import gen_core as gen
import xml.etree.ElementTree as ET
import argparse
import time
import sys


# global variables
verbose = False
progname = 'merge_config.py'

# print to stderr and abort program execution
def die(*args, **kwargs):
    print("%s: error:" % progname, *args, file=sys.stderr, **kwargs)
    sys.exit(1)


# --- main ---

# main
def main():
    parser = argparse.ArgumentParser(prog=progname,
                                     description='Merge content of merge.xml into config.xml or hardware.xml.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity')
    parser.add_argument('-o', metavar='output.xml', nargs=1, type=str,
                        help='output (XML)')
    parser.add_argument('-p', metavar='partname', nargs=1, type=str,
                        help='partition name')
    parser.add_argument('c', metavar='config_hardware.xml', nargs=1, type=str,
                        help='configuration or hardware description (XML)')
    parser.add_argument('m', metavar='merge.xml', nargs='*', type=str,
                        help='configuration snippets to merge (XML)')

    args = parser.parse_args()

    if args.o is None:
        die("need output.xml")
    output_filename = args.o[0]

    if args.p is None:
        partname = ''
    else:
        partname = args.p[0]

    # ---

    # parse config_hardware.xml
    base_filename = args.c[0]
    try:
        base_tree = ET.parse(base_filename)
    except ET.ParseError as e:
        die('%s:' % base_filename, e)

    base_xml = base_tree.getroot()
    if base_xml is None:
        die('%s: no root element' % base_filename)
    if base_xml.tag == "hardware":
        parts = base_xml.findall('./kernel')
        if len(parts) == 0:
            die('no <kernel> entries found in %s' % base_filename)
        if len(parts) != 1:
            die('multiple <kernel> entries found in %s' % base_filename)
        target_part = parts[0]
        rootname = './hardware'
        rootpartname = './hardware/kernel'
    elif base_xml.tag == "config":
        if partname != '':
            parts = base_xml.findall('./partition[@name="%s"]' % partname)
            if len(parts) == 0:
                die('<partition name="%s"> not found in %s' % (partname, base_filename))
            if len(parts) != 1:
                die('multiple <partition name="%s"> entries found in %s' % (partname, base_filename))
            target_part = parts[0]
        else:
            target_part = None
        rootname = './config'
        rootpartname = './config/partition'
    else:
        die('%s: must start with <hardware> or <config> tag' % base_filename)

    # parse merge.xml(s)
    for merge_filename in args.m:
        try:
            merge_tree = ET.parse(merge_filename)
        except ET.ParseError as e:
            die('%s:' % merge_filename, e)

        merge_xml = merge_tree.getroot()
        if merge_xml is None:
            die('%s: no root element' % merge_filename)
        if merge_xml.tag != 'merge':
            die('%s: must start with <merge> tag' % merge_filename)

        # merge at global level
        base_xml.append(ET.Comment('from %s' % merge_filename))
        for node in merge_xml.findall('%s/mem' % rootname):
            base_xml.append(node)
        for node in merge_xml.findall('%s/io' % rootname):
            base_xml.append(node)
        for node in merge_xml.findall('%s/shm' % rootname):
            base_xml.append(node)
        for node in merge_xml.findall('%s/file' % rootname):
            base_xml.append(node)
        for node in merge_xml.findall('%s/irq' % rootname):
            base_xml.append(node)
        for node in merge_xml.findall('%s/kldd' % rootname):
            base_xml.append(node)

        # merge at partition level
        if target_part is None:
            # skip is no partition was given
            continue

        target_part.append(ET.Comment('from %s' % merge_filename))
        for node in merge_xml.findall('%s/memrq' % rootpartname):
            target_part.append(node)
        for node in merge_xml.findall('%s/io_map' % rootpartname):
            target_part.append(node)
        for node in merge_xml.findall('%s/shm_map' % rootpartname):
            target_part.append(node)
        for node in merge_xml.findall('%s/file_access' % rootpartname):
            target_part.append(node)
        for node in merge_xml.findall('%s/irq_map' % rootpartname):
            target_part.append(node)
        for node in merge_xml.findall('%s/kldd_call' % rootpartname):
            target_part.append(node)

    # write output.xml
    try:
        fd = open(output_filename, 'wb')

        header = '<!-- generated on: %s -->\n' % time.strftime("%Y-%m-%d %H:%M:%S")
        fd.write(header.encode('utf-8'))

        # NOTE: pretty printing requires Python 3.9
        if sys.version_info >= (3, 9):
            ET.indent(base_xml)

        xml_string = ET.tostring(base_xml)
        fd.write(xml_string)

    except OSError as e:
        die('error writing to "%s":' % output_filename, e)

    finally:
        fd.close()

if __name__ == '__main__':
    main()
