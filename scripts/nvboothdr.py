#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2022 Alexander Zuepke
#
# nvboothdr.py -- add checksummed header for CBOOT for NVIDIA Xavier
#
# usage:
# $ ./nvboothdr.py <infile> <outfile> [<magic (defaults to "KNRL")>]
#
# How stuff works:
#   We append a header of 4K size to the payload data.
#   The header contains two parts, a generic header (named "header" below)
#   of 1136 bytes and a payload descriptor (named "desc") of 2960 bytes.
#   Both the header contain two SHA-256 hashes, one of the payload descriptor
#   in the header, and one of the payload in the payload descriptor.
#   We read the payload, align it to 16 bytes, hash it,
#   then we create the payload descriptor and hash it,
#   finally we create the header, and dump everything into the new file.
#   Note that kernels use 'KRNL' as magic, DTBs use 'DATA'.
#
# azuepke, 2022-06-03: initial

import argparse
import hashlib
import struct
import sys

# align up to next power of twp
def align_up(addr, align):
    return (addr + align - 1) & ~(align - 1)

if __name__ == '__main__':
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        print('usage: test.py <infile> <outfile> [<magic (defaults to "KNRL")>]', file=sys.stderr)
        sys.exit(1)

    # parameters
    infile = sys.argv[1]
    outfile = sys.argv[2]
    magic = b'KRNL'
    if len(sys.argv) == 4:
        magic = bytes(sys.argv[3], 'ASCII')

    # read file
    blob = None
    try:
        fd = open(infile, 'rb')
        blob = fd.read()
        fd.close()

    except OSError as e:
        print('error reading file: %s' % e, file=sys.stderr)
        sys.exit(1)

    # binary blob: aligned to 16 bytes, with 0x80 padding for SHA-256
    blob_len = len(blob)
    blub_len = align_up(blob_len, 16)
    to_add = blub_len - blob_len
    blub = bytearray(blob)
    if to_add > 0:
        blub += struct.pack("<1B %dx" % (to_add - 1), 0x80)
    blub_hash = hashlib.sha256(blub).digest()

    # payload descriptor
    desc = struct.pack("<16x 1L 12x 4s 1L 40x 32s 1024x", 1, magic, blub_len, blub_hash)
    desc_hash = hashlib.sha256(desc).digest()

    # header
    header = struct.pack("<4s 12x 32s 2912x", b'NVDA', desc_hash)

    # write file
    try:
        fd = open(outfile, 'wb')
        fd.write(header)
        fd.write(desc)
        fd.write(blub)
        fd.close()

    except OSError as e:
        print('error writing file: %s' % e, file=sys.stderr)
        sys.exit(1)
