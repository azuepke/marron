#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
# Copyright 2017 Alexander Zuepke
#
# gen_image.py -- Generate final image from final BIN files
#
# azuepke, 2017-11-01: initial from gen_config.xml
#
#
# What does this tool do?
#
# This tool generates the final image comprising the kernel and all binaries
#
# example:
# $ ./gen_image.py -c config.xml -hw hardware.xml image.bin

import gen_core as gen
import argparse


# --- main ---

# main
def main():
    prog = 'gen_image.py'
    gen.set_progname(prog)

    parser = argparse.ArgumentParser(prog=prog,
                                     description='Generate final image from final BIN files.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity')
    parser.add_argument('-hw', metavar='hardware.xml', nargs=1, type=str,
                        help='hardware description (XML)')
    parser.add_argument('-c', metavar='config.xml', nargs=1, type=str,
                        help='configuration (XML)')
    parser.add_argument('-nm', metavar='nm tool', nargs=1, type=str,
                        help='name mangling tool to use, e.g. arm-linux-gnueabihf-nm')
    parser.add_argument('-s', metavar='section', nargs=1, type=str,
                        help='<mem> section, e.g. "ram"')
    parser.add_argument('bin', metavar='image.bin', nargs=1, type=str,
                        help='final image (BIN)')

    args = parser.parse_args()
    gen.set_verbose(args.verbose)

    # default argument for nm
    if args.nm is not None:
        gen.set_nm_tool(args.nm[0])

    # parse hardware.xml
    if args.hw is None:
        gen.die("need hardware.xml")
    gen.parse_hardware_xml(args.hw[0])

    # parse config.xml
    if args.c is None:
        gen.die("need config.xml")
    gen.parse_config_xml(args.c[0])

    # resolve addresses and allocate memory requirements
    gen.Partition.resolve_all(True, True)
    gen.gen_allocate_all()

    section = 'ram'
    if args.s is not None:
        section = args.s[0]

    gen.gen_image(args.bin[0], section)


if __name__ == '__main__':
    main()
