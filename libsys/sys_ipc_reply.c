/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * sys_ipc_reply.c
 *
 * System call library
 *
 * azuepke, 2024-12-29: initial
 */

#include <marron/api.h>
#include <stddef.h>

err_t sys_ipc_reply(
	uint32_t send_rhnd,
	const struct sys_ipc *send_msg)
{
	return sys_ipc_reply_wait(send_rhnd, send_msg, 0, NULL, NULL);
}
