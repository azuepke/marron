/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * sys_ipc_read_exact.c
 *
 * System call library
 *
 * azuepke, 2025-01-26: initial
 */

#include <marron/api.h>
#include <stddef.h>

err_t sys_ipc_read_exact(
	uint32_t rhnd,
	unsigned int id,
	void *buf,
	size_t size,
	size_t offset,
	size_t *read_size)
{
	err_t err;

	err = sys_ipc_read(rhnd, id, buf, size, offset, read_size);
	if ((err != EOK) ||
	    ((read_size != NULL) && (*read_size != size))) {
		return EFAULT;
	}

	return EOK;
}
