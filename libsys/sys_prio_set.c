/* SPDX-License-Identifier: MIT */
/* Copyright 2020 Alexander Zuepke */
/*
 * sys_prio_set.c
 *
 * System call library
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/api.h>
#include <marron/arch_tls.h>
#include <marron/compiler.h>

/* put syscall into a dedicated function */
static __noinline unsigned int sys_prio_set_preempt(unsigned int old_prio)
{
	sys_preempt();
	return old_prio;
}

unsigned int sys_prio_set(unsigned int new_prio)
{
	struct sys_tls *tls = (struct sys_tls *)__tls_base();
	unsigned int next_prio;
	unsigned int old_prio;
	unsigned int max_prio;

	max_prio = tls->max_prio;
	if (new_prio > max_prio) {
		new_prio = max_prio;
	}

	old_prio = tls->user_prio;
	tls->user_prio = new_prio & 0xff;

	barrier();

	next_prio = tls->next_prio;
	if (unlikely(new_prio < next_prio)) {
		return sys_prio_set_preempt(old_prio);
	}

	return old_prio;
}
