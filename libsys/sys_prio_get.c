/* SPDX-License-Identifier: MIT */
/* Copyright 2020 Alexander Zuepke */
/*
 * sys_prio_get.c
 *
 * System call library
 *
 * azuepke, 2020-02-02: initial
 */

#include <marron/api.h>
#include <marron/arch_tls.h>
#include <stddef.h>


unsigned int sys_prio_get(void)
{
	return tls_get_1(offsetof(struct sys_tls, user_prio));
}
