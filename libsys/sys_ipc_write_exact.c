/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * sys_ipc_write_exact.c
 *
 * System call library
 *
 * azuepke, 2025-01-26: initial
 */

#include <marron/api.h>
#include <stddef.h>

err_t sys_ipc_write_exact(
	uint32_t rhnd,
	unsigned int id,
	const void *buf,
	size_t size,
	size_t offset,
	size_t *written_size)
{
	err_t err;

	err = sys_ipc_write(rhnd, id, buf, size, offset, written_size);
	if ((err != EOK) ||
	    ((written_size != NULL) && (*written_size != size))) {
		return EFAULT;
	}

	return EOK;
}
