/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sys_vm_map.c
 *
 * System call library
 *
 * azuepke, 2021-11-09: initial
 */

#include <marron/api.h>

/* NOTE: sys_vm_map_syscall() does not have a proper export! */
/* NOTE: the syscall binding has "addr_p" at a different position */
err_t sys_vm_map_syscall(
	addr_t addr,
	size_t size,
	uint32_t prot,
	uint32_t flags,
	addr_t *map_addr,
	uint32_t fd,
	phys_addr_t offset);

err_t sys_vm_map(
	addr_t addr,
	size_t size,
	uint32_t prot,
	uint32_t flags,
	uint32_t fd,
	phys_addr_t offset,
	addr_t *map_addr)
{
	return sys_vm_map_syscall(addr, size, prot, flags, map_addr, fd, offset);
}
