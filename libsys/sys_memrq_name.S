/* SPDX-License-Identifier: MIT */
/* sys_memrq_name.S -- system call stub for sys_memrq_name() */
/* GENERATED BY scripts/generate_syscall_stubs.sh -- DO NOT EDIT */

#include <syscalls.h>
#include <arch_syscall.h>

SYSCALL_FUNC(IN4, sys_memrq_name, SYSCALL_MEMRQ_NAME)
