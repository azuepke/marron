/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * sys_ipc_wait.c
 *
 * System call library
 *
 * azuepke, 2024-12-29: initial
 */

#include <marron/api.h>
#include <stddef.h>

err_t sys_ipc_wait(
	uint32_t fd,
	uint32_t *recv_rhnd,
	struct sys_ipc *recv_msg)
{
	return sys_ipc_reply_wait(0, NULL, fd, recv_rhnd, recv_msg);
}
