/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * ioreq.c
 *
 * I/O request manager on top of epoll.
 *
 * azuepke, 2025-01-17: initial
 */

#include <ioreq.h>
#include <sys/epoll.h>
#include <stdio.h>
#include <stdlib.h>


/** create epoll instance; exit the program on error */
int ioreq_create_epfd(void)
{
	int epfd;

	epfd = epoll_create1(0);
	if (epfd == -1) {
		perror("epoll_create1");
		exit(EXIT_FAILURE);
	}

	return epfd;
}

/** allocate ioreq structure and register at epoll instance; exits the program on error */
struct ioreq *ioreq_add(int epfd, int fd, int events, int (*callback)(struct ioreq *ioreq), void *priv)
{
	struct ioreq *ioreq;
	struct epoll_event event;
	int ret;

	ioreq = malloc(sizeof(*ioreq));
	if (ioreq == NULL) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	ioreq->epfd = epfd;
	ioreq->fd = fd;
	ioreq->callback = callback;
	ioreq->priv = priv;

	event.events = events;
	event.data.ptr = ioreq;
	ret = epoll_ctl(epfd, EPOLL_CTL_ADD, ioreq->fd, &event);
	if (ret == -1) {
		perror("epoll_ctl");
		exit(EXIT_FAILURE);
	}

	return ioreq;
}

/** unregister ioreq from epoll instance and free ioreq structure; returns fd */
int ioreq_del(struct ioreq *ioreq)
{
	int fd;

	fd = ioreq->fd;
	(void)epoll_ctl(ioreq->epfd, EPOLL_CTL_DEL, fd, NULL);

	free(ioreq);

	return fd;
}

/** wait on epoll instance and return pending ioreq; exits the program on error */
struct ioreq *ioreq_wait(int epfd)
{
	struct epoll_event event;
	int ret;

	ret = epoll_wait(epfd, &event, 1, -1);
	if (ret != 1) {
		perror("epoll_wait");
		exit(EXIT_FAILURE);
	}

	return event.data.ptr;
}
