/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * config_irq_id.c
 *
 * Useful functions.
 *
 * azuepke, 2025-01-16: initial
 */

#include <marron/util.h>
#include <marron/api.h>
#include <string.h>


/** search for IRQs by name in the partition configuration
 *
 * returns the IRQ ID (IRQ number) (>=0) or -1 if not found
 */
int config_irq_id(const char *name)
{
	char iter_name[256];
	uint32_t irq_id;
	uint32_t index;
	err_t err;

	for (index = 0; ; index++) {
		err = sys_irq_iter_name(index, iter_name, sizeof(iter_name));
		if (err != EOK) {
			/* not found */
			break;
		}

		if (strcmp(iter_name, name) == 0) {
			irq_id = -1;
			(void)sys_irq_iter_id(index, &irq_id);
			return irq_id;
		}
	}

	/* not found */
	return -1;
}
