/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * panic_prefix.c
 *
 * Userspace panic.
 *
 * azuepke, 2025-01-16: initial
 */

#include <panic.h>
#include <stdio.h>
#include <marron/api.h>
#include <errno.h>
#include <string.h>

/** user space panic */
void __panic_prefix(const char *file, int line, const char *func)
{
	int err = errno;

	if (file != NULL) {
		printf("%s:%d: ", file, line);
	}
	if (func != NULL) {
		printf("%s: ", func);
	}
	if (err != 0) {
		printf("[errno=%s] ", __strerror(err));
	}
}
