/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * salloc.c
 *
 * Simple memory allocator -- just alloc, no free.
 *
 * azuepke, 2025-01-17: initial
 * azuepke, 2025-02-25: allocate from fd
 */

#include <salloc.h>
#include <marron/api.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <marron/macros.h>
#include "libc_internal.h"


/** initialze a simple memory allocator:
 * - state: internal state, the function will initialize the state on success
 * - memrq_name: memory requirement of the partition to allocate the memory from
 * - requested_size: the size of the memory allocator to create
 *
 * The function opens the memory requirement temporarily
 * and maps the memory requirement into the address space of the caller.
 * Any memory allocations later will be performed on the mapped memory.
 * The function uses an implicit offset of zero into the memory requirement,
 * i.e. memory will be allocated from the start of the memory requirement.
 * Use salloc_init_from_fd() for allocations of fragments in a memory requirement.
 *
 * Returns 0 on success or -1 and sets errno.
 */
int salloc_init(struct salloc *state, const char *memrq_name, size_t requested_size)
{
	int ret;
	int fd;

	fd = __open_memrq(memrq_name, MEMRQ_TYPE_MEM, O_RDWR);
	if (fd == -1) {
		/* errno already set */
		return -1;
	}

	ret = salloc_init_from_fd(state, fd, 0, requested_size);
	/* errno set on failure; sys_fd_close() does not change errno */
	(void)sys_fd_close(fd);

	return ret;
}

/** initialze a simple memory allocator from a memory file descriptor:
 * - state: internal state, the function will initialize the state on success
 * - memrq_fd: open memrq file descriptor to allocate the memory from
 * - memrq_offset: offset in memory requirement (must be page aligned)
 * - requested_size: the size of the memory allocator to create
 *
 * The function maps a fragment specified by "memrq_offset" and "requested_size"
 * of the memory requirement into the address space of the caller.
 * Any memory allocations later will be performed on the mapped memory.
 *
 * Returns 0 on success or -1 and sets errno.
 */
int salloc_init_from_fd(struct salloc *state, int memrq_fd, off_t memrq_offset, size_t requested_size)
{
	unsigned int prot;
	void *addr;
	addr_t next;

	addr = mmap(NULL, requested_size, PROT_READ|PROT_WRITE, MAP_SHARED, memrq_fd, memrq_offset);
	if (addr == MAP_FAILED) {
		/* errno already set */
		return -1;
	}

	state->addr = (char *)addr;
	state->size = requested_size;
	state->used = 0;
	state->phys_addr = 0;
	(void)sys_vm_v2p((addr_t)addr, &state->phys_addr, &prot, &next);

	return 0;
}

/** retrieve free space (in bytes) in memory allocator */
size_t salloc_free_space(struct salloc *state)
{
	return state->size - state->used;
}

/** allocate from memory allocator, returns NULL if the size doesn't fit
 *
 * NOTE: alignment must be a power-of-two and at least 1 byte.
 */
void *salloc_alloc(struct salloc *state, size_t size, size_t align)
{
	size_t cutoff;
	char *start;

	cutoff = ALIGN_UP((addr_t)state->addr, align) - (addr_t)state->addr;
	if (state->used + cutoff + size > state->size) {
		__errno_set(ENOMEM);
		return NULL;
	}

	start = state->addr + state->used + cutoff;
	state->used += cutoff + size;
	return start;
}

/** return physical address of memory allocated from memory allocator */
phys_addr_t salloc_phys_addr(struct salloc *state, void *addr)
{
	size_t offset;

	offset = (char*)addr - state->addr;

	return state->phys_addr + offset;
}
