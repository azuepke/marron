/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * config_cpu_num.c
 *
 * Useful functions.
 *
 * azuepke, 2025-01-16: initial
 */

#include <marron/util.h>
#include <marron/api.h>


/** get number of CPUs available to the partition */
unsigned int config_cpu_num(void)
{
	unsigned int num = 0;

	for (unsigned long cpu = 0, cpu_mask = sys_cpu_mask();
	     (cpu < sizeof(cpu_mask) * 8) && ((cpu_mask & (1ul << cpu)) != 0);
	     cpu++) {
		num++;
	}

	return num;
}
