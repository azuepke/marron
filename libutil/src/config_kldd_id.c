/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * config_kldd_id.c
 *
 * Useful functions.
 *
 * azuepke, 2025-01-16: initial
 */

#include <marron/util.h>
#include <marron/api.h>
#include <string.h>


/** search for KLDD name in the partition configuration
 *
 * returns the ID of the KDD (>=0) or -1 if not found
 */
int config_kldd_id(const char *name)
{
	char iter_name[256];
	int kldd_id;
	err_t err;

	for (kldd_id = 0; ; kldd_id++) {
		err = sys_kldd_name(kldd_id, iter_name, sizeof(iter_name));
		if (err != EOK) {
			/* not found */
			break;
		}

		if (strcmp(iter_name, name) == 0) {
			return kldd_id;
		}
	}

	/* not found */
	return -1;
}
