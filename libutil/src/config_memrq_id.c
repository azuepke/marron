/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * config_memrq_id.c
 *
 * Useful functions.
 *
 * azuepke, 2025-01-16: initial
 */

#include <marron/util.h>
#include <marron/api.h>
#include <string.h>


/** search for memory requirement name in the partition configuration
 *
 * The type of the memory requirement, e.g.
 * - MEMRQ_TYPE_MEM
 * - MEMRQ_TYPE_IO
 * - MEMRQ_TYPE_SHM
 * - MEMRQ_TYPE_FILE
 * must be specified as well.
 *
 * returns the ID of the memory requirement (>=0) or -1 if not found
 */
int config_memrq_id(const char *name, unsigned int memrq_type)
{
	char iter_name[256];
	int memrq_id;
	err_t err;

	for (memrq_id = 0; ; memrq_id++) {
		err = sys_memrq_name(memrq_type, memrq_id, iter_name, sizeof(iter_name));
		if (err != EOK) {
			/* not found */
			break;
		}

		if (strcmp(iter_name, name) == 0) {
			return memrq_id;
		}
	}

	/* not found */
	return -1;
}
