/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * config_memrq_map.c
 *
 * Useful functions.
 *
 * azuepke, 2025-01-17: initial
 */

#include <marron/util.h>
#include <marron/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>


/** map memory requirement of given name and type into virtual address space
 *
 * The name follows the name in the partition configuration.
 * The type of the memory requirement, e.g.
 * - MEMRQ_TYPE_MEM
 * - MEMRQ_TYPE_IO
 * - MEMRQ_TYPE_SHM
 * - MEMRQ_TYPE_FILE
 * must be specified, and the requested size as well.
 *
 * The memory requirement will be mapped with read and write permissions,
 * except for MEMRQ_TYPE_FILE, which is always mapped read-only.
 *
 * returns mapping address on success or MAP_FAILED on failure and sets errno
 */
void *config_memrq_map(const char *memrq_name, unsigned int memrq_type, size_t requested_size)
{
	void *addr;
	int flags;
	int prot;
	int fd;

	if (memrq_type == MEMRQ_TYPE_FILE) {
		flags = O_RDONLY;
	} else {
		flags = O_RDWR;
	}
	fd = __open_memrq(memrq_name, memrq_type, flags);
	if (fd == -1) {
		return (void *)-1;
	}

	if (memrq_type == MEMRQ_TYPE_FILE) {
		prot = PROT_READ;
	} else {
		prot = PROT_READ|PROT_WRITE;
	}
	addr = mmap(NULL, requested_size, prot, MAP_SHARED, fd, 0);
	close(fd);

	/* errno already set on failure */
	return addr;
}
