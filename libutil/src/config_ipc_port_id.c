/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * config_ipc_port_id.c
 *
 * Useful functions.
 *
 * azuepke, 2025-01-16: initial
 */

#include <marron/util.h>
#include <marron/api.h>
#include <string.h>


/** search for IPC endpoint (IPC port) in the partition configuration
 *
 * The type of the IPC endpoint, e.g.
 * - IPC_TYPE_CLIENT
 * - IPC_TYPE_SERVER
 * must be specified as well.
 *
 * returns the ID of the IPC port (>=0) or -1 if not found
 */
int config_ipc_port_id(const char *name, unsigned int ipc_port_type)
{
	char iter_name[256];
	int ipc_port_id;
	err_t err;

	for (ipc_port_id = 0; ; ipc_port_id++) {
		err = sys_ipc_port_name(ipc_port_type, ipc_port_id, iter_name, sizeof(iter_name));
		if (err != EOK) {
			/* not found */
			break;
		}

		if (strcmp(iter_name, name) == 0) {
			return ipc_port_id;
		}
	}

	/* not found */
	return -1;
}
