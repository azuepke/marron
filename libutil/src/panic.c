/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * panic.c
 *
 * Userspace panic.
 *
 * azuepke, 2025-01-16: initial
 */

#include <panic.h>
#include <stdio.h>
#include <marron/api.h>
#include <stdarg.h>
#include <stdlib.h>

/** user space panic */
void __panic(const char *format, ...)
{
	va_list args;

	va_start(args, format);
	vprintf(format, args);
	va_end(args);

	exit(EXIT_FAILURE);
}
