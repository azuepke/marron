/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2025 Alexander Zuepke */
/*
 * assert_ext.c
 *
 * Extended assert.
 *
 * azuepke, 2021-11-16: adapted from assert.c
 * azuepke, 2025-01-16: adapted from myassert.h
 */

#include <assert_ext.h>
#include <stdio.h>
#include <marron/api.h>

/** extended assert internal fail function */
void __assert_ext_fail(const char *cond, unsigned long a, unsigned long b, const char *file, int line, const char *func)
{
	printf("%s:%d: %s: %lu 0x%lx <-> %lu 0x%lx\n", file, line, func, a, a, b, b);
	printf("%s:%d: %s: assertion '%s' failed.\n", file, line, func, cond);
	printf("%s:%d: %s: called from: %p\n", file, line, func, __builtin_return_address(0));

	sys_abort();
}
