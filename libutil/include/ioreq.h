/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * ioreq.c
 *
 * I/O request manager on top of epoll.
 *
 * azuepke, 2025-01-17: initial
 */

#ifndef IOREQ_H_
#define IOREQ_H_

#ifdef __cplusplus
extern "C" {
#endif

/** I/O request structure
 *
 * This structure binds file descriptor "fd" to the epoll instance "epfd",
 * together with a callback when fd indicates readiness, and private data.
 *
 * The structure is allocated by ioreq_add() and freed by ioreq_del().
 * Both functions also register resp. unregister fd from the epoll instance.
 *
 * Usage:
 *
 *   int epfd = ioreq_create_epfd();
 *
 *   ioreq_add(epfd, socked_fd, EPOLLIN|EPOLLOUT, socket_callback, socket_data);
 *   ioreq_add(epfd, timer_fd, EPOLLIN, timer_callback, timer_priv);
 *   ...
 *
 *   while (1) {
 *     struct ioreq *ioreq = ioreq_wait(epfd);
 *     ioreq->callback(ioreq);
 *   }
 *
 * To unregister an ioreq from the epoll instance and free an ioreq structure,
 * use ioreq_del(), then close fd afterwards:
 *
 *   int fd = ioreq_del(ioreq);
 *   close(fd);
 */
struct ioreq {
	int epfd;
	int fd;
	int (*callback)(struct ioreq *ioreq);
	void *priv;
};

/** create epoll instance; exit the program on error */
int ioreq_create_epfd(void);

/** allocate ioreq structure and register at epoll instance; exits the program on error */
struct ioreq *ioreq_add(int epfd, int fd, int events, int (*callback)(struct ioreq *ioreq), void *priv);

/** unregister ioreq from epoll instance and free ioreq structure; returns fd */
int ioreq_del(struct ioreq *ioreq);

/** wait on epoll instance and return pending ioreq; exits the program on error */
struct ioreq *ioreq_wait(int epfd);

#ifdef __cplusplus
}
#endif

#endif
