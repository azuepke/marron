/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * panic.h
 *
 * Userspace panic.
 *
 * azuepke, 2025-01-16: initial
 */

#ifndef PANIC_H_
#define PANIC_H_

#include <marron/compiler.h>

#ifdef __cplusplus
extern "C" {
#endif

/** user space panic */
void __panic(const char *format, ...) __noreturn __printflike(1, 2) __cold;
void __panic_prefix(const char *file, int line, const char *func) __cold;
#ifdef NDEBUG
#define panic(x...) do { __panic(x); } while (0)
#else
#define panic(x...) do { __panic_prefix(__FILE__, __LINE__, __FUNCTION__); __panic(x); } while (0)
#endif

#ifdef __cplusplus
}
#endif

#endif
