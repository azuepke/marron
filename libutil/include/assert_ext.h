/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2025 Alexander Zuepke */
/*
 * assert_ext.h
 *
 * Extended assert.
 *
 * azuepke, 2021-11-16: adapted from assert.h
 * azuepke, 2025-01-16: adapted from myassert.h
 */

#ifndef ASSERT_EXT_H_
#define ASSERT_EXT_H_

#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

/** fail function */
/* NOTE: this function is always available, regardless of NDEBUG settings */
void __assert_ext_fail(const char *cond, unsigned long a, unsigned long b, const char *file, int line, const char *func) __attribute__((__noreturn__));

/** runtime assert */
#ifdef NDEBUG
#define assert3(a, op, b) do { } while (0)
#else
#define assert3(a, op, b) \
	do { \
		if ((a) op (b)) { \
			/* EMPTY */ \
		} else { \
			__assert_ext_fail(#a " " #op " " #b, (unsigned long)(a), (unsigned long)(b), __FILE__, __LINE__, __FUNCTION__); \
		} \
	} while (0)
#endif

#define assert_eq(a, b) assert3(a, ==, b)
#define assert_ne(a, b) assert3(a, !=, b)
#define assert_lt(a, b) assert3(a, <, b)
#define assert_le(a, b) assert3(a, <=, b)
#define assert_gt(a, b) assert3(a, >, b)
#define assert_ge(a, b) assert3(a, >=, b)

#ifdef __cplusplus
}
#endif

#endif
