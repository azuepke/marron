/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * marron/util.h
 *
 * Useful functions.
 *
 * azuepke, 2025-01-16: initial
 */

#ifndef MARRON_UTIL_H_
#define MARRON_UTIL_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/** map memory requirement of given name and type into virtual address space
 *
 * The name follows the name in the partition configuration.
 * The type of the memory requirement, e.g.
 * - MEMRQ_TYPE_MEM
 * - MEMRQ_TYPE_IO
 * - MEMRQ_TYPE_SHM
 * - MEMRQ_TYPE_FILE
 * must be specified, and the requested size as well.
 *
 * The memory requirement will be mapped with read and write permissions,
 * except for MEMRQ_TYPE_FILE, which is always mapped read-only.
 *
 * returns mapping address on success or MAP_FAILED on failure and sets errno
 */
void *config_memrq_map(const char *memrq_name, unsigned int memrq_type, size_t requested_size);

/** search for memory requirement name in the partition configuration
 *
 * The type of the memory requirement, e.g.
 * - MEMRQ_TYPE_MEM
 * - MEMRQ_TYPE_IO
 * - MEMRQ_TYPE_SHM
 * - MEMRQ_TYPE_FILE
 * must be specified as well.
 *
 * returns the ID of the memory requirement (>=0) or -1 if not found
 */
int config_memrq_id(const char *name, unsigned int memrq_type);

/** search for IRQs by name in the partition configuration
 *
 * returns the IRQ ID (IRQ number) (>=0) or -1 if not found
 */
int config_irq_id(const char *name);

/** search for KLDD name in the partition configuration
 *
 * returns the ID of the KDD (>=0) or -1 if not found
 */
int config_kldd_id(const char *name);

/** search for IPC endpoint (IPC port) in the partition configuration
 *
 * The type of the IPC endpoint, e.g.
 * - IPC_TYPE_CLIENT
 * - IPC_TYPE_SERVER
 * must be specified as well.
 *
 * returns the ID of the IPC port (>=0) or -1 if not found
 */
int config_ipc_port_id(const char *name, unsigned int ipc_port_type);

/** get number of CPUs available to the partition */
unsigned int config_cpu_num(void);

#ifdef __cplusplus
}
#endif

#endif
