/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * salloc.h
 *
 * Simple memory allocator -- just alloc, no free.
 *
 * azuepke, 2025-01-17: initial
 * azuepke, 2025-02-25: allocate from fd
 */

#ifndef SALLOC_H_
#define SALLOC_H_

#include <marron/types.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/** simple memory allocator internal state */
struct salloc {
	char *addr;
	size_t size;
	size_t used;
	phys_addr_t phys_addr;
};

/** initialze a simple memory allocator:
 * - state: internal state, the function will initialize the state on success
 * - memrq_name: memory requirement of the partition to allocate the memory from
 * - requested_size: the size of the memory allocator to create
 *
 * The function opens the memory requirement temporarily
 * and maps the memory requirement into the address space of the caller.
 * Any memory allocations later will be performed on the mapped memory.
 * The function uses an implicit offset of zero into the memory requirement,
 * i.e. memory will be allocated from the start of the memory requirement.
 * Use salloc_init_from_fd() for allocations of fragments in a memory requirement.
 *
 * Returns 0 on success or -1 and sets errno.
 */
int salloc_init(struct salloc *state, const char *memrq_name, size_t requested_size);

/** initialze a simple memory allocator from a memory file descriptor:
 * - state: internal state, the function will initialize the state on success
 * - memrq_fd: open memrq file descriptor to allocate the memory from
 * - memrq_offset: offset in memory requirement (must be page aligned)
 * - requested_size: the size of the memory allocator to create
 *
 * The function maps a fragment specified by "memrq_offset" and "requested_size"
 * of the memory requirement into the address space of the caller.
 * Any memory allocations later will be performed on the mapped memory.
 *
 * Returns 0 on success or -1 and sets errno.
 */
int salloc_init_from_fd(struct salloc *state, int memrq_fd, off_t memrq_offset, size_t requested_size);

/** retrieve free space (in bytes) in memory allocator */
size_t salloc_free_space(struct salloc *state);

/** allocate from memory allocator, returns NULL if the size doesn't fit
 *
 * NOTE: alignment must be a power-of-two and at least 1 byte.
 */
void *salloc_alloc(struct salloc *state, size_t size, size_t align);

/** return physical address of memory allocated from memory allocator */
phys_addr_t salloc_phys_addr(struct salloc *state, void *addr);

#ifdef __cplusplus
}
#endif

#endif
