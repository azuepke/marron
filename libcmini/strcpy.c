/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * strcpy.c
 *
 * simple byte-wise strcpy
 *
 * azuepke, 2013-05-03
 */

#include <string.h>

char *strcpy(char *dst, const char *src)
{
	char *d = dst;

	while (*src != '\0') {
		*d++ = *src++;
	}
	*d = '\0';

	return dst;
}
