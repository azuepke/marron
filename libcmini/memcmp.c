/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * memcmp.c
 *
 * simple byte-wise memcmp
 *
 * azuepke, 2013-03-22
 */

#include <string.h>

int memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char *us1 = (const void *)s1;
	const unsigned char *us2 = (const void *)s2;

	while (n > 0) {
		if (*us1 != *us2) {
			return (*us1 < *us2) ? -1 : (*us1 > *us2);
		}
		us1++;
		us2++;
		n--;
	}

	return 0;
}
