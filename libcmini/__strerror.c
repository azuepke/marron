/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * __strerror.c
 *
 * Internal strerror().
 *
 * azuepke, 2013-09-10: initial
 * azuepke, 2018-01-04: imported
 */

#include <marron/error.h>
#include <string.h>

/** strerror() */
const char *__strerror(unsigned int err)
{
	static const char __strerror_names[] =
		"EOK\0"
		"ENOSYS\0"
		"EINVAL\0"
		"ELIMIT\0"
		"EFAULT\0"
		"ETIMEDOUT\0"
		"EAGAIN\0"
		"EBUSY\0"
		"ESTATE\0"
		"ETYPE\0"
		"EPERM\0"
		"ECANCELED\0"
		"ENOMEM\0"
		"EINTR\0"
		"ENOENT\0"
		"ESRCH\0"
		"ENOTSUP\0"
		"EOVERFLOW\0"
		"EDEADLK\0"
		"EACCES\0"
		"EEXIST\0"
		"EBADF\0"
		"ENODEV\0"
		"EMSGSIZE\0"
		"EIO\0"
		"ENOTCONN\0"
		"EDOM\0"
		"ERANGE\0"
		"EILSEQ\0"
		"EMFILE\0"
		"ENFILE\0"
		"EPIPE\0"
		"ENOTTY\0"
		"ELOOP\0"
		"ENOTDIR\0"
		"EISDIR\0"
		"EFBIG\0"
		"ENOSPC\0"
		"EROFS\0"
		"ENXIO\0"
		"ETXTBSY\0"
		"ENOBUFS\0"
		"ENAMETOOLONG\0"
		"ESPIPE\0"
		"ENOTSOCK\0"
		"EAFNOSUPPORT\0"
		"EPROTONOSUPPORT\0"
		"EPROTOTYPE\0"
		"EADDRNOTAVAIL\0"
		"EADDRINUSE\0"
		"ECONNREFUSED\0"
		"EINPROGRESS\0"
		"ECONNABORTED\0"
		"EDESTADDRREQ\0"
		"EISCONN\0"
		"ECONNRESET\0"
		"ENOPROTOOPT\0"
		"ENETDOWN\0"
		"ENETUNREACH\0"
		"EHOSTUNREACH\0"
		"EPROTO\0"
		"E2BIG\0"
		"EALREADY\0"
		"EBADMSG\0"
		"ECHILD\0"
		"EXDEV\0"
		"EMLINK\0"
		"EDQUOT\0"
		"EIDRM\0"
		"ENETRESET\0"
		"ENOTEMPTY\0"
		"ENOTRECOVERABLE\0"
		"EOWNERDEAD\0"
		"EMULTIHOP\0"
		"ENOEXEC\0"
		"ENOLCK\0"
		"ENOLINK\0"
		"ENOMSG\0"
		"ESTALE\0"
		"ENODATA\0"
		"ENOSR\0"
		"ENOSTR\0"
		"ETIME\0"
		"E???\0";
	const char *s = __strerror_names;

	if (err > ETIME + 1) {
		err = ETIME + 1;
	}
	while (err-- > 0) {
		while (*s++ != '\0') {
			;
		}
	}
	return s;
}
