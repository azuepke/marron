/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * strncpy.c
 *
 * simple byte-wise strncpy
 *
 * azuepke, 2013-05-03
 */

#include <string.h>

char *strncpy(char *dst, const char *src, size_t n)
{
	char *d = dst;

	while (n > 0 && *src != '\0') {
		*d++ = *src++;
		n--;
	}
	while (n > 0) {
		*d++ = '\0';
		n--;
	}

	return dst;
}
