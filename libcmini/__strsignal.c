/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * __strsignal.c
 *
 * Internal strsignal().
 *
 * azuepke, 2022-08-12: initial
 */

#include <marron/signal.h>
#include <string.h>

/** strsignal() */
const char *__strsignal(unsigned int sig)
{
	static const char __strsignal_names[] =
		"SIGNULL\0"	/* has no name in API */
		"SIGHUP\0"
		"SIGINT\0"
		"SIGQUIT\0"
		"SIGILL\0"
		"SIGTRAP\0"
		"SIGABRT\0"
		"SIGBUS\0"
		"SIGFPE\0"
		"SIGKILL\0"
		"SIGUSR1\0"
		"SIGSEGV\0"
		"SIGUSR2\0"
		"SIGPIPE\0"
		"SIGALRM\0"
		"SIGTERM\0"
		"SIGCANCEL\0"	/* libc private */
		"SIGCHLD\0"
		"SIGCONT\0"
		"SIGSTOP\0"
		"SIGTSTP\0"
		"SIGTTIN\0"
		"SIGTTOU\0"
		"SIGURG\0"
		"SIGXCPU\0"
		"SIGXFSZ\0"
		"SIGVTALRM\0"
		"SIGPROF\0"
		"SIGWINCH\0"
		"SIGPOLL\0"
		"SIGTIMER\0"	/* libc private */
		"SIGSYS\0"
		"SIGRTMIN+0\0"
		"SIGRTMIN+1\0"
		"SIGRTMIN+2\0"
		"SIGRTMIN+3\0"
		"SIGRTMIN+4\0"
		"SIGRTMIN+5\0"
		"SIGRTMIN+6\0"
		"SIGRTMIN+7\0"
		"SIGRTMIN+8\0"
		"SIGRTMIN+9\0"
		"SIGRTMIN+10\0"
		"SIGRTMIN+11\0"
		"SIGRTMIN+12\0"
		"SIGRTMIN+13\0"
		"SIGRTMIN+14\0"
		"SIGRTMIN+15\0"
		"SIGRTMIN+16\0"
		"SIGRTMIN+17\0"
		"SIGRTMIN+18\0"
		"SIGRTMIN+19\0"
		"SIGRTMIN+20\0"
		"SIGRTMIN+21\0"
		"SIGRTMIN+22\0"
		"SIGRTMIN+23\0"
		"SIGRTMIN+24\0"
		"SIGRTMIN+25\0"
		"SIGRTMIN+26\0"
		"SIGRTMIN+27\0"
		"SIGRTMIN+28\0"
		"SIGRTMIN+29\0"
		"SIGRTMIN+30\0"
		"SIGRTMIN+31\0"
		"SIG???\0";
	const char *s = __strsignal_names;

	if (sig > SIGRTMIN + 31 + 1) {
		sig = SIGRTMIN + 31 + 1;
	}
	while (sig-- > 0) {
		while (*s++ != '\0') {
			;
		}
	}
	return s;
}
