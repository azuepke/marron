/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * strncmp.c
 *
 * simple byte-wise strncmp
 *
 * azuepke, 2025-01-16: initial
 */

#include <string.h>

int strncmp(const char *s1, const char *s2, size_t n)
{
	const unsigned char *us1 = (const void *)s1;
	const unsigned char *us2 = (const void *)s2;

	if (n == 0) {
		return 0;
	}
	n--;

	while ((n > 0) && (*us1 != '\0') && (*us1 == *us2)) {
		us1++;
		us2++;
		n--;
	}

	return (*us1 < *us2) ? -1 : (*us1 > *us2);
}
