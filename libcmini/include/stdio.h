/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * stdio.h
 *
 * Console output handling functions
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported
 */

#ifndef STDIO_H_
#define STDIO_H_

#include <marron/compiler.h>

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_off_t
#define __NEED_size_t
#define __NEED_ssize_t
#define __NEED_va_list
#define __NEED_uint64_t // for putd,putx
#include <bits/alltypes.h>

#ifndef NULL
#ifdef __cplusplus
#define NULL 0L
#else
#define NULL ((void*)0)
#endif
#endif

/* whence mode for lseek() (also defined in fcntl.h and unistd.h) */
#ifndef SEEK_SET
#define SEEK_SET	0		/**< seek from start of file */
#endif
#ifndef SEEK_CUR
#define SEEK_CUR	1		/**< seek from current position */
#endif
#ifndef SEEK_END
#define SEEK_END	2		/**< seek from end of file */
#endif


/** print character */
void putchar(int c);

/** print NUL-terminated string */
void puts(const char *s);

/** simple vprintf() */
void vprintf(const char* format, va_list args);

/** simple printf()
 *
 * field width:
 *  0   zero padding (ignored)
 *  nn  decimal field width
 *
 * length modifiers:
 *  l   long
 *  ll  long long
 *  z   size_t or uintptr_t, native register width
 *
 * conversion specifiers:
 *  c    char
 *  s    string
 *  p    pointer (implicit 'z')
 *  x    hex
 *  d    signed decimal
 *  u    unsigned decimal
 *
 * TIPS:
 * - use '%zx' to print register values
 *
 * NOTE:
 * - for hex numbers, a given field width truncates the number
 * - for decimals, a field width aligns to the right
 */
void printf(const char *format, ...) __printflike(1, 2);

/** dummy implementation of fflush() */
#define fflush(file) do { } while (0)

/** print error text and errno */
void perror(const char *s);

#ifdef __cplusplus
}
#endif

#endif
