/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * string.h
 *
 * String handling functions.
 *
 * azuepke, 2013-03-22
 */

#ifndef STRING_H_
#define STRING_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_size_t
#define __NEED_locale_t
#include <bits/alltypes.h>

#ifndef NULL
#ifdef __cplusplus
#define NULL 0L
#else
#define NULL ((void*)0)
#endif
#endif

/* memcpy.c */
/** simple byte-wise memcpy() */
void *memcpy(void *dst, const void *src, size_t n);

/* memset.c */
/** simple byte-wise memset() */
void *memset(void *s, int c, size_t n);

/* memcmp.c */
/** simple byte-wise memcmp() */
int memcmp(const void *s1, const void *s2, size_t n);

/* strlen.c */
/** simple strlen() */
size_t strlen(const char *s);

/* strcmp.c */
/** simple strcmp() */
int strcmp(const char *s1, const char *s2);

/* strncmp.c */
/** simple strncmp() */
int strncmp(const char *s1, const char *s2, size_t n);

/* strcpy.c */
/** simple strcpy() */
char *strcpy(char *dst, const char *src);

/* strncpy.c */
/** simple strncpy() */
char *strncpy(char *dst, const char *src, size_t n);

/* __strerror.c */
/** extension: simple strerror() to print error name, i.e. Exxx */
const char *__strerror(unsigned int err);

/* __strsignal.c */
/** extension: simple strsignal() to print signal name, i.e. SIGxxx */
const char *__strsignal(unsigned int sig);

#ifdef __cplusplus
}
#endif

#endif
