/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * perror.c
 *
 * stdio
 *
 * azuepke, 2025-01-17: initial
 */

#include <stdio.h>
#include <string.h>
#include "libc_internal.h"

void perror(const char *s)
{
	int err;

	/* save errno before it might get overwritten by puts/putchar */
	err = __errno_get();

	if ((s != NULL) && (*s != '\0')) {
		puts(s);
		putchar(':');
		putchar(' ');
	}
	puts(__strerror(err));
	putchar('\n');
}
