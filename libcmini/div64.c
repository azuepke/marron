/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * div64.c
 *
 * 64-bit division routines for 32-bit systems
 *
 * azuepke, 2021-08-19: initial
 */

/** 64 / 32 -> 32 bit division
 *
 * algorithm from Hacker's Delight, Figure 9-4
 * correction step from https://github.com/ridiculousfish/libdivide
 */
unsigned int udiv64_32(unsigned long long u, unsigned int v);
unsigned int udiv64_32(unsigned long long u, unsigned int v)
{
	unsigned int u1, u0;
	unsigned int un1, un0;
	unsigned int vn1, vn0;
	unsigned int q1, q0;
	unsigned int un32, un21, un10;
	unsigned int rhat;
	unsigned int c1, c2;	/* correction factors */
	int s;	/* shift factor for normalization */

	/* overflow check, also catches division by 0 */
	u1 = (u >> 32) & 0xffffffff;
	u0 = u & 0xffffffff;
	if (u1 >= v) {
		return 0xffffffff;
	}

	s = __builtin_clz(v);

	/* normalize numerator */
	un32 = (u1 << s) | ((u0 >> (-s & 31)) & (-s >> 31));
	un10 = u0 << s;
	un1 = un10 >> 16;
	un0 = un10 & 0xffff;

	/* normalize divisor */
	v <<= s;
	vn1 = v >> 16;
	vn0 = v & 0xffff;

	/* quotient and remainder of first digit */
	q1 = un32 / vn1;
	rhat = un32 - (q1 * vn1);
	/* correction step */
	c1 = q1 * vn0;
	c2 = (rhat << 16) + un1;
	if (c1 > c2) {
		q1 -= ((c1 - c2) > v) ? 2 : 1;
	}

	/* multiply and subtract */
	un21 = (un32 << 16) + un1 - (q1 * v);

	/* quotient and remainder of second digit */
	q0 = un21 / vn1;
	rhat = un21 - (q0 * vn1);
	/* correction step */
	c1 = q0 * vn0;
	c2 = (rhat << 16) + un0;
	if (c1 > c2) {
		q0 -= ((c1 - c2) > v) ? 2 : 1;
	}

	return (q1 << 16) + q0;
}

/** 64 / 64 -> 64 bit division
 *
 * algorithm from Hacker's Delight, Figure 9-5
 */
unsigned long long udiv64_64(unsigned long long u, unsigned long long v);
unsigned long long udiv64_64(unsigned long long u, unsigned long long v)
{
	unsigned int u1, u0;
	unsigned int v1;
	unsigned long long q;
	unsigned int q1, q0;
	unsigned int r;
	int s;	/* shift factor for normalization */

	if ((v >> 32) == 0) {
		if ((u >> 32) < v) {
			/* handles division by zero, etc */
			return udiv64_32(u, v);
		} else {
			u1 = (u >> 32) & 0xffffffff;
			u0 = u & 0xffffffff;
			q1 = udiv64_32(u1, v);
			r = u1 - (q1 * v);
			q0 = udiv64_32(((unsigned long long)r << 32) + u0, v);
			return ((unsigned long long)q1 << 32) + q0;
		}
	}

	/* normalize divisor */
	s = __builtin_clz((v >> 32) & 0xffffffff);
	v1 = ((v << s) >> 32) & 0xffffffff;
	q1 = udiv64_32(u >> 1, v1);

	/* correct quotient */
	q = ((unsigned long long)q1 << s) >> 31;
	if (q != 0) {
		q = q - 1;
	}
	if ((u - (q * v)) >= v) {
		q = q + 1;
	}
	return q;
}

#ifndef __arm__
/* 32-bit ARM has a different interface due to EABI */
unsigned long long __udivdi3(unsigned long long u, unsigned long long v);
unsigned long long __udivdi3(unsigned long long u, unsigned long long v)
{
	return udiv64_64(u, v);
}
#endif
