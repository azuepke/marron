/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * __assert.c
 *
 * Internal __assert()
 *
 * azuepke, 2018-01-04: imported
 */

#include <assert.h>
#include <stdio.h>
#include <marron/api.h>

/** user space assertion */
void __assert_fail(const char *cond, const char *file, int line, const char *func)
{
	printf("%s:%d: %s: assertion '%s' failed.\n", file, line, func, cond);
	printf("%s:%d: %s: called from: %p\n", file, line, func, __builtin_return_address(0));

	sys_abort();
}
