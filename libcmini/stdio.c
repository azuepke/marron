/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * stdio.c
 *
 * Console output handling functions
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2013-05-17: added user mode build
 * azuepke, 2017-10-02: imported and adapted
 */

#include <marron/api.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

void putchar(int c)
{
	if (c == '\n') {
		putchar('\r');
	}

	/* print character using the sys_putc() system call */
	while (sys_putc(c) != 0) {
		/* nothing */
	}
}

void puts(const char *s)
{
	while (*s != '\0')
		putchar(*s++);
}

static const char hex[16] = "0123456789abcdef";

static inline void putx(uint64_t h, int width)
{
	int shift;

	for (shift = width * 4 - 4; shift >= 0; shift -= 4)
		putchar(hex[(h >> shift) & 0xf]);
}

static inline void puto(uint64_t h, int width)
{
	int shift;

	for (shift = width * 3 - 3; shift >= 0; shift -= 3)
		putchar(hex[(h >> shift) & 0x7]);
}

static inline uint64_t divide_by_10(uint64_t n, unsigned int *rem)
{
	/* divide by 10 algorithm from Hacker's delight */
	uint64_t q, r;
	uint32_t t;

	q = (n >> 1) + (n >> 2);
	q += q >> 4;
	q += q >> 8;
	q += q >> 16;
	q += q >> 32;
	q >>= 3;
	r = n - (q << 3) - (q << 1);	/* n - (q * 10) */
	t = (r + 6) >> 4;
	q += t;							/* q + (r > 9) */

	*rem = (uint32_t)r - (t << 3) - (t << 1);
	return q;
}

static inline void putd(uint64_t num, int width)
{
	unsigned int rem;
	char tmp[21];		/* 2^64-1 = "18446744073709551615\0", 21 chars */
	int pos;

	pos = sizeof(tmp) - 1;
	if (width > pos)
		width = pos;

	tmp[pos] = '\0';
	do {
		num = divide_by_10(num, &rem);
		pos--;
		tmp[pos] = '0' + rem;
	} while (num > 0);

	while ((int)sizeof(tmp) - 1 - pos < width) {
		pos--;
		tmp[pos] = ' ';
	}

	puts(&tmp[pos]);
}

void vprintf(const char* format, va_list args)
{
	uint64_t num;
	int l, z;	/* length modifiers: long, long long, size_t */
	int fmode;
	int sign;
	int base;
	int width;
	char c;

	goto reset_statemachine;
	while ((c = *format++) != '\0') {
		if (!fmode) {
			/* normal text mode */
			if (c == '%') {
				fmode = 1;
			} else {
				putchar(c);
			}
			continue;
		}

		/* field witdth */
		if (c >= '0' && c <= '9') {
			width = width * 10 + (c - '0');
			continue;
		}

		/* length modifiers */
		switch (c) {
		case 'l':
			l++;
			continue;
		case 'z':
			z = 1;
			continue;

		default:
			break;
		}

		/* conversion specifiers */
		switch (c) {
		case '%':
			putchar(c);
			goto reset_statemachine;

		case 'c':
			putchar(va_arg(args, int));
			goto reset_statemachine;

		case 's':
			puts(va_arg(args, char *));
			goto reset_statemachine;

		case 'p':	/* pointer */
			z = 1;
			fallthrough;

		case 'x':	/* hex */
		case 'X':
			base = 16;
			break;

		case 'd':	/* decimal */
			sign = 1;
			fallthrough;

		case 'u':
			base = 10;
			break;

		case 'o':	/* octal */
			base = 8;
			break;

		default:
			break;
		}

		/* "z" overrides l or ll and any given field width */
		if (z) {
			l = 1;
			width = 0;
		}

		if (l == 0) {
			if (sign) {
				num = va_arg(args, int);
			} else {
				num = va_arg(args, unsigned int);
			}
		}
#if __SIZEOF_LONG__ < 8
		else if (l >= 2) {
			num = va_arg(args, unsigned long long);
		}
#endif
		else {
			if (sign) {
				num = va_arg(args, long);
			} else {
				num = va_arg(args, unsigned long);
			}
			/* indicate in l that a long is just 32-bit */
#if __SIZEOF_LONG__ < 8
			l = 0;
#endif
		}

		/* hex */
		if (base == 16) {
			/* a given width overrides natural width */
			putx(num, width ? width : (l ? 16 : 8));
			goto reset_statemachine;
		}

		/* octal */
		if (base == 8) {
			/* a given width overrides natural width */
			puto(num, width ? width : (l ? 22 : 11));
			goto reset_statemachine;
		}

		/* decimal */
		if (sign && ((int64_t)num < 0)) {
			num = -num;
			putchar('-');
		}
		putd(num, width);

reset_statemachine:
		fmode = 0;
		l = 0;
		z = 0;
		sign = 0;
		base = 0;
		width = 0;
	}
}

/* disable varargs FPU prologue, printing floats is not supported */
#if defined(__aarch64__)
#pragma GCC target ("general-regs-only")
#endif

void printf(const char* format, ...)
{
	va_list args;

	va_start(args, format);
	vprintf(format, args);
	va_end(args);
}
