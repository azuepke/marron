/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2014, 2021 Alexander Zuepke */
/*
 * rbtree_test.c
 *
 * Testcase for red-black tree.
 *
 * $ gcc -g -Og -W -Wall -Wextra -Werror rbtree_test.c -o rbtree_test
 *
 * azuepke, 2013-10-09: initial
 * azuepke, 2014-03-23: adapted to red-black trees
 * azuepke, 2021-08-27: adapted to use parent pointer
 * azuepke, 2021-09-16: testcase for augmented trees
 */

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/* always enable assert() */
#undef NDEBUG
#include <assert.h>

#define RBTREE_HEADER_ONLY
//#define RBTREE_HEADER_INSTANTIATE
#include "../include/marron/rbtree.h"

/* from compiler.h */
/** container_of() */
/* see http://www.kroah.com/log/linux/container_of.html */
#define container_of(ptr, type, member) ({	\
		typeof(((type *)0)->member) *__mptr = (ptr);	\
		(type *)((char *)__mptr - offsetof(type, member));	\
	})
#define TEST_FROM_NODE(n) rbtree_entry((n), struct test, rb)


/** a test rb data structure */
struct test {
	rbtree_node_t rb;

	unsigned int key;

	/* node ID number */
	unsigned int id;

	/* for testing the augmentation hook */
	unsigned int sum_of_children;
};

/** comparator: returns TRUE if \a n1 and \a n2 are in order */
static int cmp(const rbtree_node_t *n1, const rbtree_node_t *n2)
{
	assert(n1 != NULL);
	assert(n2 != NULL);

	if (((struct test *)n1)->key < ((struct test *)n2)->key) return -1;
	if (((struct test *)n1)->key > ((struct test *)n2)->key) return 1;
	return 0;
}

/** access red-black node pointer and color */
//#define RB_PTR(n)	((rbtree_node_t *)((n) & ~3UL))
//#define RB_COLOR(n)	((int)(n) & 3)
#define RB_PTR(n)	((rbtree_node_t *)n)
#define RB_COLOR(n)	({	\
		rbtree_node_t *__n = (rbtree_node_t *)(n);	\
		int color = 0;	\
		if (__n != NULL) {	\
			color = __n->parent_color & 0x1;	\
		}	\
		color;	\
	})

static int augment(rbtree_node_t *n)
{
	struct test *t = (struct test *)n;
	unsigned int sum_of_children;
	struct test *l, *r;

	sum_of_children = 0;

	l = (struct test *)RB_PTR(n->link[0]);
	if (l != NULL) {
		sum_of_children += l->sum_of_children + l->id;
	}
	r = (struct test *)RB_PTR(n->link[1]);
	if (r != NULL) {
		sum_of_children += r->sum_of_children + r->id;
	}

	if (t->sum_of_children != sum_of_children) {
		t->sum_of_children = sum_of_children;
		return 1;
	}
	return 0;
}

unsigned int rb_check_counter;

/**
 * test a red-black tree for consistency
 * NOTE: internal routine, with pointer to parent
 */
static unsigned int _rb_check(unsigned long _n)
{
	unsigned int height_l, height_r;
	unsigned int sum_of_children;
	rbtree_node_t *n, *l, *r;
	int dir;

	n = RB_PTR(_n);
	if (n == 0) {
		/* leaves are black */
		assert(_n == 0);
		return 0;
	}

	rb_check_counter++;

	if (RB_COLOR(_n)) {
		/* when I'm already red, my children mustn't be red */
		assert(!RB_COLOR(n->link[0]));
		assert(!RB_COLOR(n->link[1]));
	}

	sum_of_children = 0;

	/* check order */
	l = RB_PTR(n->link[0]);
	if (l != NULL) {
		dir = cmp(l, n);
		assert(dir <= 0);
		sum_of_children += ((struct test *)l)->sum_of_children + ((struct test *)l)->id;
	}
	r = RB_PTR(n->link[1]);
	if (r != NULL) {
		dir = cmp(n, r);
		assert(dir <= 0);
		sum_of_children += ((struct test *)r)->sum_of_children + ((struct test *)r)->id;
	}

	/* heights on left and right side must be equal */
	height_l = _rb_check((unsigned long)n->link[0]);
	height_r = _rb_check((unsigned long)n->link[1]);
	assert(height_l == height_r);
	(void)height_l;
	(void)height_r;
	assert(sum_of_children == ((struct test *)n)->sum_of_children);

	/* only black nodes account to the tree's height */
	if (RB_COLOR(_n) == 0)
		return height_l + 1;
	return height_l;
}

/**
 * test a red-black tree for consistency
 */
void rb_check(rbtree_root_t *r)
{
	unsigned long n = (unsigned long)r->root_node;

	rb_check_counter = 0;

	/* the root must be black */
	assert(RB_COLOR(n) == 0);
	_rb_check(n);
}

/**
 * create a new node with a given key (and incrementing ID for testing)
 */
static inline struct test *test_new(unsigned int key)
{
	static unsigned int id = 0;
	struct test *t;

	t = malloc(sizeof(*t));
	if (t != NULL) {
		t->key = key;
		t->id = id++;
		t->sum_of_children = -1;
	}

	return t;
}

unsigned int rb_check_key;
unsigned int rb_check_id;

/**
 * dump a single element
 */
static void _test_print(unsigned long _n, unsigned int level)
{
	unsigned int i;
	struct test *t;
	rbtree_node_t *n;

	n = RB_PTR(_n);
	assert(n != NULL);
	t = (struct test *)n;

	for (i = 0; i < level; i++) {
		printf("    "); /* 4 spaces */
	}

	printf("node %p", n);

	for (; i < 12; i++) {
		printf("    "); /* 4 spaces */
	}

	printf("%5s - id %3d, key %3d\n", //, L:%p(%d) R:%p(%d)\n",
	       RB_COLOR(_n) ? "\033[1;31mred  \033[0;34m" : "\033[1;30mblack\033[0;34m",
	       t->id, t->key
	       //, RB_PTR(n->link[0]), RB_COLOR(n->link[0]),
	       //, RB_PTR(n->link[1]), RB_COLOR(n->link[1]),
	      );

	if (t->key == rb_check_key) {
		assert(t->id < rb_check_id);
	} else {
		rb_check_key = t->key;
	}
	rb_check_id = t->id;
}


/**
 * internal dump and validator function, follows the right path first
 */
static void _test_dump(unsigned long _n, unsigned int level)
{
	rbtree_node_t *n, *l, *r;

	n = RB_PTR(_n);
	assert(n != NULL);

	//assert(level <= 16);

	r = RB_PTR(n->link[1]);
	if (r != NULL) {
		_test_dump((unsigned long)n->link[1], level + 1);
	}

	_test_print(_n, level + 1);

	l = RB_PTR(n->link[0]);
	if (l != NULL) {
		_test_dump((unsigned long)n->link[0], level + 1);
	}
}

/**
 * dump rb
 */
void test_dump(rbtree_root_t *r)
{
	unsigned long n = (unsigned long)r->root_node;

	printf("test_dump\n");

	/* we do in-order traversal and check FIFO order on same keys */
	rb_check_key = 0xffffffff;
	rb_check_id = 0xffffffff;

	printf("\033[34m");	/* blau */
	fflush(stdout);
	if (RB_PTR(n) != NULL) {
		_test_dump(n, 0);
	} else {
		printf("\tempty\n");
	}
	printf("\033[m");	/* normal */
	fflush(stdout);
}

int main(int argc, char *argv[])
{
	struct test *e;
	rbtree_root_t root;
	unsigned int i;
	unsigned int j;

	printf("\033[m");	/* normal */
	fflush(stdout);

	if (argc == 2)
		i = atoi(argv[1]);
	else
		i = time(NULL) * getpid();
	printf("*** SRAND %u ***\n", i);
	srand(i);

#define MAX 300
	rbtree_root_init(&root);
	for (i = 0; i < MAX; i++) {
		unsigned int key;
		key = rand() % MAX;	/* random */
		//key = MAX - i;		/* decreasing */
		//key = i;			/* increasing */
		printf("\n\n****** insert element %d (key %d) ******\n\n", i, key);
		e = test_new(key);

#if 1
		rbtree_insert_cmp(&root, &e->rb, cmp, augment);
#else
		rbtree_insert(&root, &e->rb, e->key == TEST_FROM_NODE(__ITER__)->key ? 0 :
		                           e->key < TEST_FROM_NODE(__ITER__)->key ? -1 : 1);
#endif
		//test_dump(&root);
		rb_check(&root);

		assert(rb_check_counter == i+1);
	}

	rbtree_node_t *n = rbtree_find(&root, TEST_FROM_NODE(__ITER__)->key == 42);
	if (n != NULL) {
		e = TEST_FROM_NODE(n);
		printf("yes, %d!\n", e->id);
	} else {
		printf("no!\n");
	}

	printf("\n\n");
	printf("******************************************************");
	printf("******************************************************");
	printf("\n\t\t\tnow going to delete ...\n\n");
	//exit(0);
	test_dump(&root);

	j = i;
	do {
		rb_check(&root); // FIXME

		printf("\n\n****** deleteMin ******\n\n");
#if 1
		e = (struct test *)rbtree_remove_core(&root, rbtree_first(&root), augment);
#else
		e = (struct test *)rbtree_remove_first(&root);
#endif
		printf("removed id:%d, key:%d\n\n", e->id, e->key);
		//test_dump(&root);
		rb_check(&root); // FIXME
		assert(rb_check_counter == i-1);

		if (1 && (rand() % 3 < 1)) {
			printf("\n\n****** reInsert ******\n\n");
			/* randomize key again */
			e->key = rand() % MAX;
			e->id = j++;
			rbtree_insert_cmp(&root, &e->rb, cmp, augment);
			//test_dump(&root);
			rb_check(&root); // FIXME
		} else {
			free(e);
			i--;
		}
	} while (!rbtree_is_empty(&root));


	/* test for "corner case: start fixup one level below" */
	printf("\nspecial sequence\n\n");
	struct test *e0, *e1, *e2, *e3, *e4, *e5;
	e0 = test_new(0);
	e1 = test_new(1);
	e2 = test_new(2);
	e3 = test_new(3);
	e4 = test_new(4);
	e5 = test_new(5);
	rbtree_insert_cmp(&root, &e0->rb, cmp, augment);
	rbtree_insert_cmp(&root, &e1->rb, cmp, augment);
	rbtree_insert_cmp(&root, &e2->rb, cmp, augment);
	rbtree_remove_core(&root, &e0->rb, augment);
	rbtree_insert_cmp(&root, &e3->rb, cmp, augment);
	rbtree_insert_cmp(&root, &e5->rb, cmp, augment);
	rbtree_insert_cmp(&root, &e4->rb, cmp, augment);
	rbtree_remove_core(&root, &e2->rb, augment);
	rbtree_remove_core(&root, &e1->rb, augment);
	rbtree_remove_core(&root, &e4->rb, augment);
	rbtree_remove_core(&root, &e3->rb, augment);
	rbtree_remove_core(&root, &e5->rb, augment);
	free(e0);
	free(e1);
	free(e2);
	free(e3);
	free(e4);
	free(e5);

	return 0;
}
