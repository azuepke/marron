/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * fixpoint.c
 *
 * Testcase for fix-point conversion.
 *
 * $ gcc -g -Og -W -Wall -Wextra -Werror fixpoint.c -o fixpoint
 *
 * azuepke, 2022-02-10: initial
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>


/** factor and shift for fix-point arithmetic used in conversion */
static inline void factor_and_shift(uint32_t a, uint32_t b, uint32_t *f, uint32_t *s)
{
	uint64_t factor64;
	uint32_t shift;

	assert(a > 0);
	assert(b > 0);

	/* We use an x.y fixed-point format that fits onto 32 bits.
	 * For this, we have to determine the right shift factor.
	 */
	if (a < b) {
		/* max out fraction bits */
		shift = 31;
	} else {
		/* determine right amount of integer bits */
		uint32_t small = b;
		uint32_t large = a;
		uint32_t steps = 0;

		while (large >= small) {
			large >>= 1;
			steps++;
		}
		assert(steps <= 31);
		shift = 32 - steps;
	}
	*s = shift;

	factor64 = (uint64_t)a * (1u << shift);
	*f = factor64 / b;
}

static void test(uint32_t clock_tt, uint32_t clock_ns)
{
	uint32_t f;
	uint32_t s;
	uint32_t step;

	printf("* tt %u, fs %u\n", clock_tt, clock_ns);

	factor_and_shift(clock_tt, clock_ns, &f, &s);
	step = (1ull * f) >> s;
	printf("- ns -> tt: factor %10u %08x shift %2u step %4u tt\n", f, f, s, step);

	factor_and_shift(clock_ns, clock_tt, &f, &s);
	step = (1ull * f) >> s;
	printf("- tt -> ns: factor %10u %08x shift %2u step %4u ns\n", f, f, s, step);
}

int main(int argc, char *argv[])
{
	if (argc >= 2) {
		test(atoi(argv[1]), 1000*1000*1000);	/* user specified clock */
	} else {
		test(        1, 1000*1000*1000);	/*   1 Hz clock */
		test(    15625, 1000*1000*1000);	/*  15.6 KHzclock (PAL lines/s) */
		test(    32768, 1000*1000*1000);	/*  32.7 KHz clock (RTC) */
		test(  3579545, 1000*1000*1000);	/*   3.58 MHz clock (NTSC) */
		test(  4772727, 1000*1000*1000);	/*   4.77 MHz clock (ISA bus) */
		test(  8333333, 1000*1000*1000);	/*   8.33 MHz clock (ISA bus) */
		test( 10000000, 1000*1000*1000);	/*  10 MHz clock */
		test( 14318180, 1000*1000*1000);	/*  14.32 MHz clock (4x NTSC) */
		test( 16666667, 1000*1000*1000);	/*  16.67 MHz clock (2x ISA bus) */
		test( 48000000, 1000*1000*1000);	/*  48 MHz clock (USB) */
		test( 75000000, 1000*1000*1000);	/*  75 MHz clock */
		test(100000000, 1000*1000*1000);	/* 100 MHz clock */
		test(175000000, 1000*1000*1000);	/* 175 MHz clock */
		test(250000000, 1000*1000*1000);	/* 250 MHz clock */
		test(300000000, 1000*1000*1000);	/* 300 MHz clock */
		test(396000000, 1000*1000*1000);	/* 396 MHz clock */
		test(800000000, 1000*1000*1000);	/* 800 MHz clock */
		test(100000000, 1000*1000*1000);	/* 1.0 GHz clock */
		test(120000000, 1000*1000*1000);	/* 1.2 GHz clock */
		test(150000000, 1000*1000*1000);	/* 1.5 GHz clock */
		test(220000000, 1000*1000*1000);	/* 2.2 GHz clock */
	}

	return 0;
}
