/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * normalize.c
 *
 * Testcases for pathname normalization.
 *
 * $ gcc -g -Og -W -Wall -Wextra -Werror normalize.c -o normalize
 *
 * azuepke, 2022-08-18: initial
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>

/* print token of given length */
void print_tok(const char *s, size_t len)
{
	printf("<");
	while (len-- > 0) {
		printf("%c", *s++);
	}
	printf(">");
	fflush(stdout);
}

typedef unsigned int err_t;
#define EOK 0

const char *known_names(unsigned int i)
{
	switch (i) {
	case 0: return "/dev/null";
	case 1: return "/dev/tty0";
	case 2: return "/dev/tty01";
	case 3: return "/dev/eth0";
	case 4: return "/dev/eth";
	case 5: return "/home";
	case 6: return "/home/alex";
	case 7: return "/home/alex2";
	case 8: return "/shm/foo";
	case 9: return "/shm/bar";
	case 10: return "socket/0";
	default: return NULL;
	}
}

void check(const char *pathname)
{
	const char *devname;
	const char *subname;
	unsigned int id;
	size_t len1, len2;

	printf("* path:  <%s>\n", pathname);

	id = 0;
	while ((devname = known_names(id)) != NULL) {
		//printf("  - try: <%s>\n", devname);
		len1 = strlen(devname);
		if (strncmp(devname, pathname, len1) == 0) {
			subname = pathname + len1;
			if ((subname[0] == '\0') || (subname[0] == '/')) {
				len2 = strlen(subname);

				printf("    - dev: ");
				print_tok(devname, len1);
				printf(" ");
				print_tok(subname, len2);
				printf("\n");
			}
		}

		id++;
	}
}

/* iterator template to match IPC port names
 *
 * - iterate over IPC port names and find a port name matching the path name
 * - the path name is split into the actual port name and the file name
 */
void check2(const char *pathname)
{
	char portname[256];
	const char *filename;
	size_t len1, len2;
	unsigned int port_id;
	const char *s1;
	const char *s2;
	//err_t err;

	printf("# path:  <%s>\n", pathname);

	for (port_id = 0; ; port_id++) {
#if 0
		err = sys_ipc_port_name(IPC_TYPE_CLIENT, port_id,
		                        portname, sizeof(portname));
		if (err != EOK) {
			/* not found */
			break;
		}
#else
		s1 = known_names(port_id);
		if (s1 == NULL) {
			break;
		}
		strcpy(portname, s1);
#endif

		/* strcmp */
		s1 = portname;
		s2 = pathname;
		while ((*s1 != '\0') && (*s1 == *s2)) {
			s1++;
			s2++;
		}
		if (*s1 != '\0') {
			/* portname not a substring of pathname */
			continue;
		}

		if ((*s2 == '\0') || (*s2 == '/')) {
			/* pathname matches or is a directory */
			filename = s2;
			/* strlen */
			while (*s2 != '\0') {
				s2++;
			}

			len1 = filename - pathname;
			len2 = s2 - filename;

			printf("    - dev: ");
			print_tok(pathname, len1);
			printf(" ");
			print_tok(filename, len2);
			printf("\n");
		}
	}
}

/* include cwd and normalize path name
 *
 * - buf/size: target buffer
 * - cwd: normalized current working directory, absolute path
 * - path: absolute or relative path
 */
err_t __libc_normalize(char *buf, size_t size, const char *cwd, const char *path)
{
	char *buf_start;
	char *buf_end;
	char last;

	buf_start = buf;
	buf_end = buf + size;

	if (*path != '/') {
		/* relative path -- add cwd, considered to be a normalized pathname */
		/* strncpy */
		last = '\0';
		while ((*cwd != '\0') && (buf < buf_end)) {
			last = *cwd++;
			*buf++ = last;
		}
		if (*cwd != '\0') {
			return ENAMETOOLONG;
		}
		/* append '/' to end of path if missing */
		if (last != '/') {
			if (buf >= buf_end) {
				return ENAMETOOLONG;
			}
			*buf++ = '/';
		}
	} else {
		/* absolute path -- consume leading slashes */
		while (*path == '/') {
			path++;
		}
		*buf++ = '/';
	}

	/* buf contains a slash at the end */
	/* path does not have leading slashes */
	/* we are now at the start of a new file or directory name */
	last = '/';
	while (buf < buf_end) {
		if (path[0] == '\0') {
			break;
		}
		if ((path[0] == '.') && (last == '/')) {
			/* special cases */
			/* "." dot */
			if ((path[1] == '\0') || (path[1] == '/')) {
				/* skip over "./" */
				path += 1;
				goto collapse_slashes;
			}
			if (path[1] == '.') {
				/* ".." dot dot */
				if ((path[2] == '\0') || (path[2] == '/')) {
					/* skip over "../" and remove last directory from buf */
					path += 2;
					if (buf > buf_start + 1) {
						buf--;
						while (buf[-1] != '/') {
							buf--;
						}
					}
					goto collapse_slashes;
				}
			}
		}
		last = *path++;
		*buf++ = last;

		/* collapse further slashes */
		if (last == '/') {
collapse_slashes:
			while (*path == '/') {
				path++;
			}
		}
	}
	if (*path != '\0') {
		return ENAMETOOLONG;
	}
	if (buf >= buf_end) {
		return ENAMETOOLONG;
	}

	/* terminate string */
	*buf = '\0';
	return EOK;
}

void check3(const char *cwd, const char *path, size_t len)
{
	char pathname[256];
	err_t err;

	if (len > sizeof pathname) {
		len = sizeof pathname;
	}
	err = __libc_normalize(pathname, len, cwd, path);
	printf("<%s> + <%s> @ %zd = ", cwd, path, len);
	if (err == EOK) {
		printf("OK: %s (%zd)\n", pathname, strlen(pathname));
	} else {
		printf("ERR\n");
	}
}

int main(void)
{
	check("/home");
	check("/home/a");
	check("/home/alex");
	check("/home/alexa");
	check("/home/alex/goo");

	check2("/home");
	check2("/home/a");
	check2("/home/alex");
	check2("/home/alexa");
	check2("/home/alex/goo");

	check3("", "file", 16);
	check3("/", "file", 16);
	check3("/home", "file", 16);
	check3("/homeverylong", "", 16);
	check3("/homeverylongs", "", 16);
	check3("/homeverylongst", "", 16);	// too long
	check3("/homeverylongstr", "", 16);	// too long
	check3("/home/", "file", 16);
	check3("/home", "file/", 16);
	check3("/home", "file//", 16);
	check3("/home", "file///", 16);
	check3("/home", "file///.", 16);
	check3("/home", "file///./", 16);
	check3("/home", "file///./.", 16);
	check3("/home", "file///./..", 16);
	check3("/home", "file///./../", 16);
	check3("/home", "file///../../", 16);
	check3("/home", "file///../../.", 16);
	check3("/home", "file///../../..", 16);
	check3("/home", "file///../../../", 16);
	check3("/home", "file///.././", 16);
	check3("/home", "file///./../.", 16);
	check3("/home", "file///./.../", 20);
	check3("/home", "file///./.../.", 20);
	check3("/home", "file///./.../.x", 20);
	check3("/home", "file///./..././a", 20);
	check3("/home", "file///./..././ab", 20);
	check3("/home", "file///./..././abc", 20);
	check3("/home", "file///./..././abcd", 20);
	check3("/home", "file///./..././abcde", 20);	// too long
	check3("/home", "file///./..././abcdef", 20);	// too long
	check3("/home", "file///./..././abc/", 20);
	check3("/home", "file///./..././abc/.", 20);
	check3("/home", "file///./..././abc/..", 20);
	check3("/home", "", 0);
	check3("", "", 0);
	check3("", "", 1);
	check3("", "", 2);
	check3("", "", 3);
	check3("", "/", 2);
	check3("/", "/", 2);
	check3("/", "", 2);
	check3("/", "", 1);
	check3("", "/", 1);

	return EXIT_SUCCESS;
}
