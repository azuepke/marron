# SPDX-License-Identifier: MIT
# Copyright 2013, 2021, 2022 Alexander Zuepke
#
# BUILDENV.sh
#
# azuepke, 2022-05-02: new build environment from previous build scripts


# This script is expected to be sourced on the current shell,
# so we use "return" to exit the script. However, in case the script
# is executed directly, we also provide an "exit 1" directly after the return.

usage() {
	echo "# Marron build environment"
	echo "#"
	echo "# info: this script needs to be sourced on the current shell"
	echo "#"
	echo "# Usage: $ source BUILDENV.sh <args...>"
	echo "#             -b, --bsp <bspname>    BSP name (in bsp/ directory)"
	echo "#             -d, --demo <demoname>  demo name (in demos/ directory)"
	echo "#             -B, --bsplist          list BSPs (in bsp/ directory)"
	echo "#             -D, --demolist         list demos (in demos/ directory)"
	echo "#             -A, --archlist         list architectures (in kernel/arch/ directory)"
	echo "#             -S, --subarchlist      list subarchitectures (in kernel/arch/ directory)"
	echo "#             -p, --print            print current build environment"
}

bsplist() {
	echo "# list of BSPs"
	for d in bsp/*/ ; do
		if [ -f "$d/BUILDENV.inc" ]; then
			basename "$d"
		fi
	done
}

demolist() {
	echo "# list of demos"
	for d in demos/*/ ; do
		if [ -f "$d/BUILDENV.inc" ]; then
			basename "$d"
		fi
	done
}

archlist() {
	echo "# list of archtectures"
	for d in kernel/arch/*/ ; do
		if [ -f "$d/BUILDENV.inc" ]; then
			basename "$d"
		fi
	done
}

subarchlist() {
	echo "# list of sub-archtectures"
	for d1 in kernel/arch/*/ ; do
		if [ -f "$d1/BUILDENV.inc" ]; then
			a=$(basename "$d1")
			for d2 in $d1/*.mk ; do
				s=$(basename -s .mk "$d2")
				echo "$a $s"
			done
		fi
	done
}

printcurrent() {
	echo "# current settings"

	# base settings
	if [ -z "$MARRON_BASE" ]; then
		echo "# not configured"
		return
	fi
	echo "MARRON_BASE=$MARRON_BASE"

	# architecture + compiler settings
	echo "MARRON_BSP=$MARRON_BSP"
	echo "MARRON_BSP_DIR=$MARRON_BSP_DIR"
	echo "MARRON_ARCH=$MARRON_ARCH"
	echo "MARRON_SUBARCH=$MARRON_SUBARCH"
	echo "MARRON_SMP=$MARRON_SMP"

	# architecture + compiler settings
	echo "MARRON_DEMO=$MARRON_DEMO"
	echo "MARRON_DEMO_DIR=$MARRON_DEMO_DIR"
	echo "MARRON_APPS=\"$MARRON_APPS\""
	echo "MARRON_LIBS=\"$MARRON_LIBS\""
	echo "MARRON_KLDDS=\"$MARRON_KLDDS\""
}

#-------------------------------------------------------------------------------

ARG_BSP=
ARG_DEMO=

#while shift ; do
while [ -n "$1" ]; do
	#[ -z "$1" ] && break
	case "$1" in
		-h|--help)
			usage
			return
			exit 1
			;;
		-p|--print)
			printcurrent
			return
			exit 1
			;;
		-b|--bsp)
			if [ -z "$2" ]; then
				echo "# error: \"$1\" requires a BSP name"
				return
			fi
			ARG_BSP=$2
			shift
			shift
			;;
		-B|--bsplist)
			bsplist
			return
			exit 1
			;;
		-d|--demo)
			if [ -z "$2" ]; then
				echo "# error: \"$1\" requires a demo name"
				return
			fi
			ARG_DEMO=$2
			shift
			shift
			;;
		-D|--demolist)
			demolist
			return
			exit 1
			;;
		-A|--archlist)
			archlist
			return
			exit 1
			;;
		-S|--subarchlist)
			subarchlist
			return
			exit 1
			;;
		*)
			usage
			return
			exit 1
			;;
	esac
done

#-------------------------------------------------------------------------------

echo "# Marron build environment"

MARRON_DEMO=$ARG_DEMO
if [ -z "$MARRON_DEMO" ]; then
	MARRON_DEMO=playground
	echo "# info: no --demo <demoname> given, defaulting to built-in --demo $MARRON_DEMO"
fi
#echo "# demo: $MARRON_DEMO"

MARRON_DEMO_DIR="demos/$MARRON_DEMO"
if [ ! -d "$MARRON_DEMO_DIR" ]; then
	echo "# error: $MARRON_DEMO_DIR: demo directory not found"
	demolist
	return
	exit 1
fi
I="$MARRON_DEMO_DIR/BUILDENV.inc"
if [ ! -f "$I" ]; then
	echo "# error: $MARRON_DEMO_DIR: BUILDENV.inc missing in demo directory"
	return
	exit 1
fi

source "$I"
if [ -z "$MARRON_DEMO_NAME" ]; then
	echo "# error: $I: \$MARRON_DEMO_NAME not defined (invalid demo)"
	return
	exit 1
fi
if [ -z "$MARRON_APPS" ]; then
	echo "# error: $I: \$MARRON_APPS not defined (invalid demo)"
	return
	exit 1
fi
if [ -z "$MARRON_LIBS" ]; then
	echo "# error: $I: \$MARRON_LIBS not defined (invalid demo)"
	return
	exit 1
fi
# $MARRON_KLDDS are optional

# prepend demo name to $APPS
apps=
for app in $( echo "$MARRON_APPS" ); do
	if [ -z "$apps" ]; then
		apps=$MARRON_DEMO_DIR/$app
	else
		apps="$apps $MARRON_DEMO_DIR/$app"
	fi
done
MARRON_APPS=$apps
MARRON_CONFIG_XML="$MARRON_DEMO_DIR/config.xml"

# prepend kldd/ directory to $KLDDS
kldds=
for kldd in $( echo "$MARRON_KLDDS" ); do
	if [ -z "$kldds" ]; then
		kldds=kldd/$kldd
	else
		kldds="$kldds kldd/$kldd"
	fi
done
MARRON_KLDDS=$kldds

#-------------------------------------------------------------------------------

MARRON_BSP=$ARG_BSP
if [ -z "$MARRON_BSP" ]; then
	MARRON_BSP=qemu-aarch64
	echo "# info: no --bsp <bspname> given, defaulting to built-in --bsp $MARRON_BSP"
fi
#echo "# BSP: $MARRON_BSP"

MARRON_BSP_DIR="bsp/$MARRON_BSP"
if [ ! -d "$MARRON_BSP_DIR" ]; then
	echo "# error: $MARRON_BSP_DIR: BSP directory not found"
	bsplist
	return
	exit 1
fi
I="$MARRON_BSP_DIR/BUILDENV.inc"
if [ ! -f "$I" ]; then
	echo "# error: $MARRON_BSP_DIR: BUILDENV.inc missing in BSP directory"
	return
	exit 1
fi

source "$I"
if [ -z "$MARRON_BSP_NAME" ]; then
	echo "# error: $I: \$MARRON_BSP_NAME not defined (invalid BSP)"
	return
	exit 1
fi
if [ -z "$MARRON_ARCH" ]; then
	echo "# error: $I: \$MARRON_ARCH not defined (invalid BSP)"
	return
	exit 1
fi
if [ -z "$MARRON_SUBARCH" ]; then
	echo "# error: $I: \$MARRON_SUBARCH not defined (invalid BSP)"
	return
	exit 1
fi
if [ -z "$MARRON_SMP" ]; then
	echo "# error: $I: \$MARRON_SMP not defined (invalid BSP)"
	return
	exit 1
fi
MARRON_HARDWARE_XML="$MARRON_BSP_DIR/hardware.xml"

#-------------------------------------------------------------------------------

# get toolchain (provides $MARRON_CROSS_PREFIX)
I="kernel/arch/$MARRON_ARCH/BUILDENV.inc"
if [ -f "$I" ]; then
	source  "$I"
fi
if [ -z "$MARRON_CROSS_PREFIX" ]; then
	echo "# error: \$MARRON_CROSS_PREFIX not defined (invalid BSP or architecture)"
	return
	exit 1
fi

#-------------------------------------------------------------------------------

echo "# info: building $MARRON_DEMO_NAME for $MARRON_BSP_NAME"

MARRON_BASE=/opt/marron
MARRON_BIN=$MARRON_BASE/bin
MARRON_SCRIPTS=$MARRON_BASE/scripts
MARRON_TARGET=$MARRON_BASE/target/$MARRON_ARCH/$MARRON_SUBARCH
MARRON_ARCHSCRIPTS=$MARRON_TARGET/scripts

export MARRON_BASE=$MARRON_BASE
export MARRON_BIN=$MARRON_BIN
export MARRON_SCRIPTS=$MARRON_SCRIPTS
export MARRON_TARGET=$MARRON_TARGET
export MARRON_ARCHSCRIPTS=$MARRON_ARCHSCRIPTS
export MARRON_CROSS_PREFIX=$MARRON_CROSS_PREFIX

export MARRON_BSP=$MARRON_BSP
export MARRON_BSP_DIR=$MARRON_BSP_DIR
export MARRON_BSP_NAME=$MARRON_BSP_NAME
export MARRON_ARCH=$MARRON_ARCH
export MARRON_SUBARCH=$MARRON_SUBARCH
export MARRON_SMP=$MARRON_SMP
export MARRON_HARDWARE_XML=$MARRON_HARDWARE_XML

export MARRON_DEMO=$MARRON_DEMO
export MARRON_DEMO_DIR=$MARRON_DEMO_DIR
export MARRON_DEMO_NAME=$MARRON_DEMO_NAME
export MARRON_APPS=$MARRON_APPS
export MARRON_LIBS=$MARRON_LIBS
export MARRON_KLDDS=$MARRON_KLDDS
export MARRON_CONFIG_XML=$MARRON_CONFIG_XML
