/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * irq_ctl.c
 *
 * POSIX library.
 *
 * azuepke, 2025-01-06: initial
 */

#include "libc_internal.h"
#include <sys/irq.h>
#include <marron/api.h>


int irq_ctl(int fd, int op, int flags)
{
	err_t err;

	err = sys_irq_ctl(fd, op, flags);
	if (err != EOK) {
		return __errno_return(err);
	}

	return 0;
}
