/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * irq_attach.c
 *
 * POSIX library.
 *
 * azuepke, 2025-01-06: initial
 */

#include "libc_internal.h"
#include <sys/irq.h>
#include <marron/api.h>


int irq_attach(unsigned int irq_id, unsigned int irq_mode, int flags)
{
	uint32_t real_flags;
	uint32_t fd;
	err_t err;

	if ((flags & ~(IRQ_CLOEXEC | IRQ_NONBLOCK)) != 0) {
		return __errno_return(EINVAL);
	}

	real_flags = 0;
	if ((flags & IRQ_CLOEXEC) != 0) {
		real_flags |= SYS_FD_CLOEXEC;
	}
	if ((flags & IRQ_NONBLOCK) != 0) {
		real_flags |= SYS_FD_NONBLOCK;
	}

	err = sys_irq_attach(irq_id, irq_mode, real_flags, &fd);
	if (err != EOK) {
		return __errno_return(err);
	}

	return fd;
}
