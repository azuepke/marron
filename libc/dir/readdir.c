/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * readdir.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>


struct dirent *readdir(DIR *dir)
{
	struct dirent *d;
	size_t len;

	if (dir->buf_pos >= dir->buf_len) {
		/* fill cache buffer */
		len = getdents(dir->fd, dir->buf, sizeof(dir->buf));
		if (len <= 0) {
			/* no more entries or error; errno already set in the latter case */
			return NULL;
		}
		dir->buf_pos = 0;
		dir->buf_len = len;
	}

	d = (struct dirent *)&dir->buf[dir->buf_pos];
	dir->buf_pos += d->d_reclen;
	dir->seek_pos = d->d_off;
	return d;
}
