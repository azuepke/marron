/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * seekdir.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>
#include <unistd.h>


void seekdir(DIR *dir, long loc)
{
	int old_errno;

	dir->buf_pos = 0;
	dir->buf_len = 0;

	old_errno = __errno_get();
	dir->seek_pos = lseek(dir->fd, loc, SEEK_SET);
	__errno_set(old_errno);
}
