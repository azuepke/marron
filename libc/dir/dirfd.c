/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * dirfd.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>


int dirfd(DIR *dir)
{
	return dir->fd;
}
