/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * fdopendir.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <marron/api.h>


DIR *fdopendir(int fd)
{
	uint32_t oflags;
	int old_errno;
	DIR *dir;

	oflags = fcntl(fd, F_GETFL);
	if (oflags == -1u) {
		/* errno already set */
		return NULL;
	}
	if ((oflags & (O_DIRECTORY|__O_READ)) != (O_DIRECTORY|__O_READ)) {
		__errno_set(EBADF);
		return NULL;
	}

	dir = malloc(sizeof(*dir));
	if (dir == NULL) {
		/* errno already set */
		return NULL;
	}
	dir->fd = fd;
	dir->buf_pos = 0;
	dir->buf_len = 0;
	dir->seek_pos = 0;

	old_errno = __errno_get();
	(void)sys_fd_cloexec_set(fd, SYS_FD_CLOEXEC);
	(void)lseek(fd, 0, SEEK_SET);
	__errno_set(old_errno);

	return dir;
}
