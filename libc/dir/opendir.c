/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * opendir.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>


DIR *opendir(const char *dirname)
{
	int old_errno;
	DIR *dir;
	int fd;

	fd = open(dirname, O_RDONLY | O_DIRECTORY | O_CLOEXEC);
	if (fd < 0) {
		/* errno already set */
		return NULL;
	}

	dir = malloc(sizeof(*dir));
	if (dir == NULL) {
		/* errno already set */
		old_errno = __errno_get();
		(void)close(fd);
		__errno_set(old_errno);
		return NULL;
	}
	dir->fd = fd;
	dir->buf_pos = 0;
	dir->buf_len = 0;
	dir->seek_pos = 0;

	return dir;
}
