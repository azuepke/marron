/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * telldir.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>


long telldir(DIR *dir)
{
	return dir->seek_pos;
}
