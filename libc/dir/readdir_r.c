/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * readdir_r.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>
#include <string.h>


int readdir_r(DIR *dir, struct dirent *entry, struct dirent **result)
{
	struct dirent *d;
	int old_errno;
	int err;

	old_errno = __errno_get();
	__errno_set(0);
	d = readdir(dir);
	err = __errno_get();
	__errno_set(old_errno);

	if (d == NULL) {
		if (err == 0) {
			*result = NULL;
		}
		return err;
	}

	memcpy(entry, d, d->d_reclen);
	*result = entry;
	return 0;
}
