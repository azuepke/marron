/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * closedir.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-08: initial
 */

#include "libc_internal.h"
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>


int closedir(DIR *dir)
{
	int fd;

	fd = dir->fd;
	free(dir);
	return close(fd);
}
