/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * ipc_server_attach.c
 *
 * POSIX library.
 *
 * azuepke, 2025-01-14: initial
 */

#include "libc_internal.h"
#include <marron/api.h>
#include <marron/ipc.h>
#include <string.h>


/** attach to IPC server, return file descriptor to IPC server, sets errno */
int __ipc_server_attach(const char *name)
{
	unsigned int ipc_fd;
	char iter_name[256];
	err_t err;

	for (unsigned int i = 0; ; i++) {
		err = sys_ipc_port_name(IPC_TYPE_SERVER, i, iter_name, sizeof(iter_name));
		if (err != EOK) {
			/* not found */
			break;
		}

		if (strcmp(iter_name, name) == 0) {
			err = sys_ipc_port_fd(IPC_TYPE_SERVER, i, &ipc_fd);
			if (err != EOK) {
				/* other error */
				return __errno_return(err);
			}

			return ipc_fd;
		}
	}

	return __errno_return(ENOENT);
}
