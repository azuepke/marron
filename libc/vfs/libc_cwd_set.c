/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * libc_cwd_set.c
 *
 * Current working directory, managed in the process.
 *
 * azuepke, 2022-08-18: initial
 * azuepke, 2025-02-08: free old cwd
 * azuepke, 2025-02-09: rework common code (from chdir.c)
 */

#include <unistd.h>
#include <fcntl.h>
#include "libc_internal.h"
#include <marron/api.h>
#include <stdlib.h>
#include <string.h>


int __libc_cwd_set(const char *pathname, size_t len)
{
	char *old_cwd;
	char *new_cwd;

	new_cwd = malloc(len + 1);
	if (new_cwd == NULL) {
		return __errno_return(ENOMEM);
	}
	memcpy(new_cwd, pathname, len + 1);

	__libc_cwd_lock();
	old_cwd = __libc_cwd_ptr;
	__libc_cwd_ptr = new_cwd;
	__libc_cwd_unlock();

	if (old_cwd != __libc_cwd_default) {
		free(old_cwd);
	}

	return EOK;
}
