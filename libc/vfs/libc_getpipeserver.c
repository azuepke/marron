/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * libc_getpipeserver.c
 *
 * POSIX library internals.
 *
 * azuepke, 2022-08-18: initial
 */

#include "libc_internal.h"
#include <marron/api.h>
#include <marron/compiler.h>


static __noinline int __libc_getpipeserver_find(void)
{
	char portname[256];
	const char *s1;
	const char *s2;
	uint32_t fd;
	err_t err;

	for (unsigned int port_id = 0; ; port_id++) {
		err = sys_ipc_port_name(IPC_TYPE_CLIENT, port_id,
		                        portname, sizeof(portname));
		if (err != EOK) {
			/* not found */
			break;
		}

		/* strcmp */
		s1 = portname;
		s2 = "pipe";
		while ((*s1 != '\0') && (*s1 == *s2)) {
			s1++;
			s2++;
		}
		if (*s1 == *s2) {
			/* pathname matches */
			err = sys_ipc_port_fd(IPC_TYPE_CLIENT, port_id, &fd);
			if (err == EOK) {
				return fd;
			}
			/* try another one */
		}
	}

	/* not found */
	return -1;
}

/* we cache the address of the pipe server */
static int __libc_getpipeserver_cache;

int __libc_getpipeserver(void)
{
	int fd;

	if (__libc_getpipeserver_cache != 0) {
		/* static fds always have a non-zero value */
		return __libc_getpipeserver_cache;
	}

	fd = __libc_getpipeserver_find();
	__libc_getpipeserver_cache = fd;
	return fd;
}
