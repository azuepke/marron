/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * open_irq.c
 *
 * POSIX library.
 *
 * azuepke, 2025-01-14: open IRQs by name
 * azuepke, 2025-01-16: split special open functions from open.c and export
 */

#include "libc_internal.h"
#include <fcntl.h>
#include <marron/api.h>
#include <marron/irq.h>
#include <string.h>


/* iterator to match IRQ names
 *
 * - iterate over IRQ names and find a name matching the path name
 */
static inline int irq_iter(const char *name, int irq_id)
{
	char iter_name[256];
	err_t err;

	for (; ; irq_id++) {
		err = sys_irq_iter_name(irq_id, iter_name, sizeof(iter_name));
		if (err != EOK) {
			/* not found */
			break;
		}

		if (strcmp(iter_name, name) == 0) {
			return irq_id;
		}
	}

	/* not found */
	return -1;
}

int __open_irq(const char *name, int oflags)
{
	unsigned int irq_no;
	unsigned int flags;
	int irq_id;
	uint32_t fd;
	err_t err;

	/* iterate all memory requirements */
	irq_id = irq_iter(name, 0);
	if (irq_id < 0) {
		return __errno_return(ENOENT);
	}
	err = sys_irq_iter_id(irq_id, &irq_no);
	if (err != EOK) {
		/* other error */
		return __errno_return(ENOENT);
	}

	if ((oflags & (O_CREAT|O_EXCL)) == (O_CREAT|O_EXCL)) {
		/* creating /cfg devices not supported */
		return __errno_return(EEXIST);
	}
	if ((oflags & (O_PATH | O_DIRECTORY)) != 0) {
		/* flags not supported */
		return __errno_return(EINVAL);
	}
	if ((oflags & O_ACCMODE) != O_RDWR) {
		/* invalid access mode */
		return __errno_return(EINVAL);
	}

	flags = 0;
	if ((oflags & O_CLOEXEC) != 0) {
		flags |= SYS_FD_CLOEXEC;
	}
	if ((oflags & O_NONBLOCK) != 0) {
		flags |= SYS_FD_NONBLOCK;
	}

	err = sys_irq_attach(irq_no, IRQ_MODE_DEFAULT, flags, &fd);
	if (err != EOK) {
		return __errno_return(err);
	}

	return fd;
}
