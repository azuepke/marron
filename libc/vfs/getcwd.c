/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * getcwd.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-18: initial
 */

#include <unistd.h>
#include "libc_internal.h"
#include <string.h>


char *getcwd(char *buf, size_t size)
{
	char *ret = NULL;
	size_t len;

	__libc_cwd_lock();

	len = strlen(__libc_cwd_ptr) + 1;
	if (len <= size) {
		ret = strcpy(buf, __libc_cwd_ptr);
	}
	__libc_cwd_unlock();

	return ret;
}
