/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * open.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2025-01-16: split special open functions from open.c and export
 * azuepke, 2025-02-09: split again into open.c and libc_open.c
 */

#include "libc_internal.h"
#include <fcntl.h>
#include <stdarg.h>

int open(const char *path, int flags, ...)
{
	char pathname[256];
	va_list args;
	mode_t mode;
	err_t err;

	/* normalize the pathname first */
	err = __libc_normalize(pathname, sizeof(pathname), AT_FDCWD, path);
	if (err != EOK) {
		return __errno_return(err);
	}

	mode = 0;
	if ((flags & O_CREAT) != 0) {
		va_start(args, flags);
		mode = va_arg(args, mode_t);
		va_end(args);
	}

	return __libc_open(pathname, flags, mode);
}
