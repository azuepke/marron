/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * libc_getpipeserver.c
 *
 * POSIX library internals.
 *
 * azuepke, 2022-08-18: initial
 */

#include "libc_internal.h"
#include <marron/api.h>
#include <marron/compiler.h>


static __noinline int __libc_getsocketserver_find(unsigned int domain)
{
	char portname[256];
	char pathname[] = { 's', 'o', 'c', 'k', 'e', 't', ':', '\0', '\0', '\0' };
	const char *s1;
	const char *s2;
	uint32_t fd;
	err_t err;

	if (domain < 10) {
		pathname[7] = '0' + domain;
	} else if (domain < 100) {
		pathname[7] = '0' + (domain / 10);
		pathname[8] = '0' + (domain % 10);
	} else {
		/* only up to 99 packet families possible */
		return -1;
	}

	for (unsigned int port_id = 0; ; port_id++) {
		err = sys_ipc_port_name(IPC_TYPE_CLIENT, port_id,
		                        portname, sizeof(portname));
		if (err != EOK) {
			/* not found */
			break;
		}

		/* strcmp */
		s1 = portname;
		s2 = pathname;
		while ((*s1 != '\0') && (*s1 == *s2)) {
			s1++;
			s2++;
		}
		if (*s1 == *s2) {
			/* pathname matches */
			err = sys_ipc_port_fd(IPC_TYPE_CLIENT, port_id, &fd);
			if (err == EOK) {
				return fd;
			}
			/* try another one */
		}
	}

	/* not found */
	return -1;
}

/* we cache the address of up to four socket servers */
#define NUM 4
static union id {
	struct {
		unsigned int domain;
		int fd;
	};
	unsigned long long asint;
} __libc_getsocketserver_cache[NUM];

int __libc_getsocketserver(unsigned int domain)
{
	unsigned int i;
	union id c;

	for (i = 0; i < NUM; i++) {
		c.asint = __libc_getsocketserver_cache[i].asint;
		if (c.fd == 0) {
			/* empty slot -- static fds always have a non-zero value */
			break;
		}
		if (c.domain == domain) {
			return c.fd;
		}
	}
	if (i == NUM) {
		/* random replacement, based domain value */
		i = domain % (NUM-1);
	}

	c.domain = domain;
	c.fd = __libc_getsocketserver_find(domain);
	if (c.fd < 0) {
		return c.fd;
	}
	__libc_getsocketserver_cache[i].asint = c.asint;
	return c.fd;
}
