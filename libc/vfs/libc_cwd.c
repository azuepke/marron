/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * libc_cwd.c
 *
 * Current working directory, managed in the process.
 *
 * azuepke, 2022-08-18: initial
 * azuepke, 2025-02-08: free old cwd
 */

#include "libc_internal.h"


/* default cwd */
const char __libc_cwd_default[] = "/cfg/file";

/* current cwd */
char *__libc_cwd_ptr = (char *)(uintptr_t)__libc_cwd_default;

/* internal cwd lock */
unsigned int __libc_cwd_the_lock;
