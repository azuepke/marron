/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * open_memrq.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2025-01-16: split special open functions from open.c and export
 */

#include "libc_internal.h"
#include <fcntl.h>
#include <marron/api.h>
#include <string.h>


/* iterator to match memory requirement (memrq, shm, io, file_access) names
 *
 * - iterate over memrq names (of type memrq_type) and find a name matching the path name
 */
static inline int memrq_iter(const char *name, int memrq_type, int memrq_id)
{
	char iter_name[256];
	err_t err;

	for (; ; memrq_id++) {
		err = sys_memrq_name(memrq_type, memrq_id, iter_name, sizeof(iter_name));
		if (err != EOK) {
			/* not found */
			break;
		}

		if (strcmp(iter_name, name) == 0) {
			return memrq_id;
		}
	}

	/* not found */
	return -1;
}

int __open_memrq(const char *name, int memrq_type, int oflags)
{
	unsigned int flags;
	int memrq_id;
	uint32_t fd;
	err_t err;

	/* iterate all memory requirements */
	memrq_id = memrq_iter(name, memrq_type, 0);
	if (memrq_id < 0) {
		return __errno_return(ENOENT);
	}
	if ((oflags & (O_CREAT|O_EXCL)) == (O_CREAT|O_EXCL)) {
		/* creating /cfg devices not supported */
		return __errno_return(EEXIST);
	}

	if ((oflags & (O_PATH | O_DIRECTORY)) != 0) {
		/* flags not supported */
		return __errno_return(EINVAL);
	}
	if ((oflags & (__O_READ | __O_WRITE)) == 0) {
		/* invalid access mode */
		return __errno_return(EINVAL);
	}

	flags = 0;
	if ((oflags & __O_READ) != 0) {
		flags |= SYS_MEMRQ_READ;
	}
	if ((oflags & __O_WRITE) != 0) {
		flags |= SYS_MEMRQ_WRITE;
	}
	if ((oflags & O_CLOEXEC) != 0) {
		flags |= SYS_MEMRQ_CLOEXEC;
	}

	err = sys_memrq_open(memrq_type, memrq_id, flags, &fd);
	if (err != EOK) {
		return __errno_return(err);
	}

	return fd;
}
