/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * chdir.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-18: initial
 * azuepke, 2025-02-09: rework common code
 */

#include <unistd.h>
#include <fcntl.h>
#include "libc_internal.h"
#include <marron/api.h>
#include <stdlib.h>
#include <string.h>


int chdir(const char *path)
{
	char pathname[256];
	ssize_t len;
	int fd;

	fd = open(path, O_PATH | O_DIRECTORY);
	if (fd < 0) {
		/* errno already set */
		return fd;
	}
	len = __getpath(fd, pathname, sizeof(pathname));
	(void)sys_fd_close(fd);

	if (len < 0) {
		/* errno already set */
		return len;
	}

	return __libc_cwd_set(pathname, len);
}
