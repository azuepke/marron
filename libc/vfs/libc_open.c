/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * libc_open.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2025-01-16: split special open functions from open.c and export
 * azuepke, 2025-02-09: split again into open.c and libc_open.c
 */

#include "libc_internal.h"
#include <fcntl.h>
#include <stdarg.h>
#include <marron/api.h>
#include <marron/compiler.h>
#include <string.h>

static int __open_memrq_memrq(const char *name, int flags)
{
	return __open_memrq(name, MEMRQ_TYPE_MEM, flags);
}

static int __open_memrq_io(const char *name, int flags)
{
	return __open_memrq(name, MEMRQ_TYPE_IO, flags);
}

static int __open_memrq_shm(const char *name, int flags)
{
	return __open_memrq(name, MEMRQ_TYPE_SHM, flags);
}

static int __open_memrq_file(const char *name, int flags)
{
	return __open_memrq(name, MEMRQ_TYPE_FILE, flags);
}

/* further dispatcher for prefixes in /cfg/ */
static const struct open_redirect_table {
	char prefix[7];
	unsigned char len;
	int (*func)(const char *name, int flags);
} __open_redirect_table[] = {
	{	.prefix = "memrq/",	.len = 6,	.func = __open_memrq_memrq,	},
	{	.prefix = "io/",	.len = 3,	.func = __open_memrq_io,	},
	{	.prefix = "shm/",	.len = 4,	.func = __open_memrq_shm,	},
	{	.prefix = "file/",	.len = 5,	.func = __open_memrq_file,	},
	{	.prefix = "irq/",	.len = 4,	.func = __open_irq,			},
	{	.prefix = "",		.len = 0,	.func = NULL,				}
};

int __libc_open(const char *pathname, int flags, mode_t mode)
{
	/* check for special files in /cfg/ name space */
	if (strncmp(pathname, "/cfg/", 5) == 0) {
		const char *cfg_pathname = &pathname[5];
		const struct open_redirect_table *r;

		for (r = __open_redirect_table; r->len != 0; r++) {
			if (strncmp(cfg_pathname, r->prefix, r->len) == 0) {
				const char *filename = &cfg_pathname[r->len];
				return r->func(filename, flags);
			}
		}
	}

	return __open_ipc(pathname, flags, mode);
}
