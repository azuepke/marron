/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * open_ipc.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2025-01-16: split special open functions from open.c and export
 */

#include "libc_internal.h"
#include <fcntl.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


/* iterator to match IPC client port names
 *
 * - iterate over IPC port names and find a port name matching the path name
 * - the path name is split into the actual port name and the file name
 */
static inline int ipc_port_iter(const char *pathname, int port_id, struct sys_ipc *m)
{
	char portname[256];
	const char *filename;
	uint32_t fd;
	const char *s1;
	const char *s2;
	err_t err;

	for (; ; port_id++) {
		err = sys_ipc_port_name(IPC_TYPE_CLIENT, port_id, portname, sizeof(portname));
		if (err != EOK) {
			/* not found */
			break;
		}

		/* strcmp */
		s1 = portname;
		s2 = pathname;
		while ((*s1 != '\0') && (*s1 == *s2)) {
			s1++;
			s2++;
		}
		if (*s1 != '\0') {
			/* portname not a substring of pathname */
			continue;
		}

		if ((*s2 == '\0') || (*s2 == '/')) {
			/* pathname matches or is a directory */
			filename = s2;
			/* strlen */
			while (*s2 != '\0') {
				s2++;
			}

			m->buf0_const = pathname;
			m->size0 = filename - pathname;
			m->buf1_const = filename;
			m->size1 = s2 - filename;

			err = sys_ipc_port_fd(IPC_TYPE_CLIENT, port_id, &fd);
			if (err == EOK) {
				return fd;
			}
			/* try another one */
		}
	}

	/* not found */
	return -1;
}

int __open_ipc(const char *path, int flags, mode_t mode)
{
	struct sys_ipc m = { 0 };
	err_t err;
	int pfd;

	/* IPC message */
	m.flags = SYS_IPC_FD0 | SYS_IPC_BUF0R | SYS_IPC_BUF1R;
	if ((flags & O_CLOEXEC) != 0) {
		m.flags |= SYS_IPC_FD_CLOEXEC;
	}
	m.req = IPC_IO_FS_OPEN;
	m.h8 = IPC_IO_FS_OPEN_FILE;
	m.h32 = flags & ~O_CLOEXEC;
	m.h64l = mode;

	/* iterate all matching IPC ports in given order */
	pfd = ipc_port_iter(path, 0, &m);
	if (pfd < 0) {
		return __errno_return(ENOENT);
	}

	err = sys_ipc_call(pfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.fd0;
}
