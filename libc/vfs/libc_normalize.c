/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * libc_normalize.c
 *
 * Pathname normalization.
 *
 * azuepke, 2022-08-18: initial
 * azuepke, 2025-02-09: support *at() functions
 */

#include "libc_internal.h"
#include <fcntl.h>


/* include cwd or getpath(dirfd) and normalize path name
 *
 * - buf/size: target buffer
 * - dirfd: directory for getpath() or AT_FDCWD
 * - path: absolute or relative path
 */
err_t __libc_normalize(char *buf, size_t size, int dirfd, const char *path)
{
	char *buf_start;
	char *buf_end;
	char last;

	buf_start = buf;
	buf_end = buf + size;

	last = *path;
	if (last == '\0') {
		/* empty file name */
		return ENOENT;
	}
	if (last != '/') {
		/* relative path */
		if (dirfd == AT_FDCWD) {
			/* cwd is considered to be a normalized pathname */
			const char *cwd;
			char curr;

			__libc_cwd_lock();
			cwd = __libc_cwd_ptr;
			/* strncpy */
			while (((curr = *cwd) != '\0') && (buf < buf_end)) {
				*buf++ = *cwd++;
			}
			__libc_cwd_unlock();

			if (curr != '\0') {
				return ENAMETOOLONG;
			}
		} else {
			/* getpath also provides normalized pathnames */
			ssize_t len;

			len = __getpath(dirfd, buf, size);
			if (len < 0) {
				return ENOTDIR;
			}
			buf = &buf_start[len];
		}
	}
	if (buf >= buf_end) {
		return ENAMETOOLONG;
	}
	*buf++ = '/';

	/* buf contains a slash at the end */
	/* we collapse any leading slashes in path */
	/* we are now at the start of a new file or directory name */
	last = '/';
	goto collapse_slashes;
	while (buf < buf_end) {
		if (path[0] == '\0') {
			break;
		}
		if ((path[0] == '.') && (last == '/')) {
			/* special cases */
			/* "." dot */
			if ((path[1] == '\0') || (path[1] == '/')) {
				/* skip over "./" */
				path += 1;
				goto collapse_slashes;
			}
			if (path[1] == '.') {
				/* ".." dot dot */
				if ((path[2] == '\0') || (path[2] == '/')) {
					/* skip over "../" and remove last directory from buf */
					path += 2;
					if (buf > buf_start + 1) {
						buf--;
						while (buf[-1] != '/') {
							buf--;
						}
					}
					goto collapse_slashes;
				}
			}
		}
		last = *path++;
		*buf++ = last;

		/* collapse further slashes */
		if (last == '/') {
collapse_slashes:
			while (*path == '/') {
				path++;
			}
		}
	}
	if (*path != '\0') {
		return ENAMETOOLONG;
	}
	if (buf >= buf_end) {
		return ENAMETOOLONG;
	}

	/* terminate string */
	*buf = '\0';
	return EOK;
}
