/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * fchdir.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-18: initial
 * azuepke, 2025-02-09: rework common code
 */

#include <unistd.h>
#include "libc_internal.h"
#include <stdlib.h>
#include <string.h>


int fchdir(int fd)
{
	char pathname[256];
	ssize_t len;

	len = __getpath(fd, pathname, sizeof(pathname));
	if (len < 0) {
		/* errno already set */
		return len;
	}

	return __libc_cwd_set(pathname, len);
}
