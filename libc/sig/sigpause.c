/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigpause.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>


int sigpause(int sig)
{
	sigset_t mask;

	if ((unsigned int)sig >= sizeof(mask) * 8) {
		return __errno_return(EINVAL);
	}

	/* just get the current signal mask */
	(void)__libc_sigmask(SIG_SETMASK, NULL, &mask);
	mask.__bits &= ~(1ULL << sig);

	return sigsuspend(&mask);
}
