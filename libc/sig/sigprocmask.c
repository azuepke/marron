/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigprocmask.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>


int sigprocmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset)
{
	return __errno_return(__libc_sigmask(how, set, oldset));
}
