/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigqueue.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>
#include <marron/api.h>


/** queue signal to process */
int sigqueue(pid_t pid, int sig, const union sigval value)
{
	struct sys_sig_info info;
	err_t err;

	if (pid != 0) {
		return __errno_return(ESRCH);
	}

	if (sig == 0) {
		return EOK;
	}

	info.sig = sig;
	info.code = SI_QUEUE;
	info.ex_type = 0;
	info.status = 0;
	info.addr = (unsigned long)value.sival_ptr;

	err = sys_sig_send(THR_ID_ALL, sig, &info);
	if (err != EOK) {
		return __errno_return(EINVAL);
	}

	return err;
}
