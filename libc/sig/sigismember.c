/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigismember.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>


int sigismember(const sigset_t *set, int sig)
{
	if ((unsigned int)sig >= sizeof(set) * 8) {
		return __errno_return(EINVAL);
	}

	return (set->__bits & (1ULL << sig)) != 0;
}
