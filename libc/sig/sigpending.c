/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigpending.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/api.h>


int sigpending(sigset_t *set)
{
	set->__bits = sys_sig_pending();

	return EOK;
}
