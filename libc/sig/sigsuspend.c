/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigsuspend.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/api.h>


int sigsuspend(const sigset_t *set)
{
	sys_sig_mask_t wait_mask;

	/* NOTE: sigsuspend uses a block mask, not a wait mask */
	wait_mask = ~set->__bits;
	wait_mask |= UNMASKABLE_SIG_BITS;
	sys_sig_suspend(wait_mask);

	return __errno_return(EINTR);
}
