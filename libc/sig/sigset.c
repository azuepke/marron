/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigset.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>


void (*sigset(int sig, void (*disp)(int)))(int)
{
	struct sigaction sa, prev_sa;
	sigset_t mask, prev_mask;
	int how;
	int err;

	if ((unsigned int)sig >= sizeof(mask) * 8) {
		__errno_set(EINVAL);
		return SIG_ERR;
	}

	if (disp == SIG_HOLD) {
		err = sigaction(sig, NULL, &prev_sa);
	} else {
		sa.sa_handler = disp;
		sa.sa_flags = 0;
		sa.sa_mask.__bits = 1ULL << sig;
		err = sigaction(sig, &sa, &prev_sa);
	}
	if (err < 0) {
		/* errno already set */
		return SIG_ERR;
	}

	mask.__bits = 1ULL << sig;
	if (disp == SIG_HOLD) {
		how = SIG_BLOCK;
	} else {
		how = SIG_UNBLOCK;
	}
	err = __libc_sigmask(how, &mask, &prev_mask);
	if (err != 0) {
		__errno_set(err);
		return SIG_ERR;
	}

	if ((prev_mask.__bits & (1ULL << sig)) != 0) {
		return SIG_HOLD;
	} else {
		return prev_sa.sa_handler;
	}
}
