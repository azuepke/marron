/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigwait.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/api.h>

/* internal function; does not set errno, but returns a negative error number */
static int __sigtimedwait(const sigset_t *restrict set, siginfo_t *restrict si, const struct timespec *restrict reltime)
{
	sys_timeout_t timeout;
	struct sys_sig_info info;
	sys_sig_mask_t wait_mask;
	err_t err;

	if ((reltime != NULL) && !TS_VALID(*reltime)) {
		return -EINVAL;
	}

	timeout = __libc_tsopt2ns(reltime);

	wait_mask = set->__bits;
	wait_mask |= UNMASKABLE_SIG_BITS;
	err = sys_sig_wait(wait_mask, 0, timeout, CLK_ID_MONOTONIC, &info);


	if (err != EOK) {
		/* the kernel returns EAGAIN and not ETIMEDOUT, like POSIX expects */
		return -err;
	}

	if (si != NULL) {
		si->si_signo = info.sig;
		si->si_code = info.code;
		si->__si.__si_kern.status = info.status;
		si->__si.__si_kern.ex_type = info.ex_type;
		si->__si.__si_kern.addr = info.addr;
	}

	return info.sig;
}

int sigwait(const sigset_t *restrict set, int *restrict sig)
{
	siginfo_t si = { 0 };
	int err;

	err = __sigtimedwait(set, &si, NULL);
	if (err < 0) {
		/* does not set errno; return positive error number */
		return -err;
	}

	*sig = si.si_signo;
	return EOK;
}

int sigwaitinfo(const sigset_t *restrict set, siginfo_t *restrict si)
{
	return sigtimedwait(set, si, NULL);
}

int sigtimedwait(const sigset_t *restrict set, siginfo_t *restrict si, const struct timespec *restrict reltime)
{
	int err;

	err = __sigtimedwait(set, si, reltime);
	if (err < 0) {
		return __errno_return(-err);
	}
	return err;
}
