/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigprocmask.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-19: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>
#include <marron/api.h>
#include <stddef.h>


/** change current threads signal mask */
int __libc_sigmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset)
{
	sys_sig_mask_t new_mask, clear_mask, set_mask, prev_mask;

	if (set != NULL) {
		new_mask = set->__bits;

		switch (how) {
		case 0: /* SIG_BLOCK */
			clear_mask = SIG_MASK_NONE;
			set_mask = new_mask;
			break;
		case 1: /* SIG_UNBLOCK */
			clear_mask = new_mask;
			set_mask = SIG_MASK_NONE;
			break;

		case 2: /* SIG_SETMASK */
			clear_mask = SIG_MASK_ALL;
			set_mask = new_mask;
			break;

		default:
			return EINVAL;
		}

		/* ensure that special signals are not masked */
		clear_mask |= UNMASKABLE_SIG_BITS;
		set_mask &= ~UNMASKABLE_SIG_BITS;
	} else {
		/* signal mask unchanged and "how" ignored */
		clear_mask = SIG_MASK_NONE;
		set_mask = SIG_MASK_NONE;
	}

	prev_mask = sys_sig_mask(clear_mask, set_mask);

	if (oldset != NULL) {
		oldset->__bits = prev_mask;
	}

	/* always succeeds */
	return EOK;
}
