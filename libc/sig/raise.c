/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * raise.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/api.h>


int raise(int sig)
{
	/* sys_sig_send() performs error checking and return EINVAL if necessary */
	return __errno_return(sys_sig_send(__libc_gettid(), sig, NULL));
}
