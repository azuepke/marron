/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * signal.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>


void (*signal(int sig, void (*func)(int)))(int)
{
	struct sigaction sa, prev_sa;
	int err;

	sa.sa_handler = func;
	sa.sa_flags = SA_RESTART;	/* NOTE: BSD semantics */
	sa.sa_mask.__bits = 0;

	err = sigaction(sig, &sa, &prev_sa);

	if (err < 0) {
		/* errno already set */
		return SIG_ERR;
	}

	return prev_sa.sa_handler;
}
