/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigaction.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>
#include <marron/compiler.h>
#include <marron/api.h>


/** shadow copy of registered actions for signal handlers
 * NOTE: All signals are SIG_DFL by default, which is zero.
 */
static struct sigaction actions[_NSIG];

/* handler for all signals */
static void __libc_signal_handler(struct regs *restrict regs, sys_sig_mask_t sig_mask, struct sys_sig_info *restrict info)
{
	void (*handler)(int, siginfo_t *, void *);
	struct sigaction *as = &actions[info->sig];
	ucontext_t uc = { 0 };
	siginfo_t si = { 0 };

	if (as->sa_handler != SIG_IGN) {
		si.si_signo = info->sig;
		si.si_code = info->code;
		si.__si.__si_kern.status = info->status;
		si.__si.__si_kern.ex_type = info->ex_type;
		si.__si.__si_kern.addr = info->addr;
		if (si.si_signo == SIGTIMER) {
			/* NOTE: special override for timer_create() with default arguments
			 * POSIX states: si_value shall have the value of the timer ID,
			 * which we provide in ex_type.
			 */
			si.__si.__si_kern.addr = info->ex_type;
		}

		uc.uc_mcontext.regs = regs;
		uc.uc_sigmask.__bits = sig_mask;
		//uc.uc_stack.ss_sp = ...; // FIXME
		//uc.uc_stack.ss_size = ...; // FIXME

		handler = as->sa_sigaction;

		if (unlikely((as->sa_flags & SA_RESETHAND) != 0)) {
			as->sa_handler = SIG_DFL;
			as->sa_mask.__bits = 0;
			as->sa_flags = 0;

			(void)sys_sig_register(si.si_signo, NULL, 0, 0);
		}

		handler(si.si_signo, &si, &uc);
	}

	return sys_sig_return(regs, sig_mask);
}

int sigaction(int sig, const struct sigaction *restrict sa, struct sigaction *restrict old_sa)
{
	sig_handler_t sig_handler;
	unsigned int sig_flags;
	sys_sig_mask_t sig_mask;
	err_t err;

	if (((unsigned int)sig >= _NSIG) || (sig == 0) ||
	        (sig == SIGKILL) || (sig == SIGSTOP) ||
	        (sig == SIGCANCEL) || (sig == SIGTIMER)) {
		return __errno_return(EINVAL);
	}

	if (old_sa != NULL) {
		old_sa = &actions[sig];
	}

	if (sa != NULL) {
		sig_mask = sa->sa_mask.__bits;
		/* ensure that special signals are not masked */
		sig_mask &= ~UNMASKABLE_SIG_BITS;
		if ((sa->sa_flags & SA_NODEFER) == 0) {
			sig_mask |= SIG_TO_MASK(sig);
		}

		sig_flags = 0;
		if ((sa->sa_flags & SA_ONSTACK) != 0) {
			sig_flags |= SYS_SIG_STACK;
		}

		sig_handler = __libc_signal_handler;
		if (sa->sa_handler == SIG_DFL) {
			sig_handler = NULL;
		}

		err = sys_sig_register(sig, sig_handler, sig_mask, sig_flags);
		if (err != EOK) {
			return __errno_return(err);
		}

		actions[sig] = *sa;
	}

	return EOK;
}
