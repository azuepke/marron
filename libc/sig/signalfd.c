/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * signalfd.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-14: initial
 */

#include "libc_internal.h"
#include <sys/signalfd.h>
#include <marron/api.h>
#include <signal.h>


int signalfd(int fd, const sigset_t *mask, int flags)
{
	sys_sig_mask_t sig_mask;
	uint32_t real_flags;
	uint32_t newfd;
	err_t err;

	sig_mask = mask->__bits;
	/* ensure that special signals are not masked */
	sig_mask &= ~UNMASKABLE_SIG_BITS;

	if ((flags & ~(SFD_CLOEXEC | SFD_NONBLOCK)) != 0) {
		return __errno_return(EINVAL);
	}

	if (fd == -1) {
		real_flags = 0;
		if ((flags & SFD_CLOEXEC) != 0) {
			real_flags |= SYS_FD_CLOEXEC;
		}
		if ((flags & SFD_NONBLOCK) != 0) {
			real_flags |= SYS_FD_NONBLOCK;
		}

		err = sys_signalfd_create(sig_mask, real_flags, &newfd);
		fd = newfd;
	} else {
		err = sys_signalfd_change(fd, sig_mask, 0);
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return fd;
}
