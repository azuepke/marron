/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * siginterrupt.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>


int siginterrupt(int sig, int flag)
{
	struct sigaction sa;

	(void)sigaction(sig, NULL, &sa);
	if (flag) {
		sa.sa_flags &= ~SA_RESTART;
	} else {
		sa.sa_flags |= SA_RESTART;
	}
	return sigaction(sig, &sa, NULL);
}
