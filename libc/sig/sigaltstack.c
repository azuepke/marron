/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigaltstack.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-31: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>
#include <marron/api.h>


int sigaltstack(const stack_t *restrict ss, stack_t *restrict old_ss)
{
	if (old_ss != NULL) {
		// FIXME: IMPLEMENT ME
		return __errno_return(ENOSYS);
	}

	if (ss != NULL) {
		void *base;
		size_t size;
		int flags;
		err_t err;

		flags = ss->ss_flags;
		if ((flags & SS_DISABLE) != 0) {
			if (flags != 0) {
				__errno_return(EINVAL);
			}

			base = ss->ss_sp;
			if (base == NULL) {
				__errno_return(EINVAL);
			}

			size = ss->ss_size;
			if (size < MINSIGSTKSZ) {
				__errno_return(ENOMEM);
			}
		} else {
			base = NULL;
			size = 0;
		}

		err = sys_sig_stack(base, size);
		if (err != EOK) {
			__errno_return(err);
		}
	}

	return EOK;
}
