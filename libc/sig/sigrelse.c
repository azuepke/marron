/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sigrelse.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>


int sigrelse(int sig)
{
	sigset_t mask;

	if ((unsigned int)sig >= sizeof(mask) * 8) {
		return __errno_return(EINVAL);
	}
	mask.__bits = 1ULL << sig;

	return __errno_return(__libc_sigmask(SIG_UNBLOCK, &mask, NULL));
}
