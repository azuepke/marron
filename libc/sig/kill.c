/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * kill.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <signal.h>
#include <marron/error.h>
#include <marron/api.h>


/** send signal to process */
int kill(pid_t pid, int sig)
{
	err_t err;

	if (pid != 0) {
		return __errno_return(ESRCH);
	}

	if (sig == 0) {
		return EOK;
	}

	err = sys_sig_send(THR_ID_ALL, sig, NULL);
	if (err != EOK) {
		return __errno_return(EINVAL);
	}

	return err;
}
