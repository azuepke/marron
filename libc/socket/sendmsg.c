/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sendmsg.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


ssize_t sendmsg(int sfd, const struct msghdr *msg, int flags)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_IOV | SYS_IPC_BUF0R | SYS_IPC_BUF1R | SYS_IPC_BUF2R;
	m.req = IPC_IO_SEND;
	m.h32 = flags;
	m.buf0 = msg->msg_iov;
	m.size0 = msg->msg_iovlen;
	if (msg->msg_name != NULL) {
		m.buf1 = msg->msg_name;
		m.size1 = msg->msg_namelen;
	}
	if (msg->msg_control != NULL) {
		m.buf2 = msg->msg_control;
		m.size2 = msg->msg_controllen;
	}

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.size0;
}
