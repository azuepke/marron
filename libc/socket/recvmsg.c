/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * recvmsg.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


ssize_t recvmsg(int sfd, struct msghdr *msg, int flags)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_IOV | SYS_IPC_BUF0W | SYS_IPC_BUF1W | SYS_IPC_BUF2W;
	m.req = IPC_IO_SEND;
	m.h32 = flags;
	m.buf0 = msg->msg_iov;
	m.size0 = msg->msg_iovlen;
	if (msg->msg_name != NULL) {
		m.buf1 = msg->msg_name;
		m.size1 = msg->msg_namelen;
	}
	if (msg->msg_control != NULL) {
		m.buf2 = msg->msg_control;
		m.size2 = msg->msg_controllen;
	}

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	msg->msg_iovlen = m.size0;
	if (msg->msg_name != NULL) {
		msg->msg_namelen = m.size1;
	}
	if (msg->msg_control != NULL) {
		msg->msg_controllen = m.size2;
	}
	msg->msg_flags = m.h32;
	return m.size0;
}
