/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * recvfrom.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


ssize_t recvfrom(int sfd, void *buf, size_t size, int flags, struct sockaddr *addr, socklen_t *addrlen)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0W | SYS_IPC_BUF1W;
	m.req = IPC_IO_RECV;
	m.h32 = flags;
	m.buf0 = buf;
	m.size0 = size;
	if (addr != NULL) {
		m.buf1 = addr;
		m.size1 = *addrlen;
	}

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	if (addr != NULL) {
		*addrlen = m.size1;
	}
	return m.size0;
}
