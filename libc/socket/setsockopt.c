/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * setsockopt.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


int setsockopt(int sfd, int level, int optname, const void *optval, socklen_t optlen)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0R;
	m.req = IPC_IO_SETSOCKOPT;
	m.h32 = level;
	m.h64l = optname;
	m.buf0 = (void *)(addr_t)optval;	/* NOTE: const-qualifier warning */
	m.size0 = optlen;

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return err;
}
