/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * recv.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>


ssize_t recv(int sfd, void *buf, size_t size, int flags)
{
	return recvfrom(sfd, buf, size, flags, NULL, NULL);
}
