/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * connect.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


int connect(int sfd, const struct sockaddr *addr, socklen_t addrlen)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0R;
	m.req = IPC_IO_CONNECT;
	m.buf0 = (void *)(addr_t)addr;	/* NOTE: const-qualifier warning */
	m.size0 = addrlen;

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return err;
}
