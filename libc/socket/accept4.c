/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * accept4.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


int accept4(int sfd, struct sockaddr *addr, socklen_t *addrlen, int flags)
{
	struct sys_ipc m = { 0 };
	err_t err;

	if (flags & ~(SOCK_NONBLOCK | SOCK_CLOEXEC)) {
		return __errno_return(EINVAL);
	}

	m.flags = SYS_IPC_BUF0W | SYS_IPC_FD0;
	if ((flags & SOCK_CLOEXEC) != 0) {
		m.flags |= SYS_IPC_FD_CLOEXEC;
	}
	m.req = IPC_IO_ACCEPT;
	if ((flags & SOCK_NONBLOCK) != 0) {
		m.h32 = O_NONBLOCK;
	}
	if (addr != NULL) {
		m.buf0 = addr;
		m.size0 = *addrlen;
	}

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	if (addr != NULL) {
		*addrlen = m.size0;
	}
	return m.fd0;
}
