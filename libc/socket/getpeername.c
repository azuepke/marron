/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * getpeername.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


int getpeername(int sfd, struct sockaddr *addr, socklen_t *addrlen)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0W;
	m.req = IPC_IO_GETPEERNAME;
	m.buf0 = (void *)addr;
	m.size0 = *addrlen;

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	*addrlen = m.size0;

	return err;
}
