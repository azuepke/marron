/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * accept.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>


int accept(int sfd, struct sockaddr *addr, socklen_t *addrlen)
{
	return accept4(sfd, addr, addrlen, 0);
}
