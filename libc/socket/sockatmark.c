/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sockatmark.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <sys/ioctl.h>


int sockatmark(int sfd)
{
	struct sys_ipc m = { 0 };
	err_t err;
	int val;

	m.flags = SYS_IPC_BUF0W;
	m.req = IPC_IO_IOCTL;
	m.h32 = SIOCATMARK;
	m.buf0 = &val;
	m.size0 = sizeof val;

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return val;
}
