/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * socketpair.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


int socketpair(int domain, int type, int protocol, int sfds[2])
{
	struct sys_ipc m = { 0 };
	unsigned int flags;
	err_t err;
	int sfd;

	flags = type & 0xffff0000;
	if (flags & ~(SOCK_NONBLOCK | SOCK_CLOEXEC)) {
		return __errno_return(EINVAL);
	}
	type &= 0xffff;

	m.flags = SYS_IPC_FD0 | SYS_IPC_FD1;
	if ((flags & SOCK_CLOEXEC) != 0) {
		m.flags |= SYS_IPC_FD_CLOEXEC;
	}
	m.req = IPC_IO_FS_OPEN;
	m.h8 = IPC_IO_FS_OPEN_SOCKETPAIR;
	m.h32 = ((flags & SOCK_NONBLOCK) != 0) ? O_NONBLOCK : 0;
	m.h64l = domain;
	m.h64h = type;
	m.a64l = protocol;

	sfd = __libc_getsocketserver(domain);
	if (sfd < 0) {
		return __errno_return(EAFNOSUPPORT);
	}

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	sfds[0] = m.fd0;
	sfds[1] = m.fd1;
	return EOK;
}
