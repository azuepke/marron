/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * getsockopt.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


int getsockopt(int sfd, int level, int optname, void *optval, socklen_t *optlen)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0W;
	m.req = IPC_IO_GETSOCKOPT;
	m.h32 = level;
	m.h64l = optname;
	m.buf0 = (void *)optval;
	m.size0 = *optlen;

	err = sys_ipc_call(sfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	*optlen = m.size0;

	return err;
}
