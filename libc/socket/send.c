/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * send.c
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <sys/socket.h>
#include <marron/api.h>


ssize_t send(int sfd, const void *buf, size_t size, int flags)
{
	return sendto(sfd, buf, size, flags, NULL, 0);
}
