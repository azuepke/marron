/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024 Alexander Zuepke */
/*
 * ppoll.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2024-12-11: poll based on epoll
 */

#include "libc_internal.h"
#include <poll.h>
#include <sys/epoll.h>
#include <marron/api.h>
#include <signal.h>


/*
 * poll() emulation based on epoll
 *
 * We temporarily create an epoll instance for that.
 */

#define EPOLL_OK_MASK	\
	(EPOLLIN | EPOLLPRI | EPOLLOUT | EPOLLERR |	\
	 EPOLLHUP | 0 | EPOLLRDNORM | EPOLLRDBAND |	\
	 EPOLLWRNORM | EPOLLWRBAND | EPOLLMSG | EPOLLRDHUP)

int ppoll(struct pollfd fds[], nfds_t nfds, const struct timespec *reltime, const sigset_t *sigmask)
{
	const sys_sig_mask_t *block_mask_p;
	sys_sig_mask_t block_mask;
	struct pollfd *fdsentry;
	struct sys_epoll event;
	sys_timeout_t timeout;
	unsigned int epfd;
	uint32_t ignored;
	uint32_t events;
	int event_count;
	err_t err;
	int fd;

	if ((reltime != NULL) && !TS_VALID(*reltime)) {
		return __errno_return(EINVAL);
	}
	timeout = __libc_tsopt2ns(reltime);

	if (sigmask != NULL) {
		block_mask = sigmask->__bits;
		block_mask &= ~UNMASKABLE_SIG_BITS;
		block_mask_p = &block_mask;
	} else {
		block_mask_p = NULL;
	}

	err = sys_epoll_create(SYS_FD_CLOEXEC, &epfd);
	if (err != EOK) {
		return __errno_return(ENOMEM);
	}

	for (nfds_t i = 0; i < nfds; i++) {
		fd = fds[i].fd;
		events = fds[i].events & EPOLL_OK_MASK;
		fds[i].revents = 0;

		if (fd < 0) {
			continue;
		}

		event.eventmask = events;
		event.data = (unsigned long)&fds[i];
		err = sys_epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &event);
		if (err == EEXIST) {
			err = sys_epoll_ctl(epfd, EPOLL_CTL_MOD, fd, &event);
		}
		if (err != EOK) {
			fds[i].revents = POLLNVAL;
		}
	}

	event_count = 0;

again:
	err = sys_epoll_wait(epfd, 1, timeout, block_mask_p, &event, &ignored);
	if (err == EOK) {
		fdsentry = (struct pollfd *)(unsigned long)event.data;
		fdsentry->revents = event.eventmask;

		/* drain remaining events without blocking */
		block_mask_p = NULL;
		timeout = 0;

		event_count++;
		goto again;
	} else if (err == EAGAIN) {
		/* EAGAIN is only returned after draining */
		err = EOK;
	} else if (err == ETIMEDOUT) {
		/* the API expects EINTR for timeouts */
		err = EINTR;
	}

	(void)sys_fd_close(epfd);

	if (err != EOK) {
		return __errno_return(err);
	}

	return event_count;
}
