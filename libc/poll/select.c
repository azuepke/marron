/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * select.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <sys/select.h>
#include <marron/api.h>


int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, struct timeval *tv)
{
	struct timespec *timeout;
	struct timespec ts;

	if (tv != NULL) {
		ts.tv_sec = tv->tv_sec;
		ts.tv_nsec = tv->tv_usec * 1000;
		timeout = &ts;
	} else {
		timeout = NULL;
	}

	return pselect(nfds, readfds, writefds, errorfds, timeout, NULL);
}
