/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll_create1.c
 *
 * POSIX library.
 *
 * azuepke, 2022-12-08: initial
 */

#include "libc_internal.h"
#include <sys/epoll.h>
#include <marron/api.h>


int epoll_create1(int flags)
{
	uint32_t real_flags;
	uint32_t epfd;
	err_t err;

	if ((flags & ~EPOLL_CLOEXEC) != 0) {
		return __errno_return(EINVAL);
	}
	real_flags = 0;
	if ((flags & EPOLL_CLOEXEC) != 0) {
		real_flags |= SYS_FD_CLOEXEC;
	}

	err = sys_epoll_create(real_flags, &epfd);
	if (err != EOK) {
		return __errno_return(err);
	}

	return epfd;
}
