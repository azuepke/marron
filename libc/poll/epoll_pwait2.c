/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll_pwait2.c
 *
 * POSIX library.
 *
 * azuepke, 2022-12-08: initial
 */

#include "libc_internal.h"
#include <sys/epoll.h>
#include <marron/signal.h>
#include <marron/api.h>
#include <signal.h>


int epoll_pwait2(int epfd, struct epoll_event *events, int maxevents, const struct timespec *reltime, const sigset_t *sigmask)
{
	const sys_sig_mask_t *block_mask_p;
	sys_sig_mask_t block_mask;
	sys_timeout_t timeout;
	uint32_t event_count;
	err_t err;

	if ((reltime != NULL) && !TS_VALID(*reltime)) {
		return -EINVAL;
	}
	timeout = __libc_tsopt2ns(reltime);

	if (sigmask != NULL) {
		block_mask = sigmask->__bits;
		block_mask &= ~UNMASKABLE_SIG_BITS;
		block_mask_p = &block_mask;
	} else {
		block_mask_p = NULL;
	}

	event_count = 0;
	err = sys_epoll_wait(epfd, maxevents, timeout, block_mask_p, (struct sys_epoll *)events, &event_count);
	if (err == EAGAIN) {
		/* EAGAIN is only returned after polling */
		err = EOK;
	} else if (err == ETIMEDOUT) {
		/* the API expects EINTR also for timeouts */
		err = EINTR;
	}

	if (err != EOK) {
		return __errno_return(err);
	}

	return event_count;
}
