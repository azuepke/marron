/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll_ctl.c
 *
 * POSIX library.
 *
 * azuepke, 2022-12-08: initial
 */

#include "libc_internal.h"
#include <sys/epoll.h>
#include <marron/api.h>


int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event)
{
	err_t err;

	err = sys_epoll_ctl(epfd, op, fd, (struct sys_epoll *)event);

	return __errno_return(err);
}
