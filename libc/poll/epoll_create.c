/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll_create.c
 *
 * POSIX library.
 *
 * azuepke, 2022-12-08: initial
 */

#include "libc_internal.h"
#include <sys/epoll.h>
#include <marron/api.h>


int epoll_create(int size)
{
	if (size < 1) {
		return __errno_return(EINVAL);
	}

	return epoll_create1(0);
}
