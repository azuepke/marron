/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll_pwait.c
 *
 * POSIX library.
 *
 * azuepke, 2022-12-08: initial
 */

#include "libc_internal.h"
#include <sys/epoll.h>
#include <marron/api.h>


int epoll_pwait(int epfd, struct epoll_event *events, int maxevents, int timeout, const sigset_t *sigmask)
{
	struct timespec t;
	struct timespec *tp;

	if (timeout >= 0) {
		t.tv_sec = timeout / 1000;
		t.tv_nsec = (timeout % 1000) * 1000000;
		tp = &t;
	} else {
		tp = NULL;
	}

	return epoll_pwait2(epfd, events, maxevents, tp, sigmask);
}
