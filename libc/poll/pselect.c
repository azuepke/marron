/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024 Alexander Zuepke */
/*
 * pselect.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2024-12-11: select based on epoll
 */

#include "libc_internal.h"
#include <sys/select.h>
#include <sys/epoll.h>
#include <marron/api.h>
#include <signal.h>


int pselect(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, const struct timespec *reltime, const sigset_t *sigmask)
{
	const sys_sig_mask_t *block_mask_p;
	sys_sig_mask_t block_mask;
	struct sys_epoll event;
	sys_timeout_t timeout;
	unsigned int epfd;
	uint32_t ignored;
	uint32_t events;
	int event_count;
	err_t err;

	if (nfds < 1) {
		return __errno_return(EINVAL);
	}
	if (nfds > FD_SETSIZE) {
		nfds = FD_SETSIZE;
	}

	if ((reltime != NULL) && !TS_VALID(*reltime)) {
		return __errno_return(EINVAL);
	}
	timeout = __libc_tsopt2ns(reltime);

	if (sigmask != NULL) {
		block_mask = sigmask->__bits;
		block_mask &= ~UNMASKABLE_SIG_BITS;
		block_mask_p = &block_mask;
	} else {
		block_mask_p = NULL;
	}

	err = sys_epoll_create(SYS_FD_CLOEXEC, &epfd);
	if (err != EOK) {
		return __errno_return(ENOMEM);
	}

	event_count = 0;

	for (int fd = 0; fd < nfds; fd++) {
		events = 0;
		if ((readfds != NULL)  && FD_ISSET(fd, readfds)) {
			events |= EPOLLIN;
		}
		if ((writefds != NULL) && FD_ISSET(fd, writefds)) {
			events |= EPOLLOUT;
		}
		if ((errorfds != NULL) && FD_ISSET(fd, errorfds)) {
			events |= EPOLLPRI;
		}
		if (events == 0) {
			continue;
		}

		event.eventmask = events;
		event.data = fd;
		err = sys_epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &event);
		if (err != EOK) {
			goto out;
		}
	}

	err = sys_epoll_wait(epfd, 1, timeout, block_mask_p, &event, &ignored);
	if ((err == EOK) || (err == EAGAIN) || (err == ETIMEDOUT)) {
		if (readfds != NULL) {
			FD_ZERO(readfds);
		}
		if (writefds != NULL) {
			FD_ZERO(writefds);
		}
		if (errorfds != NULL) {
			FD_ZERO(errorfds);
		}

		while (err == EOK) {
			int fd = event.data;
			if (((event.eventmask & EPOLLIN)  != 0) && (readfds  != NULL)) {
				FD_SET(fd, readfds);
			}
			if (((event.eventmask & EPOLLOUT) != 0) && (writefds != NULL)) {
				FD_SET(fd, writefds);
			}
			if (((event.eventmask & EPOLLPRI) != 0) && (errorfds != NULL)) {
				FD_SET(fd, errorfds);
			}

			event_count++;

			/* drain remaining events without blocking */
			block_mask_p = NULL;
			timeout = 0;
			err = sys_epoll_wait(epfd, 1, timeout, block_mask_p, &event, &ignored);
		}
	}

out:
	(void)sys_fd_close(epfd);

	if (err != EOK) {
		return __errno_return(err);
	}

	return event_count;
}
