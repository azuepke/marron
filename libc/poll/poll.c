/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * poll.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <poll.h>
#include <marron/api.h>


int poll(struct pollfd fds[], nfds_t nfds, int timeout)
{
	struct timespec t;
	struct timespec *tp;

	if (timeout >= 0) {
		t.tv_sec = timeout / 1000;
		t.tv_nsec = (timeout % 1000) * 1000000;
		tp = &t;
	} else {
		tp = NULL;
	}

	return ppoll(fds, nfds, tp, NULL);
}
