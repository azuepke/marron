/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll_wait.c
 *
 * POSIX library.
 *
 * azuepke, 2022-12-08: initial
 */

#include "libc_internal.h"
#include <sys/epoll.h>
#include <marron/api.h>


int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout)
{
	return epoll_pwait(epfd, events, maxevents, timeout, NULL);
}
