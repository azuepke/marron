/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2022, 2024, 2025 Alexander Zuepke */
/*
 * libc_internal.h
 *
 * libc library internal API (can be called from external).
 *
 * azuepke, 2013-09-01: initial
 * azuepke, 2021-07-19: libthr -> libposix rework
 * azuepke, 2022-08-11: libc rework
 * azuepke, 2024-12-08: time handling moved to libc_internal.h
 * azuepke, 2025-02-08: directory services
 */

#ifndef LIBC_INTERNAL_H_
#define LIBC_INTERNAL_H_

#define __NEED_size_t
#define __NEED_mode_t
#define __NEED_off_t
#define __NEED_time_t
#define __NEED_struct_timespec
#define __NEED_suseconds_t
#define __NEED_struct_timeval
#define __NEED_sigset_t
#include <bits/alltypes.h>

#include <marron/error.h>
#include <marron/types.h>
#include <marron/arch_tls.h>
#include <stddef.h>


/* crt */
void _start_c(void *heap_base, size_t heap_size, void *stack_base, size_t stack_size, void *mmap_base);
int main(char argc, char *argv[], char *envp[]);

/* internal light-weight futex-based mutexes */

/* NOTE: the lock is a zero-initialized integer */

/** lock a light-weight futex-based mutex */
void __libc_lwmutex_lock(unsigned int *lock);

/** unlock a light-weight futex-based mutex */
void __libc_lwmutex_unlock(unsigned int *lock);

/** internal IPC call to retrieve pathname from file descriptor */
/* NOTE: also exported in fcntl.h */
ssize_t __getpath(int fd, void *pathname, size_t size);

/** internal IPC call to perform mmap on file descriptor */
void *__libc_mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset);

/** get fd to pipe server */
int __libc_getpipeserver(void);

/** get fd to socket server */
int __libc_getsocketserver(unsigned int domain);

/** get oflags of open file */
int __libc_getoflags(int fd);
int __libc_setoflags(int fd, int oflags);

/** setup thread internal data for the main thread */
/* NOTE: called once at startup time */
void __main_init(void);
extern char *__main_argv[];
extern char *__main_envp[];

/* malloc/heap.c */
void __heap_init(void *base, size_t size);
void *__heap_alloc(size_t size, size_t align);
void *__heap_realloc(void *ptr, size_t size, size_t *oldsize);
void __heap_free(void *ptr);

/* internal malloc implementation */
void __malloc_init(void *base, size_t size);
void *__malloc_noerrno(size_t size);
void *__calloc_noerrno(size_t num, size_t size);
void *__realloc_noerrno(void *ptr, size_t size);

/* vfs */
/** current working directory (cwd) */
extern char *__libc_cwd_ptr;
extern const char __libc_cwd_default[];
extern unsigned int __libc_cwd_the_lock;

int __libc_cwd_set(const char *path, size_t len);

/** lock internal cwd lock */
static inline void __libc_cwd_lock(void)
{
	__libc_lwmutex_lock(&__libc_cwd_the_lock);
}

/** unlock internal cwd lock */
static inline void __libc_cwd_unlock(void)
{
	__libc_lwmutex_unlock(&__libc_cwd_the_lock);
}

/** include cwd and normalize path name */
err_t __libc_normalize(char *buf, size_t size, int dirfd, const char *path);

/** internal open function (requires normalized path name) */
int __libc_open(const char *pathname, int flags, mode_t mode);


/* inlined errno helpers */
__attribute__((always_inline))
static inline void *__errno_addr(void)
{
	struct sys_tls *tls;

#if defined(__i386__) || defined(__x86_64__)
	/* only x86 requires indirect memory access to self pointer */
	tls = tls_get_p(offsetof(struct sys_tls, tls));
#else
	tls = (struct sys_tls *)__tls_base();
#endif

	return &tls->errno_value;
}

__attribute__((always_inline))
static inline int __errno_get(void)
{
	return tls_get_4(offsetof(struct sys_tls, errno_value));
}

__attribute__((always_inline))
static inline void __errno_set(int err)
{
	tls_set_4(offsetof(struct sys_tls, errno_value), err);
}

__attribute__((always_inline))
static inline int __errno_return(int err)
{
	if (err != 0) {
		__errno_set(err);
		return -1;
	}
	return err;
}

/** thread ID of current thread */
__attribute__((always_inline))
static inline unsigned int __libc_gettid(void)
{
	return tls_get_4(offsetof(struct sys_tls, thr_id));
}

/* signals */
#define SIGCANCEL	16	/* private signal for thread cancellation */
#define SIGTIMER	30	/* private signal for timer handler */

#define UNMASKABLE_SIG_BITS	(SIG_TO_MASK(SIGCANCEL) | SIG_TO_MASK(SIGKILL))

/** change current threads signal mask */
int __libc_sigmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset);


/* division by 1000000000 for timeout conversion */

/* high-order 64-bit of 64-bit product */
__attribute__((always_inline))
static inline unsigned long long mulhu64(unsigned long long u, unsigned long long v)
{
#ifdef __SIZEOF_INT128__
	__uint128_t u_ = u, v_ = v;
	__uint128_t t = u_ * v_;
	return (unsigned long long)(t >> 64);
#else
	/* algorithm from Figure 8-1 in Hacker's Delight unrolled & optimized */
	unsigned int w_1, w_2;
	unsigned long long t;

#define hi(x) (((x) >> 32) & 0xffffffffu)
#define lo(x) ((x) & 0xffffffffu)

	t = lo(u)*lo(v);
	/* w_0 = lo(t); */

	t = hi(u)*lo(v) + hi(t);
	w_1 = lo(t);
	w_2 = hi(t);

	t = lo(u)*hi(v) + w_1;
	/* w_1 = lo(t); */

	t = hi(u)*hi(v) + w_2 + hi(t);
	/* w_2 = lo(t); */
	/* w_3 = hi(t); */

#undef hi
#undef lo

	return t;
#endif
}

/* divide by 1000000000 using multiplication with a constant */
__attribute__((always_inline))
static inline unsigned long long div1000000000(unsigned long long num)
{
	unsigned long long tmp;

	tmp = mulhu64(num, 1360296554856532783ull);
	num -= tmp;
	num = tmp + (num >> 1);
	num >>= 29;

	return num;
}

/* timeout conversion */
/* 18446744073709551615 is the maximum value of a 64-bit integer */

/** convert struct timespec to nanoseconds */
__attribute__((always_inline))
static inline sys_time_t __libc_ts2ns(const struct timespec *ts)
{
	return (ts->tv_sec * 1000000000) + ts->tv_nsec;
}

/** convert struct timespec or NULL to nanoseconds, with bounds check */
__attribute__((always_inline))
static inline sys_time_t __libc_tsopt2ns(const struct timespec *ts)
{
	if (ts == NULL) {
		return TIMEOUT_INFINITE;
	}

	if ((unsigned long long)ts->tv_sec >= 18446744073ull) {
		return TIMEOUT_INFINITE;
	}

	return __libc_ts2ns(ts);
}

/** convert nanoseconds to struct timespec */
__attribute__((always_inline))
static inline struct timespec __libc_ns2ts(sys_time_t time)
{
	struct timespec ts;

	ts.tv_sec = div1000000000(time);
	ts.tv_nsec = time - (ts.tv_sec * 1000000000); /* remainder */

	return ts;
}

#define TS_VALID(ts) ((unsigned long)(ts).tv_nsec < 1000000000ul)
#define TS_BOUNDED(ts) ((unsigned long long)(ts).tv_sec < 18446744073ull)

/** convert struct timeval to nanoseconds */
__attribute__((always_inline))
static inline sys_time_t __libc_tv2ns(const struct timeval *tv)
{
	return (tv->tv_sec * 1000000000) + (tv->tv_usec * 1000);
}

/** convert nanoseconds to struct timeval */
__attribute__((always_inline))
static inline struct timeval __libc_ns2tv(sys_time_t time)
{
	struct timeval tv;

	tv.tv_sec = div1000000000(time);
	tv.tv_usec = time - (tv.tv_sec * 1000000000); /* remainder */
	tv.tv_usec /= 1000;

	return tv;
}

#define TV_VALID(tv) ((unsigned long long)(tv).tv_usec < 1000000ull)
#define TV_BOUNDED(tv) ((unsigned long long)(tv).tv_sec < 18446744073ull)

/* directory stream type */
/* NOTE: using the same directory stream concurrently is not thread-safe */
struct __dirstream {
	int fd;
	uint16_t buf_pos;
	uint16_t buf_len;
	off_t seek_pos;
	char buf[2048] __attribute__((__aligned__(8)));
};

#endif
