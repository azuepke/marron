/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * realloc.c
 *
 * Malloc implementation.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-11-11: adapted
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/error.h>
#include <stddef.h>
#include <string.h>


void *__realloc_noerrno(void *ptr, size_t size)
{
	size_t oldsize;
	void *newptr;

	if (ptr == NULL) {
		/* reduces to malloc, no copying */
		newptr = __heap_alloc(size, 1);
	} else if (size == 0) {
		/* reduces to free, also no copying */
		__heap_free(ptr);
		newptr = NULL;
	} else {
		/* tries to shrink or grow (always in place) */
		newptr = __heap_realloc(ptr, size, &oldsize);
		if (newptr == NULL) {
			/* simple shrink or grow failed, allocate new memory block */
			newptr = __heap_alloc(size, 1);
			if (newptr != NULL) {
				/* copy if a new block was allocated */
				memcpy(newptr, ptr, oldsize);
				__heap_free(ptr);
			}
		}
	}

	return newptr;
}

void *realloc(void *ptr, size_t size)
{
	void *p;

	p = __realloc_noerrno(ptr, size);
	if ((p == NULL) && (size > 0)) {
		__errno_set(ENOMEM);
	}

	return p;
}
