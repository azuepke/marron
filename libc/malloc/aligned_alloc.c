/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * aligned_alloc.c
 *
 * Malloc implementation.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-11-11: adapted
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/error.h>
#include <stddef.h>


void *aligned_alloc(size_t align, size_t size)
{
	void *p;

	if ((align < sizeof(void *)) || ((align & (align - 1)) != 0)) {
		return NULL;
	}

	p = __heap_alloc(size, align);

	return p;
}
