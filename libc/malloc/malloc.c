/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * malloc.c
 *
 * Malloc implementation.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-11-11: adapted
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/error.h>
#include <stddef.h>


void *__malloc_noerrno(size_t size)
{
	void *p;

	p = __heap_alloc(size, 1);

	return p;
}

void *malloc(size_t size)
{
	void *p;

	p = __malloc_noerrno(size);
	if (p == NULL) {
		__errno_set(ENOMEM);
	}

	return p;
}
