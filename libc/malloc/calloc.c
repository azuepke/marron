/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * calloc.c
 *
 * Malloc implementation.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-11-11: adapted
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/error.h>
#include <stddef.h>
#include <string.h>


static inline size_t __calloc_noerrno_mult(size_t _a, size_t _b)
{
#if __SIZEOF_LONG__ == 8
	/* 64-bit overflow detection */
	__uint128_t a = _a;
	__uint128_t b = _b;
	__uint128_t c = a * b;
#else
	/* 32-bit overflow detection */
	unsigned long long a = _a;
	unsigned long long b = _b;
	unsigned long long c = a * b;
#endif

	if ((c >> 8 * __SIZEOF_LONG__) != 0) {
		return ~0UL;
	}
	return c;
}

void *__calloc_noerrno(size_t num, size_t size)
{
	size_t newsize;
	void *p;

	newsize = __calloc_noerrno_mult(num, size);

	p = __heap_alloc(newsize, 1);
	if (p != NULL) {
		memset(p, 0, newsize);
	}

	return p;
}

void *calloc(size_t num, size_t size)
{
	void *p;

	p = __calloc_noerrno(num, size);
	if (p == NULL) {
		__errno_set(ENOMEM);
	}

	return p;
}
