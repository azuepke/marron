/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * free.c
 *
 * Malloc implementation.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-11-11: adapted
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/error.h>
#include <stddef.h>


void free(void *ptr)
{
	__heap_free(ptr);
}
