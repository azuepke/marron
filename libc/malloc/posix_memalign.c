/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * posix_memalign.c
 *
 * Malloc implementation.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-11-11: adapted
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/error.h>
#include <stddef.h>


int posix_memalign(void **ptr, size_t align, size_t size)
{
	void *p;

	/* always nullify ptr */
	*ptr = NULL;

	if ((align < sizeof(void *)) || ((align & (align - 1)) != 0)) {
		return EINVAL;
	}

	p = __heap_alloc(size, align);

	if (p == NULL) {
		return ENOMEM;
	}

	*ptr = p;
	return EOK;
}
