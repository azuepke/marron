/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2025 Alexander Zuepke */
/*
 * malloc/heap.c
 *
 * Malloc implementation.
 *
 * NOTE: This module implements a simple K&R-style first-fit memory allocator.
 *       These allocators aren't the best allocators, but simple to write.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-11-11: adapted
 * azuepke, 2021-11-15: support alignment
 * azuepke, 2025-03-08: simple heap corruption check
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/error.h>
#include <marron/macros.h>
#include <stddef.h>
#include <marron/arch_defs.h>
#include <marron/mapping.h>
#include <marron/api.h>
#include <marron/compiler.h>

/** malloc block */
struct malloc_block {
	struct malloc_block *next;
	size_t size;
};

#if __LONG_WIDTH__ == 64
#define MAGICN ((struct malloc_block *)0xfeedca75feedca75ul)
#else
#define MAGICN ((struct malloc_block *)0xfeedca75dul)
#endif

#define NEXT_BLOCK(p) ((p) + ((p)->size / sizeof(*(p))))
#define ALIGN_SIZE(s) ALIGN_UP((s), sizeof(struct malloc_block))
#define HEAP_CHECK(p) do { if (unlikely((p)->next != MAGICN)) { sys_abort(); } } while (0)

/** free list */
static struct malloc_block malloc_head;
static struct malloc_block *freep;

/** top of heap */
static addr_t heap_top;


/** internal malloc lock */
static unsigned int heap_lock;

/** lock internal malloc lock */
__attribute__((always_inline))
static inline void __heap_lock(void)
{
	__libc_lwmutex_lock(&heap_lock);
}

/** unlock internal malloc lock */
__attribute__((always_inline))
static inline void __heap_unlock(void)
{
	__libc_lwmutex_unlock(&heap_lock);
}


/** free memory (internal function) */
static void __heap_free_internal(struct malloc_block *thisp);


/** increase core memory in whole pages */
static struct malloc_block *morecore(size_t size)
{
	struct malloc_block *p;
	addr_t map_addr;
	err_t err;

	size = (size + PAGE_SIZE - 1) & -(1ul * PAGE_SIZE);

	/* adjust brk */
	err = sys_vm_map(heap_top, size, PROT_READ|PROT_WRITE,
	                 MAP_PRIVATE|MAP_ANON|MAP_FIXED|MAP_EXCL,
	                 -1, 0, &map_addr);
	if (err != EOK) {
		return NULL;
	}

	p = (struct malloc_block *)heap_top;
	heap_top += size;

	/* feed into free list */
	p->next = MAGICN;
	p->size = size;
	__heap_free_internal(p);
	return freep;
}

/** allocate memory */
void *__heap_alloc(size_t size, size_t align)
{
	struct malloc_block *p, *prevp;
	addr_t start, end;
	size_t needsize;
	void *ptr;

	if (size == 0) {
		return NULL;
	}

	/* align to multiples of struct malloc_block, including the header */
	size = ALIGN_SIZE(size + sizeof(struct malloc_block));

	__heap_lock();

	if (align < ALIGN_SIZE(1)) {
		align = ALIGN_SIZE(1);
	}

	/* iterate the free list */
	ptr = NULL;
	prevp = freep;
	for (p = prevp->next; ; prevp = p, p = p->next) {
		/* consider both size and alignment:
		 * calculate a fictuous start address that is properly aligned
		 * and large enough to hold the request,
		 * then derive the required size for this block.
		 */
		end = (addr_t)(p + 1 + p->size / sizeof(*p));
		start = ALIGN_DOWN(end - size, align);
		needsize = end - start;

		if (p->size >= needsize) {
			/* fit */
			if (p->size == needsize) {
				/* exact fit */
				prevp->next = p->next;
			} else {
				/* shrink block, return last part */
				p->size -= needsize;
				p += p->size / sizeof(*p);
				p->size = needsize;
			}
			freep = prevp;
			p->next = MAGICN;
			ptr = (void *)(p + 1);
			break;
		}

		if (p == freep) {
			/* no entry found, increase core memory and try again */
			p = morecore(size);
			if (p == NULL) {
				break;
			}
		}
	}

	__heap_unlock();

	return ptr;
}

/** free memory (internal function) */
static void __heap_free_internal(struct malloc_block *thisp)
{
	struct malloc_block *p;

	HEAP_CHECK(thisp);

	/* keep list sorted by increasing addresses */
	for (p = freep; !(thisp > p && thisp < p->next); p = p->next) {
		if (p >= p->next && (thisp > p || thisp < p->next)) {
			break;
		}
	}

	if (NEXT_BLOCK(thisp) == p->next) {
		/* merge with next neighbor */
		thisp->size += p->next->size;
		thisp->next = p->next->next;
	} else {
		/* insert */
		thisp->next = p->next;
	}

	if (NEXT_BLOCK(p) == thisp) {
		/* merge with former neighbor */
		p->size += thisp->size;
		p->next = thisp->next;
	} else {
		/* insert */
		p->next = thisp;
	}

	freep = p;
}

/** free memory */
void __heap_free(void *ptr)
{
	struct malloc_block *p;

	if (ptr == NULL) {
		return;
	}

	/* get block header */
	p = (struct malloc_block*)ptr - 1;

	__heap_lock();
	__heap_free_internal(p);
	__heap_unlock();
}

/** internal realloc */
void *__heap_realloc(void *ptr, size_t size, size_t *oldsize)
{
	struct malloc_block *thisp = (struct malloc_block *)ptr - 1;
	struct malloc_block *p;
	size_t size_remaining;
	void *newptr;

	HEAP_CHECK(thisp);

	/* align to multiples of struct malloc_block, including the header */
	size = ALIGN_SIZE(size + sizeof(struct malloc_block));

	__heap_lock();

	/* always return oldsize */
	*oldsize = thisp->size - sizeof(struct malloc_block);

	if (thisp->size == size) {
		/* same size */
		newptr = ptr;
	} else if (thisp->size > size) {
		/* shrink */
		size_remaining = thisp->size - size;
		thisp->size = size;
		p = NEXT_BLOCK(thisp);
		p->size = size_remaining;
		__heap_free_internal(p);
		newptr = ptr;
	} else {
		/* grow */
		/* NOTE: grow not implemented, we always fall back to copying */
		newptr = NULL;
	}

	__heap_unlock();

	return newptr;
}

/** initialize memory allocator (called unlocked during startup) */
void __heap_init(void *base, size_t size)
{
	struct malloc_block *p = base;

	malloc_head.next = &malloc_head;
	malloc_head.size = 0;
	freep = &malloc_head;
	heap_top = (addr_t)base + size;

	/* feed into free list */
	p->next = MAGICN;
	p->size = size;
	__heap_free_internal(p);
}

void __malloc_init(void *base, size_t size) __alias(__heap_init);
