/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * getpagesize.c
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/arch_defs.h>


int getpagesize(void)
{
	return PAGE_SIZE;
}
