/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * mmap.c
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#include "libc_internal.h"
#include <sys/mman.h>
#include <marron/api.h>
#include <marron/arch_defs.h>
#include <marron/macros.h>


void *mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset)
{
	addr_t map_addr;
	err_t err;

	/* the kernel expects that start address and size are aligned,
	 * but POSIX only requires this for the start address.
	 */
	len = ALIGN_UP(len, PAGE_SIZE);

	/* the kernel does not support MAP_STACK */
	flags &= ~MAP_STACK;

	/* non-anonymous mappings are IPC based (send IPC to server) */
	if ((flags & MAP_ANON) == 0) {
		return __libc_mmap(addr, len, prot, flags, fd, offset);
	}

	map_addr = 0;
	err = sys_vm_map((addr_t)addr, len, prot, flags, fd, offset, &map_addr);

	if (err != EOK) {
		__errno_set(err);
		return MAP_FAILED;
	}
	return (void *)map_addr;
}
