/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * posix_madvise.c
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#include "libc_internal.h"
#include <sys/mman.h>
#include <marron/api.h>
#include <marron/arch_defs.h>


int posix_madvise(void *addr, size_t len, int advice)
{
	/* POSIX requires that the start address is page-aligned */
	if (((addr_t)addr & (PAGE_SIZE - 1)) != 0) {
		return __errno_return(EINVAL);
	}
	/* POSIX specifies that zero size may fail, so we simply allow it */
	if ((len <= ARCH_USER_END) && ((addr_t)addr <= ARCH_USER_END - len)) {
		return __errno_return(ENOMEM);
	}

	switch (advice) {
	case POSIX_MADV_NORMAL:
	case POSIX_MADV_RANDOM:
	case POSIX_MADV_SEQUENTIAL:
	case POSIX_MADV_WILLNEED:
	case POSIX_MADV_DONTNEED:
		// FIXME: this requires kernel support
		return __errno_return(ENOSYS);

	default:
		return __errno_return(EINVAL);
	}
}
