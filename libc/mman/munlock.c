/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * munlock.c
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#include "libc_internal.h"
#include <sys/mman.h>
#include <marron/error.h>
#include <marron/arch_defs.h>


int munlock(const void *addr, size_t len)
{
	/* POSIX requires that the start address is page-aligned */
	if (((addr_t)addr & (PAGE_SIZE - 1)) != 0) {
		return __errno_return(EINVAL);
	}
	if ((len <= ARCH_USER_END) && ((addr_t)addr <= ARCH_USER_END - len)) {
		return __errno_return(ENOMEM);
	}

	return EOK;
}
