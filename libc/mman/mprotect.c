/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * mprotect.c
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#include "libc_internal.h"
#include <sys/mman.h>
#include <marron/api.h>
#include <marron/arch_defs.h>
#include <marron/macros.h>


int mprotect(void *addr, size_t len, int prot)
{
	err_t err;

	/* the kernel expects that start address and size are aligned,
	 * but POSIX only requires this for the start address.
	 */
	len = ALIGN_UP(len, PAGE_SIZE);

	err = sys_vm_protect((addr_t)addr, len, prot);

	if (err != EOK) {
		return __errno_return(err);
	}

	return EOK;
}
