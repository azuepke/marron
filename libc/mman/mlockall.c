/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * mlockall.c
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#include "libc_internal.h"
#include <sys/mman.h>
#include <marron/error.h>


int mlockall(int flags)
{
	switch (flags) {
	case MCL_CURRENT:
	case MCL_FUTURE:
		return EOK;

	default:
		return __errno_return(EINVAL);
	}
}
