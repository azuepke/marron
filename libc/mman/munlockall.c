/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * munlockall.c
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#include "libc_internal.h"
#include <sys/mman.h>
#include <marron/error.h>


int munlockall(void)
{
	return EOK;
}
