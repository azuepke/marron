/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * eventfd_read.c
 *
 * POSIX library.
 *
 * azuepke, 2024-12-29: initial
 */

#include "libc_internal.h"
#include <sys/eventfd.h>
#include <unistd.h>


int eventfd_read(int fd, eventfd_t *value)
{
	ssize_t r;

	r = read(fd, value, sizeof(*value));
	if (r != sizeof(*value)) {
		/* errno already set */
		return -1;
	}

	return 0;
}
