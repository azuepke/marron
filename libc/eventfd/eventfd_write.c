/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * eventfd_write.c
 *
 * POSIX library.
 *
 * azuepke, 2024-12-29: initial
 */

#include "libc_internal.h"
#include <sys/eventfd.h>
#include <unistd.h>


int eventfd_write(int fd, eventfd_t value)
{
	ssize_t w;

	w = write(fd, &value, sizeof(value));
	if (w != sizeof(value)) {
		/* errno already set */
		return -1;
	}

	return 0;
}
