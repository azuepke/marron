/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * eventfd.c
 *
 * POSIX library.
 *
 * azuepke, 2024-12-29: initial
 */

#include "libc_internal.h"
#include <sys/eventfd.h>
#include <marron/api.h>


int eventfd(unsigned int initval, int flags)
{
	uint32_t real_flags;
	uint32_t fd;
	err_t err;

	if ((flags & ~(EFD_CLOEXEC | EFD_NONBLOCK | EFD_SEMAPHORE)) != 0) {
		return __errno_return(EINVAL);
	}

	real_flags = 0;
	if ((flags & EFD_CLOEXEC) != 0) {
		real_flags |= SYS_FD_CLOEXEC;
	}
	if ((flags & EFD_NONBLOCK) != 0) {
		real_flags |= SYS_FD_NONBLOCK;
	}
	if ((flags & EFD_SEMAPHORE) != 0) {
		real_flags |= SYS_FD_SPECIAL;
	}

	err = sys_eventfd_create(initval, real_flags, &fd);
	if (err != EOK) {
		return __errno_return(err);
	}

	return fd;
}
