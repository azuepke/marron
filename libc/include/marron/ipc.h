/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * marron/ipc.h
 *
 * POSIX library.
 *
 * azuepke, 2025-01-14: initial
 */

#ifndef MARRON_IPC_H_
#define MARRON_IPC_H_

#ifdef __cplusplus
extern "C" {
#endif

/** attach to IPC server, return file descriptor to IPC server, sets errno */
int __ipc_server_attach(const char *server_name);

#ifdef __cplusplus
}
#endif

#endif
