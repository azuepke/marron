/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * time.h
 *
 * POSIX time handling data types
 *
 * azuepke, 2021-07-26: initial
 */

#ifndef TIME_H_
#define TIME_H_

// may include <signal.h>

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------

#define __NEED_size_t
#define __NEED_pid_t
#define __NEED_mode_t
#define __NEED_time_t
#define __NEED_struct_timespec
#define __NEED_struct_itimerspec
#define __NEED_struct_tm
#define __NEED_locale_t
#define __NEED_clock_t
#define __NEED_clockid_t
#define __NEED_timer_t
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

/* forward declarations */
struct sigevent;

#ifndef NULL
#ifdef __cplusplus
#define NULL 0L
#else
#define NULL ((void*)0)
#endif
#endif

//------------------------------------------------------------------------------

#define CLOCKS_PER_SEC 1000000L

#define TIME_UTC 1

#define CLOCK_REALTIME 0
#define CLOCK_MONOTONIC 1
#define CLOCK_PROCESS_CPUTIME_ID 2
#define CLOCK_THREAD_CPUTIME_ID 3

#define TIMER_ABSTIME 1

int clock_getcpuclockid(pid_t pid, clockid_t *clock_id);
int clock_getres(clockid_t clock_id, struct timespec *tp);
int clock_settime(clockid_t clock_id, const struct timespec *tp);
int clock_gettime(clockid_t clock_id, struct timespec *tp);
int clock_nanosleep(clockid_t clock_id, int flags, const struct timespec *rqtp, struct timespec *rmtp);
int nanosleep(const struct timespec *rqtp, struct timespec *rmtp);

/* get current time (C11) */
int timespec_get(struct timespec *ts, int base);

/* get time resolution (C23) */
int timespec_getres(struct timespec *ts, int base);

/* POSIX timer API */
int timer_create(clockid_t clock_id, struct sigevent *restrict sevp, timer_t *restrict timer_id);
int timer_delete(timer_t timer_id);
int timer_settime(timer_t timer_id, int flags, const struct itimerspec *restrict new_value, struct itimerspec *restrict old_value);
int timer_gettime(timer_t timer_id, struct itimerspec *curr_value);
int timer_getoverrun(timer_t timer_id);

/* get processor time of program */
clock_t clock(void);

/* get current time since origin */
time_t time(time_t *tloc);

//------------------------------------------------------------------------------

// FIXME: not implemented
char *asctime(const struct tm *);
char *asctime_r(const struct tm *restrict, char *restrict);
char *ctime(const time_t *);
char *ctime_r(const time_t *, char *);
double difftime(time_t, time_t);
struct tm *getdate(const char *);
struct tm *gmtime(const time_t *);
struct tm *gmtime_r(const time_t *restrict, struct tm *restrict);
struct tm *localtime(const time_t *);
struct tm *localtime_r(const time_t *restrict, struct tm *restrict);
time_t mktime(struct tm *);
size_t strftime(char *restrict, size_t, const char *restrict, const struct tm *restrict);
size_t strftime_l(char *restrict, size_t, const char *restrict, const struct tm *restrict, locale_t);
char *strptime(const char *restrict, const char *restrict, struct tm *restrict);
time_t time(time_t *);
void tzset(void);

extern int getdate_err;
extern int daylight;
extern long timezone;
extern char *tzname[];

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
