/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * dirent.h
 *
 * Directory entry handling.
 *
 * azuepke, 2022-08-15: initial
 */

#ifndef DIRENT_H_
#define DIRENT_H_

#include <marron/dirent.h>

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_size_t
#define __NEED_ssize_t
#include <bits/alltypes.h>

/* opaque directory stream type */
typedef struct __dirstream DIR;

DIR *opendir(const char *dirname);
DIR *fdopendir(int fd);
int dirfd(DIR *dir);
int closedir(DIR *dir);

struct dirent *readdir(DIR *dir);
/* obsolete */
int readdir_r(DIR *dir, struct dirent *entry, struct dirent **result);

/* broken on 32-bit platforms */
void seekdir(DIR *dir, long loc);
long telldir(DIR *dir);
void rewinddir(DIR *dir);

int alphasort(const struct dirent **d1, const struct dirent **d2);
int scandir(const char *dir, struct dirent ***namelist,
            int (*sel)(const struct dirent *),
            int (*cmp)(const struct dirent **, const struct dirent **));

/* system call to retrieve directory entries (like in BSD and Linux) */
ssize_t getdents(int fd, void *buf, size_t size);

#ifdef __cplusplus
}
#endif

#endif
