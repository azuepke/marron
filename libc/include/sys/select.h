/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sys/select.h
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#ifndef SYS_SELECT_H_
#define SYS_SELECT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_time_t
#define __NEED_suseconds_t
#define __NEED_struct_timeval
#define __NEED_struct_timespec
#define __NEED_sigset_t
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#define FD_SETSIZE	1024

typedef struct {
	unsigned int fd_bits[FD_SETSIZE / (8 * sizeof(unsigned int))];
} fd_set;

#define FD_SET(bit, set)	\
	((set)->fd_bits[(bit) / (8 * sizeof(unsigned int))] |= (1u << ((bit) % (8 * sizeof(unsigned int)))))
#define FD_CLR(bit, set)	\
	((set)->fd_bits[(bit) / (8 * sizeof(unsigned int))] &= ~(1u << ((bit) % (8 * sizeof(unsigned int)))))
#define FD_ISSET(bit, set)	\
	(((set)->fd_bits[(bit) / (8 * sizeof(unsigned int))] & (1u << ((bit) % (8 * sizeof(unsigned int))))) != 0)
#define FD_ZERO(set) do {	\
		unsigned int fd_zero_pos_tmp = 0;	\
		for (fd_zero_pos_tmp = 0;	\
		     fd_zero_pos_tmp < sizeof(fd_set) / sizeof(unsigned int);	\
		     fd_zero_pos_tmp++) {	\
			(set)->fd_bits[fd_zero_pos_tmp] = 0;	\
		}	\
	} while (0)

int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, struct timeval *timeout);
int pselect(int nfds, fd_set *readfds, fd_set *writefds, fd_set *errorfds, const struct timespec *timeout, const sigset_t *sigmask);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
