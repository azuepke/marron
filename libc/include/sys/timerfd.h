/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sys/timerfd.h
 *
 * POSIX library.
 *
 * azuepke, 2022-10-23: initial
 */

#ifndef SYS_TIMERFD_H_
#define SYS_TIMERFD_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_time_t
#define __NEED_struct_timespec
#define __NEED_struct_itimerspec
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#include <marron/fcntl.h>

#define TFD_NONBLOCK	O_NONBLOCK
#define TFD_CLOEXEC		O_CLOEXEC

#define TFD_TIMER_ABSTIME		0x1
#define TFD_TIMER_CANCEL_ON_SET	0x2

int timerfd_create(int clockid, int flags);
int timerfd_settime(int fd, int flags, const struct itimerspec *new_value, struct itimerspec *old_value);
int timerfd_gettime(int fd, struct itimerspec *curr_value);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
