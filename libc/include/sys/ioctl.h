/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sys/ioctl.h
 *
 * POSIX library
 *
 * azuepke, 2022-08-12: initial
 */

#ifndef SYS_IOCTL_H_
#define SYS_IOCTL_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ioctl() command encoding:
 * - bits 31..30: direction
 *    00  no arg (void)
 *    01  write pointer (in)
 *    10  read pointer (out)
 *    11  read-write pointer (inout)
 * - bits 16..29: size
 * - bits 8..15:  ioctl group / type
 * - bits 7..0:   ioctl number
 *
 * NOTE: The meaning of read and write pointers
 *       follows the read() and write() system calls,
 *       i.e. a write pointer is *read* by a kernel,
 *       while a read pointer is *written* by a kernel.
 *       The flags _IOC_IN and _IOC_OUT encode the direction
 *       from a kernel's point of view.
 */
#define _IOC_NONE	0x0u
#define _IOC_WRITE	0x1u
#define _IOC_READ	0x2u

/* encoding */
#define _IOC(dir, grp, nr, sz)	\
	(((dir) << 30) | ((grp) << 8) | ((nr) << 0) | ((sz) << 16))

#define _IO(grp, nr)		_IOC(_IOC_NONE, (grp), (nr), 0)
#define _IOR(grp, nr, sz)	_IOC(_IOC_READ, (grp), (nr), sizeof(sz))
#define _IOW(grp, nr, sz)	_IOC(_IOC_WRITE, (grp), (nr), sizeof(sz))
#define _IOWR(grp, nr, sz)	_IOC(_IOC_READ | _IOC_WRITE, (grp), (nr), sizeof(sz))

/* decoding */
#define _IOC_DIR(val)		(((val) >> 30) & 0x3)
#define _IOC_GRP(val)		(((val) >> 8) & 0xff)
#define _IOC_NR(val)		(((val) >> 0) & 0xff)
#define _IOC_SIZE(val)		(((val) >> 16) & 0x3fff)

#define _IOC_IN				(_IOC_WRITE << 30)
#define _IOC_OUT			(_IOC_READ << 30)


/* ioctl() commands common for all file descriptors */
#define FIOCLEX		_IO('f',  1)
#define FIONCLEX	_IO('f',  2)
#define FIOGETOWN	_IOR('f', 123, int)
#define FIOSETOWN	_IOW('f', 124, int)
#define FIOASYNC	_IOW('f', 125, int)
#define FIONBIO		_IOW('f', 126, int)
#define FIONREAD	_IOR('f', 127, int)	/* mainly for pipes */

/* ioctl() commands for sockets */
#define SIOCATMARK	_IOR('s', 7, int)
#define SIOCSPGRP	_IOW('s', 8, int)	/* same as FIOSETOWN */
#define SIOCGPGRP	_IOR('s', 9, int)	/* same as FIOGETOWN */

/* NOTE: POSIX defines "cmd" as int, but Linux and BSD use unsigned long */
int ioctl(int fd, int cmd, ...);

#ifdef __cplusplus
}
#endif

#endif
