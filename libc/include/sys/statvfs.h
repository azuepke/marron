/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sys/statvfs.h
 *
 * POSIX library
 *
 * azuepke, 2022-08-12: initial
 */

#ifndef SYS_STATVFS_H_
#define SYS_STATVFS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_fsblkcnt_t
#define __NEED_fsfilcnt_t
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#include <marron/stat.h>

/* struct statvfs */
/* NOTE: this structure varies in size depending on the bitsize */
struct statvfs {
	unsigned long f_bsize;
	unsigned long f_frsize;
	fsblkcnt_t f_blocks;
	fsblkcnt_t f_bfree;
	fsblkcnt_t f_bavail;
	fsfilcnt_t f_files;
	fsfilcnt_t f_ffree;
	fsfilcnt_t f_favail;
	unsigned long f_fsid;
	unsigned long f_flag;
	unsigned long f_namemax;
#if __SIZEOF_LONG__ == 4
	unsigned long padding;
#endif
};

#define ST_RDONLY	0x1
#define ST_NOSUID	0x2

int fstatvfs(int fd, struct statvfs *buf);
int statvfs(const char *path, struct statvfs *buf);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
