/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * sys/signalfd.h
 *
 * POSIX library
 *
 * azuepke, 2025-02-14: initial
 */

#ifndef SYS_SIGNALFD_H_
#define SYS_SIGNALFD_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_sigset_t
#include <bits/alltypes.h>
#include <marron/signalfd.h>
#include <marron/fcntl.h>

#define SFD_CLOEXEC		O_CLOEXEC
#define SFD_NONBLOCK	O_NONBLOCK

int signalfd(int fd, const sigset_t *mask, int flags);

#ifdef __cplusplus
}
#endif

#endif
