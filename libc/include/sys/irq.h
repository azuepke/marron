/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * sys/irq.h
 *
 * POSIX library
 *
 * azuepke, 2025-01-06: initial
 */

#ifndef SYS_IRQ_H_
#define SYS_IRQ_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_uint64_t
#include <bits/alltypes.h>
#include <marron/fcntl.h>
#include <marron/irq.h>

#define IRQ_CLOEXEC		O_CLOEXEC
#define IRQ_NONBLOCK	O_NONBLOCK

int irq_attach(unsigned int irq_id, unsigned int irq_mode, int flags);
int irq_ctl(int fd, int op, int flags);

#ifdef __cplusplus
}
#endif

#endif
