/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sys/mman.h
 *
 * POSIX memory management
 *
 * azuepke, 2021-09-15: initial
 */

#ifndef SYS_MMAN_H_
#define SYS_MMAN_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_size_t
#define __NEED_off_t
#define __NEED_mode_t
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#include <marron/mapping.h>


#define MAP_FAILED  ((void *)-1)

/* mapping type and flags (extending marron/mapping.h) */

/** disable replacing of mappings (Linux) */
#define MAP_FIXED_NOREPLACE (MAP_FIXED | MAP_EXCL)

/** disable replacing of mappings (OpenBSD) */
#define MAP_TRYFIXED    (MAP_FIXED | MAP_EXCL)

/** mapping from a file (BSD compat, ignored) */
#define MAP_FILE    0x00

/** allocate anonymous memory (compat name) */
#define MAP_ANONYMOUS   MAP_ANON

/** memory used as thread stack (BSD compat, effectively ignored) */
#define MAP_STACK   0x80

void *mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset);
int munmap(void *addr, size_t len);
int mprotect(void *addr, size_t len, int prot);

#define MS_ASYNC        0x01
#define MS_SYNC         0x02
#define MS_INVALIDATE   0x04

int msync(void *addr, size_t len, int flags);

#define MCL_CURRENT 1
#define MCL_FUTURE  2

int mlock(const void *addr, size_t len);
int munlock(const void *addr, size_t len);
int mlockall(int flags);
int munlockall(void);

#define POSIX_MADV_NORMAL       0
#define POSIX_MADV_RANDOM       1
#define POSIX_MADV_SEQUENTIAL   2
#define POSIX_MADV_WILLNEED     3
#define POSIX_MADV_DONTNEED     4

/** advice about use of memory (effectively ignored, not dynamic paging) */
int posix_madvise(void *addr, size_t len, int advice);


// FIXME: missing
//int shm_open(const char *name, int oflag, mode_t mode);
//int shm_unlink(const char *name);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
