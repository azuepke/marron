/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sys/uio.h
 *
 * POSIX library
 *
 * azuepke, 2022-08-12: initial
 */

#ifndef SYS_UIO_H_
#define SYS_UIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_size_t
#define __NEED_ssize_t
#define __NEED_off_t
#define __NEED_struct_iovec
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

ssize_t readv(int fd, const struct iovec *iov, int iovcnt);
ssize_t writev(int fd, const struct iovec *iov, int iovcnt);

/* extension */
ssize_t preadv(int fd, const struct iovec *iov, int iovcnt, off_t offset);
ssize_t pwritev(int fd, const struct iovec *iov, int iovcnt, off_t offset);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
