/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sys/socket.h
 *
 * Socket interface.
 *
 * azuepke, 2022-08-15: initial
 */

#ifndef SYS_SOCKET_H_
#define SYS_SOCKET_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_size_t
#define __NEED_ssize_t
#define __NEED_struct_iovec
#include <bits/alltypes.h>

typedef unsigned socklen_t;

#ifndef __DEFINED_sa_family_t
#define __DEFINED_sa_family_t
typedef unsigned short sa_family_t;
#endif

struct sockaddr {
	sa_family_t sa_family;	/**< address family */
	char sa_data[14];		/**< actually variable length */
};

struct sockaddr_storage {
	sa_family_t ss_family;
	char __ss_padding[128 - sizeof(sa_family_t) - sizeof(unsigned long)];
	unsigned long __ss_align;
};

struct msghdr {
	void *msg_name;
	socklen_t msg_namelen;
	struct iovec *msg_iov;
	int msg_iovlen;
	void *msg_control;
	socklen_t msg_controllen;
	int msg_flags;
};

/* control message header */
/* NOTE: enforce alignment of 8 bytes for struct timeval in payload */
struct cmsghdr {
	socklen_t cmsg_len;		/* data byte count, including header */
	int cmsg_level;			/* protocol level */
	int cmsg_type;			/* protocol-specific type */
	int __cmsg_padding;		/* padding */
	/* the payload follows immediately after the header */
} __attribute__((__aligned__(8)));

/* socket control message types */
#define SCM_RIGHTS		0x01	/* file descriptors (array of int) */
#define SCM_TIMESTAMP	0x02	/* timestamp (struct timeval) */

#define CMSG_ALIGN(n)	\
	(((n) + sizeof(long long) - 1) & ~(sizeof(long long) - 1))
#define CMSG_SPACE(n)	\
	(sizeof(struct cmsghdr) + CMSG_ALIGN(n))
#define CMSG_LEN(n)	\
	(sizeof(struct cmsghdr) + (n))

#define CMSG_DATA(cmsg)	\
	(((unsigned char *)(cmsg)) + sizeof(struct cmsghdr))
#define __CMSG_NEXT(cmsg)	\
	(CMSG_DATA(cmsg) + CMSG_ALIGN((cmsg)->cmsg_len))
#define CMSG_FIRSTHDR(mhdr)	\
	((struct cmsghdr *)(((mhdr)->msg_controllen > 0) ? (mhdr)->msg_control : 0))
#define __CMSG_END(mhdr)	\
	(((unsigned char *)((mhdr)->msg_control)) + (mhdr)->msg_controllen)
#define CMSG_NXTHDR(mhdr, cmsg)	\
	((struct cmsghdr *)((__CMSG_NEXT(cmsg) < __CMSG_END(mhdr)) ? __CMSG_NEXT(cmsg) : 0))

/* socket option parameter types */
struct linger {
	int l_onoff;
	int l_linger;
};

/* socket types */
#define SOCK_STREAM		1
#define SOCK_DGRAM		2
#define SOCK_RAW		3
#define SOCK_RDM		4
#define SOCK_SEQPACKET	5

/* socket type flags */
#define SOCK_CLOEXEC	0x10000
#define SOCK_NONBLOCK	0x20000

/* socket option level */
#define SOL_SOCKET		0xffff

/* socket options */
/* NOTE: the following options can be used as flags */
#define SO_DEBUG		0x0001	/* POSIX; int */
#define SO_ACCEPTCONN	0x0002	/* POSIX; int; get only */
#define SO_REUSEADDR	0x0004	/* POSIX; int */
#define SO_KEEPALIVE	0x0008	/* POSIX; int */
#define SO_DONTROUTE	0x0010	/* POSIX; int */
#define SO_BROADCAST	0x0020	/* POSIX; int; SOCK_DGRAM */
#define SO_USELOOPBACK	0x0040	/* BSD; int */
#define SO_LINGER		0x0080	/* POSIX; struct linger */
#define SO_OOBINLINE	0x0100	/* POSIX; int */
#define SO_REUSEPORT	0x0200	/* BSD; int */
#define SO_TIMESTAMP	0x0400	/* BSD; int; SOCK_DGRAM */
#define SO_NOSIGPIPE	0x0800	/* BSD; int */

/* NOTE: the following options cannot be used as flags */
#define SO_SNDBUF		0x1001	/* POSIX; int */
#define SO_RCVBUF		0x1002	/* POSIX; int */
#define SO_SNDLOWAT		0x1003	/* POSIX; int */
#define SO_RCVLOWAT		0x1004	/* POSIX; int */
#define SO_SNDTIMEO		0x1005	/* POSIX; struct timeval */
#define SO_RCVTIMEO		0x1006	/* POSIX; struct timeval */
#define SO_ERROR		0x1007	/* POSIX; int; get only */
#define SO_TYPE			0x1008	/* POSIX; int; get only */
#define SO_DOMAIN		0x1009	/* BSD; int; get only */
#define SO_PROTOCOL		0x100a	/* BSD; int; get only */

/* maximum backlog queue length */
#define SOMAXCONN		128

/* message flags */
#define MSG_OOB			0x0001	/* process out-of-band data */
#define MSG_PEEK		0x0002	/* recv: peek at incoming message */
#define MSG_DONTROUTE	0x0004	/* send: ignored */
#define MSG_EOR			0x0008	/* end of record (SOCK_SEQPACKET only) */
#define MSG_TRUNC		0x0010	/* status: data truncated */
#define MSG_CTRUNC		0x0020	/* status: control data truncated */
#define MSG_WAITALL		0x0040	/* recv: wait for all data */
#define MSG_DONTWAIT	0x0080	/* extension: non-blocking */
#define MSG_NOSIGNAL	0x0400	/* send: don't send SIGPIPE */
#define MSG_CMSG_CLOEXEC	0x0800	/* extension: recv: O_CLOEXEC on received fds */

/* address families */
#define AF_UNSPEC		0
#define AF_UNIX			1
#define AF_LOCAL		AF_UNIX
#define AF_INET			2
#define AF_INET6		10

/* shutdown modes */
#define SHUT_RD			0
#define SHUT_WR			1
#define SHUT_RDWR		2

/* API */
int socket(int domain, int type, int protocol);
int socketpair(int domain, int type, int protocol, int sfds[2]);
int connect(int sfd, const struct sockaddr *addr, socklen_t addrlen);
int getpeername(int sfd, struct sockaddr *addr, socklen_t *addrlen);
int bind(int sfd, const struct sockaddr *addr, socklen_t addrlen);
int getsockname(int sfd, struct sockaddr *addr, socklen_t *addrlen);
int listen(int sfd, int backlog);
int accept(int sfd, struct sockaddr *addr, socklen_t *addrlen);
int accept4(int sfd, struct sockaddr *addr, socklen_t *addrlen, int flags); /* BSD, Linux extension */
int shutdown(int sfd, int how);

ssize_t send(int sfd, const void *buf, size_t size, int flags);
ssize_t sendto(int sfd, const void *buf, size_t size, int flags, const struct sockaddr *addr, socklen_t addrlen);
ssize_t sendmsg(int sfd, const struct msghdr *msg, int flags);
ssize_t recv(int sfd, void *buf, size_t size, int flags);
ssize_t recvfrom(int sfd, void *buf, size_t size, int flags, struct sockaddr *addr, socklen_t *addrlen);
ssize_t recvmsg(int sfd, struct msghdr *msg, int flags);

int getsockopt(int sfd, int level, int optname, void *optval, socklen_t *optlen);
int setsockopt(int sfd, int level, int optname, const void *optval, socklen_t optlen);
int sockatmark(int sfd);

#ifdef __cplusplus
}
#endif

#endif
