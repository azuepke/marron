/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * sys/stat.h
 *
 * POSIX library
 *
 * azuepke, 2022-08-12: initial
 */

#ifndef SYS_STAT_H_
#define SYS_STAT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_dev_t
#define __NEED_ino_t
#define __NEED_mode_t
#define __NEED_nlink_t
#define __NEED_uid_t
#define __NEED_gid_t
#define __NEED_off_t
#define __NEED_blksize_t
#define __NEED_blkcnt_t
#define __NEED_time_t
#define __NEED_struct_timespec
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#include <marron/stat.h>

/* struct stat */
/* NOTE: this structure always has a fixed size of 128 bytes */
struct stat {
	dev_t st_dev;
	ino_t st_ino;
	mode_t st_mode;
	nlink_t st_nlink;
	uid_t st_uid;
	gid_t st_gid;
	dev_t st_rdev;
	off_t st_size;
	blksize_t st_blksize;
#if __SIZEOF_LONG__ == 4
	unsigned long padding;
#endif
	blkcnt_t st_blocks;
	struct timespec st_atim;
	struct timespec st_mtim;
	struct timespec st_ctim;
	unsigned int unused[4];
};

/* compatibility accessors for struct stat */
#define st_atime st_atim.tv_sec
#define st_mtime st_mtim.tv_sec
#define st_ctime st_ctim.tv_sec

/* mode check */
#define S_ISFIFO(m)	(((m) & S_IFMT) == S_IFIFO)
#define S_ISCHR(m)	(((m) & S_IFMT) == S_IFCHR)
#define S_ISDIR(m)	(((m) & S_IFMT) == S_IFDIR)
#define S_ISBLK(m)	(((m) & S_IFMT) == S_IFBLK)
#define S_ISREG(m)	(((m) & S_IFMT) == S_IFREG)
#define S_ISLNK(m)	(((m) & S_IFMT) == S_IFLNK)
#define S_ISSOCK(m)	(((m) & S_IFMT) == S_IFSOCK)

/* type check (on struct stat) */
#define S_TYPEISMQ(buf)		0
#define S_TYPEISSEM(buf)	0
#define S_TYPEISSHM(buf)	0
#define S_TYPEISTMO(buf)	0

int fstat(int fd, struct stat *buf);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
