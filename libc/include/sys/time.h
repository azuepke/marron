/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * sys/time.h
 *
 * POSIX time handling data types
 *
 * azuepke, 2021-07-26: initial
 */

#ifndef SYS_TIME_H_
#define SYS_TIME_H_

// may include <sys/select.h>

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_time_t
#define __NEED_suseconds_t
#define __NEED_struct_timespec
#define __NEED_struct_timeval
#define __NEED_struct_itimerval
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#define ITIMER_REAL 0
#define ITIMER_VIRTUAL 1
#define ITIMER_PROF 2

/** set interval timer (only ITIMER_REAL supported) */
int setitimer(int which, const struct itimerval *restrict new_value, struct itimerval *restrict old_value);

/** get interval timer (only ITIMER_REAL supported) */
int getitimer(int which, struct itimerval *curr_value);

/** get time of day (obsolete, tz is not used) */
int gettimeofday(struct timeval *restrict tv, void *restrict tz);

//------------------------------------------------------------------------------

// FIXME: not implemented

/** change modification time of file */
int utimes(const char *filename, const struct timeval times[2]);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
