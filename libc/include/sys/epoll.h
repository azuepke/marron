/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll.h
 *
 * POSIX library.
 *
 * azuepke, 2024-12-08: initial
 */

#ifndef EPOLL_H_
#define EPOLL_H_

#include <marron/epoll.h>
#include <marron/fcntl.h>

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_uint32_t
#define __NEED_uint64_t
#define __NEED_sigset_t
#define __NEED_time_t
#define __NEED_struct_timespec
#include <bits/alltypes.h>

/* Flags for epoll_create1.  */
#define EPOLL_CLOEXEC O_CLOEXEC

/* user-defined event data */
typedef union epoll_data {
	void *ptr;
	int fd;
	uint32_t u32;
	uint64_t u64;
} epoll_data_t;

/* event structure */
struct epoll_event {
	uint32_t events;
	epoll_data_t data;
};

int epoll_create(int size);
int epoll_create1(int flags);
int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout);
int epoll_pwait(int epfd, struct epoll_event *events, int maxevents, int timeout, const sigset_t *sigmask);
int epoll_pwait2(int epfd, struct epoll_event *events, int maxevents, const struct timespec *timeout, const sigset_t *sigmask);

#ifdef __cplusplus
}
#endif

#endif
