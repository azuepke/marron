/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * sys/eventfd.h
 *
 * POSIX library
 *
 * azuepke, 2024-12-29: initial
 */

#ifndef SYS_EVENTFD_H_
#define SYS_EVENTFD_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_uint64_t
#include <bits/alltypes.h>
#include <marron/fcntl.h>

typedef uint64_t eventfd_t;

#define EFD_CLOEXEC		O_CLOEXEC
#define EFD_NONBLOCK	O_NONBLOCK
#define EFD_SEMAPHORE	0x0001

int eventfd(unsigned int initval, int flags);
int eventfd_read(int fd, eventfd_t *value);
int eventfd_write(int fd, eventfd_t value);

#ifdef __cplusplus
}
#endif

#endif
