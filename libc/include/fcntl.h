/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * fcntl.h
 *
 * POSIX-conforming file flags and modes.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2025-01-16: split special open functions from open.c and export
 */

#ifndef FCNTL_H_
#define FCNTL_H_

#include <marron/fcntl.h>
#include <marron/stat.h>

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_pid_t
#define __NEED_off_t
#define __NEED_mode_t
#include <bits/alltypes.h>

/* fcntl() commands */
#define F_DUPFD		0		/**< dup functionality */
#define F_DUPFD_CLOEXEC	1	/**< dup + FD_CLOEXEC */
#define F_GETFD		2		/**< get file descriptor flags */
#define F_SETFD		3		/**< set file descriptor flags */
#define F_GETFL		4		/**< get file status flags and file access mode */
#define F_SETFL		5		/**< set file status flags and file access mode */
#define F_GETLK		6		/**< get file lock */
#define F_SETLK		7		/**< set file lock */
#define F_SETLKW	8		/**< set file lock and wait */
#define F_GETOWN	9		/**< get process ID for SIGURG */
#define F_SETOWN	10		/**< set process ID for SIGURG */

/* flags for fcntl() */
#define FD_CLOEXEC	0x1		/**< close on exec */

/* flags for file locking */
#define F_RDLCK		0		/**< shared or read lock */
#define F_WRLCK		1		/**< exclusive or write lock */
#define F_UNLCK		2		/**< unlock */

#define POSIX_FADV_NORMAL		0
#define POSIX_FADV_RANDOM		1
#define POSIX_FADV_SEQUENTIAL	2
#define POSIX_FADV_WILLNEED		3
#define POSIX_FADV_DONTNEED		4
#define POSIX_FADV_NOREUSE		5

/* xxxat() fd */
#define AT_FDCWD	(-100)	/**< placeholder fd for current directory */

/* for lockf(), F_SETLKW, F_GETOWN, F_SETOWN and lockf() */
/* NOTE: unsupported due to broken semantics on close */
struct flock {
	short l_type;
	short l_whence;
	pid_t l_pid;
	off_t l_start;
	off_t l_len;
};

/* create new file -- wrapper to open */
int fcntl(int fd, int cmd, ...);
int creat(const char *path, mode_t mode);
int open(const char *path, int flags, ...);
int posix_fadvise(int fd, off_t offset, off_t len, int advice);
int posix_fallocate(int fd, off_t offset, off_t len);

/* special open functions */
int __open_ipc(const char *path, int flags, mode_t mode);
int __open_memrq(const char *name, int memrq_type, int flags);
int __open_irq(const char *name, int flags);

#ifdef __cplusplus
}
#endif

#endif
