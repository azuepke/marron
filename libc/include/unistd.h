/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * unistd.h
 *
 * POSIX library
 *
 * azuepke, 2022-08-12: initial (only subset)
 */

#ifndef UNISTD_H_
#define UNISTD_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_size_t
#define __NEED_ssize_t
#define __NEED_uid_t
#define __NEED_gid_t
#define __NEED_off_t
#define __NEED_pid_t
#define __NEED_intptr_t
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#ifndef NULL
#ifdef __cplusplus
#define NULL 0L
#else
#define NULL ((void*)0)
#endif
#endif

/* flags for access(), encoding follows rwx assignment */
#define F_OK 0x0
#define R_OK 0x4
#define W_OK 0x2
#define X_OK 0x1

/* whence mode for lseek() (also defined in stdio.h and fcntl.h) */
#ifndef SEEK_SET
#define SEEK_SET	0		/**< seek from start of file */
#endif
#ifndef SEEK_CUR
#define SEEK_CUR	1		/**< seek from current position */
#endif
#ifndef SEEK_END
#define SEEK_END	2		/**< seek from end of file */
#endif

/* flags for lockf() */
#define F_LOCK	0
#define F_TEST	1
#define F_TLOCK	2
#define F_ULOCK	3

/* standard file streams */
#define STDIN_FILENO	0
#define STDOUT_FILENO	1
#define STDERR_FILENO	2

void _exit(int status) __attribute__((__noreturn__));

int dup(int oldfd);
int dup2(int oldfd, int newfd);
/* extension */
int dup3(int oldfd, int newfd, int flags);
int close(int fd);

ssize_t read(int fd, void *buf, size_t size);
ssize_t pread(int fd, void *buf, size_t size, off_t offset);
ssize_t write(int fd, const void *buf, size_t size);
ssize_t pwrite(int fd, const void *buf, size_t size, off_t offset);

off_t lseek(int fd, off_t offset, int whence);
int ftruncate(int fd, off_t len);
int fsync(int fd);
int fdatasync(int fd);
void sync(void);	/* not implemented */

int lockf(int fd, int cmd, off_t len);	/* not supported */

int pipe(int fds[2]);
int pipe2(int fds[2], int flags);

/** get page size (legacy) */
int getpagesize(void);

/* optarg */
//extern char *optarg;
//extern int opterr;
//extern int optind;
//extern int optopt;

char *getcwd(char *buf, size_t size);
int chdir(const char *path);
int fchdir(int fd);

/** internal IPC call to retrieve pathname from file descriptor */
ssize_t __getpath(int fd, void *pathname, size_t size);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
