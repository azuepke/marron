/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * stdlib.h
 *
 * Subset of standard library
 *
 * azuepke, 2021-07-29: initial
 */

#ifndef STDLIB_H_
#define STDLIB_H_

// may #include <stddef.h>
// may #include <limits.h>
// may #include <math.h>
// may #include <sys/wait.h>

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------

#define __NEED_size_t
#include <bits/alltypes.h>

#ifndef NULL
#ifdef __cplusplus
#define NULL 0L
#else
#define NULL ((void*)0)
#endif
#endif

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

void abort(void);
void _Exit(int status) __attribute__((__noreturn__));
void exit(int status) __attribute__((__noreturn__));
//int atexit(void (*func)(void));

/* from C11 */
//void quick_exit(int status) __attribute__((__noreturn__));
//int at_quick_exit(void (*func)(void));

/*
 * malloc(), calloc(), and realloc() set errno to ENOMEM
 * posix_memalign() returns EOK, EINVAL, ENOMEM
 * aligned_alloc() does not set errno
 */
void *malloc(size_t size);
void *calloc(size_t num, size_t size);
void *realloc(void *ptr, size_t size);
void free(void *ptr);
int posix_memalign(void **ptr, size_t align, size_t size);

/* from C11 */
void *aligned_alloc(size_t align, size_t size);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
