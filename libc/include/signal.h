/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * signal.h
 *
 * POSIX signal handling data types and API
 *
 * azuepke, 2021-05-18: initial, following the POSIX 2017 standard
 */

#ifndef SIGNAL_H_
#define SIGNAL_H_

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------

#define __NEED_size_t
#define __NEED_pthread_t
#define __NEED_pid_t
#define __NEED_uid_t
#define __NEED_pthread_attr_t
#define __NEED_time_t
#define __NEED_suseconds_t
#define __NEED_struct_timespec
#define __NEED_struct_timeval
#define __NEED_sigval_t
#define __NEED_sigset_t
#include <bits/alltypes.h>

//------------------------------------------------------------------------------

#include <marron/signal.h>


#define SIG_ERR		((void(*)(int))-1)
#define SIG_DFL		((void(*)(int))0)
#define SIG_IGN		((void(*)(int))1)
#define SIG_HOLD	((void(*)(int))2)

/* access to integers is atomic */
typedef int sig_atomic_t;

struct sigevent {
	int sigev_notify;
	int sigev_signo;
	union sigval sigev_value;
	union {
		int __tid;
		struct {
			void (*__func)(union sigval);
			pthread_attr_t *__attr;
		} __thr;
	} __sigev;
};
#define sigev_notify_function __sigev.__thr.__func
#define sigev_notify_attributes __sigev.__thr.__attr
#define sigev_notify_thread_id __sigev.__tid

#define SIGEV_NONE 0
#define SIGEV_SIGNAL 1 /* process signal */
#define SIGEV_THREAD 2 /* create thread; not supported */
#define SIGEV_THREAD_ID 3 /* extension: thread signal to specific thread */


typedef struct {
	void *ss_sp;
	size_t ss_size;
	int ss_flags;
#if __SIZEOF_LONG__ == 8
	int __ss_padding;
#endif
} stack_t;

#define SS_ONSTACK 1
#define SS_DISABLE 2

#define MINSIGSTKSZ 4096
#define SIGSTKSZ 8192

/* points to struct regs */
typedef struct __mcontext {
	/* POSIX actually expects a register frame here,
	 * but we provide a pointer to the saved register frame.
	 */
	void *regs;
} mcontext_t;

typedef struct __ucontext ucontext_t;
struct __ucontext {
	mcontext_t uc_mcontext;
	ucontext_t *uc_link;	/* not supported, always NULL */
	sigset_t uc_sigmask;
	stack_t uc_stack;
};

typedef struct __siginfo {
	int si_signo;
	int si_code;
	int si_errno;	/* not supported */
	int __si_padding;
	union {
#if __SIZEOF_LONG__ == 4
		long __si_padding[4];
#elif __SIZEOF_LONG__ == 8
		long __si_padding[2];
#endif

		/* the kernel's data types */
		struct {
			unsigned int status;
			unsigned int ex_type;
			unsigned long addr;
		} __si_kern;

		/* kill, sigqueue, etc */
		struct {
			pid_t __si_pid;
			uid_t __si_uid;
			union sigval __si_value;
		} __si_sig;

		/* POSIX timers */
		struct {
			int __si_overrun;
			int __si_timerid;
			union sigval __si_value;
		} __si_timer;

		/* exceptions (SIGILL, SIGFPE, SIGSEGV, SIGBUS) */
		struct {
			int __si_trapno;
			int __si_fault_padding;
			void *__si_addr;
		} __si_fault;

		/* SIGCHILD; not supported */
		struct {
			pid_t __si_pid;
			uid_t __si_uid;
			int __si_status;
		} __si_child;

		/* SIGPOLL; not supported */
		struct {
			int __si_fd;
			int __si_poll_padding;
			long __si_band;
		} __si_poll;
	} __si;
} siginfo_t;

#define si_pid __si.__si_sig.__si_pid
#define si_uid __si.__si_sig.__si_uid
#define si_value __si.__si_sig.__si_value
#define si_int __si.__si_sig.__si_value.sival_int
#define si_ptr __si.__si_sig.__si_value.sival_ptr
#define si_timerid __si.__si_timer.__si_timerid
#define si_overrun __si.__si_timer.__si_overrun
#define si_trapno __si.__si_fault.__si_trapno
#define si_addr __si.__si_fault.__si_addr
#define si_status __si.__si_child.__si_status
#define si_fd __si.__si_poll.__si_fd
#define si_band __si.__si_poll.__si_band

struct sigaction {
	union {
		void (*__sa_handler)(int);
		void (*__sa_sigaction)(int, siginfo_t *, void *);
	} __sa;
	int sa_flags;
#if __SIZEOF_LONG__ == 8
	int __sa_padding;
#endif
	sigset_t sa_mask;
};
#define sa_handler __sa.__sa_handler
#define sa_sigaction __sa.__sa_sigaction

#define SA_SIGINFO		0x01	/* queue signal and provide struct siginfo */
#define SA_ONSTACK		0x02	/* signal delivery on alternate stack */
#define SA_NODEFER		0x04	/* signal not blocked in signal handler */
#define SA_NOMASK		SA_NODEFER	/* Linux alias name */
#define SA_RESETHAND	0x08	/* reset handler to SIG_DFL on delivery */
#define SA_ONESHOT		SA_RESETHAND	/* Linux alias name */

#define SA_RESTART		0x10	/* restartable system calls, not supported */
#define SA_NOCLDSTOP	0x20	/* no SIGCHLD if children stop, not supported */
#define SA_NOCLDWAIT	0x40	/* no SIGCHLD for zombies, not supported */

#define SIG_BLOCK 0
#define SIG_UNBLOCK 1
#define SIG_SETMASK 2

//------------------------------------------------------------------------------

/** print signal message */
// FIXME: not implemented -> belongs to stdio
void psiginfo(const siginfo_t *si, const char *s);

/** print signal message */
// FIXME: not implemented -> belongs to stdio
void psignal(int sig, const char *s);

/** send signal to caller (ISO C compat API) */
int raise(int sig);

/** send signal to process (POSIX, only pid == 0 supported) */
int kill(pid_t pid, int sig);

/** send signal to process group (POSIX, only pgrp == 0 supported) */
int killpg(pid_t pgrp, int sig);

/** send signal to thread (POSIX) */
int pthread_kill(pthread_t thread, int sig);

/** queue signal to process (POSIX, only pid == 0 supported) */
int sigqueue(pid_t pid, int sig, const union sigval value);

/** queue signal to thread (Linux extension) */
int pthread_sigqueue_np(pthread_t thread, int sig, const union sigval value);

/** change blocked signals (POSIX) */
int sigprocmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset);

/** change current threads signal mask */
int pthread_sigmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset);


/** mask signal (System V compat API) */
int sighold(int sig);

/** unmask signal (System V compat API) */
int sigrelse(int sig);

/** ignore signal (System V compat API) */
int sigignore(int sig);

/** wait for signal (System V compat API) */
int sigpause(int sig);

/** register signal handler (System V compat API) */
void (*sigset(int sig, void (*disp)(int)))(int);

/** register signal handler (ISO C compat API) */
void (*signal(int sig, void (*func)(int)))(int);

/** control interruptible system calls (BSD compat API) */
int siginterrupt(int sig, int flag);

/* clear signal set (POSIX) */
int sigemptyset(sigset_t *set);

/* fill signal set (POSIX) */
int sigfillset(sigset_t *set);

/* add signal to set (POSIX) */
int sigaddset(sigset_t *set, int sig);

/* remove signal from set (POSIX) */
int sigdelset(sigset_t *set, int sig);

/* test if signal is a member of set (POSIX) */
int sigismember(const sigset_t *set, int sig);

/** register signal handler (POSIX) */
int sigaction(int sig, const struct sigaction *restrict sa, struct sigaction *restrict old_sa);

/** change signal stack (POSIX) */
int sigaltstack(const stack_t *restrict ss, stack_t *restrict old_ss);

/** retrieve pending signals (POSIX) */
int sigpending(sigset_t *set);

/** wait for signal (POSIX) */
int sigsuspend(const sigset_t *set);

/** wait for signal (POSIX) */
int sigwait(const sigset_t *restrict set, int *restrict sig);

/** wait for signal (POSIX) */
int sigwaitinfo(const sigset_t *restrict set, siginfo_t *restrict si);

/** wait for signal (POSIX) */
int sigtimedwait(const sigset_t *restrict set, siginfo_t *restrict si, const struct timespec *restrict reltime);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

#endif
