/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * poll.h
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#ifndef POLL_H_
#define POLL_H_

#include <marron/poll.h>

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_time_t
#define __NEED_struct_timespec
#define __NEED_sigset_t
#include <bits/alltypes.h>

/* for poll() */
struct pollfd {
	int fd;
	short events;
	short revents;
};

typedef unsigned long nfds_t;

int poll(struct pollfd fds[], nfds_t nfds, int timeout);
int ppoll(struct pollfd fds[], nfds_t nfds, const struct timespec *timeout, const sigset_t *sigmask);

#ifdef __cplusplus
}
#endif

#endif
