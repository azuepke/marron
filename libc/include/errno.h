/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * errno.h
 *
 * Multithreaded implementation of errno.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-07-19: libthr -> libposix rework
 */

#ifndef ERRNO_H_
#define ERRNO_H_

#include <marron/errno.h>

#ifdef __cplusplus
extern "C" {
#endif

extern int *__errno_location(void) __attribute__((__const__)) __attribute__((__returns_nonnull__));
#define errno (*__errno_location())

#ifdef __cplusplus
}
#endif

#endif
