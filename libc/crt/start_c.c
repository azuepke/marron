/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022 Alexander Zuepke */
/*
 * start_c.c
 *
 * Startup code calling main (C level, for all architectures)
 *
 * azuepke, 2021-11-15: initial
 * azuepke, 2022-08-11: split into start.c and start_c.c
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/compiler.h>

/* forward declarations */
static void call_main(void);

void _start_c(void *heap_base, size_t heap_size, void *stack_base, size_t stack_size, void *mmap_base)
{
	(void)stack_base;
	(void)stack_size;
	(void)mmap_base;

	__heap_init(heap_base, heap_size);

	__main_init();

	call_main();
}

static void call_main(void)
{
	char **argv = __main_argv;
	char **envp = __main_envp;
	int argc;
	int ret;

	for (argc = 0; argv[argc] != NULL; argc++) {
		;
	}

	ret = main(argc, argv, envp);
	exit(ret);
}
