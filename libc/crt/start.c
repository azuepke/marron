/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022 Alexander Zuepke */
/*
 * start.c
 *
 * Startup code calling main (asm level, for all architectures)
 *
 * azuepke, 2021-11-15: initial
 * azuepke, 2022-08-11: split into start.c and start_c.c
 */

#include "libc_internal.h"

/* NOTE: Bare-metal applications can skip any libc initialization
 * by providing their own _start_c function.
 * Dependencies are satisfied as follows:
 * - the linker script refers to _start, this pulls in start.c
 * - an application that provides _start_c has priority over
 *   our _start_c from _start_c.c
 */

/* the initial arguments to _start are start and size of heap and stack
 * and the start address hint for mappings
 */
__asm__(
	"	.text\n"
	"	.global _start\n"
	"	.type _start,%function\n"
	"_start:\n"
#ifdef __aarch64__
	"	b	_start_c\n"
#elif defined __arm__
	"	push	{r4, r5, r6, r7}\n"
	"	b	_start_c\n"
#else
	"	j	_start_c\n"
#endif
);
