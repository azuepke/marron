/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022, 2025 Alexander Zuepke */
/*
 * heap_init.c
 *
 * Dummy heap init
 *
 * azuepke, 2025-01-26: split from start_c.c
 */

#include "libc_internal.h"
#include <marron/compiler.h>

/* dummy __heap_init() in case we do not use malloc */
static void __heap_init_dummy(void *base __unused, size_t size __unused)
{
}
extern void __heap_init(void *base, size_t size) __weakref(__heap_init_dummy);
