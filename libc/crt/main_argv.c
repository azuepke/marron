/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * main_argv.c
 *
 * Default process arguments.
 *
 * azuepke, 2025-01-26: initial
 */

#include <marron/compiler.h>
#include "libc_internal.h"


static const char * const __main_argv_default[] = { "a.out", NULL };
extern char *__main_argv[countof(__main_argv_default)] __weakref(__main_argv_default);
