/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * main_init.c
 *
 * Main thread TLS initialization.
 *
 * azuepke, 2025-01-26: initial
 */

#include <marron/api.h>
#include <marron/types.h>
#include <marron/compiler.h>
#include "libc_internal.h"


/** TLS of main thread (kept in .bss) */
static struct sys_tls __main_tls;

/** __main_init() for single-threaded applications */
static void __main_init_single_threaded(void)
{
	/* the kernel initializes the .sys part of the thread descriptor */
	sys_tls_set(&__main_tls);

	/* enable all signals */
	sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);
}
extern void __main_init(void) __weakref(__main_init_single_threaded);
