/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * main_envp.c
 *
 * Default process environment.
 *
 * azuepke, 2025-01-26: initial
 */

#include <marron/compiler.h>
#include "libc_internal.h"


static const char * const __main_envp_default[] = { NULL };
extern char *__main_envp[countof(__main_envp_default)] __weakref(__main_envp_default);
