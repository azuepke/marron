/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * gettimeofday.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <sys/time.h>
#include <marron/api.h>


/** get time of day (obsolete, tz is not used) */
int gettimeofday(struct timeval *restrict tv, void *restrict tz)
{
	sys_time_t now;

	(void)tz;

	if (tv != NULL) {
		sys_clock_get(CLK_ID_REALTIME, &now);
		*tv = __libc_ns2tv(now);
	}
	return 0;
}
