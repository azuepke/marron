/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * clock_getcpuclockid.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


int clock_getcpuclockid(pid_t pid, clockid_t *clock_id)
{
	(void)pid;
	(void)clock_id;
	return ENOSYS;
}
