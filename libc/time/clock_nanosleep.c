/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * clock_nanosleep.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


int clock_nanosleep(clockid_t clock_id, int flags, const struct timespec *rqtp, struct timespec *rmtp)
{
	sys_time_t start, elapsed, now, leftover;
	sys_timeout_t timeout;
	unsigned int clk_id;
	err_t err;

	/* silence compiler warning on uninitialized variables
	 * when compiling without optimizations
	 */
	start = 0;

	/* covers: CLOCK_REALTIME, CLOCK_MONOTONIC */
	if ((unsigned int)clock_id > 1U) {
		return __errno_return(EINVAL);
	}
	clk_id = clock_id;

	if (!TS_VALID(*rqtp)) {
		return __errno_return(EINVAL);
	}

	timeout = __libc_tsopt2ns(rqtp);
	if ((flags & TIMER_ABSTIME) != 0) {
		clk_id |= TIMEOUT_ABSOLUTE;
		rmtp = NULL;
	} else {
		if (rmtp != NULL) {
			sys_clock_get(CLK_ID_MONOTONIC, &start);
		}
	}

	err = sys_sleep(timeout, clk_id);

	if (err != EOK) {
		// FIXME: check that the kernel returns EINTR when interrupted!
		err = EINTR;

		/* remaining time is only returned in relative timer mode */
		if (rmtp != NULL) {
			sys_clock_get(CLK_ID_MONOTONIC, &now);
			elapsed = now - start;
			if (timeout < elapsed) {
				leftover = elapsed - timeout;
			} else {
				leftover = 0;
			}
			*rmtp = __libc_ns2ts(leftover);
		}
	}

	return err;
}
