/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * timerfd_gettime.c
 *
 * POSIX library.
 *
 * azuepke, 2021-08-11: initial timer code
 * azuepke, 2022-10-23: timerfd
 */

#include "libc_internal.h"
#include <sys/timerfd.h>
#include <marron/api.h>


int timerfd_gettime(int fd, struct itimerspec *curr_value)
{
	sys_timeout_t remaining, period;
	err_t err;

	err = sys_timer_get(fd, &remaining, &period);
	if (err != EOK) {
		return __errno_return(err);
	}

	curr_value->it_value = __libc_ns2ts(remaining);
	curr_value->it_interval = __libc_ns2ts(period);

	return EOK;
}
