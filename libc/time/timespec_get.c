/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * timespec_get.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


/* get current time (C11) */
/* NOTE: returns 0 on failure, and "base" on success */
/* NOTE: only TIME_UTC is currently valid for "base" */
int timespec_get(struct timespec *ts, int base)
{
	sys_time_t now;

	if (base != TIME_UTC) {
		return 0;
	}

	(void)sys_clock_get(CLK_ID_REALTIME, &now);
	*ts = __libc_ns2ts(now);

	return TIME_UTC;
}
