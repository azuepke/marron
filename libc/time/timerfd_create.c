/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * timerfd_create.c
 *
 * POSIX library.
 *
 * azuepke, 2022-10-23: initial
 */

#include "libc_internal.h"
#include <sys/timerfd.h>
#include <marron/api.h>


int timerfd_create(int clockid, int flags)
{
	uint32_t real_flags;
	uint32_t fd;
	err_t err;

	if ((flags & ~(TFD_CLOEXEC | TFD_NONBLOCK)) != 0) {
		return __errno_return(EINVAL);
	}

	real_flags = 0;
	if ((flags & TFD_CLOEXEC) != 0) {
		real_flags |= SYS_FD_CLOEXEC;
	}
	if ((flags & TFD_NONBLOCK) != 0) {
		real_flags |= SYS_FD_NONBLOCK;
	}

	err = sys_timer_create(clockid, real_flags, NULL, 0, &fd);
	if (err != EOK) {
		if (err == ELIMIT) {
			err = ENOMEM;
		}
		return __errno_return(err);
	}
	return fd;
}
