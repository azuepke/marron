/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * nanosleep.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


int nanosleep(const struct timespec *rqtp, struct timespec *rmtp)
{
	return __errno_return(clock_nanosleep(CLOCK_REALTIME, 0, rqtp, rmtp));
}
