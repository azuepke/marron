/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * timerfd_settime.c
 *
 * POSIX library.
 *
 * azuepke, 2021-08-11: initial timer code
 * azuepke, 2022-10-23: timerfd
 */

#include "libc_internal.h"
#include <sys/timerfd.h>
#include <marron/api.h>


int timerfd_settime(int fd, int flags, const struct itimerspec *new_value, struct itimerspec *old_value)
{
	sys_timeout_t initial, period;
	unsigned int clock_flags;
	err_t err;

	if (!TS_VALID(new_value->it_value) || !TS_BOUNDED(new_value->it_value) ||
	    !TS_VALID(new_value->it_interval) || !TS_BOUNDED(new_value->it_interval)) {
		return __errno_return(EINVAL);
	}

	if (old_value != NULL) {
		err = timerfd_gettime(fd, old_value);
		if (err != EOK) {
			/* errno already set */
			return err;
		}
	}

	initial = __libc_ts2ns(&new_value->it_value);
	period = __libc_ts2ns(&new_value->it_interval);
	clock_flags = 0;
	if ((flags & TFD_TIMER_ABSTIME) != 0) {
		clock_flags = TIMEOUT_ABSOLUTE;
	}
	if ((flags & TFD_TIMER_CANCEL_ON_SET) != 0) {
		/* FIXME: flag not supported */
		return __errno_return(EINVAL);
	}

	err = sys_timer_set(fd, clock_flags, initial, period);
	if (err != EOK) {
		return __errno_return(err);
	}

	return EOK;
}
