/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * clock_settime.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


int clock_settime(clockid_t clock_id, const struct timespec *tp)
{
	sys_time_t time;
	err_t err;

	if (!TS_VALID(*tp) || !TS_BOUNDED(*tp)) {
		return __errno_return(EINVAL);
	}

	time = __libc_ts2ns(tp);
	err = sys_clock_set(clock_id, time);
	return __errno_return(err);
}
