/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * clock_gettime.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


int clock_gettime(clockid_t clock_id, struct timespec *tp)
{
	sys_time_t now;
	err_t err;

	err = sys_clock_get(clock_id, &now);
	if (err != EOK) {
		return __errno_return(EINVAL);
	}

	*tp = __libc_ns2ts(now);
	return EOK;
}
