/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * clock_getres.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


int clock_getres(clockid_t clock_id, struct timespec *tp)
{
	sys_time_t res;
	err_t err;

	err = sys_clock_res(clock_id, &res);
	if (err != EOK) {
		return __errno_return(EINVAL);
	}

	if (tp != NULL) {
		*tp = __libc_ns2ts(res);
	}

	return EOK;
}
