/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * clock.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


/* get processor time of program */
clock_t clock(void)
{
	return (clock_t)-1;
}
