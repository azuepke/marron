/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * time.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


time_t time(time_t *tloc)
{
	sys_time_t now;
	time_t t;

	sys_clock_get(CLK_ID_REALTIME, &now);
	t = div1000000000(now);

	if (tloc != NULL) {
		*tloc = t;
	}

	return t;
}
