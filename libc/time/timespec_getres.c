/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * timespec_getres.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <time.h>
#include <marron/api.h>


/* get time resolution (C23) */
/* NOTE: returns 0 on failure, and "base" on success */
/* NOTE: only TIME_UTC is currently valid for "base" */
int timespec_getres(struct timespec *ts, int base)
{
	sys_time_t res;

	if (base != TIME_UTC) {
		return 0;
	}

	if (ts != NULL) {
		(void)sys_clock_res(CLK_ID_REALTIME, &res);
		*ts = __libc_ns2ts(res);
	}

	return TIME_UTC;
}
