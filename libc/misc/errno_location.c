/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * errno_location.c
 *
 * Multithreaded implementation of errno.
 *
 * azuepke, 2013-11-18: initial
 * azuepke, 2021-07-19: rework from libthr
 */

#include "libc_internal.h"
#include <errno.h>


int *__errno_location(void)
{
	return __errno_addr();
}
