/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * exit.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <unistd.h>
#include <marron/api.h>
#include <marron/compiler.h>

void _Exit(int status)
{
	(void)status;
	sys_part_shutdown();
}

void _exit(int status) __alias(_Exit);

void exit(int status)
{
	/* FIXME: run global destructors here */

	_Exit(status);
}
