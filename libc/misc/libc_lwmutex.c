/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * libc_lwmutex.c
 *
 * Internal light-weight futex-based mutexes implementation.
 *
 * azuepke, 2013-09-02: initial
 * azuepke, 2021-07-19: libthr -> libposix rework
 */

#include "libc_internal.h"
#include <marron/api.h>
#include <marron/atomic.h>
#include <marron/compiler.h>


/** lock a light-weight futex-based mutex */
void __libc_lwmutex_lock(unsigned int *lock)
{
	unsigned int val, val2;

again:
	val = atomic_load_relaxed(lock);
	val2 = __libc_gettid() | SYS_FUTEX_LOCKED;

	/* safety-net -- we must not be the current lock holder */
	if (unlikely((val & SYS_FUTEX_TID_LOCKED_MASK) == val2)) {
		sys_abort();
	}

	/* fast path -- unlocked mutex */
	if (likely(val == 0)) {
		if (atomic_cas_acquire(lock, val, val2) == 0) {
			goto again;	/* CAS failed */
		}
		return;
	}

	/* slow path -- system call */
	(void)sys_futex_lock(lock, 0, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
	/* cannot fail */
}

/** unlock a light-weight futex-based mutex */
void __libc_lwmutex_unlock(unsigned int *lock)
{
	unsigned int val, val2;

again:
	val = atomic_load_relaxed(lock);
	val2 = __libc_gettid() | SYS_FUTEX_LOCKED;

	/* safety-net -- we must be the current lock holder */
	if (unlikely((val & SYS_FUTEX_TID_LOCKED_MASK) != val2)) {
		sys_abort();
	}

	/* fast path -- uncontended mutex */
	if (likely((val & SYS_FUTEX_WAITERS) == 0)) {
		val2 = 0;
		if (atomic_cas_release(lock, val, val2) == 0) {
			goto again;	/* CAS failed */
		}
		return;
	}

	/* slow path -- system call */
	(void)sys_futex_unlock(lock, 0);
	/* cannot fail */
}
