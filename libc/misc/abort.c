/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * abort.c
 *
 * POSIX library.
 *
 * azuepke, 2021-07-26: initial
 */

#include "libc_internal.h"
#include <stdlib.h>
#include <marron/api.h>
#include <marron/signal.h>


void abort(void)
{
	/* unmask + send signal */
	(void)sys_sig_mask(SIG_TO_MASK(SIGABRT), SIG_MASK_NONE);
	sys_abort();
	/* does not return */
}
