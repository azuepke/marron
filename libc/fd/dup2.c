/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * dup2.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>


int dup2(int oldfd, int newfd)
{
	err_t err;

	err = sys_fd_dup2(oldfd, newfd, 0);

	return __errno_return(err);
}
