/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * dup.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>


int dup(int oldfd)
{
	uint32_t newfd;
	err_t err;

	err = sys_fd_dup(oldfd, 0, 0, &newfd);
	if (err != EOK) {
		return __errno_return(err);
	}

	return newfd;
}
