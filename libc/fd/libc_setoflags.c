/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * libc_setoflags.c
 *
 * POSIX library.
 *
 * azuepke, 2025-02-09: initial
 */

#include "libc_internal.h"
#include <dirent.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


int __libc_setoflags(int fd, int oflags)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.req = IPC_IO_SETOFLAGS;
	m.h32 = oflags;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.h32;
}
