/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * close.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>


int close(int fd)
{
	err_t err;

	err = sys_fd_close(fd);

	return __errno_return(err);
}
