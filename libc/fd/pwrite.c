/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * pwrite.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


ssize_t pwrite(int fd, const void *buf, size_t size, off_t offset)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0R;
	m.req = IPC_IO_WRITE;
	m.h8 = SEEK_SET;
	m.h64 = offset;
	m.buf0 = (void *)(addr_t)buf;	/* NOTE: const-qualifier warning */
	m.size0 = size;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.size0;
}
