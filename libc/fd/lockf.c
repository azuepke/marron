/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * lockf.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>


int lockf(int fd, int cmd, off_t len)
{
	(void)fd;
	(void)cmd;
	(void)len;

	/* not supported */
	return __errno_return(EINVAL);
}
