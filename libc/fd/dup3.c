/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * dup3.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <fcntl.h>
#include <marron/api.h>


int dup3(int oldfd, int newfd, int flags)
{
	unsigned int fd_flags;
	err_t err;

	if (oldfd == newfd) {
		return __errno_return(EINVAL);
	}

	if ((flags & ~O_CLOEXEC) != 0) {
		return __errno_return(EINVAL);
	}
	fd_flags = ((flags & O_CLOEXEC) != 0) ? FD_CLOEXEC : 0;

	err = sys_fd_dup2(oldfd, newfd, fd_flags);

	return __errno_return(err);
}
