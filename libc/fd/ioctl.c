/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * ioctl.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdarg.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


int ioctl(int fd, int cmd, ...)
{
	struct sys_ipc m = { 0 };
	va_list args;
	void *arg;
	err_t err;
	int ret;

	va_start(args, cmd);
	arg = va_arg(args, void *);
	va_end(args);

	switch (cmd) {
	case FIOCLEX:
		/* set FD_CLOEXEC flag */
		ret = fcntl(fd, F_GETFD);
		if (ret == -1) {
			return ret;
		}

		ret |= FD_CLOEXEC;
		return fcntl(fd, F_SETFD, ret);

	case FIONCLEX:
		/* clear FD_CLOEXEC flag */
		ret = fcntl(fd, F_GETFD);
		if (ret == -1) {
			return ret;
		}

		ret &= ~FD_CLOEXEC;
		return fcntl(fd, F_SETFD, ret);

	case FIOGETOWN:
	case SIOCGPGRP:
		/* get owner */
		ret = fcntl(fd, F_GETOWN);
		if (ret != -1) {
			*(int *)arg = ret;
		}
		return ret;

	case FIOSETOWN:
	case SIOCSPGRP:
		/* set owner */
		return fcntl(fd, F_SETOWN, *(int *)arg);

	case FIOASYNC:
		/* set or clear O_ASYNC flag */
		ret = fcntl(fd, F_GETFL);
		if (ret == -1) {
			return ret;
		}

		if (*(int *)arg != 0) {
			/* set */
			ret |= O_ASYNC;
		} else {
			/* clear */
			ret &= ~O_ASYNC;
		}

		return fcntl(fd, F_SETFL, ret);

	case FIONBIO:
		/* set or clear O_NONBLOCK flag */
		ret = fcntl(fd, F_GETFL);
		if (ret == -1) {
			return ret;
		}

		if (*(int *)arg != 0) {
			/* set */
			ret |= O_NONBLOCK;
		} else {
			/* clear */
			ret &= ~O_NONBLOCK;
		}

		return fcntl(fd, F_SETFL, ret);

	default:
	case FIONREAD:
		/* number of characters waiting to read */
	case SIOCATMARK:
		/* sockatmark() */
		break;
	}

	/* perform ioctl() */
	m.req = IPC_IO_IOCTL;
	m.h32 = cmd;
	if ((cmd & _IOC_IN) != 0) {
		m.flags |= SYS_IPC_BUF0R;
	}
	if ((cmd & _IOC_OUT) != 0) {
		m.flags |= SYS_IPC_BUF0W;
	}
	m.buf0 = arg;
	m.size0 = _IOC_SIZE(cmd);

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.h32;
}
