/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * fsync.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


int fsync(int fd)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.req = IPC_IO_SYNC;
	/* m.h64 = 0; // offset */
	/* m.a64 = 0; // len */

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return err;
}
