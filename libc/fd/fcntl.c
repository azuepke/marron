/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * fcntl.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <fcntl.h>
#include <stdarg.h>
#include <marron/api.h>
#include <marron/types.h>
#include <marron/ipc_io.h>


int fcntl(int fd, int cmd, ...)
{
	va_list args;
	err_t err;

	switch (cmd) {
	case F_DUPFD:
	case F_DUPFD_CLOEXEC:
		{
			unsigned int flags;
			uint32_t new_fd;
			int start;

			va_start(args, cmd);
			start = va_arg(args, int);
			va_end(args);

			flags = (cmd == F_DUPFD_CLOEXEC) ? SYS_FD_CLOEXEC : 0;
			err = sys_fd_dup(fd, start, flags, &new_fd);
			if (err != EOK) {
				return __errno_return(err);
			}

			return new_fd;
		}

	case F_GETFD:
		{
			uint32_t attrib;

			err = sys_fd_cloexec_get(fd, &attrib);
			if (err != EOK) {
				return __errno_return(err);
			}

			return ((attrib & SYS_FD_CLOEXEC) != 0) ? FD_CLOEXEC : 0;
		}

	case F_SETFD:
		{
			int attrib;

			va_start(args, cmd);
			attrib = va_arg(args, int);
			va_end(args);

			attrib = ((attrib & FD_CLOEXEC) != 0) ? SYS_FD_CLOEXEC : 0;

			err = sys_fd_cloexec_set(fd, attrib);
			return __errno_return(err);
		}

	case F_GETFL:
		{
			int oflags;

			oflags = __libc_getoflags(fd);

			return oflags;
		}

	case F_SETFL:
		{
			int oflags;

			va_start(args, cmd);
			oflags = va_arg(args, int);
			va_end(args);

			__libc_setoflags(fd, oflags);
			return 0;
		}

	case F_GETLK:
	case F_SETLK:
	case F_SETLKW:
		{
			struct flock *flock;

			va_start(args, cmd);
			flock = va_arg(args, struct flock *);
			va_end(args);

			(void)flock;

			/* file locking not supported */
			return __errno_return(EINVAL);
		}

	case F_GETOWN:
		{
			/* validate fd */
			err = sys_fd_cloexec_get(fd, NULL);
			if (err != EOK) {
				return __errno_return(err);
			}

			/* no SIGURG to be send */
			return 0;
		}

	case F_SETOWN:
		{
			int arg;

			va_start(args, cmd);
			arg = va_arg(args, int);
			va_end(args);

			/* validate fd */
			err = sys_fd_cloexec_get(fd, NULL);
			if (err != EOK) {
				return __errno_return(err);
			}

			if (arg != 0) {
				/* process not found */
				return __errno_return(ESRCH);
			}

			return EOK;
		}

	default:
		return __errno_return(EINVAL);
	}
}
