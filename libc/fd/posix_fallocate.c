/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * posix_fallocate.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <fcntl.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


int posix_fallocate(int fd, off_t offset, off_t len)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.req = IPC_IO_ALLOCATE;
	m.h64 = offset;
	m.a64 = len;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return err;
}
