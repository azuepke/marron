/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * getpath.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <marron/api.h>
#include <marron/ipc_io.h>


ssize_t __getpath(int fd, void *buf, size_t size)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0W;
	m.req = IPC_IO_GETPATH;
	m.buf0 = buf;
	m.size0 = size;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.size0;
}
