/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * pipe.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>


int pipe(int fds[2])
{
	return pipe2(fds, 0);
}
