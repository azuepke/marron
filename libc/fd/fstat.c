/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * fstat.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <sys/stat.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


int fstat(int fd, struct stat *buf)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_BUF0W;
	m.req = IPC_IO_STAT;
	m.buf0 = buf;
	m.size0 = sizeof *buf;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return err;
}
