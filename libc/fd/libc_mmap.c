/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * libc_mmap.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <sys/mman.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


void *__libc_mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset)
{
	struct sys_ipc m = { 0 };
	err_t err;

	if ((unsigned int)prot > 0xff) {
		__errno_set(EINVAL);
		return MAP_FAILED;
	}

	m.flags = SYS_IPC_MAP;
	m.req = IPC_IO_MMAP;
	m.map_prot = prot;
	m.map_flags = flags;
	m.h64 = offset;
	m.map_addr = (addr_t)addr;
	m.map_size = len;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		__errno_set(err);
		return MAP_FAILED;
	}

	return (void*)m.map_start;
}
