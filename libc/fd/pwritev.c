/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * pwritev.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <sys/uio.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <unistd.h>


ssize_t pwritev(int fd, const struct iovec *iov, int iovcnt, off_t offset)
{
	struct sys_ipc m = { 0 };
	err_t err;

	m.flags = SYS_IPC_IOV | SYS_IPC_BUF0R;
	m.req = IPC_IO_WRITE;
	m.h8 = SEEK_SET;
	m.h64 = offset;
	m.buf0 = (void *)(addr_t)iov;	/* NOTE: const-qualifier warning */
	m.size0 = iovcnt;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.size0;
}
