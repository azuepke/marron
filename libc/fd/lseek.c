/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * lseek.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-12: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>
#include <marron/ipc_io.h>


off_t lseek(int fd, off_t offset, int whence)
{
	struct sys_ipc m = { 0 };
	err_t err;

	if ((unsigned)whence > SEEK_END) {
		return __errno_return(EINVAL);
	}

	m.req = IPC_IO_SEEK;
	m.h8 = whence;
	m.h64 = offset;

	err = sys_ipc_call(fd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	return m.h64;
}
