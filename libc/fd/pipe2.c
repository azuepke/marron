/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * pipe2.c
 *
 * POSIX library.
 *
 * azuepke, 2022-08-15: initial
 */

#include "libc_internal.h"
#include <unistd.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>


int pipe2(int fds[2], int flags)
{
	struct sys_ipc m = { 0 };
	err_t err;
	int pfd;

	if ((flags & ~(O_NONBLOCK | O_CLOEXEC)) != 0) {
		return __errno_return(EINVAL);
	}

	m.flags = SYS_IPC_FD0 | SYS_IPC_FD1;
	if ((flags & O_CLOEXEC) != 0) {
		m.flags |= SYS_IPC_FD_CLOEXEC;
	}
	m.req = IPC_IO_FS_OPEN;
	m.h8 = IPC_IO_FS_OPEN_PIPE;
	m.h32 = flags & O_NONBLOCK;

	pfd = __libc_getpipeserver();
	if (pfd < 0) {
		return __errno_return(ENFILE);
	}

	err = sys_ipc_call(pfd, &m, &m);
	if (err == EOK) {
		err = m.err;
	}
	if (err != EOK) {
		return __errno_return(err);
	}

	fds[0] = m.fd0;
	fds[1] = m.fd1;
	return EOK;
}
