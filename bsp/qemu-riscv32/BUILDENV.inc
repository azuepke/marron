# SPDX-License-Identifier: MIT
# Copyright 2013, 2021, 2023 Alexander Zuepke

MARRON_BSP_NAME="QEMU 32-bit RISC-V"
MARRON_ARCH=riscv
MARRON_SUBARCH=rv32g
MARRON_SMP=yes
