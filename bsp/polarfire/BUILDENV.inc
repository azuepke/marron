# SPDX-License-Identifier: MIT
# Copyright 2013, 2021, 2023 Alexander Zuepke

MARRON_BSP_NAME="Polarfire on Icicle (64-bit)"
MARRON_ARCH=riscv
MARRON_SUBARCH=rv64g
MARRON_SMP=yes
