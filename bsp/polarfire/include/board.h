/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021, 2023 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for Polarfire
 *
 * azuepke, 2015-07-13: initial
 * azuepke, 2021-01-16: adapted to mini-OS
 * azuepke, 2023-09-03: adapted for RISC-V from qemu-aarch64
 * azuepke, 2023-09-12: BSP for Polarfire using OpenSBI 0.6
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[2];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

/* bsp.c */
void bsp_ipi_handler(void);

#endif

#define BOARD_NAME			"polarfire"

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 4M on 32-bit, offset 2M on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/* hart ID remapping phys to kernel
 * On Polarfire, the M-only is hart ID 0, and others start at hart ID 1
 */
#define RISCV_HART_REMAP_TABLE				{ -1, 0, 1, 2, 3 }
/* hart ID remapping kernel to phys */
#define RISCV_HART_REMAP_K2P(cpu_id)		((cpu_id) + 1)
#define RISCV_HART_REMAP_K2P_MASK(cpu_mask)	((cpu_mask) << 1)

/* overrides for S-mode contexts */
#define PLIC_ENABLE_OFFSET		0x002100
#define PLIC_ENABLE_PER_CPU		0x100
#define PLIC_CONTEXT_OFFSET		0x202000
#define PLIC_CONTEXT_PER_CPU	0x2000

/* timer frequency */
#define SBI_TIMER_FREQ			1000000	/* 1 MHz */

/*
 * UART on Polarfire (all 8250 compatible)
 * - 20000000 MMUART0
 * - 20100000 MMUART1
 * - 20102000 MMUART2
 * - 20104000 MMUART3
 * - 20106000 MMUART4
 */
#define NS16550_UART_REGS		CFG_IO_ADDR_uart1
#define NS16550_UART_REGSIZE	4
#define NS16550_UART_CLOCK		300000000
#define NS16550_UART_KEEPBAUD	1	/* keep baudrate of bootloader */

#if 0
/* OpenSBI startup from any possible hart */
#define OPENSBI_SMP_STARTUP 1
#endif

#if 1
/* enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1
#endif

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
