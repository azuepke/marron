# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="Rockchip RK3568 with 4x Cortex-A55 (64-bit)"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a55
MARRON_SMP=yes
