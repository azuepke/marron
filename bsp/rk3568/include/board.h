/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021-2024 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for RK3568
 *
 * azuepke, 2024-07-21: cloned from rk3588
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_ICC_MODE		1	/* use ICC_x_EL1 registers to access GICC */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_REDIST_BASE		CFG_IO_ADDR_gic_redist
#define GIC_REDIST_SIZE		0x20000		/* GICv3: 128K, GICv4: 256K */
#define GIC_NUM_IRQS		(32 + 320)
#define GIC_NUM_CORES		4
/* GICv3: translate logical CPU ID to REDIST offset, SGIR ID, or IROUTER ID */
#define GIC_CPU_REDIST_ID(x)	(x)
#define GIC_CPU_SGIR_ID(x)		(((x) << 16) | 0x1)
#define GIC_CPU_ROUTER_ID(x)	((x) << 8)

/* we have specific settings for the CPU */
/* With a DSU, we have up to eight ARM cores in a single cluster. */
#define ARM_MPIDR_AFF0_BITS 0
#define ARM_MPIDR_AFF1_BITS 3
#define ARM_MPIDR_AFF2_BITS 0
#define ARM_MPIDR_AFF3_BITS 0

/*
 * UARTs on RK3568 (all 8250-style UARTs):
 *
 * UART0: 0xfdd50000
 * UART1: 0xfe650000
 * UART2: 0xfe660000
 * UART3: 0xfe670000
 * UART4: 0xfe680000
 * UART5: 0xfe690000
 * UART6: 0xfe6a0000
 * UART7: 0xfe6b0000
 * UART8: 0xfe6c0000
 * UART9: 0xfe6d0000
 */
#define NS16550_UART_REGS		CFG_IO_ADDR_uart2
#define NS16550_UART_REGSIZE	4
#define NS16550_UART_CLOCK		24000000	/* 24 MHz clock */
#if 0
#define NS16550_UART_KEEPBAUD	1	/* keep baudrate of bootloader */
#endif

#if 0
#define CPU_COUNTER_FREQ	24000000	/* 24 MHz */
#endif

#endif
