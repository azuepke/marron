# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="QEMU 64-bit with ARM Cortex A57 (64-bit)"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a57
MARRON_SMP=yes
