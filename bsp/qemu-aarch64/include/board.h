/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for QEMU ARM "virt" board with Cortex-A57.
 *
 * azuepke, 2015-07-13: initial
 * azuepke, 2021-01-16: adapted to mini-OS
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* pl011_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS	288
#define GIC_CPU_BIT(x)	(1u << (x))

/*
 * The PL011 UART on QEMU ARM "virt" board is located
 * at physical address 0x09000000.
 * See also qemu/hw/arm/virt.c in QEMU sources.
 */
#define PL011_SERIAL_REGS	CFG_IO_ADDR_uart0
#define PL011_SERIAL_CLOCK	24000000

/** enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1

/** QEMU implements PSCI calls via HVC instead of SMC when EL2 */
/* enable EL2 mode in QEMU: $ qemu-system-aarch64 -M virt,virtualization=on */
/* #define QEMU_PSCI_CALL 1 */

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
