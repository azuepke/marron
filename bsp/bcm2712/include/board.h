/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021, 2023 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for Raspberry Pi 5 (BCM2712)
 *
 * azuepke, 2020-08-05: Raspberry Pi port
 * azuepke, 2023-11-29: cloned from RPi4 BSP
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS	320
#define GIC_CPU_BIT(x)	(1u << (x))

/* we have specific settings for the CPU */
/* With a DSU, we have up to eight ARM cores in a single cluster. */
#define ARM_MPIDR_AFF0_BITS 0
#define ARM_MPIDR_AFF1_BITS 3
#define ARM_MPIDR_AFF2_BITS 0
#define ARM_MPIDR_AFF3_BITS 0

/*
 * UARTs on Raspberry Pi 5 (all PL011 style):
 *
 * UART0: 0x107d001000
 * UART2: 0x107d001400
 * UART3: 0x107d001600
 * UART4: 0x107d001800
 * UART5: 0x107d001a00
 */
#define PL011_SERIAL_REGS		(CFG_IO_ADDR_uart0 + 0x000)
#define PL011_SERIAL_CLOCK		44236800
#if 0
#define PL011_SERIAL_KEEPBAUD	1	/* keep baudrate of bootloader */
#endif

#define CPU_COUNTER_FREQ	54000000	/* 54 MHz */

#define GPU_IRQ_REGS	(CFG_IO_ADDR_rpi_irq + 0x200)	/* GPU interrupt controller */
#define ARM_IRQ_REGS	CFG_IO_ADDR_arm_ctrl	/* ARM interrupt router/controller */

#endif
