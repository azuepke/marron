/* SPDX-License-Identifier: MIT */
/* Copyright 2020 Alexander Zuepke */
/*
 * rpi_irq.h
 *
 * Raspberry Pi BCM2711 IRQ handling
 *
 * azuepke, 2020-11-05: initial
 */

#ifndef RPI_IRQ_H_
#define RPI_IRQ_H_

/* secondary interrupt controller (BCM2835) */
// 64 ARM_TIMER
// 65 ARM_MAILBOX
// 66 ARM_DOORBELL_0
// 67 ARM_DOORBELL_1
#define IRQ_ID_GPU_HALT0	68
#define IRQ_ID_GPU_HALT1	69
#define IRQ_ID_ARM_ADDR_ERROR	70
#define IRQ_ID_ARM_AXI_ERROR	71
// 72..84 selected GPU IRQs 7, 9, 10, 18, 19, 53, 54, 55, 56, 57, 62; do not use
// 96..127 IRQs in GPU_IRQ_PENDING1 (GPU IRQs 0..31)
// 128..159 IRQs in GPU_IRQ_PENDING2  (GPU IRQs 32..63)

void bcm_irq_init(void);

#endif
