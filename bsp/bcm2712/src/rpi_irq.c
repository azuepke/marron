/* SPDX-License-Identifier: MIT */
/* Copyright 2020 Alexander Zuepke */
/*
 * rpi_irq.c
 *
 * Raspberry Pi BCM2711 IRQ handling (for Raspberry Pi 4)
 *
 * azuepke, 2020-08-05: initial
 * azuepke, 2020-11-05: from RPi2 code
 */

#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <assert.h>
#include <arch_io.h>
#include <arm_insn.h>
#include <board.h>
#include <rpi_irq.h>
#include <marron/bit.h>
#include <cpu_timer.h>
#include <barrier.h>

////////////////////////////////////////////////////////////////////////////////

#if 0
/* GPU interrupt controller registers at 0x7e00b000 (mapped at 0x3f00b200) */
#define GPU_IRQ_PENDING0 0x00
#define GPU_IRQ_PENDING1 0x04
#define GPU_IRQ_PENDING2 0x08
#define GPU_FIQ_CONTROL  0x0c
#define GPU_IRQ_ENABLE1  0x10
#define GPU_IRQ_ENABLE2  0x14
#define GPU_IRQ_ENABLE0  0x18
#define GPU_IRQ_DISABLE1 0x1c
#define GPU_IRQ_DISABLE2 0x20
#define GPU_IRQ_DISABLE0 0x24

/* register accessors */
static inline uint32_t gpu_irq_rd(unsigned long reg)
{
	return read32(io_ptr(GPU_IRQ_REGS, reg));
}

static inline void gpu_irq_wr(unsigned long reg, uint32_t val)
{
	write32(io_ptr(GPU_IRQ_REGS, reg), val);
}
#endif

////////////////////////////////////////////////////////////////////////////////

/* ARM interrupt router registers at 0x40000000 */
#define ARM_IRQ_CORE_TIMER_CONTROL			0x00
#define ARM_IRQ_CORE_TIMER_PRESCALER		0x08
#define ARM_GPU_IRQ_ROUTING					0x0c
#define ARM_PMU_IRQ_ROUTING_SET				0x10
#define ARM_PMU_IRQ_ROUTING_CLR				0x14
#define ARM_IRQ_CORE_TIMER_LO				0x1c
#define ARM_IRQ_CORE_TIMER_HI				0x20
#define ARM_IRQ_LOCAL_TIMER_IRQ_ROUTING		0x24
#define ARM_IRQ_AXI_OUTSTANDING_COUNTERS	0x2c
#define ARM_IRQ_AXI_OUTSTANDING_IRQ			0x30
#define ARM_IRQ_LOCAL_TIMER_CONTROL			0x34
#define ARM_IRQ_LOCAL_TIMER_CLR				0x38

#define ARM_IRQ_CORE_TIMER_IRQS(core)		(0x40 + 4*(core))
#define ARM_IRQ_CORE_MBOX_IRQS(core)		(0x50 + 4*(core))
#define ARM_IRQ_CORE_IRQ_SOURCE(core)		(0x60 + 4*(core))
#define ARM_IRQ_CORE_FIQ_SOURCE(core)		(0x70 + 4*(core))

#define ARM_IRQ_MBOX_SET(core, mbox)		(0x80 + 16*(core) + 4*(mbox))
#define ARM_IRQ_MBOX_GET(core, mbox)		(0xc0 + 16*(core) + 4*(mbox))
#define ARM_IRQ_MBOX_CLR(core, mbox)		(0xc0 + 16*(core) + 4*(mbox))

/* register accessors */
static inline uint32_t arm_irq_rd(unsigned long reg)
{
	return read32(io_ptr(ARM_IRQ_REGS, reg));
}

static inline void arm_irq_wr(unsigned long reg, uint32_t val)
{
	write32(io_ptr(ARM_IRQ_REGS, reg), val);
}

////////////////////////////////////////////////////////////////////////////////

/** initialize the RPi BCM2636 interrupt controller */
__init void bcm_irq_init(void)
{
	/* core timer runs on external 19.2 MHz clock (ABP does not work) */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_CONTROL, 0);

	/* reset core clock (this resets the prescaler) */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_LO, 0);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_HI, 0);

	/* set core timer prescaler for divider ratio of 1 */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_PRESCALER, 0x80000000);

	/* route IRQ and FIQ of GPU to core #0 */
	arm_irq_wr(ARM_GPU_IRQ_ROUTING, (0 << 0) | (0 << 2));

	/* route each core's PMU interrupts to IRQs */
	arm_irq_wr(ARM_PMU_IRQ_ROUTING_CLR, 0xff);
	arm_irq_wr(ARM_PMU_IRQ_ROUTING_SET, 0x0f);

	/* route each core's four timer interrupt to IRQs */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(0), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(1), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(2), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(3), 0x0f);

	/* clear all mailboxes */
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 3), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 3), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 3), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 3), 0xffffffff);

	/* route each core's four mailbox interrupt to IRQs */
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(0), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(1), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(2), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(3), 0x0f);

	/* disable AXI interrupts */
	arm_irq_wr(ARM_IRQ_AXI_OUTSTANDING_IRQ, 0);

	/* disable local timer (interrupts routes to core #0) */
	arm_irq_wr(ARM_IRQ_LOCAL_TIMER_CONTROL, 0);
	arm_irq_wr(ARM_IRQ_LOCAL_TIMER_IRQ_ROUTING, 0x0);

#if 0
	/* disable GPU FIQ interrupt */
	gpu_irq_wr(GPU_FIQ_CONTROL, 0x0);

	/* disable all GPU IRQs */
	gpu_irq_wr(GPU_IRQ_DISABLE0, 0x0);
	gpu_irq_wr(GPU_IRQ_DISABLE1, 0x0);
	gpu_irq_wr(GPU_IRQ_DISABLE2, 0x0);

	/* enable GPU IRQs for access errors */
	// FIXME: do not enable
	//gpu_irq_wr(GPU_IRQ_ENABLE0, (1u << 7) | (1u << 6));

#endif
}
