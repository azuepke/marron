/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2016, 2020-2021 Alexander Zuepke */
/*
 * board.c
 *
 * Board initialization for NXP IMX6 Cortex A9.
 *
 * azuepke, 2013-11-19: initial cloned
 * awerner, 2020-01-30: copy form qemu and port to imx6
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <gic.h>
#include <assert.h>
#include <mpcore_timer.h>
#include <arm_insn.h>
#include <preboot.h>
#include <arch_io.h>
#include <barrier.h>


/** BSP name string */
const char bsp_name[] = "imx6";

#ifdef SMP
// CPUs online
unsigned long bsp_cpu_online_mask = 0;
// CPUs started scheduling
unsigned int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned int cpu;

	/* Sytem reset controller @ 0x020d8000, see page 5069 in i.MX6 manual */
	volatile uint32_t *src_scr = io_ptr(CFG_IO_ADDR_src_base, 0x00);
	volatile uint32_t *src_gpr = io_ptr(CFG_IO_ADDR_src_base, 0x20);
	uint32_t val;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
		/* set SMP entry point */
		write32(&src_gpr[cpu * 2], BOARD_KERNEL_TO_PHYS((unsigned long)_start));
		/* bits 22..24: enable core1..3 */
		/* bits 13..16: reset core0..3 */
		arm_dmb();
		val = read32(src_scr);
		val |= (1 << (21 + cpu)) | (1 << (13 + cpu));
		write32(src_scr, val);

		/* wait until CPU is up */
		barrier_wait_bit(&bsp_cpu_online_mask, cpu);
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu_id)
{
	assert(cpu_id == bsp_cpu_id());
	barrier_set_bit(&bsp_cpu_online_mask, cpu_id);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu_id == 0) {
		barrier_set_val(&bsp_cpu_go, 1);
	} else {
		/* wait until we see the "go" signal */
		barrier_wait_val(&bsp_cpu_go, 1);
	}

	/* start timer here on SMP */
	mpcore_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(38400);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		gic_init_dist();
		gic_init_cpu(cpu_id);
		mpcore_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		mpcore_timer_start();
#endif
	}
#ifdef SMP
	else {
		printk("secondary CPU %u came up\n", cpu_id);
		gic_init_cpu(cpu_id);
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	/* use WFE instead of WFI to keep the cycle counter running */
	__asm__ volatile ("dsb; wfe" : : : "memory");
}

/* reset the board */
static inline void imx6_reset(void)
{
	/* Reset via watchdog control register @ 0x020bc000 */
	volatile uint16_t *wdog_wcr = io_ptr(WDOG_REGS, 0x0);

	/* enable the watchdog twice, see
	 * https://community.freescale.com/thread/323034?db=5
	 */
	write16(wdog_wcr, 0x4);
	arm_dmb();
	write16(wdog_wcr, 0x4);
	arm_dmb();

	/* now wait for WFI */
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		gic_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		imx6_reset();
	}

	__bsp_halt();
	unreachable();
}
