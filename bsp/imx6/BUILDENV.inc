# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="i.MX6 with four ARM Cortex A9"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a9
MARRON_SMP=yes
