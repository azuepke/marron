/* SPDX-License-Identifier: MIT */
/* Copyright 2017,2023 Alexander Zuepke */
/*
 * sbi.h
 *
 * RISC-V SBI (supervisor binary interface)
 *
 * This interface targets the following APIs:
 * - legacy SBI API (version 0.1)
 * - base extension (version 0.2)
 * - hart state management extension (0.3)
 * - system reset extension (0.3)

 * described in version 1.0 of the SBI specification.
 *
 * NOTE: The SBI takes a legacy function <= 15 in a7,
 * or an extension ID >= 16 in a7, and a function ID in a6.
 * SBI preserves all registers except a0 and a1
 *
 * azuepke, 2017-04-06: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 * azuepke, 2023-09-09: extended API to start other cores
 */

#ifndef SBI_H_
#define SBI_H_

#include <stdint.h>
#include <marron/compiler.h>

/* SBI legacy functions (passed in a7) */
#define SBI_SET_TIMER				0
#define SBI_CONSOLE_PUTCHAR			1
#define SBI_CONSOLE_GETCHAR			2
#define SBI_CLEAR_IPI				3
#define SBI_SEND_IPI				4
#define SBI_REMOTE_FENCE_I			5
#define SBI_REMOTE_SFENCE_VMA		6
#define SBI_REMOTE_SFENCE_VMA_ASID	7
#define SBI_SHUTDOWN				8

/* SBI base extension (EID 0x10) */
#define SBI_BASE					0x10
#define SBI_GET_SPEC_VERSION		0
#define SBI_GET_IMPL_ID				1
#define SBI_GET_IMPL_VERSION		2
#define SBI_PROBE_EXTENSION			3
#define SBI_GET_MVENDORID			4
#define SBI_GET_MARCHID				5
#define SBI_GET_MIMPID				6

/* SBI hart state management extension (EID 0x48534D) */
#define SBI_HSM						0x48534D
#define SBI_HART_START				0
#define SBI_HART_STOP				1
#define SBI_HART_GET_STATUS			2
#define SBI_HART_SUSPEND			3

/* SBI system reset extension (EID 0x53525354) */
#define SBI_SRST					0x53525354
#define SBI_SYSTEM_RESET			0

#define SBI_RESET_TYPE_SHUTDOWN		0
#define SBI_RESET_TYPE_COLD_REBOOT	1
#define SBI_RESET_TYPE_WARM_REBOOT	2

#define SBI_RESET_REASON_NO_REASON	0
#define SBI_RESET_REASON_SYSTEM_FAILURE	1


/* SBI error codes (returned in a0) */
#define SBI_SUCCESS					0
#define SBI_ERR_FAILED				-1
#define SBI_ERR_NOT_SUPPORTED		-2
#define SBI_ERR_INVALID_PARAM		-3
#define SBI_ERR_DENIED				-4
#define SBI_ERR_INVALID_ADDRESS		-5
#define SBI_ERR_ALREADY_AVAILABLE	-6
#define SBI_ERR_ALREADY_STARTED		-7
#define SBI_ERR_ALREADY_STOPPED		-8

/* inlined SBI call (legacy API) */
#define SBI_CALL_LEGACY(id, a0, a1, a2, a3)	({	\
	register unsigned long tmp_a7 __asm__("a7") = (unsigned long)(id);	\
	register unsigned long tmp_a0 __asm__("a0") = (unsigned long)(a0);	\
	register unsigned long tmp_a1 __asm__("a1") = (unsigned long)(a1);	\
	register unsigned long tmp_a2 __asm__("a2") = (unsigned long)(a2);	\
	register unsigned long tmp_a3 __asm__("a3") = (unsigned long)(a3);	\
	__asm__ volatile (	\
		"ecall"	\
		: "+r" (tmp_a0), "+r" (tmp_a1)	\
		: "r" (tmp_a2), "r" (tmp_a3), "r" (tmp_a7)	\
		: "memory"	\
		);	\
	tmp_a0;	\
})

/* inlined SBI call (extended API) */
#define SBI_CALL_EXT(eid, fid, a0, a1, a2, a3, retval)	({	\
	register unsigned long tmp_a7 __asm__("a7") = (unsigned long)(eid);	\
	register unsigned long tmp_a6 __asm__("a6") = (unsigned long)(fid);	\
	register unsigned long tmp_a0 __asm__("a0") = (unsigned long)(a0);	\
	register unsigned long tmp_a1 __asm__("a1") = (unsigned long)(a1);	\
	register unsigned long tmp_a2 __asm__("a2") = (unsigned long)(a2);	\
	register unsigned long tmp_a3 __asm__("a3") = (unsigned long)(a3);	\
	__asm__ volatile (	\
		"ecall"	\
		: "+r" (tmp_a0), "+r" (tmp_a1)	\
		: "r" (tmp_a2), "r" (tmp_a3), "r" (tmp_a6), "r" (tmp_a7)	\
		: "memory"	\
		);	\
	(retval) = tmp_a1;	\
	tmp_a0;	\
})

/** Set next timer expiry in ticks
 */
static inline void sbi_set_timer(uint64_t stime_value)
{
#ifdef __LP64__
	SBI_CALL_LEGACY(SBI_SET_TIMER, stime_value, stime_value >> 32, 0, 0);
#else /* 32-bit */
	SBI_CALL_LEGACY(SBI_SET_TIMER, stime_value, 0, 0, 0);
#endif
}

/** Write char to debug console (blocking)
 *
 *  Returns 0 on success, negative on failure.
 *
 *  NOTE: deprecated
 */
static inline long sbi_console_putchar(unsigned char c)
{
	return SBI_CALL_LEGACY(SBI_CONSOLE_PUTCHAR, c, 0, 0, 0);
}

/** Read char from debug console
 *
 *  Returns char on success, negative on failure.
 *
 *  NOTE: deprecated
 */
static inline long sbi_console_getchar(void)
{
	return SBI_CALL_LEGACY(SBI_CONSOLE_GETCHAR, 0, 0, 0, 0);
}

/** Clear IPI
 *
 *  Returns != 0 if an IPI was pending, 0 otherwise.
 *
 *  NOTE: deprecated, clear IPI in sip.SSIP
 */
static inline long sbi_clear_ipi(void)
{
	return SBI_CALL_LEGACY(SBI_CLEAR_IPI, 0, 0, 0, 0);
}

/** Send IPI to harts listed in bitmap
 *
 *  Returns 0 on success, negative on failure.
 */
static inline long sbi_send_ipi(const unsigned long *harts)
{
	return SBI_CALL_LEGACY(SBI_SEND_IPI, harts, 0, 0, 0);
}

/** Execute FENCE.I on harts listed in bitmap
 *
 *  Returns 0 on success, negative on failure.
 */
static inline long sbi_remote_fence_i(const unsigned long *harts)
{
	return SBI_CALL_LEGACY(SBI_REMOTE_FENCE_I, harts, 0, 0, 0);
}

/** Execute SFENCE.VM for given ASID on harts listed in bitmap
 *
 *  Returns 0 on success, negative on failure.
 */
static inline long sbi_remote_sfence_vma(const unsigned long *harts, unsigned long start, unsigned long size)
{
	return SBI_CALL_LEGACY(SBI_REMOTE_SFENCE_VMA, harts, start, size, 0);
}

/** Execute SFENCE.VMA for given ASID on harts listed in bitmap
 *
 *  Returns 0 on success, negative on failure.
 */
static inline long sbi_remote_sfence_vma_asid(const unsigned long *harts, unsigned long start, unsigned long size, unsigned long asid)
{
	return SBI_CALL_LEGACY(SBI_REMOTE_SFENCE_VMA_ASID, harts, start, size, asid);
}

/** Shutdown kernel
 *
 * NOTE: might return if function is not implemented by SBI.
 */
static inline void sbi_shutdown(void)
{
	SBI_CALL_LEGACY(SBI_SHUTDOWN, 0, 0, 0, 0);
}

/** Start hart
 */
static inline long sbi_hart_start(unsigned long hartid, unsigned long start_addr, unsigned long opaque)
{
	register unsigned long retval __unused;
	return SBI_CALL_EXT(SBI_HSM, SBI_HART_START, hartid, start_addr, opaque, 0, retval);
}

/** System reset
 */
static inline long sbi_system_reset(unsigned int reset_type, unsigned int reset_reason)
{
	register unsigned long retval __unused;
	return SBI_CALL_EXT(SBI_SRST, SBI_SYSTEM_RESET, reset_type, reset_reason, 0, 0, retval);
}

#endif
