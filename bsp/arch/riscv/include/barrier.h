/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * barrier.h
 *
 * Synchronization barriers for RISC-V
 *
 * azuepke, 2023-09-29: initial
 */

#ifndef BARRIER_H_
#define BARRIER_H_

#include <marron/atomic.h>
#include <riscv_insn.h>


/** wait until given bit is set in atomic bitmask barrier */
static inline void barrier_wait_bit(unsigned long *barrier, unsigned long bit)
{
	unsigned long mask = 1ul << bit;
	unsigned long val;

	do {
		riscv_pause();
		val = atomic_load_acquire(barrier);
	} while ((val & mask) == 0);
}

/** set bit in atomic bitmask barrier */
static inline void barrier_set_bit(unsigned long *barrier, unsigned long bit)
{
	unsigned long mask = 1ul << bit;

	atomic_set_mask_release(barrier, mask);
}

/** wait until expected value is shown in atomic value barrier */
static inline void barrier_wait_val(unsigned int *barrier, unsigned int expect)
{
	unsigned long val;

	do {
		riscv_pause();
		val = atomic_load_acquire(barrier);
	} while (val != expect);
}

/** set value in atomic value barrier */
static inline void barrier_set_val(unsigned int *barrier, unsigned int val)
{
	atomic_store_release(barrier, val);
}

#endif
