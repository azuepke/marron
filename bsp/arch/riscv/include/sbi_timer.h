/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * sbi_timer.h
 *
 * RISC-V SBI-based per-CPU timer
 *
 * azuepke, 2017-04-06: initial RISC-V BSP
 * azuepke, 2023-09-03: imported & adapted
 */

#ifndef SBI_TIMER_H_
#define SBI_TIMER_H_

void sbi_timer_int(void);
void sbi_timer_init(unsigned int freq);
void sbi_timer_start(void);

#endif
