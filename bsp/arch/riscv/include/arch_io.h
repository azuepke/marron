/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * arch_io.h
 *
 * RISC-V specific I/O access.
 *
 * NOTE: uses a different order than Linux:
 *   out8(port, value);
 *   write32(addr, value);
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 * azuepke, 2023-09-23: former riscv_io.h
 */

#ifndef ARCH_IO_H_
#define ARCH_IO_H_

#include <stdint.h>

static inline volatile void *io_ptr(uintptr_t base, size_t offset)
{
	return (volatile void *)(base + offset);
}

static inline uint8_t read8(const volatile void *addr)
{
	uint8_t ret;
	__asm__ volatile("lbu %0, %1" : "=r" (ret)
	                 : "m" (*(const volatile uint8_t *)addr) : "memory");
	return ret;
}

static inline uint16_t read16(const volatile void *addr)
{
	uint16_t ret;
	__asm__ volatile("lhu %0, %1" : "=r" (ret)
	                 : "m" (*(const volatile uint16_t *)addr) : "memory");
	return ret;
}

static inline uint32_t read32(const volatile void *addr)
{
	uint32_t ret;
#ifdef __LP64__
	__asm__ volatile("lwu %0, %1" : "=r" (ret)
	                 : "m" (*(const volatile uint32_t *)addr) : "memory");
#else
	__asm__ volatile("lw %0, %1" : "=r" (ret)
	                 : "m" (*(const volatile uint32_t *)addr) : "memory");
#endif
	return ret;
}

#ifdef __LP64__
static inline uint64_t read64(const volatile void *addr)
{
	uint64_t ret;
	__asm__ volatile("ld %0, %1" : "=r" (ret)
	                 : "m" (*(const volatile uint64_t *)addr) : "memory");
	return ret;
}
#endif

static inline void write8(volatile void *addr, uint8_t val)
{
	__asm__ volatile ("sb %0, %1" : : "r" (val),
	                  "m" (*(volatile uint8_t *)addr) : "memory");
}

static inline void write16(volatile void *addr, uint16_t val)
{
	__asm__ volatile ("sh %0, %1" : : "r" (val),
	                  "m" (*(volatile uint16_t *)addr) : "memory");
}

static inline void write32(volatile void *addr, uint32_t val)
{
	__asm__ volatile ("sw %0, %1" : : "r" (val),
	                  "m" (*(volatile uint32_t *)addr) : "memory");
}

#ifdef __LP64__
static inline void write64(volatile void *addr, uint64_t val)
{
	__asm__ volatile ("sd %0, %1" : : "r" (val),
	                  "m" (*(volatile uint64_t *)addr) : "memory");
}
#endif

/* I/O write barrier:
 * to be used when ordering writes to memory mapped devices
 * think of "flush write buffer"
 */
static inline void io_write_barrier(void)
{
	__asm__ volatile ("fence o,ow" : : : "memory");
}

#endif
