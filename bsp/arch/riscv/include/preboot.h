/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * preboot.h
 *
 * Preboot code -- setup kernel page tables at boot time.
 *
 * NOTE: This code runs in physical addressing mode! Do not call from C code!
 *
 * azuepke, 2021-05-05: initial
 * azuepke, 2023-09-03: adapted for RISC-V
 */

#ifndef PREBOOT_H_
#define PREBOOT_H_

#include <marron/compiler.h>
#include <riscv_pte.h>

/* check variable if the preboot phase succeeded */
extern unsigned int bsp_preboot_ok;

/* Setup kernel page tables (do not call from C code) */
void preboot_setup_tagetables(riscv_pte_t *pt, unsigned long load_addr);

/* initialize first 64-bytes of all shared memory segments to zero */
__init void preboot_init_shms(void);

#endif
