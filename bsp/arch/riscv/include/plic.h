/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * plic.h
 *
 * RISC-V PLIC
 *
 * azuepke, 2023-09-09: initial PLIC code
 * azuepke, 2023-09-12: split from bsp.c
 */

#ifndef PLIC_H_
#define PLIC_H_

void plic_init_global(void);
void plic_init_local(void);

#endif
