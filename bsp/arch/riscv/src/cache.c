/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * cache.c
 *
 * RISC-V cache handling
 *
 * azuepke, 2017-04-06: initial RISC-V BSP
 * azuepke, 2023-09-12: split from bsp.c
 */

#include <kernel.h>
#include <assert.h>
#include <bsp.h>
#include <marron/compiler.h>
#include <sbi.h>
#include <board.h>
#include <riscv_insn.h>
#include <current.h>


err_t bsp_cache(sys_cache_op_t mode, addr_t addr, size_t size, addr_t alias __unused)
{
#ifdef SMP
	unsigned long hart_mask;
	unsigned long cpu_mask;
#endif
	err_t err;

	(void)addr;
	(void)size;
	(void)alias;

	switch (mode) {
	case CACHE_OP_INVAL_ICACHE_RANGE:
	case CACHE_OP_INVAL_ICACHE_ALL:
		/* at the moment, we must invalidate the whole instruction cache */
		riscv_fence_i();
#ifdef SMP
		/* fence.i does not broadcast to other harts, send an IPI instead */
		cpu_mask = bsp_cpu_online_mask;
		cpu_mask &= ~(1ul << current_cpu_id());
		hart_mask = RISCV_HART_REMAP_K2P_MASK(cpu_mask);
		sbi_remote_fence_i(&hart_mask);
#endif
		err = EOK;
		break;

	case CACHE_OP_FLUSH_DCACHE_RANGE:
	case CACHE_OP_CLEAN_DCACHE_RANGE:
	case CACHE_OP_INVAL_DCACHE_RANGE:
	case CACHE_OP_FLUSH_DCACHE_ALL:
		/* FIXME -- data cache handling not implemented! */
		err = EOK;
		break;

	default:
		err = ENOSYS;
		break;
	}

	return err;
}
