/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * preboot64.c
 *
 * Preboot code -- setup kernel page tables at boot time.
 *
 * NOTE: This code runs in physical addressing mode! Do not call from C code!
 *
 * azuepke, 2021-05-05: initial
 * azuepke, 2023-08-01: large mappings
 * azuepke, 2023-09-03: adapted for RISC-V
 */

#include <marron/mapping.h>
#include <adspace_types.h>
#include <part_types.h>
#include <memrq_types.h>
#include <marron/compiler.h>
#include <riscv_pte.h>
#include <marron/arch_defs.h>
#include <board.h>
#include <preboot.h>


#ifdef __LP64__
#define PTE_NUM 512
#define BLOCK_SIZE 0x200000
#else
#define PTE_NUM 1024
#define BLOCK_SIZE 0x400000
#endif

/* kernel page tables */
#define KERN_PT (PTE_V)

/* kernel mappings: read-exec, read-only, read-write, unmapped */
#define KERN_RX (PTE_V | PTE_R |   0   | PTE_X | PTE_A |   0   )
#define KERN_RO (PTE_V | PTE_R |   0   |   0   | PTE_A |   0   )
#define KERN_RW (PTE_V | PTE_R | PTE_W |   0   | PTE_A | PTE_D )

/* no cache attributes here */
#define CACHED 0
#define UNCACHED 0


/* get the address of a symbol via PC relative addressing */
#define pcrel(sym) ({	\
		void *_addr;	\
		__asm__ (	\
			"la		%0, %1\n"	\
			: "=r"(_addr) : "S"(&sym));	\
		_addr;	\
	})

/* get the address of a symbol via absolute addressing */
#ifdef __LP64__
#define absolute(sym) ({	\
		void *_addr;	\
		__asm__ (	\
			"1:	auipc	%0, %%pcrel_hi(2f)\n"	\
			"	ld		%0, %%pcrel_lo(1b)(%0)\n"	\
			".pushsection .rodata\n"	\
			"	.balign 8\n"	\
			"2:	.quad " #sym "\n"	\
			".popsection\n"	\
			: "=r"(_addr) : "S"(&sym)); \
		_addr;	\
	})
#else
#define absolute(sym) ({	\
		void *_addr;	\
		__asm__ (	\
			"1:	auipc	%0, %%pcrel_hi(2f)\n"	\
			"	lw		%0, %%pcrel_lo(1b)(%0)\n"	\
			".pushsection .rodata\n"	\
			"	.balign 4\n"	\
			"2:	.word " #sym "\n"	\
			".popsection\n"	\
			: "=r"(_addr) : "S"(&sym)); \
		_addr;	\
	})
#endif

/* PA() and VA() refer to physical and virtual address of a symbol */
#define PA(sym) ((unsigned long)pcrel(sym))
#define VA(sym) ((addr_t)absolute(sym))


/* exported */
unsigned int bsp_preboot_ok;

/* imported */
extern char __text_start[];
extern char __text_end[];
extern char __rodata_start[];
extern char __rodata_end[];
extern char __data_start[];
extern char __data_end[];
extern char __bss_start[];
extern char __bss_end[];


/* setup a specific mapping */
__init static riscv_pte_t *preboot_map_range(
	riscv_pte_t *free_pts,
	riscv_pte_t *l1,
	addr_t addr,
	addr_t end,
	unsigned long phys,
	unsigned long flags)
{
#ifdef __LP64__
	riscv_pte_t l1_pte;
#endif
	riscv_pte_t *l2;
	riscv_pte_t l2_pte;
	riscv_pte_t *l3;

	if (((addr | end | phys) & (BLOCK_SIZE - 1)) == 0) {
		/* use a large mapping */
		while (addr < end) {
			/* get L2 table */
#ifdef __LP64__
			l1_pte = l1[riscv_addr_to_l1(addr)];
			if (l1_pte != 0) {
				l2 = (riscv_pte_t *)(unsigned long)PTE_TO_PHYS(l1_pte);
			} else {
				l2 = free_pts;
				free_pts += PTE_NUM;
				l1[riscv_addr_to_l1(addr)] = PHYS_TO_PTE((unsigned long)l2) | KERN_PT;
			}
#else
			l2 = l1;
#endif

			l2[riscv_addr_to_l2(addr)] = PHYS_TO_PTE(phys) | flags;

			addr += BLOCK_SIZE;
			phys += BLOCK_SIZE;
		}

		return free_pts;
	}

	while (addr < end) {
		/* get L2 table */
#ifdef __LP64__
		l1_pte = l1[riscv_addr_to_l1(addr)];
		if (l1_pte != 0) {
			l2 = (riscv_pte_t *)(unsigned long)PTE_TO_PHYS(l1_pte);
		} else {
			l2 = free_pts;
			free_pts += PTE_NUM;
			l1[riscv_addr_to_l1(addr)] = PHYS_TO_PTE((unsigned long)l2) | KERN_PT;
		}
#else
		l2 = l1;
#endif

		/* get L3 table */
		l2_pte = l2[riscv_addr_to_l2(addr)];
		if (l2_pte != 0) {
			l3 = (riscv_pte_t *)(unsigned long)PTE_TO_PHYS(l2_pte);
		} else {
			l3 = free_pts;
			free_pts += PTE_NUM;
			l2[riscv_addr_to_l2(addr)] = PHYS_TO_PTE((unsigned long)l3) | KERN_PT;
		}

		l3[riscv_addr_to_l3(addr)] = PHYS_TO_PTE(phys) | flags;

		addr += PAGE_SIZE;
		phys += PAGE_SIZE;
	}

	return free_pts;
}


/* setup kernel page tables */
/* NOTE: this functions executes in physical addressing mode,
 * so we cannot use the normal C way to get symbol addresses.
 * The PA() and VA() macros use explicit addressing modes to retrieve
 * virtual and physical addresses of certain symbols in a safe way.
 */
__init void preboot_setup_tagetables(
	riscv_pte_t *ttbr0_l1,
	unsigned long load_addr)
{
	const struct part_cfg *part_cfg_kern;
	riscv_pte_t *free_pts, *free_pts_end;
	unsigned int *preboot_ok_p;
	riscv_pte_t *l2;
#ifdef __LP64__
	riscv_pte_t l1_pte;
#endif
	riscv_pte_t *last_l3;
	addr_t addr, end;
	unsigned long phys;
	unsigned long flags;

	/* get pointer to pool of free page tables */
	part_cfg_kern = (const struct part_cfg *)PA(part_cfg);
	free_pts = part_cfg_kern->kmem_pagetables_mem;
	free_pts = (riscv_pte_t *)BOARD_KERNEL_TO_PHYS((unsigned long)free_pts);
	free_pts_end = free_pts + part_cfg_kern->kmem_pagetables_num * PTE_NUM;

	load_addr &= ~(PAGE_SIZE - 1);

	/* setup one page for the identity mapping */
	addr  = load_addr;
	end   = load_addr + PAGE_SIZE;
	phys  = load_addr;
	flags = KERN_RX | CACHED;
#ifndef NDEBUG
	/* debug kernel: RWX so GDB can patch breakpoints into the kernel code */
	flags |= KERN_RW;
#endif
	free_pts = preboot_map_range(free_pts, ttbr0_l1, addr, end, phys, flags);

	/* setup kernel adspace -- everything fits into one L2 table */
	/* map various kernel segments */
	addr  = VA(__text_start);
	end   = VA(__text_end);
	phys  = PA(__text_start);
	flags = KERN_RX | CACHED | PTE_G;
#ifndef NDEBUG
	/* debug kernel: RWX so GDB can patch breakpoints into the kernel code */
	flags |= KERN_RW;
#endif
	free_pts = preboot_map_range(free_pts, ttbr0_l1, addr, end, phys, flags);

	addr  = VA(__rodata_start);
	end   = VA(__rodata_end);
	phys  = PA(__rodata_start);
	flags = KERN_RO | CACHED | PTE_G;
	free_pts = preboot_map_range(free_pts, ttbr0_l1, addr, end, phys, flags);

	addr  = VA(__data_start);
	end   = VA(__bss_end);
	phys  = PA(__data_start);
	flags = KERN_RW | CACHED | PTE_G;
	free_pts = preboot_map_range(free_pts, ttbr0_l1, addr, end, phys, flags);

	/* setup I/O */
	for (unsigned int i = 0; i < part_cfg_kern->io_map_num; i++) {
		const struct memrq_cfg *m = &((const struct memrq_cfg *)PA(io_map_cfg))[i];

		addr = m->addr;
		end = addr + m->size;
		phys = m->phys;
		flags = KERN_RO;
		if ((m->prot & PROT_EXEC) != 0) {
			flags |= PTE_X;
		}
		if ((m->prot & PROT_WRITE) != 0) {
			flags |= PTE_W | PTE_D;
		}
#if 0
		if (m->cache == 7) {
			flags |= CACHED;
		} else {
			flags |= UNCACHED;
		}
#endif
		free_pts = preboot_map_range(free_pts, ttbr0_l1, addr, end, phys, flags);
	}

	/* register the special last pagetable */
	last_l3 = (riscv_pte_t *)PA(arch_adspace_xmap_pagetable);
#ifdef __LP64__
	l1_pte = ttbr0_l1[riscv_addr_to_l1(ARCH_KERNEL_BASE)];
	l2 = (riscv_pte_t *)(unsigned long)PTE_TO_PHYS(l1_pte);
#else
	l2 = ttbr0_l1;
#endif
	l2[riscv_addr_to_l2(ARCH_PERCPU_BASE)] = PHYS_TO_PTE((unsigned long)last_l3) | KERN_PT;

	/* report our state to bsp.c */
	preboot_ok_p = (unsigned int *)PA(bsp_preboot_ok);
	*preboot_ok_p = (free_pts <= free_pts_end);
}

/* initialize first 64-bytes of all shared memory segments to zero */
/* NOTE: this functions executes in physical addressing mode,
 * so we cannot use the normal C way to get symbol addresses.
 * The PA() and VA() macros use explicit addressing modes to retrieve
 * virtual and physical addresses of certain symbols in a safe way.
 */
__init void preboot_init_shms(void)
{
	const struct memrq_cfg *m;
	unsigned long *ptr;
	unsigned int num;

	num = *(const typeof(shm_map_num) *)PA(shm_map_num);
	for (unsigned int i = 0; i < num; i++) {
		m = &((const struct memrq_cfg *)PA(shm_map_cfg))[i];

#ifdef __LP64__
		ptr = (unsigned long *)m->phys;
#else
		ptr = (unsigned long *)m->phys_lo;
#endif

		for (unsigned int j = 0; j < 64 / sizeof(*ptr); j++) {
			ptr[j] = 0;
		}
	}
}
