/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021, 2023 Alexander Zuepke */
/*
 * bsp.c
 *
 * Board specific setting for QEMU RISC-V "virt" board.
 *
 * azuepke, 2015-07-13: cloned from QEMU-ARM
 * azuepke, 2021-01-16: adapted to mini-OS
 * azuepke, 2023-09-03: adapted for RISC-V from qemu-aarch64
 * azuepke, 2023-09-09: multicore works
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <assert.h>
#include <panic.h>
#include <preboot.h>
#include <sbi.h>
#include <riscv_insn.h>
#include <riscv_csr.h>
#include <arch_bsp.h>
#include <current.h>
#include <sbi_timer.h>
#include <spin.h>
#include <plic.h>
#include <barrier.h>


/** BSP name string */
const char bsp_name[] = BOARD_NAME;

/* Number of CPUs exported by the BSP */
const unsigned int bsp_cpu_num = CFG_CPU_NUM;

#ifdef RISCV_HART_REMAP_TABLE
const unsigned char riscv_hart_remap_table[] = RISCV_HART_REMAP_TABLE;
#endif

#ifdef SMP
/* CPU that actually booted the kernel
 * set by start.S, can be any CPU
 * we ignore this value in UP mode
 */
unsigned int bsp_boot_cpu_id = -1;

/* CPUs online */
unsigned long bsp_cpu_online_mask = 0;

/* CPUs started scheduling */
unsigned int bsp_cpu_go = 0;

/* CPUs halt request */
unsigned int bsp_cpu_halt = 0;

unsigned int bsp_cpu_id(void)
{
	/* NOTE: will be initialized by the kernel before calling bsp_init() */
	return current_cpu_id();
}

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned int hart;
	unsigned int cpu;
	long err;

	assert(bsp_cpu_id() == bsp_boot_cpu_id);
	assert(bsp_cpu_online_mask == 0x0);
	(void)err;

	/* we flush the caches before doing anything, as the first instructions
	 * of a newly started processor runs in uncached memory mode.
	 */
	for (cpu = 0; cpu < bsp_cpu_num; cpu++) {
		if (cpu == bsp_boot_cpu_id) {
			continue;
		}

		hart = RISCV_HART_REMAP_K2P(cpu);
		(void)hart;

#ifdef QEMU_SMP_STARTUP
		barrier_set_val(&__boot_release, cpu);
#endif

#ifdef OPENSBI_SMP_STARTUP
		err = sbi_hart_start(hart, LOAD_ADDR, 0);
		if (err != 0) {
			panic("failed to start CPU %u hart %u, err %ld\n", cpu, hart, err);
		}
#endif

		/* wait until CPU is up */
		barrier_wait_bit(&bsp_cpu_online_mask, cpu);
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu_id)
{
	assert(cpu_id == bsp_cpu_id());
	barrier_set_bit(&bsp_cpu_online_mask, cpu_id);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu_id == bsp_boot_cpu_id) {
		barrier_set_val(&bsp_cpu_go, 1);
	} else {
		/* wait until we see the "go" signal */
		barrier_wait_val(&bsp_cpu_go, 1);
	}

	/* start timer here on SMP */
	sbi_timer_start();
}

void bsp_cpu_reschedule(unsigned int cpu)
{
	unsigned long hart_mask = RISCV_HART_REMAP_K2P_MASK(1ul << cpu);
	sbi_send_ipi(&hart_mask);
}

static inline void bsp_cpu_send_halt(void)
{
	unsigned long hart_mask = RISCV_HART_REMAP_K2P_MASK(bsp_cpu_online_mask);
	access_once(bsp_cpu_halt) = 1;
	sbi_send_ipi(&hart_mask);
}

#endif

void bsp_ipi_handler(void)
{
#ifdef SMP
	if (unlikely(access_once(bsp_cpu_halt) != 0)) {
		__bsp_halt();
		unreachable();
	}
#endif
}

/** initialize BSP */
__init void bsp_init(unsigned int cpu_id)
{
	unsigned int boot_cpu_id_cached;

#ifdef SMP
	assert(bsp_boot_cpu_id != -1u);
	boot_cpu_id_cached = bsp_boot_cpu_id;
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
	boot_cpu_id_cached = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == boot_cpu_id_cached) {
		serial_init(38400);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		plic_init_global();
		plic_init_local();

		sbi_timer_init(100);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		sbi_timer_start();
#endif
	}
#ifdef SMP
	else {
		plic_init_local();
	}
#endif
}

/** idle the CPU */
void bsp_idle(void)
{
	__asm__ volatile ("wfi" : : : "memory");
}

/** halt the board (shutdown of the system) */
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		bsp_cpu_send_halt();
	}
#endif

	if (mode == BOARD_PANIC) {
		sbi_system_reset(SBI_RESET_TYPE_SHUTDOWN, SBI_RESET_REASON_SYSTEM_FAILURE);
	}
	if (mode == BOARD_RESET) {
		sbi_system_reset(SBI_RESET_TYPE_COLD_REBOOT, SBI_RESET_REASON_NO_REASON);
	}
	if (mode == BOARD_POWEROFF) {
		sbi_system_reset(SBI_RESET_TYPE_SHUTDOWN, SBI_RESET_REASON_NO_REASON);
	}

	/* poweroff QEMU instance in any other case as well */
	if (mode != BOARD_STOP) {
		sbi_shutdown();
	}

	__bsp_halt();
	unreachable();
}

/* TLB and cache management callbacks */
void bsp_remote_sfence_vma(unsigned long cpu_mask, addr_t start, size_t size)
{
	unsigned long hart_mask = RISCV_HART_REMAP_K2P_MASK(cpu_mask);
	sbi_remote_sfence_vma(&hart_mask, start, size);
}

void bsp_remote_sfence_vma_asid(unsigned long cpu_mask, addr_t start, size_t size, unsigned long asid)
{
	unsigned long hart_mask = RISCV_HART_REMAP_K2P_MASK(cpu_mask);
	sbi_remote_sfence_vma_asid(&hart_mask, start, size, asid);
}

void bsp_remote_fence_i(unsigned long cpu_mask)
{
	unsigned long hart_mask = RISCV_HART_REMAP_K2P_MASK(cpu_mask);
	sbi_remote_fence_i(&hart_mask);
}
