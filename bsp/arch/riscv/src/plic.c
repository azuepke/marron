/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * plic.c
 *
 * RISC-V PLIC
 *
 * This code does not consider local interrupts, but we could dispatch
 * them as well and then shift all PLIC interrupts by 64.
 *
 * azuepke, 2023-09-09: initial PLIC code
 * azuepke, 2023-09-12: split from bsp.c
 */

#include <kernel.h>
#include <assert.h>
#include <bsp.h>
#include <marron/compiler.h>
#include <plic.h>
#include <current.h>
#include <board.h>
#include <spin.h>
#include <arch_io.h>
#include <riscv_csr.h>
#include <sbi_timer.h>
#include <panic.h>


/* PLIC specs
 *
 * up to 1024 IRQs, IRQ 0 not used
 *
 * 0x000000	priority (multiples of 0x4)
 * 0x001000	pending (single bits per interrupt)
 * 0x002000	enable (multiples of 0x80 for each CPU)
 * 0x200000	context (multiples of 0x1000)
 *       +0	priority threshold
 *       +4	claim/complete
 */
#define PLIC_PRIO_OFFSET		0x000000
#define PLIC_PENDING_OFFSET		0x001000
#ifndef PLIC_ENABLE_OFFSET
#define PLIC_ENABLE_OFFSET		0x002000
#endif
#ifndef PLIC_ENABLE_PER_CPU
#define PLIC_ENABLE_PER_CPU		0x80
#endif
#ifndef PLIC_CONTEXT_OFFSET
#define PLIC_CONTEXT_OFFSET		0x200000
#endif
#ifndef PLIC_CONTEXT_PER_CPU
#define PLIC_CONTEXT_PER_CPU	0x1000
#endif
#define PLIC_THRESHOLD_OFFSET	(PLIC_CONTEXT_OFFSET + 0)
#define PLIC_CLAIM_OFFSET		(PLIC_CONTEXT_OFFSET + 4)


/* Number of IRQs exported by the BSP */
const unsigned int bsp_irq_num = CFG_IRQ_NUM;


/* internal lock */
static spin_t plic_lock;


err_t bsp_irq_config(unsigned int irq __unused, unsigned int mode __unused)
{
	assert(irq > 0 && irq < bsp_irq_num);
	return EOK;
}

void bsp_irq_enable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int word = irq / 32;
	unsigned int bit = irq % 32;
	unsigned int val;
	volatile void *addr;

	spin_lock(&plic_lock);

	addr = io_ptr(CFG_IO_ADDR_plic, PLIC_ENABLE_OFFSET + cpu_id * PLIC_ENABLE_PER_CPU + word * 4);
	val = read32(addr);
	val |= 1u << bit;
	write32(addr, val);

	spin_unlock(&plic_lock);
}

void bsp_irq_disable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int word = irq / 32;
	unsigned int bit = irq % 32;
	unsigned int val;
	volatile void *addr;

	spin_lock(&plic_lock);

	addr = io_ptr(CFG_IO_ADDR_plic, PLIC_ENABLE_OFFSET + cpu_id * PLIC_ENABLE_PER_CPU + word * 4);
	val = read32(addr);
	val &= ~(1u << bit);
	write32(addr, val);

	spin_unlock(&plic_lock);
}

void plic_init_global(void)
{
	unsigned int cached_cpu_num;
	unsigned int cpu_id;
	unsigned int irq;
	volatile void *addr;

	spin_init(&plic_lock);
	spin_lock(&plic_lock);
	spin_unlock(&plic_lock);

	/* Disable all global interrupts for current hart
	 * (at each PLIC's address we have 32 interrupts)
	 */
	cpu_id = current_cpu_id();
	for (irq = 0; irq < bsp_irq_num / 32; irq++) {
		addr = io_ptr(CFG_IO_ADDR_plic, PLIC_ENABLE_OFFSET + cpu_id * PLIC_ENABLE_PER_CPU + irq * 4);
		write32(addr, 0);
	}

	for (irq = 0; irq < bsp_irq_num; irq++) {
		/* priority 0 disables the interrupt, so we use 1 as minimum priority */
		addr = io_ptr(CFG_IO_ADDR_plic, PLIC_PRIO_OFFSET + irq * 4);
		write32(addr, 1);
	}

#ifdef SMP
	cached_cpu_num = bsp_cpu_num;
#else
	cached_cpu_num = 1;
#endif
	for (cpu_id = 0; cpu_id < cached_cpu_num; cpu_id++) {
		addr = io_ptr(CFG_IO_ADDR_plic, PLIC_THRESHOLD_OFFSET + cpu_id * PLIC_CONTEXT_PER_CPU);
		/* minimum priority is 0 as the PLIC masks all interrupts
		 * with an irq_priority <= threshold
		 */
		write32(addr, 0);
	}
}

void plic_init_local(void)
{
	/* enable interrupts (becomes active when in user mode) */
	__asm__ volatile ("csrw sie, %0" : : "r" (SIE_SEIE | SIE_STIE | SIE_SSIE) : "memory");
}

static void plic_dispatch(void)
{
	unsigned int cpu_id = current_cpu_id();
	unsigned int irq;
	volatile void *addr;

	addr = io_ptr(CFG_IO_ADDR_plic, PLIC_CLAIM_OFFSET + cpu_id * PLIC_CONTEXT_PER_CPU);

	irq = read32(addr);
	if (irq > 0) {
		write32(addr, irq);
		kernel_irq(irq);
	}
}

/** dispatch IRQ: mask and ack, call handler */
static void bsp_irq_dispatch_internal(unsigned long pending_mask)
{
	if ((pending_mask & SIP_SSIP) != 0) {
		/* software interrupt */
		/* clear pending local interrupt */
		__asm__ volatile ("csrc sip, %0" : : "r" (SIP_SSIP) : "memory");
		/* the IPI handlers triggers rescheduling when leaving the kernel */
		bsp_ipi_handler();
	}

	if ((pending_mask & SIP_STIP) != 0) {
		/* timer */
		/* pending local interrupt is cleared when timer is reprogrammed */
		sbi_timer_int();
	}

	if ((pending_mask & SIP_SEIP) != 0) {
		/* external */
		/* pending local interrupt is cleared when source is masked in PLIC */
		plic_dispatch();
	}

	if ((pending_mask & ~(SIP_SSIP | SIP_STIP | SIP_SEIP)) != 0) {
		panic("unknown interrupts pendings 0x%lu\n", pending_mask);
		assert(0);
	}
}

/** dispatch current IRQ (called from architecture layer) */
void bsp_irq_dispatch(ulong_t vector)
{
	assert(vector < __riscv_xlen);
	bsp_irq_dispatch_internal(1ul << vector);
}

/** dispatch any pending IRQ (called from scheduler) */
void bsp_irq_poll(void)
{
	unsigned long pending_mask;
	unsigned long sip, sie;

	__asm__ volatile ("csrr %0, sip" : "=r" (sip));
	__asm__ volatile ("csrr %0, sie" : "=r" (sie));

	/* SIP indicates any interrupt pending to the hart, so filter by SIE */
	pending_mask = (sip & sie);

	if (pending_mask != 0) {
		bsp_irq_dispatch_internal(pending_mask);
	}
}
