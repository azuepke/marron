/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * sbi_timer.c
 *
 * RISC-V SBI-based per-CPU timer
 *
 * azuepke, 2017-04-06: initial RISC-V BSP
 * azuepke, 2023-09-03: imported & adapted
 */

#include <kernel.h>
#include <assert.h>
#include <bsp.h>
#include <marron/compiler.h>
#include <sbi_timer.h>
#include <current.h>
#include <board.h>
#include <sbi.h>
#include <riscv_insn.h>


/* FIXME: the jiffies will overflow in 49 days at 1000 HZ! */
static uint64_t jiffies[CFG_CPU_NUM];
static uint32_t clock_ns;
static uint64_t timer_reload;
static uint64_t timer_last[CFG_CPU_NUM];

// Timer resolution in nanoseconds
uint64_t bsp_timer_resolution;

uint64_t bsp_timer_get_time(void)
{
	unsigned int cpu_id = current_cpu_id();

	return (uint64_t)jiffies[cpu_id] * clock_ns;
}

// set next timer expiry (oneshot)
void bsp_timer_set_expiry(uint64_t expiry __unused, unsigned int cpu_id __unused)
{
	/* ignored in periodic mode */
}

void sbi_timer_int(void)
{
	unsigned int cpu_id = current_cpu_id();

	jiffies[cpu_id]++;
	timer_last[cpu_id] += timer_reload;

	sbi_set_timer(timer_last[cpu_id] + timer_reload);

	/* notify kernel to expiry timeouts */
	kernel_timer((uint64_t)jiffies[cpu_id] * clock_ns);
}

__init void sbi_timer_init(unsigned int freq)
{
	uint64_t now;

	timer_reload = SBI_TIMER_FREQ / freq;
	clock_ns = 1000000000 / freq;
	bsp_timer_resolution = clock_ns;

	now = riscv_rdtime();
	for (unsigned int i = 0; i < CFG_CPU_NUM; i++) {
		timer_last[i] = now;
	}
}

__init void sbi_timer_start(void)
{
	unsigned int cpu_id = current_cpu_id();

	sbi_set_timer(timer_last[cpu_id] + timer_reload);
}
