/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * preboot.c
 *
 * Preboot code -- setup kernel page tables at boot time.
 *
 * NOTE: This code runs in physical addressing mode! Do not call from C code!
 *
 * azuepke, 2021-05-05: initial
 * azuepke, 2023-08-01: large mappings
 */

#include <marron/mapping.h>
#include <part_types.h>
#include <adspace_types.h>
#include <memrq_types.h>
#include <marron/compiler.h>
#include <arm_pte_v6.h>
#include <marron/arch_defs.h>
#include <board.h>
#include <preboot.h>


/* kernel page tables */
#define KERN_PT (PDE_TYPE_PT)

/* kernel mappings: read-exec, read-only, read-write, unmapped */
#define KERN_RX (PTE_P | PTE_AP0 | PTE_AP2 |    0  )
#define KERN_RO (PTE_P | PTE_AP0 | PTE_AP2 | PTE_XN)
#define KERN_RW (PTE_P | PTE_AP0 |    0    | PTE_XN)

/* cache attributes */
#ifdef ARM_CORTEX_A8
/* MAP_WB cached memory with default attributes */
#define CACHED (PTE_C | PTE_B)
#else
/* MAP_WA cached memory with default attributes */
#define CACHED (PTE_TEX0 | PTE_C | PTE_B | PTE_S)
#endif
/* MAP_UC uncached strongly-ordered, no exec permissions */
#define UNCACHED (PTE_XN)
/* MAP_DEV uncached device, no exec permission */
#define DEVICE (PTE_B | PTE_XN)


/* get the address of a symbol via absolute addressing */
#define absolute(sym) ({	\
		void *_addr;	\
		__asm__ (	\
			"ldr	%0, =" __stringify(sym) "\n"	\
			: "=r"(_addr));	\
		_addr;	\
	})

/* PA() and VA() refer to physical and virtual address of a symbol */
#define PA(sym) (BOARD_KERNEL_TO_PHYS((unsigned long)absolute(sym)))
#define VA(sym) ((addr_t)absolute(sym))


/* exported */
unsigned int bsp_preboot_ok;

/* imported */
extern char __text_start[];
extern char __text_end[];
extern char __rodata_start[];
extern char __rodata_end[];
extern char __data_start[];
extern char __data_end[];
extern char __bss_start[];
extern char __bss_end[];


/* setup a specific mapping */
__init static arm_pte_t *preboot_map_range(
	arm_pte_t *free_pts,
	arm_pte_t *pd,
	addr_t addr,
	addr_t end,
	unsigned long phys,
	unsigned long flags)
{
	arm_pte_t pde;
	arm_pte_t *pt;

	if (((addr | end | phys) & (SECTION_SIZE - 1)) == 0) {
		/* use a large mapping */
		flags = arm_page_to_sect(flags);

		while (addr < end) {
			/* get page table */
			pd[arm_addr_to_pde(addr)] = PHYS_TO_PTE(phys) | flags;

			addr += SECTION_SIZE;
			phys += SECTION_SIZE;
		}

		return free_pts;
	}

	while (addr < end) {
		/* get page table */
		pde = pd[arm_addr_to_pde(addr)];
		if (pde != 0) {
			pt = (arm_pte_t *)PDE_TO_PHYS(pde);
		} else {
			pt = free_pts;
			free_pts += PTE_NUM;
			pd[arm_addr_to_pde(addr)] = PHYS_TO_PDE(pt) | KERN_PT;
		}

		pt[arm_addr_to_pte(addr)] = PHYS_TO_PTE(phys) | flags;

		addr += PAGE_SIZE;
		phys += PAGE_SIZE;
	}

	return free_pts;
}


/* setup kernel page tables */
/* NOTE: this functions executes in physical addressing mode,
 * so we cannot use the normal C way to get symbol addresses.
 * The PA() and VA() macros use explicit addressing modes to retrieve
 * virtual and physical addresses of certain symbols in a safe way.
 */
__init void preboot_setup_tagetables(
	arm_pte_t *pd,
	unsigned long load_addr)
{
	const struct part_cfg *part_cfg_kern;
	arm_pte_t *free_pts, *free_pts_end;
	unsigned int *preboot_ok_p;
	arm_pte_t *pt;
	addr_t addr, end;
	unsigned long phys;
	unsigned long flags;

	/* get pointer to pool of free page tables */
	part_cfg_kern = (const struct part_cfg *)PA(part_cfg);
	free_pts = part_cfg_kern->kmem_pagetables_mem;
	free_pts = (arm_pte_t *)BOARD_KERNEL_TO_PHYS((unsigned long)free_pts);
	free_pts_end = free_pts + part_cfg_kern->kmem_pagetables_num * PTE_NUM;

	load_addr &= ~(PAGE_SIZE - 1);

	/* setup identity mapping -- just one page of the identity mapping */
	addr  = load_addr;
	end   = load_addr + PAGE_SIZE;
	phys  = load_addr;
	flags = KERN_RX | CACHED | PTE_NG;
	free_pts = preboot_map_range(free_pts, pd, addr, end, phys, flags);

	/* setup kernel address space */
	/* map various kernel segments */
	addr  = VA(__text_start);
	end   = VA(__text_end);
	phys  = PA(__text_start);
	flags = KERN_RX | CACHED;
	free_pts = preboot_map_range(free_pts, pd, addr, end, phys, flags);

	addr  = VA(__rodata_start);
	end   = VA(__rodata_end);
	phys  = PA(__rodata_start);
	flags = KERN_RO | CACHED;
	free_pts = preboot_map_range(free_pts, pd, addr, end, phys, flags);

	addr  = VA(__data_start);
	end   = VA(__bss_end);
	phys  = PA(__data_start);
	flags = KERN_RW | CACHED;
	free_pts = preboot_map_range(free_pts, pd, addr, end, phys, flags);

	/* setup I/O */
	for (unsigned int i = 0; i < part_cfg_kern->io_map_num; i++) {
		const struct memrq_cfg *m = &((const struct memrq_cfg *)PA(io_map_cfg))[i];

		addr = m->addr;
		end = addr + m->size;
		phys = m->phys;
		flags = KERN_RO;
		if ((m->prot & PROT_EXEC) != 0) {
			flags &= ~PTE_XN;
		}
		if ((m->prot & PROT_WRITE) != 0) {
			flags &= ~PTE_AP2;
		}
		if (m->cache == 7) {
			flags |= CACHED;
		} else if (m->cache == 1) {
			flags |= DEVICE;
		} else {
			flags |= UNCACHED;
		}
		free_pts = preboot_map_range(free_pts, pd, addr, end, phys, flags);
	}

	/* register the special last pagetable */
	pt = (arm_pte_t *)PA(arch_adspace_xmap_pagetable);
	pd[arm_addr_to_pde(ARCH_PERCPU_BASE)] = PHYS_TO_PDE(pt) | KERN_PT;

	/* report our state to bsp.c */
	preboot_ok_p = (unsigned int *)PA(bsp_preboot_ok);
	*preboot_ok_p = (free_pts <= free_pts_end);
}

/* initialize first 64-bytes of all shared memory segments to zero */
/* NOTE: this functions executes in physical addressing mode,
 * so we cannot use the normal C way to get symbol addresses.
 * The PA() and VA() macros use explicit addressing modes to retrieve
 * virtual and physical addresses of certain symbols in a safe way.
 */
__init void preboot_init_shms(void)
{
	const struct memrq_cfg *m;
	unsigned long *ptr;
	unsigned int num;

	num = *(const typeof(shm_map_num) *)PA(shm_map_num);
	for (unsigned int i = 0; i < num; i++) {
		m = &((const struct memrq_cfg *)PA(shm_map_cfg))[i];

		ptr = (unsigned long *)m->phys_lo;

		for (unsigned int j = 0; j < 64 / sizeof(*ptr); j++) {
			ptr[j] = 0;
		}
	}
}
