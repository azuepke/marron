/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2020, 2021-2022 Alexander Zuepke */
/*
 * cpu_timer.c
 *
 * ARM generic per-CPU virtual timer (for Cortex-A15 or newer)
 *
 * azuepke, 2015-07-24: initial
 * azuepke, 2018-03-05: imported
 * azuepke, 2020-11-22: cleanup
 * azuepke, 2021-01-16: add 64-bit support
 * azuepke, 2022-02-10: high-resolution periodic timer
 */

#include <kernel.h>
#include <assert.h>
#include <bsp.h>
#include <marron/compiler.h>
#include <cpu_timer.h>
#include <current.h>
#include <marron/arch_defs.h>
#include <board.h>
#include <arch.h>	/* for udiv64_32() on 32-bit systems */


/* override default interrupt */
#ifndef CPU_TIMER_INTERRUPT
#include <gic.h>
#define CPU_TIMER_INTERRUPT	IRQ_ID_VTIMER
#endif


#define CTL_ENABLE			0x001	/* timer enabled */
#define CTL_IMASK			0x002	/* interrupt masked */
#define CTL_ISTATUS			0x004	/* interrupt pending */

/** Set counter commpare */
static inline void arm_set_cnt_compare(unsigned long long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr CNTV_CVAL_EL0, %0" : : "r"(val) : "memory");
#else /* 32-bit ARM */
	__asm__ volatile ("mcrr p15, 3, %Q0, %R0 , c14" : : "r" (val) : "memory");
#endif
}

/** Get counter control */
static inline unsigned long arm_get_cnt_control(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, CNTV_CTL_EL0" : "=r"(val));
#else /* 32-bit ARM */
	__asm__ volatile ("mrc p15, 0, %0, c14, c3, 1" : "=r"(val));
#endif
	return val;
}

/** Set counter control */
static inline void arm_set_cnt_control(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr CNTV_CTL_EL0, %0" : : "r"(val) : "memory");
#else /* 32-bit ARM */
	__asm__ volatile ("mcr p15, 0, %0, c14, c3, 1" : : "r"(val) : "memory");
#endif
}

/** Get counter frequency (user accessible) */
static inline unsigned long arm_get_cnt_freq(void)
{
	/* some SOCs not support get Counter Frequency! */
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, CNTFRQ_EL0" : "=r"(val));
#else /* 32-bit ARM */
	__asm__ volatile ("mrc p15, 0, %0, c14, c0, 0" : "=r"(val));
#endif
	return val;
}

/** Get virtual counter value (user accessible) */
static inline unsigned long long arm_get_cnt_val(void)
{
	unsigned long long val;
	/* NOTE: the ISB prevents prefetching */
#ifdef __aarch64__
	__asm__ volatile ("isb; mrs %0, CNTVCT_EL0" : "=r"(val));
#else /* 32-bit ARM */
	__asm__ volatile ("isb; mrrc p15, 1, %Q0, %R0, c14" : "=r" (val));
#endif
	return val;
}

/** Per CPU timer state
 *
 * We track the last/next expiry in nanosecond and in native timer resolution.
 * This allows us to handle most conversion between the two worlds
 * in 32-bit arithmetics based on deltas to previously calculated 64-bit values.
 *
 * Because 64-bit division is slow (or event not available) on 32-bit system,
 * we multiply with a constant factor and shift right instead of using
 * a multiple and divide sequence for conversion.
 * Factors and shift amounts are calculated once at boot time.
 */
struct per_cpu_timer {
	/* last expiry in nanoseconds and timer ticks */
	uint64_t last_expiry_ns;
	uint64_t last_expiry_tt;
	/* next expiry in nanoseconds and timer ticks */
	uint64_t next_expiry_ns;	/* NOTE: currently not used */
	uint64_t next_expiry_tt;

	/* we copy the data over to each per-CPU data (less cachelines to access) */

	/* periodic timer tick time in timer ticks */
	uint32_t tick_tt;
	/* periodic timer tick time in nanoseconds */
	uint32_t tick_ns;

	/* conversion ns to tt: factor */
	uint32_t ns_to_tt_factor;
	/* conversion ns to tt: right-shift */
	uint32_t ns_to_tt_shift;

	/* conversion tt to ns: factor */
	uint32_t tt_to_ns_factor;
	/* conversion tt to ns: right-shift */
	uint32_t tt_to_ns_shift;

	/* unused padding */
	uint64_t padding;
} __aligned(ARCH_ALIGN);

static struct per_cpu_timer timer_state[BOARD_CPU_NUM];

/** Timer resolution in nanoseconds (exported to kernel) */
uint64_t bsp_timer_resolution;

uint64_t bsp_timer_get_time(void)
{
	unsigned int cpu_id = current_cpu_id();
	struct per_cpu_timer *t = &timer_state[cpu_id];
	uint64_t now_tt;
	uint32_t delta_tt;
	uint32_t delta_ns;

	now_tt = arm_get_cnt_val();
	assert(now_tt >= t->last_expiry_tt);
	assert(now_tt - t->last_expiry_tt <= 0xffffffffu);
	delta_tt = now_tt - t->last_expiry_tt;

#if 0
	/* native 64-bit division available */
	delta_ns = ((uint64_t)delta_tt * t->tick_ns) / t->tick_tt;
#else
	/* multiplication with a constant factor */
	delta_ns = ((uint64_t)delta_tt * t->tt_to_ns_factor) >> t->tt_to_ns_shift;
#endif

	assert(delta_ns < 1000000000);

	return t->last_expiry_ns + delta_ns;
}

/** set next timer expiry (oneshot) */
void bsp_timer_set_expiry(uint64_t expiry __unused, unsigned int cpu_id __unused)
{
	/* ignored in periodic mode */
}

/** timer interrupt handler */
static void timer_handler(unsigned int irq __unused)
{
	unsigned int cpu_id = current_cpu_id();
	struct per_cpu_timer *t = &timer_state[cpu_id];

	t->last_expiry_tt = t->next_expiry_tt;
	t->next_expiry_tt += t->tick_tt;
	arm_set_cnt_compare(t->next_expiry_tt);

	t->last_expiry_ns += t->tick_ns;

	/* notify kernel to expiry timeouts */
	kernel_timer(t->last_expiry_ns);
}

/** factor and shift for fix-point arithmetic used in conversion */
static inline void factor_and_shift(uint32_t a, uint32_t b, uint32_t *f, uint32_t *s)
{
	uint64_t factor64;
	uint32_t shift;

	assert(a > 0);
	assert(b > 0);

	/* We use an x.y fixed-point format that fits onto 32 bits.
	 * For this, we have to determine the right shift factor.
	 */
	if (a < b) {
		/* max out fraction bits */
		shift = 31;
	} else {
		/* determine right amount of integer bits */
		uint32_t small = b;
		uint32_t large = a;
		uint32_t steps = 0;

		while (large >= small) {
			large >>= 1;
			steps++;
		}
		assert(steps <= 31);
		shift = 32 - steps;
	}
	*s = shift;

	factor64 = (uint64_t)a * (1u << shift);
#if __SIZEOF_LONG__ == 8
	/* native 64-bit division available */
	*f = factor64 / b;
#else
	/* fallback to software routine for division */
	*f = udiv64_32(factor64, b);
#endif
}

/** timer implementation -- uses ARM per core virtual timer */
__init void cpu_timer_init(unsigned int freq)
{
	uint32_t clock_tt;
	uint32_t clock_ns;
	uint32_t tick_tt;
	uint32_t tick_ns;
	uint32_t ns_to_tt_factor;
	uint32_t ns_to_tt_shift;
	uint32_t tt_to_ns_factor;
	uint32_t tt_to_ns_shift;

#ifdef CPU_COUNTER_FREQ
	clock_tt = CPU_COUNTER_FREQ;
#else
	clock_tt = arm_get_cnt_freq();
#endif
	clock_ns = 1000000000;

	tick_tt = clock_tt / freq;
	tick_ns = clock_ns / freq;
	bsp_timer_resolution = tick_ns;

	factor_and_shift(clock_tt, clock_ns, &ns_to_tt_factor, &ns_to_tt_shift);
	factor_and_shift(clock_ns, clock_tt, &tt_to_ns_factor, &tt_to_ns_shift);

	for (unsigned int i = 0; i < countof(timer_state); i++) {
		struct per_cpu_timer *t = &timer_state[i];

		t->tick_tt = tick_tt;
		t->tick_ns = tick_ns;
		t->ns_to_tt_factor = ns_to_tt_factor;
		t->ns_to_tt_shift = ns_to_tt_shift;
		t->tt_to_ns_factor = tt_to_ns_factor;
		t->tt_to_ns_shift = tt_to_ns_shift;
	}

	/* install handler */
	bsp_irq_register(CPU_TIMER_INTERRUPT, timer_handler);
}


/** timer implementation -- uses ARM per core virtual timer */
__init void cpu_timer_start(void)
{
	unsigned int cpu_id = current_cpu_id();
	struct per_cpu_timer *t = &timer_state[cpu_id];

	/* unmask interrupt in GIC */
	bsp_irq_enable(CPU_TIMER_INTERRUPT, cpu_id);

	/* all cores call this function at the same time,
	 * so we define this as "time 0", our origin in time.
	 */
	t->last_expiry_tt = arm_get_cnt_val();
	t->next_expiry_tt = t->last_expiry_tt + t->tick_tt;
	arm_set_cnt_compare(t->next_expiry_tt);

	/* enable timer interrupt, unmasked */
	arm_set_cnt_control(CTL_ENABLE);
}
