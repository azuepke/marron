/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * preboot64.c
 *
 * Preboot code -- setup kernel page tables at boot time.
 *
 * NOTE: This code runs in physical addressing mode! Do not call from C code!
 *
 * azuepke, 2021-05-05: initial
 * azuepke, 2023-08-01: large mappings
 */

#include <marron/mapping.h>
#include <adspace_types.h>
#include <part_types.h>
#include <memrq_types.h>
#include <marron/compiler.h>
#include <arm_pte_v8.h>
#include <marron/arch_defs.h>
#include <marron/cache_type.h>
#include <board.h>
#include <preboot.h>


/* number of elements in a page table */
#define PTE_NUM 512


/* kernel page tables: user space cannot access */
#define KERN_PT (PTE_TYPE_PT | PTE_PT_AP1 | PTE_PT_XN)

/* kernel mappings: read-exec, read-only, read-write, unmapped */
#define KERN_RX (PTE_TYPE_PAGE | PTE_AF | PTE_AP2 | PTE_XN |    0   )
#define KERN_RO (PTE_TYPE_PAGE | PTE_AF | PTE_AP2 | PTE_XN | PTE_PXN)
#define KERN_RW (PTE_TYPE_PAGE | PTE_AF |    0    | PTE_XN | PTE_PXN)

/* cache attributes */
/* MAP_WA cached memory with default attributes */
#define CACHED (PTE_ATTR2 | PTE_ATTR1 | PTE_ATTR0 | PTE_SH1 | PTE_SH0)


/* get the address of a symbol via PC relative addressing */
#define pcrel(sym) ({	\
		void *_addr;	\
		__asm__ (	\
			"adrp	%0, %1\n"	\
			"add	%0, %0, :lo12:%1\n"	\
			: "=r"(_addr) : "S"(&sym));	\
		_addr;	\
	})

/* get the address of a symbol via absolute addressing */
#define absolute(sym) ({	\
		void *_addr;	\
		__asm__ (	\
			"ldr	%0, =%1\n"	\
			: "=r"(_addr) : "S"(&sym)); \
		_addr;	\
	})

/* PA() and VA() refer to physical and virtual address of a symbol */
#define PA(sym) ((unsigned long)pcrel(sym))
#define VA(sym) ((addr_t)absolute(sym))


/* exported */
unsigned int bsp_preboot_ok;

/* imported */
extern char __text_start[];
extern char __text_end[];
extern char __rodata_start[];
extern char __rodata_end[];
extern char __data_start[];
extern char __data_end[];
extern char __bss_start[];
extern char __bss_end[];


/* setup a specific mapping */
__init static arm_pte_t *preboot_map_range(
	arm_pte_t *free_pts,
	arm_pte_t *l1,
	addr_t addr,
	addr_t end,
	unsigned long phys,
	unsigned long flags)
{
	arm_pte_t l1_pte;
	arm_pte_t *l2;
	arm_pte_t l2_pte;
	arm_pte_t *l3;

	if (((addr | end | phys) & (BLOCK_SIZE - 1)) == 0) {
		/* use a large mapping */
		flags = (flags & ~PTE_TYPE_MASK) | PTE_TYPE_BLOCK;

		while (addr < end) {
			/* get L2 table */
			l1_pte = l1[arm_addr_to_l1(addr)];
			if (l1_pte != 0) {
				l2 = (arm_pte_t *)PTE_TO_PHYS(l1_pte);
			} else {
				l2 = free_pts;
				free_pts += PTE_NUM;
				l1[arm_addr_to_l1(addr)] = PHYS_TO_PTE(l2) | KERN_PT;
			}

			l2[arm_addr_to_l2(addr)] = PHYS_TO_PTE(phys) | flags;

			addr += BLOCK_SIZE;
			phys += BLOCK_SIZE;
		}

		return free_pts;
	}

	while (addr < end) {
		/* get L2 table */
		l1_pte = l1[arm_addr_to_l1(addr)];
		if (l1_pte != 0) {
			l2 = (arm_pte_t *)PTE_TO_PHYS(l1_pte);
		} else {
			l2 = free_pts;
			free_pts += PTE_NUM;
			l1[arm_addr_to_l1(addr)] = PHYS_TO_PTE(l2) | KERN_PT;
		}

		/* get L3 table */
		l2_pte = l2[arm_addr_to_l2(addr)];
		if (l2_pte != 0) {
			l3 = (arm_pte_t *)PTE_TO_PHYS(l2_pte);
		} else {
			l3 = free_pts;
			free_pts += PTE_NUM;
			l2[arm_addr_to_l2(addr)] = PHYS_TO_PTE(l3) | KERN_PT;
		}

		l3[arm_addr_to_l3(addr)] = PHYS_TO_PTE(phys) | flags;

		addr += PAGE_SIZE;
		phys += PAGE_SIZE;
	}

	return free_pts;
}


/* setup kernel page tables */
/* NOTE: this functions executes in physical addressing mode,
 * so we cannot use the normal C way to get symbol addresses.
 * The PA() and VA() macros use explicit addressing modes to retrieve
 * virtual and physical addresses of certain symbols in a safe way.
 */
__init void preboot_setup_tagetables(
	arm_pte_t *ttbr0_l1,
	arm_pte_t *ttbr1_l1,
	unsigned long load_addr)
{
	const struct part_cfg *part_cfg_kern;
	arm_pte_t *free_pts, *free_pts_end;
	unsigned int *preboot_ok_p;
	arm_pte_t *l2;
	arm_pte_t l1_pte;
	arm_pte_t *last_l3;
	addr_t addr, end;
	unsigned long phys;
	unsigned long flags;

	/* get pointer to pool of free page tables */
	part_cfg_kern = (const struct part_cfg *)PA(part_cfg);
	free_pts = part_cfg_kern->kmem_pagetables_mem;
	free_pts = (arm_pte_t *)BOARD_KERNEL_TO_PHYS((unsigned long)free_pts);
	free_pts_end = free_pts + part_cfg_kern->kmem_pagetables_num * PTE_NUM;

	load_addr &= ~(PAGE_SIZE - 1);

	/* setup TTBR0 adspace -- just one page of the identity mapping */
	addr  = load_addr;
	end   = load_addr + PAGE_SIZE;
	phys  = load_addr;
	flags = KERN_RX | CACHED | PTE_NG;
	free_pts = preboot_map_range(free_pts, ttbr0_l1, addr, end, phys, flags);

	/* setup TTBR1 adspace -- everything fits into one L2 table */
	/* map various kernel segments */
	addr  = VA(__text_start);
	end   = VA(__text_end);
	phys  = PA(__text_start);
	flags = KERN_RX | CACHED;
	free_pts = preboot_map_range(free_pts, ttbr1_l1, addr, end, phys, flags);

	addr  = VA(__rodata_start);
	end   = VA(__rodata_end);
	phys  = PA(__rodata_start);
	flags = KERN_RO | CACHED;
	free_pts = preboot_map_range(free_pts, ttbr1_l1, addr, end, phys, flags);

	addr  = VA(__data_start);
	end   = VA(__bss_end);
	phys  = PA(__data_start);
	flags = KERN_RW | CACHED;
	free_pts = preboot_map_range(free_pts, ttbr1_l1, addr, end, phys, flags);

	/* setup I/O */
	for (unsigned int i = 0; i < part_cfg_kern->io_map_num; i++) {
		const struct memrq_cfg *m = &((const struct memrq_cfg *)PA(io_map_cfg))[i];

		addr = m->addr;
		end = addr + m->size;
		phys = m->phys;
		flags = KERN_RO;
		if ((m->prot & PROT_EXEC) != 0) {
			flags &= ~PTE_PXN;
		}
		if ((m->prot & PROT_WRITE) != 0) {
			flags &= ~PTE_AP2;
		}
		/* the cache mode directly encodes to the PTE_ATTR bits */
		assert(m->cache < 8);
		flags |= m->cache << PTE_ATTR_SHIFT;
		if (m->cache >= CACHE_TYPE_WC) {
			/* memory */
			flags |= PTE_SH1 | PTE_SH0;
		} else {
			/* I/O */
			flags |= PTE_XN | PTE_PXN;
		}

		free_pts = preboot_map_range(free_pts, ttbr1_l1, addr, end, phys, flags);
	}

	/* register the special last pagetable */
	last_l3 = (arm_pte_t *)PA(arch_adspace_xmap_pagetable);
	l1_pte = ttbr1_l1[arm_addr_to_l1(ARCH_KERNEL_BASE)];
	l2 = (arm_pte_t *)PTE_TO_PHYS(l1_pte);
	l2[arm_addr_to_l2(ARCH_PERCPU_BASE)] = PHYS_TO_PTE(last_l3) | KERN_PT | PTE_PT_PXN;

	/* report our state to bsp.c */
	preboot_ok_p = (unsigned int *)PA(bsp_preboot_ok);
	*preboot_ok_p = (free_pts <= free_pts_end);
}

/* initialize first 64-bytes of all shared memory segments to zero */
/* NOTE: this functions executes in physical addressing mode,
 * so we cannot use the normal C way to get symbol addresses.
 * The PA() and VA() macros use explicit addressing modes to retrieve
 * virtual and physical addresses of certain symbols in a safe way.
 */
__init void preboot_init_shms(void)
{
	const struct memrq_cfg *m;
	unsigned long *ptr;
	unsigned int num;

	num = *(const typeof(shm_map_num) *)PA(shm_map_num);
	for (unsigned int i = 0; i < num; i++) {
		m = &((const struct memrq_cfg *)PA(shm_map_cfg))[i];

		ptr = (unsigned long *)m->phys;

		for (unsigned int j = 0; j < 64 / sizeof(*ptr); j++) {
			ptr[j] = 0;
		}
	}
}
