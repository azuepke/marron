/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * zynq_uart.c
 *
 * ZYNQ UART driver
 *
 * azuepke, 2013-09-17: initial
 * azuepke, 2021-01-22: adapted
 */

/*
 * See chapters 19 and B.33 of the ZYNQ manual:
 * "Xilinx UG585 Zynq-7000 All Programmable SoC Technical Reference Manual"
 * September 10, 2013
 */

#include <bsp.h>
#include <arch_io.h>
#include <board.h>


/** UART registers */
#define UART_REG_CTRL			0x00	/* control register */
#define UART_REG_MODE			0x04	/* mode register */
#define UART_REG_IRQ_EN			0x08	/* interrupt enable register */
#define UART_REG_IRQ_DIS		0x0c	/* interrupt disable register */
#define UART_REG_IRQ_MASK		0x10	/* interrupt mask register (read-only) */
#define UART_REG_CHAN_IRQ		0x14	/* channel interrupt status register */
#define UART_REG_BAUDRATE		0x18	/* baud rate generator */
#define UART_REG_RX_TIMEOUT		0x1c	/* receiver timeout */
#define UART_REG_RX_TRIGGER		0x20	/* receiver FIFO trigger level */
#define UART_REG_MODEM_CTRL		0x24	/* modem control register */
#define UART_REG_MODEM_STATUS	0x28	/* modem status register */
#define UART_REG_STATUS			0x2c	/* channel status register */
#define UART_REG_FIFO			0x30	/* RX / TX FIFO access */
#define UART_REG_BAUD_DIV		0x34	/* baud rate divider */
#define UART_REG_FLOW_DELAY		0x38	/* Flow control delay register */
#define UART_REG_TX_TRIGGER		0x44	/* transmitter FIFO trigger level */

/* register accessors */
static inline uint32_t rd(unsigned long reg)
{
	return read32(io_ptr(ZYNQ_UART_REGS, reg));
}

static inline void wr(unsigned long reg, uint32_t val)
{
	write32(io_ptr(ZYNQ_UART_REGS, reg), val);
}

/** UART control register bits */
#define UART_CTRL_STPBRK	0x100
#define UART_CTRL_STTBRK	0x080
#define UART_CTRL_RSTTO		0x040
#define UART_CTRL_TXDIS		0x020
#define UART_CTRL_TXEN		0x010
#define UART_CTRL_RXDIS		0x008
#define UART_CTRL_RXEN		0x004
#define UART_CTRL_TXRES		0x002
#define UART_CTRL_RXRES		0x001


/** UART mode register bits */
#define UART_MODE_NORMAL	0x000
#define UART_MODE_STOP1		0x000
#define UART_MODE_STOP15	0x040
#define UART_MODE_STOP2		0x080
#define UART_MODE_E			0x000	/* parity even */
#define UART_MODE_O			0x008	/* parity odd */
#define UART_MODE_N			0x020	/* parity none */
#define UART_MODE_8			0x000	/* 8 bits */
#define UART_MODE_7			0x004	/* 7 bits */
#define UART_MODE_6			0x006	/* 6 bits */
#define UART_MODE_CLKS		0x001	/* select uart_ref_clk / 8 */


/** UART interrupt bits */
#define UART_IRQ_TOVR		0x1000
#define UART_IRQ_TNFUL		0x0800
#define UART_IRQ_TTRIG		0x0400
#define UART_IRQ_DMSI		0x0200
#define UART_IRQ_TIMEOUT	0x0100
#define UART_IRQ_PARE		0x0080
#define UART_IRQ_FRAME		0x0040
#define UART_IRQ_ROVR		0x0020
#define UART_IRQ_TFUL		0x0010
#define UART_IRQ_TEMPTY		0x0008
#define UART_IRQ_RFUL		0x0004
#define UART_IRQ_REMPTY		0x0002
#define UART_IRQ_RTRIG		0x0001


/** UART status bits */
#define UART_STATUS_TNFUL	0x4000
#define UART_STATUS_TTRIG	0x2000
#define UART_STATUS_FDELT	0x1000
#define UART_STATUS_TACTIVE	0x0800
#define UART_STATUS_RACTIVE	0x0400
#define UART_STATUS_TFUL	0x0010
#define UART_STATUS_TEMPTY	0x0008
#define UART_STATUS_RFUL	0x0004
#define UART_STATUS_REMPTY	0x0002
#define UART_STATUS_RTRIG	0x0001


err_t bsp_putc(int c)
{
	/* poll until transmitter has space for a character */
	while ((rd(UART_REG_STATUS) & UART_STATUS_TFUL) != 0) {
		return EBUSY;
	}

	wr(UART_REG_FIFO, c & 0xffu);
	return EOK;
}

err_t bsp_getc(int *c)
{
	/* poll until RX FIFO is not empty */
	if ((rd(UART_REG_STATUS) & UART_STATUS_REMPTY) != 0) {
		return EBUSY;
	}

	*c = rd(UART_REG_FIFO) & 0xffu;
	return EOK;
}

__init void serial_init(unsigned int baudrate)
{
	unsigned int div;

	/* reset UART and poll until completed */
	wr(UART_REG_CTRL, UART_CTRL_TXRES|UART_CTRL_RXRES);
	while (rd(UART_REG_CTRL) & (UART_CTRL_TXRES|UART_CTRL_RXRES))
		;

	/* set 8n1, 1/8 reference clock */
	wr(UART_REG_MODE, UART_MODE_NORMAL | UART_MODE_8 | UART_MODE_N | UART_MODE_STOP1);

	/* set baudrate */
	/* FIXME: we can be done better and adjust BAUD_DIV as well */
	div = (ZYNQ_UART_CLOCK + (baudrate * 16) / 2) / (baudrate * 16);
	wr(UART_REG_BAUDRATE, div);
	wr(UART_REG_BAUD_DIV, 15);	/* 16x oversampling */

	/* enable RX and TX */
	wr(UART_REG_CTRL, UART_CTRL_TXEN|UART_CTRL_RXEN);
}
