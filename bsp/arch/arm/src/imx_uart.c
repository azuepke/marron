/* SPDX-License-Identifier: MIT */
/* Copyright 2016 Alexander Zuepke */
/*
 * imx_uart.c
 *
 * i.MX6 UART driver
 *
 * azuepke, 2016-04-14: initial
 * awerner, 2020-01-30: imported
 * azuepke, 2023-01-21: baudrate initialization
 */

/* See chapter 64 in the i.MX6 manual (page 5169) */

#include <bsp.h>
#include <arch_io.h>
#include <board.h>

/** UART registers */
#define UART_URXD				0x00	/* receiver register */
#define UART_UTXD				0x40	/* transmitter register */
#define UART_UCR1				0x80	/* control register 1 */
#define UART_UCR2				0x84	/* control register 2 */
#define UART_UCR3				0x88	/* control register 3 */
#define UART_UCR4				0x8c	/* control register 4 */
#define UART_UFCR				0x90	/* FIFO control register */
#define UART_USR1				0x94	/* status register 1 */
#define UART_USR2				0x98	/* status register 2 */
#define UART_UESC				0x9c	/* escape character register */
#define UART_UTIM				0xa0	/* escape timer register */
#define UART_UBIR				0xa4	/* BRM incremental register */
#define UART_UBMR				0xa8	/* BRM modulator register */
#define UART_UBRC				0xac	/* baud rate count register */
#define UART_ONEMS				0xb0	/* one millisecond register */
#define UART_UTS				0xb4	/* test register */
#define UART_UMCR				0xb8	/* RS-485 mode control register */


/* register accessors */
static inline uint32_t rd(unsigned long reg)
{
	return read32(io_ptr(IMX_UART_REGS, reg));
}

static inline void wr(unsigned long reg, uint32_t val)
{
	write32(io_ptr(IMX_UART_REGS, reg), val);
}

/** UART control register bits */
// ...

/** UART status register 1 bits */
#define UART_USR1_TRDY			(1<<13)	/* transmitter requires data */

/** UART status register 2 bits */
#define UART_USR2_RDR			(1<<0)	/* receiver buffer not empty */
#define UART_USR2_TXFE			(1<<14)	/* transmitter buffer empty */

/** UART test register */
#define UART_UTS_SOFTRST		(1<<0)	/* software reset */

err_t bsp_putc(int c)
{
	/* poll until transmitter has space for a character */
	while ((rd(UART_USR2) & UART_USR2_TXFE) == 0) {
		return EBUSY;
	}

	wr(UART_UTXD, c & 0xffu);
	return EOK;
}

err_t bsp_getc(int *c)
{
	/* poll until RX FIFO is not empty */
	if ((rd(UART_USR2) & UART_USR2_RDR) == 0) {
		return EBUSY;
	}

	*c = rd(UART_URXD) & 0xffu;
	return EOK;
}

__init void serial_init(unsigned int baudrate)
{
#ifdef IMX_UART_CLOCK
	/* disable UART */
	wr(UART_UCR1, 0x0000);

	/* software reset */
	wr(UART_UCR2, 0x0000);
	while ((rd(UART_UTS) & UART_UTS_SOFTRST) != 0) {
		/* just polling */
	}

	/* software flow control, 8n1 */
	wr(UART_UCR2, 0x4027);

	/* hardware settings, disable interrupts (from example sequence) */
	wr(UART_UCR3, 0x0704);

	/* CTS trigger level at 31 */
	wr(UART_UCR4, 0x7c00);

	/* FIFO: transmitter trigger level at 2,
	 *       receiver trigger level at 1,
	 *       divide clock by 1 (value 5)
	 */
	wr(UART_UFCR, (2 << 10) | (1 << 0) | (5 << 7) );

	/* 16x sampling */
	wr(UART_UBIR, 0x000f);

	/* baudrate */
	wr(UART_UBMR, IMX_UART_CLOCK / baudrate);

	/* enable UART */
	wr(UART_UCR1, 0x0001);
#else
	/* we stick with the U-BOOT settings */
	(void)baudrate;
#endif
}
