/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * cache.c
 *
 * ARM cache handling
 *
 * azuepke, 2013-09-14: initial
 * azuepke, 2021-01-21: complete rework
 */

#include <bsp.h>
#include <marron/macros.h>
#include <ex_table.h>


/** clean and invalidate all dcache */
/* NOTE: the GCC attribute enforces ARM code when compiling in Thumb mode */
__attribute__((target("arm")))
static void flush_dcache_all(void)
{
	/*
	 * based on ARM example code
	 * ARM ARM DDI 0406A, "Example code for cache maintenance operations"
	 * clobbers r0 .. r7, r9 .. r10
	 */
	__asm__ volatile (
		"	dmb								\n"
		"	mrc		p15, 1, r0, c0, c0, 1	\n"	// Read CLIDR
		"	ands	r3, r0, #0x7000000		\n"
		"	mov		r3, r3, lsr #23			\n"	// Cache level value (naturally aligned)
		"	beq		5f						\n"
		"	mov		r10, #0					\n"

		"1:									\n"
		"	add		r2, r10, r10, lsr #1	\n"	// Work out 3xcachelevel
		"	mov		r1, r0, lsr r2			\n"	// bottom 3 bits are the Cache type for this level
		"	and		r1, r1, #7				\n"	// get those 3 bits alone
		"	cmp		r1, #2					\n"
		"	blt		4f						\n"	// no cache or only instruction cache at this level
		"	mcr		p15, 2, r10, c0, c0, 0	\n"	// write the Cache Size selection register
		"	isb								\n"	// ISB to sync the change to the CacheSizeID reg
		"	mrc		p15, 1, r1, c0, c0, 0	\n"	// reads current Cache Size ID register
		"	and		r2, r1, #0x7			\n"	// extract the line length field
		"	add		r2, r2, #4				\n"	// add 4 for the line length offset (log2 16 bytes)
		"	ldr		r4, =0x3ff				\n"
		"	ands	r4, r4, r1, lsr #3		\n"	// r4 is the max number on the way size (right aligned)
		"	clz		r5, r4					\n"	// r5 is the bit position of the way size increment
		"	ldr		r7, =0x7fff				\n"
		"	ands	r7, r7, r1, lsr #13		\n"	// r7 is the max number of the index size (right aligned)

		"2:									\n"
		"	mov		r9, r4					\n"	// r9 working copy of the max way size (right aligned)

		"3:									\n"
		"	lsl		r6, r9, r5				\n"	// factor in the way number and cache number into r11
		"	orr		r11, r10, r6			\n"
		"	lsl		r6, r7, r2				\n"	// factor in the index number
		"	orr		r11, r11, r6			\n"
		"	mcr		p15, 0, r11, c7, c14, 2	\n"	// clean and invalidate by set/way
		"	subs	r9, r9, #1				\n"	// decrement the way number
		"	bge		3b						\n"
		"	subs	r7, r7, #1				\n"	// decrement the index
		"	bge		2b						\n"

		"4:									\n"
		"	add		r10, r10, #2			\n"	// increment the cache number
		"	cmp		r3, r10					\n"
		"	bgt		1b						\n"

		"5:									\n"
		"	mov		r10, #0					\n"	// select cache level 0
		"	mcr		p15, 2, r10, c0, c0, 0	\n"	// write the Cache Size selection register
		"	isb								\n"
		: : : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7",
		      "r9", "r10", "r11", "cc", "memory");
}

/** invalidate icache and branch prediction */
static inline void inval_icache_all(void)
{
#ifdef SMP
	/* inner shareable */
	__asm__ volatile ("mcr p15, 0, %0, c7, c1, 0" : : "r"(0) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c7, c5, 0" : : "r"(0) : "memory");
#endif
}

/** get size of data cacheline in bytes */
static inline unsigned long arm_dcache_linesize(void)
{
	unsigned long ctr;
	__asm__ volatile ("mrc p15, 0, %0, c0, c0, 1" : "=r"(ctr));
	return (1u << ((ctr >> 16) & 0xf)) * 4;
}

/** get size of instruction cacheline in bytes */
static inline unsigned long arm_icache_linesize(void)
{
	unsigned long ctr;
	__asm__ volatile ("mrc p15, 0, %0, c0, c0, 1" : "=r"(ctr));
	return (1u << ((ctr >> 0) & 0xf)) * 4;
}

/** ARM cache handling */
err_t bsp_cache(sys_cache_op_t mode, addr_t addr, size_t size, addr_t alias __unused)
{
	unsigned long tmp;
	size_t linesize;
	err_t err;
	addr_t end;

	switch (mode) {
	case CACHE_OP_FLUSH_DCACHE_ALL:
		/* flushes current CPU's dcache to PoC */
		flush_dcache_all();
		err = EOK;
		break;

	case CACHE_OP_INVAL_ICACHE_ALL:
		/* invalidates current CPU's icache only */
		inval_icache_all();
		err = EOK;
		break;

	case CACHE_OP_INVAL_ICACHE_RANGE:
		/* clean dcache + inval icache (to PoU each) */
		linesize = arm_dcache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		/*
		 * The code below is in fact:
		 *		while (addr < end) {
		 *			MAGIC_CACHE_OP(addr);
		 *			addr += linesize;
		 *		}
		 *		err = EOK;
		 *	But robust against exceptions (return EFAULT then).
		 */
		__asm__ volatile (
			"	b		2f						\n"
			"1:	mcr		p15, 0, %0, c7, c11, 1	\n"	// DCCMVAU
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	bcc		1b						\n"
			/* dc operations must complete before ic */
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		if (err != EOK) {
			break;
		}

		linesize = arm_icache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			"1:	mcr		p15, 0, %0, c7, c5, 1	\n"// ICIMVAU
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	bcc		1b						\n"
			/* ic operations require a DSB */
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case CACHE_OP_FLUSH_DCACHE_RANGE:
		/* flush dcache to PoC */
		linesize = arm_dcache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			"1:	mcr		p15, 0, %0, c7, c14, 1	\n"	// DCCIMVAC
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	bcc		1b						\n"
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case CACHE_OP_CLEAN_DCACHE_RANGE:
		/* clean dcache to PoC */
		linesize = arm_dcache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			"1:	mcr		p15, 0, %0, c7, c10, 1	\n"	// DCCMVAC
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	bcc		1b						\n"
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case CACHE_OP_INVAL_DCACHE_RANGE:
		/* inval dcache to PoC */
		linesize = arm_dcache_linesize();
		/* NOTE: 32-bit ARM lacks a write-permission check in DCIMVAC,
		 * therefore we check if the memory is writable (label 0)
		 * before invalidating cache content. However, this is racy
		 * if page tables entries change under our feet
		 * and suddenly point to a different read-only mapping.
		 * TODO: take a page table lock to prevent page table changes.
		 */
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			/* check if address is writable */
			"0:	mcr		p15, 0, %0, c7, c8, 1	\n"	/* V2PCWPW */
			"	isb								\n"
			"	mrc		p15, 0, %1, c7, c4, 0	\n"	/* PAR */
			/* if bit 1 is clear, the access was successful */
			"	subs	%1, %1, #1				\n"
			"	beq		4f						\n"
			"1:	mcr		p15, 0, %0, c7, c6, 1	\n"	// DCIMVAC
			"	add		%0, %0, %3				\n"
			"2:	cmp		%0, %2					\n"
			"	bcc		0b						\n"
			"	dsb		sy						\n"
			"	mov		%0, %5					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %6					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err), "=&r"(tmp)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	default:
		err = ENOSYS;
		break;
	}

	return err;
}
