/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2020 Alexander Zuepke */
/*
 * mpcore_timer.c
 *
 * ARM Cortex A9 MPCore specific per-CPU timer
 *
 * azuepke, 2013-09-18: initial
 * azuepke, 2013-11-20: split into IRQ and timer code
 * azuepke, 2020-11-23: cleanup and sync
 */

#include <kernel.h>
#include <assert.h>
#include <bsp.h>
#include <gic.h>
#include <marron/compiler.h>
#include <mpcore_timer.h>
#include <arch_io.h>
#include <board.h>
#include <current.h>


/* registers in per-CPU area, relative to MPCORE_BASE + 0x0100 */

/* the private timer is a count-down timer that fires on the transition to 0 */
#define PTIMER_RELOAD	0x000
#define PTIMER_COUNTER	0x004
#define PTIMER_CTRL		0x008
	/*
	 * Control bits
	 *  bit 15-8:  PRESCALER
	 *  bit 2:     IRQ_ENABLE
	 *  bit 1:     AUTO_RELOAD
	 *  bit 0:     ENABLE
	 */

#define PTIMER_ACK		0x00c

/* access to per-CPU specific registers */
static inline uint32_t ptimer_read32(unsigned int reg)
{
	return read32(io_ptr(MPCORE_PTIMER_BASE, reg));
}

static inline void ptimer_write32(unsigned int reg, uint32_t val)
{
	write32(io_ptr(MPCORE_PTIMER_BASE, reg), val);
}

/** acknowledge private timer */
static inline void ptimer_ack(void)
{
	ptimer_write32(PTIMER_ACK, 0x1);
}

/** initialize private timer */
static __init void ptimer_init(unsigned int reload)
{
	/* disable and initialize the timer, but do not start it yet */
	ptimer_write32(PTIMER_CTRL, 0);
	ptimer_write32(PTIMER_RELOAD, reload);
	ptimer_write32(PTIMER_COUNTER, reload);
	ptimer_ack();

	/* start the timer (enable timer, IRQ, auto reload, prescaler == 0) */
	ptimer_write32(PTIMER_CTRL, 0x7);
}

/* FIXME: the jiffies will overflow in 49 days at 1000 HZ! */
static uint64_t jiffies[4];
static uint32_t clock_ns;

static uint64_t next_expiry[4];
static uint64_t reload;

// Timer resolution in nanoseconds
uint64_t bsp_timer_resolution;

uint64_t bsp_timer_get_time(void)
{
	unsigned int cpu_id = current_cpu_id();

	return (uint64_t)jiffies[cpu_id] * clock_ns;
}

// set next timer expiry (oneshot)
void bsp_timer_set_expiry(uint64_t expiry __unused, unsigned int cpu_id __unused)
{
	/* ignored in periodic mode */
}

static void timer_handler(unsigned int irq __unused)
{
	unsigned int cpu_id = current_cpu_id();
	ptimer_ack();

	jiffies[cpu_id]++;

	/* notify kernel to expiry timeouts */
	kernel_timer((uint64_t)jiffies[cpu_id] * clock_ns);
}

/** timer implementation -- uses ARM MPCore per core private timer */
__init void mpcore_timer_init(unsigned int freq)
{
#ifdef SMP
	assert(bsp_cpu_num <= 4);
#endif

	reload = MPCORE_TIMER_CLOCK / freq;
	clock_ns = 1000000000 / freq;
	bsp_timer_resolution = clock_ns;

	/* install handler -- the interrupt controller logic never masks this IRQ */
	bsp_irq_register(IRQ_ID_PTIMER, timer_handler);
}


/** timer implementation -- uses ARM MPCore per core private timer */
__init void mpcore_timer_start(void)
{
	unsigned int cpu_id = current_cpu_id();

	/* unmask interrupt in GIC */
	bsp_irq_enable(IRQ_ID_PTIMER, cpu_id);

	next_expiry[cpu_id] = reload;
	ptimer_init(reload);
}
