/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2021 Alexander Zuepke */
/*
 * cache64.c
 *
 * ARM 64-bit cache handling
 *
 * azuepke, 2013-09-14: initial
 * azuepke, 2015-07-15: fork 64-bit version
 * azuepke, 2021-01-21: complete rework
 */

#include <bsp.h>
#include <marron/macros.h>
#include <ex_table.h>


/** clean and invalidate all dcache */
static void flush_dcache_all(void)
{
	/*
	 * based on ARM example code
	 * ARM ARM DDI 0406A, "Example code for cache maintenance operations"
	 * clobbers x0 .. x7, x9 .. x11
	 */
	__asm__ volatile (
		"	dmb		sy						\n"
		"	mrs		x0, CLIDR_EL1			\n"	// Read CLIDR
		"	and		x3, x0, #0x7000000		\n"
		"	lsr		x3, x3, #23				\n"	// Cache level value (naturally aligned)
		"	cbz		x3, 5f					\n"
		"	mov		x10, #0					\n"

		"1:									\n"
		"	add		x2, x10, x10, lsr #1	\n"	// Work out 3xcachelevel
		"	lsr		x1, x0, x2				\n"	// bottom 3 bits are the Cache type for this level
		"	and		x1, x1, #7				\n"	// get those 3 bits alone
		"	cmp		x1, #2					\n"
		"	b.lt	4f						\n"	// no cache or only instruction cache at this level
		"	msr		CSSELR_EL1, x10			\n"	// write the Cache Size selection register
		"	isb								\n"	// ISB to sync the change to the CacheSizeID reg
		"	mrs		x1, CCSIDR_EL1			\n"	// reads current Cache Size ID register
		"	and		x2, x1, #0x7			\n"	// extract the line length field
		"	add		x2, x2, #4				\n"	// add 4 for the line length offset (log2 16 bytes)
		"	mov		x4, #0x3ff				\n"
		"	ands	x4, x4, x1, lsr #3		\n"	// x4 is the max number on the way size (right aligned)
		"	clz		w5, w4					\n"	// x5 is the bit position of the way size increment
		"	mov		x7, #0x7fff				\n"
		"	and		x7, x7, x1, lsr #13		\n"	// x7 is the max number of the index size (right aligned)

		"2:									\n"
		"	mov		x9, x4					\n"	// x9 working copy of the max way size (right aligned)

		"3:									\n"
		"	lsl		x6, x9, x5				\n"	// factor in the way number and cache number into x11
		"	orr		x11, x10, x6			\n"
		"	lsl		x6, x7, x2				\n"	// factor in the index number
		"	orr		x11, x11, x6			\n"
		"	dc		CISW, x11				\n"	// clean and invalidate by set/way
		"	subs	x9, x9, #1				\n"	// decrement the way number
		"	b.ge	3b						\n"
		"	subs	x7, x7, #1				\n"	// decrement the index
		"	b.ge	2b						\n"

		"4:									\n"
		"	add		x10, x10, #2			\n"	// increment the cache number
		"	cmp		x3, x10					\n"
		"	b.gt	1b						\n"

		"5:									\n"
		"	mov		x10, #0					\n"	// select cache level 0
		"	msr		CSSELR_EL1, x10			\n"	// write the Cache Size selection register
		"	isb								\n"
		: : : "x0", "x1", "x2", "x3", "x4", "x5", "x6", "x7",
		      "x9", "x10", "x11", "cc", "memory");
}

/** invalidate icache and branch prediction */
static inline void inval_icache_all(void)
{
#ifdef SMP
	/* inner shareable */
	__asm__ volatile ("ic IALLUIS" : : "r"(0) : "memory");
#else
	__asm__ volatile ("ic IALLU" : : "r"(0) : "memory");
#endif
}

/** get size of data cacheline in bytes */
static inline unsigned long arm_dcache_linesize(void)
{
	unsigned long ctr;
	__asm__ volatile ("mrs %0, CTR_EL0" : "=r"(ctr));
	return (1u << ((ctr >> 16) & 0xf)) * 4;
}

/** get size of instruction cacheline in bytes */
static inline unsigned long arm_icache_linesize(void)
{
	unsigned long ctr;
	__asm__ volatile ("mrs %0, CTR_EL0" : "=r"(ctr));
	return (1u << ((ctr >> 0) & 0xf)) * 4;
}

/** ARM cache handling */
err_t bsp_cache(sys_cache_op_t mode, addr_t addr, size_t size, addr_t alias __unused)
{
	size_t linesize;
	err_t err;
	addr_t end;

	switch (mode) {
	case CACHE_OP_FLUSH_DCACHE_ALL:
		/* flushes current CPU's dcache to PoC */
		flush_dcache_all();
		err = EOK;
		break;

	case CACHE_OP_INVAL_ICACHE_ALL:
		/* invalidates current CPU's icache only */
		inval_icache_all();
		err = EOK;
		break;

	case CACHE_OP_INVAL_ICACHE_RANGE:
		/* clean dcache + inval icache (to PoU each) */
		linesize = arm_dcache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		/*
		 * The code below is in fact:
		 *		while (addr < end) {
		 *			MAGIC_CACHE_OP(addr);
		 *			addr += linesize;
		 *		}
		 *		err = EOK;
		 *	But robust against exceptions (return EFAULT then).
		 */
		__asm__ volatile (
			"	b		2f						\n"
			"1:	dc		CVAU, %0				\n"
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	b.cc	1b						\n"
			/* dc operations must complete before ic */
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		if (err != EOK) {
			break;
		}

		linesize = arm_icache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			"1:	ic		IVAU, %0				\n"
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	b.cc	1b						\n"
			/* ic operations require a DSB */
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case CACHE_OP_FLUSH_DCACHE_RANGE:
		/* flush dcache to PoC */
		linesize = arm_dcache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			"1:	dc		CIVAC, %0				\n"
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	b.cc	1b						\n"
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case CACHE_OP_CLEAN_DCACHE_RANGE:
		/* clean dcache to PoC */
		linesize = arm_dcache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			"1:	dc		CVAC, %0				\n"
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	b.cc	1b						\n"
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case CACHE_OP_INVAL_DCACHE_RANGE:
		/* inval dcache to PoC */
		linesize = arm_dcache_linesize();
		end = ALIGN_UP(addr + size, linesize);
		__asm__ volatile (
			"	b		2f						\n"
			"1:	dc		IVAC, %0				\n"
			"	add		%0, %0, %2				\n"
			"2:	cmp		%0, %1					\n"
			"	b.cc	1b						\n"
			"	dsb		sy						\n"
			"	mov		%0, %4					\n"
			"3:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"4:	mov		%0, %5					\n"
			"	b		3b						\n"
			".popsection						\n"
			EX_TABLE(1b, 4b)
			: "=&r"(err)
			: "r"(end), "r"(linesize), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	default:
		err = ENOSYS;
		break;
	}

	return err;
}
