/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * gic-v3.c
 *
 * ARM Generic Interrupt Controller (GIC v3) specific IRQ handling
 * supports GICv3 and GICv4
 *
 * azuepke, 2021-12-02: based on GICv2 version (mpcore.c/gic.c)
 */

#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <assert.h>
#include <arch_io.h>
#include <arm_insn.h>
#include <arm_cr.h>
#include <board.h>
#include <gic.h>
#include <current.h>


#ifndef GIC_ICC_MODE
#error "Please #define GIC_ICC_MODE in board.h"
#endif


/* real number of interrupts (the GIC supports up to 1024) */
unsigned int gic_num_irqs;

// Number of IRQs exported by the BSP
const unsigned int bsp_irq_num = GIC_NUM_IRQS;

// Number of CPUs exported by the BSP
const unsigned int bsp_cpu_num = CFG_CPU_NUM;

/* CPU that actually booted the kernel (always CPU #0 on ARM) */
unsigned int bsp_boot_cpu_id = 0;

static bsp_irq_handler_t irq_handlers[GIC_NUM_IRQS];

/* default interrupt handlers */

static void resched_irq_handler(unsigned int irq __unused)
{
#ifdef SMP
	kernel_ipi();
#endif
}

/* real stopping flag -- spurious SGIs might be pending at boot time */
static int stopping;

static __cold void stop_irq_handler(unsigned int irq __unused)
{
	if (stopping) {
		__bsp_halt();
	}
}


/*
 * GIC per-CPU and distributor registers.
 *
 * Based on the following TRMs (Technical Reference Manual) by ARM:
 *  IHI0069A -- ARM Generic Interrupt Controller, GIC version 3 and version 4
 *  DDI0516E -- GIC-500, Revision: r1p1
 *
 * Interrupt types:
 *  SGI:  software generated interrupts, 0..15
 *  PPI:  private peripheral interrupt, 16..31
 *  SPI:  shared peripheral interrupt, 32..1023 (max, in multiples of 32)
 *  ESPI: extended PPIs, 1056..1119 (if any)
 *  EPPI: extended SPIs, 4096..5119 (if any)
 *  LPI: locality-specific peripheral interrupt (8192+, actually MSIs)
 *
 * Interrupt groups:
 *  - group 0: FIQs, send to EL3
 *  - secure group 1: IRQs for the secure world, send to EL1 or EL2
 *  - non-secure group 1: IRQs for the non-secure world, send to EL1
 *  The GIC always signals interrupts for the "other" security side as FIQs,
 *  so EL3 can perform the world switch, if necessary. Likewise,
 *  the GIC architecture defines "A" or "alias" registers to raise interrupts
 *  to the other security state (ICC_SGI1R_EL1 vs ICC_ASGI1R_EL1).
 *  In a system without EL3, the secure and non-secure groups 1 are merged.
 *  In any case, interrupts in group 0 are FIQs and in group 1 are IRQs.
 *
 * Conventions:
 *  GICD: distributor, global, for SPIs
 *        64K registers
 *  GICR: redistributor, per CPU, for SGIs and PPIs, set base address of LPIs
 *        64K registers RD_base registers
 *        64K registers SGI_base registers (mirrors some GICD registers)
 *        64K registers VLPI_base registers (GICv4)
 *        64K reserved (GICv4)
 *  GITS: ITS registers
 *        64K control registers (ITS_base)
 *        64K interrupt translation base
 *        64K virtual SGI space (GICv4.1)
 *  GICC: physical GIC CPU interface (per CPU)
 *        unknown size, but at least 0x2000 bytes
 *        only available for GICv2 compatibility
 *  GICV: virtual GIC CPU interface (per CPU, VM)
 *  GICH: virtual interface control registers (per CPU, hypervisor)
 *  ICC:  physical system register CPU interface (ICC_*_ELx system registers)
 *  ICV:  virtual system register CPU interface (ICV_*_ELx system registers)
 *  ICH:  virtual interface control registers (hypervisor)
 *
 * Programming model:
 *  We operate the GIC in "affinity routing" (ARE) mode
 *  and assign all interrupts to non-secure group 1.
 *  We stick to the usual number of SPIs.
 *  LPIs are not yet supported.
 *  Explicit EOIMODE (use DIR to end an interrupt) is not enabled.
 */

#define INTID_MASK		0x3ff

/* special interrupt IDs */
/* NOTE: special interrupts 1020 to 1022 are only reported to EL3 */
#define INTID_SECURE	1020
#define INTID_NONSECURE	1021
#define INTID_LEGACY	1022
#define INTID_SPURIOUS	1023


/* accessors to GIC system register interface registers */
#define gen_icc_get_reg(name, reg) \
	static inline unsigned long icc_get_##name(void)	\
	{	\
		unsigned long val;	\
		__asm__ volatile ("mrs %0, " __stringify(reg) : "=r"(val));	\
		return val;	\
	}

#define gen_icc_set_reg(name, reg) \
	static inline void icc_set_##name(unsigned long val)	\
	{	\
		__asm__ volatile ("msr " __stringify(reg) ", %0" : : "r"(val) : "memory");	\
	}

/* GIC system register interface */
gen_icc_get_reg(ctlr, ICC_CTLR_EL1)		/* CPU interface control register */
gen_icc_set_reg(ctlr, ICC_CTLR_EL1)
	#define ICC_CTLR_EOIMODE	0x00000002
gen_icc_set_reg(enable, ICC_IGRPEN1_EL1)/* enable group1 interrupts register */
gen_icc_set_reg(pmr, ICC_PMR_EL1)		/* priority mask register */
gen_icc_set_reg(bpr, ICC_BPR1_EL1)		/* binary point register */
gen_icc_get_reg(iar, ICC_IAR1_EL1)		/* interrupt acknowledge register */
gen_icc_set_reg(eoir, ICC_EOIR1_EL1)	/* end of interrupt register */
gen_icc_get_reg(hppir, ICC_HPPIR1_EL1)	/* highest priority pending interrupt register */
gen_icc_set_reg(dir, ICC_DIR_EL1)		/* deactivate interrupt register */

gen_icc_set_reg(sgir, ICC_SGI1R_EL1)	/* software generated interrupt register  */
	#define ICC_SGIR_ALL_BUT_SELF	(1ul << 40)
	#define ICC_SGIR_INTID(intid)	((intid) << 24)


/* GICD register map */
#define GICD_CTLR		0x0000	/* distributor control register */
	#define GICD_CTLR_ENABLE0		0x00000001
	#define GICD_CTLR_ENABLE1		0x00000002
	#define GICD_CTLR_ENABLE1S		0x00000004
	#define GICD_CTLR_ARE_S			0x00000010
	#define GICD_CTLR_ARE			0x00000020
	#define GICD_CTLR_DS			0x00000040
#define GICD_TYPER		0x0004	/* read-only type register */
#define GICD_IIDR		0x0008	/* read-only ID register */
#define GICD_TYPER2		0x000c	/* read-only type register 2 */
#define GICD_STATUSR	0x0010	/* error reporting status register, optional */

#define GICD_GROUP		0x0080	/* interrupt group registers */

#define GICD_ENABLE		0x0100	/* write 1 to enable an interrupt */
#define GICD_DISABLE	0x0180	/* write 1 to disable an interrupt */

#define GICD_PENDING	0x0280	/* write 1 to clear an interrupt */

#define GICD_PRIO		0x0400	/* per interrupt priority, byte accessible */

#define GICD_ICFGR		0x0c00	/* interrupt configuration registers */
	/* 2 bits per IRQ:
	 *  0x  level
	 *  1x  edge
	 */

#define GICD_ROUTER		0x6000	/* interrupt routing registers, 64-bit */

/* accessors to GICD registers */
static inline uint32_t gicd_read32(unsigned long reg)
{
	return read32(io_ptr(GIC_DIST_BASE, reg));
}

static inline void gicd_write32(unsigned long reg, uint32_t val)
{
	write32(io_ptr(GIC_DIST_BASE, reg), val);
}

static inline void gicd_write64(unsigned long reg, uint64_t val)
{
	write64(io_ptr(GIC_DIST_BASE, reg), val);
}


/* GICR register map (RD_base) */
#define GICR_CTLR		0x00000	/* redistributor control register */
#define GICR_IIDR		0x00004	/* read-only ID register */
#define GICR_TYPER		0x00008	/* read-only type register */
#define GICR_STATUSR	0x00010	/* error reporting status register, optional */
#define GICR_WAKER		0x00014	/* wake register */
	#define GICR_WAKER_PROCESSOR_SLEEP	0x2
	#define GICR_WAKER_CHILDRED_ASLEEP	0x4

#define GICR_SETLPIR	0x00040	/* set LPI pending register (64-bit) */
#define GICR_CLRLPIR	0x00048	/* clear LPI pending register (64-bit) */

#define GICR_PROPBASER	0x00070	/* properties base address register (64-bit) */
#define GICR_PENDBASER	0x00078	/* LPI pending table base address register (64-bit) */

#define GICR_INVLPIR	0x000a0	/* invalidate LPI register (64-bit) */
#define GICR_INVALLR	0x000b0	/* invalidate all register (64-bit) */
#define GICR_SYNCR		0x000c0	/* synchronize register */

/* GICR register map (SGI_base, RD_base + 0x10000) */
#define GICR_GROUP		0x10080	/* interrupt group registers */

#define GICR_ENABLE		0x10100	/* write 1 to enable an interrupt */
#define GICR_DISABLE	0x10180	/* write 1 to disable an interrupt */

#define GICR_PENDING	0x10280	/* write 1 to clear an interrupt */

#define GICR_PRIO		0x10400	/* per interrupt priority, byte accessible */
#define GICR_ICFGR		0x10c00	/* interrupt configuration registers */
	/* 2 bits per IRQ:
	 *  0x  level
	 *  1x  edge
	 */

/* GICR register map (VLPI_base, RD_base + 0x20000) */
#define GICR_VPROPBASER	0x20070	/* virtual properties base address register (64-bit) */
#define GICR_VPENDBASER	0x20078	/* virtual LPI pending table base address register (64-bit) */

/* accessors to GICR registers (with CPU-specific offset to GIC_REDIST_BASE) */
static inline addr_t gicr_percpu_addr(unsigned int cpu)
{
	return GIC_REDIST_BASE + GIC_CPU_REDIST_ID(cpu) * GIC_REDIST_SIZE;
}

static inline uint32_t gicr_read32(unsigned int cpu, unsigned long reg)
{
	return read32(io_ptr(gicr_percpu_addr(cpu), reg));
}

static inline void gicr_write32(unsigned int cpu, unsigned long reg, uint32_t val)
{
	write32(io_ptr(gicr_percpu_addr(cpu), reg), val);
}

static inline uint64_t gicr_read64(unsigned int cpu, unsigned long reg)
{
	return read64(io_ptr(gicr_percpu_addr(cpu), reg));
}

static inline void gicr_write64(unsigned int cpu, unsigned long reg, uint64_t val)
{
	write64(io_ptr(gicr_percpu_addr(cpu), reg), val);
}


#ifdef SMP
/** send stop request to other cores */
void __cold gic_send_stop(void)
{
	stopping = 1;
	arm_dsb();
	icc_set_sgir(ICC_SGIR_ALL_BUT_SELF | ICC_SGIR_INTID(IRQ_ID_STOP));
	arm_isb();
	arm_dsb();
}

/** send rescheduling request to another processor */
void bsp_cpu_reschedule(unsigned int cpu)
{
	arm_dsb();
	icc_set_sgir(GIC_CPU_SGIR_ID(cpu) | ICC_SGIR_INTID(IRQ_ID_RESCHED));
	arm_isb();
	arm_dsb();
}
#endif

/** initialize the GIC distributed interrupt controller */
__init void gic_init_dist(void)
{
	unsigned int cpu_num;
	unsigned int irq;
	unsigned int val;
	unsigned int i;

	val = gicd_read32(GICD_TYPER);
#ifdef SMP
	/* in GICv3 mode, we can no longer auto-detect the number of cores */
	cpu_num = GIC_NUM_CORES;
	printk("GIC supports %u CPUs\n", cpu_num);
#endif
	gic_num_irqs = ((val & 0x1f) + 1) * 32;
	printk("GIC supports %u IRQs\n", gic_num_irqs);

	/* mask all interrupts and clear pending bits (except for SGIs and PPIs) */
	for (i = 32 / 32; i < gic_num_irqs / 32; i++) {
		gicd_write32(GICD_DISABLE + i * 4, 0xffffffff);
		gicd_write32(GICD_PENDING + i * 4, 0xffffffff);
	}

	/* assign all interrupts to non-secure group 1 (except for SGIs and PPIs) */
	for (i = 32 / 32; i < gic_num_irqs / 32; i++) {
		gicd_write32(GICD_GROUP + i * 4, 0xffffffff);
	}

	/* set all interrupts to max prio (except for SGIs and PPIs) */
	for (i = 32 / 4; i < gic_num_irqs / 4; i++) {
		/* highest possible priority */
		gicd_write32(GICD_PRIO + i * 4, 0x00000000);
	}

	/* assign to first CPU as default */
	for (i = 32 / 1; i < gic_num_irqs / 1; i++) {
		gicd_write64(GICD_ROUTER + i * 8, GIC_CPU_ROUTER_ID(0));
	}

	/* FIXME: need to load a valid edge/level configuration for each interrupt! */
	/* The reset value for the different interrupts depends on the CPU type.
	 *
	 * SGIs and PPIs are configured in gic_init_cpu()
	 * SPI interrupts always use 0x55555555 (active-HIGH)
	 */
	for (i = 32 / 16; i < gic_num_irqs / 16; i++) {
		gicd_write32(GICD_ICFGR + i * 4, 0x55555555);
	}

	/* set bit 0 to globally enable the GIC */
	val = gicd_read32(GICD_CTLR);
	val |= GICD_CTLR_ENABLE1;
	val |= GICD_CTLR_ARE;
	gicd_write32(GICD_CTLR, val);

	/* setup default interrupt handler */
	for (irq = 0; irq < GIC_NUM_IRQS; irq++) {
		irq_handlers[irq] = kernel_irq;
	}

	/* register special interrupt handlers */
	irq_handlers[IRQ_ID_STOP] = stop_irq_handler;
	irq_handlers[IRQ_ID_RESCHED] = resched_irq_handler;
}

/** enable the CPU specific GIC parts */
__init void gic_init_cpu(unsigned int cpu_id)
{
	unsigned int val, val2;
	unsigned int i;
	uint64_t val64;

	/* check reported affinity value (proper GIC_CPU_REDIST_ID() encoding) */
	val64 = gicr_read64(cpu_id, GICR_TYPER);
	/* encoding of the affinity value:
	 * - aff3 << 56
	 * - aff2 << 48
	 * - aff1 << 40
	 * - aff0 << 32
	 */
	val = val64 >> 32;

	val64 = GIC_CPU_ROUTER_ID(cpu_id);
	/* convert IROUTER format to 32-bit
	 * - aff3 << 32
	 * - aff2 << 16
	 * - aff1 << 8
	 * - aff0 << 0
	 */
	val2 = ((val64 >> 8) & 0xff000000) | (val64 & 0x00ffffff);
	if (val != val2) {
		panic("GIC: CPU %u affinity ID mismatch: 0x%08x vs 0x%08x\n",
		      cpu_id, val, val2);
	}

	/* wake up redistributor and poll readiness */
	val = gicr_read32(cpu_id, GICR_WAKER);
	val &= ~GICR_WAKER_PROCESSOR_SLEEP;
	gicr_write32(cpu_id, GICR_WAKER, val);

	do {
		val = gicr_read32(cpu_id, GICR_WAKER);
	} while ((val & GICR_WAKER_CHILDRED_ASLEEP) != 0);

	/* mask and clear all private interrupts */
	gicr_write32(cpu_id, GICR_DISABLE + 0, 0xffffffff);
	gicr_write32(cpu_id, GICR_PENDING + 0, 0xffffffff);

	/* assign SGIs and PPIs to non-secure group 1 */
	gicr_write32(cpu_id, GICR_GROUP + 0, 0xffffffff);

	/* set SPIs and PPIs to max prio */
	for (i = 0 / 4; i < 32 / 4; i++) {
		/* highest possible priority */
		gicr_write32(cpu_id, GICR_PRIO + i * 4, 0x00000000);
	}

	/* NOTE: no need to assign SPIs and PPIs to the current CPU */

	/* IRQ edge/level configuration:
	 *
	 * SGI interrupts always use 0xaaaaaaaa (edge-triggered)
	 * PPI interrupts use 0x00000000 (level-triggered)
	 */
	gicr_write32(cpu_id, GICR_ICFGR + 0, 0xaaaaaaaa);
	gicr_write32(cpu_id, GICR_ICFGR + 4, 0x00000000);

	/* prio 255 let all interrupts pass through, no preemption */
	icc_set_pmr(0xff);
	icc_set_bpr(0x7);

	/* enable the GIC CPU interface in EOI mode */
	val = icc_get_ctlr();
#if 0
	/* NOTE: no need for ICC_CTLR_EOIMODE without hardware virtualization */
	val |= ICC_CTLR_EOIMODE;
#endif
	icc_set_ctlr(val);

	/* enable group 1 interrupts */
	icc_set_enable(0x1);

	/* make changes visible to GIC */
	arm_isb();
	arm_dsb();
}


/** check if the user may attach to an interrupt:
 * returns:
 *  1 -> OK
 *  0 -> not available or not suitable for sharing
 * -1 -> out of range
 */
// Configure interrupt source
// this call returns zero if the interrupt source was configured to the
// requested mode, or non-zero if not.
// the mode "IRQ_AUTO" always succeeds if the interrupt source is available.
// the call also fails if the interrupt is not available
err_t bsp_irq_config(unsigned int irq, unsigned int mode __unused)
{
	assert(irq < GIC_NUM_IRQS);

	if (irq < IRQ_ID_FIRST_SPI)	/* private or special */
		return EBUSY;
	if (irq >= gic_num_irqs)	/* not available */
		return EBUSY;

	return EOK;
}

/** register a handler with an interrupt */
void bsp_irq_register(unsigned int irq, bsp_irq_handler_t handler)
{
	assert(irq < gic_num_irqs);

	irq_handlers[irq] = handler;
}

/** retrieve currently registered interrupt handler */
bsp_irq_handler_t bsp_irq_handler(unsigned int irq)
{
	assert(irq < gic_num_irqs);

	return irq_handlers[irq];
}

/** mask IRQ in distributor */
void bsp_irq_disable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int word, bit;

	assert(irq < gic_num_irqs);

	word = irq / 32;
	bit = irq % 32;

	if (irq < 32) {
		/* SGIs and PPIs are always local to the current CPU */
		cpu_id = current_cpu_id();

		gicr_write32(cpu_id, GICR_DISABLE + 4 * word, 1u << bit);
	} else {
#ifdef SMP
		/* NOTE: leave current target processor unchanged */
#endif

		gicd_write32(GICD_DISABLE + 4 * word, 1u << bit);
	}
}

/** unmask IRQ in distributor */
void bsp_irq_enable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int word, bit;

	assert(irq < gic_num_irqs);

	word = irq / 32;
	bit = irq % 32;

	if (irq < 32) {
		/* SGIs and PPIs are always local to the current CPU */
		cpu_id = current_cpu_id();

		gicr_write32(cpu_id, GICR_ENABLE + 4 * word, 1u << bit);
	} else {
#ifdef SMP
		/* set target processor */
		gicd_write64(GICD_ROUTER + irq * 8, GIC_CPU_ROUTER_ID(cpu_id));
#endif

		gicd_write32(GICD_ENABLE + 4 * word, 1u << bit);
	}
}

/** dispatch IRQ: mask and ack, call handler */
static void bsp_irq_dispatch_internal(void)
{
	unsigned int val;
	unsigned int irq;

	do {
		val = icc_get_iar();
		arm_isb();
		arm_dsb();

		irq = val & INTID_MASK;
		if (irq < 32) {
			/* per CPU interrupts, never masked, directly call handler */
			irq_handlers[irq](irq);
		} else if (irq == INTID_SPURIOUS) {
			/* spurious! */
			break;
		} else {
			/* device interrupt: first mask, then call handler */
			assert(irq < gic_num_irqs);
			bsp_irq_disable(irq, current_cpu_id());

			/* call handler */
			irq_handlers[irq](irq);
		}

		icc_set_eoir(val);
#if 0
		/* NOTE: enable together with ICC_CTLR_EOIMODE */
		icc_set_dir(val);
#endif
		arm_isb();
		arm_dsb();

		/* check for further pending interrupts */
		val = icc_get_hppir();
	} while ((val & INTID_MASK) != INTID_MASK);
}

/** dispatch current IRQ (called from architecture layer) */
void bsp_irq_dispatch(ulong_t vector __unused)
{
	bsp_irq_dispatch_internal();
}

/** dispatch any pending IRQ (called from scheduler) */
void bsp_irq_poll(void)
{
	bsp_irq_dispatch_internal();
}
