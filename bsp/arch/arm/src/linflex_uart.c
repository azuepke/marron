/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * linflex_uart.c
 *
 * NXP Linflex UART
 *
 * azuepke, 2021-04-10: initial
 */

/* See chapter 47 in the S32 manual */

#include <bsp.h>
#include <arch_io.h>
#include <arm_insn.h>
#include <board.h>

#ifndef LINFLEX_UART_REGS
#error LINFLEX_UART_REGS missing, no UART base address
#endif


/** LINFLEX registers (in master mode configuration) */
#define LINCR1		0x00	/* LIN control register 1 */
	#define LINCR1_INIT		0x01	/* init mode */
	#define LINCR1_SLEEP	0x02	/* sleep mode */
	#define LINCR1_RBLM		0x04	/* receiver buffer locked mode */
	#define LINCR1_MME		0x10	/* master mode enable */
	#define LINCR1_LBKM		0x20	/* loop back mode */

#define LINIER		0x04	/* LIN interrupt enable register */
#define LINSR		0x08	/* LIN status register */
	#define LINSR_LINS_MASK			0x0000f000
	#define LINSR_LINS_SLEEP		0x00000000
	#define LINSR_LINS_INIT			0x00001000
	#define LINSR_LINS_IDLE			0x00002000
	#define LINSR_LINS_SB			0x00003000
	#define LINSR_LINS_SD			0x00004000
	#define LINSR_LINS_SF			0x00005000
	#define LINSR_LINS_IF			0x00006000
	#define LINSR_LINS_HRT			0x00007000
	#define LINSR_LINS_DRDT			0x00008000
	#define LINSR_LINS_CSUM			0x00009000

#define LINESR		0x0c	/* LIN error status register */
#define UARTCR		0x10	/* UART mode control register */
	#define UARTCR_UART		0x000001	/* UART mode */
	#define UARTCR_WL0		0x000002	/* word length 0 */
	#define UARTCR_PCE		0x000004	/* parity control enable */
	#define UARTCR_PC0		0x000008	/* parity control 0 */
	#define UARTCR_TXEN		0x000010	/* transmitter enable */
	#define UARTCR_RXEN		0x000020	/* receiver enable */
	#define UARTCR_PC1		0x000040	/* parity control 1 */
	#define UARTCR_WL1		0x000080	/* word length 1 */
	#define UARTCR_TFBM		0x000100	/* TX FIFO/buffer mode */
	#define UARTCR_RFBM		0x000200	/* TX FIFO/buffer mode */
	#define UARTCR_ROSE		0x800000	/* Reduced Over Sampling Enable */
#define UARTCR_OSR_MASK		(0xF << 24)
#define UARTCR_OSR(uartcr)	(((uartcr) & UARTCR_OSR_MASK) >> 24)

#define UARTSR		0x14	/* UART mode status register */
	#define UARTSR_NF		0x0001	/* noise flag */
	#define UARTSR_DTFTFF	0x0002	/* data transm. completed / TX FIFO full */
	#define UARTSR_DRFRFE	0x0004	/* data recpt. completed / RX FIFO empty */
	#define UARTSR_TO		0x0008	/* timeout */
	#define UARTSR_RFNE		0x0010	/* receive FIFO not empty */
	#define UARTSR_WUF		0x0020	/* wakeup flag */
	#define UARTSR_RDI		0x0040	/* receiver data input */
	#define UARTSR_BOF		0x0080	/* FIFO/buffer overrun flag */
	#define UARTSR_FEF		0x0100	/* framing error flag */
	#define UARTSR_RMB		0x0200	/* release message buffer */
	#define UARTSR_PE0		0x0400	/* parity error flag 0 */
	#define UARTSR_PE1		0x0800	/* parity error flag 1 */
	#define UARTSR_PE2		0x1000	/* parity error flag 2 */
	#define UARTSR_PE3		0x2000	/* parity error flag 3 */
	#define UARTSR_OCF		0x4000	/* output compare flag */
	#define UARTSR_SZF		0x8000	/* stuck at zero flag */

#define LINTCSR		0x18	/* LIN time-out control status register */
#define LINOCR		0x1c	/* LIN output compare register */
#define LINTOCR		0x20	/* LIN time-out control register */
#define LINFBRR		0x24	/* LIN fractional baud rate register */
#define LINIBRR		0x28	/* LIN integer baud rate register */
#define LINCFR		0x2c	/* LIN checksum field register */
#define LINCR2		0x30	/* LIN control register 2 */
#define BIDR		0x34	/* buffer identifier register */
#define BDRL		0x38	/* buffer data register least significant */
#define BDRM		0x3c	/* buffer data register most significant */

/* registers remapped in master mode */
#define GCR			0x4c	/* global control register */
#define UARTPTO		0x50	/* UART preset timeout register */
#define UARTCTO		0x54	/* UART current timeout register */
#define DMATXE		0x58	/* DMA TX enable register */
#define DMARXE		0x5c	/* DMA RX enable register */

#if 0
/* slave mode registers */
#define IFER		0x40	/* identifier filter enable register */
#define IFMI		0x44	/* identifier filter match register */
#define IFMR		0x48	/* identifier filter mode register */

#define IFCR0		0x4c	/* identifier filter control register 0 */
#define IFCR1		0x50	/* identifier filter control register 1 */
#define IFCR2		0x54	/* identifier filter control register 2 */
#define IFCR3		0x58	/* identifier filter control register 3 */
#define IFCR4		0x5c	/* identifier filter control register 4 */
#define IFCR5		0x60	/* identifier filter control register 5 */
#define IFCR6		0x64	/* identifier filter control register 6 */
#define IFCR7		0x68	/* identifier filter control register 7 */
#define IFCR8		0x6c	/* identifier filter control register 8 */
#define IFCR9		0x70	/* identifier filter control register 9 */
#define IFCR10		0x74	/* identifier filter control register 10 */
#define IFCR11		0x78	/* identifier filter control register 11 */
#define IFCR12		0x7c	/* identifier filter control register 12 */
#define IFCR13		0x80	/* identifier filter control register 13 */
#define IFCR14		0x84	/* identifier filter control register 14 */
#define IFCR15		0x88	/* identifier filter control register 15 */

#define GCR			0x8c	/* global control register */
#define UARTPTO		0x90	/* UART preset timeout register */
#define UARTCTO		0x94	/* UART current timeout register */
#define DMATXE		0x98	/* DMA TX enable register */
#define DMARXE		0x9c	/* DMA RX enable register */
#endif

/* register accessors */
static inline uint32_t rd32(unsigned long reg)
{
	return read32(io_ptr(LINFLEX_UART_REGS, reg));
}

static inline void wr32(unsigned long reg, uint32_t val)
{
	write32(io_ptr(LINFLEX_UART_REGS, reg), val);
	arm_dsb();
}

err_t bsp_putc(int c)
{
	/* write character to buffer */
	wr32(BDRL, c & 0xffu);

	/* poll until DTF flag is set in UARTSR (transmission complete) */
	while ((rd32(UARTSR) & UARTSR_DTFTFF) == 0) {
		/* poll */
	}

	/* clear DTF flag */
	wr32(UARTSR, UARTSR_DTFTFF);
	return EOK;
}

err_t bsp_getc(int *c)
{
	(void)c;
	return ENOSYS;
}

static uint32_t get_ldiv_mult(void)
{
	uint32_t cr = rd32(UARTCR);

	if ((cr & UARTCR_ROSE) != 0) {
		return UARTCR_OSR(cr);
	}
	return 16;
}

static void set_baudrate(unsigned int baudrate)
{
	uint32_t divisr = LINFLEX_UART_CLK;
	uint32_t dividr = (uint32_t)(baudrate * get_ldiv_mult());

	wr32(LINIBRR, (uint32_t)(divisr / dividr));
	wr32(LINFBRR, (uint32_t)((divisr % dividr) * 16 / dividr) & 0xF);
}

__init void serial_init(unsigned int baudrate)
{
	uint32_t val;

	/* enter INIT mode */
	val = LINCR1_INIT;
	wr32(LINCR1, val);

	/* wait for device to enter INIT mode */
	while ((rd32(LINSR) & LINSR_LINS_MASK) != LINSR_LINS_INIT) {
		/* poll */
	}

	/* set Master mode */
	val |= LINCR1_MME;
	wr32(LINCR1, val);

	/* set UART buffer mode */
	val = UARTCR_UART;
	wr32(UARTCR, val);

	/* 8n1, enable transmitter and receiver */
	val |= UARTCR_WL0 | UARTCR_TXEN | UARTCR_RXEN;
	wr32(UARTCR, val);

	set_baudrate(baudrate);

	/* exit INIT mode */
	val = rd32(LINCR1);
	val &= ~LINCR1_INIT;
	wr32(LINCR1, val);
}
