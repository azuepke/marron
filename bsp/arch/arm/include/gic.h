/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * gic.h
 *
 * ARM Generic Interrupt Controller (GIC) specific IRQ handling
 * supports GICv1/v2 via gic.c and GICv3/v4 via gic-v3.c
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored from board.h
 * azuepke, 2013-11-20: split into IRQ and timer code
 * azuepke, 2020-11-23: cleanup
 * azuepke, 2021-12-02: refactor from mpcore.c to gic.c
 */

#ifndef GIC_H_
#define GIC_H_

/* special IRQ IDs */
#define IRQ_ID_STOP			0
#define IRQ_ID_RESCHED		1

#if defined(ARM_CORTEX_A9) || defined(ARM_CORTEX_A5)
#define IRQ_ID_GTIMER		27	/* global timer */
#define IRQ_ID_LEGACY_FIQ	28
#define IRQ_ID_PTIMER		29	/* private timer */
#define IRQ_ID_WATCHDOG		30	/* watchdog timer */
#define IRQ_ID_LEGACY_IRQ	31
#else /* A15/A5/GIC-400 or later */
#define IRQ_ID_DCC			22	/* debug communications channel (DCC) interrupt */
#define IRQ_ID_PMU			23	/* performace counter (PMU) interrupt */
#define IRQ_ID_CTI			24	/* cross trigger interface (CTI) interrupt */
#define IRQ_ID_VIRT			25	/* virtual maintenance */
#define IRQ_ID_HTIMER		26	/* hypervisor timer */
#define IRQ_ID_VTIMER		27	/* virtual timer */
#define IRQ_ID_LEGACY_FIQ	28
#define IRQ_ID_PTIMER		29	/* physical timer */
#define IRQ_ID_NS_TIMER		30	/* non-secure physical timer */
#define IRQ_ID_LEGACY_IRQ	31
#endif

#define IRQ_ID_FIRST_SPI	32

void gic_send_stop(void);
void gic_init_dist(void);
void gic_init_cpu(unsigned int cpu_id);

/* real number of interrupts */
extern unsigned int gic_num_irqs;

#endif
