/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * psci.h
 *
 * ARM PSCI calls
 *
 * azuepke, 2021-01-23: refactored from QEMU BSP
 */

#ifndef PSCI_H_
#define PSCI_H_

#include <board.h>
#include <marron/compiler.h>

/* inlined PSCI call */
static inline __always_inline unsigned long psci_call(
	unsigned long r0,
	unsigned long r1,
	unsigned long r2,
	unsigned long r3)
{
	register long reg_r0 __asm__("r0") = r0;
	register long reg_r1 __asm__("r1") = r1;
	register long reg_r2 __asm__("r2") = r2;
	register long reg_r3 __asm__("r3") = r3;

	__asm__ volatile (
#ifdef QEMU_PSCI_CALL
		"hvc #0"
#else
		"smc #0"
#endif
		: "=r" (reg_r0), "=r" (reg_r1), "=r" (reg_r2), "=r" (reg_r3)
		: "r" (reg_r0), "r" (reg_r1), "r" (reg_r2), "r" (reg_r3)
		: "memory", "cc"
#ifdef __aarch64__
		/* the SMC Calling Convention (ARM DEN 0028B) notes that on 64-bit ARM,
		 * volatile registers x4 to x17 are not preserved;
		 * however on 32-bit ARM, r4 to r14 must be preserved
		 */
		,
		"x4", "x5", "x6", "x7", "x8", "x9", "x10", "x11",
		"x12", "x13", "x14", "x15", "x16", "x17"
#endif
		);

	return reg_r0;
}

/* PSCI 1.1: ARM Power State Coordination Interface, ARM DEN 0022D, 2017 */

/* PSCI commands */
#define PSCI_CMD_PSCI_VERSION		0x84000000
#define PSCI_CMD_CPU_SUSPEND		0xc4000001
#define PSCI_CMD_CPU_OFF			0x84000002
#define PSCI_CMD_CPU_ON				0xc4000003
#define PSCI_CMD_AFFINITY_INFO		0xc4000004
#define PSCI_CMD_SYSTEM_OFF			0x84000008
#define PSCI_CMD_SYSTEM_RESET		0x84000009

/* PSCI error codes */
#define PSCI_ERR_SUCCESS			 0
#define PSCI_ERR_NOT_SUPPORTED		-1
#define PSCI_ERR_INVALID_PARAMETERS	-2
#define PSCI_ERR_DENIED				-3
#define PSCI_ERR_ALREADY_ON			-4
#define PSCI_ERR_ON_PENDING			-5
#define PSCI_ERR_INTERNAL_FAILURE	-6
#define PSCI_ERR_NOT_PRESENT		-7
#define PSCI_ERR_DISABLED			-8
#define PSCI_ERR_INVALID_ADDRESS	-9

static inline unsigned int psci_version(void)
{
	/* the PSCI version is encoded as <16 bit>.<16 bit> pair */
	return psci_call(PSCI_CMD_PSCI_VERSION, 0, 0, 0);
}

static inline int psci_cpu_on(unsigned long mpidr, unsigned long entry, unsigned long arg)
{
	return psci_call(PSCI_CMD_CPU_ON, mpidr, entry, arg);
}

static inline void psci_system_off(void)
{
	(void)psci_call(PSCI_CMD_SYSTEM_OFF, 0, 0, 0);
}

static inline void psci_system_reset(void)
{
	(void)psci_call(PSCI_CMD_SYSTEM_RESET, 0, 0, 0);
}

#endif
