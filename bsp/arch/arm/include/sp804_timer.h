/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * sp804_timer.h
 *
 * ARM Cortex A9 sp804 specific IRQ handling
 *
 * azuepke, 2013-11-20: initial
 */

#ifndef SP804_TIMER_H_
#define SP804_TIMER_H_

void sp804_timer_init(unsigned int freq);

#endif
