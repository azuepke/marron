/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * barrier.h
 *
 * Synchronization barriers for ARM
 *
 * Note: SEV+WFE has slightly different semantics on 32-bit ARM and no SEVL
 *
 * azuepke, 2023-09-29: initial
 */

#ifndef BARRIER_H_
#define BARRIER_H_

#include <marron/atomic.h>
#include <arm_insn.h>


/** wait until given bit is set in atomic bitmask barrier */
static inline void barrier_wait_bit(unsigned long *barrier, unsigned long bit)
{
	unsigned long mask = 1ul << bit;
	unsigned long val;

#ifdef __aarch64__
	arm_sevl();
#endif
	do {
#ifdef __aarch64__
		arm_wfe();
#endif
		val = atomic_load_acquire(barrier);
#ifdef __arm__
		if ((val & mask) != 0) {
			arm_wfe();
		}
#endif
	} while ((val & mask) == 0);
}

/** set bit in atomic bitmask barrier */
static inline void barrier_set_bit(unsigned long *barrier, unsigned long bit)
{
	unsigned long mask = 1ul << bit;

	atomic_set_mask_release(barrier, mask);
	_arm_dsb("ISH");
	arm_sev();
}

/** wait until expected value is shown in atomic value barrier */
static inline void barrier_wait_val(unsigned int *barrier, unsigned int expect)
{
	unsigned long val;

#ifdef __aarch64__
	arm_sevl();
#endif
	do {
#ifdef __aarch64__
		arm_wfe();
#endif
		val = atomic_load_acquire(barrier);
#ifdef __arm__
		if (val == expect) {
			arm_wfe();
		}
#endif
	} while (val != expect);
}

/** set value in atomic value barrier */
static inline void barrier_set_val(unsigned int *barrier, unsigned int val)
{
	atomic_store_release(barrier, val);
	_arm_dsb("ISH");
	arm_sev();
}

/** set value in atomic value barrier + clean cache */
static inline void barrier_set_val_clean_cache(unsigned int *barrier, unsigned int val)
{
	atomic_store_release(barrier, val);
	arm_dmb();
	arm_clean_dcache(barrier);
	_arm_dsb("ISH");
	arm_sev();
}

#endif
