/* SPDX-License-Identifier: MIT */
/* Copyright 2015 Alexander Zuepke */
/*
 * cpu_timer.h
 *
 * ARM AARCH64 generic per-CPU virtual timer
 *
 * azuepke, 2015-07-24: initial
 * azuepke, 2018-03-05: imported
 */

#ifndef CPU_TIMER_H_
#define CPU_TIMER_H_

void cpu_timer_init(unsigned int freq);
void cpu_timer_start(void);

#endif
