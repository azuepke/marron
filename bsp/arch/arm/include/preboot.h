/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * preboot.h
 *
 * Preboot code -- setup kernel page tables at boot time.
 *
 * NOTE: This code runs in physical addressing mode! Do not call from C code!
 *
 * azuepke, 2021-05-05: initial
 */

#ifndef PREBOOT_H_
#define PREBOOT_H_

#ifdef __aarch64__
#include <arm_pte_v8.h>
#else
#include <arm_pte_v6.h>
#endif


/* check variable if the preboot phase succeeded */
extern unsigned int bsp_preboot_ok;


/* Setup kernel page tables (do not call from C code) */
#ifdef __aarch64__
void preboot_setup_tagetables(
	arm_pte_t *ttbr0_l1,
	arm_pte_t *ttbr1_l1,
	unsigned long load_addr);
#else
void preboot_setup_tagetables(
	arm_pte_t *pd,
	unsigned long load_addr);
#endif

/* initialize first 64-bytes of all shared memory segments to zero */
__init void preboot_init_shms(void);

#endif
