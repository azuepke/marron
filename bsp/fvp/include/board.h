/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021, 2023 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for Arm FVP.
 *
 * azuepke, 2023-08-12: adapted from qemu-arm
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* pl011_uart.c */
void serial_init(unsigned int baud);

/* bsp.c */
unsigned long gic_cpu_sgir_id(unsigned int cpu_id);
unsigned long gic_cpu_router_id(unsigned int cpu_id);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_ICC_MODE		1	/* use ICC_x_EL1 registers to access GICC */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_DIST_PHYS		CFG_IO_PHYS_gic_dist
#define GIC_REDIST_BASE		CFG_IO_ADDR_gic_redist
#define GIC_REDIST_PHYS		CFG_IO_PHYS_gic_redist
#define GIC_REDIST_SIZE		0x20000		/* GICv3: 128K, GICv4: 256K */
#define GIC_NUM_IRQS		(32 + 224)
#define GIC_NUM_CORES		8
/* GICv3: translate logical CPU ID to REDIST offset, SGIR ID, or IROUTER ID */
#define GIC_CPU_REDIST_ID(x)	(x)
#define GIC_CPU_SGIR_ID(x)		gic_cpu_sgir_id(x)
#define GIC_CPU_ROUTER_ID(x)	gic_cpu_router_id(x)

/* we have specific settings for the CPU */
/* we have two ARM clusters with four cores each */
#define ARM_MPIDR_AFF0_BITS 0
#define ARM_MPIDR_AFF1_BITS 2
#define ARM_MPIDR_AFF2_BITS 2
#define ARM_MPIDR_AFF3_BITS 0


#define CPU_COUNTER_FREQ	100000000	/* 100 MHz */

#define SP804_TIMER_BASE		CFG_IO_ADDR_sp804_timer
#define SP804_TIMER_CLOCK		1000000	/* 1 MHz */
#define SP804_TIMER_IRQ			34

/*
 * PL011 UARTs on Versatile Express board:
 *
 * UART0: 0x1c090000
 * UART1: 0x1c0a0000
 * UART2: 0x1c0b0000
 * UART3: 0x1c0c0000
 */
#define PL011_SERIAL_REGS	CFG_IO_ADDR_uart0
#define PL011_SERIAL_CLOCK	24000000

/** enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
