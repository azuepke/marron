# SPDX-License-Identifier: MIT
# Copyright 2013, 2021, 2023 Alexander Zuepke

MARRON_BSP_NAME="ARM Virtual Fixed Platform (FVP) Versatile Express (VE) with ARM Cortex A7x"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a78
MARRON_SMP=yes
