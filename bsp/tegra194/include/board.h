/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for NVIDIA Xavier
 *
 * azuepke, 2021-04-18: cloned from TX2
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS		(32 + 288)
/* two clusters of cores */
#define GIC_CPU_BIT(x)		(1u << (x))

/* we have specific settings for the CPU */
/* On Xavier, we have four ARM clusters with two Carmel cores each. */
#define ARM_MPIDR_AFF0_BITS 1
#define ARM_MPIDR_AFF1_BITS 2
#define ARM_MPIDR_AFF2_BITS 0
#define ARM_MPIDR_AFF3_BITS 0

/*
 * UARTs on TX2:
 *
 * UART1: 0x03100000  8250 UART
 * UART2: 0x03110000
 * UART3: 0x0c280000
 * UART4: 0x03130000
 * UART5: 0x03140000
 * UART6: 0x03150000
 * UART7: 0x0c290000
 * UART8: 0x03170000
 */
#define NS16550_UART_REGS		CFG_IO_ADDR_uart3
#define NS16550_UART_REGSIZE	4
#if 0
#define NS16550_UART_CLOCK		408000000	/* 408 MHz APB clock */
#endif
#define NS16550_UART_KEEPBAUD	1	/* keep baudrate of bootloader */

#if 0
#define CPU_COUNTER_FREQ	31250000	/* 31.25 MHz */
#endif

#endif
