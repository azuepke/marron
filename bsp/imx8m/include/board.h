/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021, 2022 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for NXP i.MX 8MQ.
 *
 * azuepke, 2015-07-13: initial
 * azuepke, 2021-01-16: adapted to mini-OS
 * azuepke, 2022-07-10: ported from qemu-aarch64
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* imx_uart.c */
void serial_init(unsigned int baud);

#endif


#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_ICC_MODE		1	/* use ICC_x_EL1 registers to access GICC */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_REDIST_BASE		CFG_IO_ADDR_gic_redist
#define GIC_REDIST_SIZE		0x20000		/* GICv3: 128K, GICv4: 256K */
#define GIC_NUM_IRQS		(32 + 128)
#define GIC_NUM_CORES		4
/* GICv3: translate logical CPU ID to REDIST offset, SGIR ID, or IROUTER ID */
#define GIC_CPU_REDIST_ID(x)	(x)
#define GIC_CPU_SGIR_ID(x)		(1ul << x)
#define GIC_CPU_ROUTER_ID(x)	(x)

/*
 * UARTs on the i.MX8:
 * UART1: 0x30860000
 * UART2: 0x30890000
 * UART3: 0x30880000
 * UART4: 0x30a60000
 */
#define IMX_UART_REGS		CFG_IO_ADDR_uart1
#define IMX_UART_CLOCK		25000000

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
