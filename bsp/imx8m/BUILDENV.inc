# SPDX-License-Identifier: MIT
# Copyright 2022 Alexander Zuepke

MARRON_BSP_NAME="NXP i.MX 8M with ARM Cortex A53"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a53
MARRON_SMP=yes
