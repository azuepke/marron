# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="Raspberry Pi 2 Model B V1.1 with ARM Cortex A7"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a7
MARRON_SMP=yes
