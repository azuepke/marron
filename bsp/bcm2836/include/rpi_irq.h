/* SPDX-License-Identifier: MIT */
/* Copyright 2020 Alexander Zuepke */
/*
 * rpi_irq.h
 *
 * Raspberry Pi BCM2836 IRQ handling
 *
 * azuepke, 2020-08-05: initial
 */

#ifndef RPI_IRQ_H_
#define RPI_IRQ_H_

/* primary interrupt controller (BCM2836) */
#define IRQ_ID_PTIMER		0	/* physical timer */
#define IRQ_ID_NS_TIMER		1	/* non-secure physical timer */
#define IRQ_ID_HTIMER		2	/* hypervisor timer */
#define IRQ_ID_VTIMER		3	/* virtual timer */
#define IRQ_ID_MBOX0		4
#define IRQ_ID_MBOX1		5
#define IRQ_ID_MBOX2		6
#define IRQ_ID_MBOX3		7
#define IRQ_ID_GPU			8	/* secondary interrupt controller */
#define IRQ_ID_PMU			9

/* secondary interrupt controller (BCM2835) */
// 32 ARM_TIMER
// 33 ARM_MAILBOX
// 34 ARM_DOORBELL_0
// 35 ARM_DOORBELL_1
#define IRQ_ID_GPU_HALT0	36
#define IRQ_ID_GPU_HALT1	37
#define IRQ_ID_ARM_ADDR_ERROR	38
#define IRQ_ID_ARM_AXI_ERROR	39
// 40..52 selected GPU IRQs 7, 9, 10, 18, 19, 53, 54, 55, 56, 57, 62; do not use
// 64..95 IRQs in GPU_IRQ_PENDING1 (GPU IRQs 0..31)
// 96..127 IRQs in GPU_IRQ_PENDING2  (GPU IRQs 32..63)

void bsp_send_stop(void);
void bsp_irq_init(void);

#endif
