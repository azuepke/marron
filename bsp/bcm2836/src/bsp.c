/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021 Alexander Zuepke */
/*
 * bsp.c
 *
 * Board initialization for Raspberry Pi 2 Model B V1.1 (BCM2836)
 * This BSP also covers the Raspberry Pi 3 Model B (BCM2837).
 *
 * azuepke, 2020-08-05: cloned from QEMU BSP
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <rpi_irq.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <arch_io.h>
#include <preboot.h>


/** BSP name string */
#if defined(BCM2836)
const char bsp_name[] = "bcm2836";
#elif defined(BCM2837)
const char bsp_name[] = "bcm2837";
#else
#error unknown chip
#endif

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		bsp_irq_init();
		cpu_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		/* empty */
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
#ifdef __aarch64__
	__asm__ volatile ("dsb sy; wfi" : : : "memory");
#else /* 32-bit ARM */
	__asm__ volatile ("dsb; wfi" : : : "memory");
#endif
}


/* watchdog registers */
#define WDOG_RSTC	0x1c
#define WDOG_RSTS	0x20
#define WDOG_WDOG	0x24

/* register accessors */
static inline uint32_t wdog_rd(unsigned long reg)
{
	return read32(io_ptr(WDOG_REGS, reg));
}

static inline void wdog_wr(unsigned long reg, uint32_t val)
{
	write32(io_ptr(WDOG_REGS, reg), val);
}

static inline void bsp_reset(void)
{
	uint32_t rstc;

	rstc = wdog_rd(WDOG_RSTC);
	rstc &= ~0x30;
	rstc |= 0x20;

	wdog_wr(WDOG_WDOG, 0x5a00000d);
	wdog_wr(WDOG_RSTC, 0x5a000000 | rstc);
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		bsp_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		bsp_reset();
	}

	__bsp_halt();
	unreachable();
}
