# SPDX-License-Identifier: MIT
# Copyright 2024 Alexander Zuepke

MARRON_BSP_NAME="TI TDA4VM with ARM Cortex A72"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a72
MARRON_SMP=yes
