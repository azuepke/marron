/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2020-2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for Beaglebone Black with ARM Cortex A8.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored board.h
 * azuepke, 2013-12-23: Beaglebone Black port
 * azuepke, 2020-01-31: adapted to Marron
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* ti_uart.c */
void serial_init(unsigned int baud);

/* dmtimer.c */
void dmtimer_init(unsigned int freq);

/* intc.c */
void irq_init(void);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/* SoC specific addresses */
/* UART0 */
#define TI_UART_REGS			CFG_IO_ADDR_uart0
#define TI_UART_CLOCK			48000000	/* 48 MHz */

/* DMTIMER0 */
#define DMTIMER_BASE		CFG_IO_ADDR_dmtimer0
#define DMTIMER_IRQ			66
#define DMTIMER_CLOCK		32768	/* 32 kHz */
/* FIXME: better use DMTimer2? -- 0x48040000, IRQ 68, 25 MHz(?) clock */

/* INTC */
#define INTC_BASE			CFG_IO_ADDR_intc

/* PRM */
#define PRM_BASE			(CFG_IO_ADDR_prm_device + 0x0f00)

#endif
