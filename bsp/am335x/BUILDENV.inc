# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="TI AM335x with ARM Cortex A8"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a8
MARRON_SMP=no
