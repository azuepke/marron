# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="NVIDIA Tegra X2 with 2x Denver 2 + 4x Cortex A57"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a57
MARRON_SMP=yes
