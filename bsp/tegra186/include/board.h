/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for NVIDIA Jetson TX2
 *
 * azuepke, 2021-03-11: cloned from QEMU BSP
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

/* bsp.c */
unsigned int gic_cpu_id(unsigned int cpu_id);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS		(32 + 288)
/* two clusters of cores */
#define GIC_CPU_BIT(x)		(1u << gic_cpu_id(x))

/* we have specific settings for the CPU */
/* On TX2, we have two ARM clusters:
 * - cluster 0 is the two Denver cores.
 * - cluster 1 is the four A57 cores.
 * We boot on the first core on cluster 1 (an A57 core), so
 * we map cluster 0 to cores 0..3
 * and map cluster 1 to cores 4..5.
 */
#define ARM_MPIDR_AFF0_BITS 2
#define ARM_MPIDR_AFF1_BITS 1
#define ARM_MPIDR_AFF2_BITS 0
#define ARM_MPIDR_AFF3_BITS 0
#define ARM_MPIDR_REMAP_TABLE 4, 5, -1, -1, 0, 1, 2, 3

/*
 * UARTs on TX2:
 *
 * UART1: 0x03100000  8250 UART
 * UART2: 0x03110000
 * UART3: 0x03120000
 * UART4: 0x03130000
 * UART5: 0x03140000
 * UART6: 0x03150000
 */
#define NS16550_UART_REGS		CFG_IO_ADDR_uart1
#define NS16550_UART_REGSIZE	4
#define NS16550_UART_CLOCK		408000000	/* 408 MHz APB clock */
#define NS16550_UART_KEEPBAUD	1	/* keep baudrate of bootloader */

#define CPU_COUNTER_FREQ	31250000	/* 31.25 MHz */

#endif
