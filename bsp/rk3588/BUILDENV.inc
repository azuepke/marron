# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="Firefly ITX-3588J with RK3588 with 4x Cortex-A55 and 4x Cortex-A76 (64-bit)"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a76
MARRON_SMP=yes
