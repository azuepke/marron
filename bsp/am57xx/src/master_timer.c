/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Andreas Werner */

//Errata i940: MPU COUNTER_REALTIME saturates after several hundred days
//FIXME: Use GP Timer for CPU Tick Interrupt
#include <stdint.h>
#include <omap-smc.h>
#include <board.h>
#include <master_timer.h>
#define SYS_MODE_NUMERATOR_MASK 0x7FF
#define DENOMINATOR_MASK 0x7FF
#define RELOAD (1 << 16)
void master_timer_init(void) {
	uint32_t *PRM_FRAC_INCREMENTER_NUMERATOR = (uint32_t *)(CFG_IO_ADDR_mpu_prcm + 0x210);
	uint32_t *PRM_FRAC_INCREMENTER_DENUMERATOR_RELOAD = (uint32_t *)(CFG_IO_ADDR_mpu_prcm + 0x214);
	uint32_t reg;
	uint32_t n;
	uint32_t d;

	/* FIXME: change constant values to calculate values base on clock tree */
	n = 75;
	d = 244;
	reg = *PRM_FRAC_INCREMENTER_NUMERATOR;
	reg &= ~SYS_MODE_NUMERATOR_MASK;
	reg |= (n & SYS_MODE_NUMERATOR_MASK);
	*PRM_FRAC_INCREMENTER_NUMERATOR = reg;
	reg = *PRM_FRAC_INCREMENTER_DENUMERATOR_RELOAD;
	reg &= ~DENOMINATOR_MASK;
	reg |= (d & DENOMINATOR_MASK);
	reg |= RELOAD;
	*PRM_FRAC_INCREMENTER_DENUMERATOR_RELOAD = reg;
	omap_smc1(OMAP5_DRA7_MON_SET_CNTFRQ_INDEX, 6147541 /* MHz */);
}
