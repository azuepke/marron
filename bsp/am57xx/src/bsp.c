/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021 Alexander Zuepke */
/*
 * board.c
 *
 * Board initialization for QEMU Versatile Express Cortex A15.
 *
 * azuepke, 2013-11-19: initial cloned
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <gic.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <master_timer.h>
#include <preboot.h>
#include <arch_io.h>
#include <barrier.h>


/** BSP name string */
const char bsp_name[] = "am57xx";

#ifdef SMP
// CPUs online
unsigned long bsp_cpu_online_mask = 0;
// CPUs started scheduling
unsigned int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	volatile uint32_t *PM_CPU1_PWRSTCTRL = io_ptr(CFG_IO_ADDR_mpu_prcm, 0x800);
	volatile uint32_t *RM_CPU1_CPU1_RSTCTRL = io_ptr(CFG_IO_ADDR_mpu_prcm, 0x810);
	volatile uint32_t *WKG_CONTROL_1 = io_ptr(CFG_IO_ADDR_mpu_wugen, 0x400);
	volatile uint32_t *AUX_CORE_BOOT = io_ptr(CFG_IO_ADDR_mpu_wugen, 0x800);
	uint32_t val;
	printk("Start CPU1\n");

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	/**
	 * Check CPU is Power UP
	 */
	val = read32(PM_CPU1_PWRSTCTRL);
	if ((val & (1 << 7))) {
		printk("Disable forced off state\n");
		/* disable forced in OFF state */
		val &= ~(1 << 7);
		write32(PM_CPU1_PWRSTCTRL, val);
		arm_dmb();
	}
	if ((val & 0x3) != 0x3) {
		printk("Power and Reset CPU1\n");
		/* Set CPU to ON State */
		val |= 0x3;
		write32(PM_CPU1_PWRSTCTRL, val);
	}
	/* reset CPU1 */
	val = read32(RM_CPU1_CPU1_RSTCTRL);
	val |= 0x1;
	write32(RM_CPU1_CPU1_RSTCTRL, val);
	arm_dmb();
	/* relase the reset of CPU1 */
	val &= ~0x1;
	write32(RM_CPU1_CPU1_RSTCTRL, val);
	arm_dmb();

	write32(&AUX_CORE_BOOT[1], (uint32_t) 0x80008000);
	write32(&AUX_CORE_BOOT[0], 0x00000020);
	arm_dmb();
	printk("Wakeup CPU1\n");
	val = read32(WKG_CONTROL_1);
	val |= (1 << 9);
	write32(WKG_CONTROL_1, val);
	arm_dsb();
	arm_sev();

	/* wait until CPU1 is up */
	barrier_wait_bit(&bsp_cpu_online_mask, 1);
	printk("CPU 1 is online\n");
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu_id)
{
	assert(cpu_id == bsp_cpu_id());
	barrier_set_bit(&bsp_cpu_online_mask, cpu_id);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu_id == 0) {
		barrier_set_val(&bsp_cpu_go, 1);
	} else {
		/* wait until we see the "go" signal */
		barrier_wait_val(&bsp_cpu_go, 1);
	}

	/* start timer here on SMP */
	cpu_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		master_timer_init();

		gic_init_dist();
		gic_init_cpu(cpu_id);
		cpu_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		gic_init_cpu(cpu_id);
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb; wfi" : : : "memory");
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode __unused)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		gic_send_stop();
	}
#endif

	__bsp_halt();
	unreachable();
}
