/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Andreas Werner */

#include <kernel.h>
#include <stdint.h>
#include <gic.h>
#include <board.h>
#include <irq_crossbar.h>
#include <irq_types.h>

#define AM57xx_IRQ_MAP_MPU_BASE ((uint32_t *)(CFG_IO_ADDR_ctrl_core + 0xa48))

enum am57xx_irq_status {
	IRQ_FREE,
	IRQ_USED,
	IRQ_SKIP,
	IRQ_RESERVED,
};

struct am57xx_irq_map {
  uint32_t irq;
  enum am57xx_irq_status status;
  int32_t offset;
};

static struct am57xx_irq_map irqMap[159];
#define IRQ_NOT_MAPED 0xFFFF
uint32_t crossbarToIrq[400];

#define IRQ_GIC_NUM_SHREAD_IRQS (gic_num_irqs - IRQ_ID_FIRST_SPI)
#define ARRAY_SIZE(array) (sizeof(array) / sizeof((array)[0]))

#define DEBUG_PRINTF_RAW(format, ...)
//#define DEBUG_PRINTF_RAW(format, ...) printk(format, ##__VA_ARGS__)
#define DEBUG_PRINTF(format, ...) DEBUG_PRINTF_RAW("%s: line: %d " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)

uint32_t am57xx_map_interrupt(uint32_t irqnr)
{
	unsigned int i;
	uint32_t *reg;
	/* am57xx has a interrupt crossbar find new slot */
	/* TODO Add spinlock and move array to shm*/
	DEBUG_PRINTF("IRQ Map irq: %u ", irqnr);
	for (i = 0; i < IRQ_GIC_NUM_SHREAD_IRQS; i++) {
		if (irqMap[i].status == IRQ_USED && irqMap[i].irq == irqnr) {
			/* already muxed */
			DEBUG_PRINTF_RAW(" to: %u\n", i + IRQ_ID_FIRST_SPI);
			return i + IRQ_ID_FIRST_SPI;
		}else if (irqMap[i].status == IRQ_FREE) {
			irqMap[i].status = IRQ_USED;
			irqMap[i].irq = irqnr;
			break;
		}
	}
	if (i == IRQ_GIC_NUM_SHREAD_IRQS) {
		/* no more irq posible */
		return gic_num_irqs;
	}

	/* map IRQ */
	reg = AM57xx_IRQ_MAP_MPU_BASE + (irqMap[i].offset >> 1);
	DEBUG_PRINTF_RAW(" to: %u nr: %d reg: 0x%p", i + IRQ_ID_FIRST_SPI, i,reg);
	if ((irqMap[i].offset & 0x1) == 0) {
		*reg &= ~0x1FF;
		*reg |= irqnr;
		DEBUG_PRINTF_RAW("[8:0]\n");
	} else {
		*reg &= ~(0x1FF << 16);
		*reg |= (irqnr << 16);
		DEBUG_PRINTF_RAW("[24:16]\n");
	}
	return i + IRQ_ID_FIRST_SPI;
}

uint32_t am57xx_crossbar_to_gic(unsigned int irqnr) {
	DEBUG_PRINTF("IRQ Translate: %u ", irqnr);
	/* RAW GIC IRQs Warning overlapping with maped interrupts */
	if (irqnr >= 400) {
		uint32_t irqcheck;
		if (irqnr >= 591) {
			DEBUG_PRINTF_RAW("to: IRQ_NOT_MAPED\n");
			return IRQ_NOT_MAPED;
		}
		/* First 32 irqs are privated IRQs of the CPU */
		if ((irqnr - 400) >= IRQ_ID_FIRST_SPI) {
			/* reverse check */
			irqcheck = am57xx_gic_to_crossbar(irqnr - 400);
			if (irqcheck != (irqnr - 400)) {
				/* irq is accessble over IRQ Mapping! */
				return IRQ_NOT_MAPED;
			}
		}
		return irqnr-400;
	}
	DEBUG_PRINTF_RAW("to: %u", crossbarToIrq[irqnr]);
	return crossbarToIrq[irqnr];
}

uint32_t am57xx_gic_to_crossbar(unsigned int irqnr) {
	DEBUG_PRINTF("IRQ Translate: %u ", irqnr);
	/* subtract private irqs */
	irqnr -= IRQ_ID_FIRST_SPI;
	if (irqnr > IRQ_GIC_NUM_SHREAD_IRQS) {
		DEBUG_PRINTF_RAW("to: IRQ_NOT_MAPED\n");
		return IRQ_NOT_MAPED;
	}
	switch (irqMap[irqnr].status) {
		case IRQ_USED:
			DEBUG_PRINTF_RAW("to: %u\n", irqMap[irqnr].irq);
			return irqMap[irqnr].irq;
		case IRQ_RESERVED:
		case IRQ_SKIP:
			DEBUG_PRINTF_RAW("to: IRQ_RESERVED or IRQ SKIP\n");
			return irqnr + IRQ_ID_FIRST_SPI + 400;
		default:
		case IRQ_FREE:
			DEBUG_PRINTF_RAW("to: IRQ_NOT_MAPED\n");
			return IRQ_NOT_MAPED;
	}
	/* never reach */
}

void am57xx_irq_crossbar_init(void)
{
	uint32_t *reg;
	unsigned int i;
	/* reserved <0 1 2 3 5 6 131 132> skip: <10 133 139 140>*/
	/* FIXME: Get info out of device tree */
	int reserved[] = {0, 1, 2, 3, 5, 6, 131, 132};
	int skip[] = {10, 133, 139, 140};
	int offset;
	for (i = 0; i < IRQ_GIC_NUM_SHREAD_IRQS; i++) {
		irqMap[i].status = IRQ_FREE;
	}
	for (i = 0; i < ARRAY_SIZE(reserved); i++) {
		irqMap[reserved[i]].status = IRQ_RESERVED;
	}
	for (i = 0; i < ARRAY_SIZE(skip); i++) {
		irqMap[skip[i]].status = IRQ_SKIP;
	}
	offset = 0;
	/* Init GIC to Crossbar map */
	for (i = 0; i < IRQ_GIC_NUM_SHREAD_IRQS; i++) {
		if (irqMap[i].status == IRQ_RESERVED) continue;
		irqMap[i].offset = offset;
		if (irqMap[i].status != IRQ_SKIP) {
			reg = AM57xx_IRQ_MAP_MPU_BASE + (offset >> 1);
			/* set all IRQs to Reserved */
			if ((offset & 0x1) == 0) {
				*reg |= 0x1FF;
			} else {
				*reg |= (0x1FF << 16);
			}
		}
		offset++;
	}
	/* Init crossbar to GIC IRQ map */
  	for (i = 0; i < 400; i++) {
		crossbarToIrq[i] = IRQ_NOT_MAPED;
	}
	/* Map all interrupts */
  	{
		const struct irq_cfg *cfg;
		for (i = 0; i < irq_num; i++) {
			cfg = &irq_cfg[i];
			if (cfg->irq_id >= 400) {
				/* ignore interrupt */
				continue;
			}
			crossbarToIrq[cfg->irq_id] = am57xx_map_interrupt(cfg->irq_id);
		}
	}
}
