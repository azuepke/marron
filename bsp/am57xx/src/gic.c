/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * gic.c
 *
 * ARM Generic Interrupt Controller (GIC) specific IRQ handling
 * supports GICv1 and GICv2
 *
 * azuepke, 2013-09-18: initial
 * azuepke, 2013-11-20: split into IRQ and timer code
 * azuepke, 2020-11-23: cleanup
 * azuepke, 2021-12-02: refactor from mpcore.c to gic.c
 */

#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <assert.h>
#include <arch_io.h>
#include <arm_insn.h>
#include <board.h>
#include <gic.h>
#include <irq_crossbar.h>
#include <current.h>


/* real number of interrupts (the GIC supports up to 1024) */
unsigned int gic_num_irqs;

// Number of IRQs exported by the BSP
/* this number is not equal to GIC max! */
const unsigned int bsp_irq_num = 591;

// Number of CPUs exported by the BSP
const unsigned int bsp_cpu_num = CFG_CPU_NUM;

/* CPU that actually booted the kernel (always CPU #0 on ARM) */
unsigned int bsp_boot_cpu_id = 0;

static bsp_irq_handler_t irq_handlers[GIC_NUM_IRQS];

/* default interrupt handlers */

static void resched_irq_handler(unsigned int irq __unused)
{
#ifdef SMP
	kernel_ipi();
#endif
}

/* real stopping flag -- spurious SGIs might be pending at boot time */
static int stopping;

static __cold void stop_irq_handler(unsigned int irq __unused)
{
	if (stopping) {
		__bsp_halt();
	}
}


/*
 * GIC per-CPU and distributor registers.
 *
 * Based on the following TRMs (Technical Reference Manual) by ARM:
 *  DDI0416B -- PrimeCell Generic Interrupt Controller (PL390), Revision: r0p0
 *  IHI0048A -- ARM Generic Interrupt Controller, Architecture version 1.0
 *  IHI0048B -- ARM Generic Interrupt Controller, Architecture version 2.0
 *  DDI0388I -- Cortex-A9, Revision: r4p1
 *  DDI0407I -- Cortex-A9 MPCore, Revision: r4p1
 *  DDI0471B -- GIC-400, Revision: r0p1
 *
 * Interrupt types:
 *  SGI: software generated interrupts, 0..15
 *  PPI: private peripheral interrupt, 16..31
 *  SPI: shared peripheral interrupt, 32..1023 (max, in multiples of 32)
 *
 * Conventions:
 *  GICD: distributor, global (DIST_xxx below)
 *  GICC: physical GIC CPU interface (PERCPU_xxx below)
 *  GICV: virtual GIC CPU interface (VM)
 *  GICH: virtual interface control registers (hypervisor)
 */


/* registers in per-CPU area, relative to GIC_BASE + 0x0100 (Cortex-A9)
 *                                     or GIC_BASE + 0x2000 (all others) */
#define PERCPU_CTLR		0x000	/* CPU interface control register */
	#define PERCPU_CTLR_ENABLE		0x00000001
	#define PERCPU_CTLR_EOIMODE		0x00000200
#define PERCPU_PMR		0x004	/* priority mask register */
#define PERCPU_BPR		0x008	/* binary point register */
#define PERCPU_IAR		0x00c	/* interrupt acknowledge register */
#define PERCPU_EOIR		0x010	/* end of interrupt register */
#define PERCPU_RPR		0x014	/* running priority register */
#define PERCPU_HPPIR	0x018	/* highest priority pending interrupt register */
#define PERCPU_DIR		0x1000	/* deactivate interrupt register (GIC v2+) */

#define INTID_MASK		0x3ff
/* NOTE: special interrupts 1020 to 1022 are only reported to EL3 */
#define INTID_SPURIOUS	1023

/* registers in the distributor area, relative to GIC_BASE + 0x1000 */
#define DIST_CTLR		0x000	/* distributor control register */
	#define DIST_CTLR_ENABLE		0x00000001
#define DIST_TYPER		0x004	/* read-only ID register */

#define DIST_ENABLE		0x100	/* write 1 to enable an interrupt */
#define DIST_DISABLE	0x180	/* write 1 to disable an interrupt */

#define DIST_PENDING	0x280	/* write 1 to clear an interrupt */

#define DIST_PRIO		0x400	/* per interrupt priority, byte accessible */
#define DIST_TARGET		0x800	/* per interrupt target, byte accessible */

#define DIST_ICFGR		0xc00	/* interrupt configuration registers */
	/* 2 bits per IRQ:
	 *  0x  level
	 *  1x  edge
	 *  x0  N:N model: all processors handle the interrupt
	 *  x1  1:N model: only one processor handles the interrupt
	 */

#define DIST_SGIR		0xf00	/* software generated interrupt register */
	#define DIST_SGI_ALL_BUT_SELF	0x01000000
	#define DIST_SGI_SELF_ONLY		0x02000000
	/* bits 23..16: target CPUs */
	/* bits 3..0: interrupt ID */


/* GIC specific register accessors */

/* access to per-CPU specific registers */
static inline uint32_t percpu_read32(unsigned long reg)
{
	return read32(io_ptr(GIC_PERCPU_BASE, reg));
}

static inline void percpu_write32(unsigned long reg, uint32_t val)
{
	write32(io_ptr(GIC_PERCPU_BASE, reg), val);
}

/* access to distributor registers */
static inline uint32_t dist_read32(unsigned long reg)
{
	return read32(io_ptr(GIC_DIST_BASE, reg));
}

static inline void dist_write32(unsigned long reg, uint32_t val)
{
	write32(io_ptr(GIC_DIST_BASE, reg), val);
}

static inline uint8_t dist_read8(unsigned long reg)
{
	return read8(io_ptr(GIC_DIST_BASE, reg));
}

static inline void dist_write8(unsigned long reg, uint8_t val)
{
	write8(io_ptr(GIC_DIST_BASE, reg), val);
}



#ifdef SMP
/** send stop request to other cores */
void __cold gic_send_stop(void)
{
	stopping = 1;
	arm_dsb();
	dist_write32(DIST_SGIR, DIST_SGI_ALL_BUT_SELF | IRQ_ID_STOP);
}

/** send rescheduling request to another processor */
void bsp_cpu_reschedule(unsigned int cpu)
{
	arm_dsb();
	dist_write32(DIST_SGIR, (GIC_CPU_BIT(cpu) << 16) | IRQ_ID_RESCHED);
}
#endif

/** initialize the GIC distributed interrupt controller */
__init void gic_init_dist(void)
{
	unsigned int cpu_num;
	unsigned int irq;
	unsigned int val;
	unsigned int i;

	val = dist_read32(DIST_TYPER);
#ifdef SMP
	cpu_num = ((val >> 5) & 0x7) + 1;
	printk("GIC supports %u CPUs\n", cpu_num);
#endif
	gic_num_irqs = ((val & 0x1f) + 1) * 32;
	printk("GIC supports %u IRQs\n", gic_num_irqs);

	/* mask all interrupts and clear pending bits */
	for (i = 0 / 32; i < gic_num_irqs / 32; i++) {
		dist_write32(DIST_DISABLE + i * 4, 0xffffffff);
		dist_write32(DIST_PENDING + i * 4, 0xffffffff);
	}

	/* set all interrupts to max prio */
	for (i = 0 / 1; i < gic_num_irqs / 1; i++) {
		/* highest possible priority */
		dist_write8(DIST_PRIO + i, 0x00);
	}
	/* assign to first CPU as default */
	for (i = 0 / 1; i < gic_num_irqs / 1; i++) {
		dist_write8(DIST_TARGET + i, 0x01);
	}

	/* FIXME: need to load a valid edge/level configuration for each interrupt! */
	/* The reset value for the different interrupts depends on the CPU type.
	 *
	 * SGI interrupts always use 0xaaaaaaaa (edge-triggered)
	 *
	 * PPI interrupts depend on the CPU type:
	 *
	 * - Cortex-A9/A5:
	 *   IRQ	Interrupt	Name						Type
	 *    27	PPI0		global timer				rising-edge
	 *    28	PPI1		legacy nFIQ					active-LOW
	 *    29	PPI2		private timer				rising-edge
	 *    30	PPI3		watchdog					rising-edge
	 *    31	PPI4		legacy nIRQ					active-LOW
	 *
	 * - Cortex-A15/A7/GIC-400:
	 *   IRQ	Interrupt	Name						Type
	 *    25	PPI6		virtual maintenance			active-HIGH
	 *    26	PPI5		hypervisor timer			active-LOW
	 *    27	PPI4		virtual timer				active-LOW
	 *    28	PPI0		legacy nFIQ					active-LOW
	 *    29	PPI1		secure physical timer		active-LOW
	 *    30	PPI2		non-secure physical timer	active-LOW
	 *    31	PPI3		nIRQ						active-LOW
	 *
	 * Note that the PPI assignment is different!
	 *
	 * SPI interrupts always use 0x55555555 (active-HIGH)
	 */
	dist_write32(DIST_ICFGR + 0, 0xaaaaaaaa);
#if defined(ARM_CORTEX_A9) || defined(ARM_CORTEX_A5)
	dist_write32(DIST_ICFGR + 4, 0x7dc00000);
#else
	dist_write32(DIST_ICFGR + 4, 0x55540000);
#endif
	for (i = 32 / 16; i < gic_num_irqs / 16; i++) {
		dist_write32(DIST_ICFGR + i * 4, 0x55555555);
	}

	/* set bit 0 to globally enable the GIC */
	val = dist_read32(DIST_CTLR);
	val |= DIST_CTLR_ENABLE;
	dist_write32(DIST_CTLR, val);

	/* setup default interrupt handler */
	for (irq = 0; irq < GIC_NUM_IRQS; irq++) {
		irq_handlers[irq] = kernel_irq;
	}

	/* register special interrupt handlers */
	irq_handlers[IRQ_ID_STOP] = stop_irq_handler;
	irq_handlers[IRQ_ID_RESCHED] = resched_irq_handler;

	am57xx_irq_crossbar_init();
}

/** enable the CPU specific GIC parts */
__init void gic_init_cpu(unsigned int cpu_id __unused)
{
	unsigned int val;

	/* mask and clear all private interrupts */
	dist_write32(DIST_DISABLE + 0, 0xffffffff);
	dist_write32(DIST_PENDING + 0, 0xffffffff);

	/* prio 255 let all interrupts pass through, no preemption */
	percpu_write32(PERCPU_PMR, 0xff);
	percpu_write32(PERCPU_BPR, 0x7);

	/* set bit 0 to enable the GIC CPU interface */
	val = percpu_read32(PERCPU_CTLR);
	val |= PERCPU_CTLR_ENABLE;
#if 0
	/* NOTE: no need for PERCPU_CTLR_EOIMODE without hardware virtualization */
	val |= PERCPU_CTLR_EOIMODE;
#endif
	percpu_write32(PERCPU_CTLR, val);
}


/** check if the user may attach to an interrupt:
 * returns:
 *  1 -> OK
 *  0 -> not available or not suitable for sharing
 * -1 -> out of range
 */
// Configure interrupt source
// this call returns zero if the interrupt source was configured to the
// requested mode, or non-zero if not.
// the mode "IRQ_AUTO" always succeeds if the interrupt source is available.
// the call also fails if the interrupt is not available
err_t bsp_irq_config(unsigned int irq, unsigned int mode __unused)
{
	/* remap irq */
	irq = am57xx_crossbar_to_gic(irq);
	assert(irq < gic_num_irqs);

	if (irq < IRQ_ID_FIRST_SPI)	/* private or special */
		return EBUSY;
	if (irq >= gic_num_irqs)	/* not available */
		return EBUSY;

	return EOK;
}

/** register a handler with an interrupt */
void bsp_irq_register(unsigned int irq, bsp_irq_handler_t handler)
{
	/* remap irq */
	irq = am57xx_crossbar_to_gic(irq);
	assert(irq < gic_num_irqs);

	irq_handlers[irq] = handler;
}

/** retrieve currently registered interrupt handler */
bsp_irq_handler_t bsp_irq_handler(unsigned int irq)
{
	/* remap irq */
	irq = am57xx_crossbar_to_gic(irq);
	assert(irq < gic_num_irqs);

	return irq_handlers[irq];
}

/** mask IRQ in distributor */
void bsp_irq_disable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int word, bit;

	/* remap irq */
	irq = am57xx_crossbar_to_gic(irq);
	assert(irq < gic_num_irqs);

	word = irq / 32;
	bit = irq % 32;
	dist_write32(DIST_DISABLE + 4 * word, 1u << bit);
}

/** unmask IRQ in distributor */
void bsp_irq_enable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int word, bit;

	/* remap irq */
	irq = am57xx_crossbar_to_gic(irq);
	assert(irq < gic_num_irqs);

#ifdef SMP
	/* set target processor */
	dist_write8(DIST_TARGET + irq, GIC_CPU_BIT(cpu_id));
#endif

	word = irq / 32;
	bit = irq % 32;
	dist_write32(DIST_ENABLE + 4 * word, 1u << bit);
}

/** dispatch IRQ: mask and ack, call handler */
static void bsp_irq_dispatch_internal(void)
{
	unsigned int val;
	unsigned int irq;

	do {
		val = percpu_read32(PERCPU_IAR);

		irq = val & INTID_MASK;
		if (irq < 32) {
			/* per CPU interrupts, never masked, directly call handler */
			irq_handlers[irq](irq);
		} else if (irq == INTID_SPURIOUS) {
			/* spurious! */
			break;
		} else {
			unsigned int crossbarIRQ;
			/* device interrupt: first mask, then call handler */
			assert(irq < gic_num_irqs);
			crossbarIRQ = am57xx_gic_to_crossbar(irq);
			bsp_irq_disable(crossbarIRQ, current_cpu_id());

			/* call handler */
			irq_handlers[irq](crossbarIRQ);
		}

		percpu_write32(PERCPU_EOIR, val);
#if 0
		/* NOTE: enable together with PERCPU_CTLR_EOIMODE */
		percpu_write32(PERCPU_DIR, val);
#endif

		/* check for further pending interrupts */
		val = percpu_read32(PERCPU_HPPIR);
	} while ((val & INTID_MASK) != INTID_MASK);
}

/** dispatch current IRQ (called from architecture layer) */
void bsp_irq_dispatch(ulong_t vector __unused)
{
	bsp_irq_dispatch_internal();
}

/** dispatch any pending IRQ (called from scheduler) */
void bsp_irq_poll(void)
{
	bsp_irq_dispatch_internal();
}
