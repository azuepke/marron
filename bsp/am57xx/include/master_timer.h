/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Andreas Werner */

#ifndef MASTER_TIMER_H_
#define MASTER_TIMER_H_

void master_timer_init(void);

#endif
