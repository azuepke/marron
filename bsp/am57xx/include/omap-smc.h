/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Andreas Werner */

#ifndef OMAP_SMC_H_
#define OMAP_SMC_H_

#define OMAP5_DRA7_MON_SET_CNTFRQ_INDEX 0x109
#define OMAP5_MON_AMBA_IF_INDEX         0x108
#define OMAP5_DRA7_MON_SET_ACR_INDEX    0x107

void omap_smc1(uint32_t fn, uint32_t arg);

#endif
