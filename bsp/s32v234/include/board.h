/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific code for S32V234 with four Cortex-A53.
 *
 * azuepke, 2021-03-16: adapted from QEMU
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* linflex_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS		(32 + 192)
#define GIC_CPU_BIT(x)		(1u << (x))

/* we have specific settings for the CPU */
/* On S32, we have two ARM clusters with two cores each.
 */
#define ARM_MPIDR_AFF0_BITS 1
#define ARM_MPIDR_AFF1_BITS 1
#define ARM_MPIDR_AFF2_BITS 0
#define ARM_MPIDR_AFF3_BITS 0

/* U-boot releases all cores at the same time; rate limit the booting */
#define QEMU_SMP_STARTUP 1
#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

/*
 * The S32V234 has two UARTs:
 * UART1: 0x40053000
 * UART2: 0x400bc000
 */
#define LINFLEX_UART_REGS	CFG_IO_ADDR_uart0
#define LINFLEX_UART_CLK    66000000

#endif
