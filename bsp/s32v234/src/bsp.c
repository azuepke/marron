/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * bsp.c
 *
 * Board specific code for S32V234 with four Cortex-A53.
 *
 * azuepke, 2015-07-13: cloned from QEMU-ARM
 * azuepke, 2021-01-16: adapted to mini-OS
 * azuepke, 2021-03-16: adapted from QEMU
 */

#include <kernel.h>
#include <stddef.h>
#include <bsp.h>
#include <board.h>
#include <gic.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <panic.h>
#include <fdt.h>
#include <preboot.h>
#include <barrier.h>


/** BSP name string */
const char bsp_name[] = "s32v234";

#ifdef SMP
/* use FDT to locate the CPU release addresses */
static void fdt_print_cpu_release_addr(void *fdt)
{
	fdt_handle_t *fdt_root;
	fdt_handle_t *ctxt;
	fdt_handle_t *p, *q, *r;

	fdt_root = fdt_header_parse(fdt);
	if (fdt_root == NULL) {
		printk("invalid FDT header\n");
		return;
	}

	ctxt = fdt_node_find(fdt_root, "cpus");
	if (ctxt != NULL) {
		p = NULL;
		while ((p = fdt_node_iter(ctxt, p)) != NULL) {
			q = fdt_prop_find(p, "device_type");
			if ((q != NULL) && (strcmp(fdt_prop_data_str(q), "cpu") == 0)) {
				r = fdt_prop_find(p, "cpu-release-addr");
				if (r != NULL) {
					printk("FDT: %s: cpu-release-addr: %lx\n",
					       fdt_node_name(p), fdt64(fdt_prop_data(r)));
				}
			}
		}
	}
}

// CPUs online
unsigned long bsp_cpu_online_mask = 0;
// CPUs started scheduling
unsigned int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	/* U-boot kept the other cores spin-waiting on addresses 0xbff7cb90 */
	unsigned long *magic = (unsigned long*)(CFG_IO_ADDR_boot_spinlocks + 0xb90);
	unsigned int cpu;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	if (__boot_args[0] != 0) {
		printk("FDT located at: %lx\n", __boot_args[0]);
		/* warning: the FDT address range must be mapped! */
		fdt_print_cpu_release_addr((void*)__boot_args[0]);
	}

	printk("releasing cores from %p\n", magic);
	//magic[0] = 0;
	//magic[1] = 0;
	magic[2] = LOAD_ADDR;
	arm_clean_dcache(&magic[0]);
	arm_clean_dcache(&magic[1]);
	arm_clean_dcache(&magic[2]);

	/* we flush the caches before doing anything, as the first instructions
	 * of a newly started processor runs in uncached memory mode.
	 */
	for (cpu = 0; cpu < bsp_cpu_num; cpu++) {
		if (cpu == 0) {
			continue;
		}

#ifdef QEMU_SMP_STARTUP
		barrier_set_val_clean_cache(&__boot_release, cpu);
#endif

		/* wait until CPU is up */
		barrier_wait_bit(&bsp_cpu_online_mask, cpu);
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu_id)
{
	assert(cpu_id == bsp_cpu_id());
	barrier_set_bit(&bsp_cpu_online_mask, cpu_id);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu_id == 0) {
		barrier_set_val(&bsp_cpu_go, 1);
	} else {
		/* wait until we see the "go" signal */
		barrier_wait_val(&bsp_cpu_go, 1);
	}

	/* start timer here on SMP */
	cpu_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		gic_init_dist();
		gic_init_cpu(cpu_id);
		cpu_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		unsigned long mpidr;
		__asm__ volatile ("mrs %0, MPIDR_EL1" : "=r"(mpidr));
		printk("MPIDR: %x, ID:%u\n", (unsigned int)mpidr, cpu_id);

		gic_init_cpu(cpu_id);
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb sy; wfi" : : : "memory");
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		gic_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		// FIXME
	}

	__bsp_halt();
	unreachable();
}
