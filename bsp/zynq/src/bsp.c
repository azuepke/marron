/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * board.c
 *
 * Board initialization for NXP IMX6 Cortex A9.
 *
 * azuepke, 2013-09-15: initial
 * azuepke, 2021-10-06: adapted to Marron
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <gic.h>
#include <assert.h>
#include <mpcore_timer.h>
#include <arm_insn.h>
#include <preboot.h>
#include <arch_io.h>
#include <barrier.h>


/** BSP name string */
const char bsp_name[] = "zynq";

#ifdef SMP
// CPUs online
unsigned long bsp_cpu_online_mask = 0;
// CPUs started scheduling
unsigned int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	/* section "6.1.10 Starting Code on CPU 1" in ug585 Zynq 7000 manual:
	 * - CPU 1 kept in WFE
	 * - after receiving SEV:
	 *   - read new PC from 0xfffffff0
	 *   - jumps to new PC
	 */
	volatile uint32_t *vector = io_ptr(START_VECTOR, 0);

	unsigned int cpu;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	write32(vector, BOARD_KERNEL_TO_PHYS((unsigned long)_start));

	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
		arm_dsb();
		arm_sev();

		/* wait until CPU is up */
		barrier_wait_bit(&bsp_cpu_online_mask, cpu);
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu_id)
{
	assert(cpu_id == bsp_cpu_id());
	barrier_set_bit(&bsp_cpu_online_mask, cpu_id);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu_id == 0) {
		barrier_set_val(&bsp_cpu_go, 1);
	} else {
		/* wait until we see the "go" signal */
		barrier_wait_val(&bsp_cpu_go, 1);
	}

	/* start timer here on SMP */
	mpcore_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		gic_init_dist();
		gic_init_cpu(cpu_id);
		mpcore_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		mpcore_timer_start();
#endif
	}
#ifdef SMP
	else {
		printk("secondary CPU %u came up\n", cpu_id);
		gic_init_cpu(cpu_id);
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	/* use WFE instead of WFI to keep the cycle counter running */
	__asm__ volatile ("dsb; wfe" : : : "memory");
}

/** ZYNQ specific reset */
static inline void zynq_reset(void)
{
	/* the SLCR is mapped at 0xf8000000 */

	/* unlock SLCR region */
	write32(io_ptr(0xf8000000, 0x8), 0xDF0D);

	/* reset */
	write32(io_ptr(0xf8000000, 0x200), 1);
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		gic_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		zynq_reset();
	}

	__bsp_halt();
	unreachable();
}
