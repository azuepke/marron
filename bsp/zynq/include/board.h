/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for ARM ZYNQ with Cortex A9.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored board.h
 * azuepke, 2021-10-06: adapted to Marron
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* zynq_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the ZYNQ board */
#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC and MPCore specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		(CFG_IO_ADDR_gic_percpu + 0x100)
#define GIC_NUM_IRQS			96
#define GIC_CPU_BIT(x)			(1u << (x))
#define MPCORE_SCU_BASE_PHYS	CFG_IO_PHYS_scu_base
#define MPCORE_GTIMER_BASE		(CFG_IO_ADDR_scu_base + 0x200)
#define MPCORE_PTIMER_BASE		(CFG_IO_ADDR_scu_base + 0x600)

#if 0
#define MPCORE_TIMER_CLOCK		100000000	/* 100 MHz @ QEMU */
#endif
#define MPCORE_TIMER_CLOCK		333333333	/* 333 MHz @ Zybo Z7 (half CPU freq) */

/*
 * UARTs on the Zynq 7000:
 * UART1: 0xe0000000
 * UART2: 0xe0001000
 */
#define ZYNQ_UART_REGS		CFG_IO_ADDR_uart2
#define ZYNQ_UART_CLOCK		100000000

/** SMP core start vector @ 0xfffffff0 */
#define START_VECTOR		(CFG_IO_ADDR_start_vector + 0xff0)

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
