# SPDX-License-Identifier: MIT
# Copyright 2024 Alexander Zuepke

MARRON_BSP_NAME="TI AM67x with ARM Cortex A53"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a53
MARRON_SMP=yes
