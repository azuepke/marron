# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="Raspberry Pi 3 Model B with ARM Cortex A53 (32-bit)"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a7 ## fake, 32-bit
MARRON_SMP=yes
