/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * fdt.h
 *
 * Firmware device tree (FDT) handling.
 *
 * azuepke, 2021-04-11: initial
 */

#ifndef FDT_H_
#define FDT_H_

#include <stdint.h>
#include <string.h>

/* opaque handle to FDT data */
typedef const uint8_t fdt_handle_t;

/* decode big-endian uint32 data */
static inline uint32_t fdt32(fdt_handle_t *p)
{
	return (p[0] << 24) | (p[1] << 16) | (p[2] << 8) | (p[3] << 0);
}

/* decode big-endian uint64 data */
static inline uint64_t fdt64(fdt_handle_t *p)
{
	return ((uint64_t)fdt32(p+0) << 32) | fdt32(p+4);
}

/** node name */
const char *fdt_node_name(fdt_handle_t *p);

/** length of property data */
uint32_t fdt_prop_len(fdt_handle_t *p);

/** property name */
const char *fdt_prop_name(fdt_handle_t *p);

/** property data */
const void *fdt_prop_data(fdt_handle_t *p);

/** property data as string */
#define fdt_prop_data_str(p) ((const char *)fdt_prop_data(p))

/** check and validate FDT header
  *
  * returns a handle to the FDT root node if a valid FDT header is found;
  * NULL otherwise
  * on success, fdt_node_iter() and fdt_prop_*() services are available
  */
fdt_handle_t *fdt_header_parse(const void *fdt);

/** iterate child nodes in the given context
 *
 * iterate child nodes in the given context;
 * on the first invocation, initialize "p" to NULL;
 * on further invocation, pass the previous return value as "p".
 * returns a pointer to a child node or NULL at the end
 *
 *   fdt_handle_t *p = NULL;
 *   while ((p = fdt_node_iter(ctxt, p)) != NULL) {
 *     ...
 *   }
 */
fdt_handle_t *fdt_node_iter(fdt_handle_t *ctxt, fdt_handle_t *p);

/** find child node by name in the given context */
fdt_handle_t *fdt_node_find(fdt_handle_t *ctxt, const char *node_name);

/** iterate properties in the given context
 *
 * iterate properties in the given context;
 * on the first invocation, initialize "p" to NULL;
 * on further invocation, pass the previous return value as "p".
 * returns a pointer to a property or NULL at the end
 *
 *   fdt_handle_t *p = NULL;
 *   while ((p = fdt_prop_iter(ctxt, p)) != NULL) {
 *     ...
 *   }
 */
fdt_handle_t *fdt_prop_iter(fdt_handle_t *ctxt, fdt_handle_t *p);

/** find child node by name in the given context */
fdt_handle_t *fdt_prop_find(fdt_handle_t *ctxt, const char *prop_name);

#endif
