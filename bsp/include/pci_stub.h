/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * pci_stub.h
 *
 * PCI config space access and PCI hierarchy
 *
 * azuepke, 2025-01-22: initial
 */

#ifndef PCI_STUB_H_
#define PCI_STUB_H_

#include <marron/pcidev.h>

/* read and write PCI config space
 *
 * Offset is 0..255 for the PCI configuration space
 * or 0..4095 for the extended PCI configuration space (if supported).
 *
 * NOTES:
 * - reads from a non-supported device or address return 0xffffffff
 * - writes to a non-supported device or address are ignored
 * - offsets need to be naturally aligned
 * - only 32-bit accesses supported
 */

/* read PCI config space */
uint32_t pci_config_read32(pcidev_t pcidev, uint32_t offset);

/* write PCI config space */
void pci_config_write32(pcidev_t pcidev, uint32_t offset, uint32_t value);

/* get base address of PCI I/O space (I/O ports) if mapped in the kernel
 *
 * The offset of the I/O space is limited to 64KB.
 *
 * NOTES:
 * - reads from a non-supported device or address return 0xffffffff
 * - writes to a non-supported device or address are ignored
 * - offsets need to be naturally aligned
 * - only 8-bit, 16-bit and 32-bit accesses are supported
 * - PCI controllers might offer different I/O space regions,
 *   so we provide the pcidev as well
 */
volatile void *pci_ioport_addr(pcidev_t pcidev, size_t offset);

/* PCI topology: retrieve number of PCI domains (typically 1) */
uint32_t pci_topology_domain_num(void);

/* PCI topology: retrieve number of PCI buses in given domain (up to 256) */
uint32_t pci_topology_domain_bus_num(uint32_t domain);

#endif
