/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * efi.h
 *
 * Unified Extensible Firmware Interface (UEFI) subset for kernel booting
 *
 * Provide a minimal set of UEFI APIs to start a kernel:
 * get the memory map and then call exit_boot_services()
 * to take control of the platform.
 *
 * Based on UEFI Spec Version 2.7 Errata A August 2017
 *
 * NOTE: L"wide-string" requires gcc -fshort-wchar to emit 16-bit wide chars
 *
 * azuepke, 2022-06-03: initial
 */

#ifndef EFI_H_
#define EFI_H_

#include <stdint.h>

/* additional base types */
typedef long intn_t;
typedef unsigned long uintn_t;
typedef unsigned short char16_t;

/* common EFI types */
typedef uintn_t efi_status_t;
typedef void* efi_handle_t;
typedef uint64_t efi_virtual_address_t;
typedef uint64_t efi_physical_address_t;
typedef int efi_reset_type_t;

/* status codes */
#define EFI_SUCCESS 0
#define EFI_ERROR(err) ((1UL << (8 * __SIZEOF_LONG__ - 1)) | (err))
#define EFI_LOAD_ERROR EFI_ERROR(1)
#define EFI_INVALID_PARAMETER EFI_ERROR(2)
#define EFI_BAD_BUFFER_SIZE EFI_ERROR(4)
#define EFI_BUFFER_TOO_SMALL EFI_ERROR(5)

#define EFI_MEMORY_DESCRIPTOR_VERSION 1

/* memory descriptor */
typedef struct efi_memory_descriptor {
	uint32_t type;
	uint32_t padding;
	efi_physical_address_t physical_start;
	efi_virtual_address_t virtual_start;
	uint64_t number_of_pages;
	uint64_t attribute;
} efi_memory_descriptor_t;

/* table header */
typedef struct efi_table_header {
	uint64_t signature;
	uint32_t revision;
	uint32_t header_size;
	uint32_t crc32;
	uint32_t reserved;
} efi_table_header_t;

#define EFI_BOOT_SERVICES_SIGNATURE 0x56524553544f4f42

/* boot services */
typedef struct efi_boot_services {
	efi_table_header_t hdr;

	void *raise_tpl;
	void *restore_tpl;

	void *allocate_pages;
	void *free_pages;
	efi_status_t (*get_memory_map)(
		uintn_t *memory_map_size,
		void *memory_map,
		uintn_t *map_key,
		uintn_t *descriptor_size,
		uint32_t *descriptor_version);
	void *allocate_pool;
	void *free_pool;

	void *create_event;
	void *set_timer;
	void *wait_for_event;
	void *signal_event;
	void *close_event;
	void *check_event;

	void *install_protocol_interface;
	void *reinstall_protocol_interface;
	void *uninstall_protocol_interface;
	void *handle_protocol;
	void *reserved;
	void *register_protocol_notify;
	void *locate_handle;
	void *locate_device_path;
	void *install_configuration_table;

	void *image_load;
	void *image_start;
	efi_status_t (*exit)(
		efi_handle_t image_handle,
		efi_status_t exit_status,
		uintn_t exit_data_size,
		char16_t* exit_data);
	void *image_unload;
	efi_status_t (*exit_boot_services)(
		efi_handle_t image_handle,
		uintn_t map_key);

	void *get_next_monotonic_count;
	void *stall;
	void *set_watchdog_timer;

	/* EFI 1.1 or newer */
	void *connect_controller;
	void *disconnect_controller;

	void *open_protocol;
	void *close_protocol;
	void *open_protocol_information;

	void *protocols_per_handle;
	void *locate_handleBuffer;
	void *locate_protocol;
	void *install_multiple_protocol_interfaces;
	void *uninstall_multiple_protocol_interfaces;

	void *calculate_crc32;

	void *copy_mem;
	void *set_mem;
} efi_boot_services_t;

#define EFI_RESET_COLD 0
#define EFI_RESET_WARM 1
#define EFI_RESET_SHUTDOWN 2
#define EFI_RESET_PLATFORM_SPECIFIC 3

/* runtime services */
typedef struct efi_runtime_services {
	efi_table_header_t hdr;

	void *get_time;
	void *set_time;
	void *get_wakeup_time;
	void *set_wakeup_time;

	void *set_virtual_address_map;
	void *convert_pointer;

	void *get_variable;
	void *get_next_variable_name;
	void *set_variable;

	void *get_next_high_monotonic_count;
	void (*reset_system)(
		efi_reset_type_t reset_type,
		efi_status_t reset_status,
		uintn_t data_size,
		void *reset_data);
} efi_runtime_services_t;


typedef struct efi_simple_text_input_protocol {
	void *reset;
	void *read_key;
	void *wait_for_key;
} efi_simple_text_input_protocol_t;

typedef struct efi_simple_text_output_protocol {
	void *reset;
	efi_status_t (*output_string)(
		struct efi_simple_text_output_protocol *self,
		const char16_t *string);
	void *test_string;
	void *query_mode;
	void *set_mode;
	void *set_attribute;
	void *clear_screen;
	void *set_cursor_position;
	void *enable_cursor;
	void *mode;
} efi_simple_text_output_protocol_t;

/* forward declarations */
typedef struct efi_configuration_table efi_configuration_table_t;

#define EFI_SYSTEM_TABLE_SIGNATURE 0x5453595320494249

/* system table */
typedef struct efi_system_table {
	efi_table_header_t hdr;

	char16_t *firmware_vendor;
	uint32_t firmware_revision;
#if __SIZEOF_LONG__ == 8
	uint32_t padding;
#endif

	efi_handle_t con_in_handle;
	efi_simple_text_input_protocol_t *con_in;

	efi_handle_t con_out_handle;
	efi_simple_text_output_protocol_t *con_out;

	efi_handle_t con_err_handle;
	efi_simple_text_output_protocol_t *con_err;

	efi_runtime_services_t *rts;
	efi_boot_services_t *bts;

	uintn_t number_of_table_entries;
	efi_configuration_table_t *configuration_table;
} efi_system_table_t;


/* private data storage of kernel
 *
 * We keep all information required at boot time in a structure and
 * pass a pointer as parameter to the C entry point using its physical address.
 */
typedef struct efi_private {
	efi_handle_t image_handle;
	efi_system_table_t *system_table;
	efi_runtime_services_t *rts;
	efi_boot_services_t *bts;
	uintn_t number_of_table_entries;
	efi_configuration_table_t *configuration_table;
	uintn_t map_size;
	uintn_t desc_size;

	/* space for memory map, must come last
	 *
	 * NOTE: get_memory_map() deliberately returns
	 * desc_size != sizeof(efi_memory_descriptor_t)
	 * to "prevent people from having pointer math bugs in their code".
	 *
	 * I can only guesstimate that this is a lovely hommage to the fu*#~@
	 * in the e820 API when the BIOS makers increased the e820 entries
	 * from 20 to 24 bytes to include flags for ACPI    -- grrr
	 */
	char map[2*4096 - 8*__SIZEOF_LONG__];
} efi_private_t;

/* private data storage of kernel */
extern efi_private_t _efi_private;

/* EFI entry point in C (priv and continue are our extensions to the API) */
extern efi_status_t efi_entry_preboot(
	efi_handle_t image_handle,
	efi_system_table_t *system_table,
	efi_private_t *priv,
	efi_status_t (*cont)(void));

#endif
