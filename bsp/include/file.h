/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * file.h
 *
 * Access to files in ROM image for BSPs.
 *
 * azuepke, 2022-05-04: initial
 */

#ifndef FILE_H_
#define FILE_H_

#include <stdint.h>


/** find file by name in the ROM image
 *
 * the file name is in the global name space
 * returns a pointer to a handle or NULL if the file is not found
 */
const void *file_find(const char *name);

/** get size of file (handle must be non-NULL) */
size_t file_size(const void *handle);

/** get content of file (read-only data, handle must be non-NULL) */
const void *file_data(const void *handle);

#endif
