/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * pci_config_regs.h
 *
 * PCI config space registers
 *
 * NOTE: PCI config space registers should be accessed by 32-bit accesses only.
 *
 * azuepke, 2025-01-22: initial
 */

#ifndef PCI_CONFIG_REGS_H_
#define PCI_CONFIG_REGS_H_

/* type 0 and 1 header layout; type 0 has 6 BARs, type 1 has 2 BARs */
#define PCI_CONFIG_VENDOR_DEVICE	0x00	/* vendor ID and device ID */
#define PCI_CONFIG_COMMAND_STATUS	0x04	/* command and status registers */
#define PCI_CONFIG_CLASS_REVID		0x08	/* class, interface and revision ID */
#define PCI_CONFIG_HEADER_MISC		0x0c	/* header misc registers */
#define PCI_CONFIG_BAR(bar)			(0x10 + ((bar) * 4))	/* 32-bit BAR */
#define PCI_CONFIG_TYPE0_SUBSYSTEM	0x2c	/* type 0 subsystem ID */
#define PCI_CONFIG_TYPE0_ROM		0x30	/* type 0 expansion ROM base address */
#define PCI_CONFIG_CAP_PTR			0x34	/* capabilities pointer (lower 8 bits) */
#define PCI_CONFIG_TYPE1_ROM		0x38	/* type 1 expansion ROM base address */
#define PCI_CONFIG_INT_MISC			0x3c

/* bits in command part */
#define PCI_COMMAND_INT_DIS			0x00000400	/* interrupt disable */
#define PCI_COMMAND_SERR			0x00000100	/* SERR enable */
#define PCI_COMMAND_PARITY			0x00000040	/* parity enable */
#define PCI_COMMAND_BUSM			0x00000004	/* busmaster enable */
#define PCI_COMMAND_MEM				0x00000002	/* memory space enable */
#define PCI_COMMAND_IO				0x00000001	/* I/O space enable */

/* bits in status part */
#define PCI_STATUS_PARITY			0x80000000	/* detected parity error */
#define PCI_STATUS_SERR				0x40000000	/* signaled SERR  */
#define PCI_STATUS_RMABT			0x20000000	/* received master abort */
#define PCI_STATUS_RTABT			0x10000000	/* received target abort */
#define PCI_STATUS_STABT			0x08000000	/* signaled target abort */
#define PCI_STATUS_MDPE				0x01000000	/* master data parity error */
#define PCI_STATUS_CAP				0x00100000	/* capability list */
#define PCI_STATUS_INT				0x00080000	/* interrupt status */

/* bits in header type part */
#define PCI_HEADER_TYPE(x)			(((x) >> 16) & 0x7fu)
#define PCI_HEADER_TYPE_DEVICE		0x00	/* normal device */
#define PCI_HEADER_TYPE_BRIDGE		0x01	/* PCI-PCI bridge */
#define PCI_HEADER_TYPE_CARDBUS		0x02	/* PCI-Cardbus bridge */
#define PCI_HEADER_TYPE_MULTI		0x00800000	/* multi-function device */

/* bits in BAR registers */
#define PCI_BAR_IO					0x01	/* I/O space BAR */
#define PCI_BAR_BELOW1MB			0x02	/* below 1 MB (legacy) */
#define PCI_BAR_64BIT				0x04	/* 64-bit */
#define PCI_BAR_PREF				0x08	/* prefetchable memory */
#define PCI_BAR_MASK_IO				0xfffffffc
#define PCI_BAR_MASK_MEM			0xfffffff0

/* interrupt line 0..15 */
#define PCI_INT_LINE(x)				((x) & 0xffu)

/* interrupt pin */
#define PCI_INT_PIN(x)				(((x) >> 8) & 0xffu)
#define PCI_INT_PIN_NONE			0x00
#define PCI_INT_PIN_INTA			0x01
#define PCI_INT_PIN_INTB			0x02
#define PCI_INT_PIN_INTC			0x03
#define PCI_INT_PIN_INTD			0x04

#endif
