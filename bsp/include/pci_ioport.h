/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * pci_ioport.h
 *
 * PCI I/O space (I/O ports) access
 *
 * azuepke, 2025-01-22: initial
 */

#ifndef PCI_IOPORT_H_
#define PCI_IOPORT_H_

#include <pci_stub.h>
#include <arch_io.h>
#include <stddef.h>

/* read and write I/O space (I/O ports)
 *
 * The offset of the I/O space is typically limited to 64KB.
 *
 * NOTES:
 * - reads from a non-supported device or address return 0xffffffff
 * - writes to a non-supported device or address are ignored
 * - offsets need to be naturally aligned
 * - PCI controllers might offer different I/O space regions,
 *   so we need to provide the pcidev on each call
 */

static inline uint8_t pci_ioport_read8(pcidev_t pcidev, uint32_t offset)
{
	volatile void *ptr = pci_ioport_addr(pcidev, offset);
	if (ptr != NULL) {
		return read8(ptr);
	} else {
		return 0xffu;
	}
}

static inline uint16_t pci_ioport_read16(pcidev_t pcidev, uint32_t offset)
{
	volatile void *ptr = pci_ioport_addr(pcidev, offset & ~1);
	if (ptr != NULL) {
		return read16(ptr);
	} else {
		return 0xffffu;
	}
}

static inline uint32_t pci_ioport_read32(pcidev_t pcidev, uint32_t offset)
{
	volatile void *ptr = pci_ioport_addr(pcidev, offset & ~3);
	if (ptr != NULL) {
		return read32(ptr);
	} else {
		return 0xffffffffu;
	}
}

static inline void pci_ioport_write8(pcidev_t pcidev, uint32_t offset, uint8_t value)
{
	volatile void *ptr = pci_ioport_addr(pcidev, offset);
	if (ptr != NULL) {
		return write8(ptr, value);
	}
}

static inline void pci_ioport_write16(pcidev_t pcidev, uint32_t offset, uint16_t value)
{
	volatile void *ptr = pci_ioport_addr(pcidev, offset & ~1);
	if (ptr != NULL) {
		return write16(ptr, value);
	}
}

static inline void pci_ioport_write32(pcidev_t pcidev, uint32_t offset, uint32_t value)
{
	volatile void *ptr = pci_ioport_addr(pcidev, offset & ~3);
	if (ptr != NULL) {
		return write32(ptr, value);
	}
}

#endif
