# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="NXP LX2160a with ARM Cortex A72"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a72
MARRON_SMP=yes
