/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021, 2023 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for QEMU RISC-V "virt" board.
 *
 * azuepke, 2015-07-13: initial
 * azuepke, 2021-01-16: adapted to mini-OS
 * azuepke, 2023-09-03: adapted for RISC-V from qemu-aarch64
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[2];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

/* bsp.c */
void bsp_ipi_handler(void);

#endif

#define BOARD_NAME			"qemu-riscv64"

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 4M on 32-bit, offset 2M on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/* hart ID remapping (actually not used) */
#define RISCV_HART_REMAP_K2P(cpu_id)		(cpu_id)
#define RISCV_HART_REMAP_K2P_MASK(cpu_mask)	(cpu_mask)

/* timer frequency */
#define SBI_TIMER_FREQ		10000000

/*
 * The 8250 UART on QEMU RISC-V "virt" board is located
 * at physical address 0x10000000.
 * See also qemu/hw/riscv/virt.c in QEMU sources.
 */
#define NS16550_UART_REGS		CFG_IO_ADDR_uart0
#define NS16550_UART_REGSIZE	1
#define NS16550_UART_CLOCK		3686400
#define NS16550_UART_KEEPBAUD	1	/* keep baudrate of bootloader */

/* OpenSBI startup from any possible hart */
#define OPENSBI_SMP_STARTUP 1

#if 0
/* enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1
#endif

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
