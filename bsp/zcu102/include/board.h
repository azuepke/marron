/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for Xilinx Zynq UltraScale+ with Cortex-A53.
 *
 * azuepke, 2021-01-22: adapted from QEMU
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* zynq_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS	192
#define GIC_CPU_BIT(x)	(1u << (x))

/*
 * The Ultrascale has two UARTs:
 * UART1: 0xff000000
 * UART2: 0xff010000
 */
#define ZYNQ_UART_REGS		CFG_IO_ADDR_uart1
#define ZYNQ_UART_CLOCK		100000000

#endif
