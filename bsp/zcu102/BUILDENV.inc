# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="Xilinx Zynq UltraScale+ with ARM Cortex A53"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a53
MARRON_SMP=yes
