/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for QEMU Versatile Express Cortex A15.
 *
 * azuepke, 2013-11-19: initial
 * azuepke, 2017-07-17: adapted to mini-OS
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* pl011_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS	256
#define GIC_CPU_BIT(x)	(1u << (x))

#define SP804_TIMER_BASE		CFG_IO_ADDR_sp804_timer
#define SP804_TIMER_CLOCK		1000000	/* 1 MHz */
#define SP804_TIMER_IRQ			34

/*
 * PL011 UARTs on Versatile Express Cortex A15 board:
 *
 * UART0: 0x1c090000
 * UART1: 0x1c0a0000
 * UART2: 0x1c0b0000
 * UART3: 0x1c0c0000
 */
#define PL011_SERIAL_REGS	CFG_IO_ADDR_uart0
#define PL011_SERIAL_CLOCK	50000000

/** enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
