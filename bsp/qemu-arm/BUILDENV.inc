# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="QEMU Versatile Express with ARM Cortex A15"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a15
MARRON_SMP=yes
