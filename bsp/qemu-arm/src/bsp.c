/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021, 2023 Alexander Zuepke */
/*
 * board.c
 *
 * Board initialization for QEMU Versatile Express Cortex A15.
 *
 * azuepke, 2013-11-19: initial cloned
 * azuepke, 2023-08-12: reset and power-off
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <gic.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <preboot.h>
#include <arch_io.h>
#include <barrier.h>


/** BSP name string */
const char bsp_name[] = "qemu-arm";

#ifdef SMP
// CPUs online
unsigned long bsp_cpu_online_mask = 0;
// CPUs started scheduling
unsigned int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned int cpu;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	/* we flush the caches before doing anything, as the first instructions
	 * of a newly started processor runs in uncached memory mode.
	 */
	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
		barrier_set_val_clean_cache(&__boot_release, cpu);

		/* wait until CPU is up */
		barrier_wait_bit(&bsp_cpu_online_mask, cpu);
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu_id)
{
	assert(cpu_id == bsp_cpu_id());
	barrier_set_bit(&bsp_cpu_online_mask, cpu_id);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu_id == 0) {
		barrier_set_val(&bsp_cpu_go, 1);
	} else {
		/* wait until we see the "go" signal */
		barrier_wait_val(&bsp_cpu_go, 1);
	}

	/* start timer here on SMP */
	cpu_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(38400);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		gic_init_dist();
		gic_init_cpu(cpu_id);
		cpu_timer_init(100);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		gic_init_cpu(cpu_id);
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb; wfi" : : : "memory");
}

/* reset the board */
static inline void ve_reset(void)
{
	/* SYS_CFGCTRL register @ 0x1c0100a4 */
	volatile uint32_t *sys_cfgctrl = io_ptr(CFG_IO_ADDR_ve_sysregs, 0xa4);

	write32(sys_cfgctrl, 0xc0900000);
}

/* poweroff the board */
static inline void ve_poweroff(void)
{
	/* SYS_CFGCTRL register @ 0x1c0100a4 */
	volatile uint32_t *sys_cfgctrl = io_ptr(CFG_IO_ADDR_ve_sysregs, 0xa4);

	write32(sys_cfgctrl, 0xc0800000);
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode __unused)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		gic_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		ve_reset();
	}
	if (mode == BOARD_POWEROFF) {
		ve_poweroff();
	}

	/* poweroff QEMU instance in any other case as well */
	if (mode != BOARD_STOP) {
		ve_poweroff();
	}

	__bsp_halt();
	unreachable();
}
