/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for NVIDIA Xavier
 *
 * azuepke, 2021-04-18: cloned from TX2
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

/* bsp.c */
unsigned long gic_cpu_sgir_id(unsigned int cpu_id);
unsigned long gic_cpu_router_id(unsigned int cpu_id);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_ICC_MODE		1	/* use ICC_x_EL1 registers to access GICC */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_REDIST_BASE		CFG_IO_ADDR_gic_redist
#define GIC_REDIST_SIZE		0x20000		/* GICv3: 128K, GICv4: 256K */
#define GIC_NUM_IRQS		(32 + 960)
#define GIC_NUM_CORES		12
/* GICv3: translate logical CPU ID to REDIST offset, SGIR ID, or IROUTER ID */
#define GIC_CPU_REDIST_ID(x)	(x)
#define GIC_CPU_SGIR_ID(x)		gic_cpu_sgir_id(x)
#define GIC_CPU_ROUTER_ID(x)	gic_cpu_router_id(x)

/* we have specific settings for the CPU */
/* On Xavier, we have three or four ARM clusters with four A78 cores each. */
#define ARM_MPIDR_AFF0_BITS 0
#define ARM_MPIDR_AFF1_BITS 2
#define ARM_MPIDR_AFF2_BITS 2
#define ARM_MPIDR_AFF3_BITS 0

/*
 * UARTs on Orin: see hardware.xml
 *
 */
#define PL011_SERIAL_REGS	CFG_IO_ADDR_uarti
#define PL011_SERIAL_CLOCK	204000000	/* 408 MHz clock / 2 */

#if 0
#define CPU_COUNTER_FREQ	31250000	/* 31.25 MHz */
#endif

#endif
