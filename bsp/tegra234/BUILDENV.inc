# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="NVIDIA Tegra Orin with 12x Cortex-A78AE (64-bit)"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a78
MARRON_SMP=yes
