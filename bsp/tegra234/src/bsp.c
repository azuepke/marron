/* SPDX-License-Identifier: MIT */
/* Copyright 2015, 2021 Alexander Zuepke */
/*
 * board.c
 *
 * Board initialization for NVIDIA Xavier
 *
 * azuepke, 2021-04-18: cloned from TX2
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <gic.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <panic.h>
#include <psci.h>
#include <preboot.h>
#include <barrier.h>


/** BSP name string */
const char bsp_name[] = "tegra234";

/* inverse translation from logical CPU IDs to GIC SGIR format */
unsigned long gic_cpu_sgir_id(unsigned int cpu_id)
{
	unsigned long aff0 = 0;
	unsigned long aff1 = 0;
	unsigned long aff2 = 0;
	unsigned long aff3 = 0;

	/* generic linear affinity encoding */
#if defined(ARM_MPIDR_AFF0_BITS) && (ARM_MPIDR_AFF0_BITS > 0)
	aff0 = cpu_id;
	aff0 &= ((1 << ARM_MPIDR_AFF0_BITS) - 1);
#endif
#if defined(ARM_MPIDR_AFF1_BITS) && (ARM_MPIDR_AFF1_BITS > 0)
	aff1 = cpu_id >> ARM_MPIDR_AFF0_BITS;
	aff1 &= ((1 << ARM_MPIDR_AFF1_BITS) - 1);
#endif
#if defined(ARM_MPIDR_AFF2_BITS) && (ARM_MPIDR_AFF2_BITS > 0)
	aff2 = cpu_id >> (ARM_MPIDR_AFF0_BITS + ARM_MPIDR_AFF1_BITS);
	aff2 &= ((1 << ARM_MPIDR_AFF2_BITS) - 1);
#endif
#if defined(ARM_MPIDR_AFF3_BITS) && (ARM_MPIDR_AFF3_BITS > 0)
	aff3 = cpu_id >> (ARM_MPIDR_AFF0_BITS + ARM_MPIDR_AFF1_BITS + ARM_MPIDR_AFF2_BITS);
	aff3 &= ((1 << ARM_MPIDR_AFF3_BITS) - 1);
#endif

	/* encoding of the SGIR registers:
	 * - aff3 << 48
	 * - aff2 << 32
	 * - aff1 << 16
	 * - aff0 as targetlist in lower 16 bit
	 * - (intid << 24) <- not set here
	 */
	return (aff3 << 48) | (aff2 << 32) | (aff1 << 16) | (1ul << aff0);
}

/* inverse translation from logical CPU IDs to GIC IROUTER format */
unsigned long gic_cpu_router_id(unsigned int cpu_id)
{
	unsigned long aff0 = 0;
	unsigned long aff1 = 0;
	unsigned long aff2 = 0;
	unsigned long aff3 = 0;

	/* generic linear affinity encoding */
#if defined(ARM_MPIDR_AFF0_BITS) && (ARM_MPIDR_AFF0_BITS > 0)
	aff0 = cpu_id;
	aff0 &= ((1 << ARM_MPIDR_AFF0_BITS) - 1);
#endif
#if defined(ARM_MPIDR_AFF1_BITS) && (ARM_MPIDR_AFF1_BITS > 0)
	aff1 = cpu_id >> ARM_MPIDR_AFF0_BITS;
	aff1 &= ((1 << ARM_MPIDR_AFF1_BITS) - 1);
#endif
#if defined(ARM_MPIDR_AFF2_BITS) && (ARM_MPIDR_AFF2_BITS > 0)
	aff2 = cpu_id >> (ARM_MPIDR_AFF0_BITS + ARM_MPIDR_AFF1_BITS);
	aff2 &= ((1 << ARM_MPIDR_AFF2_BITS) - 1);
#endif
#if defined(ARM_MPIDR_AFF3_BITS) && (ARM_MPIDR_AFF3_BITS > 0)
	aff3 = cpu_id >> (ARM_MPIDR_AFF0_BITS + ARM_MPIDR_AFF1_BITS + ARM_MPIDR_AFF2_BITS);
	aff3 &= ((1 << ARM_MPIDR_AFF3_BITS) - 1);
#endif

	/* encoding of the IROUTER registers:
	 * - aff3 << 32
	 * - aff2 << 16
	 * - aff1 << 8
	 * - aff0 << 0
	 */
	return (aff3 << 32) | (aff2 << 16) | (aff1 << 8) | (aff0 << 0);
}

#ifdef SMP
// CPUs online
unsigned long bsp_cpu_online_mask = 0;
// CPUs started scheduling
unsigned int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned long mpidr;
	unsigned long entry;
	unsigned int cpu;
	int psci_err;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	/* we flush the caches before doing anything, as the first instructions
	 * of a newly started processor runs in uncached memory mode.
	 */
	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
#ifdef QEMU_SMP_STARTUP
		barrier_set_val_clean_cache(&__boot_release, cpu);
#endif

		/* multiple cluster MPIDR: same format as GIC IROUTER */
		mpidr = GIC_CPU_ROUTER_ID(cpu);
		entry = BOARD_KERNEL_TO_PHYS((unsigned long)_start);
		psci_err = psci_cpu_on(mpidr, entry, mpidr);
		if (psci_err != PSCI_ERR_SUCCESS) {
			panic("PSCI: problem starting CPU %u, error:%d\n", cpu, psci_err);
		}

		/* wait until CPU is up */
		barrier_wait_bit(&bsp_cpu_online_mask, cpu);
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu_id)
{
	assert(cpu_id == bsp_cpu_id());
	barrier_set_bit(&bsp_cpu_online_mask, cpu_id);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu_id == 0) {
		barrier_set_val(&bsp_cpu_go, 1);
	} else {
		/* wait until we see the "go" signal */
		barrier_wait_val(&bsp_cpu_go, 1);
	}

	/* start timer here on SMP */
	cpu_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
	unsigned int version;

#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		version = psci_version();
		printk("PSCI version: %u.%u\n", version >> 16, version & 0xffff);

		gic_init_dist();
		gic_init_cpu(cpu_id);
		cpu_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		gic_init_cpu(cpu_id);
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb sy; wfi" : : : "memory");
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		gic_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		psci_system_reset();
	}
	if (mode == BOARD_POWEROFF) {
		psci_system_off();
	}

	__bsp_halt();
	unreachable();
}
