/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021 Alexander Zuepke */
/*
 * board.c
 *
 * Board initialization for Raspberry Pi 4 Model B (BCM2711)
 *
 * azuepke, 2020-11-04: cloned from RPi2 BSP
 */

#include <kernel.h>
#include <bsp.h>
#include <board.h>
#include <gic.h>
#include <rpi_irq.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <arch_io.h>
#include <preboot.h>


/** BSP name string */
const char bsp_name[] = "bcm2711";

// initialize BSP
__init void bsp_init(unsigned int cpu_id)
{
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printk("Starting up ...\n");

		if (bsp_preboot_ok == 0) {
			printk("kernel out of pagetables\n");
			__bsp_halt();
		}

		gic_init_dist();
		bcm_irq_init();
		gic_init_cpu(cpu_id);
		cpu_timer_init(1000);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		gic_init_cpu(cpu_id);
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
#ifdef __aarch64__
	__asm__ volatile ("dsb sy; wfi" : : : "memory");
#else /* 32-bit ARM */
	__asm__ volatile ("dsb; wfi" : : : "memory");
#endif
}


/* watchdog registers */
#define WDOG_RSTC	0x1c
#define WDOG_RSTS	0x20
#define WDOG_WDOG	0x24

/* register accessors */
static inline uint32_t wdog_rd(unsigned long reg)
{
	return read32(io_ptr(WDOG_REGS, reg));
}

static inline void wdog_wr(unsigned long reg, uint32_t val)
{
	write32(io_ptr(WDOG_REGS, reg), val);
}

static inline void bsp_reset(void)
{
	uint32_t rstc;

	rstc = wdog_rd(WDOG_RSTC);
	rstc &= ~0x30;
	rstc |= 0x20;

	wdog_wr(WDOG_WDOG, 0x5a00000d);
	wdog_wr(WDOG_RSTC, 0x5a000000 | rstc);
}

// halt the board (shutdown of the system)
__cold void bsp_halt(halt_mode_t mode)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		gic_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		bsp_reset();
	}

	__bsp_halt();
	unreachable();
}
