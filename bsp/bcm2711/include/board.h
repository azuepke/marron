/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2020-2021 Alexander Zuepke */
/*
 * board.h
 *
 * Board specific setting for Raspberry Pi 2 Model B V1.1 with Cortex A7.
 *
 * azuepke, 2020-08-05: Raspberry Pi port
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <config.h>

#ifndef __ASSEMBLER__

/* start.S */
void _start(void);
void __bsp_halt(void);
extern unsigned long __boot_args[4];

/* ns16550_uart.c */
void serial_init(unsigned int baud);

#endif

#define BOARD_RAM_PHYS		CFG_KERNEL_MEM_BASE
#define BOARD_RAM_VIRT		CFG_KERNEL_VADDR

/* physical kernel load address; offset 32K on 32-bit, offset 512K on 64-bit */
#define LOAD_ADDR			(BOARD_RAM_PHYS + CFG_KERNEL_LOAD_OFFSET)

/* linker setting */
#define LD_TEXT_BASE		(BOARD_RAM_VIRT + CFG_KERNEL_LOAD_OFFSET)

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

#define BOARD_CPU_NUM		CFG_CPU_NUM

/** GIC specific addresses */
#define GIC_DIST_BASE		CFG_IO_ADDR_gic_dist
#define GIC_PERCPU_BASE		CFG_IO_ADDR_gic_percpu
#define GIC_NUM_IRQS	224
#define GIC_CPU_BIT(x)	(1u << (x))

/*
 * UARTs on Raspberry Pi 4:
 *
 * UART1: 0xfe215040  8250 Mini-UART
 */
#define NS16550_UART_REGS		(CFG_IO_ADDR_uart1 + 0x40)
#define NS16550_UART_REGSIZE	4
#define NS16550_UART_CLOCK		250000000
#define NS16550_UART_KEEPBAUD	1	/* keep baudrate of bootloader */

/** BCM specific addresses and frequencies */
#define CORE_FREQ	900000000	/* 900 MHz */
#define BCM_FREQ	396000000	/* 250 MHz */

#define CPU_COUNTER_FREQ	54000000	/* 54 MHz */

#define GPU_IRQ_REGS	(CFG_IO_ADDR_rpi_irq + 0x200)	/* GPU interrupt controller */
#define ARM_IRQ_REGS	CFG_IO_ADDR_arm_ctrl	/* ARM interrupt router/controller */
#define WDOG_REGS		CFG_IO_ADDR_wdog	/* Watchdog */

/** enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
