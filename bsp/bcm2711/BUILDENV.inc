# SPDX-License-Identifier: MIT
# Copyright 2013, 2021 Alexander Zuepke

MARRON_BSP_NAME="Raspberry Pi 4 Model B with ARM Cortex A72 (32-bit)"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a15
MARRON_SMP=yes
