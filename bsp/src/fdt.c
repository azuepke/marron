/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * fdt.c
 *
 * Firmware device tree (FDT) handling.
 *
 * A simple parser library for FDT.
 * The implementation follows the Devicetree Specification Release v0.3.
 *
 * azuepke, 2021-04-11: initial
 */

#include "fdt.h"
#include <assert.h>
#include <stddef.h>
#include <string.h>

/* FDT version 17 header
 *
 * FDT_HEADER:
 *   fdt_uint32_t magic;
 *   fdt_uint32_t totalsize;
 *   fdt_uint32_t off_dt_struct;
 *   fdt_uint32_t off_dt_strings;
 *   fdt_uint32_t off_mem_rsvmap;
 *   fdt_uint32_t version;
 *   fdt_uint32_t last_comp_version;
 *   fdt_uint32_t boot_cpuid_phys;
 *   fdt_uint32_t size_dt_strings;
 *   fdt_uint32_t size_dt_struct;
 *
 * FDT data types are always big-endian encoded.
 */

/* FDT header magic */
#define FDT_MAGIC				0xd00dfeed

#define FDT_NESTING_LEVEL		100

/* FDT reserved memory entry:
 *   fdt_uint64_t address;
 *   fdt_uint64_t size;
 */

/* token types */
#define FDT_TOKEN_BEGIN_NODE	1
#define FDT_TOKEN_END_NODE		2
#define FDT_TOKEN_PROP			3
#define FDT_TOKEN_NOP			4
#define FDT_TOKEN_END			9

/* The FDT data is encoded as a stream of integers defining tokens.
 * Two token have additional payload data:
 *
 * FDT_TOKEN_BEGIN_NODE:
 *   fdt_uint32_t token;
 *   char name[];           // NUL-terminated string, can be empty string
 *
 * FDT_TOKEN_PROP:
 *   fdt_uint32_t token;
 *   fdt_uint32_t len;      // payload length, can be zero sized
 *   fdt_uint32_t nameoff;  // offset in string table
 *   char data[len];        // optional if zero sized (special NULL property)
 *
 * All other tokens don't have payload data.
 * The data part is NUL-byte padded to keep the overall integer alignment.
 */
#define FDT_ALIGN(addr)			(((addr) + 3UL) & ~3UL)

/* pointer to FDT string table (copied from FDT header) */
static const char *fdt_strings;

/** node name */
const char *fdt_node_name(fdt_handle_t *p)
{
	assert(p != NULL);
	assert(fdt32(p) == FDT_TOKEN_BEGIN_NODE);

	return (const char *)(p + 4);
}

/** length of property data */
uint32_t fdt_prop_len(fdt_handle_t *p)
{
	assert(p != NULL);
	assert(fdt32(p) == FDT_TOKEN_PROP);
	return fdt32(p + 4);
}

/** property name */
const char *fdt_prop_name(fdt_handle_t *p)
{
	assert(p != NULL);
	assert(fdt32(p) == FDT_TOKEN_PROP);
	return fdt_strings + fdt32(p + 8);
}

/** property data */
const void *fdt_prop_data(fdt_handle_t *p)
{
	assert(p != NULL);
	assert(fdt32(p) == FDT_TOKEN_PROP);
	return p + 12;
}

/** get next FDT token */
static inline fdt_handle_t *fdt_next(uint32_t tok, fdt_handle_t *p)
{
	size_t len;

	assert(p != NULL);
	assert(tok == fdt32(p));

	switch (tok) {
	case FDT_TOKEN_BEGIN_NODE:
		len = strlen((const char *)p + 4);
		return p + 4 + FDT_ALIGN(len + 1);

	case FDT_TOKEN_END_NODE:
		return p + 4;

	case FDT_TOKEN_PROP:
		len = fdt32(p + 4);
		return p + 12 + FDT_ALIGN(len);

	case FDT_TOKEN_NOP:
		return p + 4;

	case FDT_TOKEN_END:
		return NULL;

	default:
		/* invalid token type -- parsing error */
		return NULL;
	}
}

/** check and validate FDT header
  *
  * returns a handle to the FDT root node if a valid FDT header is found;
  * NULL otherwise
  * on success, fdt_node_iter() and fdt_prop_*() services are available
  */
fdt_handle_t *fdt_header_parse(const void *fdt)
{
	fdt_handle_t *p = fdt;
	uint32_t val;

	assert(p != NULL);

	/* magic */
	val = fdt32(p + 0);
	if (val != FDT_MAGIC) {
		return NULL;
	}

	/* off_dt_strings */
	val = fdt32(p + 12);
	fdt_strings = (const char *)p + val;

	/* off_dt_struct */
	val = fdt32(p + 8);
	return p + val;
}

/** iterate child nodes in the given context
 *
 * iterate child nodes in the given context;
 * on the first invocation, initialize "p" to NULL;
 * on further invocation, pass the previous return value as "p".
 * returns a pointer to a child node or NULL at the end
 *
 *   fdt_handle_t *p = NULL;
 *   while ((p = fdt_node_iter(ctxt, p)) != NULL) {
 *     ...
 *   }
 */
fdt_handle_t *fdt_node_iter(fdt_handle_t *ctxt, fdt_handle_t *p)
{
	uint32_t tok;
	int level;

	assert(ctxt != NULL);
	assert(fdt32(ctxt) == FDT_TOKEN_BEGIN_NODE);

	if (p == NULL) {
		/* start */
		p = ctxt;
		level = 0;
	} else {
		/* p points to previous child */
		level = 1;
	}

	tok = fdt32(p);
	while ((p = fdt_next(tok, p)) != NULL) {
		tok = fdt32(p);

		if (tok == FDT_TOKEN_BEGIN_NODE) {
			if (level == 0) {
				return p;
			}

			assert(level < FDT_NESTING_LEVEL);
			level++;
		}
		if (tok == FDT_TOKEN_END_NODE) {
			if (level == 0) {
				return NULL;
			}

			assert(level > 0);
			level--;
		}
	}

	/* should not be reached (FDT not well-formed) */
	assert(0);
	return NULL;
}

/** find child node by name in the given context */
fdt_handle_t *fdt_node_find(fdt_handle_t *ctxt, const char *node_name)
{
	fdt_handle_t *p = NULL;
	while ((p = fdt_node_iter(ctxt, p)) != NULL) {
		if (strcmp(fdt_node_name(p), node_name) == 0) {
			return p;
		}
	}
	return NULL;
}

/** iterate properties in the given context
 *
 * iterate properties in the given context;
 * on the first invocation, initialize "p" to NULL;
 * on further invocation, pass the previous return value as "p".
 * returns a pointer to a property or NULL at the end
 *
 *   fdt_handle_t *p = NULL;
 *   while ((p = fdt_prop_iter(ctxt, p)) != NULL) {
 *     ...
 *   }
 */
fdt_handle_t *fdt_prop_iter(fdt_handle_t *ctxt, fdt_handle_t *p)
{
	uint32_t tok;
	int level;

	assert(ctxt != NULL);
	assert(fdt32(ctxt) == FDT_TOKEN_BEGIN_NODE);

	if (p == NULL) {
		/* start */
		p = ctxt;
	}
	level = 0;

	tok = fdt32(p);
	while ((p = fdt_next(tok, p)) != NULL) {
		tok = fdt32(p);

		if (tok == FDT_TOKEN_BEGIN_NODE) {
			assert(level < FDT_NESTING_LEVEL);
			level++;
		}
		if (tok == FDT_TOKEN_END_NODE) {
			if (level == 0) {
				return NULL;
			}

			assert(level > 0);
			level--;
		}

		if (tok == FDT_TOKEN_PROP) {
			if (level == 0) {
				return p;
			}
		}
	}

	/* should not be reached (FDT not well-formed) */
	assert(0);
	return NULL;
}

/** find child node by name in the given context */
fdt_handle_t *fdt_prop_find(fdt_handle_t *ctxt, const char *prop_name)
{
	fdt_handle_t *p = NULL;
	while ((p = fdt_prop_iter(ctxt, p)) != NULL) {
		if (strcmp(fdt_prop_name(p), prop_name) == 0) {
			return p;
		}
	}
	return NULL;
}
