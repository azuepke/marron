/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * ns16550_uart.c
 *
 * 16550-like UART driver
 *
 * azuepke, 2013-03-24: initial
 * azuepke, 2020-11-05: BSP for Raspberry Pi 4
 * azuepke, 2023-09-11: renamed from 8250_uart.c
 */

#include <bsp.h>
#include <arch_io.h>
#include <board.h>

/* register accessors */
static inline uint32_t rd(unsigned long reg)
{
#if NS16550_UART_REGSIZE == 4
	return read32(io_ptr(NS16550_UART_REGS, reg * NS16550_UART_REGSIZE));
#else /* NS16550_UART_REGSIZE == 1 */
	return read8(io_ptr(NS16550_UART_REGS, reg * NS16550_UART_REGSIZE));
#endif
}

static inline void wr(unsigned long reg, uint32_t val)
{
#if NS16550_UART_REGSIZE == 4
	write32(io_ptr(NS16550_UART_REGS, reg * NS16550_UART_REGSIZE), val);
#else /* NS16550_UART_REGSIZE == 1 */
	write8(io_ptr(NS16550_UART_REGS, reg * NS16550_UART_REGSIZE), val);
#endif
}

err_t bsp_putc(int c)
{
	/* poll until transmitter is empty */
	while ((rd(5) & 0x20) == 0) {
		return EBUSY;
	}

	wr(0, c & 0xffu);
	return EOK;
}

err_t bsp_getc(int *c)
{
	/* poll until receiver is not empty */
	if ((rd(5) & 0x01) == 0) {
		return EBUSY;
	}

	*c = rd(0) & 0xffu;
	return EOK;
}

__init void serial_init(unsigned int baudrate)
{
#ifndef NS16550_UART_KEEPBAUD
	unsigned int div;

	/* standard sequence for NS16550 UART */

	/* disable all interrupts */
	wr(1, 0);

	/* enable DLAB, set baudrate */
	div = (NS16550_UART_CLOCK + (baudrate * 16) / 2) / (baudrate * 16);
	wr(3, 0x80);
	wr(0, div & 0xff);
	wr(1, div >> 8);

	/* 8n1 mode */
	wr(3, 0x03);
	/* enable FIFO, 14 byte threshold */
	wr(2, 0xc7);

	/* clear pending status bits */
	rd(5);
#endif

	(void)baudrate;
}
