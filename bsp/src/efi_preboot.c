/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * efi_preboot.c
 *
 * Unified Extensible Firmware Interface (UEFI) subset for kernel booting
 *
 * Provide a minimal set of UEFI APIs to start a kernel:
 * get the memory map and then call exit_boot_services()
 * to take control of the platform.
 *
 * Based on UEFI Spec Version 2.7 Errata A August 2017
 *
 * NOTE: This code runs in physical addressing mode! Do not call from C code!
 *
 * NOTE: L"wide-string" requires gcc -fshort-wchar to emit 16-bit wide chars
 *
 * azuepke, 2022-06-03: initial
 */

#include <efi.h>
#include <marron/compiler.h>

/* private data storage of kernel */
efi_private_t _efi_private __section(.bss..uninit) = { 0 };

/* EFI entry point in C (priv and continue are our extension) */
extern efi_status_t efi_entry_preboot(
	efi_handle_t image_handle,
	efi_system_table_t *system_table,
	efi_private_t *priv,
	efi_status_t (*cont)(void))
{
	uint32_t desc_version;
	efi_status_t err;
	uintn_t map_key;

	/* save all relevant parameters in private structure */
	priv->image_handle = image_handle;
	priv->system_table = system_table;
	priv->rts = system_table->rts;
	priv->bts = system_table->bts;
	priv->number_of_table_entries = system_table->number_of_table_entries;
	priv->configuration_table = system_table->configuration_table;


	/* get memory map -- we need the map_key for exit_boot_services() */
	priv->map_size = sizeof(priv->map);
	err = system_table->bts->get_memory_map(
	        &priv->map_size,
	        priv->map,
	        &map_key,
	        &priv->desc_size,
	        &desc_version);

	if (err != EFI_SUCCESS) {
		/* returns EFI_INVALID_PARAMETER (impossibe) or EFI_BUFFER_TOO_SMALL */
		return err;
	}

	/* take control of the platform */
	err = system_table->bts->exit_boot_services(priv->image_handle, map_key);
	if (err != EFI_SUCCESS) {
		/* returns EFI_INVALID_PARAMETER (impossible) */
		return err;
	}

	/* tail call (does not return) */
	return cont();
}
