/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * file.c
 *
 * Access to files in ROM image for BSPs.
 *
 * azuepke, 2022-05-04: initial
 */

#include "file.h"
#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <memrq_types.h>


/** find file by name in the ROM image */
const void *file_find(const char *name)
{
	unsigned int i;

	for (i = 0; i < file_map_num; i++) {
		if (strcmp(name, file_map_cfg[i].name) == 0) {
			return &file_map_cfg[i];
		}
	}
	return NULL;
}

/** get size of file (handle must be non-NULL) */
size_t file_size(const void *handle)
{
	const struct memrq_cfg *f = handle;

	assert(f != NULL);
	return f->size;
}

/** get content of file (read-only data, handle must be non-NULL) */
const void *file_data(const void *handle)
{
	const struct memrq_cfg *f = handle;

	assert(f != NULL);
	return (const void *)f->addr;
}
