/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * pci_stub.c
 *
 * PCI config space access and PCI hierarchy
 *
 * This stub is pretty generic and requires a "pci_ecam" mapping in the kernel
 * to access the PCI config space. An optional "pci_ioport" mapping allows
 * to retrieve the base address of specific I/O ports.
 *
 * azuepke, 2025-01-22: initial
 */

#include <kernel.h>
#include <board.h>
#include <pci_stub.h>
#include <arch_io.h>
#include <stddef.h>


/* internal function to get address in PCI config space if mapped in the kernel */
static inline volatile void *pci_config_addr(pcidev_t pcidev, size_t offset)
{
#ifdef CFG_IO_SIZE_pci_ecam
	/* The PCI ECAM layout exposes 4K per device ordered by bus/dev/fn as usual.
	 * We simply shift the pcidev ID accordingly to get the right device.
	 */
	size_t addr = pcidev << 12 | offset;
	if (addr < CFG_IO_SIZE_pci_ecam) {
		return (volatile void *)(CFG_IO_ADDR_pci_ecam + addr);
	}
#endif

	(void)pcidev;
	(void)offset;
	return NULL;
}

/* read PCI config space */
uint32_t pci_config_read32(pcidev_t pcidev, uint32_t offset)
{
	volatile void *ptr = pci_config_addr(pcidev, offset & 0xffcu);
	if (ptr != NULL) {
		return read32(ptr);
	} else {
		return 0xffffffffu;
	}
}

/* write PCI config space */
void pci_config_write32(pcidev_t pcidev, uint32_t offset, uint32_t value)
{
	volatile void *ptr = pci_config_addr(pcidev, offset & 0xffcu);
	if (ptr != NULL) {
		return write32(ptr, value);
	}
}

/* get base address of PCI I/O space (I/O ports) if mapped in the kernel */
volatile void *pci_ioport_addr(pcidev_t pcidev, size_t offset)
{
#ifdef CFG_IO_SIZE_pci_ioport
	if ((PCIDEV_DOMAIN_BUS(pcidev) == 0) && (offset < CFG_IO_SIZE_pci_ioport)) {
		return (volatile void *)(CFG_IO_ADDR_pci_ioport + offset);
	}
#endif

	(void)pcidev;
	(void)offset;
	return NULL;
}

/* PCI topology: retrieve number of PCI domains (typically 1) */
uint32_t pci_topology_domain_num(void)
{
	return 1;
}

/* PCI topology: retrieve number of PCI buses in given domain (up to 256) */
uint32_t pci_topology_domain_bus_num(uint32_t domain)
{
	switch (domain) {
	case 0:
#ifdef CFG_IO_SIZE_pci_ecam
		return CFG_IO_SIZE_pci_ecam / 4096 / 8 / 32;
#endif
	default:
		return 0;
	}
}
