# SPDX-License-Identifier: MIT
# Copyright 2024 Alexander Zuepke

MARRON_BSP_NAME="NXP i.MX 8M Plus with ARM Cortex A53"
MARRON_ARCH=arm
MARRON_SUBARCH=cortex-a53
MARRON_SMP=yes
