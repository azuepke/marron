/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * stddef.h
 *
 * Standard C definitions.
 *
 * azuepke, 2013-09-17: initial
 * azuepke, 2017-10-02: imported
 * azuepke, 2021-07-29: POSIX conformity
 */

#ifndef STDDEF_H_
#define STDDEF_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_ptrdiff_t
#define __NEED_wchar_t
#define __NEED_size_t
#include <bits/alltypes.h>

#ifndef NULL
#ifdef __cplusplus
#define NULL 0L
#else
#define NULL ((void*)0)
#endif
#endif

/** offsetof() */
#if (defined(__GNUC_MAJOR__) && (__GNUC_MAJOR__ >= 4)) || (__GNUC__ >= 5)
#define offsetof(type, member)	__builtin_offsetof(type, member)
#else
#define offsetof(type, member)	((size_t)(&((type *)0)->member))
#endif

/* for C11 */
typedef struct {
	long long __ll;
	long double __ld;
} max_align_t;

#ifdef __cplusplus
}
#endif

#endif
