/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * stdnoreturn.h
 *
 * Standard C definitions.
 *
 * azuepke, 2022-08-13: initial
 */

#ifndef STDNORETURN_H_
#define STDNORETURN_H_

#ifndef __cplusplus

/* fallback for pre-C11 compilation mode */
#if __STDC_VERSION__ < 201112L
#define _Noreturn	__attribute__((__noreturn__))
#endif

#define noreturn	_Noreturn

#endif

#endif
