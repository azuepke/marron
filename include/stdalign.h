/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * stdalign.h
 *
 * Standard C definitions.
 *
 * azuepke, 2022-08-13: initial
 */

#ifndef STDALIGN_H_
#define STDALIGN_H_

#ifndef __cplusplus

/* fallback for pre-C11 compilation mode */
#if __STDC_VERSION__ < 201112L
#define _Alignas(val)	__attribute__((__aligned__(val)))
#define _Alignof(type)	__builtin_offsetof(struct { char __c; typeof(type) __t; }, __t)
#endif

#define alignas	_Alignas
#define alignof	_Alignof

#endif

#define __alignas_is_defined 1
#define __alignof_is_defined 1

#endif
