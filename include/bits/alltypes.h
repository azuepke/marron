/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022 Alexander Zuepke */
/*
 * bits/alltypes.h
 *
 * C library
 *
 * This header is an internal header of the C library.
 * Indirectly included by other headers, does not need to be C++ safe.
 *
 * We follow musl's convention and define __NEED_xxx for all types we need.
 * The actual types follow the definition of Linux and musl.
 *
 * azuepke, 2021-07-26: initial
 * azuepke, 2022-08-10: more POSIX types
 */

#if __SIZEOF_LONG__ == 8
#define _Int64	long
#define _Reg	long
#else
#define _Int64	long long
#define _Reg	int
#endif


/* unsigned base types */
#if defined(__NEED_uint8_t) && !defined(__DEFINED_uint8_t)
#define __DEFINED_uint8_t
typedef unsigned char uint8_t;
#endif

#if defined(__NEED_uint16_t) && !defined(__DEFINED_uint16_t)
#define __DEFINED_uint16_t
typedef unsigned short uint16_t;
#endif

#if defined(__NEED_uint32_t) && !defined(__DEFINED_uint32_t)
#define __DEFINED_uint32_t
typedef unsigned int uint32_t;
#endif

#if defined(__NEED_uint64_t) && !defined(__DEFINED_uint64_t)
#define __DEFINED_uint64_t
typedef unsigned _Int64 uint64_t;
#endif


/* signed base types */
#if defined(__NEED_int8_t) && !defined(__DEFINED_int8_t)
#define __DEFINED_int8_t
typedef signed char int8_t;
#endif

#if defined(__NEED_int16_t) && !defined(__DEFINED_int16_t)
#define __DEFINED_int16_t
typedef signed short int16_t;
#endif

#if defined(__NEED_int32_t) && !defined(__DEFINED_int32_t)
#define __DEFINED_int32_t
typedef signed int int32_t;
#endif

#if defined(__NEED_int64_t) && !defined(__DEFINED_int64_t)
#define __DEFINED_int64_t
typedef signed _Int64 int64_t;
#endif


/* special */
#if defined(__NEED_va_list) && !defined(__DEFINED_va_list)
#define __DEFINED_va_list
typedef __builtin_va_list va_list;
#endif


/* wchar.h */
#if defined(__NEED_wchar_t) && !defined(__DEFINED_wchar_t)
#define __DEFINED_wchar_t
typedef unsigned int wchar_t;
#endif

#if defined(__NEED_wctype_t) && !defined(__DEFINED_wctype_t)
#define __DEFINED_wctype_t
typedef unsigned long wctype_t;
#endif

#if defined(__NEED_wint_t) && !defined(__DEFINED_wint_t)
#define __DEFINED_wint_t
typedef unsigned int wint_t;
#endif


/* pointer-conversion and sizes */
#if defined(__NEED_uintptr_t) && !defined(__DEFINED_uintptr_t)
#define __DEFINED_uintptr_t
typedef unsigned _Reg uintptr_t;
#endif

#if defined(__NEED_intptr_t) && !defined(__DEFINED_intptr_t)
#define __DEFINED_intptr_t
typedef _Reg intptr_t;
#endif

#if defined(__NEED_ptrdiff_t) && !defined(__DEFINED_ptrdiff_t)
#define __DEFINED_ptrdiff_t
typedef _Reg ptrdiff_t;
#endif

#if defined(__NEED_size_t) && !defined(__DEFINED_size_t)
#define __DEFINED_size_t
typedef unsigned _Reg size_t;
#endif

#if defined(__NEED_ssize_t) && !defined(__DEFINED_ssize_t)
#define __DEFINED_ssize_t
typedef _Reg ssize_t;
#endif


/* POSIX types and I/O */
#if defined(__NEED_pid_t) && !defined(__DEFINED_pid_t)
#define __DEFINED_pid_t
typedef int pid_t;
#endif

#if defined(__NEED_uid_t) && !defined(__DEFINED_uid_t)
#define __DEFINED_uid_t
typedef unsigned int uid_t;
#endif

#if defined(__NEED_gid_t) && !defined(__DEFINED_gid_t)
#define __DEFINED_gid_t
typedef unsigned int gid_t;
#endif

#if defined(__NEED_id_t) && !defined(__DEFINED_id_t)
#define __DEFINED_id_t
typedef unsigned int id_t;
#endif

#if defined(__NEED_mode_t) && !defined(__DEFINED_mode_t)
#define __DEFINED_mode_t
typedef unsigned int mode_t;
#endif

#if defined(__NEED_blkcnt_t) && !defined(__DEFINED_blkcnt_t)
#define __DEFINED_blkcnt_t
typedef _Int64 blkcnt_t;
#endif

#if defined(__NEED_blksize_t) && !defined(__DEFINED_blksize_t)
#define __DEFINED_blksize_t
typedef long blksize_t;
#endif

#if defined(__NEED_dev_t) && !defined(__DEFINED_dev_t)
#define __DEFINED_dev_t
typedef unsigned _Int64 dev_t;
#endif

#if defined(__NEED_fsblkcnt_t) && !defined(__DEFINED_fsblkcnt_t)
#define __DEFINED_fsblkcnt_t
typedef unsigned _Int64 fsblkcnt_t;
#endif

#if defined(__NEED_fsfilcnt_t) && !defined(__DEFINED_fsfilcnt_t)
#define __DEFINED_fsfilcnt_t
typedef unsigned _Int64 fsfilcnt_t;
#endif

#if defined(__NEED_ino_t) && !defined(__DEFINED_ino_t)
#define __DEFINED_ino_t
typedef unsigned _Int64 ino_t;
#endif

#if defined(__NEED_key_t) && !defined(__DEFINED_key_t)
#define __DEFINED_key_t
typedef int key_t;
#endif

#if defined(__NEED_nlink_t) && !defined(__DEFINED_nlink_t)
#define __DEFINED_nlink_t
typedef unsigned _Reg nlink_t;
#endif

#if defined(__NEED_off_t) && !defined(__DEFINED_off_t)
#define __DEFINED_off_t
typedef _Int64 off_t;
#endif

#if defined(__NEED_struct_iovec) && !defined(__DEFINED_struct_iovec)
#define __DEFINED_struct_iovec
struct iovec {
	void *iov_base;
	size_t iov_len;
};
#endif


/* time keeping */
#if defined(__NEED_time_t) && !defined(__DEFINED_time_t)
#define __DEFINED_time_t
typedef _Int64 time_t;
#endif

#if defined(__NEED_suseconds_t) && !defined(__DEFINED_suseconds_t)
#define __DEFINED_suseconds_t
typedef _Int64 suseconds_t;
#endif

#if defined(__NEED_clock_t) && !defined(__DEFINED_clock_t)
#define __DEFINED_clock_t
typedef long clock_t;
#endif

#if defined(__NEED_clockid_t) && !defined(__DEFINED_clockid_t)
#define __DEFINED_clockid_t
typedef int clockid_t;
#endif

#if defined(__NEED_timer_t) && !defined(__DEFINED_timer_t)
#define __DEFINED_timer_t
typedef void *timer_t;
#endif

#if defined(__NEED_locale_t) && !defined(__DEFINED_locale_t)
#define __DEFINED_locale_t
typedef struct __locale_struct *locale_t;
#endif

#if defined(__NEED_struct_timespec) && !defined(__DEFINED_struct_timespec)
#define __DEFINED_struct_timespec
struct timespec {
	time_t tv_sec;
#if (__SIZEOF_LONG__ == 4) && (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
	unsigned int padding;
#endif
	long tv_nsec;
#if (__SIZEOF_LONG__) == 4 && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	unsigned int padding;
#endif
};
#endif

#if defined(__NEED_struct_itimerspec) && !defined(__DEFINED_struct_itimerspec)
#define __DEFINED_struct_itimerspec
struct itimerspec {
	struct timespec it_interval;
	struct timespec it_value;
};
#endif

#if defined(__NEED_struct_timeval) && !defined(__DEFINED_struct_timeval)
#define __DEFINED_struct_timeval
struct timeval {
	time_t tv_sec;
	suseconds_t tv_usec;
};
#endif

#if defined(__NEED_struct_itimerval) && !defined(__DEFINED_struct_itimerval)
#define __DEFINED_struct_itimerval
struct itimerval {
	struct timeval it_interval;
	struct timeval it_value;
};
#endif

#if defined(__NEED_struct_tm) && !defined(__DEFINED_struct_tm)
#define __DEFINED_struct_tm
struct tm {
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
	int tm_wday;
	int tm_yday;
	int tm_isdst;
};
#endif


/* signal handling */
#if defined(__NEED_sigval_t) && !defined(__DEFINED_sigval_t)
#define __DEFINED_sigval_t
typedef union sigval {
	int sival_int;
	void *sival_ptr;
} sigval_t;
#endif

#if defined(__NEED_sigset_t) && !defined(__DEFINED_sigset_t)
#define __DEFINED_sigset_t
typedef struct {
	unsigned long long __bits;
} sigset_t;
#endif


/* pthreads */
#if defined(__NEED_pthread_t) && !defined(__DEFINED_pthread_t)
#define __DEFINED_pthread_t
#ifdef __cplusplus
typedef unsigned long pthread_t;
#else
typedef struct __pthread *pthread_t;
#endif
#endif

#if defined(__NEED_pthread_attr_t) && !defined(__DEFINED_pthread_attr_t)
#define __DEFINED_pthread_attr_t
typedef struct {
	void *stackaddr;
	unsigned int stacksize;
	unsigned int guardsize;
	unsigned char detachstate;
	unsigned char inheritsched;
	unsigned char sched_policy;
	unsigned char sched_priority;
	unsigned int padding;
	unsigned long affinity_mask;
} pthread_attr_t;
#endif

#if defined(__NEED_pthread_mutex_t) && !defined(__DEFINED_pthread_mutex_t)
#define __DEFINED_pthread_mutex_t
typedef struct {
	unsigned int lock;
	unsigned char flags;
	unsigned char prioceiling;
	unsigned char recursion_count;
	unsigned char prevprio;
} __attribute__((__aligned__(8))) pthread_mutex_t;
#endif

#if defined(__NEED_pthread_mutexattr_t) && !defined(__DEFINED_pthread_mutexattr_t)
#define __DEFINED_pthread_mutexattr_t
typedef struct {
	unsigned char flags;
	unsigned char prioceiling;
	unsigned char padding[2];
} __attribute__((__aligned__(4))) pthread_mutexattr_t;
#endif

#if defined(__NEED_pthread_cond_t) && !defined(__DEFINED_pthread_cond_t)
#define __DEFINED_pthread_cond_t
typedef struct {
	void *mutex;
	unsigned int seq_flags;
#if __SIZEOF_LONG__ == 8
	unsigned char padding[4];
#endif
} __attribute__((__aligned__(16))) pthread_cond_t;
#endif

#if defined(__NEED_pthread_condattr_t) && !defined(__DEFINED_pthread_condattr_t)
#define __DEFINED_pthread_condattr_t
typedef struct {
	unsigned char flags;
	unsigned char padding[3];
} __attribute__((__aligned__(4))) pthread_condattr_t;
#endif

#if defined(__NEED_pthread_barrier_t) && !defined(__DEFINED_pthread_barrier_t)
#define __DEFINED_pthread_barrier_t
typedef struct {
	unsigned int lock;
	unsigned short count;
	unsigned char flags;
	unsigned char padding[1];
} __attribute__((__aligned__(8))) pthread_barrier_t;
#endif

#if defined(__NEED_pthread_barrierattr_t) && !defined(__DEFINED_pthread_barrierattr_t)
#define __DEFINED_pthread_barrierattr_t
typedef struct {
	unsigned char flags;
	unsigned char padding[3];
} __attribute__((__aligned__(4))) pthread_barrierattr_t;
#endif

#if defined(__NEED_pthread_rwlock_t) && !defined(__DEFINED_pthread_rwlock_t)
#define __DEFINED_pthread_rwlock_t
typedef struct {
	int counter;
	unsigned int seq_flags;
} __attribute__((__aligned__(8))) pthread_rwlock_t;
#endif

#if defined(__NEED_pthread_rwlockattr_t) && !defined(__DEFINED_pthread_rwlockattr_t)
#define __DEFINED_pthread_rwlockattr_t
typedef struct {
	unsigned char flags;
	unsigned char padding[3];
} __attribute__((__aligned__(4))) pthread_rwlockattr_t;
#endif

#if defined(__NEED_pthread_once_t) && !defined(__DEFINED_pthread_once_t)
#define __DEFINED_pthread_once_t
typedef struct {
	unsigned int lock;
} pthread_once_t;
#endif

#if defined(__NEED_pthread_spinlock_t) && !defined(__DEFINED_pthread_spinlock_t)
#define __DEFINED_pthread_spinlock_t
typedef struct {
	unsigned int lock;
} pthread_spinlock_t;
#endif

#if defined(__NEED_pthread_key_t) && !defined(__DEFINED_pthread_key_t)
#define __DEFINED_pthread_key_t
typedef unsigned pthread_key_t;
#endif


/* types used by internally by musl */
#if defined(__NEED_float_t) && !defined(__DEFINED_float_t)
#define __DEFINED_float_t
typedef float float_t;
#endif

#if defined(__NEED_double_t) && !defined(__DEFINED_double_t)
#define __DEFINED_double_t
typedef double double_t;
#endif


#undef _Int64
#undef _Reg
