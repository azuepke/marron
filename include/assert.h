/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * assert.h
 *
 * Assert.
 *
 * azuepke, 2013-05-03: split from kernel.h
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2021-08-18: conforming
 */

#ifndef ASSERT_H_
#define ASSERT_H_

#ifdef __cplusplus
extern "C" {
#endif

/** assert internal function */
/* NOTE: this function is always available, regardless of NDEBUG settings */
void __assert_fail(const char *cond, const char *file, int line, const char *func) __attribute__((__noreturn__));

/** runtime assert */
#ifdef NDEBUG
#define assert(cond)	((void)0)
#else
#define assert(cond)	((void)((cond) ? 0 : __assert_fail(#cond, __FILE__, __LINE__, __FUNCTION__)))
#endif

/** compile-time assert */
#ifndef __cplusplus

/* fallback definition of _Static_assert for pre-C11 compilation mode */
#if __STDC_VERSION__ < 201112L
#define _Static_assert(cond, ...)	\
	_Pragma("GCC diagnostic push")	\
	_Pragma("GCC diagnostic ignored \"-Wunused-local-typedefs\"")	\
	_Pragma("GCC diagnostic ignored \"-Wdeclaration-after-statement\"")	\
	typedef char static_assert_failed[(cond) ? 1 : -1];	\
	_Pragma("GCC diagnostic push")
#endif

/** static assert, verified at compile time */
#define static_assert	_Static_assert

#endif

#ifdef __cplusplus
}
#endif

#endif
