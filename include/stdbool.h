/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * stdbool.h
 *
 * Standard C definitions.
 *
 * azuepke, 2021-07-29: initial
 */

#ifndef STDBOOL_H_
#define STDBOOL_H_

#ifndef __cplusplus

#define bool _Bool
#define true 1
#define false 0

#endif

#define __bool_true_false_are_defined 1

#endif
