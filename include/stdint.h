/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2022 Alexander Zuepke */
/*
 * stdint.h
 *
 * Standard integer types.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported
 * azuepke, 2022-08-10: conforming
 */

#ifndef STDINT_H_
#define STDINT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_uint8_t
#define __NEED_uint16_t
#define __NEED_uint32_t
#define __NEED_uint64_t
#define __NEED_int8_t
#define __NEED_int16_t
#define __NEED_int32_t
#define __NEED_int64_t
#define __NEED_intptr_t
#define __NEED_uintptr_t
#define __NEED_size_t
#define __NEED_ssize_t
#define __NEED_wchar_t
#define __NEED_wint_t
#include <bits/alltypes.h>

/* 128 bit types */
#ifdef __HAS_INT128
typedef __uint128_t uint128_t;
#endif
#ifdef __HAS_INT128
typedef __int128_t int128_t;
#endif

/* limits */
#define INT8_MAX	0x7f
#define INT16_MAX	0x7fff
#define INT32_MAX	0x7fffffff
#if __SIZEOF_LONG__ == 8
#define INT64_MAX	0x7fffffffffffffffl
#else
#define INT64_MAX	0x7fffffffffffffffll
#endif

#define INT8_MIN	(-INT8_MAX-1)
#define INT16_MIN	(-INT16_MAX-1)
#define INT32_MIN	(-INT32_MAX-1)
#define INT64_MIN	(-INT64_MAX-1)

#define UINT8_MAX	0xffu
#define UINT16_MAX	0xffffu
#define UINT32_MAX	0xffffffffu
#if __SIZEOF_LONG__ == 8
#define UINT64_MAX	0xfffffffffffffffful
#else
#define UINT64_MAX	0xffffffffffffffffull
#endif


/* least types */
typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;
typedef uint32_t uint_least32_t;
typedef uint64_t uint_least64_t;

typedef int8_t int_least8_t;
typedef int16_t int_least16_t;
typedef int32_t int_least32_t;
typedef int64_t int_least64_t;

#define INT_LEAST8_MIN		INT8_MIN
#define INT_LEAST16_MIN		INT16_MIN
#define INT_LEAST32_MIN		INT32_MIN
#define INT_LEAST64_MIN		INT64_MIN

#define INT_LEAST8_MAX		INT8_MAX
#define INT_LEAST16_MAX		INT16_MAX
#define INT_LEAST32_MAX		INT32_MAX
#define INT_LEAST64_MAX		INT64_MAX

#define UINT_LEAST8_MAX		UINT8_MAX
#define UINT_LEAST16_MAX	UINT16_MAX
#define UINT_LEAST32_MAX	UINT32_MAX
#define UINT_LEAST64_MAX	UINT64_MAX


/* fast types */
/* NOTE: [u]int_fast16_t uses 32-bit */
typedef uint8_t uint_fast8_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
typedef uint64_t uint_fast64_t;

typedef int8_t int_fast8_t;
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef int64_t int_fast64_t;

#define INT_FAST8_MIN	INT8_MIN
#define INT_FAST16_MIN	INT32_MIN
#define INT_FAST32_MIN	INT32_MIN
#define INT_FAST64_MIN	INT64_MIN

#define INT_FAST8_MAX	INT8_MAX
#define INT_FAST16_MAX	INT32_MAX
#define INT_FAST32_MAX	INT32_MAX
#define INT_FAST64_MAX	INT64_MAX

#define UINT_FAST8_MAX	UINT8_MAX
#define UINT_FAST16_MAX	UINT32_MAX
#define UINT_FAST32_MAX	UINT32_MAX
#define UINT_FAST64_MAX	UINT64_MAX

/* intmax types */
typedef uint64_t uintmax_t;
typedef int64_t intmax_t;

#define INTMAX_MIN	INT64_MIN
#define INTMAX_MAX	INT64_MAX
#define UINTMAX_MAX	UINT64_MAX


/* pointer and size limits */
#if __SIZEOF_LONG__ == 8
#define PTRDIFF_MIN	INT64_MIN
#define PTRDIFF_MAX	INT64_MAX
#define SIZE_MAX	UINT64_MAX
#else
#define PTRDIFF_MIN	INT32_MIN
#define PTRDIFF_MAX	INT32_MAX
#define SIZE_MAX	UINT32_MAX
#endif


/* sig_atomic_t limits */
#define SIG_ATOMIC_MIN	INT32_MIN
#define SIG_ATOMIC_MAX	INT32_MAX


/* wchar_t and wint_t limits */
#if L'\0' - 1 > 0
#define WCHAR_MIN	(0u + L'\0')
#define WCHAR_MAX	(UINT32_MAX + L'\0')
#else
#define WCHAR_MIN	(INT32_MIN + L'\0')
#define WCHAR_MAX	(INT32_MAX + L'\0')
#endif

#define WINT_MAX	UINT32_MAX
#define WINT_MIN	0u


/* macros for integer constant expressions */
#define INT8_C(value)		value
#define INT16_C(value)		value
#define INT32_C(value)		value

#define UINT8_C(value)		value
#define UINT16_C(value)		value
#define UINT32_C(value)		value ## u

#if __SIZEOF_LONG__ == 8
#define INT64_C(value)		value ## l
#define UINT64_C(value)		value ## ul
#define INTMAX_C(value)		value ## l
#define UINTMAX_C(value)	value ## ul
#else
#define INT64_C(value)		value ## ll
#define UINT64_C(value)		value ## ull
#define INTMAX_C(value)		value## ll
#define UINTMAX_C(value)	value ## ull
#endif

#ifdef __cplusplus
}
#endif

#endif
