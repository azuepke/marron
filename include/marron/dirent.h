/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * marron/dirent.h
 *
 * Directory entry type.
 *
 * azuepke, 2022-08-15: initial
 */

#ifndef MARRON_DIRENT_H_
#define MARRON_DIRENT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define __NEED_ino_t
#define __NEED_off_t
#include <bits/alltypes.h>

/* directory entry */
struct dirent {
	ino_t d_ino;				/**< inode of file */
	off_t d_off;				/**< index/key to next entry (opaque) */
	unsigned short d_reclen;	/**< length of this record, including padding */
	unsigned char d_type;		/**< file type */
	unsigned char d_namlen;		/**< length of file name */
	char d_name[255 + 1];		/**< file name, NUL-terminated */
};

/* file types */
/* NOTE: follows other systems for interchangeable file systems */
/* NOTE: the encoding matches S_IFMT shifted right by 12 bits */
#define DT_UNKNOWN	0
#define DT_FIFO		1
#define DT_CHR		2
#define DT_DIR		4
#define DT_BLK		6
#define DT_REG		8
#define DT_LNK		10
#define DT_SOCK		12

#ifdef __cplusplus
}
#endif

#endif
