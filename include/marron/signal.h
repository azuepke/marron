/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * marron/signal.h
 *
 * Marron-specific signals.
 *
 * azuepke, 2021-05-18: initial, following the POSIX 2017 standard
 */

#ifndef MARRON_SIGNAL_H_
#define MARRON_SIGNAL_H_

/* kernel API signals */
/* NOTE: order follows Linux for x86 and ARM */
#define SIGHUP		1
#define SIGINT		2	/* ISO C; not raised */
#define SIGQUIT		3
#define SIGILL		4	/* ISO C; exception */
#define SIGTRAP		5	/* exception */
#define SIGABRT		6	/* ISO C; abort() */
#define SIGIOT		SIGABRT	/* alias name */
#define SIGBUS		7	/* ISO C; exception */
/*#define SIGEMT	SIGBUS	-- SVR4 and BSD; not supported */
#define SIGFPE		8	/* ISO C; exception */
#define SIGKILL		9	/* special; implicitly kill process */
#define SIGUSR1		10
#define SIGSEGV		11	/* exception */
#define SIGUSR2		12
#define SIGPIPE		13
#define SIGALRM		14
#define SIGTERM		15	/* ISO C; indicate pending process termination */
/*#define SIGCANCEL	16	-- private; thread cancellation */
/*#define SIGSTKFLT	16	-- Linux; unused */
#define SIGCHLD		17	/* not raised; ignored by default */
/*#define SIGCLD	SIGCHLD	-- System V; compatibility name */
#define SIGCONT		18	/* special; continue execution; not supported */
#define SIGSTOP		19	/* special; stop execution; not supported */
#define SIGTSTP		20	/* not supported */
#define SIGTTIN		21	/* not supported */
#define SIGTTOU		22	/* not supported */
#define SIGURG		23	/* socket; ignored by default */
#define SIGXCPU		24	/* realtime; deadline missed */
#define SIGXFSZ		25	/* not raised */
#define SIGVTALRM	26	/* not supported */
#define SIGPROF		27	/* not supported; obsolete */
#define SIGWINCH	28	/* non-POSIX; ignored by default */
#define SIGPOLL		29	/* obsolete; not supported */
#define SIGIO		SIGPOLL	/* non-standard; not supported */
/*#define SIGTIMER	30		-- private; timer expiration */
/*#define SIGPWR	30		-- non-standard; not raised */
/*#define SIGINFO	SIGPWR	-- BSD; not raised */
/*#define SIGLOST	SIGPWR	-- Solaris; not raised */
#define SIGSYS		31	/* exception; SVR4 and BSD; not raised */
/*#define SIGUNUSED	SIGSYS	-- Linux; not raised */

#define SIGRTMIN	32
#define SIGRTMAX	63

#define _NSIG		64


/* si_code */
/* SIGILL */
#define ILL_ILLOPC	1
#define ILL_ILLOPN	2
#define ILL_ILLADR	3
#define ILL_ILLTRP	4
#define ILL_PRVOPC	5
#define ILL_PRVREG	6
#define ILL_COPROC	7
#define ILL_BADSTK	8

/* SIGFPE */
#define FPE_INTDIV	1
#define FPE_INTOVF	2
#define FPE_FLTDIV	3
#define FPE_FLTOVF	4
#define FPE_FLTUND	5
#define FPE_FLTRES	6
#define FPE_FLTINV	7
#define FPE_FLTSUB	8
#define FPE_FLTUNK	9	/* Linux addon (value 14 on Linux) */
#define FPE_UNAVAIL	10	/* Addon to indicate that the FPU is disabled */

/* SIGSEGV */
#define SEGV_MAPERR	1
#define SEGV_ACCERR	2

/* SIGBUS */
#define BUS_ADRALN	1
#define BUS_ADRERR	2
#define BUS_OBJERR	3

/* SIGTRAP */
#define TRAP_BRKPT	1
#define TRAP_TRACE	2
#define TRAP_BRANCH	3	/* Linux addon */
#define TRAP_HWBKPT	4	/* Linux addon */
#define TRAP_UNK	5	/* Linux addon */

/* SIGCHLD */
#define CLD_EXITED	1
#define CLD_KILLED	2
#define CLD_DUMPED	3
#define CLD_TRAPPED	4
#define CLD_STOPPED	5
#define CLD_CONTINUED	6

/* SIGPOLL */
#define POLL_IN	1
#define POLL_OUT	2
#define POLL_MSG	3
#define POLL_ERR	4
#define POLL_PRI	5
#define POLL_HUP	6

/* other */
#define SI_USER	0
#define SI_QUEUE	(-1)
#define SI_TIMER	(-2)
#define SI_ASYNCIO	(-3)
#define SI_MESGQ	(-4)

#endif
