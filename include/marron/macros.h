/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * marron/macros.h
 *
 * Common helper macros
 *
 * azuepke, 2021-01-21: initial import from kernel.h
 * azuepke, 2021-05-05: renamed helper.h to macros.h
 */

#ifndef MARRON_MACROS_H_
#define MARRON_MACROS_H_

/** align address up, alignment must be a power of two */
#define ALIGN_UP(addr, align)	(((addr) + (align) - 1UL) & ~((align) - 1UL))

/** align address down, alignment must be a power of two */
#define ALIGN_DOWN(addr, align)	( (addr)                  & ~((align) - 1UL))

/** get CPU mask for "num" CPUs (and don't shift beyond 32/64 bit) */
#define CPU_ALL_MASK(num)	((((1UL << ((num) - 1)) -1) << 1) | 1UL)

/** convert CPU ID "cpu" to a CPU mask */
#define CPU_TO_MASK(cpu)	(1UL << (cpu))

#endif
