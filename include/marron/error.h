/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2018, 2021 Alexander Zuepke */
/*
 * marron/error.h
 *
 * Marron-specific error codes.
 *
 * azuepke, 2013-05-09: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-01-04: cleaned up for API documentation
 * azuepke, 2018-12-21: standalone version
 * azuepke, 2021-07-19: new error codes for POSIX
 * azuepke, 2021-11-11: split into error.h and errno.h
 */

#ifndef MARRON_ERROR_H_
#define MARRON_ERROR_H_

#include <marron/errno.h>

/* kernel API error codes */
#define EOK				0	/**< no error */
#define ELIMIT			3	/**< value outside configured limits */
#define ESTATE			8	/**< resource in wrong state */
#define ETYPE			9	/**< resource has wrong type */
#define ECANCEL			ECANCELED

/* kernel internal error codes (never reported to user space) */
#define ERESTARTSYS		512	/**< restart system call  */

#endif
