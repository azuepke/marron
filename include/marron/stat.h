/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * marron/stat.h
 *
 * POSIX-conforming file flags and modes.
 *
 * azuepke, 2022-07-17: initial
 */

#ifndef MARRON_STAT_H_
#define MARRON_STAT_H_

/* mode_t file access modes */
/* NOTE: follows POSIX; POSIX requires octal encoding */
#define S_IXOTH		0000001	/**< --x by others */
#define S_IWOTH		0000002	/**< -w- by others */
#define S_IROTH		0000004	/**< r-- by others */
#define S_IRWXO		0000007	/**< rwx by others */

#define S_IXGRP		0000010	/**< --x by group */
#define S_IWGRP		0000020	/**< -w- by group */
#define S_IRGRP		0000040	/**< r-- by group */
#define S_IRWXG		0000070	/**< rwx by group */

#define S_IXUSR		0000100	/**< --x by user */
#define S_IWUSR		0000200	/**< -w- by user */
#define S_IRUSR		0000400	/**< r-- by user */
#define S_IRWXU		0000700	/**< rwx by user */

#define S_ISVTX		0001000	/**< sticky flag (restricted deletion) */
#define S_ISGID		0002000	/**< SGID bit */
#define S_ISUID		0004000	/**< SUID bit */

/* mode_t type types (upper bits of file access modes) */
/* NOTE: follows other systems for interchangeable file systems */
/* NOTE: the encoding matches DT_XXX in struct dirent shifted left by 12 bits */
#define S_IFIFO		0010000	/**< FIFO special */
#define S_IFCHR		0020000	/**< character special */
#define S_IFDIR		0040000	/**< directory */
#define S_IFBLK		0060000	/**< block special */

#define S_IFREG		0100000	/**< regular */
#define S_IFLNK		0120000	/**< symbolic link */
#define S_IFSOCK	0140000	/**< socket */

#define S_IFMT		0170000	/**< type of file */

#endif
