/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022 Alexander Zuepke */
/*
 * marron/atomic.h
 *
 * Atomic operations.
 *
 * The provided atomic operations follow the spirit of C11 atomics,
 * but do not offer the full set operations defined by the standard.
 * Instead, we offer some useful addons.
 * However, all macros that match C11 names follow C11 semantics.
 *
 * Note that these atomics depend on GCC atomic builtins
 * and may not be portable, however they are supported by Clang; see:
 * - https://gcc.gnu.org/onlinedocs/gcc/_005f_005fatomic-Builtins.html
 * - https://clang.llvm.org/docs/LanguageExtensions.html#langext-c11-atomic
 *
 * azuepke, 2021-01-20: rework using GCC atomic builtins
 * azuepke, 2021-09-03: catch size mismatches in CAS
 */

#ifndef MARRON_ATOMIC_H_
#define MARRON_ATOMIC_H_

/* memory order -- mimics C11 memory order */
#define ATOMIC_RELAXED	__ATOMIC_RELAXED
#define ATOMIC_ACQUIRE	__ATOMIC_ACQUIRE
#define ATOMIC_RELEASE	__ATOMIC_RELEASE
#define ATOMIC_ACQ_REL	__ATOMIC_ACQ_REL
#define ATOMIC_SEQ_CST	__ATOMIC_SEQ_CST

/* C11 defines that the default operations are SEQ_CST */
/* we provide extra _relaxed, _acquire, and _release variants */
#define atomic_load(a)						__atomic_load_n((a), ATOMIC_SEQ_CST)
#define atomic_load_relaxed(a)				__atomic_load_n((a), ATOMIC_RELAXED)
#define atomic_load_acquire(a)				__atomic_load_n((a), ATOMIC_ACQUIRE)
#define atomic_load_explicit(a, m)			__atomic_load_n((a), (m))

#define atomic_store(a, v)					__atomic_store_n((a), (v), ATOMIC_SEQ_CST)
#define atomic_store_relaxed(a, v)			__atomic_store_n((a), (v), ATOMIC_RELAXED)
#define atomic_store_release(a, v)			__atomic_store_n((a), (v), ATOMIC_RELEASE)
#define atomic_store_explicit(a, v, m)		__atomic_store_n((a), (v), (m))

/* atomic swap -- returns previous value */
/* non-standard, but follows the spirit of atomic_exchange_*() */
#define atomic_swap(a, v)					__atomic_exchange_n((a), (v), ATOMIC_SEQ_CST)
#define atomic_swap_relaxed(a, v)			__atomic_exchange_n((a), (v), ATOMIC_RELAXED)
#define atomic_swap_explicit(a, v, m)		__atomic_exchange_n((a), (v), (m))

/* return the value BEFORE modification */
/* this follows C11 */
/* we provide extra _relaxed and _acquire variants */
/* the BIt Clear operation is a non-standard addon to handle bit operations */
#define atomic_fetch_add(a, v)				__atomic_fetch_add((a), (v), ATOMIC_SEQ_CST)
#define atomic_fetch_add_relaxed(a, v)		__atomic_fetch_add((a), (v), ATOMIC_RELAXED)
#define atomic_fetch_add_acquire(a, v)		__atomic_fetch_add((a), (v), ATOMIC_ACQUIRE)
#define atomic_fetch_add_explicit(a, v, m)	__atomic_fetch_add((a), (v), (m))
#define atomic_fetch_sub(a, v)				__atomic_fetch_sub((a), (v), ATOMIC_SEQ_CST)
#define atomic_fetch_sub_relaxed(a, v)		__atomic_fetch_sub((a), (v), ATOMIC_RELAXED)
#define atomic_fetch_sub_acquire(a, v)		__atomic_fetch_sub((a), (v), ATOMIC_ACQUIRE)
#define atomic_fetch_sub_explicit(a, v, m)	__atomic_fetch_sub((a), (v), (m))
#define atomic_fetch_and(a, v)				__atomic_fetch_and((a), (v), ATOMIC_SEQ_CST)
#define atomic_fetch_and_relaxed(a, v)		__atomic_fetch_and((a), (v), ATOMIC_RELAXED)
#define atomic_fetch_and_acquire(a, v)		__atomic_fetch_and((a), (v), ATOMIC_ACQUIRE)
#define atomic_fetch_and_explicit(a, v, m)	__atomic_fetch_and((a), (v), (m))
#define atomic_fetch_bic(a, v)				__atomic_fetch_and((a), ~(v), ATOMIC_SEQ_CST)
#define atomic_fetch_bic_relaxed(a, v)		__atomic_fetch_and((a), ~(v), ATOMIC_RELAXED)
#define atomic_fetch_bic_acquire(a, v)		__atomic_fetch_and((a), ~(v), ATOMIC_ACQUIRE)
#define atomic_fetch_bic_explicit(a, v, m)	__atomic_fetch_and((a), ~(v), (m))
#define atomic_fetch_or(a, v)				__atomic_fetch_or((a), (v), ATOMIC_SEQ_CST)
#define atomic_fetch_or_relaxed(a, v)		__atomic_fetch_or((a), (v), ATOMIC_RELAXED)
#define atomic_fetch_or_acquire(a, v)		__atomic_fetch_or((a), (v), ATOMIC_ACQUIRE)
#define atomic_fetch_or_explicit(a, v, m)	__atomic_fetch_or((a), (v), (m))
#define atomic_fetch_xor(a, v)				__atomic_fetch_xor((a), (v), ATOMIC_SEQ_CST)
#define atomic_fetch_xor_relaxed(a, v)		__atomic_fetch_xor((a), (v), ATOMIC_RELAXED)
#define atomic_fetch_xor_acquire(a, v)		__atomic_fetch_xor((a), (v), ATOMIC_ACQUIRE)
#define atomic_fetch_xor_explicit(a, v, m)	__atomic_fetch_xor((a), (v), (m))

/* simple add, sub and bitmask, return the value AFTER modification */
/* non-standard, we provide _relaxed, _release, and _explicit variants */
#define atomic_add_relaxed(a, v)			__atomic_add_fetch((a), (v), ATOMIC_RELAXED)
#define atomic_add_release(a, v)			__atomic_add_fetch((a), (v), ATOMIC_RELEASE)
#define atomic_add_explicit(a, v, m)		__atomic_add_fetch((a), (v), (m))
#define atomic_sub_relaxed(a, v)			__atomic_sub_fetch((a), (v), ATOMIC_RELAXED)
#define atomic_sub_release(a, v)			__atomic_sub_fetch((a), (v), ATOMIC_RELEASE)
#define atomic_sub_explicit(a, v, m)		__atomic_sub_fetch((a), (v), (m))
#define atomic_set_mask_relaxed(a, v)		__atomic_or_fetch((a), (v), ATOMIC_RELAXED)
#define atomic_set_mask_release(a, v)		__atomic_or_fetch((a), (v), ATOMIC_RELEASE)
#define atomic_set_mask_explicit(a, v, m)	__atomic_or_fetch((a), (v), (m))
#define atomic_clear_mask_relaxed(a, v)		__atomic_and_fetch((a), ~(v), ATOMIC_RELAXED)
#define atomic_clear_mask_release(a, v)		__atomic_and_fetch((a), ~(v), ATOMIC_RELEASE)
#define atomic_clear_mask_explicit(a, v, m)	__atomic_and_fetch((a), ~(v), (m))

/* compare and swap (CAS) with weak semantics and boolean return:
 *   tmp = *addr";
 *   if (tmp == old) {
 *     *addr = new; // weak semantics: can still fail here!
 *     return TRUE;
 *   }
 *   return FALSE;
 *
 * non-standard: We provide SEQ_CST, _relaxed, _acquire, and _release variants.
 * The wrappers expose their memory ordering semantincs only on success,
 * but not on failure of the atomic operation. They should be used in loops.
 */
#define atomic_cas(a, o, n)	({	\
		__typeof__(a) __tmp_a = (a);	\
		__typeof__(o) __tmp_o = (o);	\
		__typeof__(n) __tmp_n = (n);	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_o, "CAS: size mismatch old");	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_n, "CAS: size mismatch new");	\
		__atomic_compare_exchange_n(__tmp_a, &__tmp_o, __tmp_n, 0, ATOMIC_SEQ_CST, ATOMIC_RELAXED);	\
	})
#define atomic_cas_relaxed(a, o, n)	({	\
		__typeof__(a) __tmp_a = (a);	\
		__typeof__(o) __tmp_o = (o);	\
		__typeof__(n) __tmp_n = (n);	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_o, "CAS: size mismatch old");	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_n, "CAS: size mismatch new");	\
		__atomic_compare_exchange_n(__tmp_a, &__tmp_o, __tmp_n, 0, ATOMIC_ACQUIRE, ATOMIC_RELAXED);	\
	})
#define atomic_cas_acquire(a, o, n)	({	\
		__typeof__(a) __tmp_a = (a);	\
		__typeof__(o) __tmp_o = (o);	\
		__typeof__(n) __tmp_n = (n);	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_o, "CAS: size mismatch old");	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_n, "CAS: size mismatch new");	\
		__atomic_compare_exchange_n(__tmp_a, &__tmp_o, __tmp_n, 0, ATOMIC_ACQUIRE, ATOMIC_RELAXED);	\
	})
#define atomic_cas_release(a, o, n)	({	\
		__typeof__(a) __tmp_a = (a);	\
		__typeof__(o) __tmp_o = (o);	\
		__typeof__(n) __tmp_n = (n);	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_o, "CAS: size mismatch old");	\
		_Static_assert(sizeof *__tmp_a == sizeof __tmp_n, "CAS: size mismatch new");	\
		__atomic_compare_exchange_n(__tmp_a, &__tmp_o, __tmp_n, 0, ATOMIC_RELEASE, ATOMIC_RELAXED);	\
	})

/* memory barriers (following the spirit of C11) */
#define atomic_full_barrier()		__atomic_thread_fence(ATOMIC_SEQ_CST)
#define atomic_acquire_barrier()	__atomic_thread_fence(ATOMIC_ACQUIRE)
#define atomic_release_barrier()	__atomic_thread_fence(ATOMIC_RELEASE)

/* non-standard memory barriers */
#if defined(__i386__) || defined(__x86_64__)
#define atomic_load_barrier()		__asm__ volatile ("" : : : "memory")
#define atomic_store_barrier()		__asm__ volatile ("" : : : "memory")
#elif defined(__arm__)
#define atomic_load_barrier()		__asm__ volatile ("dmb ISH" : : : "memory")
#define atomic_store_barrier()		__asm__ volatile ("dmb ISHST" : : : "memory")
#elif defined(__aarch64__)
#define atomic_load_barrier()		__asm__ volatile ("dmb ISHLD" : : : "memory")
#define atomic_store_barrier()		__asm__ volatile ("dmb ISHST" : : : "memory")
#elif defined(__powerpc__) || defined(__powerpc64__)
#define atomic_load_barrier()		__asm__ volatile ("lwsync" : : : "memory")
#define atomic_store_barrier()		__asm__ volatile ("lwsync" : : : "memory")
#elif defined(__riscv) || defined(__riscv_xlen)
#define atomic_load_barrier()		__asm__ volatile ("fence r,r" : : : "memory")
#define atomic_store_barrier()		__asm__ volatile ("fence w,w" : : : "memory")
#else
#error Check atomics for this platform!
#endif

/* relax core on SMT/hyperthreading systems */
#if defined(__i386__) || defined(__x86_64__)
#define atomic_relax()				__asm__ volatile("pause" : : : "memory")
#elif defined(__arm__) || defined(__aarch64__)
#define atomic_relax()				__asm__ volatile("yield" : : : "memory")
#elif defined(__powerpc__) || defined(__powerpc64__)
#define atomic_relax()				__asm__ volatile("or 27, 27, 27" : : : "memory")
#elif defined(__riscv) || defined(__riscv_xlen)
#define atomic_relax()				__asm__ volatile(".word 0x0100000f" : : : "memory")	/* fence w,0 */
#else
#define atomic_relax()				__asm__ volatile("" : : : "memory")
#endif

#endif
