/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2021 Alexander Zuepke */
/*
 * marron/compiler.h
 *
 * Compiler specific macros.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2021-09-02: __has_attribute and fallthrough
 */

#ifndef MARRON_COMPILER_H_
#define MARRON_COMPILER_H_

/* these are gcc 3+ specific macros,
 * but most free compilers like LLVM/Clang, PCC or TinyCC also understand them
 */

/** preprocessor test if compiler recognizes a give attribute
 * __has_attribute is a builtin in both GCC and LLVM, so we provide a fallback
 */
#ifndef __has_attribute
#define __has_attribute(x) 0
#endif

/** compiler optimization barrier */
#define barrier()			__asm__ volatile("" : : : "memory")

/** optimization hints */
#define likely(cond)		__builtin_expect((cond) != 0, 1)
#define unlikely(cond)		__builtin_expect((cond) != 0, 0)
#define const_var(var)		__builtin_constant_p(var)
#if (__GNUC__ >= 4 && __GNUC_MINOR__ >= 5) || __GNUC__ >= 5
#define unreachable()		__builtin_unreachable()
#else
#define unreachable()		do { } while (1)
#endif

/** pseudo fallthrough keyword to prevent warnings in switch-case constructs */
#if __has_attribute(__fallthrough__)
#define fallthrough			__attribute__((__fallthrough__))
#else
/** NOTE: the comment already prevents the warning in GCC (but not in LLVM) */
#define fallthrough			do {} while (0)	/* fallthrough */
#endif


/** common attributes */
#define __packed			__attribute__((__packed__))
#define __unused			__attribute__((__unused__))
#define __used				__attribute__((__used__))
#if (__GNUC__ >= 4 && __GNUC_MINOR__ >= 8) || __GNUC__ >= 5
#define __really_used		__attribute__((__used__))
#else
#define __really_used
#endif
#define __noinline			__attribute__((__noinline__))
#define __always_inline		__attribute__((__always_inline__))
#define __noreturn			__attribute__((__noreturn__))
#define __pure				__attribute__((__pure__))
#define __const				__attribute__((__const__))
#define __section(s)		__attribute__((__section__(#s)))
#define __aligned(a)		__attribute__((__aligned__(a)))
#define __wur				__attribute__((__warn_unused_result__))
#define __nonnull(x...)		__attribute__((__nonnull__(x)))
#if (__GNUC__ >= 4 && __GNUC_MINOR__ >= 9) || __GNUC__ >= 5
#define __returns_nonnull	__attribute__((__returns_nonnull__))
#else
#define __returns_nonnull
#endif
#define __alloc_size(x...)	__attribute__((__alloc_size__(x)))
#define __malloc_type		__attribute__((__malloc__))

/** linkage */
#define __weak				__attribute__((__weak__))
#define __weakref(s)		__attribute__((__weak__, __alias__(#s)))
#define __alias(s)			__attribute__((__alias__(#s)))

/** hot or not? */
#if (__GNUC__ >= 4 && __GNUC_MINOR__ >= 3) || __GNUC__ >= 5
#define __cold				__attribute__((__cold__)) __section(.text.cold)
#else
#define __cold				__section(.text.cold)
#endif
#define __init				__section(.text.init)


/** preprocessor stringify magic */
#define __stringify2(x...)	#x
#define __stringify(x...)	__stringify2(x)


/** concatenate and expand macros */
#define __concatenate2(a,b)	a ## b
#define __concatenate(a,b)	__concatenate2(a, b)


/** formatting */
#define __printflike(n,m)	__attribute__((format(printf, (n), (m))))

/** typeof() */
#define typeof	__typeof__

/** countof() */
#define countof(a)	(sizeof(a) / sizeof((a)[0]))

/** alignof() */
#define alignof(type) offsetof(struct { char __c; typeof(type) __t; }, __t)

/** container_of() */
/* see http://www.kroah.com/log/linux/container_of.html */
#define container_of(ptr, type, member) ({	\
		typeof(((type *)0)->member) *__mptr = (ptr);	\
		(type *)((char *)__mptr - offsetof(type, member));	\
	})

/** const_container_of() */
#define const_container_of(ptr, type, member) ({	\
		typeof(((const type *)0)->member) *__mptr = (ptr);	\
		(const type *)((const char *)__mptr - offsetof(type, member));	\
	})

/* prevent compiler from reordering memory access */
#define access_once(x)	(*(volatile typeof(x) *)&(x))

#endif
