/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2023 Alexander Zuepke */
/*
 * marron/list.h
 *
 * Double linked lists.
 *
 * The double linked lists use the list head as sentinel.
 *
 * azuepke, 2013-03-27: initial
 * azuepke, 2017-10-02: imported
 * azuepke, 2021-08-29: remove dependency on compiler.h and new names
 * azuepke, 2023-02-12: clean naming of temp variables in macros
 */

#ifndef MARRON_LIST_H_
#define MARRON_LIST_H_

#include <stddef.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

/** internal list type */
struct list_str {
	struct list_str *next;
	struct list_str *prev;
};

/** list head or node */
typedef struct list_str list_t;


/** assert that a list is NULL */
#define assert_list_is_null(list)	\
	do {	\
		assert((list)->next == NULL);	\
		assert((list)->prev == NULL);	\
	} while (0)

/** assert that a list is not NULL */
#define assert_list_is_not_null(list)	\
	do {	\
		assert((list)->next != NULL);	\
		assert((list)->prev != NULL);	\
	} while (0)

/** add element n between a and b to list */
#define list_insert_internal(a, n, b)	\
	do {	\
		(b)->prev = (n);	\
		(n)->next = (b);	\
		(n)->prev = (a);	\
		(a)->next = (n);	\
	} while (0)

/** delete element n between a and b from list */
#define list_remove_internal(a, n, b)	\
	do {	\
		(a)->next = (b);	\
		(b)->prev = (a);	\
	} while (0)


/** static list head initializer */
#define LIST_HEAD_INIT(head) { &(head), &(head) }

/** init list head */
static inline void list_head_init(list_t *head)
{
	head->next = head;
	head->prev = head;
}


/** init list node */
static inline void list_node_init(list_t *node)
{
	(void)node;
#ifndef NDEBUG
	node->next = NULL;
	node->prev = NULL;
#endif
}

/** static list node initializer */
#define LIST_NODE_INIT(node) { NULL, NULL }


/** check if a list head is empty */
static inline int list_is_empty(const list_t *head)
{
	assert_list_is_not_null(head);
	return head->next == head;
}


/** get the first element of from a non-empty list */
static inline list_t *list_first_nonempty(const list_t *head)
{
	assert_list_is_not_null(head);
	return head->next;
}

/** get the last element of from a non-empty list */
static inline list_t *list_last_nonempty(const list_t *head)
{
	assert_list_is_not_null(head);
	return head->prev;
}

/** get the first element of from a list or NULL if the list is empty */
static inline list_t *list_first(const list_t *head)
{
	assert_list_is_not_null(head);
	return (head->next != head) ? head->next : NULL;
}

/** get the last element of from a list or NULL if the list is empty */
static inline list_t *list_last(const list_t *head)
{
	assert_list_is_not_null(head);
	return (head->prev != head) ? head->prev : NULL;
}


/** get the next element after a node in a list */
static inline list_t *list_next(const list_t *head, const list_t *node)
{
	assert_list_is_not_null(head);
	assert_list_is_not_null(node);
	return (node->next != head) ? node->next : NULL;
}

/** get the previous element before a node in a list */
static inline list_t *list_prev(const list_t *head, const list_t *node)
{
	assert_list_is_not_null(head);
	assert_list_is_not_null(node);
	return (node->prev != head) ? node->prev : NULL;
}


/** remove a linked node from its list */
static inline void list_remove(list_t *node)
{
	assert_list_is_not_null(node);

	list_remove_internal(node->prev, node, node->next);

#ifndef NDEBUG
	/* debugging */
	node->next = NULL;
	node->prev = NULL;
#endif
}

/** remove and return the first element from a non-empty list */
static inline list_t *list_remove_first_nonempty(list_t *head)
{
	list_t *node;

	node = list_first_nonempty(head);
	list_remove(node);

	return node;
}

/** remove and return the last element from a non-empty list */
static inline list_t *list_remove_last_nonempty(list_t *head)
{
	list_t *node;

	node = list_last_nonempty(head);
	list_remove(node);

	return node;
}

/** remove and return the first element from a list or NULL if the list is empty */
static inline list_t *list_remove_first(list_t *head)
{
	list_t *node;

	node = list_first(head);
	if (node != NULL) {
		list_remove(node);
	}

	return node;
}

/** remove and return the last element from a list or NULL if the list is empty */
static inline list_t *list_remove_last(list_t *head)
{
	list_t *node;

	node = list_last(head);
	if (node != NULL) {
		list_remove(node);
	}

	return node;
}


/** insert node at beginning of list */
static inline void list_insert_first(list_t *head, list_t *node)
{
	list_t *next;

	assert_list_is_not_null(head);
	assert_list_is_null(node);

	next = head->next;
	list_insert_internal(head, node, next);
}

/** insert node at tail of list */
static inline void list_insert_last(list_t *head, list_t *node)
{
	list_t *prev;

	assert_list_is_not_null(head);
	assert_list_is_null(node);

	prev = head->prev;
	list_insert_internal(prev, node, head);
}


/** concatenate non-empty list head2 at the end of list head1 */
static inline void list_concat(list_t *head1, list_t *head2)
{
	list_t *prev1, *prev2;

	assert_list_is_not_null(head1);
	assert_list_is_not_null(head2);

	prev1 = head1->prev;
	prev2 = head2->prev;

	head1->prev = prev2;
	prev1->next = head2;
	head2->prev = prev1;
	prev2->next = head1;
}


/** insert an entry into a sorted list where criteria becomes true */
/* NOTE: __ITER__ is the internal iterator */
#define list_insert_ordered(head, node, criteria)	\
	do {	\
		list_t *tmp_head_ = (head);	\
		list_t *tmp_node_ = (node);	\
		list_t *__ITER__, *tmp_prev_;	\
		assert_list_is_not_null(tmp_head_);	\
		assert_list_is_null(tmp_node_);	\
		list_for_each(tmp_head_, __ITER__)	\
			if (criteria)	\
				break;	\
		tmp_prev_ = __ITER__->prev;	\
		list_insert_internal(tmp_prev_, tmp_node_, __ITER__);	\
	} while (0)


/** simple list iterator -- do not delete list elements while iterating! */
#define list_for_each(head, iter)	\
	for ((iter) = (head)->next; (iter) != (head); (iter) = (iter)->next)

/** robust list iterator */
#define list_for_each_safe(head, iter, tmp)	\
	for ((iter) = (head)->next, (tmp) = (iter)->next;	\
		 (iter) != (head);	\
		 (iter) = (tmp), (tmp) = (tmp)->next)

/** drain list */
#define list_for_each_remove_first(head, first)	\
	while (((first) = list_remove_first(head)) != NULL)


/** magic cast to get surrounding data structure where node is embedded in */
#define list_entry(ptr, type, member)	\
	((type *)((char *)(ptr) - offsetof(type, member)))

#ifdef __cplusplus
}
#endif

#endif
