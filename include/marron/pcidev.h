/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * marron/pcidev.h
 *
 * PCI devices
 *
 * azuepke, 2025-01-22: initial
 */

#ifndef MARRON_PCIDEV_H_
#define MARRON_PCIDEV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/* ID for PCI devices */
typedef uint32_t pcidev_t;

/* we encode PCI devices as follows:
 *
 * bits 31..16: domain (16 bit)
 * bits 15.. 8: bus (8 bit)
 * bits  7.. 3: device (5 bit)
 * bits  2.. 0: function (3 bit)
 */
#define PCIDEV(dom, bus, dev, fn)	((dom) << 16 | (bus) << 8 | (dev) << 3 | (fn) << 0)

#define PCIDEV_DOMAIN(devid)		(((devid) & 0xffff0000u) >> 16)
#define PCIDEV_BUS(devid)			(((devid) & 0x0000ff00u) >>  8)
#define PCIDEV_DEV(devid)			(((devid) & 0x000000f8u) >>  5)
#define PCIDEV_FN(devid)			(((devid) & 0x00000007u) >>  0)

#define PCIDEV_DOMAIN_BUS(devid)	(((devid) & 0xffffff00u) >> 8)

#ifdef __cplusplus
}
#endif

#endif
