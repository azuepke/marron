/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * marron/irq.h
 *
 * IRQ operations and flags.
 *
 * azuepke, 2025-01-06: from former sys_irq_mode_t
 */

#ifndef MARRON_IRQ_H_
#define MARRON_IRQ_H_

/* IRQ mode flags in sys_irq_attach()
 *
 * The bits are encoded in the following way:
 * - bit 0:  LOW:   active on low level / falling edge
 * - bit 1:  HIGH:  active on high level / rising edge
 * - bit 2:  EDGE:  edge-triggered
 * - bit 3:  LEVEL: level-triggered
 *
 * Firstly, EDGE and LEVEL bits take precedence:
 * - If neither bits are set, the default configuration for the IRQ applies.
 * - If either bit is set, the IRQ will be configured in the respective mode.
 * - If both are set, the combination is invalid and results in an error.
 *
 * Secondly, LOW and HIGH bits configure the specific mode:
 * - If neither bits are set, the default configuration for the IRQ applies.
 * - For any other combination, the IRQ will be configured as such.
 * - Setting both HIGH and LOW in LEVEL mode is invalid and results in an error.
 *
 * The kernel rejects invalid combinations as mentioned above
 * and forwards the configuration request to the BSP. If the BSP
 * cannot configure the requested mode, it might return with an error, or
 * ignore the request mode and apply default settings and return successfully.
 */
#define _IRQ_MODE_LOW			0x1	/**< Low level / falling edge */
#define _IRQ_MODE_HIGH			0x2	/**< High level / rising edge */
#define _IRQ_MODE_FALLING		_IRQ_MODE_LOW
#define _IRQ_MODE_RISING		_IRQ_MODE_HIGH
#define _IRQ_MODE_EDGE			0x4	/**< Edge-triggered */
#define _IRQ_MODE_LEVEL			0x8	/**< Level-triggered */

/* IRQ modes for sys_irq_attach()
 *
 * The upper two bits encode EDGE/LEVEL, and the lower bits HIGH/LOW settings:
 * - modes 0..3 represent the default configuration mode (HIGH/LOW bits ignored)
 * - modes 4..7 represents the edge-triggered modes
 * - modes 8..10 represents the level-triggered modes
 * - mode 11 is invalid (LEVEL mode with both HIGH and LOW bits set)
 * - mode 12..15 are invalid (both EDGE and LEVEL mode bits set)
 */
#define IRQ_MODE_DEFAULT		0
#define IRQ_MODE_EDGE_DEFAULT	(_IRQ_MODE_EDGE)
#define IRQ_MODE_EDGE_FALLING	(_IRQ_MODE_EDGE | _IRQ_MODE_FALLING)
#define IRQ_MODE_EDGE_RISING	(_IRQ_MODE_EDGE | _IRQ_MODE_RISING)
#define IRQ_MODE_EDGE_BOTH		(_IRQ_MODE_EDGE | _IRQ_MODE_FALLING | _IRQ_MODE_RISING)
#define IRQ_MODE_LEVEL_DEFAULT	(_IRQ_MODE_LEVEL)
#define IRQ_MODE_LEVEL_LOW		(_IRQ_MODE_LEVEL | _IRQ_MODE_LOW)
#define IRQ_MODE_LEVEL_HIGH		(_IRQ_MODE_LEVEL | _IRQ_MODE_HIGH)

/* IRQ operations in sys_irq_ctl() */
#define IRQ_CTL_UNMASK			1	/**< Unmask (enable) IRQ */
#define IRQ_CTL_MASK			2	/**< Mask (disable) IRQ */
#define IRQ_CTL_CLEAR			3	/**< Clear pending interrupt count */

#endif
