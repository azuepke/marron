/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * marron/signalfd.h
 *
 * Linux-conforming signalfd structure.
 *
 * azuepke, 2025-02-14: initial
 */

#ifndef MARRON_SIGNALFD_H_
#define MARRON_SIGNALFD_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct signalfd_siginfo {
	uint32_t ssi_signo;		/* Kernel signal number */
	int32_t ssi_errno;		/* unused */
	int32_t ssi_code;		/* Kernel signal code */
	uint32_t ssi_pid;		/* SIGKILL, SIGCHLD, SIGRT* */
	uint32_t ssi_uid;		/* SIGKILL, SIGCHLD, SIGRT* */
	int32_t ssi_fd;			/* SIGIO */
	uint32_t ssi_tid;		/* SIGTIMER: si_timerid */
	uint32_t ssi_band;		/* SIGIO */
	uint32_t ssi_overrun;	/* SIGTIMER */
	uint32_t ssi_trapno;	/* SIGILL, SIGFPE, SIGSEGV, SIGBUS */
	int32_t ssi_status;		/* SIGCHLD */
	int32_t ssi_int;		/* SIGRT*: sival_int */
	uint64_t ssi_ptr;		/* SIGRT*: sival_ptr */
	uint64_t ssi_utime;		/* SIGCHLD (unused) */
	uint64_t ssi_stime;		/* SIGCHLD (unused) */
	uint64_t ssi_addr;		/* SIGILL, SIGFPE, SIGSEGV, SIGBUS */
	uint16_t ssi_addr_lsb;	/* SIGILL, SIGFPE, SIGSEGV, SIGBUS */
	uint16_t __pad2;
	int32_t ssi_syscall;	/* SIGSYS */
	uint64_t ssi_call_addr;	/* SIGSYS */
	uint32_t ssi_arch;		/* SIGSYS */
	uint8_t __pad[28];		/* 128 bytes */
};

#ifdef __cplusplus
}
#endif

#endif
