/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2020 Alexander Zuepke */
/*
 * marron/stack.h
 *
 * User space stack handling
 *
 * azuepke, 2018-04-10: initial
 * azuepke, 2020-02-02: rework for Kuri
 */

#ifndef MARRON_STACK_H_
#define MARRON_STACK_H_

/** define an opaque stack object "name" with "size_in_byte" stack inside */
#define SYS_STACK_DEFINE(name, size_in_bytes)	\
	struct {	\
		char stack[size_in_bytes] __attribute__((__aligned__(16)));	\
	} name;

/** get the stack stack pointer for a stack object "name" */
#define SYS_STACK_PTR(name) ((void*)(((addr_t)&(name)) + (sizeof((name).stack))))

/** get the base address of a stack object "name" */
#define SYS_STACK_BASE(name) ((void*)&(name))

/** get the size of a stack object "name" */
#define SYS_STACK_SIZE(name) sizeof((name).stack)

#endif
