/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * marron/rbtree.h
 *
 * Balanced binary search tree with parent pointers.
 *
 * The implementation uses a red-black tree. Logically, the tree is a min-tree,
 * with link[0] (min, prev, first) on the left side
 * and link[1] (max, next, last) on the right side.
 * We keep the color information in the nodes, i.e. in the parent pointer,
 * and do not use colored pointers to the children nodes.
 * Using parent pointers instead of implicitly tracking the tree structure
 * on the call stack allows to have nodes with duplicate keys.
 * Duplicate keys will be handled in order of insertion.
 *
 * Rules:
 * - the root is always black
 * - red nodes cannot have red children (no two consecutive red links)
 * - leaf nodes are always black
 * - all paths down to the leaves have the same number of black nodes
 *
 * Implicit:
 * - NULL pointers are always black
 * - a minimum node on the far left of a subtree has no left child
 *
 * Color Encoding:
 * - 0: black
 * - 1: red
 *
 * Direction:
 * - we derive a node's direction by checking the right link in the parent:
 *     dir = parent->link[1] == node;
 *   this yields the right array index, i.e. 0 or 1.
 * - we get the opposite direction via XOR: dir ^ 1
 *
 * For the references to special cases, see Chapter 13 on red-black trees
 * in "Introduction to Algorithms", 2nd edition, by Cormen, Leiserson,
 * Rivest, and Stein (CLSR).
 *
 *
 * NOTE: all code is provided in this header file, but the header file can
 * be instantiated in different ways.
 * For "header only" mode without any external dependencies, use:
 *   #define RBTREE_HEADER_ONLY
 *   #include <rbtree.h>
 * This provides all functions as static inliner functions.
 *
 * For normal usage, use:
 *   #include <rbtree.h>
 * This provides all functions except rbtree_insert_core()
 * and rbtree_remove_core() as static inline functions.
 * Then use once, in one module:
 *   #define RBTREE_HEADER_INSTANTIATE
 *   #include <rbtree.h>
 * This instantiates the two functions.
 *
 *
 * azuepke, 2014-03-23: initial
 * azuepke, 2021-08-27: adapted to use parent pointer
 * azuepke, 2021-09-02: keep color information in parent pointer
 * azuepke, 2023-02-12: clean naming of temp variables in macros
 */

#ifndef MARRON_RBTREE_H_
#define MARRON_RBTREE_H_

#include <stddef.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

/* enforce to generate the implementation in "header only" mode */
#ifdef RBTREE_HEADER_ONLY
#define RBTREE_HEADER_INLINE static inline
#define RBTREE_HEADER_INSTANTIATE
#else
#define RBTREE_HEADER_INLINE
#endif

/** colored pointer (bit 0 encodes the color) */
typedef unsigned long rbtree_link_t;

/** internal tree node type */
struct rbtree_node_struct {
	struct rbtree_node_struct *link[2];
	/* we encode the color of the node in bit 0 of the parent pointer */
	rbtree_link_t parent_color;
};

/** tree node */
typedef struct rbtree_node_struct rbtree_node_t;

/** tree root */
typedef struct {
	struct rbtree_node_struct *root_node;
} rbtree_root_t;

/** tree comparator, returns:
 * -1 if a < b
 *  0 if a == b
 * +1 if a > b
 */
typedef int (*rbtree_cmp_t)(const rbtree_node_t *a, const rbtree_node_t *b);

/** augmentation callback
 *
 * The augmentation callback is always invoked for structural changes
 * from bottom to top in the tree.
 *
 * The function's return value is used while propagating changes upwards
 * to the root of a tree.
 * A non-zero return value indicates that augmentation data was changed.
 * A zero return value indicates that augmentation data is unchanged,
 * and the proparation can stop. It is safe to always return a non-zero value.
 */
typedef int (*rbtree_augment_t)(rbtree_node_t *n);

/* internal macros */
#define RBTREE_NODE_PARENT(n)         ((rbtree_node_t *)((n)->parent_color & ~0x1ul))
#define RBTREE_NODE_COLOR(n)          ((n)->parent_color & 0x1ul)
#define RBTREE_NODE_NULL_OR_BLACK(n)  (((n) == NULL) || (RBTREE_NODE_COLOR(n) == 0))
#define RBTREE_MK_LINK(p, c)          ((unsigned long)(p) | (c))
#define RBTREE_MK_BLACK(n)            ((n)->parent_color &= ~0x1ul)
#define RBTREE_MK_RED(n)              ((n)->parent_color |= 0x1ul)

/* forward declaration */
RBTREE_HEADER_INLINE void rbtree_insert_core(rbtree_root_t *root, rbtree_node_t *node, rbtree_node_t *parent, int dir, rbtree_augment_t augment);
RBTREE_HEADER_INLINE rbtree_node_t *rbtree_remove_core(rbtree_root_t *root, rbtree_node_t *node, rbtree_augment_t augment);


/** static tree root initializer */
#define RBTREE_ROOT_INIT(root) { 0 }

/** init tree root */
static inline void rbtree_root_init(rbtree_root_t *root)
{
	assert(root != NULL);
	root->root_node = NULL;
}


/** init tree node */
static inline void rbtree_node_init(rbtree_node_t *node)
{
	assert(node != NULL);
	(void)node;
#ifndef NDEBUG
	node->link[0] = NULL;
	node->link[1] = NULL;
	node->parent_color = 0;
#endif
}

/** static tree node initializer */
#define RBTREE_NODE_INIT(node) { 0 }


/** check if a tree is empty */
static inline int rbtree_is_empty(const rbtree_root_t *root)
{
	assert(root != NULL);
	return root->root_node == NULL;
}

/** return minimum (left-most) node in subtree of node (internal function) */
static inline rbtree_node_t *rbtree_node_first(rbtree_node_t *node)
{
	assert(node != NULL);

	while (node->link[0] != NULL) {
		node = node->link[0];
	}
	return node;
}

/** return maximum (right-most) node in subtree of node (internal function) */
static inline rbtree_node_t *rbtree_node_last(rbtree_node_t *node)
{
	assert(node != NULL);

	while (node->link[1] != NULL) {
		node = node->link[1];
	}
	return node;
}

/** return parent of node */
static inline rbtree_node_t *rbtree_node_parent(const rbtree_node_t *node)
{
	assert(node != NULL);

	return RBTREE_NODE_PARENT(node);
}

/** return left/right child node of node */
static inline rbtree_node_t *rbtree_node_child(const rbtree_node_t *node, int dir)
{
	assert(node != NULL);
	assert((dir == 0) || (dir == 1));

	return node->link[dir];
}

/** return minimum node in tree (left-most node) */
static inline rbtree_node_t *rbtree_first(const rbtree_root_t *root)
{
	rbtree_node_t *node;

	assert(root != NULL);

	node = root->root_node;
	if (node != NULL) {
		node = rbtree_node_first(node);
	}
	return node;
}

/** return maximum node in tree (right-most node) */
static inline rbtree_node_t *rbtree_last(const rbtree_root_t *root)
{
	rbtree_node_t *node;

	assert(root != NULL);

	node = root->root_node;
	if (node != NULL) {
		node = rbtree_node_last(node);
	}
	return node;
}

/** return previous node of a node in tree (going in left/first direction) */
static inline rbtree_node_t *rbtree_prev(const rbtree_root_t *root, rbtree_node_t *node)
{
	assert(root != NULL);
	assert(node != NULL);

	(void)root;

	if (node->link[0] != NULL) {
		/* go down to left child's right-most child node */
		node = rbtree_node_last(node->link[0]);
	} else {
		/* go up to parent if we are the left child */
		while ((node->parent_color != 0) && (RBTREE_NODE_PARENT(node)->link[0] == node)) {
			node = RBTREE_NODE_PARENT(node);
		}
		/* go up to parent; we are the parent's right child */
		node = RBTREE_NODE_PARENT(node);
	}

	return node;
}

/** return next node of a node in tree (going in right/last direction) */
static inline rbtree_node_t *rbtree_next(const rbtree_root_t *root, rbtree_node_t *node)
{
	assert(root != NULL);
	assert(node != NULL);

	(void)root;

	if (node->link[1] != NULL) {
		/* go down to right child's left-most child node */
		node = rbtree_node_first(node->link[1]);
	} else {
		/* go up to parent if we are the right child */
		while ((node->parent_color != 0) && (RBTREE_NODE_PARENT(node)->link[1] == node)) {
			node = RBTREE_NODE_PARENT(node);
		}
		/* go up to parent; we are the parent's left child */
		node = RBTREE_NODE_PARENT(node);
	}

	return node;
}

/** find first (min) match where cmp(srch, iter) returns 0 in tree "root" */
static inline rbtree_node_t *rbtree_find_cmp(const rbtree_root_t *root, const rbtree_node_t *srch, rbtree_cmp_t cmp)
{
	rbtree_node_t *match;
	rbtree_node_t *iter;
	int dir;

	assert(root != NULL);
	assert(srch != NULL);
	assert(cmp != NULL);

	match = NULL;
	iter = root->root_node;
	while (iter != NULL) {
		dir = cmp(srch, iter);
		if (dir == 0) {
			match = iter;
		}
		/* go left (min) direction on equal match */
		dir = dir > 0;
		iter = iter->link[dir];
	}
	return match;
}

/** search node in ordered tree; uses three-val "cmp_expr" for navigation */
/* NOTE: __ITER__ is the internal iterator */
#define rbtree_find(root, dir_expr)	\
	({	\
		rbtree_root_t *tmp_root_ = (root);	\
		rbtree_node_t *tmp_match_ = NULL;	\
		rbtree_node_t *__ITER__ = tmp_root_->root_node;	\
		while (__ITER__ != NULL) {	\
			int tmp_dir_ = (dir_expr);	\
			if (tmp_dir_ == 0) {	\
				tmp_match_ = __ITER__;	\
			}	\
			tmp_dir_ = tmp_dir_ > 0;	\
			__ITER__ = __ITER__->link[tmp_dir_];	\
		}	\
		tmp_match_;	\
	})


/** update augmentation (internal function) */
static inline void rbtree_augment_update(rbtree_augment_t augment, rbtree_node_t *node)
{
	assert(node != NULL);

	if (augment != NULL) {
		(void)augment(node);
	}
}

/** update augmentation from "note" upwards until "stop" (internal function)
 *
 * The difference between rbtree_augment_update() and this function
 * is that for this function, "node" can be NULL and the propagation
 * may stop after one call to the augmentation function.
 */
static inline void rbtree_augment_propagate(rbtree_augment_t augment, rbtree_node_t *node, rbtree_node_t *stop)
{
	int cont;

	if (augment != NULL) {
		while (node != stop) {
			assert(node != NULL);
			cont = augment(node);
			if (!cont) {
				break;
			}
			node = RBTREE_NODE_PARENT(node);
		}
	}
}

/** propagate augmentation changes from "node" upwards to the root */
static inline void rbtree_update_augment(const rbtree_root_t *root, rbtree_node_t *node, rbtree_augment_t augment)
{
	assert(root != NULL);
	assert(node != NULL);

	(void)root;
	rbtree_augment_propagate(augment, node, NULL);
}


/** tree rotation (internal function)
 *
 * Example for rotation to the left (dir=0):
 * X is rotated to the left
 *
 *    X                       Y
 *  /   \                   /   \
 * A      Y      --->      X      C
 *      /   \            /   \
 *     B     C          A     B
 *
 */
static inline void rbtree_rotate(rbtree_root_t *root, rbtree_node_t *node_x, int dir, rbtree_augment_t augment)
{
	rbtree_node_t *node_y, *parent, *node_b;
	int gp_dir;

	assert(root != NULL);
	assert(node_x != NULL);
	assert((dir == 0) || (dir == 1));

	node_y = node_x->link[dir ^ 1];
	assert(node_y != NULL);

	parent = RBTREE_NODE_PARENT(node_x);
	if (parent != NULL) {
		gp_dir = parent->link[1] == node_x;
		assert(parent->link[gp_dir] == node_x);
		parent->link[gp_dir] = node_y;
	} else {
		root->root_node = node_y;
	}

	node_y->parent_color = RBTREE_MK_LINK(parent, RBTREE_NODE_COLOR(node_y));
	node_b = node_y->link[dir];
	node_y->link[dir] = node_x;

	node_x->parent_color = RBTREE_MK_LINK(node_y, RBTREE_NODE_COLOR(node_x));
	node_x->link[dir ^ 1] = node_b;

	if (node_b != NULL) {
		node_b->parent_color = RBTREE_MK_LINK(node_x, RBTREE_NODE_COLOR(node_b));
	}

	rbtree_augment_update(augment, node_x);
	rbtree_augment_update(augment, node_y);
}

#ifdef RBTREE_HEADER_INSTANTIATE
/** insertion of note as child of parent; fixup tree going up */
RBTREE_HEADER_INLINE void rbtree_insert_core(rbtree_root_t *root, rbtree_node_t *node, rbtree_node_t *parent, int dir, rbtree_augment_t augment)
{
	rbtree_node_t *gparent;
	rbtree_node_t *uncle;
	int parent_dir;

	assert(root != NULL);
	assert(node != NULL);

	if (parent != NULL) {
		parent->link[dir] = node;
	} else {
		root->root_node = node;
	}

	/* insert as red node */
	node->link[0] = NULL;
	node->link[1] = NULL;
	node->parent_color = RBTREE_MK_LINK(parent, 1);

	rbtree_augment_update(augment, node);
	rbtree_augment_propagate(augment, parent, NULL);

	/* upward pass to fix red violations */
	while ((node != root->root_node) && ((parent = RBTREE_NODE_PARENT(node)), (RBTREE_NODE_COLOR(parent) != 0))) {
		gparent = RBTREE_NODE_PARENT(parent);
		/* if parent is red, a grand parent must exist */
		assert(gparent != NULL);

		parent_dir = gparent->link[1] == parent;
		assert(gparent->link[parent_dir] == parent);
		uncle = gparent->link[parent_dir ^ 1];
		if ((uncle != NULL) && (RBTREE_NODE_COLOR(uncle) != 0)) {
			/* case 1:
			 * both parent and uncle are red:
			 * recolor them to black and push red upwards to grand parent
			 */
			RBTREE_MK_BLACK(uncle);
			RBTREE_MK_BLACK(parent);
			RBTREE_MK_RED(gparent);
			node = gparent;
			continue;
		} else {
			/* parent is red, uncle is black */
			dir = parent->link[1] == node;
			assert(parent->link[dir] == node);
			if (dir != parent_dir) {
				/* case 2:
				 * double rotation case -- swaps position of node and parent
				 */
				rbtree_node_t *tmp;
				tmp = node;
				node = parent;
				parent = tmp;
				rbtree_rotate(root, node, parent_dir, augment);
			}

			/* case 3 */
			RBTREE_MK_BLACK(parent);
			RBTREE_MK_RED(gparent);
			rbtree_rotate(root, gparent, parent_dir ^ 1, augment);
			/* we can stop here, as the new parent is black */
			break;
		}
	}

	/* fix possible red violation at the root */
	RBTREE_MK_BLACK(root->root_node);
}
#endif

/** insert "node" into tree; insert as last (max) on equal match; compares cmp(node, iter) */
static inline void rbtree_insert_cmp(rbtree_root_t *root, rbtree_node_t *node, rbtree_cmp_t cmp, rbtree_augment_t augment)
{
	rbtree_node_t *parent;
	rbtree_node_t *iter;
	int dir;

	assert(root != NULL);
	assert(node != NULL);
	assert(cmp != NULL);

	/* downward pass for insertion, as we insert as new leaf node */
	parent = NULL;
	dir = 0;
	iter = root->root_node;
	while (iter != NULL) {
		parent = iter;
		dir = cmp(node, iter);
		/* go right (max) direction on equal match */
		dir = dir >= 0;
		iter = iter->link[dir];
	}

	rbtree_insert_core(root, node, parent, dir, augment);
}

/** insert "node" into tree; uses three-val "cmp_expr" for navigation */
/* NOTE: __ITER__ is the internal iterator */
#define rbtree_insert(root, node, cmp_expr)	\
	do {	\
		rbtree_root_t *tmp_root_ = (root);	\
		rbtree_node_t *tmp_node_ = (node);	\
		rbtree_node_t *tmp_parent_ = NULL;	\
		int tmp_dir_ = 0;	\
		rbtree_node_t *__ITER__ = tmp_root_->root_node;	\
		while (__ITER__ != NULL) {	\
			tmp_parent_ = __ITER__;	\
			tmp_dir_ = (cmp_expr);	\
			tmp_dir_ = (tmp_dir_ >= 0);	\
			__ITER__ = __ITER__->link[tmp_dir_];	\
		}	\
		rbtree_insert_core(tmp_root_, tmp_node_, tmp_parent_, tmp_dir_, NULL);	\
	} while (0)


#ifdef RBTREE_HEADER_INSTANTIATE
/** remove node in tree; fixup tree going up */
RBTREE_HEADER_INLINE rbtree_node_t *rbtree_remove_core(rbtree_root_t *root, rbtree_node_t *node, rbtree_augment_t augment)
{
	rbtree_node_t *orig, *succ, *child, *parent, *sibling;
	long color;
	int dir;

	assert(root != NULL);
	assert(node != NULL);

	orig = node;

	if ((node->link[0] != NULL) && (node->link[1] != NULL)) {
		/* special case: inner node with two children */

		/* locate in-order successor */
		succ = rbtree_node_first(node->link[1]);

		/* the successor node might have a right child */
		assert(succ->link[0] == NULL);
		child = succ->link[1];
		parent = RBTREE_NODE_PARENT(succ);
		color = RBTREE_NODE_COLOR(succ);

		/* fixup pointers */
		assert(parent != NULL);
		dir = parent->link[1] == succ;
		assert(parent->link[dir] == succ);
		parent->link[dir] = child;

		if (child != NULL) {
			assert(RBTREE_NODE_PARENT(child) == succ);
			child->parent_color = RBTREE_MK_LINK(parent, RBTREE_NODE_COLOR(child));
		}

		rbtree_augment_propagate(augment, parent, node);

		/* corner case: start fixup one level below */
		if (RBTREE_NODE_PARENT(succ) == node) {
			parent = succ;
		}

		/* put successor in place of node (this also copies the color) */
		succ->link[0] = node->link[0];
		succ->link[1] = node->link[1];
		succ->parent_color = node->parent_color;

		if (RBTREE_NODE_PARENT(succ) != NULL) {
			dir = RBTREE_NODE_PARENT(succ)->link[1] == node;
			assert(RBTREE_NODE_PARENT(succ)->link[dir] == node);
			RBTREE_NODE_PARENT(succ)->link[dir] = succ;
		} else {
			assert(root->root_node == node);
			root->root_node = succ;
		}

		assert(RBTREE_NODE_PARENT(succ->link[0]) == node);
		succ->link[0]->parent_color = RBTREE_MK_LINK(succ, RBTREE_NODE_COLOR(succ->link[0]));
		if (succ->link[1] != NULL) {
			assert(RBTREE_NODE_PARENT(succ->link[1]) == node);
			succ->link[1]->parent_color = RBTREE_MK_LINK(succ, RBTREE_NODE_COLOR(succ->link[1]));
		}

		rbtree_augment_update(augment, succ);
		rbtree_augment_propagate(augment, RBTREE_NODE_PARENT(succ), NULL);
	} else {
		/* only one child */
		assert((node->link[0] == NULL) || (node->link[1] == NULL));

		child = node->link[0];
		if (child == NULL) {
			child = node->link[1];
		}

		/* move up child to replace us at parent */
		parent = RBTREE_NODE_PARENT(node);
		if (parent != NULL) {
			dir = parent->link[1] == node;
			assert(parent->link[dir] == node);
			parent->link[dir] = child;
		} else {
			root->root_node = child;
		}
		if (child != NULL) {
			assert(RBTREE_NODE_PARENT(child) == node);
			child->parent_color = RBTREE_MK_LINK(parent, RBTREE_NODE_COLOR(child));
		}

		color = RBTREE_NODE_COLOR(node);

		rbtree_augment_propagate(augment, parent, NULL);
	}

#ifndef NDEBUG
	/* clear original node */
	node->link[0] = 0;
	node->link[1] = 0;
	node->parent_color = 0;
#endif

	/* upward pass to fix double black violations */
	if (color == 0) {
		/* NOTE: node might be NULL, we actually work on the parent only */
		node = child;
		while ((node != root->root_node) && RBTREE_NODE_NULL_OR_BLACK(node)) {
			dir = parent->link[1] == node;
			assert(parent->link[dir] == node);
			sibling = parent->link[dir ^ 1];
			assert(sibling != NULL);

			if (RBTREE_NODE_COLOR(sibling) != 0) {
				/* case 1 */
				RBTREE_MK_BLACK(sibling);
				RBTREE_MK_RED(parent);
				rbtree_rotate(root, parent, dir, augment);
				sibling = parent->link[dir ^ 1];
			}

			if (RBTREE_NODE_NULL_OR_BLACK(sibling->link[0]) &&
			        RBTREE_NODE_NULL_OR_BLACK(sibling->link[1])) {
				/* case 2 */
				/* both children of sibling are black -> make sibling red */
				RBTREE_MK_RED(sibling);
				node = parent;
				parent = RBTREE_NODE_PARENT(node);
			} else {
				if (RBTREE_NODE_NULL_OR_BLACK(sibling->link[dir ^ 1])) {
					/* case 3 */
					child = sibling->link[dir];
					if (child != NULL) {
						RBTREE_MK_BLACK(child);
					}
					RBTREE_MK_RED(sibling);
					rbtree_rotate(root, sibling, dir ^ 1, augment);
					sibling = parent->link[dir ^ 1];
				}

				/* case 4 */
				sibling->parent_color = RBTREE_MK_LINK(RBTREE_NODE_PARENT(sibling), RBTREE_NODE_COLOR(parent));
				RBTREE_MK_BLACK(parent);
				child = sibling->link[dir ^ 1];
				if (child != NULL) {
					RBTREE_MK_BLACK(child);
				}
				rbtree_rotate(root, parent, dir, augment);

				/* we can stop, the double black violation is solved */
				node = root->root_node;
				break;
			}
		}
		if (node != NULL) {
			RBTREE_MK_BLACK(node);
		}
	}

	return orig;
}
#endif

/** remove node in tree */
static inline rbtree_node_t *rbtree_remove(rbtree_root_t *root, rbtree_node_t *node)
{
    return rbtree_remove_core(root, node, NULL);
}

/** return and remove the miminum node in tree (left side) */
static inline rbtree_node_t *rbtree_remove_first(rbtree_root_t *root)
{
	rbtree_node_t *node;

	assert(root != NULL);

	node = rbtree_first(root);
	if (node != NULL) {
		return rbtree_remove(root, node);
	}
	return node;
}

/** return and remove the maximum node in tree (right side) */
static inline rbtree_node_t *rbtree_remove_last(rbtree_root_t *root)
{
	rbtree_node_t *node;

	assert(root != NULL);

	node = rbtree_last(root);
	if (node != NULL) {
		return rbtree_remove(root, node);
	}
	return node;
}


/** simple tree iterator -- do not delete tree nodes while iterating! */
#define rbtree_for_each(root, iter)	\
	for ((iter) = rbtree_first(root); (iter) != NULL; (iter) = rbtree_next((root), (iter)))

/** robust tree iterator */
#define rbtree_for_each_safe(root, iter, tmp)	\
	for ((iter) = rbtree_first(root);	\
		 ((iter) != NULL) && ((tmp) = rbtree_next((root), (iter)), 1);	\
		 (iter) = (tmp))

/** drain tree */
#define rbtree_for_each_remove_first(root, min)	\
	while (((min) = rbtree_remove_first(root)) != NULL)


/** magic cast to get surrounding data structure where node is embedded in */
#define rbtree_entry(ptr, type, member)	\
	((type *)((char *)(ptr) - offsetof(type, member)))

#ifdef __cplusplus
}
#endif

#endif
