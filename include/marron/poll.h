/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024 Alexander Zuepke */
/*
 * marron/poll.h
 *
 * Linux-conforming poll() flags.
 *
 * azuepke, 2022-08-12: initial
 * azuepke, 2024-12-08: encoding as in Linux
 */

#ifndef MARRON_POLL_H_
#define MARRON_POLL_H_

/* poll() event flags */
/* NOTE: definition follows Linux and is the same as in epoll.h.
 * Only POLLIN, POLLPRI, POLLOUT, POLLERR, POLLHUP and POLLRDHUP carry meaning.
 * POSIX defines POLLRDNORM, POLLRDBAND, POLLWRNORM, POLLWRBAND and POLLMSG
 * for the obsolete STREAM interface, therefore the semantics of normal,
 * priority and high-priority is driver specific if used at all.
 * POLLNVAL is an output flag and signals error conditions in poll() only.
 *
 * The relation to select() is as follows:
 * - POLLIN_SET		(POLLIN | POLLRDNORM | POLLRDBAND | POLLHUP | POLLERR)
 * - POLLOUT_SET	(POLLOUT | POLLWRNORM | POLLWRBAND | POLLERR)
 * - POLLEX_SET		(POLLPRI)
 */
#define POLLIN		0x0001	/**< data available for reading */
#define POLLPRI		0x0002	/**< high priority data available for reading */
#define POLLOUT		0x0004	/**< data available may be written */
#define POLLERR		0x0008	/**< error occurred */
#define POLLHUP		0x0010	/**< device disconnected */
#define POLLNVAL	0x0020	/**< invalid file descriptor */
#define POLLRDNORM	0x0040	/**< normal data available for reading (obsolete) */
#define POLLRDBAND	0x0080	/**< priority data available for reading (obsolete) */
#define POLLWRNORM	0x0100	/**< normal data may be written (obsolete) */
#define POLLWRBAND	0x0200	/**< priority data may be written (obsolete) */
#define POLLMSG		0x0400	/**< defined but not used (obsolete) */
#define POLLRDHUP	0x2000	/**< socket connection closed by peer (Linux) */

#endif
