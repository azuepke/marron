/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2018, 2021 Alexander Zuepke */
/*
 * marron/error.h
 *
 * POSIX-conforming error codes.
 *
 * azuepke, 2013-05-09: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-01-04: cleaned up for API documentation
 * azuepke, 2018-12-21: standalone version
 * azuepke, 2021-07-19: new error codes for POSIX
 * azuepke, 2021-11-11: split into error.h and errno.h
 */

#ifndef MARRON_ERRNO_H_
#define MARRON_ERRNO_H_

/* error codes */
/*		EOK				0	//   no error */
#define ENOSYS			1	/**< function not implemented */
#define EINVAL			2	/**< invalid argument */
/*		ELIMIT			3	//   value outside configured limits */
#define EFAULT			4	/**< bad address */
#define ETIMEDOUT		5	/**< operation timed out */
#define EAGAIN			6	/**< resource unavailable, try again */
#define EBUSY			7	/**< device or resource busy */
/*		ESTATE			8	//   resource in wrong state */
/*		ETYPE			9	//   resource has wrong type */
#define EPERM			10	/**< operation not permitted */
#define ECANCELED		11	/**< operation canceled */
#define ENOMEM			12	/**< out of memory */
#define EINTR			13	/**< function interrupted */
#define ENOENT			14	/**< no such file or directory */
#define ESRCH			15	/**< no such process */
#define ENOTSUP			16	/**< not supported */
#define EOVERFLOW		17	/**< value too large for defined data type */
#define EDEADLK			18	/**< deadlock detected */
#define EACCES			19	/**< permission denied */
#define EEXIST			20	/**< resource exists */
#define EBADF			21	/**< bad file descriptor */
#define ENODEV			22	/**< no such device */
#define EMSGSIZE		23	/**< message too long */
#define EIO				24	/**< I/O error */
#define ENOTCONN		25	/**< socket not connected */
#define EDOM			26	/**< numerical argument out of domain */
#define ERANGE			27	/**< result too large */
#define EILSEQ			28	/**< illegal byte sequence */
#define EMFILE			29	/**< per-process descriptor table full */
#define ENFILE			30	/**< system file table full */
#define EPIPE			31	/**< broken pipe */
#define ENOTTY			32	/**< not a tty */
#define ELOOP			33	/**< loop detected */
#define ENOTDIR			34	/**< not a directory */
#define EISDIR			35	/**< is a directory */
#define EFBIG			36	/**< file too large */
#define ENOSPC			37	/**< no space left on device */
#define EROFS			38	/**< read-only file system */
#define ENXIO			39	/**< no such device or address */
#define ETXTBSY			40	/**< text file busy */
#define ENOBUFS			41	/**< no buffer space available */
#define ENAMETOOLONG	42	/**< name too long */
#define ESPIPE			43	/**< invalid seek */
#define ENOTSOCK		44	/**< not a socket */
#define EAFNOSUPPORT	45	/**< address family not supported */
#define EPROTONOSUPPORT	46	/**< protocol not supported */
#define EPROTOTYPE		47	/**< wrong protocol type */
#define EADDRNOTAVAIL	48	/**< address not available */
#define EADDRINUSE		49	/**< address in use */
#define ECONNREFUSED	50	/**< connection refused */
#define EINPROGRESS		51	/**< operation in progress */
#define ECONNABORTED	52	/**< connection aborted */
#define EDESTADDRREQ	53	/**< destination address required */
#define EISCONN			54	/**< already connected */
#define ECONNRESET		55	/**< connection reset */
#define ENOPROTOOPT		56	/**< option not supported by protocol */
#define ENETDOWN		57	/**< network interface down */
#define ENETUNREACH		58	/**< network unreachable */
#define EHOSTUNREACH	59	/**< host unreachable */
#define EPROTO			60	/**< protocol error */
#define E2BIG			61	/**< argument list too long */
#define EALREADY		62	/**< connection already in progress */
#define EBADMSG			63	/**< bad message */
#define ECHILD			64	/**<  no child process */
#define EXDEV			65	/**< cross-device link */
#define EMLINK			66	/**< too many links */
#define EDQUOT			67	/**< quota exhausted */
#define EIDRM			68	/**< identifier removed */
#define ENETRESET		69	/**< connection reset by network */
#define ENOTEMPTY		70	/**< directory not empty */
#define ENOTRECOVERABLE	71	/**< state not recoverable */
#define EOWNERDEAD		72	/**< previous owner died */
#define EMULTIHOP		73	/**< multihop attempted */
#define ENOEXEC			74	/**< executable format error */
#define ENOLCK			75	/**< no locks available */
#define ENOLINK			76	/**< link has been severed */
#define ENOMSG			77	/**< no message of desired type */
#define ESTALE			78	/**< stale file handle */
#define ENODATA			79	/**< no data available */
#define ENOSR			80	/**< out of stream resources */
#define ENOSTR			81	/**< not a stream */
#define ETIME			82	/**< device timeout */

/* aliases */
#define EWOULDBLOCK		EAGAIN
#define EOPNOTSUPP		ENOTSUP
#define EDEADLOCK       EDEADLK

#endif
