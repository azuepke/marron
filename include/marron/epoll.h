/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024 Alexander Zuepke */
/*
 * marron/epoll.h
 *
 * Linux-conforming epoll operations and flags.
 *
 * azuepke, 2024-12-08: from poll.h
 */

#ifndef MARRON_EPOLL_H_
#define MARRON_EPOLL_H_

/* epoll operations in epoll_ctl() */
#define EPOLL_CTL_ADD	1
#define EPOLL_CTL_DEL	2
#define EPOLL_CTL_MOD	3

/* epoll event flags */
/* NOTE: definition follows Linux and is the same as in poll.h.
 * Only POLLIN, POLLPRI, POLLOUT, POLLERR, POLLHUP and POLLRDHUP carry meaning.
 * POSIX defines POLLRDNORM, POLLRDBAND, POLLWRNORM, POLLWRBAND and POLLMSG
 * for the obsolete STREAM interface, therefore the semantics of normal,
 * priority and high-priority is driver specific if used at all.
 * POLLNVAL is an output flag and signals error conditions in poll() only.
 *
 * The relation to select() is as follows:
 * - POLLIN_SET		(POLLIN | POLLRDNORM | POLLRDBAND | POLLHUP | POLLERR)
 * - POLLOUT_SET	(POLLOUT | POLLWRNORM | POLLWRBAND | POLLERR)
 * - POLLEX_SET		(POLLPRI)
 *
 * Higher bits are epoll-specific and change the behavior of event notification.
 * EPOLLWAKEUP is always ignored for Linux compatibility.
 */
#define EPOLLIN		0x0001	/**< data available for reading */
#define EPOLLPRI	0x0002	/**< high priority data available for reading */
#define EPOLLOUT	0x0004	/**< data available may be written */
#define EPOLLERR	0x0008	/**< error occurred */
#define EPOLLHUP	0x0010	/**< device disconnected */
#define EPOLLNVAL	0x0020	/**< invalid file descriptor */
#define EPOLLRDNORM	0x0040	/**< normal data available for reading (obsolete) */
#define EPOLLRDBAND	0x0080	/**< priority data available for reading (obsolete) */
#define EPOLLWRNORM	0x0100	/**< normal data may be written (obsolete) */
#define EPOLLWRBAND	0x0200	/**< priority data may be written (obsolete) */
#define EPOLLMSG	0x0400	/**< defined but not used (obsolete) */
#define EPOLLRDHUP	0x2000	/**< socket connection closed by peer (Linux) */

#define EPOLLEXCLUSIVE	(1U<<28)	/**< exclusive wakeup mode */
#define EPOLLWAKEUP		(1U<<29)	/**< do not suspend system while pending (ignored) */
#define EPOLLONESHOT	(1U<<30)	/**< oneshot mode */
#define EPOLLET			(1U<<31)	/**< event-triggered mode */

#endif
