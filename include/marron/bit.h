/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * marron/bit.h
 *
 * Operations on bits.
 *
 * This file provides "find highest bit set" and "find lowest bit set"
 * operations for 32-bit and 64-bit integers.
 *
 * Important note: Unlike POSIX ffs(), the lowest bit is 0.
 * Also, the functions not robust if the value is zero.
 *
 * This code uses GCC atomic builtins and may not be portable.
 *
 * azuepke, 2021-01-21: rework using GCC builtins
 */

#ifndef MARRON_BIT_H_
#define MARRON_BIT_H_

#ifdef __cplusplus
extern "C" {
#endif

/** find highest set bit -- 32-bit integer */
static inline unsigned int bit_fhs(unsigned int val)
{
	return (unsigned int)((sizeof(val) * 8) - 1) - (unsigned int)__builtin_clz(val);
}

/** find highest set bit -- long integer */
static inline unsigned int bit_fhsl(unsigned long val)
{
	return (unsigned int)((sizeof(val) * 8) - 1) - (unsigned int)__builtin_clzl(val);
}

/** find highest set bit -- 64-bit integer */
static inline unsigned int bit_fhs64(unsigned long long val)
{
	return (unsigned int)((sizeof(val) * 8) - 1) - (unsigned int)__builtin_clzll(val);
}


/** find lowest set bit -- 32-bit variant */
static inline unsigned int bit_fls(unsigned int val)
{
	return (unsigned int)((sizeof(val) * 8) - 1) - (unsigned int)__builtin_clz(val & -val);
}

/** find lowest set bit -- long integer */
static inline unsigned int bit_flsl(unsigned long val)
{
	return (unsigned int)((sizeof(val) * 8) - 1) - (unsigned int)__builtin_clzl(val & -val);
}

/** find lowest set bit -- 64-bit integer */
static inline unsigned int bit_fls64(unsigned long long val)
{
	return (unsigned int)((sizeof(val) * 8) - 1) - (unsigned int)__builtin_clzll(val & -val);
}

#ifdef __cplusplus
}
#endif

#endif
