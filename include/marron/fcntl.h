/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * marron/fcntl.h
 *
 * POSIX-conforming file flags and modes.
 *
 * NOTE: the encoding of the following flags does not necessarily follow
 * the convention of other operating systems!
 *
 * azuepke, 2022-07-17: initial
 */

#ifndef MARRON_FCNTL_H_
#define MARRON_FCNTL_H_

/* file descriptor flags for open() */
/* NOTE: O_RDONLY, O_WRONLY, and O_RDWR encode as 1/2/3, not 0/1/2 */
#define __O_READ	0x0001	/**< readable (internal) */
#define __O_WRITE	0x0002	/**< writable (internal) */
#define O_RDONLY	0x0001	/**< read-only */
#define O_WRONLY	0x0002	/**< write-only */
#define O_RDWR		0x0003	/**< read/write */
#define O_PATH		0x0004	/**< path traversal */

#define O_ACCMODE	0x0007	/**< mask for file access mode */

#define O_DIRECTORY	0x0008	/**< directory type */

#define O_CREAT		0x0010	/**< create file if not exist */
#define O_EXCL		0x0020	/**< exclusive use flag */
#define O_TRUNC		0x0040	/**< truncate flag */
#define O_APPEND	0x0080	/**< append mode */

#define __O_SEEK	0x0100	/**< seekable (internal) */
#define O_CLOEXEC	0x0100	/**< close on exec */
#define O_NOCTTY	0x0200	/**< do not assign controlling terminal */
#define O_NOFOLLOW	0x0400	/**< do not follow symbolic links */
#define O_NONBLOCK	0x0800	/**< non-blocking mode */

#define O_SYNC		0x1000	/**< file integrity */
#define O_ASYNC		0x2000	/**< asynchronous, enable SIGIO, ignored in open() */

#define O_LARGEFILE	0		/**< large file support (always implicit) */
#define O_TTY_INIT	0		/**< initialize tty (not supported) */
#define O_NDELAY	O_NONBLOCK
#define O_DSYNC		O_SYNC	/**< data integrity on write */
#define O_RSYNC		O_SYNC	/**< data integrity on read */
#define O_EXEC		O_PATH	/**< open file for execute */
#define O_SEARCH	O_PATH	/**< open directory for search */

/* whence mode for lseek() (also defined in stdio.h and unistd.h) */
#ifndef SEEK_SET
#define SEEK_SET	0		/**< seek from start of file */
#endif
#ifndef SEEK_CUR
#define SEEK_CUR	1		/**< seek from current position */
#endif
#ifndef SEEK_END
#define SEEK_END	2		/**< seek from end of file */
#endif

/* xxxat() flags */
#define AT_EACCESS			0x01
#define AT_SYMLINK_NOFOLLOW	0x02
#define AT_SYMLINK_FOLLOW	0x04
#define AT_REMOVEDIR		0x08

#endif
