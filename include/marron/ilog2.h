/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * marron/ilog2.h
 *
 * integer logarithm to the base 2
 *
 * azuepke, 2023-02-12: initial
 */

#ifndef MARRON_ILOG2_H_
#define MARRON_ILOG2_H_

#ifdef __cplusplus
extern "C" {
#endif

/** ilog2() -- integer logarithm to the base 2
 *
 * ilog2(0) = undefined result
 * ilog2(1) = 0
 * ilog2(2) = 1
 * ilog2(3) = 1
 * ilog2(4) = 2
 * ilog2(5) = 2
 * ilog2(6) = 2
 * ilog2(7) = 2
 * ilog2(8) = 3
 * ...
 */
static inline unsigned int ilog2(unsigned long val)
{
	unsigned int max_bits = (sizeof(val) * 8) - 1;
	unsigned int l = max_bits - __builtin_clzl(val);

	if (l > max_bits) {
		/* the __builtin_unreachable() helps GCC's value analysis
		 * to bound the output range
		 */
		__builtin_unreachable();
	}

	return l;
}

#ifdef __cplusplus
}
#endif

#endif
