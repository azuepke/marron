/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * marron/ipc_io.h
 *
 * IPC-based I/O.
 *
 * NOTE: This API is private to servers, clients, and the kernel
 * when using the IPC-based I/O protocol.
 * Servers linked into the kernel only implement a subset of the protocol,
 * e.g. for interrupt handling.
 *
 * We use the same protocol for both file system and file operations.
 * File system operations such as open(), chown() or mkdir() operate
 * on the mount point fd, while operations such as read(), ioctl() or mmap()
 * use specific files that need to be opened first. We use "fs" as identifier
 * for file system operations and "file" for file operations.
 *
 * azuepke, 2022-07-17: initial
 * azuepke, 2022-08-15: fairly complete
 * azuepke, 2025-02-09: file system services
 */

#ifndef MARRON_IPC_IO_H_
#define MARRON_IPC_IO_H_

/* open request types */
#define IPC_IO_FS_OPEN_FILE			1
#define IPC_IO_FS_OPEN_PIPE			2
#define IPC_IO_FS_OPEN_SOCKET		3
#define IPC_IO_FS_OPEN_SOCKETPAIR	4

////////////////////////////////////////////////////////////////////////////////

/* generic operations on all file descriptors */

#define IPC_IO_GETOFLAGS		0	/**< fcntl(F_GETFL) */
// int __libc_getoflags(int fd);
//
// retrieve the current open flags
//
// IN:
// OUT:
// - h32: open flags, for fcntl(F_GETFL), uint32_t

#define IPC_IO_SETOFLAGS		1	/**< fcntl(F_SETFL) */
// int __libc_setoflags(int fd, int flags);
//
// set the following open flags O_APPEND, O_NONBLOCK, O_SYNC, O_ASYNC
//
// IN:
// - h32: open flags, for fcntl(F_SETFL), uint32_t
// OUT:
// - h32: open flags, for fcntl(F_GETFL), uint32_t

////////////////////////////////////////////////////////////////////////////////

/* generic file API operations -- basic I/O */

#define IPC_IO_FS_OPEN			2	/**< open file/directory or create socket/pipe */
// int open(const char *pathname, int flags);
// int open(const char *pathname, int flags, mode_t mode);
//
// int socket(int domain, int type, int protocol);
// int socketpair(int domain, int type, int protocol, int sv[2]);
// int pipe(int pipefd[2]);
// int pipe2(int pipefd[2], int flags);
//
// open file or directly with fully resolved VFS name, or create socket or pipe
//
// for open, the VFS name comprises:
// - the absolute name of the device or mount point, and
// - the relative name of the file to the start of the mount point
//   (which is the absolute name in the namespace provided by the server)
// for sockets, no names are needed (unnamed file descriptors)
//
// depending on the IPC protocol ID in h8, open reacts differently:
// IPC protocol:
// - 1: file: open a specific file
// - 2: pipe: create a pipe pair
// - 3: socket
// - 4: socketpair: create a socketpair
//
// IN:
// - ipc.flag:  SYS_IPC_FD0 resp. SYS_IPC_FD1 for pipe() and socketpair()
// - h8: protocol, 1=file, 2=pipe, 3=socket, 4=sockpair
// - h32: flags: O_NONBLOCK and O_CLOEXEC are always supported
//               dito SOCK_NONBLOCK and SOCK_CLOEXEC for sockets
//               SOCK_NONBLOCK becomes O_NONBLOCK
//               sockets and pipe will get O_RDWR
// - ipc.flags: O_CLOEXEC and SOCK_CLOEXEC map to the SYS_IPC_FD_CLOEXEC flag
//
// for files:
// - h64l: mode
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
//
// for pipe
// - <none>
//
// for sockets / socketpair
// - h64l: domain
// - h64h: type
// - a64l: protocol
//
// OUT:
// - fd0, fd1: return file descriptors (up to two for socketpair() and pipe())
//
// int creat(const char *pathname, mode_t mode);
// - via open()
// int openat(int dirfd, const char *pathname, int flags);
// int openat(int dirfd, const char *pathname, int flags, mode_t mode);
// - via __getpath() + open()

#define IPC_IO_STAT			3	/**< fstat() */
// int fstat(int fd, struct stat *statbuf);
//
// stat file descriptor (even supported for sockets!)
//
// IN:
// - buf0: statbuf
// - size0: sizeof(struct stat)
// OUT:

#define IPC_IO_READ			4	/**< read(), pread(), readv(), preadv() */
#define IPC_IO_WRITE		5	/**< write(), pwrite(), writev(), pwritev() */
// ssize_t read(int fd, void *buf, size_t count);
// ssize_t write(int fd, const void *buf, size_t count);
// ssize_t pread(int fd, void *buf, size_t count, off_t offset);
// ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset);
// ssize_t readv(int fd, const struct iovec *iov, int iovcnt);
// ssize_t writev(int fd, const struct iovec *iov, int iovcnt);
// ssize_t preadv(int fd, const struct iovec *iov, int iovcnt, off_t offset);
// ssize_t pwritev(int fd, const struct iovec *iov, int iovcnt, off_t offset);
//
// read or write from file descriptor; the IPC API handles the I/O vectors
//
// IN:
// - h8: SEEK_SET for "p" variants, SEEK_CUR for non-"p" variants
// - h32: 0 (future flags field)
// - h64: offset or 0 for non-"p" variants
// - buf0/size0: buffer or vector in IPC message
// OUT:
// - size0: size read or written

#define IPC_IO_IOCTL		6	/**< ioctl() */
// int ioctl(int fd, int request, char *argp);
//
// multiplexer system call:
// - request encoding NONE/IN/OUT/INOUT semantics and a size
// - argp refers to a memory buffer
//
// IN:
// - h32: request
// - arg0: integer data value
// - buf0/size0: buffer to data
// OUT:
// - h32: retval

////////////////////////////////////////////////////////////////////////////////

/* generic file API operations -- seekable and memory-mapped files */

#define IPC_IO_MMAP			7	/**< mmap() */
// void *mmap(void *addr, size_t len, int prot, int flags, int fd, off_t offset);
//
// Map memory request
//
// The server receives the request and, with the help of sys_ipc_sharemap(),
// creates a mapping in the address space of the IPC caller.
//
// IN:
// - ipc.flags: SYS_IPC_MAP is set
// - buf0: addr
// - size0: len
// - h8: prot
// - h32: flags
// - h64: offset
// OUT:
// - ipc.map_start: effective address (retval)

#define IPC_IO_MPROTECT		8	/**< mprotect() */
// int mprotect(void *addr, size_t len, int prot); // FIXME
// int __kern_mprotect(int fd, off_t offset, size_t len, int prot); // FIXME
//
// Change memory protection
//
// The kernel invokes this IPC on behalf of the caller of sys_vm_protect()
// to notify the server that a previous mapping shall be changed.
// The kernel invokes this with the same parameters as a mapping IPC.
// The server installs a new mapping with the help of sys_ipc_sharemap().
//
// IN:
// - ipc.flags: SYS_IPC_MAP and SYS_IPC_KERNEL are set
// - buf0: addr (already final target address)
// - size0: len
// - h8: prot (new protection)
// - h32: flags (MAP_FIXED implicit)
// - h64: offset
// OUT:
// - ipc.map_start: ignored

#define IPC_IO_MUNMAP		9	/**< munmap() */
// int munmap(void *addr, size_t len); // FIXME
// int __kern_munmap(int fd, off_t offset, size_t len); // FIXME
//
// Unmap memory notification
//
// The kernel invokes this IPC on behalf of the caller of sys_vm_unmap()
// to notify the server that a previous mapping was unmapped.
//
// IN:
// - ipc.flags: SYS_IPC_KERNEL is set
// - buf0: addr (previous address of mapping)
// - size0: len
// - h8: zero
// - h32: zero
// - h64: offset
// OUT:
// - ipc.map_start: ignored

#define IPC_IO_SEEK			10	/**< lseek() */
// off_t lseek(int fd, off_t offset, int whence);
//
// IN:
// - h8: whence
// - h64: offset
// OUT:
// - h64: offset

#define IPC_IO_TRUNCATE		11	/**< ftruncate() */
// int ftruncate(int fd, off_t len);
//
// IN:
// - a64: len
// OUT:
//
// int truncate(const char *path, off_t len);
// - via open() + ftruncate()

#define IPC_IO_ALLOCATE		12	/**< fallocate() */
// int posix_fallocate(int fd, off_t offset, off_t len);
//
// IN:
// - h64: offset
// - a64: len
// OUT:

#define IPC_IO_SYNC			13	/**< fsync(), fdatasync(), msync() */
// int fsync(int fd);
// int fdatasync(int fd);
// int msync(void *addr, size_t len, int flags); // FIXME
// int __kern_msync(int fd, off_t offset, size_t len); // FIXME
//
// Synchronize to disk
//
// Offset defines the start; a length of zero means "to the end of the file"
// NOTE: fsync() and fdatasync() map to the same implementation
//
// IN:
// - h64: offset (=0 for fsync/fdatasync)
// - a64: len (=0 for fsync/fdatasync)
// OUT:

#define IPC_IO_ADVISE		14	/**< posix_fadvise(), posix_madvise() */
// int posix_fadvise(int fd, off_t offset, off_t len, int advice);
// int posix_madvise(void *addr, size_t len, int advice);  // FIXME
// int __kern_madvise(int fd, off_t offset, size_t len, int advice); // FIXME
//
// Handle the prefetch/writeback behavior of a buffer cache in a server
//
// Offset defines the start; a length of zero means "to the end of the file"
//
// IN:
// - h32: advice
// - h64: offset (=0 for fsync/fdatasync)
// - a64: len (=0 for fsync/fdatasync)
// OUT:

#define IPC_IO_LOCK			15	/**< flock(), advisory file locking */
// int flock(int fd, int operation);
// fcntl(F_OFD_SETLK) and struct flock
//
// Lock operations:
// - 0: rdlock     (F_RDLCK)
// - 1: wrlock     (F_RDLCK)
// - 2: unlock     (F_UNLCK)
// - 4: rdtrylock  (F_RDLCK | __F_TRYLCK)
// - 5: wrtrylock  (F_WRLCK | __F_TRYLCK)
// - 8: rdtestlock (F_RDLCK | __F_TSTLCK)
// - 9: wrtestlock (F_WRLCK | __F_TSTLCK)
//
// Advisory file locking (with sane semantics w.r.t. close() like in flock())
//
// Offset defines the start; a length of zero means "to the end of the file"
//
// IN:
// - h8: whence (=0 for flock)
// - h32: operation
// - h64: offset (=0 for flock)
// - a64: len (=0 for flock)
// OUT:

////////////////////////////////////////////////////////////////////////////////

/* FS (file system layer) API operations -- FS iteration */

#define IPC_IO_STATVFS		16	/**< fstatvfs() */
// int fstatvfs(int fd, struct statvfs *buf);
//
// ASSUME: the referenced file descriptor is any file from a mounted file system
//
// Retrieve file system information
//
// IN:
// - buf0: statbuf
// - size0: sizeof(struct stat)
// OUT:
//
// int statvfs(const char *path, struct statvfs *buf);
// - via open(O_PATH) + fstatvfs()

#define IPC_IO_GETPATH		17	/**< __getpath() */
// ssize_t __getpath(int fd, void *pathname, size_t size);
//
// ASSUME: the referenced file descriptor is a directory
//
// retrieve the pathname of a directory by its file descriptor (private extension)
// NOTE: this syscall is needed to implement other *at() system calls properly!
//
// IN:
// - buf0: addr
// - size0: size
// OUT:
// - size0: length
//
// fcntl(F_GETPATH) // similar BSD extension, but not fully compatible

#define IPC_IO_GETDENTS		18	/**< getdents() */
// ssize_t getdents(int fd, void *buf, size_t size); // BSD and Linux
//
// ASSUME: the referenced file descriptor is a directory
//
// retrieve directory entries, a read() for directories
//
// IN:
// - buf0: addr
// - size0: size
// OUT:
// - size0: actual size read
//
// struct dirent *readdir(DIR *dirp);
// - via getdents()

#define IPC_IO_FS_ACCESS	19	/**< access(), faccessat(), __libc_access() */
// int __libc_access(const char *pathname, int mode, int flags);
// int access(const char *pathname, int mode);
//
// check file access and existence
//
// IN:
// - h32: flags
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// OUT:
//
// int faccessat(int dirfd, const char *pathname, int mode, int flags);
// - via __getpath() + __libc_access()

#define IPC_IO_FS_STAT		20	/**< stat(), lstat(), fstatat(), __libc_stat() */
// int __libc_stat(const char *pathname, struct stat *statbuf, int flags);
// int stat(const char *pathname, struct stat *statbuf);
// int lstat(const char *pathname, struct stat *statbuf);
// - via __libc_stat()
// - lstat: O_NOFOLLOW
//
// stat file name
//
// IN:
// - h32: flags
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// - buf2: statbuf
// - size2: sizeof(struct stat)
// OUT:
//
// int fstatat(int dirfd, const char *pathname, struct stat *statbuf, int flags);
// - via __getpath() + __libc_stat()


////////////////////////////////////////////////////////////////////////////////

/* FS (file system layer) API operations -- FS modification */

#define IPC_IO_FS_RENAME	21	/**< rename(), renameat() */
// int rename(const char *oldpath, const char *newpath);
//
// Rename directory entries
//
// IN:
// - h32: flags (must be zero)
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: oldpath
// - size1: strlen(oldpath)
// - buf2: newpath
// - size2: strlen(newpath)
// OUT:
//
// int renameat(int olddirfd, const char *oldpath, int newdirfd, const char *newpath);
// - via __getpath + rename()

#define IPC_IO_FS_REMOVE	22	/**< unlink(), rmdir(), __libc_remove() */
// int __libc_remove(const char *pathname, uint32_t flags);
// int unlink(const char *pathname);
// int rmdir(const char *pathname);
//
// Remove directory entries
//
// IN:
// - h32: flags (O_DIRECTORY for directory or AT_REMOVEDIR?) // FIXME
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// OUT:
//
// int remove(const char *pathname);
// int unlinkat(int dirfd, const char *pathname, int flags);
// - via __getpath + __libc_remove()

#define IPC_IO_FS_MKDIR		23	/**< mkdir() */
// int mkdir(const char *pathname, mode_t mode);
//
// Create directory
//
// IN:
// - h32: mode
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// OUT:
//
// int mkdirat(int dirfd, const char *pathname, mode_t mode);
// - via __getpath + mkdir()

#define IPC_IO_FS_MKNOD		24	/**< mknod(), mkfifo() */
// int mknod(const char *pathname, mode_t mode, dev_t dev);
//
// Create device node or FIFO
//
// IN:
// - h32: mode
// - h64: dev for mknod
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// OUT:
//
// int mkfifo(const char *pathname, mode_t mode);
// - via mknod(S_IFIFO | mode)
// int mknodat(int dirfd, const char *pathname, mode_t mode, dev_t dev);
// int mkfifoat(int dirfd, const char *pathname, mode_t mode);
// - via __getpath + ...

#define IPC_IO_FS_LINK		25	/**< link() */
// int __libc_link(const char *oldpath, const char *newpath, int flags);
// int link(const char *oldpath, const char *newpath);
//
// Create hard link
//
// IN:
// - h32: flags
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: oldpath
// - size1: strlen(oldpath)
// - buf2: newpath
// - size2: strlen(newpath)
// OUT:
//
// int linkat(int olddirfd, const char *oldpath, int newdirfd, const char *newpath, int flags);
// - via __getpath + __libc_link()

#define IPC_IO_FS_SYMLINK	26	/**< symlink() */
// int symlink(const char *target, const char *linkpath);
//
// NOTE: order of linkpath and target reversed in protocol
//
// Create symlink
//
// IN:
// - h32: flags (must be zero)
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: linkpath
// - size1: strlen(linkpath)
// - buf2: target
// - size2: strlen(target)
// OUT:
//
// int symlinkat(const char *target, int newdirfd, const char *linkpath);
// - via __getpath + symlink()

#define IPC_IO_FS_READLINK	27	/**< readlink() */
// ssize_t readlink(const char *pathname, char *buf, size_t size);
//
// Read symlink
//
// IN:
// - h32: flags (must be zero)
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// - buf2: buf
// - size2: size
// OUT:
// - size2: returned size // FIXME: is this the right slot?
//
// ssize_t readlinkat(int dirfd, const char *pathname, char *buf, size_t bufsiz);
// - via __getpath + readlink()

#define IPC_IO_FS_CHMOD		28	/**< chmod(), __libc_chmod */
// int __libc_chmod(const char *pathname, mode_t mode, int flags);
// int chmod(const char *pathname, mode_t mode);
//
// change file access permissions
//
// IN:
// - h32: flags
// - h64l: mode
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// OUT:
//
// int fchmodat(int dirfd, const char *pathname, mode_t mode, int flags);
// - via __getpath + __libc_chmod()

#define IPC_IO_FS_CHOWN		29	/**< chown(), lchown(), __libc_chown */
// int __libc_chown(const char *pathname, uid_t owner, gid_t group, int flags);
// int chown(const char *pathname, uid_t owner, gid_t group);
// int lchown(const char *pathname, uid_t owner, gid_t group);
//
// change file owner
//
// IN:
// - h32: flags
// - h64l: owner
// - h64h: group
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// OUT:
//
// int fchownat(int dirfd, const char *pathname, uid_t owner, gid_t group, int flags);
// - via __getpath + __libc_chown()

#define IPC_IO_FS_UTIME		30	/**< utime(), utimes(), lutimes(), utimensat(), __libc_utime() */
// int __libc_utime(const char *pathname, const struct timespec times[2], int flags);
//
// change file time
//
// IN:
// - h32: flags
// - buf0: mp_pathname
// - size0: strlen(mp_pathname)
// - buf1: pathname
// - size1: strlen(pathname)
// - buf2: times[2]
// - size2: sizeof(times[2])
// OUT:
//
// int utime(const char *filename, const struct utimbuf *times);
// int utimes(const char *filename, const struct timeval tv[2]);
// int lutimes(const char *filename, const struct timeval tv[2]);
// - via __libc_utime()
// int utimensat(int dirfd, const char *pathname, const struct timespec times[2], int flags);
// int futimesat(int dirfd, const char *pathname, const struct timeval times[2]); // BSD + Linux
// - via __getpath + __libc_utime()

#define IPC_IO_CHMOD		31	/**< fchmod() */
// int fchmod(int fd, mode_t mode);
//
// change file access permissions of open file
//
// IN:
// - h64l: mode
// OUT:

#define IPC_IO_CHOWN		32	/**< fchown() */
// int fchown(int fd, uid_t owner, gid_t group);
//
// change file owner of open file
//
// IN:
// - h64l: owner
// - h64h: group
// OUT:

#define IPC_IO_UTIME		33	/**< futimes(), futimens() */
// int futimens(int fd, const struct timespec times[2]);
//
// change file time of open file
//
// IN:
// - buf0: times[2]
// - size0: sizeof(times[2])
// OUT:
//
// int futimes(int fd, const struct timeval tv[2]); // BSD + Linux extension
// - via futimens()

////////////////////////////////////////////////////////////////////////////////

/* socket API operations */

#define IPC_IO_SEND			34	/**< send(), sendto(), sendmsg() */
// ssize_t send(int sfd, const void *buf, size_t size, int flags);
// ssize_t sendto(int sfd, const void *buf, size_t size, int flags, const struct sockaddr *addr, socklen_t addrlen);
// ssize_t sendmsg(int sfd, const struct msghdr *msg, int flags);
//
// send data and control data to specific address
//
// IN:
// - h32: flags
// - buf0: buf
// - size0: size
// - buf1: addr
// - size1: addrlen
// - buf2: 0
// - size2: 0
// OUT:
// - h32: 0
// - size0: send size
//
// IN:
// - h32: flags
// - buf0: msg->msg_iov
// - size0: msg->msg_iovlen
// - buf1: msg->msg_addrlen
// - size1: msg->msg_name
// - buf2: msg->msg_control
// - size2: msg->msg_controllen
// OUT:
// - h32: 0
// - size0: send size

#define IPC_IO_RECV			35	/**< recv(), recvfrom(), recvmsg() */
// ssize_t recv(int sfd, void *buf, size_t size, int flags);
// ssize_t recvfrom(int sfd, void *buf, size_t size, int flags, struct sockaddr *addr, socklen_t *addrlen);
// ssize_t recvmsg(int sfd, struct msghdr *msg, int flags);
//
// IN:
// - h32: flags
// - buf0: buf
// - size0: size
// - buf1: addr
// - size1: addrlen
// - buf2: 0
// - size2: 0
// OUT:
// - h32: msg->msg_flags
// - size0: recv size
// - size1: addrlen
//
// IN:
// - h32: flags
// - buf0: msg->msg_iov
// - size0: msg->msg_iovlen
// - buf1: msg->msg_addrlen
// - size1: msg->msg_name
// - buf2: msg->msg_control
// - size2: msg->msg_controllen
// OUT:
// - h32: msg->msg_flags
// - size0: recv size
// - size1: addrlen

#define IPC_IO_ACCEPT		36	/**< accept() */
// int accept(int sfd, struct sockaddr *addr, socklen_t *addrlen);
// int accept4(int sfd, struct sockaddr *addr, socklen_t *addrlen, int flags); // BSD, Linux
//
// accept connection
//
// IN:
// - h32: flags
// - buf0: addr
// - size0: addrlen
// OUT:
// - size0: addrlen
// - fd0: new file descriptor

#define IPC_IO_CONNECT		37
#define IPC_IO_BIND			38
#define IPC_IO_GETPEERNAME	39
#define IPC_IO_GETSOCKNAME	40
#define IPC_IO_LISTEN		41
#define IPC_IO_SHUTDOWN		42
#define IPC_IO_GETSOCKOPT	43
#define IPC_IO_SETSOCKOPT	44
// int connect(int sfd, const struct sockaddr *addr, socklen_t addrlen);
// int bind(int sfd, const struct sockaddr *addr, socklen_t addrlen);
// int getpeername(int sfd, struct sockaddr *addr, socklen_t *addrlen);
// int getsockname(int sfd, struct sockaddr *addr, socklen_t *addrlen);
// int listen(int sfd, int backlog);
// int shutdown(int sfd, int how);
// int getsockopt(int sfd, int level, int optname, void *optval, socklen_t *optlen);
// int setsockopt(int sfd, int level, int optname, const void *optval, socklen_t optlen);
//
// socket multiplexer
//
// IN:
// - h8: socketcall, 1: connect, 2: bind, 3: getpeername, 4: getsockname, 5: listen, 6: shutdown, 7: getsockopt, 8: setsockopt
// - h32: backlog / how / level
// - h64l: optname
// - buf0: addr / optval
// - size0: addrlen / optlen
// OUT:
// - size0: actual len (getpeername, getsockname, getsockopt)
// NOTE: no retval here for getsockopt()

/* number of entries in protocol */
#define IPC_IO_NUM			(IPC_IO_SETSOCKOPT + 1)

#endif
