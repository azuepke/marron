/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * stdarg.h
 *
 * GCC specific variable argument handling.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported
 */

#ifndef STDARG_H_
#define STDARG_H_

#ifdef __cplusplus
extern "C" {
#endif

#define va_start(v, l)		__builtin_va_start(v, l)
#define va_arg(v, l)		__builtin_va_arg(v, l)
#define va_end(v)			__builtin_va_end(v)
#define va_copy(d, s)		__builtin_va_copy(d, s)
#ifndef __DEFINED_va_list
#define __DEFINED_va_list
typedef __builtin_va_list	va_list;
#endif

#ifdef __cplusplus
}
#endif

#endif
