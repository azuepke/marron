/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * limits.h
 *
 * Implementatin-defined constants
 *
 * azuepke, 2025-02-09: initial with selected constants
 */

#ifndef LIMITS_H_
#define LIMITS_H_

#ifdef __cplusplus
extern "C" {
#endif

/* numerical limits */
#define CHAR_BIT	8
#if '\xff' > 0
#define CHAR_MAX	0xff
#define CHAR_MIN	0
#else
#define CHAR_MAX	0x7f
#define CHAR_MIN	(-CHAR_MAX-1)
#endif
#define INT_MAX		0x7fffffff
#define INT_MIN		(-INT_MAX-1)
#define LLONG_MAX	0x7fffffffffffffffll
#define LLONG_MIN	(-LLONG_MAX-1)
#define LONG_BIT	(8 * __SIZEOF_LONG__)
#if __SIZEOF_LONG__ == 8
#define LONG_MAX	0x7fffffffffffffffl
#else
#define LONG_MAX	0x7fffffffl
#endif
#define LONG_MIN	(-LONG_MAX-1)
#define MB_LEN_MAX	4
#define SCHAR_MAX	0x7f
#define SCHAR_MIN	(-SCHAR_MAX-1)
#define SHRT_MAX	0x7fff
#define SHRT_MIN 	(-SHRT_MAX-1)
#if __SIZEOF_LONG__ == 8
#define SSIZE_MAX	0x7fffffffffffffffl
#else
#define SSIZE_MAX	0x7fffffff
#endif
#define UCHAR_MAX	0xff
#define UINT_MAX	0xffffffffu
#if __SIZEOF_LONG__ == 8
#define ULONG_MAX	0xfffffffffffffffful
#else
#define ULONG_MAX	0xfffffffful
#endif
#define ULLONG_MAX	0xffffffffffffffffull
#define USHRT_MAX	0xffff
#define WORD_BIT	32

/* OS constants */
#define GETENTROPY_MAX	256

/* OS limits */
#define DELAYTIMER_MAX	0x7fffffff
#define IOV_MAX			16
#define PTHREAD_DESTRUCTOR_ITERATIONS	4
#define PTHREAD_KEYS_MAX	128
#define PTHREAD_STACK_MIN	0x2000	/* 8K */
#define SEM_VALUE_MAX	0x7fff

/* FS limits */
#define	FILESIZEBITS	64
#define NAME_MAX		255
#define PIPE_BUF		4096

#ifdef __cplusplus
}
#endif

#endif
