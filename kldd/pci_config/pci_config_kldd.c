/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * pci_config_kldd.c
 *
 * PCI config space access for user space
 *
 *
 * To use the driver, include the following KLDD entries in hardware.xml:
 *
 *   <kldd name="pci_config" handler="pci_config_kldd"/>
 *
 * And grant access to the target partition:
 *
 *   <kldd_call name="pci_config" kldd="pci_config"/>
 *
 * The API is:
 *
 *   err_t sys_kldd_call(kldd_id, pcidev, offset, change_mask, new_val);
 *
 * See the end of this file for common PCI config space accessors.
 *
 * NOTE: this API provides access to _all_ PCI devices. Use with care!
 *
 * azuepke, 2025-01-22: initial
 */

#include <pci_stub.h>


/* forward declarations */
struct kldd_priv;

/* prototypes */
uint32_t pci_config_kldd(
	struct kldd_priv *priv,
	unsigned long pcidev,
	unsigned long offset,
	unsigned long new_value,
	unsigned long change_mask);

/** generic KLDD to access PCI config space */
uint32_t pci_config_kldd(
	struct kldd_priv *priv,
	unsigned long pcidev,
	unsigned long access_size_offset,
	unsigned long new_value,
	unsigned long change_mask)
{
	uint32_t access_size = access_size_offset >> 16;
	uint32_t offset = access_size_offset & 0xffffu;
	uint32_t old_value;

	(void)priv;

	old_value = 0xffffffff;

	switch (access_size) {
	case 0x14:
		old_value = pci_config_read32(pcidev, offset);
		break;

	case 0x24:
		pci_config_write32(pcidev, offset, new_value);
		break;

	case 0x34:
		old_value = pci_config_read32(pcidev, offset);
		new_value = (old_value & ~change_mask) | (new_value & change_mask);
		pci_config_write32(pcidev, offset, new_value);
		break;

	default:
		break;
	}

	return old_value;
}

#if 0

// Usage examples

/*
 * ID of pci_config KLDD
 *
 * Initialize with:
 *
 *   pci_config_kldd_id = config_kldd_id("pci_config");
 */
extern unsigned int pci_config_kldd_id;

static inline uint32_t pci_config_read32(pcidev_t pcidev, uint32_t offset)
{
	return sys_kldd_call(pci_config_kldd_id, pcidev, (0x14 << 16) | offset, 0, 0);
}

static inline void pci_config_write32(pcidev_t pcidev, uint32_t offset, uint32_t value)
{
	sys_kldd_call(pci_config_kldd_id, pcidev, (0x24 << 16) | offset, value, 0);
}

static inline uint32_t pci_config_rmw32(pcidev_t pcidev, uint32_t offset, uint32_t value, uint32_t change_mask)
{
	/* Sequence:
	 * - old_value = read(...)
	 * - new_value = (old_value & ~change_mask) | (new_value & change_mask)
	 * - write(..., new_value)
	 * - return old_value
	 */
	return sys_kldd_call(pci_config_kldd_id, pcidev, (0x34 << 16) | offset, value, change_mask);
}

#endif
