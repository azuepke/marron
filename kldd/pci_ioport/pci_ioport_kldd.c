/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * pci_ioport_kldd.c
 *
 * PCI I/O space (I/O port) access for user space
 *
 *
 * To use the driver, include the following KLDD entries in hardware.xml:
 *
 *   <kldd name="pci_ioport" handler="pci_ioport_kldd"/>
 *
 * And grant access to the target partition:
 *
 *   <kldd_call name="pci_ioport" kldd="pci_ioport"/>
 *
 * The API is:
 *
 *   err_t sys_kldd_call(kldd_id, pcidev, offset, change_mask, new_val);
 *
 * See the end of this file for common PCI I/O space accessors.
 *
 * NOTE: this API provides access to _all_ PCI I/O space. Use with care!
 *
 * azuepke, 2025-01-22: initial
 */

#include <pci_ioport.h>


/* forward declarations */
struct kldd_priv;

/* prototypes */
uint32_t pci_ioport_kldd(
	struct kldd_priv *priv,
	unsigned long pcidev,
	unsigned long access_size_offset,
	unsigned long new_value,
	unsigned long change_mask);

/** generic KLDD to access PCI I/O space */
uint32_t pci_ioport_kldd(
	struct kldd_priv *priv,
	unsigned long pcidev,
	unsigned long access_size_offset,
	unsigned long new_value,
	unsigned long change_mask)
{
	uint32_t access_size = access_size_offset >> 16;
	uint32_t offset = access_size_offset & 0xffffu;
	uint32_t old_value;

	(void)priv;
	(void)change_mask;

	old_value = 0xffffffff;

	switch (access_size) {
	case 0x11:
		old_value = pci_ioport_read8(pcidev, offset);
		break;

	case 0x12:
		old_value = pci_ioport_read16(pcidev, offset);
		break;

	case 0x14:
		old_value = pci_ioport_read32(pcidev, offset);
		break;

	case 0x21:
		pci_ioport_write8(pcidev, offset, new_value);
		break;

	case 0x22:
		pci_ioport_write16(pcidev, offset, new_value);
		break;

	case 0x24:
		pci_ioport_write32(pcidev, offset, new_value);
		break;

	case 0x31:
	case 0x32:
	case 0x34:
		/* read-modify-write not supported */
		/* fall-through */
	default:
		break;
	}

	return old_value;
}

#if 0

// Usage examples

/*
 * ID of pci_ioport KLDD
 *
 * Initialize with:
 *
 *   pci_ioport_kldd_id = config_kldd_id("pci_ioport");
 */
extern unsigned int pci_ioport_kldd_id;

static inline uint8_t pci_ioport_read8(pcidev_t pcidev, uint32_t offset)
{
	return sys_kldd_call(pci_ioport_kldd_id, pcidev, (0x11 << 16) | offset, 0, 0);
}

static inline uint16_t pci_ioport_read16(pcidev_t pcidev, uint32_t offset)
{
	return sys_kldd_call(pci_ioport_kldd_id, pcidev, (0x12 << 16) | offset, 0, 0);
}

static inline uint32_t pci_ioport_read32(pcidev_t pcidev, uint32_t offset)
{
	return sys_kldd_call(pci_ioport_kldd_id, pcidev, (0x14 << 16) | offset, 0, 0);
}

static inline void pci_ioport_write8(pcidev_t pcidev, uint32_t offset, uint8_t value)
{
	sys_kldd_call(pci_ioport_kldd_id, pcidev, (0x21 << 16) | offset, value, 0);
}

static inline void pci_ioport_write16(pcidev_t pcidev, uint32_t offset, uint16_t value)
{
	sys_kldd_call(pci_ioport_kldd_id, pcidev, (0x22 << 16) | offset, value, 0);
}

static inline void pci_ioport_write32(pcidev_t pcidev, uint32_t offset, uint32_t value)
{
	sys_kldd_call(pci_ioport_kldd_id, pcidev, (0x24 << 16) | offset, value, 0);
}

#endif
