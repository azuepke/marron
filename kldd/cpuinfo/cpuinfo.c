/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * cpuinfo.c
 *
 * Kernel-level device driver (KLDD) to query supervisor CPU config registers
 *
 * Usage:
 *
 *   unsigned long val;
 *   err_t err;
 *   err = sys_kldd_call(kldd_id, reg, opt, 0, (unsigned long)&val);
 *   // system register returned in "val"
 *
 * val   register   cp-encoding   comment
 * ====  =========  ============  ========
 *   0   MIDR       0, c0, c0, 0  opt ignored
 *   1   CTR        0, c0, c0, 1  opt ignored
 *   2   REVIDR     0, c0, c0, 6  opt ignored
 *   3   CLIDR      1, c0, c0, 1  opt ignored
 *   4   CCSIDR     1, c0, c0, 0  opt sets CSSELR / CSSELR_EL1
 *
 * azuepke, 2023-08-08: initial
 */

#include <marron/error.h>
#include <kldd.h>
#include <kernel.h>
#include <uaccess.h>
#include <bsp.h>


#if defined(__arm__) || defined(__aarch64__)

/** Get MIDR_EL1 */
static inline unsigned long arm_get_midr(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, MIDR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c0, c0, 0" : "=r"(val));
#endif
	return val;
}

/** Get CTR_EL0 */
static inline unsigned long arm_get_ctr(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, CTR_EL0" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c0, c0, 1" : "=r"(val));
#endif
	return val;
}

/** Get REVIDR */
static inline unsigned long arm_get_revidr(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, REVIDR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c0, c0, 6" : "=r"(val));
#endif
	return val;
}

/** Get CLIDR */
static inline unsigned long arm_get_clidr(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, CLIDR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 1, %0, c0, c0, 1" : "=r"(val));
#endif
	return val;
}

/** Get CCSIDR */
static inline unsigned long arm_get_ccsidr(unsigned long level_and_ibit)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("msr CSSELR_EL1, %1\n"
	                  "isb\n"
	                  "mrs %0, CCSIDR_EL1\n"
	                  : "=r"(val) : "r"(level_and_ibit));
#else
	__asm__ volatile ("mcr p15, 2, %0, c0, c0, 0\n"
	                  "isb\n"
	                  "mrc p15, 1, %0, c0, c0, 0"
	                  : "=r"(val) : "r"(level_and_ibit));
#endif
	return val;
}

static inline err_t get_config_reg(ulong_t reg, ulong_t opt, ulong_t *val_p)
{
	switch (reg) {
	case 0:
		*val_p = arm_get_midr();
		return EOK;
	case 1:
		*val_p = arm_get_ctr();
		return EOK;
	case 2:
		*val_p = arm_get_revidr();
		return EOK;
	case 3:
		*val_p = arm_get_clidr();
		return EOK;
	case 4:
		*val_p = arm_get_ccsidr(opt);
		return EOK;
	default:
		return EINVAL;
	}
}

#else

/* default implementation */
static inline err_t get_config_reg(ulong_t reg, ulong_t opt, ulong_t *val_p)
{
	(void)reg;
	(void)opt;
	(void)val_p;
	return EINVAL;
}

#endif

/** KLDD cpuinfo driver */
err_t kldd_cpuinfo(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4);

/** KLDD driver to query supervisor CPU config registers */
err_t kldd_cpuinfo(
	struct kldd_priv *priv __unused,
	unsigned long reg,
	unsigned long opt,
	unsigned long arg3 __unused,
	unsigned long user_ptr __unused)
{
	unsigned long *val_p = (unsigned long *)user_ptr;
	unsigned long val;
	err_t err;

	err = get_config_reg(reg, opt, &val);

	if (err == EOK) {
		err = user_put_p(val_p, val);
	}

	return err;
}
