/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023, 2024 Alexander Zuepke */
/*
 * console.c
 *
 * Kernel-level device drivers (KLDD) console implementation
 *
 * azuepke, 2023-08-08: initial, from kldd.c
 * azuepke, 2024-01-02: console input
 */

#include <marron/error.h>
#include <kldd.h>
#include <kernel.h>
#include <bsp.h>
#include <uaccess.h>


/** KLDD console driver */
err_t kldd_console(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4);

/** KLDD driver for console printing and input (alternative to sys_putc()) */
err_t kldd_console(
	struct kldd_priv *priv __unused,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3 __unused,
	unsigned long arg4 __unused)
{
	int c;
	err_t err;

	switch (arg1) {
	case 0:	/* putc */
		c = arg2;

		console_lock();
		err = bsp_putc(c);
		console_unlock();
		break;

	case 1: /* getc */
		console_lock();
		err = bsp_getc(&c);
		console_unlock();

		if (err == EOK) {
			err = user_put_4(arg2, c);
		}

		break;

	default:
		err = ENOSYS;
		break;
	}

	return err;
}
