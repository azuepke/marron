/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * kldd_tlb_mapper.c
 *
 * KLDD driver to map and unmap large TLB entries (for testing purposes).
 *
 * The VM subsystem does not support large TLB entries for now.
 * This driver bypasses the VM subsystem to map or unmap large TLB entries
 * (also called large mappings or huge pages or super pages).
 *
 * To use the driver, include the following KLDD entries in hardware.xml:
 *
 *   <kldd name="tlb_map" handler="kldd_tlb_map"/>
 *   <kldd name="tlb_unmap" handler="kldd_tlb_unmap"/>
 *
 * And grant access to the target partition:
 *
 *   <kldd_call name="tlb_map" kldd="tlb_map"/>
 *   <kldd_call name="tlb_unmap" kldd="tlb_unmap"/>
 *
 * Use tlb_map(addr, size, memrq_type, memrq_id) to map a memory requirement
 * using a large TLB, similar to a call to sys_memrq_map().
 * Note that the map operation requires a properly aligned memory requirement.
 * It will always map from the start of the memory requirement,
 * and the access permissions ("prot") are derived from the memory requirement.
 * The mapping must be created into an otherwise unused and unmapped part
 * of the virtual address space, as this bypasses the VM subsystem
 * and any page reference accounting.
 *
 * For explicit unmapping, use tlb_unmap(addr, size).
 * The mapping will also implicitly be removed when killing the partition.
 *
 * WARNING: This bypasses the VM subsystem, so handle with care.
 *
 * azuepke, 2023-08-02: initial
 */

#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <assert.h>
//#include <board.h>
#include <uaccess.h>
#include <arm_insn.h>
#include <arm_pte_v8.h>
#include <vm.h>
#include <adspace.h>
#include <memrq.h>
#include <part.h>
#include <marron/macros.h>


struct kldd_priv;

/** KLDD driver to map large TLB entries */
err_t kldd_tlb_map(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4);

/** KLDD driver to unmap large TLB entries */
err_t kldd_tlb_unmap(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4);

////////////////////////////////////////////////////////////////////////////////

/* 48 bits physical address space mask */
#define PHYS_ADDR_MASK 0x0000fffffffff000ULL

/* accessors for page table entries */
#define PTE_TO_VIRT(pte) PHYS_TO_KERNEL(((unsigned long long)(pte)) & PHYS_ADDR_MASK)

/** write/update entry in page table */
static inline void arm_pte_set(arm_pte_t *ptep, arm_pte_t val)
{
	access_once(*ptep) = val;
	/* the barrier orders changes in the page tables w.r.t. page table walks */
	_arm_dsb("ISHST");
}

/** permission bits encoding */
static inline arm_pte_t prot_bits(unsigned int prot)
{
	arm_pte_t pte_bits;

	/* we set PTE_PXN for all user space mappings */
	pte_bits = PTE_PXN;
	if (prot != 0) {
		/* checking for prot != 0 instead of the PROT_READ bit
		 * enables "read permission implied" for all mappings
		 */
		pte_bits |= PTE_AF | PTE_AP1;
	}

	if ((prot & PROT_WRITE) == 0) {
		pte_bits |= PTE_AP2;
	}

	if ((prot & PROT_EXEC) == 0) {
		pte_bits |= PTE_XN;
	}

	return pte_bits;
}

/** cache mode encoding */
static inline arm_pte_t cache_bits(unsigned int cache)
{
	arm_pte_t pte_bits;

	/* the cache mode directly encodes to the PTE_ATTR bits */
	assert(cache < 8);
	pte_bits = cache << PTE_ATTR_SHIFT;

	/* for modes 0 .. 3 we also disable execution (I/O or device mappings) */
	if (cache < 4) {
		pte_bits |= PTE_XN;
	}

	/* inner shareable */
	pte_bits |= PTE_SH0 | PTE_SH1;

	return pte_bits;
}

/** create a mapping of a physical resource in an address space */
/* FIXME: does not handle overmapping of large mappings */
static err_t arch_adspace_map_large(struct adspace *adspace, addr_t va, size_t size,
	phys_addr_t pa, unsigned int prot, unsigned int cache)
{
	arm_pte_t *ptep;
	arm_pte_t *pt;
	arm_pte_t pte;
	addr_t end;

	assert(adspace != NULL);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);

	pte = pa;
	pte |= PTE_NG | PTE_TYPE_BLOCK;
	pte |= prot_bits(prot);
	pte |= cache_bits(cache);

	end = va + size;
	while (va < end) {
		/* check level 1 table */
		pt = adspace->l1_table;
		ptep = &pt[arm_addr_to_l1(va)];
		if (!PTE_IS_PT(*ptep)) {
				return ENOMEM;
		}

		/* check level 2 table */
		pt = PTE_TO_VIRT(*ptep);
		ptep = &pt[arm_addr_to_l2(va)];
		if (PTE_IS_VALID(*ptep)) {
			/* no support for overmapping */
			return ENOMEM;
		}

		/* update page table entry */
		arm_pte_set(ptep, pte);

		pte += BLOCK_SIZE;
		va += BLOCK_SIZE;
	}

	return EOK;
}

////////////////////////////////////////////////////////////////////////////////


err_t kldd_tlb_map(
	struct kldd_priv *priv __unused,
	unsigned long addr,			/* virtual target address */
	unsigned long size,			/* size of mapping */
	unsigned long memrq_type,	/* memrq type */
	unsigned long memrq_id)		/* memrq ID */
{
	const struct memrq_cfg *m;
	struct adspace *adspace;
	size_t aligned_size;
	err_t err;

	err = vm_check_memrq(memrq_type, memrq_id, &m);
	if (err != EOK) {
		return err;
	}

	if (((m->phys | size | addr) & (BLOCK_SIZE - 1)) != 0) {
		/* not page aligned */
		return EINVAL;
	}
	aligned_size = ALIGN_UP(m->size, BLOCK_SIZE);
	if (size > aligned_size) {
		/* range exceeds memory requirement */
		return ESTATE;
	}
	if (!USER_CHECK_RANGE(addr, size)) {
		/* not valid user space region */
		return EINVAL;
	}

	adspace = current_part_cfg()->adspace;

	// FIXME: fix locking in mapping
	//part_lock(current_part_cfg()->part);
	//adspace_lock(adspace);

	/* linear mapping without reference counting */
	err = arch_adspace_map_large(adspace, addr, size, m->phys, m->prot, m->cache);

	//adspace_unlock(adspace);
	//part_unlock(current_part_cfg()->part);

	return err;
}


err_t kldd_tlb_unmap(
	struct kldd_priv *priv __unused,
	unsigned long addr,			/* virtual target address */
	unsigned long size,			/* size of mapping */
	unsigned long arg3 __unused,
	unsigned long arg4 __unused)
{
	struct adspace *adspace;
	err_t err;

	if (((addr | size) & (BLOCK_SIZE - 1)) != 0) {
		/* not page aligned */
		return EINVAL;
	}
	if (!USER_CHECK_RANGE(addr, size)) {
		/* not valid user space region */
		return EINVAL;
	}
	if (size == 0) {
		/* zero size is not OK */
		return EINVAL;
	}

	adspace = current_part_cfg()->adspace;

	// FIXME: fix locking in mapping
	//part_lock(current_part_cfg()->part);
	//adspace_lock(adspace);

	err = arch_adspace_unmap(adspace, addr, size);

	//adspace_unlock(adspace);
	//part_unlock(current_part_cfg()->part);

	return err;
}
