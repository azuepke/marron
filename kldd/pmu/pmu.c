/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * kldd_pmu.c
 *
 * KLDD driver to configure the PMU per core.
 *
 * This driver for 64-bit ARM allows to change PMU settings at runtime.
 * Any changes to a PMU counter affects only the calling core.
 *
 * To use the driver, include the following KLDD entries in hardware.xml:
 *
 *   <kldd name="pmu_ctrl" handler="kldd_pmu_ctrl"/>
 *
 * And grant access to the target partition:
 *
 *   <kldd_call name="pmu_ctrl" kldd="pmu_ctrl"/>
 *
 * The API is:
 *
 *   err_t sys_kldd_call(kldd_id, index, type, ignored1, ignored2);
 *
 * To change a PMU setting, e.g. set counter #3 to type 0x17, use:
 *
 *   err = sys_kldd_call(kldd_id, 3, 0x17, 0, 0);
 *
 * The number of available PMU registers "index" is defined by the core's
 * microarchitecture (note that PMCR_EL0 is readable from user space),
 * like the specific "type" that can be configured for each PMU register.
 * Setting "type" to zero disables a PMU counter.
 * The last two arguments are ignored.
 *
 * azuepke, 2023-08-07: initial
 */

#include <kernel.h>
#include <panic.h>
#include <bsp.h>

#if defined(__arm__) || defined(__aarch64__)
#include <arm_insn.h>
#include <arm_perf.h>
#endif

struct kldd_priv;

/** KLDD driver to map large TLB entries */
err_t kldd_pmu_ctrl(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4);

err_t kldd_pmu_ctrl(
	struct kldd_priv *priv __unused,
	unsigned long index,		/* index to PMU register */
	unsigned long type,			/* type */
	unsigned long arg3 __unused,
	unsigned long arg4 __unused)
{
#if defined(__arm__) || defined(__aarch64__)
	unsigned int num_counters;

	/*
	 * ARMv7 and v8 define a cycle counter and up to 31 individual counters.
	 * QEMU does not implement the individual counters,
	 * so check against what is available on the core.
	 */
	num_counters = (arm_perf_get_ctrl() >> 11) & 0x1f;

	if (index >= num_counters) {
		return EINVAL;
	}

	if (type != 0) {
		/* enable PMU counter */
		arm_perf_select(index);
		arm_isb();
		arm_perf_set_type(type);
		arm_isb();
		arm_perf_enable_counter(1u << index);
	} else {
		/* disable PMU counter */
		arm_perf_disable_counter(1u << index);
	}

	return EOK;
#else
	(void)index;
	(void)type;
	return EINVAL;
#endif
}
