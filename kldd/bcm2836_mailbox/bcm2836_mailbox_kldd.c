/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * bcm2836_mailbox_kldd.c
 *
 * Raspberry Pi GPU mailbox communication
 *
 *
 * To use the driver, include the following KLDD entries in hardware.xml:
 *
 *   <kldd name="bcm2836_mailbox" handler="bcm2836_mailbox_kldd"/>
 *
 * And grant access to the target partition:
 *
 *   <kldd_call name="bcm2836_mailbox" kldd="bcm2836_mailbox"/>
 *
 * The API is:
 *
 *   err_t sys_kldd_call(kldd_id, mbox_msg, size, 0, 0);
 *
 *
 * See https://github.com/raspberrypi/firmware/wiki/Mailboxes
 * for the mailbox registers.
 * See https://github.com/raspberrypi/firmware/wiki/Mailbox-property-interface
 * for the mailbox message format.
 *
 *
 * azuepke, 2025-03-08: initial
 */

#include <marron/error.h>
#include <marron/compiler.h>
#include <kldd.h>
#include <kernel.h>
#include <bsp.h>
#include <uaccess.h>
#include <board.h>
#include <arch_io.h>
#include <spin.h>


/* prototypes */
uint32_t bcm2836_mailbox_kldd(
	struct kldd_priv *priv,
	uint32_t *mbox_msg,
	size_t size,
	unsigned long arg3,
	unsigned long arg4);


/* GPU mailbox registers at 0x3f00b880 behind the rpi IRQ controller */
#define MBOX_ADDR			(CFG_IO_ADDR_rpi_irq + 0x880)

/* GPU mailbox registers */
#define MBOX0_READ			0x00
		/* 0x04, 0x08, 0x0c alias MBOX0_READ */
#define MBOX0_PEEK			0x10
#define MBOX0_SENDER		0x14
#define MBOX0_STATUS		0x18
#define MBOX0_CONFIG		0x1c

#define MBOX1_WRITE			0x20
		/* 0x24, 0x28, 0x2c alias MBOX1_WRITE */
		/* 0x30 unused */
		/* 0x34 unused */
#define MBOX1_STATUS		0x38
		/* 0x3c unused */

/* mailbox register accessors */
static inline uint32_t mbox_rd(unsigned long reg)
{
	return read32(io_ptr(MBOX_ADDR, reg));
}

static inline void mbox_wr(unsigned long reg, uint32_t val)
{
	write32(io_ptr(MBOX_ADDR, reg), val);
}

/* mailbox status register */
#define MBOX_STATUS_FULL				0x80000000u
#define MBOX_STATUS_EMPTY				0x40000000u
#define MBOX_STATUS_LEVEL_MASK			0x000000ffu

/* mailbox config register */
#define MBOX_CONFIG_IRQ_DATA_ENABLE		(1u << 0)
#define MBOX_CONFIG_IRQ_SPACE_ENABLE	(1u << 1)
#define MBOX_CONFIG_IRQ_OPP_ENABLE		(1u << 2)
#define MBOX_CONFIG_IRQ_CLEAR			(1u << 3)
#define MBOX_CONFIG_IRQ_DATA_PENDING	(1u << 4)
#define MBOX_CONFIG_IRQ_SPACE_PENDING	(1u << 5)
#define MBOX_CONFIG_IRQ_OPP_PENDING		(1u << 6)
#define MBOX_CONFIG_ERR_OWNER			(1u << 8)
#define MBOX_CONFIG_ERR_OVERFLOW		(1u << 9)
#define MBOX_CONFIG_ERR_UDERFLOW		(1u << 10)

/* mailbox channels */
#define MBOX_CHAN_PROPERTY	8

/* number of words in mailbox message */
#define MBOX_SIZE			64

/* maibox word 1 request / response */
#define MBOX_RR_REQUEST						0x00000000u
#define MBOX_RR_RESPONSE					0x80000000u
#define MBOX_RR_ERROR						0x00000001u

static spin_t mbox_lock = SPIN_INIT;

/** Raspberry Pi GPU mailbox communication */
uint32_t bcm2836_mailbox_kldd(
	struct kldd_priv *priv,
	uint32_t *mbox_msg,
	size_t size,
	unsigned long arg3,
	unsigned long arg4)
{
	uint32_t mbox[MBOX_SIZE] __aligned(16);
	uint32_t msg;
	uint32_t val;
	err_t err;

	(void)priv;
	(void)arg3;
	(void)arg4;

	assert((((unsigned long)mbox) & 0xf) == 0);

	if ((size == 0) || ((size & 0x3) != 0) || (size > 4*MBOX_SIZE)) {
		return EINVAL;
	}

	err = user_copy_in(mbox, mbox_msg, size);
	if (err != EOK) {
		return err;
	}

	/* sanitize */
	mbox[0] = size;
	mbox[1] = MBOX_RR_REQUEST;
	mbox[(size - 1) / 4] = 0;

	spin_lock(&mbox_lock);

	/* wait if mailbox is full */
	do {
		val = mbox_rd(MBOX0_STATUS);
	} while ((val & MBOX_STATUS_FULL) != 0);

	/* send */
	msg = KERNEL_TO_PHYS(mbox) | MBOX_CHAN_PROPERTY;
	mbox_wr(MBOX1_WRITE, msg);

	while (1) {
		/* wait for response */
		do {
			val = mbox_rd(MBOX0_STATUS);
		} while ((val & MBOX_STATUS_EMPTY) != 0);

		/* our request? */
		val = mbox_rd(MBOX0_READ);
		if (val == msg) {
			break;
		}
	}

	spin_unlock(&mbox_lock);

	err = user_copy_out(mbox_msg, mbox, size);

	return err;
}
