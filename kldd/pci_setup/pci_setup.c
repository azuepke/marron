/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * pci_setup.c
 *
 * PCI setup -- generic PCI initialization code to assign BARs to PCI devices
 *
 *
 * To use this driver, include the following KLDD entry in hardware.xml:
 *
 *   <kldd name="pci_setup" handler="pci_setup_kldd" init="pci_setup_once"/>
 *
 * That's it. There's nothing to assign to a partition or call at runtime.
 *
 *
 * azuepke, 2025-01-22: initial
 */

#include <kernel.h>
#include <board.h>
#include <pci_stub.h>
#include <pci_config_regs.h>
#include <stddef.h>
#include <marron/macros.h>
#include <marron/arch_defs.h>
#include <marron/error.h>


/* forward declarations */
struct kldd_priv;

/* prototypes */
void pci_setup_once(void);

err_t pci_setup_kldd(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4);


/* default alignments for I/O and memory BARs */
#define BAR_ALIGN_IO_FN		0x100		/* group I/O of multifunction devices */
#define BAR_ALIGN_IO_DEV	PAGE_SIZE	/* PAGE_SIZE limits us to 16 devices */
#define BAR_ALIGN_MEM		PAGE_SIZE
#define CACHE_LINE_SIZE		ARCH_ALIGN

static void pci_config_dev(pcidev_t pcidev, uint32_t *assign_io, uint32_t *assign_mem)
{
	uint32_t bar_mask;
	uint64_t bar_limit;
	uint64_t bar_addr;
	uint64_t bar_size;
	uint64_t bar_align;
	int bar_is_io;
	int bar_is_prefetch;
	int bar_is_64bit;
	uint32_t bar;
	uint32_t val;

	/* 1. disable interrupt, bus master, mem and I/O space */
	val = pci_config_read32(pcidev, PCI_CONFIG_COMMAND_STATUS);
	val &= ~(PCI_COMMAND_BUSM | PCI_COMMAND_MEM | PCI_COMMAND_IO);
	val |= PCI_COMMAND_INT_DIS;
	pci_config_write32(pcidev, PCI_CONFIG_COMMAND_STATUS, val);

	for (bar = 0; bar < 6; bar++) {
		/* 2. detect BAR type and size
		 *
		 * We write 0xffffffff to the BAR register to get the configurable bits,
		 * which will remain set and not get cleared.
		 *
		 * The lower bits encode status information that should be filtered out.
		 */
		pci_config_write32(pcidev, PCI_CONFIG_BAR(bar), 0xffffffff);
		val = pci_config_read32(pcidev, PCI_CONFIG_BAR(bar));
		bar_is_io = ((val & PCI_BAR_IO) != 0);
		if (bar_is_io) {
			bar_mask = PCI_BAR_MASK_IO;
			bar_limit = 0xffff;
			bar_is_64bit = 0;
			bar_is_prefetch = 0;
		} else {
			bar_mask = PCI_BAR_MASK_MEM;
			bar_is_64bit = ((val & PCI_BAR_64BIT) != 0);
			if (bar_is_64bit) {
				bar_limit = 0xffffffffffffffffull;
			} else if ((val & PCI_BAR_BELOW1MB) != 0) {
				bar_limit = 0x000fffff;
			} else {
				bar_limit = 0xffffffff;
			}
			bar_is_prefetch = ((val & PCI_BAR_PREF) != 0);
		}
		if ((val & bar_mask) == 0) {
			continue;
		}

		bar_addr = val & bar_mask;
		if (bar_is_64bit) {
			pci_config_write32(pcidev, PCI_CONFIG_BAR(bar + 1), 0xffffffff);
			val = pci_config_read32(pcidev, PCI_CONFIG_BAR(bar + 1));
			bar_addr |= ((unsigned long long)val) << 32;
		} else {
			bar_addr |= 0xffffffff00000000ull;
		}
		bar_size = (unsigned long long)~bar_addr+1;

		/* 3. assign BAR address */
		if (bar_is_io) {
			bar_align = bar_size;
			if (bar_align < BAR_ALIGN_IO_FN) {
				bar_align = BAR_ALIGN_IO_FN;
			}
			bar_addr = ALIGN_UP(*assign_io, bar_align);
			*assign_io = bar_addr + bar_align;
		} else {
			bar_align = bar_size;
			if (bar_align < BAR_ALIGN_MEM) {
				bar_align = BAR_ALIGN_MEM;
			}
			bar_addr = ALIGN_UP(*assign_mem, bar_align);
			*assign_mem = bar_addr + bar_align;
		}
		(void)bar_limit;

		pci_config_write32(pcidev, PCI_CONFIG_BAR(bar), bar_addr);
		if (bar_is_64bit) {
			pci_config_write32(pcidev, PCI_CONFIG_BAR(bar + 1), bar_addr >> 32);
		}

		printk("  - bar %u: %s 0x%016llx, size 0x%016llx",
			   bar, bar_is_io ? "I/O" : "mem",
			   (unsigned long long)bar_addr,
			   (unsigned long long)bar_size);
		if (bar_is_64bit) {
			printk(" 64-bit");
		}
		if (bar_is_prefetch) {
			printk(" prefetch");
		}
		printk("\n");

		if (bar_is_64bit) {
			/* skip over next bar */
			bar++;
		}
	}

	/* 4. set cacheline size (ignored for PCI Express) */
	val = pci_config_read32(pcidev, PCI_CONFIG_HEADER_MISC);
	val &= 0x0000ff00;	/* keep latency timer */
	val |= CACHE_LINE_SIZE / sizeof(uint32_t);
	pci_config_write32(pcidev, PCI_CONFIG_HEADER_MISC, val);

	/* 5. enable mem and I/O space, keep interrupt and bus master disabled */
	val = pci_config_read32(pcidev, PCI_CONFIG_COMMAND_STATUS);
	val |= (PCI_COMMAND_MEM | PCI_COMMAND_IO);
	pci_config_write32(pcidev, PCI_CONFIG_COMMAND_STATUS, val);
}

static void pci_config_bridge(pcidev_t pcidev, uint32_t *assign_io, uint32_t *assign_mem)
{
	// FIXME: IMPLEMENT ME
	(void)pcidev;
	(void)assign_io;
	(void)assign_mem;
}

static void pci_setup_buses(void)
{
	uint32_t domain_max;
	uint32_t bus_max;
	uint32_t domain;
	uint32_t bus;
	uint32_t dev;
	uint32_t fn;
	pcidev_t pcidev;
	uint32_t val;

	/* we assigning BAR addresses from zero on */
	uint32_t assign_io = 0;
	uint32_t assign_mem = 0;

	domain_max = pci_topology_domain_num();
	for (domain = 0; domain < domain_max; domain++) {
		bus_max = pci_topology_domain_bus_num(domain);
		for (bus = 0; bus < bus_max; bus++) {
			/* we always probe all 32 devices on a bus */
			for (dev = 0; dev < 32; dev++) {
				/* multi-function devices must have at least fn 0 */
				for (fn = 0; fn < 8; fn++) {
					pcidev = PCIDEV(domain, bus, dev, fn);
					val = pci_config_read32(pcidev, PCI_CONFIG_VENDOR_DEVICE);
					if (val == 0xffffffff) {
						/* no device found */
						/* multi-function might not have devices in order */
						if (fn == 0) {
							break;
						} else {
							continue;
						}
					}

					printk("- PCI %04x:%02x:%02x.%1x ", domain, bus, dev, fn);
					printk("id=%04x:%04x ", val & 0xffff, val >> 16);

					val = pci_config_read32(pcidev, PCI_CONFIG_CLASS_REVID);
					printk("class=%04x, if=%02x, rev=%02x\n",
					       val >> 16, (val >> 8) & 0xff, val & 0xff);

					/* check header type */
					val = pci_config_read32(pcidev, PCI_CONFIG_HEADER_MISC);
					switch (PCI_HEADER_TYPE(val)) {
					case PCI_HEADER_TYPE_DEVICE:
						pci_config_dev(pcidev, &assign_io, &assign_mem);
						break;
					case PCI_HEADER_TYPE_BRIDGE:
						pci_config_bridge(pcidev, &assign_io, &assign_mem);
						break;
					case PCI_HEADER_TYPE_CARDBUS:
						/* cardbus bridge -- skipped */
					default:
						/* unknown type */
						break;
					}

					if ((fn == 0) && ((val & PCI_HEADER_TYPE_MULTI) == 0)) {
						/* not a multi-function device, no more devices */
						break;
					}
				}

				assign_io = ALIGN_UP(assign_io, BAR_ALIGN_IO_DEV);
			}
		}
	}
}

/** initializer, call at boot time */
__init void pci_setup_once(void)
{
	pci_setup_buses();
}

/** unimplemented KLDD -- the callback must just exist for the config tool */
err_t pci_setup_kldd(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4)
{
	(void)priv;
	(void)arg1;
	(void)arg2;
	(void)arg3;
	(void)arg4;

	return ENOSYS;
}
