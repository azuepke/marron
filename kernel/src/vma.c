/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * vma.c
 *
 * Virtual memory area handling.
 *
 * azuepke, 2021-09-22: initial
 */

#include <kernel.h>
#include <vma.h>
#include <adspace_types.h>
#include <memrq_types.h>
#include <part_types.h>
#include <marron/mapping.h>
#include <marron/cache_type.h>
#include <stdbool.h>
#include <vm.h>


/* forward declarations */

/** insert a VMA with a fixed address */
static struct vma *vma_insert_by_addr(struct adspace *adspace, addr_t addr, size_t size, int overmap);

/** insert a VMA with a given size address, preferredly at hint address */
static struct vma *vma_insert_by_size(struct adspace *adspace, addr_t hint, size_t size);

/** cut a hole (addr; addr+size) into the given VMA */
static struct vma *vma_cut(struct adspace *adspace, struct vma *vma, addr_t addr, size_t size);

/** try to merge a VMA to the left */
static struct vma *vma_try_merge_left(struct adspace *adspace, struct vma *vma);

/** split a given VMA into two VMAs at split_addr */
static struct vma *vma_split(struct adspace *adspace, struct vma *vma, addr_t split_addr);

/** split VMA common code */
static struct vma *vma_split_common(struct adspace *adspace, struct vma *vma, struct vma *vma2);


/** VMA node comparator -- compare by start address */
static int vma_cmp(const rbtree_node_t *a, const rbtree_node_t *b)
{
	const struct vma *vma_a = const_container_of(a, struct vma, vma_tree_node);
	const struct vma *vma_b = const_container_of(b, struct vma, vma_tree_node);

	if (vma_a->start < vma_b->start) {
		return -1;
	} else if (vma_a->start > vma_b->start) {
		return 1;
	} else {
		return 0;
	}
}

/* gap helpers for augmentation */

/** a node's gap to the next VMA on the right side */
static inline size_t vma_gap_to_next(const struct vma *vma)
{
	addr_t this_end, next_start;

	assert(vma != NULL);

	this_end = vma_end(vma);
	next_start = vma_next_start(vma);

	return next_start - this_end;
}

/** gap of the left/right child node */
static inline size_t vma_gap_child(const struct vma *vma, int dir)
{
	rbtree_node_t *child_node;
	struct vma *child_vma;

	assert(vma != NULL);
	assert((dir == 0) || (dir == 1));

	child_node = rbtree_node_child(&vma->vma_tree_node, dir);
	if (child_node == NULL) {
		return 0;	/* no gap */
	}

	child_vma = rbtree_entry(child_node, struct vma, vma_tree_node);
	return child_vma->largest_gap;
}

/** VMA node augmentation function */
static int vma_augment(rbtree_node_t *n)
{
	struct vma *vma = VMA_FROM_VMATREE(n);
	size_t gap, largest_gap;

	assert(n != NULL);

	largest_gap = vma_gap_to_next(vma);

	gap = vma_gap_child(vma, 0);
	if (gap > largest_gap) {
		largest_gap = gap;
	}

	gap = vma_gap_child(vma, 1);
	if (gap > largest_gap) {
		largest_gap = gap;
	}

	if (vma->largest_gap != largest_gap) {
		vma->largest_gap = largest_gap;
		return 1;
	}

	return 0;
}

/** initialize a VMA node */
static inline void vma_init(struct vma *vma)
{
	assert(vma != NULL);

	rbtree_node_init(&vma->vma_tree_node);
	vma->vma_next = NULL;
	vma->vma_prev = NULL;
	vma->start = 0;
	vma->size = 0;
	vma->largest_gap = 0;
	vma->type = VMA_GUARD;
	vma->curr_prot = PROT_NONE;
	vma->max_prot = PROT_NONE;
	vma->cache = CACHE_TYPE_UC;
	vma->flags = 0;
	vma->memrq_cfg = 0;
	vma->offset = 0;
}

/** initialize the VMA-specific part of all adspace objects at boot time */
__init void vma_init_all(void)
{
	for (unsigned int i = 0; i < part_num; i++) {
		struct adspace *adspace = &adspace_dyn[i];

		rbtree_root_init(&adspace->vma_tree_root);
		adspace->vma_first = NULL;
		adspace->hint_addr = 0;
		list_head_init(&adspace->vma_free_list_head);
	}
}

/** reset/free all VMAs in address space */
void vma_adspace_init(struct adspace *adspace)
{
	const struct part_cfg *cfg;
	struct vma *vma;

	assert(adspace != NULL);
	cfg = adspace->part->part_cfg;
	assert(cfg != NULL);
	assert(cfg->vma_num >= 1);

	/* prepare special NULL VMA as guard
	 * that reserves the first part of the address space
	 * from ARCH_USER_BASE to ARCH_USER_NULLGUARD.
	 */
	vma = &cfg->vma[0];
	vma_init(vma);
	vma->start = ARCH_USER_BASE;
	vma->size = ARCH_USER_NULLGUARD - ARCH_USER_BASE;
	vma->largest_gap = ARCH_USER_END - ARCH_USER_NULLGUARD;
	vma->type = VMA_NULL_GUARD;

	/* insert NULL VMA as first and only node in the tree */
	adspace->vma_first = vma;

	rbtree_root_init(&adspace->vma_tree_root);
	rbtree_insert_cmp(&adspace->vma_tree_root, &vma->vma_tree_node,
	                  vma_cmp, vma_augment);

	adspace->hint_addr = ARCH_USER_NULLGUARD;

	/* prepare remaining VMA as free VMAs */
	list_head_init(&adspace->vma_free_list_head);
	for (unsigned int i = 1; i < cfg->vma_num; i++) {
		vma = &cfg->vma[i];

		list_node_init(&vma->vma_free_list_node);
		list_insert_last(&adspace->vma_free_list_head, &vma->vma_free_list_node);
	}
}

/** destroy all VMAs in address space
 *
 * We must ensure that all VMAs of types VMA_MEM and VMA_ANON get properly unref'd.
 */
void vma_adspace_destroy(struct adspace *adspace)
{
	struct vma *vma;

	for (vma = adspace->vma_first; vma != NULL; vma = vma->vma_next) {
		if (VMA_TYPE_REFD(vma->type)) {
			/* unref affected pages */
			vm_callback_unref(adspace, vma->memrq_cfg, vma->start, vma_end(vma));
		}
	}
}

/** set mmap hint address */
void vma_adspace_set_hint(struct adspace *adspace, addr_t addr)
{
	assert(adspace != NULL);
	assert((addr & (PAGE_SIZE - 1)) == 0);

	adspace->hint_addr = addr;
}

/** allocate a VMA node */
static inline struct vma *vma_alloc(struct adspace *adspace)
{
	struct vma *vma;
	list_t *node;

	assert(adspace != NULL);

	node = list_remove_first(&adspace->vma_free_list_head);
	if (node == NULL) {
		return NULL;
	}

	vma = VMA_FROM_VMAFREELIST(node);
	return vma;
}

/** free a VMA node */
static inline void vma_free(struct adspace *adspace, struct vma *vma)
{
	assert(adspace != NULL);
	assert(vma != NULL);

	/* insert at head to keep it hot in caches */
	list_node_init(&vma->vma_free_list_node);
	list_insert_first(&adspace->vma_free_list_head, &vma->vma_free_list_node);
}

#if 0
/** iterate VMA tree by address (for calls of rbtree_find()) */
static inline int vma_iter_by_addr(struct vma *vma, addr_t addr)
{
	assert(vma != NULL);

	if (addr < vma->start) {
		return -1;
	}
	if (addr < vma_end(vma)) {
		return 0;
	}
	return 1;
}

/** find the VMA that covers the given address */
struct vma *vma_find_by_addr(struct adspace *adspace, addr_t addr)
{
	rbtree_node_t *node;
	struct vma *vma;

	assert(adspace != NULL);

	node = rbtree_find(&adspace->vma_tree_root,
	                   vma_iter_by_addr(VMA_FROM_VMATREE(__ITER__), addr));
	if (node == NULL) {
		return NULL;
	}

	vma = rbtree_entry(node, struct vma, vma_tree_node);
	return vma;
}
#endif

/** find the VMA exactly at or before/left of a given address */
struct vma *vma_find_exact_addr_or_before(struct adspace *adspace, addr_t addr)
{
	struct vma *vma;
	int dir;

	assert(adspace != NULL);
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(addr < ARCH_USER_END);

	/*
	 * We search by "addr" and try to find a VMA that is an exact match
	 * (addr in VMA), or that is to the left and right of addr.
	 */
	vma = NULL;
	rbtree_find(&adspace->vma_tree_root, ({
		vma = VMA_FROM_VMATREE(__ITER__);

		if (addr < vma->start) {
			/* go to the left */
			dir = -1;
		} else if (addr < vma_end(vma)) {
			/* exact match ... we do not need to continue searching */
			return vma;
		} else /* addr > end */ {
			/* go to the right */
			dir = 1;
		}
		dir;
	}));

	/* the VMA tree is never empty */
	assert(vma != NULL);

	/* the remaining VMA is either left or right of the target address */
	if (vma->start < addr) {
		/* left */
		return vma;
	} else {
		/* right */
		assert(vma->start > addr);
		assert(vma->vma_prev != NULL);
		assert(vma->vma_prev->start < addr);
		return vma->vma_prev;
	}
}

/** find a suitable VMA with a gap of given size (best fit strategy) */
static struct vma *vma_find_range_free(struct adspace *adspace, size_t size)
{
	size_t gap, best_gap, child0_gap, child1_gap;
	struct vma *best_fit;
	struct vma *vma;
	int dir;

	assert(adspace != NULL);
#if ARCH_USER_BASE > 0
	assert(hint >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(size <= ARCH_USER_END - ARCH_USER_BASE);
	assert(!rbtree_is_empty(&adspace->vma_tree_root));

	/*
	 * We search for a "best fit" gap in the virtual address space.
	 */
	best_fit = NULL;
	best_gap = -1ul;
	rbtree_find(&adspace->vma_tree_root, ({
		vma = VMA_FROM_VMATREE(__ITER__);

		gap = vma_gap_to_next(vma);
		if ((gap >= size) && (gap < best_gap)) {
			best_gap = gap;
			best_fit = vma;
		}

		/* strategy: follow smaller child gap as long as it fits */
		child0_gap = vma_gap_child(vma, 0);
		child1_gap = vma_gap_child(vma, 1);

		if (child0_gap <= child1_gap) {
			gap = child0_gap;
			dir = 0;
		} else {
			gap = child1_gap;
			dir = 1;
		}

		/* ... if it does not fit, flip direction */
		if (gap < size) {
			dir ^= 1;
		}
		dir;
	}));

	return best_fit;
}

/** common VMA insertion code */
static inline struct vma *vma_insert_common(struct adspace *adspace, struct vma *vma_prev, addr_t addr, size_t size)
{
	struct vma *vma, *vma_next;

	assert(adspace != NULL);
	assert(vma_prev != NULL);

	vma = vma_alloc(adspace);
	if (vma == NULL) {
		return NULL;
	}

	vma_init(vma);
	vma->start = addr;
	vma->size = size;

	/* insert into list first (rbtree augmentation relies on this) */
	vma->vma_prev = vma_prev;
	vma_next = vma_prev->vma_next;
	vma_prev->vma_next = vma;

	vma->vma_next = vma_next;
	if (vma_next != NULL) {
		assert(vma_next->vma_prev == vma_prev);
		vma_next->vma_prev = vma;
	}

	rbtree_insert_cmp(&adspace->vma_tree_root, &vma->vma_tree_node,
	                  vma_cmp, vma_augment);

	return vma;
}

/** insert a VMA with a fixed address
 *
 * the requested memory area must be free, unless the overmap flag is set,
 * then any previous VMAs in that area will be cut and overmapped.
 */
static struct vma *vma_insert_by_addr(struct adspace *adspace, addr_t addr, size_t size, int overmap)
{
	struct vma *vma_prev, *vma_next;

	assert(adspace != NULL);
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);

	vma_prev = vma_find_exact_addr_or_before(adspace, addr);
	assert(vma_prev->start <= addr);
	if (addr < vma_end(vma_prev)) {
		/* overlaps with prev */
		if (!overmap) {
			return NULL;
		}

		if (vma_prev->type == VMA_NULL_GUARD) {
			/* the NULL guard cannot be overmapped */
			return NULL;
		}

		vma_prev = vma_cut(adspace, vma_prev, addr, size);
		if (vma_prev == NULL) {
			/* out of memory while splitting the VMA */
			return NULL;
		}
		/* corner case: the cut VMA might now be on the right of the hole */
		if (vma_prev->start > addr) {
			/* use node on the left of it */
			vma_prev = vma_prev->vma_prev;
			assert(vma_prev != NULL);
		}
		assert(vma_end(vma_prev) <= addr);
	}

	while (addr + size > vma_next_start(vma_prev)) {
		/* overlaps with next */
		if (!overmap) {
			return NULL;
		}

		vma_next = vma_prev->vma_next;
		assert(vma_next != NULL);
		vma_next = vma_cut(adspace, vma_next, addr, size);
		/* this will never fail: splitting a VMA cannot happen here
		 * (we cut a VMA on the left side or completely remove it),
		 * and the "next" node is never the NULL guard.
		 */
		assert(vma_next != NULL);
		(void)vma_next;
	}

	return vma_insert_common(adspace, vma_prev, addr, size);
}

/** insert a VMA with a given size address, preferredly at hint address */
static struct vma *vma_insert_by_size(struct adspace *adspace, addr_t hint, size_t size)
{
	struct vma *vma, *vma_prev;
	addr_t next_start;
	addr_t addr;

	assert(adspace != NULL);
#if ARCH_USER_BASE > 0
	assert(hint >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(hint < hint + size);
	assert(hint + size <= ARCH_USER_END);

	/*
	 * allocation strategy:
	 * - 1. if no hint is given, use hint_addr
	 * - 2. try to use the exact hinted address
	 * - 3. try to use a slightly larger address
	 * - 4. search for free space anywhere if it does not fit before
	 */
	if (hint == 0) {
		hint = adspace->hint_addr;
	}

	vma_prev = vma_find_exact_addr_or_before(adspace, hint);
	assert(vma_prev->start <= hint);
	if (hint < vma_end(vma_prev)) {
		/* overlaps with prev */
		hint = vma_end(vma_prev);
	}

	next_start = vma_next_start(vma_prev);
	if (hint + size <= next_start) {
		/* fits */
		addr = hint;
		assert(addr != 0);
	} else {
		/* overlaps with next, search for a suitable gap */
		vma_prev = vma_find_range_free(adspace, size);
		if (vma_prev == NULL) {
			/* out of memory while splitting the VMA */
			return NULL;
		}
		addr = vma_end(vma_prev);
		assert(addr + size <= vma_next_start(vma_prev));
	}

	vma = vma_insert_common(adspace, vma_prev, addr, size);

	adspace->hint_addr = addr + size;

	return vma;
}

/** map (insert) a new VMA at or near the given memory area
 *
 * the allocation strategy follows mmap:
 * - fixed == 0:
 *   the address is taken as a hint
 * - fixed == 1 && overmap == 0:
 *   the address is fixed && the requested memory area must be free
 * - fixed == 1 && overmap == 1:
 *   the address is fixed, any previous VMAs in the area will be cut and overmapped
 *
 * returns new VMA or NULL on failure (out of memory)
 */
struct vma *vma_create(struct adspace *adspace, addr_t addr, size_t size, int fixed, int overmap)
{
	struct vma *vma;

	assert(adspace != NULL);
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);

	if (fixed == 0) {
		vma = vma_insert_by_size(adspace, addr, size);
	} else /* fixed == 1 */ {
		vma = vma_insert_by_addr(adspace, addr, size, overmap);
	}

	return vma;
}

/** set type and attributes of VMA */
void vma_set_type(struct vma *vma, unsigned int vma_type, unsigned int prot, unsigned int max_prot, unsigned int cache, const struct memrq_cfg *m, phys_addr_t offset)
{
	assert(vma != NULL);
	assert(vma_type <= VMA_ANON);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);
	assert((max_prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);
	assert(cache <= CACHE_TYPE_WA);
	assert((offset & (PAGE_SIZE - 1)) == 0);

	assert(vma->type == VMA_GUARD);
	vma->type = vma_type;
	vma->curr_prot = prot;
	vma->max_prot = max_prot;
	vma->cache = cache;
	vma->flags = 0;
	vma->memrq_cfg = m;
	vma->offset = offset;
}

/** finalizing mapping of a new VMA
 *
 * called as finishing step after populating a mapping
 *
 * returns the given error code (pass-through)
 */
err_t vma_finalize(struct adspace *adspace, struct vma *vma, err_t err)
{
	assert(adspace != NULL);
	assert(vma != NULL);

	if (err == EOK) {
		/* try to merge to the left and to the right */
		vma = vma_try_merge_left(adspace, vma);
		assert(vma != NULL);
		if (vma->vma_next != NULL) {
			(void)vma_try_merge_left(adspace, vma->vma_next);
		}
	} else {
		/* mapping failed: undo any partial mappings and remove VMA */
		vma_cut(adspace, vma, vma->start, vma->size);
	}

	return err;
}

/** merge to two adjacent VMAs */
static inline struct vma *vma_merge(struct adspace *adspace, struct vma *vma, struct vma *vma2)
{
	struct vma *vma_next;

	assert(adspace != NULL);
	assert(vma != NULL);
	assert(vma->type != VMA_NULL_GUARD);	/* never the NULL guard */
	assert(vma->vma_prev != NULL);	/* never the NULL guard */
	assert(vma2 != NULL);
	assert(vma->vma_next == vma2);
	assert(vma2->vma_prev == vma);

	/* merge */
	vma->size += vma2->size;

	/* remove from linked list */
	vma_next = vma2->vma_next;
	vma->vma_next = vma_next;
	if (vma_next != NULL) {
		assert(vma_next->vma_prev == vma2);
		vma_next->vma_prev = vma;
	}

	(void)rbtree_remove_core(&adspace->vma_tree_root, &vma2->vma_tree_node,
	                         vma_augment);

	vma_free(adspace, vma2);

	return vma;
}

/** try to merge a VMA to the left */
static struct vma *vma_try_merge_left(struct adspace *adspace, struct vma *vma)
{
	struct vma *vma_prev;
	unsigned int merge;

	assert(adspace != NULL);
	assert(vma != NULL);
	assert(vma->type != VMA_NULL_GUARD);	/* never the NULL guard */
	assert(vma->vma_prev != NULL);	/* never the NULL guard */

	vma_prev = vma->vma_prev;
	if (    (vma_prev->type      == vma->type) &&
	        (vma_prev->curr_prot == vma->curr_prot) &&
	        (vma_prev->max_prot  == vma->max_prot) &&
	        (vma_prev->cache     == vma->cache) &&
	        (vma_end(vma_prev)   == vma->start)) {
		/* adjacent VMAs of same type */
		/* memrq_cfg can be a NULL pointer in both cases */
		merge = (vma_prev->memrq_cfg == vma->memrq_cfg);
		if (VMA_TYPE_OFFSET(vma->type)) {
			merge |= (vma_prev->offset + vma_prev->size == vma->offset);
		}
		if (merge) {
			vma = vma_merge(adspace, vma_prev, vma);
			assert(vma != NULL);
			/* vma_prev will be the new VMA */
		}
	}

	return vma;
}

/** fixup function when removing a VMA
 *
 * the callback is invoked on the VMA before any modification
 *
 * vma->start..vma_end(vma) will be removed
 */
static inline void vma_fixup_cut_full(struct adspace *adspace, struct vma *vma)
{
	assert(adspace != NULL);
	assert(vma != NULL);

	(void)adspace;

	if (VMA_TYPE_REFD(vma->type)) {
		/* unref affected pages */
		vm_callback_unref(adspace, vma->memrq_cfg, vma->start, vma_end(vma));
	}
}

/** fixup function when cutting/resizing a VMA on the left side
 *
 * the callback is invoked on the VMA before any modification
 *
 * vma->start..cut_addr will be removed on the left side
 */
static inline void vma_fixup_cut_left(struct adspace *adspace, struct vma *vma, addr_t cut_addr)
{
	size_t cut_size;

	assert(adspace != NULL);
	assert(vma != NULL);
	assert(cut_addr > vma->start);
	assert(cut_addr < vma_end(vma));

	(void)adspace;

	if (VMA_TYPE_OFFSET(vma->type)) {
		cut_size = cut_addr - vma->start;
		vma->offset += cut_size;
	}

	if (VMA_TYPE_REFD(vma->type)) {
		/* unref affected pages */
		vm_callback_unref(adspace, vma->memrq_cfg, vma->start, cut_addr);
	}
}

/** fixup function when cutting/resizing a VMA on the right side
 *
 * the callback is invoked on the VMA before any modification
 *
 * cut_addr..end_addr will be removed on the right side
 */
static inline void vma_fixup_cut_right(struct adspace *adspace, struct vma *vma, addr_t cut_addr, addr_t end_addr)
{
	assert(adspace != NULL);
	assert(vma != NULL);
	assert(cut_addr > vma->start);
	assert(cut_addr < vma_end(vma));

	(void)adspace;

	if (VMA_TYPE_REFD(vma->type)) {
		/* unref affected pages */
		vm_callback_unref(adspace, vma->memrq_cfg, cut_addr, end_addr);
	}
}

/** fixup function when split a VMA somewhere inside
 *
 * the callback is invoked on the VMAs before any modification.
 * vma2 is a duplicate of vma.
 * later, vma will cover the range start..split_addr,
 * and   vma2 will cover the range split_addr..end.
 */
static inline void vma_fixup_split(struct adspace *adspace, struct vma *vma, struct vma *vma2, addr_t split_addr)
{
	size_t split_offset;

	assert(adspace != NULL);
	assert(vma != NULL);
	assert(vma2 != NULL);
	assert(vma->start == vma2->start);
	assert(vma->size == vma2->size);
	assert(split_addr > vma->start);
	assert(split_addr < vma_end(vma));

	(void)adspace;

	if (VMA_TYPE_OFFSET(vma->type)) {
		split_offset = split_addr - vma->start;
		vma2->offset += split_offset;
	}
}

/** cut a hole (addr; addr+size) into the given VMA
 *
 * four (five) possible scenarios:
 * - hole overlaps VMA on the left side: cut VMA on left (adjust start and size)
 * - hole overlaps VMA on the right side: cut VMA on right (adjust size)
 * - hole is larger than VMA and overlaps VMA on both sides: VMA removed
 * - hole is smaller than VMA and fully inside VMA: VMA is split into two
 * - (hole and VMA do not overlap: nothing happens, and not called in this case)
 *
 * returns:
 * - NULL on an error (out of memory when cutting a VMA in the middle)
 * - vma if vma is cut
 * - vma->vma_prev if vma was removed
 *
 * note:
 * - vma must never be the NULL guard
 */
static struct vma *vma_cut(struct adspace *adspace, struct vma *vma, addr_t addr, size_t size)
{
	struct vma *vma_prev, *vma_next, *vma2;
	addr_t cut_addr, split_addr, orig_end;
	size_t cut_size;

	assert(adspace != NULL);
	assert(vma != NULL);
	assert(vma->type != VMA_NULL_GUARD);	/* never the NULL guard */
	assert(vma->vma_prev != NULL);	/* never the NULL guard */
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);

	if ((addr <= vma->start) && (addr + size >= vma_end(vma))) {
		/* hole overlaps VMA on both sides: remove VMA */
		/* cannot fail */
		vma_fixup_cut_full(adspace, vma);
		vm_callback_unmap(adspace, vma->start, vma->size);

		/* remove from linked list */
		vma_prev = vma->vma_prev;
		vma_next = vma->vma_next;
		vma_prev->vma_next = vma_next;
		if (vma_next != NULL) {
			vma_next->vma_prev = vma_prev;
		}

		(void)rbtree_remove_core(&adspace->vma_tree_root, &vma->vma_tree_node,
		                         vma_augment);

		vma_free(adspace, vma);

		return vma_prev;
	} else if ((addr <= vma->start) && (addr + size < vma_end(vma))) {
		/* hole overlaps VMA on the left side: cut on left side */
		/* cannot fail */
		cut_addr = addr + size;
		vma_fixup_cut_left(adspace, vma, cut_addr);
		vm_callback_unmap(adspace, vma->start, cut_addr - vma->start);

		cut_size = cut_addr - vma->start;
		assert(cut_size > 0);
		assert(cut_size < vma->size);
		vma->start += cut_size;
		vma->size -= cut_size;

		rbtree_update_augment(&adspace->vma_tree_root, &vma->vma_tree_node,
		                      vma_augment);

		return vma;
	} else if ((addr > vma->start) && (addr + size >= vma_end(vma))) {
		/* hole overlaps VMA on the right side: cut on right side */
		/* cannot fail */
		cut_addr = addr;
		vma_fixup_cut_right(adspace, vma, cut_addr, vma_end(vma));
		vm_callback_unmap(adspace, cut_addr, vma_end(vma) - cut_addr);

		cut_size = vma_end(vma) - cut_addr;
		assert(cut_size > 0);
		assert(cut_size < vma->size);
		assert(vma->start < cut_addr);
		vma->size -= cut_size;

		rbtree_update_augment(&adspace->vma_tree_root, &vma->vma_tree_node,
		                      vma_augment);

		return vma;
	} else {
		assert((addr > vma->start) && (addr + size < vma_end(vma)));
		/* hole inside VMA: split VMA */
		/* duplicate VMA */
		vma2 = vma_alloc(adspace);
		if (vma2 == NULL) {
			return NULL;
		}
		*vma2 = *vma;
		/* cannot fail from here on */

		/* logically: split VMA at end of hole, then cut left VMA on right side at start of hole */
		split_addr = addr + size;
		vma_fixup_split(adspace, vma, vma2, split_addr);
		cut_addr = addr;
		vma_fixup_cut_right(adspace, vma, cut_addr, split_addr);

		vm_callback_unmap(adspace, addr, size);

		/* technically: first cut first VMA on right side */
		orig_end = vma_end(vma);
		cut_size = orig_end - cut_addr;
		assert(cut_size > 0);
		assert(cut_size < vma->size);
		assert(vma->start < cut_addr);
		vma->size -= cut_size;

		vma2->start = split_addr;
		vma2->size = orig_end - split_addr;

		/* continue at common code */
		return vma_split_common(adspace, vma, vma2);
	}
}

/* unmap a single VMA
 *
 * returns:
 * - NULL on an error (out of memory when cutting a VMA in the middle)
 * - vma as left VMA (a new VMA added to the right)
 *
 * note:
 * - the VMA must be related to the hole
 */
struct vma *vma_unmap(struct adspace *adspace, struct vma *vma, addr_t addr, size_t size)
{
	assert(adspace != NULL);
	assert(vma != NULL);
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);

	/* we silently ignore any attempt to unmap the NULL guard */
	if (vma->type != VMA_NULL_GUARD) {
		vma = vma_cut(adspace, vma, addr, size);
		if (vma == NULL) {
			/* out of memory while splitting the VMA */
			return NULL;
		}
		/* corner case: the cut VMA might now be on the right of the hole */
		if (vma->start > addr) {
			/* use node on the left of it */
			vma = vma->vma_prev;
			assert(vma != NULL);
		}
	}

	return vma;
}

/** split a given VMA into two VMAs at split_addr
 *
 * returns:
 * - NULL on an error (out of memory when cutting a VMA in the middle)
 * - vma as left VMA (a new VMA added to the right)
 *
 * note:
 * - vma must never be the NULL guard
 */
static struct vma *vma_split(struct adspace *adspace, struct vma *vma, addr_t split_addr)
{
	struct vma *vma2;
	size_t split_offset;

	assert(adspace != NULL);
	assert(vma != NULL);
	assert(vma->type != VMA_NULL_GUARD);	/* never the NULL guard */
	assert(vma->vma_prev != NULL);	/* never the NULL guard */
	assert(split_addr > vma->start);
	assert(split_addr < vma_end(vma));

	/* duplicate VMA */
	vma2 = vma_alloc(adspace);
	if (vma2 == NULL) {
		return NULL;
	}
	*vma2 = *vma;

	vma_fixup_split(adspace, vma, vma2, split_addr);

	/* first cut first VMA on right side */
	split_offset = split_addr - vma->start;
	assert(split_offset > 0);
	assert(split_offset < vma->size);
	assert(vma->start < split_addr);
	vma->size = split_offset;

	vma2->start += split_offset;
	vma2->size -= split_offset;

	/* continue below */
	return vma_split_common(adspace, vma, vma2);
}

/** split VMA common code
 *
 * expects both VMAs to be "in shape" with correct sizes etc
 * updates augmentation for first VMA before inserting second VMA
 */
static struct vma *vma_split_common(struct adspace *adspace, struct vma *vma, struct vma *vma2)
{
	assert(adspace != NULL);
	assert(vma != NULL);
	assert(vma2 != NULL);

	rbtree_update_augment(&adspace->vma_tree_root, &vma->vma_tree_node,
	                      vma_augment);

	/* then insert second VMA into list and then into rbtree */
	vma->vma_next = vma2;
	vma2->vma_prev = vma;
	if (vma2->vma_next != NULL) {
		assert(vma2->vma_next->vma_prev == vma);
		vma2->vma_next->vma_prev = vma2;
	}

	rbtree_insert_cmp(&adspace->vma_tree_root, &vma2->vma_tree_node,
	                  vma_cmp, vma_augment);

	return vma;
}

/** split VMA for protection change in area (addr; addr+size) in the given VMA
 *
 * four (five) possible scenarios:
 * - area overlaps VMA on the left side: split VMA into two
 * - area overlaps VMA on the right side: split VMA into two
 * - area is larger than VMA and overlaps VMA on both sides: no splitting
 * - area is smaller than VMA and fully inside VMA: VMA is split into three
 * - (area and VMA do not overlap: nothing happens, but not called in this case)
 *
 * returns:
 * - NULL on an error (out of memory when splitting the VMA)
 * - left-most vma in case of splitting
 * - vma if vma is unchanged
 *
 * note:
 * - vma must never be the NULL guard
 */
static struct vma *vma_maybe_split(struct adspace *adspace, struct vma *vma, addr_t addr, size_t size)
{
	struct vma *vma_tmp, *vma_left, *vma_middle;
	size_t split_addr;

	assert(adspace != NULL);
	assert(vma != NULL);
	assert(vma->type != VMA_NULL_GUARD);	/* never the NULL guard */
	assert(vma->vma_prev != NULL);	/* never the NULL guard */
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);

	if ((addr <= vma->start) && (addr + size >= vma_end(vma))) {
		/* area overlaps VMA on both sides: no splitting */
		return vma;
	} else if ((addr <= vma->start) && (addr + size < vma_end(vma))) {
		/* area overlaps VMA on the left side: split VMA into two */
		split_addr = addr + size;
		return vma_split(adspace, vma, split_addr);
	} else if ((addr > vma->start) && (addr + size >= vma_end(vma))) {
		/* area overlaps VMA on the right side: split VMA into two */
		split_addr = addr;
		return vma_split(adspace, vma, split_addr);
	} else {
		assert((addr > vma->start) && (addr + size < vma_end(vma)));
		/* area inside VMA: split VMA into three */

		/* we first allocate a temp VMA for the second allocation to succeed */
		vma_tmp = vma_alloc(adspace);
		if (vma_tmp == NULL) {
			return NULL;
		}

		/* then we split the VMA at the left split point */
		split_addr = addr;
		vma_left = vma_split(adspace, vma, split_addr);

		/* always give temp VMA back */
		vma_free(adspace, vma_tmp);

		/* splitting the first VMA failed */
		if (vma_left == NULL) {
			return NULL;
		}

		/* we now have two VMAs; split the right one at the right split point */
		split_addr = addr + size;

		vma_middle = vma_left->vma_next;
		assert(vma_middle != NULL);
		assert(split_addr > vma_middle->start);
		assert(split_addr < vma_end(vma_middle));
		vma_middle = vma_split(adspace, vma_middle, split_addr);
		assert(vma_middle != NULL);
		return vma_left;
	}
}

/** change protection of a a single VMA (mprotect())
 *
 * returns:
 * - NULL on an error (out of memory when cutting a VMA in the middle)
 * - vma as left VMA (a new VMA added to the right after splitting)
 *
 * note:
 * - the VMA must be related to the affected area
 */
struct vma *vma_protect(struct adspace *adspace, struct vma *vma, addr_t addr, size_t size, unsigned int prot)
{
	assert(adspace != NULL);
	assert(vma != NULL);
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);

	if (vma->curr_prot != prot) {
		vma = vma_maybe_split(adspace, vma, addr, size);
		if (vma == NULL) {
			/* out of memory while splitting the VMA */
			return NULL;
		}

		/* after splitting, check VMA again (might no longer overlapping) */
		if ((vma->start >= addr) && (vma_end(vma) <= addr + size)) {
			/* change protection */
			vma->curr_prot = prot & 0xff;

			/* update pagetables */
			vm_callback_protchange(adspace, vma->start, vma->size, prot);
		}
	}

	/* try to merge to the left */
	vma = vma_try_merge_left(adspace, vma);
	assert(vma != NULL);
	/* in case of merge, we still have an overlapping VMA */

	return vma;
}

////////////////////////////////////////////////////////////////////////////////

#ifndef NDEBUG

#include <adspace.h>

static inline void vma_dump_once(struct vma *vma)
{
	assert(vma != NULL);

	printk("- VMA: %08zx--%08zx (%08zx)", vma->start, vma->start + vma->size, vma->size);
	printk(" type:%s", &"NULL\0GRD \0IO  \0MEM \0ANON\0UNK5\0UNK6\0UNK7"[(vma->type % 8) * 5]);
	printk("\n");

	if (!VMA_TYPE_GUARD(vma->type)) {
		printk("  prot:%c%c%c (%c%c%c)", (vma->curr_prot & PROT_READ) != 0 ? 'r' : '-',
		                                 (vma->curr_prot & PROT_WRITE) != 0 ? 'w' : '-',
		                                 (vma->curr_prot & PROT_EXEC) != 0 ? 'x' : '-',
		                                 (vma->max_prot & PROT_READ) != 0 ? 'r' : '-',
		                                 (vma->max_prot & PROT_WRITE) != 0 ? 'w' : '-',
		                                 (vma->max_prot & PROT_EXEC) != 0 ? 'x' : '-');
		printk(" cache:%s", &"UC \0DEV\0_2 \0_3 \0WC \0WT \0WB \0WA "[(vma->cache % 8) * 4]);
		printk(" flags:%02x", vma->flags);

		if (VMA_TYPE_REFD(vma->type)) {
			printk(" memrq:%s", vma->memrq_cfg->name);
		}
		if (vma->type == VMA_IO) {
			printk(" phys:%016llx", (unsigned long long)vma->offset);
		} else if (VMA_TYPE_OFFSET(vma->type)) {
			printk(" offset:%016llx", (unsigned long long)vma->offset);
		}
		printk("\n");
	}
}

static inline void vma_dump(struct adspace *adspace, const char *msg)
{
	struct vma *vma;

	printk("\n* dump VMAs %s\n", msg != NULL ? msg : "");
	for (vma = adspace->vma_first; vma != NULL; vma = vma->vma_next) {
		vma_dump_once(vma);
	}
	printk("\n");
}

static inline struct vma *vma_index(struct adspace *adspace, unsigned int index)
{
	struct vma *vma;
	unsigned int i;

	i = 0;
	for (vma = adspace->vma_first; vma != NULL; vma = vma->vma_next) {
		if (i == index) {
			return vma;
		}
		i++;
	}

	return NULL;
}

static inline unsigned int vma_count(struct adspace *adspace)
{
	struct vma *vma;
	unsigned int i;

	i = 0;
	for (vma = adspace->vma_first; vma != NULL; vma = vma->vma_next) {
		i++;
	}

	return i;
}

#define assert_vma(adspace, index, xaddr, xsize) do { \
		struct vma *_vma = vma_index((adspace), (index));	\
		assert(_vma != NULL);	\
		assert(_vma->start == (xaddr));	\
		assert(_vma->size == (xsize));	\
	} while (0)

#define assert_vma_count(adspace, num) assert(vma_count(adspace) == (num))

static void vma_test(struct adspace *adspace)
{
	addr_t base = ARCH_USER_BASE + ARCH_USER_NULLGUARD;
	uint32_t kmem_pagetables_free_before;
	struct vma *vma;
	err_t err;

	printk("***** VMA self-test BEGIN *****\n");

	kmem_pagetables_free_before = adspace->part->kmem_pagetables.free;

	assert_vma_count(adspace, 1);
	assert_vma(adspace, 0, ARCH_USER_BASE, ARCH_USER_NULLGUARD);

	printk("- invalid mappings\n");
	vma = vma_create(adspace, 0, PAGE_SIZE, true, true);
	assert(vma == NULL);
	assert_vma_count(adspace, 1);

	vma = vma_create(adspace, base - 1*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma == NULL);
	assert_vma_count(adspace, 1);

	printk("- valid mappings\n");
	vma = vma_create(adspace, base + 0*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 2);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);

	/* null g---- */

	printk("- merging of guards\n");
	vma = vma_create(adspace, base + 1*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 2*PAGE_SIZE);

	/* null gg--- */

	vma = vma_create(adspace, base + 3*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 2*PAGE_SIZE);
	assert_vma(adspace, 2, base + 3*PAGE_SIZE, 1*PAGE_SIZE);

	/* null gg-g- */

	vma = vma_create(adspace, base + 2*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 4);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 4*PAGE_SIZE);

	/* null gggg- */

	vma = vma_create(adspace, base + 4*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	/* null ggggg */

	printk("- unmap cutting right\n");
	err = vm_unmap(adspace, base + 4*PAGE_SIZE, 2*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 4*PAGE_SIZE);

	/* null gggg- */

	printk("- unmap cutting left\n");
	err = vm_unmap(adspace, base - 1*PAGE_SIZE, 2*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 1*PAGE_SIZE, 3*PAGE_SIZE);

	/* null -ggg- */

	printk("- unmap cutting hole\n");
	err = vm_unmap(adspace, base + 2*PAGE_SIZE, 1*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 1*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 3*PAGE_SIZE, 1*PAGE_SIZE);

	/* null -g-g- */

	printk("- unmap overlapping\n");
	err = vm_unmap(adspace, base - 1*PAGE_SIZE, 7*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 1);
	assert_vma(adspace, 0, ARCH_USER_BASE, ARCH_USER_NULLGUARD);

	/* null ----- */

	printk("- unmap beginning\n");

	vma = vma_create(adspace, base + 0*PAGE_SIZE, 5*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 2);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	/* null ggggg */

	err = vm_unmap(adspace, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 1*PAGE_SIZE, 4*PAGE_SIZE);

	/* null -gggg */

	err = vm_unmap(adspace, base + 0*PAGE_SIZE, 2*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 2*PAGE_SIZE, 3*PAGE_SIZE);

	/* null --ggg */

	err = vm_unmap(adspace, base + 0*PAGE_SIZE, 3*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 3*PAGE_SIZE, 2*PAGE_SIZE);

	/* null ---gg */

	err = vm_unmap(adspace, base + 0*PAGE_SIZE, 5*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 1);

	/* null ----- */

	printk("- auto placement\n");

	vma = vma_create(adspace, base + 0*PAGE_SIZE, 1*PAGE_SIZE, true, false);
	assert(vma != NULL);
	assert_vma_count(adspace, 2);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);

	/* null g---- */

	/* suggest one page behind, but not fixed mapping */
	vma = vma_create(adspace, base + 1*PAGE_SIZE, 1*PAGE_SIZE, false, false);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 2*PAGE_SIZE);

	/* null gg--- */

	/* null address -> after last page */
	vma = vma_create(adspace, 0, 1*PAGE_SIZE, false, false);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 3*PAGE_SIZE);

	/* null ggg-- */

	/* again null address -> after last page */
	vma = vma_create(adspace, 0, 1*PAGE_SIZE, false, false);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 4*PAGE_SIZE);

	/* null gggg- */

	printk("- overmapping\n");

	/* overmapping flag cleared ->  expected to fail */
	vma = vma_create(adspace, base + 2*PAGE_SIZE, 3*PAGE_SIZE, true, false);
	assert(vma == NULL);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 4*PAGE_SIZE);

	/* overmapping flag set */
	vma = vma_create(adspace, base + 2*PAGE_SIZE, 3*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 2*PAGE_SIZE);
	assert_vma(adspace, 2, base + 2*PAGE_SIZE, 3*PAGE_SIZE);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	/* null ggggg */

	/* overmapping first page */
	vma = vma_create(adspace, base + 0*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 4*PAGE_SIZE);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	/* overmapping second page */
	vma = vma_create(adspace, base + 1*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 4);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 3, base + 2*PAGE_SIZE, 3*PAGE_SIZE);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	/* overmapping last page */
	vma = vma_create(adspace, base + 4*PAGE_SIZE, 1*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 4*PAGE_SIZE);
	assert_vma(adspace, 2, base + 4*PAGE_SIZE, 1*PAGE_SIZE);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	/* unmapping last page */
	err = vm_unmap(adspace, base + 4*PAGE_SIZE, 1*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 4*PAGE_SIZE);

	/* null gggg- */

	/* overmapping last two pages */
	vma = vma_create(adspace, base + 3*PAGE_SIZE, 2*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 2, base + 3*PAGE_SIZE, 2*PAGE_SIZE);
	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	/* overmapping covers NULL guard -> expected to fail */
	vma = vma_create(adspace, base - 1*PAGE_SIZE, 6*PAGE_SIZE, true, true);
	assert(vma == NULL);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 5*PAGE_SIZE);

	printk("- mapping I/O memory\n");

	vma = vma_create(adspace, base + 1*PAGE_SIZE, 3*PAGE_SIZE, true, true);
	assert(vma != NULL);
	assert_vma_count(adspace, 4);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 3, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	vma_set_type(vma, VMA_IO, PROT_READ|PROT_WRITE, PROT_READ|PROT_WRITE, CACHE_TYPE_UC, NULL, 0);

	err = arch_adspace_map(adspace, vma->start, vma->size,
	                       vma->offset, vma->curr_prot, vma->cache);
	assert(err == EOK);

	err = vma_finalize(adspace, vma, EOK);
	assert(err == EOK);
	assert_vma_count(adspace, 4);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 3, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null gwwwg */

	printk("- change permissions 1\n");

	err = vm_protect(adspace, base + 2*PAGE_SIZE, 1*PAGE_SIZE, PROT_READ);
	assert(err == EOK);

	assert_vma_count(adspace, 6);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 3, base + 2*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 4, base + 3*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 5, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null gwrwg */

	printk("- change permissions 2\n");

	err = vm_protect(adspace, base + 1*PAGE_SIZE, 3*PAGE_SIZE, PROT_READ);
	assert(err == EOK);

	assert_vma_count(adspace, 4);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 3, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null grrrg */

	printk("- change permissions 3\n");

	err = vm_protect(adspace, base + 0*PAGE_SIZE, 5*PAGE_SIZE, PROT_READ|PROT_WRITE);
	assert(err == EACCES);
	err = vm_protect(adspace, base + 0*PAGE_SIZE, 4*PAGE_SIZE, PROT_READ|PROT_WRITE);
	assert(err == EACCES);
	err = vm_protect(adspace, base + 1*PAGE_SIZE, 4*PAGE_SIZE, PROT_READ|PROT_WRITE);
	assert(err == EACCES);
	err = vm_protect(adspace, base + 1*PAGE_SIZE, 3*PAGE_SIZE, PROT_READ|PROT_WRITE);
	assert(err == EOK);

	assert_vma_count(adspace, 4);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 3, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null gwwwg */

	printk("- change permissions 4\n");

	err = vm_protect(adspace, base + 0*PAGE_SIZE, 5*PAGE_SIZE, PROT_READ|PROT_EXEC);
	assert(err == EACCES);

	err = vm_protect(adspace, base + 1*PAGE_SIZE, 3*PAGE_SIZE, PROT_NONE);
	assert(err == EOK);

	assert_vma_count(adspace, 4);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 3, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null g000g */

	printk("- change permissions 5\n");

	err = vm_protect(adspace, base + 1*PAGE_SIZE, 1*PAGE_SIZE, PROT_READ);
	assert(err == EOK);

	assert_vma_count(adspace, 5);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 3, base + 2*PAGE_SIZE, 2*PAGE_SIZE);
	assert_vma(adspace, 4, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null gr00g */

	err = vm_protect(adspace, base + 3*PAGE_SIZE, 1*PAGE_SIZE, PROT_READ);
	assert(err == EOK);

	assert_vma_count(adspace, 6);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 3, base + 2*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 4, base + 3*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 5, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null gr0rg */

	err = vm_protect(adspace, base + 2*PAGE_SIZE, 1*PAGE_SIZE, PROT_READ);
	assert(err == EOK);

	assert_vma_count(adspace, 4);
	assert_vma(adspace, 1, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert_vma(adspace, 2, base + 1*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 3, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null grrrg */

	printk("- change permissions boundary checks\n");

	err = vm_unmap(adspace, base + 0*PAGE_SIZE, 1*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 1*PAGE_SIZE, 3*PAGE_SIZE);
	assert_vma(adspace, 2, base + 4*PAGE_SIZE, 1*PAGE_SIZE);

	/* null -rrrg */

	err = vm_protect(adspace, base + 0*PAGE_SIZE, 5*PAGE_SIZE, PROT_READ);
	assert(err == ENOMEM);

	err = vm_protect(adspace, base + 1*PAGE_SIZE, 4*PAGE_SIZE, PROT_READ);
	assert(err == EACCES);

	err = vm_protect(adspace, base + 1*PAGE_SIZE, 3*PAGE_SIZE, PROT_READ);
	assert(err == EOK);
	assert_vma_count(adspace, 3);
	assert_vma(adspace, 1, base + 1*PAGE_SIZE, 3*PAGE_SIZE);

	/* null -rrrg */

	err = vm_unmap(adspace, base + 4*PAGE_SIZE, 1*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 1*PAGE_SIZE, 3*PAGE_SIZE);

	/* null -rrr- */

	err = vm_protect(adspace, base + 1*PAGE_SIZE, 4*PAGE_SIZE, PROT_READ);
	assert(err == ENOMEM);

	err = vm_protect(adspace, base + 1*PAGE_SIZE, 3*PAGE_SIZE, PROT_READ);
	assert(err == EOK);
	assert_vma_count(adspace, 2);
	assert_vma(adspace, 1, base + 1*PAGE_SIZE, 3*PAGE_SIZE);

	/* null -rrr- */

	printk("- change permissions corner cases\n");

	err = vm_protect(adspace, base - 1*PAGE_SIZE, 1*PAGE_SIZE, PROT_READ);
	assert(err == EACCES);

	err = vm_protect(adspace, base - 1*PAGE_SIZE, 2*PAGE_SIZE, PROT_READ);
	assert(err == EACCES);

	err = vm_protect(adspace, base + 0*PAGE_SIZE, 1*PAGE_SIZE, PROT_NONE);
	assert(err == ENOMEM);

	/* null -rrr- */

	printk("- cleanup (unmap)\n");
	err = vm_unmap(adspace, base + 0*PAGE_SIZE, 5*PAGE_SIZE);
	assert(err == EOK);
	assert_vma_count(adspace, 1);

	/* null ----- */

	/* cleanup all used page tables */
	arch_adspace_unmap(adspace, ARCH_USER_BASE, ARCH_USER_END - ARCH_USER_BASE);
	assert(adspace->part->kmem_pagetables.free == kmem_pagetables_free_before);

	printk("***** VMA self-test END *****\n");
}

/*
 * VMA test code
 */
void vma_test_run_once(struct adspace *adspace)
{
	static int run_once = 0;

	if (run_once != 0) {
		return;
	}
	run_once = 1;

	vma_test(adspace);
}

#endif
