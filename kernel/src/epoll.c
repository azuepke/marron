/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll.c
 *
 * Epoll implementation.
 *
 * The epoll implementation follows the spirit of the Linux API
 * and supports with its both level and event triggered event notifications.
 * To link a file descriptor to an epoll instance, we use an epoll_note
 * that is linked in both the file descriptor and the epoll instance.
 * The epoll_note describes our flags of interest, while the file descriptor
 * contains the currently pending epoll state. When the epoll state
 * in the file descriptor changes, epoll_notify() is called and checks
 * all notes if the flags of interest match the currently pending flags.
 * If yes, the note becomes pending and is added to the pending queue
 * of the epoll instance. A note remains pending if it is already pending.
 * Also, threads waiting on the epoll instance are woken up.
 * Later, when the waiting threads consume the pending epoll notifications
 * in epoll_consume(), the then currently pending events will be checked
 * and reported to the user. Also, level-triggered event that remain pending
 * will be added back to the pending queue (we use epoll_wait_generation
 * to identify the end of the queue).
 *
 * Locking:
 * Locking is complex. We require both fd_locks of the epoll instance
 * and the target fd to *modify* any queues to the notes.
 * This allows us to iterate the notes of the epoll fd or of the target fd
 * safely with just one lock (epoll fd or target fd) held.
 * We then use a third lock inside the epoll instance, the epoll_waitq_lock,
 * to manage the pending queue and the list of waiting threads.
 * Unfortunately, we cannot use the fd_lock of the epoll instance for this,
 * as this would cause deadlocks.
 * We also have to be careful with part_lock when allocating and freeing notes.
 * Overall, we have the following locking hierarchy:
 * - part_lock
 * - fd_lock, up to locks: epoll instance and/or target fd
 * - epoll_waitq_lock
 * - sched_lock
 *
 * azuepke, 2024-12-08: initial
 * azuepke, 2024-12-24: pending epoll notes data model
 */

#include <marron/error.h>
#include <assert.h>
#include <sys_proto.h>
#include <uaccess.h>
#include <epoll.h>
#include <fd.h>
#include <part.h>
#include <thread.h>
#include <sched.h>
#include <process.h>


/* forward declarations */
static void epoll_waitq_wake_one(
	struct fd *epfd,
	err_t wakeup_code);
static void epoll_waitq_wake_all(
	struct fd *epfd,
	err_t wakeup_code);


/** allocate epoll note (called unlocked; locks part) */
static inline struct epoll_note *epoll_note_alloc(
	struct part *part)
{
	struct epoll_note *note;
	list_t *node;

	assert(part != NULL);
	assert(part->part_id != 0);

	part_lock(part);

	node = list_remove_first(&part->epoll_note_freeqh);
	if (node != NULL) {
		note = EPOLL_NOTE_FROM_NOTEQL(node);
		note->part = part;
	} else {
		note = NULL;
	}

	part_unlock(part);

	return note;
}

/** free epoll note (called unlocked; locks part) */
static inline void epoll_note_free(
	struct epoll_note *note)
{
	struct part *part;

	assert(note != NULL);
	part = note->part;
	assert(part != NULL);
	assert(part->part_id != 0);

	part_lock(part);

	list_node_init(&note->epoll_noteql);
	list_insert_first(&part->epoll_note_freeqh, &note->epoll_noteql);

	part_unlock(part);
}

/** insert new epoll note */
static inline void epoll_note_insert_in_all(
	struct epoll_note *note,
	struct fd *epfd,
	struct fd *fd,
	uint32_t fd_index,
	struct sys_epoll *event)
{
	note->epfd = epfd;
	list_node_init(&note->epoll_noteql);
	list_insert_last(&epfd->epoll.epoll_noteqh, &note->epoll_noteql);
	note->fd = fd;
	list_node_init(&note->epoll_fdql);
	list_insert_last(&fd->epoll_fdqh, &note->epoll_fdql);
	note->fd_index = fd_index;
	note->eventmask = event->eventmask;
	note->data = event->data;
	note->is_pending = 0;
}

/** insert epoll note on epfd's pending queue (epoll_waitq_lock must be locked) */
static inline void epoll_note_insert_pendingq(
	struct epoll_note *note,
	struct fd *epfd)
{
	if (!note->is_pending) {
		note->epoll_wait_generation = epfd->epoll.epoll_wait_generation;
		note->is_pending = 1;
		list_node_init(&note->epoll_pendingql);
		list_insert_last(&epfd->epoll.epoll_pendingqh, &note->epoll_pendingql);
	}
}

/** remove epoll note from epfd's pending queue (epoll_waitq_lock must be locked) */
static inline void epoll_note_remove_pendingq(
	struct epoll_note *note,
	struct fd *epfd)
{
	(void)epfd;

	if (note->is_pending) {
		note->is_pending = 0;
		list_remove(&note->epoll_pendingql);
	}
}

/** unlink an epoll note from all data structures (both epfd and fd are locked) */
static inline void epoll_note_remove_from_all(
	struct epoll_note *note,
	struct fd *epfd)
{
	/* remove note from pending queue */
	spin_lock(&epfd->epoll.epoll_waitq_lock);
	epoll_note_remove_pendingq(note, epfd);
	spin_unlock(&epfd->epoll.epoll_waitq_lock);

	/* unlink from both lists and free note */
	note->epfd = NULL;
	list_remove(&note->epoll_noteql);
	note->fd = NULL;
	list_remove(&note->epoll_fdql);
}

/** check if an event is already pending (after insertion or modification; both epfd and fd are locked) */
static inline void epoll_note_updated_maybe_pending(
	struct epoll_note *note,
	struct fd *epfd)
{
	uint32_t eventmask;
	struct fd *fd;

	spin_lock(&epfd->epoll.epoll_waitq_lock);

	if ((note->eventmask & EPOLLET) == 0) {
		/* must be a level-triggered event, check if we have a match
		 * (event-triggered notes must wait for a real event)
		 */
		fd = note->fd;
		assert(fd != NULL);

		eventmask = access_once(fd->cached_eventmask);
		eventmask &= note->eventmask;
		if (eventmask != 0) {
			note->eventmask_triggered = 1;

			epoll_note_insert_pendingq(note, epfd);
			epoll_waitq_wake_one(epfd, EOK);
		} else {
			epoll_note_remove_pendingq(note, epfd);
		}
	}

	spin_unlock(&epfd->epoll.epoll_waitq_lock);
}

////////////////////////////////////////////////////////////////////////////////

/** wake one thread from epoll wait queue (epoll_waitq_lock must be held) */
static void epoll_waitq_wake_one(
	struct fd *epfd,
	err_t wakeup_code)
{
	struct thread *thr;
	list_t *node;

	/* check epoll wait queue and wakeup one waiting thread, if any */
	node = list_remove_first(&epfd->epoll.epoll_waitqh);
	if (node != NULL) {
		thr = THR_FROM_EPOLL_WAITQ(node);
		assert(thr->cont_args.epoll.epfd == epfd);
		assert(thr->state == THREAD_STATE_WAIT_EPOLL);

		sched_lock(thr->sched);
		thread_wakeup(thr);
		thr->wakeup_code = wakeup_code;
		thread_cancel_clear(thr);
		sched_unlock(thr->sched);
	}
}

/** wake all threads from epoll wait queue (epoll_waitq_lock must be held) */
static void epoll_waitq_wake_all(
	struct fd *epfd,
	err_t wakeup_code)
{
	while (!list_is_empty(&epfd->epoll.epoll_waitqh)) {
		epoll_waitq_wake_one(epfd, wakeup_code);
	}
}

/** update event notifications in a specific file (fd must be locked) */
void epoll_notify(
	struct fd *fd,
	uint32_t eventmask)
{
	int exclusive_already_done;
	struct epoll_note *note;
	struct fd *epfd;
	list_t *node;

	assert(fd != NULL);
	assert_fd_locked(fd);
	assert((eventmask & ~EPOLL_MASK_EVENTS) == 0);

	fd->cached_eventmask = eventmask;

	/* iterate epoll notes for matches, and wake up waiters */
	exclusive_already_done = 0;
	list_for_each(&fd->epoll_fdqh, node) {
		note = EPOLL_NOTE_FROM_FDQL(node);

		epfd = note->epfd;
		assert(epfd != NULL);
		assert(epfd->type == FD_TYPE_EPOLL);

		spin_lock(&epfd->epoll.epoll_waitq_lock);

		if ((note->eventmask & eventmask) != 0) {
			/* interest! */

			if ((note->eventmask & EPOLLEXCLUSIVE) != 0) {
				/* skip further notification of EPOLLEXCLUSIVE notes */
				if (exclusive_already_done) {
					goto next_unlock;
				}
				exclusive_already_done = 1;
			}

			note->eventmask_triggered = 1;

			epoll_note_insert_pendingq(note, epfd);
			/* level-triggered events wake all waiters */
			if ((note->eventmask & EPOLLET) == 0) {
				epoll_waitq_wake_all(epfd, EOK);
			} else {
				epoll_waitq_wake_one(epfd, EOK);
			}
		} else {
			epoll_note_remove_pendingq(note, epfd);
		}

next_unlock:
		spin_unlock(&epfd->epoll.epoll_waitq_lock);
	}
}

err_t sys_epoll_create(ulong_t flags, uint32_t *epfd_index_user)
{
	uint32_t epfd_index;
	struct fd *epfd;
	int cloexec;
	err_t err;

	if ((flags & ~(SYS_FD_CLOEXEC)) != 0) {
		/* invalid flags */
		return EINVAL;
	}
	if (epfd_index_user == NULL) {
		/* NULL pointer */
		return EINVAL;
	}

	epfd = fd_alloc(current_process());
	if (epfd == NULL) {
		/* failed to allocate a new file descriptor */
		return ENFILE;
	}

	/* further initialization */
	epfd->type = FD_TYPE_EPOLL;

	list_head_init(&epfd->epoll.epoll_noteqh);
	spin_init(&epfd->epoll.epoll_waitq_lock);
	epfd->epoll.epoll_wait_generation = 0;
	list_head_init(&epfd->epoll.epoll_waitqh);
	list_head_init(&epfd->epoll.epoll_pendingqh);

	cloexec = (flags & SYS_FD_CLOEXEC) != 0;
	err = fd_dup(current_process(), epfd, cloexec, &epfd_index);
	if (err != EOK) {
		epfd->type = FD_TYPE_DEAD;
		fd_close(epfd);
		return err;
	}

	err = user_put_4(epfd_index_user, epfd_index);
	/* NOTE: any failure here is ignored and resources are not freed
	 * (the file descriptor can be accessed by user space nevertheless)
	 */

	return err;
}

/** close epoll instance file descriptor */
void epoll_close(
	struct fd *epfd)
{
	struct epoll_note *note;
	int more_to_do;
	struct fd *fd;
	list_t *node;

	assert(epfd != NULL);
	assert(epfd->type == FD_TYPE_EPOLL);
	assert_fd_locked(epfd);

next_note:
	node = list_first(&epfd->epoll.epoll_noteqh);
	if (node != NULL) {
		note = EPOLL_NOTE_FROM_NOTEQL(node);
		assert(note->epfd == epfd);
		fd = note->fd;

		/* (re-)lock both epfd and fd and check node again under lock */
		fd_unlock(epfd);
		fd_lock2(epfd, fd);
		node = list_first(&epfd->epoll.epoll_noteqh);
		if ((note != EPOLL_NOTE_FROM_NOTEQL(node)) ||
		    (fd != note->fd)) {
			/* mismatch due to race condition, try again */
			fd_unlock(fd);
			goto next_note;
		}

		epoll_note_remove_from_all(note, epfd);

		fd_unlock(epfd);
		fd_unlock(fd);

		/* NOTE: SMP: requires part_lock, must be taken before fd_lock */
		epoll_note_free(note);

		fd_lock(epfd);
		goto next_note;
	}

next_waiter:
	spin_lock(&epfd->epoll.epoll_waitq_lock);

	epoll_waitq_wake_one(epfd, ECANCEL);
	more_to_do = !list_is_empty(&epfd->epoll.epoll_waitqh);

	spin_unlock(&epfd->epoll.epoll_waitq_lock);

	if (more_to_do) {
		goto next_waiter;
	}
}

/** close file descriptor and cleanup epoll notes */
void epoll_cleanup_notes_on_close(
	struct fd *fd)
{
	struct epoll_note *note;
	struct fd *epfd;
	list_t *node;

	assert(fd != NULL);
	assert_fd_locked(fd);

next_note:
	node = list_first(&fd->epoll_fdqh);
	if (node != NULL) {
		note = EPOLL_NOTE_FROM_FDQL(node);
		assert(note->fd == fd);
		epfd = note->epfd;

		/* (re-)lock both fd and epfd and check node again under lock */
		fd_unlock(fd);
		fd_lock2(fd, epfd);
		node = list_first(&fd->epoll_fdqh);
		if ((note != EPOLL_NOTE_FROM_FDQL(node)) ||
		    (epfd != note->epfd)) {
			/* mismatch due to race condition, try again */
			fd_unlock(epfd);
			goto next_note;
		}

		epoll_note_remove_from_all(note, epfd);

		fd_unlock(fd);
		fd_unlock(epfd);

		/* NOTE: SMP: requires part_lock, must be taken before fd_lock */
		epoll_note_free(note);

		fd_lock(fd);
		goto next_note;
	}
}

static err_t sys_epoll_ctl_add(struct fd *epfd, struct fd *fd, uint32_t fd_index, struct sys_epoll *event, struct epoll_note **new_note)
{
	struct epoll_note *note;
	list_t *node;

	assert(new_note != NULL);
	assert(*new_note != NULL);

	if (event == NULL) {
		/* event struct must be provided */
		return EINVAL;
	}
	if ((event->eventmask & ~EPOLL_MASK_CTL_ADD_EVENTS) != 0) {
		/* invalid eventmask */
		return EINVAL;
	}
	if ((event->eventmask & EPOLLEXCLUSIVE) != 0) {
		if ((event->eventmask & ~EPOLL_MASK_CTL_ADD_EXCLUSIVE_EVENTS) != 0) {
			/* invalid event with EPOLLEXCLUSIVE */
			return EINVAL;
		}
	}
	event->eventmask |= EPOLL_MASK_IMPLICIT;

	/* NOTE: SMP: both epfd and fd are locked */

	list_for_each(&epfd->epoll.epoll_noteqh, node) {
		note = EPOLL_NOTE_FROM_NOTEQL(node);

		if ((note->fd == fd) && (note->fd_index == fd_index)) {
			/* match */
			assert(note->epfd == epfd);
			/* epoll note already exists */
			return EEXIST;
		}
	}

	note = *new_note;
	*new_note = NULL;

	epoll_note_insert_in_all(note, epfd, fd, fd_index, event);
	epoll_note_updated_maybe_pending(note, epfd);

	return EOK;
}

static err_t sys_epoll_ctl_mod(struct fd *epfd, struct fd *fd, uint32_t fd_index, struct sys_epoll *event)
{
	struct epoll_note *note;
	list_t *node;

	if (event == NULL) {
		/* event struct must be provided */
		return EINVAL;
	}
	if ((event->eventmask & ~EPOLL_MASK_CTL_MOD_EVENTS) != 0) {
		/* invalid eventmask */
		return EINVAL;
	}
	event->eventmask |= EPOLL_MASK_IMPLICIT;

	/* NOTE: SMP: both epfd and fd are locked */

	list_for_each(&epfd->epoll.epoll_noteqh, node) {
		note = EPOLL_NOTE_FROM_NOTEQL(node);

		if ((note->fd == fd) && (note->fd_index == fd_index)) {
			/* match */
			assert(note->epfd == epfd);

			if ((note->eventmask & EPOLLEXCLUSIVE) != 0) {
				/* epoll notes with EPOLLEXCLUSIVE cannot be modded */
				return EINVAL;
			}

			note->eventmask = event->eventmask;
			note->data = event->data;
			epoll_note_updated_maybe_pending(note, epfd);

			return EOK;
		}
	}

	/* epoll note not found */
	return ENOENT;
}

static err_t sys_epoll_ctl_del(struct fd *epfd, struct fd *fd, uint32_t fd_index, struct epoll_note **del_note)
{
	struct epoll_note *note;
	list_t *node;

	assert(del_note != NULL);
	assert(*del_note == NULL);

	/* NOTE: SMP: both epfd and fd are locked */

	list_for_each(&epfd->epoll.epoll_noteqh, node) {
		note = EPOLL_NOTE_FROM_NOTEQL(node);

		if ((note->fd == fd) && (note->fd_index == fd_index)) {
			/* match */
			assert(note->epfd == epfd);

			epoll_note_remove_from_all(note, epfd);

			*del_note = note;
			return EOK;
		}
	}

	/* epoll note not found */
	return ENOENT;
}

err_t sys_epoll_ctl(ulong_t epfd_index, ulong_t op, ulong_t fd_index, const struct sys_epoll *event_user)
{
	struct sys_epoll event = { 0 };
	struct sys_epoll *event_p;
	struct epoll_note *note;
	struct fd *epfd;
	struct fd *fd;
	err_t err;

	if (event_user != NULL) {
		err = user_copy_in_aligned(&event, event_user, sizeof(event));
		if (err != EOK) {
			/* copy error */
			return err;
		}
		event_p = &event;
	} else {
		event_p = NULL;
	}

	note = NULL;
	if (op == EPOLL_CTL_ADD) {
		/* NOTE: SMP: requires part_lock, must be taken before fd_lock */
		note = epoll_note_alloc(current_thread()->part);
		if (note == NULL) {
			/* out of free epoll notes */
			return ENOMEM;
		}
	}

	err = fd_get_and_lock2(current_process(), epfd_index, fd_index, &epfd, &fd);
	if (err != EOK) {
		goto out_free_note;
	}

	if (epfd->type != FD_TYPE_EPOLL) {
		/* epfd is not an epoll instance */
		err = EINVAL;
		goto out_unlock_fds;
	}

	if (fd->type == FD_TYPE_EPOLL) {
		/* fd is an epoll instance (we do not support nesting) */
		err = ELOOP;
		goto out_unlock_fds;
	}

	if ((fd->cached_eventmask & EPOLLNVAL) != 0) {
		/* fd does not support epoll */
		err = EPERM;
		goto out_unlock_fds;
	}

	switch (op) {
	case EPOLL_CTL_ADD:
		err = sys_epoll_ctl_add(epfd, fd, fd_index, event_p, &note);
		break;

	case EPOLL_CTL_MOD:
		err = sys_epoll_ctl_mod(epfd, fd, fd_index, event_p);
		break;

	case EPOLL_CTL_DEL:
		err = sys_epoll_ctl_del(epfd, fd, fd_index, &note);
		break;

	default:
		err = EINVAL;
		break;
	}

out_unlock_fds:
	fd_unlock(epfd);
	fd_unlock(fd);

out_free_note:
	if (note != NULL) {
		/* NOTE: SMP: requires part_lock, must be taken before fd_lock */
		epoll_note_free(note);
	}

	return err;
}

/** consume epoll events that are pending
 * called after waiting for epoll events
 */
static err_t epoll_consume(struct fd *epfd, struct sys_epoll *event_user, uint32_t max_events, uint32_t *num_events)
{
	struct sys_epoll event = { 0 };
	uint32_t epoll_wait_generation;
	uint32_t note_orig_eventmask;
	struct epoll_note *note;
	list_t *node;
	struct fd *fd;
	err_t err;

	assert(num_events != NULL);

	epoll_wait_generation = epfd->epoll.epoll_wait_generation;
	epfd->epoll.epoll_wait_generation++;

	/* NOTE: SMP: only epfd is locked */
	*num_events = 0;

	spin_lock(&epfd->epoll.epoll_waitq_lock);

next_note:
	node = list_first(&epfd->epoll.epoll_pendingqh);
	if (node == NULL) {
		err = EOK;
		goto out_unlock;
	}

	note = EPOLL_NOTE_FROM_PENDINGQL(node);
	assert(note->epfd == epfd);

	if ((int)(note->epoll_wait_generation - epoll_wait_generation) > 0) {
		/* reached the end of the queue */
		err = EOK;
		goto out_unlock;
	}

	epoll_note_remove_pendingq(note, epfd);

	note_orig_eventmask = note->eventmask;
	note->eventmask_triggered = 0;

	fd = note->fd;
	assert(fd != NULL);

	event.eventmask = access_once(fd->cached_eventmask);
	event.eventmask &= note_orig_eventmask;

	if ((note_orig_eventmask & EPOLLONESHOT) != 0) {
		/* deactivate oneshot event after consumption */
		note->eventmask = 0;
	} else if (((note_orig_eventmask & EPOLLET) == 0) && (event.eventmask != 0)) {
		/* still pending level-triggered event : re-insert note at end */
		epoll_note_insert_pendingq(note, epfd);
	}

	if (event.eventmask != 0) {
		event.data = note->data;
		err = user_copy_out_aligned(event_user, &event, sizeof(event));
		if (err != EOK) {
			/* copy error */
			goto out_unlock;
		}

		event_user++;
		(*num_events)++;
	}

	if (*num_events < max_events) {
		goto next_note;
	}

	err = EOK;

out_unlock:
	spin_unlock(&epfd->epoll.epoll_waitq_lock);

	return err;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void epoll_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	struct fd *epfd;

	assert(thr != NULL);
	assert((wakeup_code == EINTR) ||
	       (wakeup_code == ETIMEDOUT) ||
	       (wakeup_code == ECANCEL));

	epfd = thr->cont_args.epoll.epfd;
	assert(epfd != NULL);

	/* remove thread from wait queue */
	spin_lock(&epfd->epoll.epoll_waitq_lock);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	/* FIXME: this might fail with a NULL pointer exception in a debug kernel
	 * in a rare race condition with timeout expiration.
	 */
	list_remove(&thr->cont_args.epoll.epoll_waitql);

	spin_unlock(&epfd->epoll.epoll_waitq_lock);
}

static void epoll_wait_finish(struct thread *thr)
{
	uint32_t num_events;
	struct fd *epfd;
	err_t err;

	assert(thr != NULL);
	assert(thr->cont_args.epoll.epfd != NULL);
	assert(thr->cont_args.epoll.event_user != NULL);
	assert(thr->cont_args.epoll.max_events >= 1);
	assert(thr->cont_args.epoll.num_events_user != NULL);

	/* in any case, restore original blocked mask */
	thr->sig.wait_mask = ~thr->sig.blocked_mask;

	err = thr->wakeup_code;
	if (err != EOK) {
		assert(err == ETIMEDOUT);
		goto out_return;
	}

	epfd = thr->cont_args.epoll.epfd;

	fd_lock(epfd);
	if (epfd->type != FD_TYPE_EPOLL) {
		/* wrong file type */
		assert(0); // FIXME: should not happen
		err = EINVAL;
		goto out_unlock_fd;
	}

	err = epoll_consume(epfd, thr->cont_args.epoll.event_user, thr->cont_args.epoll.max_events, &num_events);

out_unlock_fd:
	fd_unlock(epfd);

	if (err == EOK) {
		err = user_put_4(thr->cont_args.epoll.num_events_user, num_events);
	}

out_return:
	arch_syscall_retval(thr->regs, err);
}

err_t sys_epoll_wait(ulong_t epfd_index, ulong_t max_events, sys_time_t timeout, const sys_sig_mask_t *blocked_mask_p, struct sys_epoll *event_user, uint32_t *num_events_user)
{
	sys_sig_mask_t proc_pending_mask;
	sys_sig_mask_t thr_pending_mask;
	sys_sig_mask_t blocked_mask;
	sys_sig_mask_t sig_mask;
	uint32_t num_events = 0;
	struct thread *thr;
	struct fd *epfd;
	err_t err;

	if ((event_user == NULL) || (num_events_user == NULL)) {
		/* null pointers */
		return EINVAL;
	}
	if ((max_events == 0) || (max_events >= 0xffffffff / sizeof(*event_user))) {
		/* invalid number of events */
		return EINVAL;
	}

	thr = current_thread();
	blocked_mask = thr->sig.blocked_mask;
	if (blocked_mask_p != NULL) {
		err = user_get_8(blocked_mask_p, blocked_mask);
		if (unlikely(err != EOK)) {
			/* not accessible */
			return err;
		}
	}

	/* get and lock used fd */
	err = fd_get_type_and_lock(current_process(), epfd_index, FD_TYPE_EPOLL, &epfd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	/* remember for later */
	thr->cont_args.epoll.epfd = epfd;
	thr->cont_args.epoll.event_user = event_user;
	thr->cont_args.epoll.max_events = max_events;
	thr->cont_args.epoll.num_events_user = num_events_user;

	err = epoll_consume(epfd, event_user, max_events, &num_events);
	if ((err != EOK) || (num_events > 0)) {
		/* events already pending */
		goto out_unlock_fd;
	}

	/* short cut for non-blocking polling mode */
	if (timeout == TIMEOUT_NULL) {
		err = EAGAIN;
		goto out_unlock_fd;
	}

	/* check for pending signals */
	/* NOTE: unlocked access to the cached pending masks */
	proc_pending_mask = thr->process->sig.pending.cached_pending_mask;
	thr_pending_mask = thr->sig.pending.cached_pending_mask;

	sig_mask = (proc_pending_mask | thr_pending_mask) & ~blocked_mask;
	if (likely(sig_mask != 0)) {
		// FIXME: deliver signal
		err = EINTR;
		goto out_unlock_fd;
	}

	/* change accepted signals while waiting */
	thr->sig.wait_mask = ~blocked_mask;

	/* blocking */
	spin_lock(&epfd->epoll.epoll_waitq_lock);
	list_node_init(&thr->cont_args.epoll.epoll_waitql);
	list_insert_last(&epfd->epoll.epoll_waitqh, &thr->cont_args.epoll.epoll_waitql);
	spin_unlock(&epfd->epoll.epoll_waitq_lock);

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_EPOLL, timeout, CLK_ID_MONOTONIC);
	thread_continue_at(thr, epoll_wait_finish);
	thread_cancel_at(thr, epoll_wait_cancel);
	sched_unlock(thr->sched);

	err = ETIMEDOUT;

out_unlock_fd:
	fd_unlock(epfd);

	if (err == EOK) {
		err = user_put_4(num_events_user, num_events);
	}

	return err;
}
