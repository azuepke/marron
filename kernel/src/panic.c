/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017 Alexander Zuepke */
/*
 * panic.c
 *
 * Kernel panic.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 */

#include <panic.h>
#include <assert.h>
#include <bsp.h>
#include <printk.h>
#include <arch.h>
#include <current.h>
#include <stdbool.h>

static unsigned int panic_in_progress = false;

static __cold void panic_pre(void)
{
	if (panic_in_progress) {
		bsp_halt(BOARD_STOP);
		for (;;) {
			;
		}
	}

	panic_in_progress = true;

#ifdef SMP
	printk("\nKernel panic on CPU %u: ", bsp_cpu_id());
#else
	printk("\nKernel panic: ");
#endif
}

static __cold __noreturn void panic_post(void)
{
#ifdef AUTOBUILD
	printk("AUTOBUILD: halt on panic or assert in kernel\n");
	bsp_halt(BOARD_POWEROFF);
#endif
#ifdef COVBUILD
	printk("COVBUILD: halt on panic or assert in kernel\n");
	bsp_halt(BOARD_POWEROFF);
#endif

	bsp_halt(BOARD_PANIC);
	for (;;) {
		;
	}
}

/** kernel panic */
void panic(const char* format, ...)
{
	va_list args;

	panic_pre();

	va_start(args, format);
	vprintk(format, args);
	va_end(args);

	panic_post();
}

/** panic + register dump */
void panic_regs(struct regs *regs, unsigned int status, unsigned long addr, const char* format, ...)
{
	va_list args;

	panic_pre();

	va_start(args, format);
	vprintk(format, args);
	va_end(args);

	arch_dump_registers(regs, status, addr);

	panic_post();
}

#ifndef NDEBUG
/** kernel assertion */
/* NOTE: we provide this function only in debug kernels */
void __assert_fail(const char *cond, const char *file, int line, const char *func)
{
	printk("%s:%d: %s: assertion '%s' failed.\n", file, line, func, cond);
	printk("%s:%d: %s: called from: %p\n", file, line, func, __builtin_return_address(0));

	panic_post();
}
#endif
