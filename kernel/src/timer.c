/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022, 2024, 2025 Alexander Zuepke */
/*
 * timer.c
 *
 * Timers and timerfd.
 *
 * azuepke, 2021-08-13: initial
 * azuepke, 2022-10-19: timerfd
 * azuepke, 2024-12-31: epoll for timerfd
 * azuepke, 2025-01-05: merge timerfd into timer
 * azuepke, 2025-02-22: processes
 */

#include <timer.h>
#include <kernel.h>
#include <assert.h>
#include <stddef.h>
#include <marron/error.h>
#include <marron/fcntl.h>
#include <marron/ipc_io.h>
#include <sig.h>
#include <thread_types.h>
#include <sched.h>
#include <part.h>
#include <thread.h>
#include <uaccess.h>
#include <sys_proto.h>
#include <fd.h>
#include <epoll.h>
#include <time.h>
#include <process.h>


/* forward */
static void timer_wait_cancel(struct thread *thr, err_t wakeup_code);
static void timer_wait_finish(struct thread *thr);
static void timerfd_wait_finish(struct thread *thr);

static_assert(offsetof(struct thread, cont_args.timer.recv_msg_user) ==
              offsetof(struct thread, cont_args.ipc_wait.recv_msg_user));


/** allocate timer object (locks part_lock) */
static inline struct timer *timer_alloc(
	struct part *part)
{
	struct timer *timer;
	list_t *node;

	assert(part != NULL);

	part_lock(part);

	node = list_remove_first(&part->timer_freeqh);
	if (node != NULL) {
		timer = TIMER_FROM_TIMER_FREEQ(node);
		assert(timer->state == TIMER_STATE_DISABLED);
		assert(timer->part_id > 0);
	} else {
		timer = NULL;
	}

	part_unlock(part);

	return timer;
}

/** free timer object (locks part_lock) */
static inline void timer_free(
	struct part *part,
	struct timer *timer)
{
	assert(timer != NULL);
	assert(part != NULL);
	assert(timer->part_id > 0);
	assert(part == &part_dyn[timer->part_id]);

	part_lock(part);

	timer->state = TIMER_STATE_DISABLED;

	list_node_init(&timer->timer_freeql);
	list_insert_first(&part->timer_freeqh, &timer->timer_freeql);

	part_unlock(part);
}

/** initialize a timer and set the method of notification (called unlocked) */
static inline void timer_init(
	struct timer *timer,
	struct process *proc,
	struct sched *sched,
	unsigned int clk_id,
	const struct sys_sig_info *info,
	unsigned int thr_id)
{
	assert(timer != NULL);
	assert(proc != NULL);
	assert(sched != NULL);
	assert(clk_id < NUM_CLK_IDS);

	list_node_init(&timer->sigqn.sigql);
	/* explicitly mark the node as "not enqueued" */
	timer->sigqn.sigql.next = NULL;
	timer->sigqn.timer = timer;

	if (info != NULL) {
		timer->sigqn.info = *info;
		timer->state = TIMER_STATE_SIGNAL;
	} else {
		/* timer->sigqn.info ignored */
		timer->state = TIMER_STATE_WAIT;
	}
	timer->process = proc;
	timer->sched = sched;
	timer->thr_id = thr_id;
	timer->fd = NULL;
	timer->next = 0;
	timer->period = 0;
	timer->clk_id = clk_id & 0xff;
	timer->sig_enqueued = 0;

	/* clear overrun counter */
	timer->sigqn.info.status = 0;
	timer->expiry_count = 0;
	list_head_init(&timer->timer_waitqh);
}

/** disable a timer -- wakes all threads, etc (process_lock) */
void timer_disable(
	struct timer *timer)
{
	struct process *proc;
	struct thread *thr;
	list_t *node;

	assert(timer != NULL);
	proc = timer->process;
	assert_process_locked(proc);
	assert(timer->state != TIMER_STATE_DISABLED);

	/* teardown timer if active */
	assert(timer->sched != NULL);
	sched_lock(timer->sched);
	sched_timer_stop(timer->sched, timer);
	sched_unlock(timer->sched);

	/* pull any signal from the thread */
	if (timer->sig_enqueued != 0) {
		timer->sig_enqueued = 0;
		list_remove(&timer->sigqn.sigql);
	}

	/* wake all threads waiting on wait queue */
	list_for_each_remove_first(&timer->timer_waitqh, node) {
 		thr = THR_FROM_TIMER_WAITQ(node);
 		assert(thr != NULL);
 		assert(thr->state == THREAD_STATE_WAIT_TIMER);

		/* wake waiting thread with ECANCEL */
		sched_lock(thr->sched);
		thread_wakeup(thr);
		thr->wakeup_code = ECANCEL;
		thread_cancel_clear(thr);
		sched_unlock(thr->sched);
	}

	list_remove(&timer->timer_activeql);

	process_unlock(proc);

	timer_free(timer->process->part, timer);

	process_lock(proc);
}

/** disable a timer for timerfd (disable now or later, depending on waiters) */
/* NOTE: called with process_lock */
static void timer_disable_timerfd(
	struct timer *timer)
{
	assert(timer != NULL);
	assert_process_locked(timer->process);
	assert((timer->state == TIMER_STATE_DISABLED) ||
	       (timer->state == TIMER_STATE_SIGNAL) ||
	       (timer->state == TIMER_STATE_WAIT));

	assert(timer->sig_enqueued == 0);

	if (timer->state == TIMER_STATE_DISABLED) {
		return;
	}
	if (list_is_empty(&timer->timer_waitqh)) {
		/* no waiters: we can safely take down the timer now */
		timer_disable(timer);
	} else {
		/* pending waiters: do cleanup once all waiters were woken up */
		timer->state = TIMER_STATE_CLOSED;
	}
}

/** set (program) a timer */
/* NOTE: called with process_lock */
static void timer_set(
	struct timer *timer,
	unsigned int flags,
	sys_timeout_t initial,
	sys_time_t period)
{
	assert(timer != NULL);
	assert_process_locked(timer->process);
	assert((timer->state == TIMER_STATE_SIGNAL) ||
	       (timer->state == TIMER_STATE_WAIT));

	/* teardown timer if active */
	assert(timer->sched != NULL);
	sched_lock(timer->sched);
	sched_timer_stop(timer->sched, timer);
	sched_unlock(timer->sched);

	if ((flags & TIMEOUT_ABSOLUTE) == 0) {
		/* relative timeout */
		initial = time_add(initial, bsp_timer_get_time());
		initial += arch_load64(&clock_realtime_offset[timer->clk_id]);
	}

	/* clear overrun counter */
	timer->sigqn.info.status = 0;
	timer->expiry_count = 0;

	/* activate timer */
	sched_lock(timer->sched);
	timer->next = initial;
	timer->period = period;
	sched_timer_start(timer->sched, timer);
	sched_unlock(timer->sched);
}

/** get current timer programming */
/* NOTE: called with process_lock */
static void timer_get(
	struct timer *timer,
	sys_timeout_t *remaining,
	sys_time_t *period)
{
	sys_time_t now, next;
	sys_time_t rem;

	assert(timer != NULL);
	assert_process_locked(timer->process);
	assert((timer->state == TIMER_STATE_SIGNAL) ||
	       (timer->state == TIMER_STATE_WAIT));
	assert(remaining != NULL);
	assert(period != NULL);
	assert(timer->sched != NULL);

	sched_lock(timer->sched);

	*period = timer->period;
	next = timer->next;

	sched_unlock(timer->sched);

	rem = 0;
	if (next != 0) {
		now = bsp_timer_get_time();
		now += arch_load64(&clock_realtime_offset[timer->clk_id]);

		if (next > now) {
			/* timer still running */
			rem = next - now;
		} else {
			/* timer already expired (but we missed the interrupt) */
			/* 1ns is "almost immediately" */
			rem = 1;
		}
		assert(rem >= 1);
	}
	*remaining = rem;
}

/** get timer overrun counter */
/* NOTE: called with process_lock */
static void timer_overrun(
	struct timer *timer,
	unsigned int *overrun_count)
{
	assert(timer != NULL);
	assert_process_locked(timer->process);
	assert((timer->state == TIMER_STATE_SIGNAL) ||
	       (timer->state == TIMER_STATE_WAIT));
	assert(overrun_count != NULL);
	assert(timer->sched != NULL);

	/* get overrun counter (read without lock) */
	*overrun_count = access_once(timer->sigqn.info.status);
}

/** wait for timer */
/* NOTE: called with process_lock */
static err_t timer_wait(
	struct timer *timer,
	int wait,
	uint64_t *expiry_count)
{
	struct thread *thr;

	assert(timer != NULL);
	assert_process_locked(timer->process);
	assert(expiry_count != NULL);
	assert(timer->sched != NULL);

	*expiry_count = timer->expiry_count;

	if (timer->expiry_count != 0) {
		/* timer already expired in the past */
		/* reset expiry count after reading */
		timer->expiry_count = 0;

		return EOK;
	}

	if (!wait) {
		/* would block, but non-blocking mode */
		return EAGAIN;
	}

	/* waiting */
	thr = current_thread();
	list_node_init(&thr->cont_args.timer.timer_waitql);
	list_insert_last(&timer->timer_waitqh, &thr->cont_args.timer.timer_waitql);

	sched_lock(timer->sched);
	thread_wait(thr, THREAD_STATE_WAIT_TIMER, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
	thread_continue_at(thr, timer_wait_finish);
	thr->wakeup_code = ETIMEDOUT;
	thread_cancel_at(thr, timer_wait_cancel);
	sched_unlock(timer->sched);

	return ETIMEDOUT;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timer expires or the thread is deleted
 */
static void timer_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	struct process *proc;

	assert(thr != NULL);
	assert((wakeup_code == EINTR) ||
	       (wakeup_code == ETIMEDOUT) ||
	       (wakeup_code == ECANCEL));

	/* set error code to EOK instead of ETIMEDOUT */
	if (wakeup_code == ETIMEDOUT) {
		wakeup_code = EOK;
	}

	proc = thr->process;

	/* remove thread from wait queue */
	process_lock(proc);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	/* FIXME: this might fail with a NULL pointer exception in a debug kernel
	 * in a rare race condition with timeout expiration.
	 */
	list_remove(&thr->cont_args.timer.timer_waitql);

	process_unlock(proc);
}

static void timer_wait_finish(struct thread *thr)
{
	uint64_t *expiry_count_user;
	err_t err;

	err = thr->wakeup_code;

	if (err == EOK) {
		expiry_count_user = thr->cont_args.timer_wait.expiry_count_user;
		if (expiry_count_user != NULL) {
			err = user_put_8(expiry_count_user, thr->cont_args.timer.expiry_count);
		}
	}

	arch_syscall_retval(thr->regs, err);
}

/* do timer expiry action (called from scheduler) */
/* NOTE: called without sched_lock, locks process_lock internally */
void timer_expiry_action(
	struct timer *timer,
	uint64_t expiry_count)
{
	struct process *proc;
	struct thread *thr;
	list_t *node;

	assert(timer != NULL);
	assert((timer->state == TIMER_STATE_SIGNAL) ||
	       (timer->state == TIMER_STATE_WAIT) ||
	       (timer->state == TIMER_STATE_CLOSED));
	assert(expiry_count >= 1);

	proc = timer->process;
	assert(proc != NULL);
	process_lock(proc);

	/* a 64-bit expiration counter does not overflow with 64-bit timeouts */
	timer->expiry_count += expiry_count;
	if (timer->state == TIMER_STATE_SIGNAL) {
		if (timer->sig_enqueued == 0) {
			timer->sig_enqueued = 1;
			/* clear overrun counter */
			if (expiry_count <= 0xffffffff) {
				timer->sigqn.info.status = expiry_count - 1;
			} else {
				timer->sigqn.info.status = 0xffffffff;
			}

			(void)sig_send_sigqn(timer->process, timer->thr_id, timer->sigqn.info.sig, &timer->sigqn);
		} else {
			/* overrun counter -- do not overflow */
			if (timer->sigqn.info.status + expiry_count < timer->sigqn.info.status) {
				timer->sigqn.info.status += expiry_count;
			} else {
				timer->sigqn.info.status = 0xffffffff;
			}
		}
	}

	if (timer->fd != NULL) {
		fd_lock(timer->fd);
		epoll_notify(timer->fd, EPOLLIN);
		fd_unlock(timer->fd);
	}

	/* wake one thread waiting on wait queue */
	node = list_first(&timer->timer_waitqh);
	if (node != NULL) {
		thr = THR_FROM_TIMER_WAITQ(node);
		assert(thr != NULL);
		assert(thr->state == THREAD_STATE_WAIT_TIMER);

		list_remove(&thr->cont_args.timer.timer_waitql);

		thr->cont_args.timer.expiry_count = timer->expiry_count;
		/* reset expiry count after reading */
		timer->expiry_count = 0;

		/* wake waiting thread with EOK */
		sched_lock(thr->sched);
		thread_wakeup(thr);
		thr->wakeup_code = EOK;
		thread_cancel_clear(thr);
		sched_unlock(thr->sched);

		if (timer->fd != NULL) {
			fd_lock(timer->fd);
			epoll_notify(timer->fd, 0);
			fd_unlock(timer->fd);
		}
	}

	if (timer->state == TIMER_STATE_CLOSED) {
		if (list_is_empty(&timer->timer_waitqh)) {
			/* no more waiters: take down timer now */
			timer_disable(timer);
		}
	}

	process_unlock(proc);
}

////////////////////////////////////////////////////////////////////////////////

err_t sys_timer_create(
	ulong_t clk_id,
	ulong_t flags,
	const struct sys_sig_info *info_user,
	ulong_t thr_id,
	uint32_t *fd_index_user)
{
	struct sys_sig_info info_kern;
	struct sys_sig_info *info;
	struct process *proc;
	struct timer *timer;
	struct sched *sched;
	struct thread *thr;
	uint32_t fd_index;
	struct fd *fd;
	int nonblock;
	int cloexec;
	err_t err;

	if (clk_id >= NUM_CLK_IDS) {
		/* invalid clock ID */
		return EINVAL;
	}
	if ((flags & ~(SYS_FD_NONBLOCK | SYS_FD_CLOEXEC)) != 0) {
		/* invalid flags */
		return EINVAL;
	}
	if (fd_index_user == NULL) {
		/* timer ID pointer invalid */
		return EINVAL;
	}

	/* bind timer to a specific CPU:
	 * - by default, we bind it to the caller's CPU
	 * - but for signals to threads, we try to bind it to the given thread's CPU
	 */
	sched = current_thread()->sched;
	proc = current_process();

	info = NULL;
	if (info_user != NULL) {
		info = &info_kern;
		err = user_copy_in_aligned(&info_kern, info_user, sizeof(info_kern));
		if (err != EOK) {
			/* copy error */
			return err;
		}

		/* sanitize */
		if (info_kern.sig >= NUM_SIGS) {
			/* invalid signal ID */
			return EINVAL;
		}

		/* info_kern.code set by user */
		/* info_kern.ex_type is the timer ID (set later) */
		/* info_kern.status is the overrun counter */
		/* info_kern.addr set by user */

		thr_id = (uint32_t)thr_id;
		if (thr_id == THR_ID_ALL) {
			/* send signal to partition */
			/* thr = NULL; */
		} else {
			err = thread_lookup(proc, thr_id, &thr);
			if (err != EOK) {
				return err;
			}
			sched = thr->sched;
		}
	} else {
		/* thr_id is ignored if info is NULL */
		thr_id = THR_ID_ALL;
	}

	timer = timer_alloc(proc->part);
	if (timer == NULL) {
		/* failed to allocate a timer */
		return ELIMIT;
	}

	timer_init(timer, proc, sched, clk_id, info, thr_id);

	fd = fd_alloc(proc);
	if (fd == NULL) {
		/* failed to allocate a new file descriptor */
		err = ENFILE;
		goto out_free_timer;
	}

	/* further initialization */
	fd->type = FD_TYPE_TIMERFD;
	fd->cached_eventmask = 0;
	nonblock = (flags & SYS_FD_NONBLOCK) != 0;
	fd->oflags = O_RDONLY | (nonblock ? O_NONBLOCK : 0);
	fd->oflags_mask = O_NONBLOCK;
	fd->timerfd.timer = timer;

	timer->fd = fd;

	/* register timer and fd under lock */
	process_lock(proc);

	list_node_init(&timer->timer_activeql);
	list_insert_last(&proc->timer_activeqh, &timer->timer_activeql);

	cloexec = (flags & SYS_FD_CLOEXEC) != 0;
	err = fd_dup_locked(proc, fd, cloexec, &fd_index);
	if (err != EOK) {
		goto out_unregister_timer_and_unlock_and_free_fd;
	}

	timer->sigqn.info.ex_type = fd_index & 0xffff;

	process_unlock(proc);

	err = user_put_4(fd_index_user, fd_index);
	/* NOTE: any failure here is ignored and resources are not freed
	 * (the file descriptor can be accessed by user space nevertheless)
	 */

	return err;

out_unregister_timer_and_unlock_and_free_fd:
	list_remove(&timer->timer_activeql);

	process_unlock(proc);

	fd->type = FD_TYPE_DEAD;
	fd_close(fd);

out_free_timer:
	timer_free(proc->part, timer);

	return err;
}

/** close callback for timerfd */
void timerfd_close(
	struct fd *fd)
{
	struct process *proc;
	struct timer *timer;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_TIMERFD);
	assert_fd_locked(fd);

	timer = fd->timerfd.timer;
	fd->timerfd.timer = NULL;
	assert(timer != NULL);

	fd_unlock(fd);

	proc = timer->process;
	process_lock(proc);
	timer_disable_timerfd(timer);
	process_unlock(proc);

	fd_lock(fd);
}

err_t sys_timer_set(
	ulong_t fd_index,
	ulong_t flags,
	sys_timeout_t initial,
	sys_time_t period)
{
	struct timer *timer;
	struct fd *fd;
	err_t err;

	if ((flags & ~TIMEOUT_ABSOLUTE) != 0) {
		/* invalid flags */
		return EINVAL;
	}

	if (initial == TIMEOUT_NULL) {
		period = 0;
	}

	if ((period >> 60) != 0) {
		/* period value too large (limited to 2^60 - 1) */
		return EINVAL;
	}

	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_TIMERFD, &fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	timer = fd->timerfd.timer;
	fd_unlock(fd);

	process_lock(timer->process);
	if ((timer->state == TIMER_STATE_SIGNAL) ||
	    (timer->state == TIMER_STATE_WAIT)) {
		timer_set(timer, flags, initial, period);
		err = EOK;
	} else {
		err = ESTATE;
	}
	process_unlock(timer->process);

	return err;
}

err_t sys_timer_get(
	ulong_t fd_index,
	sys_timeout_t *remaining_user,
	sys_time_t *period_user)
{
	sys_timeout_t remaining;
	sys_time_t period;
	struct timer *timer;
	struct fd *fd;
	err_t err;

	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_TIMERFD, &fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	timer = fd->timerfd.timer;
	fd_unlock(fd);

	process_lock(timer->process);
	if ((timer->state == TIMER_STATE_SIGNAL) ||
	    (timer->state == TIMER_STATE_WAIT)) {
		timer_get(timer, &remaining, &period);
		err = EOK;
	} else {
		err = ESTATE;
	}
	process_unlock(timer->process);

	if (err != EOK) {
		return err;
	}

	if (remaining_user != NULL) {
		err = user_put_8(remaining_user, remaining);
		if (err != EOK) {
			return err;
		}
	}

	if (period_user != NULL) {
		err = user_put_8(period_user, period);
	}

	return err;
}

err_t sys_timer_overrun(
	ulong_t fd_index,
	unsigned int *overrun_count_user)
{
	unsigned int overrun_count;
	struct timer *timer;
	struct fd *fd;
	err_t err;

	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_TIMERFD, &fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	timer = fd->timerfd.timer;
	fd_unlock(fd);

	process_lock(timer->process);
	if ((timer->state == TIMER_STATE_SIGNAL) ||
	    (timer->state == TIMER_STATE_WAIT)) {
		timer_overrun(timer, &overrun_count);
		err = EOK;
	} else {
		err = ESTATE;
	}
	process_unlock(timer->process);

	if ((err == EOK) && (overrun_count_user != NULL)) {
		err = user_put_4(overrun_count_user, overrun_count);
	}

	return err;
}

err_t sys_timer_wait(
	ulong_t fd_index,
	ulong_t wait,
	uint64_t *expiry_count_user)
{
	struct timer *timer;
	struct thread *thr;
	struct fd *fd;
	err_t err;

	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_TIMERFD, &fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	timer = fd->timerfd.timer;
	fd_unlock(fd);

	/* need the pointer after blocking as well */
	thr = current_thread();

	thr->cont_args.timer_wait.expiry_count_user = expiry_count_user;
	thr->cont_args.timer.expiry_count = 0;

	process_lock(timer->process);
	if ((timer->state == TIMER_STATE_SIGNAL) ||
	    (timer->state == TIMER_STATE_WAIT)) {
		err = timer_wait(timer, wait, &thr->cont_args.timer.expiry_count);
	} else {
		err = ESTATE;
	}
	process_unlock(timer->process);

	if ((err == EOK) && (expiry_count_user != NULL)) {
		err = user_put_8(expiry_count_user, thr->cont_args.timer.expiry_count);
	}

	return err;
}

static err_t timerfd_ipc_io_read(struct fd *fd, struct thread *thr)
{
	uint64_t expiry_count;
	struct timer *timer;
	uint32_t oflags;
	err_t err;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_TIMERFD);
	assert(thr == current_thread());

	if ((thr->ipc.msg.flags & SYS_IPC_BUF0W) == 0) {
		/* invalid message mode */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	/* clear flags for reply, flags no longer needed */
	thr->ipc.msg.flags = 0;

	if (thr->ipc.msg.h32 != 0) {
		/* reserved flags */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	if (thr->ipc.msg.h8 != SEEK_CUR) {
		/* pread() not supported */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	if (thr->ipc.msg.size0 < 8) {
		/* receive buffer too small */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	thr->ipc.msg.size0 = 0;

	/* any pending timer events will be consumed now */
	epoll_notify(fd, 0);

	timer = fd->timerfd.timer;
	oflags = fd->oflags;

	/* NOTE: we unlock the fd early */
	fd_unlock(fd);

	/* prepare for potential blocking, but do not touch thr->cont_args,
	 * these were already set up by the IPC call
	 */
	thr->cont_args.timer.expiry_count = 0;

	process_lock(timer->process);

	assert(timer != NULL);
	assert(timer->state == TIMER_STATE_WAIT);
	assert(timer->sched != NULL);
	assert(timer->fd != NULL);

	expiry_count = timer->expiry_count;
	if (expiry_count != 0) {
		/* timer already expired in the past */
		/* reset expiry count after reading */
		timer->expiry_count = 0;

		thr->ipc.msg.err = EOK;
		err = EOK;
	} else if ((oflags & O_NONBLOCK) != 0) {
		/* non-blocking read */
		thr->ipc.msg.err = EAGAIN;
		err = EOK;
	} else {
		/* waiting */
		list_node_init(&thr->cont_args.timer.timer_waitql);
		list_insert_last(&timer->timer_waitqh, &thr->cont_args.timer.timer_waitql);

		sched_lock(timer->sched);
		thread_wait(thr, THREAD_STATE_WAIT_TIMER, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
		thread_continue_at(thr, timerfd_wait_finish);
		thr->wakeup_code = ETIMEDOUT;
		thread_cancel_at(thr, timer_wait_cancel);
		sched_unlock(timer->sched);

		thr->ipc.msg.err = EINTR;
		err = EINTR;
	}

	process_unlock(timer->process);

	if (expiry_count != 0) {
		assert(err == EOK);
		err = fd_ipc_copy_out(thr, &expiry_count, sizeof(expiry_count));
		thr->ipc.msg.err = err;
		return EOK;
	}

	assert((err == EINTR) || (err == EOK));
	return err;
}

static void timerfd_wait_finish(struct thread *thr)
{
	struct sys_ipc *recv_msg_user;
	err_t err;

	err = thr->wakeup_code;
	if (err != EOK) {
		assert((err == EINTR) || (err == ETIMEDOUT));
	}

	if (err == EOK) {
		/* copy out read() data */
		err = fd_ipc_copy_out(thr, &thr->cont_args.timer.expiry_count, sizeof(thr->cont_args.timer.expiry_count));
		thr->ipc.msg.err = err;
		err = EOK;
	}

	/* copy out the IPC message in any case */
	recv_msg_user = thr->cont_args.timer.recv_msg_user;
	assert(recv_msg_user != NULL);
	err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));

	arch_syscall_retval(thr->regs, err);
}

/* NOTE: timerfd has no support for write(), only for read() */
static err_t (* const timerfd_ipc_io[])(struct fd *fd, struct thread *thr) = {
	fd_ipc_io_getoflags,
	fd_ipc_io_setoflags,
	fd_ipc_io_default_err, /* d_open, */
	fd_ipc_io_stat_dummy,
	timerfd_ipc_io_read,
	fd_ipc_io_default_err, /* sentinel */
};

/** dispatcher for IPC-based I/O requests */
err_t timerfd_ipc_call(struct fd *fd)
{
	struct sys_ipc *recv_msg_user;
	struct thread *thr;
	size_t req;
	err_t err;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_TIMERFD);
	assert_fd_locked(fd);

	thr = current_thread();

	req = thr->ipc.msg.req;
	if (req > countof(timerfd_ipc_io) - 1) {
		req = countof(timerfd_ipc_io) - 1;
	}

	err = timerfd_ipc_io[req](fd, thr);

	/* NOTE: timerfd_ipc_io_read() already unlocked fd because of process_lock */
	if (req != IPC_IO_READ) {
		fd_unlock(fd);
	}

	/* copy out message (no blocking in this cases) */
	if (err == EOK) {
		recv_msg_user = thr->cont_args.timer.recv_msg_user;
		assert(recv_msg_user != NULL);
		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
	}

	return err;
}
