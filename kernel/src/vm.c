/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * vm.c
 *
 * Higher-level layers of a POSIX-like virtual memory management.
 *
 * azuepke, 2021-11-03: initial
 * azuepke, 2022-09-08: IPC-based in-kernel I/O server for memrq
 */

#include <vm.h>
#include <marron/error.h>
#include <stdbool.h>
#include <adspace_types.h>
#include <adspace.h>
#include <memrq_types.h>
#include <vma.h>
#include <assert.h>
#include <memrq.h>
#include <sys_proto.h>
#include <part.h>
#include <uaccess.h>
#include <string.h>
#include <fd.h>
#include <thread.h>
#include <marron/macros.h>
#include <marron/fcntl.h>
#include <marron/cache_type.h>
#include <process.h>


/* non-allocating mmap() operation (like ioremap() in Linux) */
err_t vm_map(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot,
	unsigned int max_prot,
	const struct memrq_cfg *m,
	size_t offset,
	unsigned int flags,
	addr_t *map_addr)
{
	phys_addr_t phys;
	struct vma *vma;
	err_t err;

	assert(adspace != NULL);
	assert(m != NULL);
	assert(map_addr != NULL);

	vma = vma_create(adspace, addr, size,
	                 (flags & MAP_FIXED) != 0,
	                 (flags & MAP_EXCL) == 0);
	if (vma == NULL) {
		return ENOMEM;
	}

	*map_addr = vma->start;

	phys = m->phys + offset;
	vma_set_type(vma, VMA_IO, prot, max_prot, m->cache, m, phys);

	/* linear mapping without reference counting */
	err = arch_adspace_map(adspace, vma->start, vma->size,
	                       vma->offset, vma->curr_prot, vma->cache);

	err = vma_finalize(adspace, vma, err);

	return err;
}

/* non-allocating but ref-counting mmap() operation */
err_t vm_ref_map(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot,
	unsigned int max_prot,
	const struct memrq_cfg *m,
	size_t offset,
	unsigned int flags,
	addr_t *map_addr)
{
	phys_addr_t phys;
	struct vma *vma;
	addr_t end;
	err_t err;

	assert(adspace != NULL);
	assert(m != NULL);
	assert(map_addr != NULL);

	vma = vma_create(adspace, addr, size,
	                 (flags & MAP_FIXED) != 0,
	                 (flags & MAP_EXCL) == 0);
	if (vma == NULL) {
		return ENOMEM;
	}

	*map_addr = vma->start;

	vma_set_type(vma, VMA_MEM, prot, max_prot, m->cache, m, offset);

	/* linear mapping with reference counting */
	err = EOK;
	addr = vma->start;
	end = vma->start + vma->size;
	phys = vma->memrq_cfg->phys + vma->offset;
	while (addr < end) {
		memrq_page_ref(vma->memrq_cfg, phys);

		err = arch_adspace_map(adspace, addr, PAGE_SIZE,
		                       phys, vma->curr_prot, vma->cache);
		if (err != EOK) {
			memrq_page_unref(vma->memrq_cfg, phys);
			break;
		}

		addr += PAGE_SIZE;
		phys += PAGE_SIZE;
	}

	err = vma_finalize(adspace, vma, err);

	return err;
}

/* allocating mmap() operation (like in POSIX) */
err_t vm_alloc_map(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot,
	unsigned int max_prot,
	const struct memrq_cfg *m,
	unsigned int flags,
	addr_t *map_addr)
{
	phys_addr_t phys;
	struct vma *vma;
	addr_t end;
	err_t err;

	assert(adspace != NULL);
	assert(m != NULL);
	assert(map_addr != NULL);

	vma = vma_create(adspace, addr, size,
	                 (flags & MAP_FIXED) != 0,
	                 (flags & MAP_EXCL) == 0);
	if (vma == NULL) {
		return ENOMEM;
	}

	*map_addr = vma->start;

	vma_set_type(vma, VMA_ANON, prot, max_prot, m->cache, m, 0);

	/* page-by-page allocation with reference counting */
	err = EOK;
	addr = vma->start;
	end = vma->start + vma->size;
	while (addr < end) {
		phys = memrq_page_alloc(vma->memrq_cfg);
		if (phys == PART_MEMRQ_ALLOC_FAIL) {
			err = ESTATE;
			break;
		}

		err = arch_adspace_map(adspace, addr, PAGE_SIZE,
		                       phys, vma->curr_prot, vma->cache);
		if (err != EOK) {
			memrq_page_unref(vma->memrq_cfg, phys);
			break;
		}

		addr += PAGE_SIZE;
	}

	err = vma_finalize(adspace, vma, err);

	return err;
}

/* create a mapping guard */
err_t vm_guard(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int flags,
	addr_t *map_addr)
{
	struct vma *vma;
	err_t err;

	assert(adspace != NULL);
	assert(map_addr != NULL);

	vma = vma_create(adspace, addr, size,
	                 (flags & MAP_FIXED) != 0,
	                 (flags & MAP_EXCL) == 0);
	if (vma == NULL) {
		return ENOMEM;
	}

	*map_addr = vma->start;

	vma_set_type(vma, VMA_GUARD, PROT_NONE, PROT_NONE, CACHE_TYPE_UC, NULL, 0);

	/* guards have no mappings */

	err = vma_finalize(adspace, vma, EOK);

	return err;
}

/* full-service (unref-ing) munmap() operation (like in POSIX)
 *
 * returns an error code (can fail with out of memory error)
 */
err_t vm_unmap(
	struct adspace *adspace,
	addr_t addr,
	size_t size)
{
	struct vma *vma_prev, *vma_next;
	err_t err;

	assert(adspace != NULL);
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);

	vma_prev = vma_find_exact_addr_or_before(adspace, addr);
	assert(vma_prev->start <= addr);
	if (addr < vma_end(vma_prev)) {
		/* overlaps with prev */
		vma_prev = vma_unmap(adspace, vma_prev, addr, size);
		if (vma_prev == NULL) {
			/* out of memory while splitting the VMA */
			return ENOMEM;
		}
	}

	while (addr + size > vma_next_start(vma_prev)) {
		/* overlaps with next */
		vma_next = vma_prev->vma_next;
		assert(vma_next != NULL);
		vma_next = vma_unmap(adspace, vma_next, addr, size);
		/* this will never fail: splitting a VMA cannot happen here
		 * (we cut a VMA on the left side or completely remove it),
		 * and the "next" node is never the NULL guard.
		 */
		assert(vma_next != NULL);
		(void)vma_next;
	}

	/* NOTE: flush page tables as well */
	err = arch_adspace_unmap(adspace, addr, size);
	/* cannot fail */
	assert(err == EOK);

	return err;
}

/** change protection in virtual address space (mprotect())
 *
 * returns an error code (can fail with permission error or out of memory error)
 */
err_t vm_protect(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot)
{
	struct vma *vma;
	addr_t visited;

	assert(adspace != NULL);
#if ARCH_USER_BASE > 0
	assert(addr >= ARCH_USER_BASE);
#endif
	assert(size > 0);
	assert(addr < addr + size);
	assert(addr + size <= ARCH_USER_END);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);

	vma = vma_find_exact_addr_or_before(adspace, addr);
	assert(vma != NULL);

	/* POSIX does not allow unmapped mprotect() on partly mapped memory areas */
	if (vma_end(vma) <= addr) {
		return ENOMEM;
	}
	if (vma->type == VMA_NULL_GUARD) {
		/* the NULL guard cannot be changed */
		return EACCES;
	}

	visited = addr;
	while (vma->start < addr + size) {
		/* overlaps */
		if (vma->start > visited) {
			/* no consecutive mappings */
			return ENOMEM;
		}

		/* guard mappings have PROT_NONE "---" permission */
		if ((prot & vma->max_prot) != prot) {
			/* permission error */
			return EACCES;
		}

		vma = vma_protect(adspace, vma, addr, size, prot);
		if (vma == NULL) {
			/* out of memory while splitting the VMA */
			return ENOMEM;
		}

		visited = vma_end(vma);

		vma = vma->vma_next;
		if (vma == NULL) {
			break;
		}
	}

	/* try to merge the first entry out of range as well */
	if (vma != NULL) {
		/* NOTE: a change to the current permission triggers merging */
		vma = vma_protect(adspace, vma, addr, size, vma->curr_prot);
		assert(vma != NULL);
	}

	if (visited < addr + size) {
		return ENOMEM;
	}
	return EOK;
}

////////////////////////////////////////////////////////////////////////////////

/* callbacks into both the general page allocator code
 * and the architecture-specific page table code
 */

/** unref each mapped memory page in a memory area */
void vm_callback_unref(
	struct adspace *adspace,
	const struct memrq_cfg *m,
	addr_t addr,
	addr_t end)
{
	unsigned int prot;
	phys_addr_t phys;
	addr_t next;
	err_t err;

	assert(adspace != NULL);
	assert(m != NULL);
	assert(addr < end);

	while (addr < end) {
		err = arch_adspace_v2p(adspace, addr, &phys, &prot, &next);
		if (err == EOK) {
			memrq_page_unref(m, phys);
		}
		addr = next;
	}
}

/** unmap pages in page tables (no longer backed by a VMA) */
void vm_callback_unmap(
	struct adspace *adspace,
	addr_t addr,
	size_t size)
{
	err_t err;

	assert(adspace != NULL);

	err = arch_adspace_unmap(adspace, addr, size);
	/* cannot fail */
	assert(err == EOK);

	(void)err;
}

/** update changed permission in page tables */
void vm_callback_protchange(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot)
{
	err_t err;

	assert(adspace != NULL);

	err = arch_adspace_protchange(adspace, addr, size, prot);
	/* cannot fail */
	assert(err == EOK);

	(void)err;
}

////////////////////////////////////////////////////////////////////////////////

/* NOTE: inlining this function leads to a GCC warning (compiler bug)
 * in liveness analysis in sys_memrq_map() in debug compilation:
 * main.c:1345:10: warning: ‘m’ may be used uninitialized in this function
 */
/** resolve memory requirement by ID */
err_t vm_check_memrq(
	ulong_t memrq_type,
	ulong_t memrq_id,
	const struct memrq_cfg **m_p)
{
	const struct part_cfg *cfg;
	const struct memrq_cfg *m;
	unsigned int limit;

	assert(m_p != NULL);

	cfg = current_part()->part_cfg;
	switch (memrq_type) {
	case MEMRQ_TYPE_MEM:
		m = cfg->memrq_cfg;
		limit = cfg->memrq_num;
		break;
	case MEMRQ_TYPE_IO:
		m = cfg->io_map_cfg;
		limit = cfg->io_map_num;
		break;
	case MEMRQ_TYPE_SHM:
		m = cfg->shm_map_cfg;
		limit = cfg->shm_map_num;
		break;
	case MEMRQ_TYPE_FILE:
		m = cfg->file_map_cfg;
		limit = cfg->file_map_num;
		break;
	default:
		/* invalid type */
		return EINVAL;
	}

	if (memrq_id >= limit) {
		/* out of bounds */
		return ELIMIT;
	}

	*m_p = &m[memrq_id];
	return EOK;
}

err_t sys_memrq_name(ulong_t memrq_type, ulong_t memrq_id, char *name_user, size_t size_user)
{
	const struct memrq_cfg *m;
	size_t size;
	err_t err;

	err = vm_check_memrq(memrq_type, memrq_id, &m);
	if (err != EOK) {
		return err;
	}

	size = strlen(m->name) + 1;
	if (size > size_user) {
		return ELIMIT;
	}

	return user_copy_out(name_user, m->name, size);
}

err_t sys_memrq_state(ulong_t memrq_type, ulong_t memrq_id, struct sys_memrq_state *state_user)
{
	const struct memrq_cfg *m;
	struct sys_memrq_state state;
	err_t err;

	err = vm_check_memrq(memrq_type, memrq_id, &m);
	if (err != EOK) {
		return err;
	}

	if (state_user == NULL) {
		/* null pointer */
		return EINVAL;
	}

	/* protected by part_lock, but only "free_pages" changes and is transient */
	state.phys = m->phys;
	state.size = m->size;
	state.unallocated = (m->memrq != NULL) ? access_once(m->memrq->free_pages) * PAGE_SIZE : 0;
	state.prot = m->prot;
	state.cache = m->cache;

	return user_copy_out_aligned(state_user, &state, sizeof(state));
}

/* NOTE: the syscall binding has "addr_p" at a different position */
err_t sys_vm_map_syscall(addr_t addr, size_t size, ulong_t prot, ulong_t flags, addr_t *addr_p, ulong_t fd_index, phys_addr_t offset)
{
	const struct memrq_cfg *m;
	struct adspace *adspace;
	unsigned int fd_oflags;
	unsigned int m_prot;
	struct fd *fd;
	uint32_t max_prot;
	size_t aligned_size;
	addr_t map_addr;
	err_t err;

	if (((offset | addr | size) & (PAGE_SIZE - 1)) != 0) {
		/* not page aligned */
		return EINVAL;
	}
	if (!USER_CHECK_RANGE(addr, size)) {
		/* not valid user space region */
		return EINVAL;
	}
	if (size == 0) {
		/* zero size is not OK */
		return EINVAL;
	}
	if ((prot & ~(PROT_READ|PROT_WRITE|PROT_EXEC)) != 0) {
		/* invalid permissions */
		return EINVAL;
	}

	max_prot = prot | ((flags & MAP_ALLOW_MASK) >> MAP_ALLOW_SHIFT);

	if ((flags & ~(MAP_TYPE|
	               MAP_FIXED|MAP_EXCL|
	               MAP_ANON|
	               MAP_ALLOW_MASK)) != 0) {
		/* invalid flags */
		return EINVAL;
	}

	adspace = current_process()->adspace;

	switch (flags & MAP_TYPE) {
	case MAP_SHARED:
	case MAP_PRIVATE:
		/* mappings backed by a memory requirement */
		if ((flags & MAP_ANON) != 0) {
			/* map anonymous memory (fd and offset ignored) */
			m = current_part()->part_cfg->anon_mmap_memrq;
			if ((m == NULL) || (m->memrq == NULL)) {
				return ENOMEM;
			}

			if (((max_prot | m->prot) & ~m->prot) != 0) {
				/* permissions not matching */
				return EPERM;
			}

			// FIXME: fix locking in mapping
			//part_lock(current_part());
			//adspace_lock(adspace);

			err = vm_alloc_map(adspace, addr, size, prot, max_prot, m, flags, &map_addr);

			//adspace_unlock(adspace);
			//part_unlock(current_part());
		} else {
			/* map memory requirement */
			err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_MEMRQ, &fd);
			if (err != EOK) {
				/* invalid file descriptor OR invalid file type */
				return err;
			}

			fd_oflags = fd->oflags;
			m = fd->file.memrq_cfg;
			fd_unlock(fd);

			/* consider fd->oflags and restrict prot */
			m_prot = m->prot;
			if ((fd_oflags & __O_READ) == 0) {
				m_prot &= ~PROT_READ;
			}
			if ((fd_oflags & __O_WRITE) == 0) {
				m_prot &= ~PROT_WRITE;
			}

			if (((max_prot | m_prot) & ~m_prot) != 0) {
				/* permissions not matching */
				return EPERM;
			}

			aligned_size = ALIGN_UP(m->size, PAGE_SIZE);
			if ((size > aligned_size) || (offset > aligned_size - size)) {
				/* range exceeds memory requirement */
				return ESTATE;
			}

			// FIXME: fix locking in mapping
			//part_lock(current_part());
			//adspace_lock(adspace);

			if (m->memrq != NULL) {
				err = vm_ref_map(adspace, addr, size, prot, max_prot, m, offset, flags, &map_addr);
			} else {
				err = vm_map(adspace, addr, size, prot, max_prot, m, offset, flags, &map_addr);
			}

			//adspace_unlock(adspace);
			//part_unlock(current_part());
		}
		break;

	case MAP_GUARD:
		/* map mapping guard (prot, fd and offset ignored) */

		// FIXME: fix locking in mapping
		//part_lock(current_part());
		//adspace_lock(adspace);

		err = vm_guard(adspace, addr, size, flags, &map_addr);

		//adspace_unlock(adspace);
		//part_unlock(current_part());
		break;

	default:
		/* invalid type */
		err = EINVAL;
		break;
	}


	if (err == EOK) {
		err = user_put_p(addr_p, map_addr);
	}

	return err;
}

err_t sys_vm_unmap(addr_t addr, size_t size)
{
	struct adspace *adspace;
	err_t err;

	if (((addr | size) & (PAGE_SIZE - 1)) != 0) {
		/* not page aligned */
		return EINVAL;
	}
	if (!USER_CHECK_RANGE(addr, size)) {
		/* not valid user space region */
		return EINVAL;
	}
	if (size == 0) {
		/* zero size is not OK */
		return EINVAL;
	}

	adspace = current_process()->adspace;

	// FIXME: fix locking in mapping
	//part_lock(current_part());
	//adspace_lock(adspace);

	err = vm_unmap(adspace, addr, size);

	//adspace_unlock(adspace);
	//part_unlock(current_part());

	return err;
}

err_t sys_vm_protect(addr_t addr, size_t size, ulong_t prot)
{
	struct adspace *adspace;
	err_t err;

	if (((addr | size) & (PAGE_SIZE - 1)) != 0) {
		/* not page aligned */
		return EINVAL;
	}
	if (!USER_CHECK_RANGE(addr, size)) {
		/* not valid user space region */
		return EINVAL;
	}
	if ((prot & ~(PROT_READ|PROT_WRITE|PROT_EXEC)) != 0) {
		/* invalid permissions */
		return EINVAL;
	}

	if (size == 0) {
		/* shortcut: zero size is always OK, regardless of the address */
		return EOK;
	}

	adspace = current_process()->adspace;

	// FIXME: fix locking in mapping
	//part_lock(current_part());
	//adspace_lock(adspace);

	err = vm_protect(adspace, addr, size, prot);

	//adspace_unlock(adspace);
	//part_unlock(current_part());

	return err;
}

err_t sys_vm_v2p(addr_t addr, phys_addr_t *phys_p, unsigned int *prot_p, addr_t *next_p)
{
	struct adspace *adspace;
	unsigned int prot;
	phys_addr_t phys;
	addr_t next;
	err_t err2;
	err_t err;

	if (!USER_CHECK_RANGE(addr, 1UL)) {
		/* not valid user space region */
		return EINVAL;
	}
	if ((phys_p == NULL) || (prot_p == NULL) || (next_p == NULL)) {
		/* null pointers */
		return EINVAL;
	}

	adspace = current_process()->adspace;

	// FIXME: fix locking in mapping
	//adspace_lock(adspace);
	phys = 0;
	prot = 0;
	err = arch_adspace_v2p(adspace, addr, &phys, &prot, &next);
	//adspace_unlock(adspace);

	if (err == ESTATE) {
		goto out_copy_out_next;
	}
	if (err != EOK) {
		goto out_fail;
	}

	err2 = user_put_8(phys_p, phys);
	if (err2 != EOK) {
		err = err2;
		goto out_fail;
	}

	err2 = user_put_4(prot_p, prot);
	if (err2 != EOK) {
		err = err2;
		goto out_fail;
	}

out_copy_out_next:
	err2 = user_put_p(next_p, next);
	if (err2 != EOK) {
		err = err2;
		goto out_fail;
	}

out_fail:
	return err;
}

err_t sys_vm_hint_addr(addr_t addr)
{
	struct adspace *adspace;

	if ((addr & (PAGE_SIZE - 1)) != 0) {
		/* not page aligned */
		return EINVAL;
	}
	if (!USER_CHECK_RANGE(addr, 1UL)) {
		/* not valid user space region */
		return EINVAL;
	}

	adspace = current_process()->adspace;

	// FIXME: fix locking in mapping
	//adspace_lock(adspace);

	adspace->hint_addr = addr;

	//adspace_unlock(adspace);

	return EOK;
}

////////////////////////////////////////////////////////////////////////////////

static err_t memrq_ipc_io_read(struct fd *fd, struct thread *thr)
{
	int64_t /* off_t */ offset;
	size_t size;
	err_t err;

	if ((thr->ipc.msg.flags & SYS_IPC_BUF0W) == 0) {
		/* invalid message mode */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	/* clear flags for reply, flags no longer needed */
	thr->ipc.msg.flags = 0;

	if (thr->ipc.msg.h32 != 0) {
		/* reserved flags */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	if ((fd->oflags & __O_READ) == 0) {
		/* file not opened for reading */
		thr->ipc.msg.err = EBADF;
		return EOK;
	}
	if (fd->priv != MEMRQ_TYPE_FILE) {
		/* file not mapped in kernel (unsuitable for reading) */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	/* seek position */
	if (thr->ipc.msg.h8 == SEEK_CUR) {
		/* read() */
		offset = fd->file.offset;
	} else if (thr->ipc.msg.h8 == SEEK_SET) {
		/* readp() */
		offset = thr->ipc.msg.h64;
		if (offset < 0) {
			/* negative offset */
			thr->ipc.msg.err = EINVAL;
			return EOK;
		}
	} else {
		/* invalid whence */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	/* size */
	size = thr->ipc.msg.size0;
	thr->ipc.msg.size0 = 0;

	if (offset >= fd->file.length) {
		/* beyond the end of the file */
		thr->ipc.msg.err = EOK;
		return EOK;
	}

	if (size > (size_t)(fd->file.length - offset)) {
		size = fd->file.length - offset;
	}

	err = fd_ipc_copy_out(thr, (const char *)(addr_t)(fd->file.memrq_cfg->addr + offset), size);

	/* update file pointer when necessary */
	if (err == EOK) {
		if (thr->ipc.msg.h8 == SEEK_CUR) {
			fd->file.offset += thr->ipc.msg.size0;
		}
	}

	thr->ipc.msg.err = err;
	return EOK;
}

static err_t memrq_ipc_io_mmap(struct fd *fd, struct thread *thr)
{
	int64_t /* off_t */ offset;
	struct adspace *adspace;
	unsigned int map_flags;
	uint64_t aligned_size;
	unsigned int prot;
	uint32_t max_prot;
	addr_t map_addr;
	size_t size;
	err_t err;

	if ((thr->ipc.msg.flags & SYS_IPC_MAP) == 0) {
		/* not a mapping IPC */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	/* clear flags for reply, flags no longer needed */
	thr->ipc.msg.flags = 0;

	/* the kernel already validated for us:
	 * - msg->h8: prot
	 * - msg->h32: flags
	 * - msg->buf0: address
	 * - msg->size0: length
	 */

	size = thr->ipc.msg.map_size;
	offset = thr->ipc.msg.h64;
	if ((offset < 0) || ((offset & (PAGE_SIZE - 1)) != 0)) {
		/* negative or unaligned offset */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	if ((uint64_t)offset + size < (uint64_t)offset) {
		/* address space overflow */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	aligned_size = ALIGN_UP(fd->file.length, PAGE_SIZE);
	if ((uint64_t)offset + size > aligned_size) {
		/* ENXIO covers all errors with the mappable range */
		thr->ipc.msg.err = ENXIO;
		return EOK;
	}

	prot = thr->ipc.msg.map_prot;
	if (((prot & (PROT_READ | PROT_EXEC)) != 0) && ((fd->oflags & __O_READ) == 0)) {
		/* file not open for reading */
		thr->ipc.msg.err = EACCES;
		return EOK;
	}
	if (((prot & PROT_WRITE) != 0) && ((fd->oflags & __O_WRITE) == 0)) {
		/* file not open for writing */
		thr->ipc.msg.err = EACCES;
		return EOK;
	}
	if (((prot & PROT_EXEC) != 0) && ((fd->file.memrq_cfg->prot & PROT_EXEC) == 0)) {
		/* as device driver, we do not support executable mappings */
		thr->ipc.msg.err = ENOTSUP;
		return EOK;
	}

	map_flags = thr->ipc.msg.map_flags;
	max_prot = prot | ((map_flags & MAP_ALLOW_MASK) >> MAP_ALLOW_SHIFT);

	adspace = thr->process->adspace;

	// FIXME: fix locking in mapping
	//adspace_lock(adspace);

	if (fd->file.memrq_cfg->memrq != NULL) {
		err = vm_ref_map(adspace, thr->ipc.msg.map_addr, thr->ipc.msg.map_size,
	                 prot, max_prot,
	                 fd->file.memrq_cfg, offset, map_flags, &map_addr);
	} else {
		err = vm_map(adspace, thr->ipc.msg.map_addr, thr->ipc.msg.map_size,
	                 prot, max_prot,
	                 fd->file.memrq_cfg, offset, map_flags, &map_addr);
	}

	//adspace_unlock(adspace);

	if (err == EOK) {
		thr->ipc.msg.flags = SYS_IPC_MAP;
		thr->ipc.msg.map_start = map_addr;
	}

	thr->ipc.msg.err = err;
	return EOK;
}

/* NOTE: There is no callback for write(). Also, we only support read() for ROM
 * files, because these are the only memrqs which are mapped in kernel space.
 * In contrast, mmap() is based on the physical address and is always available.
 */
static err_t (* const memrq_ipc_io[])(struct fd *fd, struct thread *thr) = {
	fd_ipc_io_getoflags,
	fd_ipc_io_setoflags,
	fd_ipc_io_default_err, /* d_open, */
	fd_ipc_io_stat_dummy,
	memrq_ipc_io_read,
	fd_ipc_io_default_err, /* d_write, */
	fd_ipc_io_default_err, /* d_ioctl, */
	memrq_ipc_io_mmap,
	fd_ipc_io_default_err, /* d_mprotect, */
	fd_ipc_io_default_err, /* d_munmap, */
	fd_ipc_io_seek_file,
	fd_ipc_io_default_err, /* sentinel */
};

/** dispatcher for IPC-based I/O requests */
err_t memrq_ipc_call(struct fd *fd)
{
	struct sys_ipc *recv_msg_user;
	struct thread *thr;
	size_t req;
	err_t err;

	assert(fd != NULL);
	assert_fd_locked(fd);

	thr = current_thread();

	req = thr->ipc.msg.req;
	if (req > countof(memrq_ipc_io) - 1) {
		req = countof(memrq_ipc_io) - 1;
	}

	err = memrq_ipc_io[req](fd, thr);

	fd_unlock(fd);

	/* copy out message */
	if (err == EOK) {
		recv_msg_user = thr->cont_args.ipc_wait.recv_msg_user;
		assert(recv_msg_user != NULL);
		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
	}

	return err;
}

err_t sys_memrq_open(ulong_t memrq_type, ulong_t memrq_id, ulong_t flags, uint32_t *fd_index_user)
{
	const struct memrq_cfg *m;
	uint32_t fd_index;
	struct fd *fd;
	int cloexec;
	err_t err;

	if ((flags & ~(SYS_MEMRQ_READ | SYS_MEMRQ_WRITE | SYS_MEMRQ_CLOEXEC)) != 0) {
		/* reserved flags */
		return EINVAL;
	}
	if (fd_index_user == NULL) {
		/* NULL pointer */
		return EINVAL;
	}
	err = vm_check_memrq(memrq_type, memrq_id, &m);
	if (err != EOK) {
		return err;
	}

	if (((flags & SYS_MEMRQ_READ) != 0) && ((m->prot & PROT_READ) == 0)) {
		/* invalid access permissions */
		return EACCES;
	}
	if (((flags & SYS_MEMRQ_WRITE) != 0) && ((m->prot & PROT_WRITE) == 0)) {
		/* invalid access permissions */
		return EACCES;
	}

	cloexec = (flags & SYS_MEMRQ_CLOEXEC) != 0;
	fd = fd_alloc(current_process());
	if (fd == NULL) {
		/* failed to allocate a new file descriptor */
		return ENFILE;
	}

	/* further initialization */
	fd->type = FD_TYPE_MEMRQ;
	if ((flags & SYS_MEMRQ_READ) != 0) {
		fd->oflags |= __O_READ;
	}
	if ((flags & SYS_MEMRQ_WRITE) != 0) {
		fd->oflags |= __O_WRITE;
	}
	fd->flags = FD_FLAG_SHAREABLE;
	fd->priv = memrq_type;
	fd->file.offset = 0;
	fd->file.length = m->size;
	fd->file.memrq_cfg = m;

	err = fd_dup(current_process(), fd, cloexec, &fd_index);
	if (err != EOK) {
		fd->type = FD_TYPE_DEAD;
		fd_close(fd);
	}

	err = user_put_4(fd_index_user, fd_index);
	/* NOTE: any failure here is ignored and resources are not freed
	 * (the file descriptor can be accessed by user space nevertheless)
	 */

	return err;
}

/** close callback for memory requirements */
void memrq_close(
	struct fd *fd)
{
	assert(fd != NULL);
	assert(fd->type == FD_TYPE_MEMRQ);
	assert_fd_locked(fd);

	(void)fd;
}
