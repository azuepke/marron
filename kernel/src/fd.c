/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024, 2025 Alexander Zuepke */
/*
 * fd.c
 *
 * File descriptor management.
 *
 * azuepke, 2022-07-02: initial
 * azuepke, 2022-09-08: default I/O operations for IPC-based in-kernel servers
 * azuepke, 2025-02-21: rework fd lookup
 */

#include <fd.h>
#include <kernel.h>
#include <assert.h>
#include <stddef.h>
#include <marron/error.h>
#include <marron/bit.h>
#include <part_types.h>
#include <part.h>
#include <uaccess.h>
#include <sys_proto.h>
#include <ipc.h>
#include <thread.h>
#include <marron/ipc_io.h>
#include <marron/fcntl.h>
#include <marron/stat.h>
#include <memrq.h>
#include <epoll.h>
#include <eventfd.h>
#include <timer.h>
#include <irq.h>
#include <signalfd.h>
#include <process.h>


/* forward declarations */
/** close() implementation, handles fd->usage == 0, fd must be locked */
static void fd_close_locked(
	struct part *part,
	struct fd *fd);

/** initialize file descriptors of a partition at boot time */
__init void fd_init_all(
	struct part *part)
{
	/* initialize fds to FD_TYPE_DEAD state */
	for (unsigned int i = 0; i < part->part_cfg->fd_num; i++) {
		struct fd *fd = &part->part_cfg->fd[i];

		spin_init(&fd->lock);
		fd->usage = 0;
		fd->part_id = part->part_id;

		list_node_init(&fd->dead.fd_freeql);
		list_insert_last(&part->fd_freeqh, &fd->dead.fd_freeql);
		fd->type = FD_TYPE_DEAD;
	}
}

////////////////////////////////////////////////////////////////////////////////

/** set bit in fd_table_used_bitmap (set bit indicates entry is used) */
static inline void fd_table_used_bitmap_set(
	struct process *proc,
	unsigned long index,
	int state)
{
	unsigned int word;
	unsigned int bit;

	assert(proc != NULL);
	assert(index < PROCESS_FD_NUM);

	word = index / FD_BITMAP_BITS_PER_WORD;
	bit = index % FD_BITMAP_BITS_PER_WORD;
	if (state) {
		proc->fd_table_used_bitmap[word] |= (1U << bit);
	} else {
		proc->fd_table_used_bitmap[word] &= ~(1U << bit);
	}
}

/** find one free entry in fd_table_used_bitmap, ordered by lowest index */
static inline unsigned int fd_table_used_bitmap_find_free(
	struct process *proc,
	unsigned int start_index)
{
	unsigned int startmask;
	unsigned int startbit;
	unsigned int freebits;
	unsigned int word;
	unsigned int bit;

	assert(proc != NULL);

	word = start_index / FD_BITMAP_BITS_PER_WORD;
	startbit = start_index % FD_BITMAP_BITS_PER_WORD;
	/* all bits before the start bit are used */
	startmask = (1U << startbit) - 1;

	for (; word < FD_BITMAP_WORDS(PROCESS_FD_NUM); word++) {
		freebits = ~(proc->fd_table_used_bitmap[word] | startmask);
		if (freebits != 0) {
			bit = bit_fls(freebits);
			return word * FD_BITMAP_BITS_PER_WORD + bit;
		}
		startmask = 0;
	}

	/* fd_table full -> EMFILE */
	return -1u;
}

////////////////////////////////////////////////////////////////////////////////

/** get bit in fd_table_cloexec_bitmap */
static inline int fd_table_cloexec_bitmap_get(
	struct process *proc,
	unsigned long index)
{
	unsigned int word;
	unsigned int bit;

	assert(proc != NULL);
	assert(index < PROCESS_FD_NUM);

	word = index / FD_BITMAP_BITS_PER_WORD;
	bit = index % FD_BITMAP_BITS_PER_WORD;
	return (proc->fd_table_cloexec_bitmap[word] & (1U << bit)) != 0;
}

/** set bit in fd_table_cloexec_bitmap */
static inline void fd_table_cloexec_bitmap_set(
	struct process *proc,
	unsigned long index,
	int state)
{
	unsigned int word;
	unsigned int bit;

	assert(proc != NULL);
	assert(index < PROCESS_FD_NUM);

	word = index / FD_BITMAP_BITS_PER_WORD;
	bit = index % FD_BITMAP_BITS_PER_WORD;
	if (state) {
		proc->fd_table_cloexec_bitmap[word] |= (1U << bit);
	} else {
		proc->fd_table_cloexec_bitmap[word] &= ~(1U << bit);
	}
}

////////////////////////////////////////////////////////////////////////////////

static inline struct fd *fd_get(
	struct process *proc,
	ulong_t fd_index)
{
	struct fd *fd;

	assert(proc != NULL);

	if (fd_index < PROCESS_FD_NUM) {
		fd = proc->fd_table[fd_index];
	} else if ((fd_index - FD_STATIC) < proc->part->part_cfg->fd_static_table_num) {
		fd = proc->part->part_cfg->fd_static_table[fd_index - FD_STATIC];
	} else {
		/* fd out of limits */
		fd = NULL;
	}

	return fd;
}

static inline err_t fd_check_non_static(
	struct process *proc,
	ulong_t fd_index)
{
	if (fd_index < PROCESS_FD_NUM) {
		/* OK */
		return EOK;
	} else if ((fd_index - FD_STATIC) < proc->part->part_cfg->fd_static_table_num) {
		/* static fd cannot be changed */
		return ESTATE;
	} else {
		/* fd out of limits */
		return EBADF;
	}
}

/** get and lock used fd */
struct fd *fd_get_and_lock(
	struct process *proc,
	ulong_t fd_index)
{
	struct fd *fd;

	assert(proc != NULL);

	process_lock(proc);

	fd = fd_get(proc, fd_index);

	if (fd != NULL) {
		fd_lock(fd);
	}

	process_unlock(proc);

	return fd;
}

/** get and lock fd of specific type */
err_t fd_get_type_and_lock(
	struct process *proc,
	ulong_t fd_index,
	unsigned int fd_type,
	struct fd **fd_p)
{
	struct fd *fd;

	assert(proc != NULL);
	assert(fd_type <= FD_TYPE_LAST);
	assert(fd_p != NULL);

	fd = fd_get_and_lock(proc, fd_index);
	if (fd == NULL) {
		return EBADF;
	}

	if (fd->type != fd_type) {
		fd_unlock(fd);
		return EINVAL;
	}

	*fd_p = fd;
	return EOK;
}

/** get and lock two used fds (preventing deadlocks) */
err_t fd_get_and_lock2(
	struct process *proc,
	ulong_t fd_index0,
	ulong_t fd_index1,
	struct fd **fd0_p,
	struct fd **fd1_p)
{
	struct fd *fd0;
	struct fd *fd1;
	err_t err;

	assert(proc != NULL);
	assert(fd0_p != NULL);
	assert(fd1_p != NULL);

	process_lock(proc);

	fd0 = fd_get(proc, fd_index0);
	fd1 = fd_get(proc, fd_index1);

	if ((fd0 != NULL) && (fd1 != NULL)) {
		if (fd0 != fd1) {
			fd_lock2(fd0, fd1);

			*fd0_p = fd0;
			*fd1_p = fd1;
			err = EOK;
		} else {
			/* same file descriptor */
			err = EINVAL;
		}
	} else {
		/* fd not open */
		err = EBADF;
	}

	process_unlock(proc);

	return err;
}

/** lock two fds (preventing deadlocks) */
void fd_lock2(struct fd *fd0, struct fd *fd1)
{
	assert(fd0 != NULL);
	assert(fd1 != NULL);
	assert(fd0 != fd1);

	/* lock both file descriptors in ascending order of addresses
	 * to prevent deadlocks
	 */
	if (fd0 < fd1) {
		fd_lock(fd0);
		fd_lock(fd1);
	} else {
		fd_lock(fd1);
		fd_lock(fd0);
	}
}

/** correct nested locking of first proc, then fd */
void fd_relock_with_proc(
	struct process *proc,
	struct fd *fd)
{
	assert(proc != NULL);
	assert(fd != NULL);

	fd->usage++;
	fd_unlock(fd);

	process_lock(proc);

	fd_lock(fd);
}

/** nested unlocking of part and fd */
void fd_unlock_with_proc(
	struct process *proc,
	struct fd *fd)
{
	int take_fast_path;

	assert(proc != NULL);
	assert(fd != NULL);

	take_fast_path = (fd->usage >= 2);
	if (take_fast_path) {
		fd->usage--;
	}

	fd_unlock(fd);
	process_unlock(proc);

	if (!take_fast_path) {
		fd_close_locked(proc->part, fd);
	}
}

////////////////////////////////////////////////////////////////////////////////

/** allocate an unused fd (the fd is unlocked; return ENFILE on failure) */
struct fd *fd_alloc(
	struct process *proc)
{
	struct part *part;
	struct fd *fd;
	list_t *node;

	assert(proc != NULL);

	part = proc->part;
	part_lock(part);

	node = list_remove_first(&part->fd_freeqh);
	if (node != NULL) {
		fd = FD_FROM_FREEQ(node);

		/* perform basic initialization */
		assert(fd->usage == 0);
		fd->usage = 1;
		assert(fd->part_id == part->part_id);
		assert(fd->type == FD_TYPE_DEAD);
		fd->cached_eventmask = EPOLLNVAL;	/* epoll inhibited */
		fd->priv = 0;
		fd->flags = 0;
		fd->oflags = 0;
		fd->oflags_mask = 0;
		list_head_init(&fd->epoll_fdqh);
	} else {
		fd = NULL;
	}

	part_unlock(part);

	return fd;
}

/** free an fd with no further references */
static inline void fd_free(
	struct part *part,
	struct fd *fd)
{
	assert(part != NULL);
	assert(fd != NULL);

	part_lock(part);

	assert(fd->usage == 0);
	assert(fd->part_id == part->part_id);
	assert(fd->type == FD_TYPE_DEAD);

	list_node_init(&fd->dead.fd_freeql);
	list_insert_last(&part->fd_freeqh, &fd->dead.fd_freeql);

	part_unlock(part);
}

/** internal dup functionality (proc is locked; fd is unlocked) */
err_t fd_dup_locked(
	struct process *proc,
	struct fd *fd,
	int cloexec,
	unsigned int *fd_index_p)
{
	unsigned int fd_index;

	assert(proc != NULL);
	assert_process_locked(proc);
	assert(fd != NULL);
	assert(fd_index_p != NULL);

	fd_index = fd_table_used_bitmap_find_free(proc, 0);
	if (fd_index == -1u) {
		/* fd_table full */
		return EMFILE;
	}
	assert(fd_index < PROCESS_FD_NUM);

	assert(proc->fd_table[fd_index] == NULL);
	proc->fd_table[fd_index] = fd;
	fd_table_used_bitmap_set(proc, fd_index, 1);
	fd_table_cloexec_bitmap_set(proc, fd_index, cloexec);
	assert(fd->usage >= 1);

	*fd_index_p = fd_index;

	return EOK;
}

/** make a new unlocked fd visible and active (proc is unlocked) */
err_t fd_dup(
	struct process *proc,
	struct fd *fd,
	int cloexec,
	unsigned int *fd_index_p)
{
	err_t err;

	assert(proc != NULL);
	assert(fd != NULL);
	assert(fd_index_p != NULL);

	process_lock(proc);

	err = fd_dup_locked(proc, fd, cloexec, fd_index_p);

	process_unlock(proc);

	return err;
}

/** make two unlocked fds visible and active (proc is unlocked) */
err_t fd_dup2(
	struct process *proc,
	struct fd *fd0,
	struct fd *fd1,
	int cloexec,
	unsigned int fd_indices[])
{
	unsigned int fd_index0;
	unsigned int fd_index1;
	err_t err;

	assert(proc != NULL);
	assert(fd0 != NULL);
	assert(fd1 != NULL);
	assert(fd_indices != NULL);

	process_lock(proc);

	fd_index0 = fd_table_used_bitmap_find_free(proc, 0);
	if (fd_index0 == -1u) {
		/* fd_table full */
		err = EMFILE;
		goto out;
	}
	fd_index1 = fd_table_used_bitmap_find_free(proc, fd_index0+1);
	if (fd_index1 == -1u) {
		/* fd_table full */
		err = EMFILE;
		goto out;
	}

	assert(fd_index0 < fd_index1);
	assert(fd_index1 < PROCESS_FD_NUM);
	assert(proc->fd_table[fd_index0] == NULL);
	assert(proc->fd_table[fd_index1] == NULL);

	proc->fd_table[fd_index0] = fd0;
	fd_table_used_bitmap_set(proc, fd_index0, 1);
	fd_table_cloexec_bitmap_set(proc, fd_index0, cloexec);
	assert(fd0->usage >= 1);

	proc->fd_table[fd_index1] = fd1;
	fd_table_used_bitmap_set(proc, fd_index1, 1);
	fd_table_cloexec_bitmap_set(proc, fd_index1, cloexec);
	assert(fd1->usage >= 1);

	fd_indices[0] = fd_index0;
	fd_indices[1] = fd_index1;

	err = EOK;

out:
	process_unlock(proc);

	return err;
}

/** close() implementation */
void fd_close(
	struct fd *fd)
{
	struct part *part;

	assert(fd != NULL);

	fd_lock(fd);

	assert_fd_used(fd);
	fd->usage--;
	if (fd->usage > 0) {
		fd_unlock(fd);
		return;
	}

	assert(fd->part_id != 0);
	assert(fd->part_id < part_num);
	part = part_cfg[fd->part_id].part;
	fd_close_locked(part, fd);
}

/** close() implementation, handles fd->usage == 0, fd must be locked */
static void fd_close_locked(
	struct part *part,
	struct fd *fd)
{
	struct fd *fd2;

	assert(part != NULL);
	assert(fd != NULL);
	assert_fd_locked(fd);
	assert(fd->usage == 0);

	fd2 = NULL;

	switch (fd->type) {
	case FD_TYPE_DEAD:
	default:
		/* already dead, nothing to do */
		break;

	case FD_TYPE_IPC_CLIENT:
		fd2 = ipc_close_client(fd);
		break;

	case FD_TYPE_IPC_SERVER:
		ipc_close_server(fd);
		break;

	case FD_TYPE_MEMRQ:
		memrq_close(fd);
		break;

	case FD_TYPE_EPOLL:
		epoll_close(fd);
		break;

	case FD_TYPE_EVENTFD:
		eventfd_close(fd);
		break;

	case FD_TYPE_TIMERFD:
		timerfd_close(fd);
		break;

	case FD_TYPE_IRQFD:
		irqfd_close(fd);
		break;

	case FD_TYPE_SIGNALFD:
		signalfd_close(fd);
		break;
	}

	epoll_cleanup_notes_on_close(fd);

	fd->type = FD_TYPE_DEAD;
	fd_unlock(fd);

	fd_free(part, fd);

	if (fd2 != NULL) {
		/* special case: notify the server (fd2) that the client (fd) was closed */
		ipc_notify_server_close(fd2, fd);
	}
}

/** close fd_table entries */
err_t fd_table_close(
	struct process *proc,
	ulong_t fd_index)
{
	struct fd *fd;

	assert(proc != NULL);
	assert(fd_index < PROCESS_FD_NUM);

	process_lock(proc);

	fd = proc->fd_table[fd_index];
	if (fd == NULL) {
		/* fd not open */
		process_unlock(proc);
		return EBADF;
	}

	proc->fd_table[fd_index] = NULL;
	fd_table_used_bitmap_set(proc, fd_index, 0);
	fd_table_cloexec_bitmap_set(proc, fd_index, 0);

	process_unlock(proc);

	fd_close(fd);

	return EOK;
}

/** fd_close() syscall */
err_t sys_fd_close(
	ulong_t fd_index)
{
	struct process *proc;
	err_t err;

	proc = current_process();
	err = fd_check_non_static(proc, fd_index);
	if (err != EOK) {
		return err;
	}

	err = fd_table_close(proc, fd_index);

	return err;
}

/** dup() functionality */
err_t sys_fd_dup(
	ulong_t old_fd_index,
	ulong_t start_fd_index,
	ulong_t flags,
	uint32_t *new_fd_index_user)
{
	unsigned int new_fd_index = 0;
	struct process *proc;
	struct fd *fd;
	int cloexec;
	err_t err;

	if ((flags & ~(SYS_FD_CLOEXEC)) != 0) {
		/* invalid attrib */
		return EINVAL;
	}
	cloexec = (flags & SYS_FD_CLOEXEC) != 0;

	proc = current_process();
	if (start_fd_index >= PROCESS_FD_NUM) {
		/* fd out of limits */
		/* NOTE: but we return fd_table full */
		return EMFILE;
	}

	process_lock(proc);

	fd = fd_get(proc, old_fd_index);

	if (fd != NULL) {
		new_fd_index = fd_table_used_bitmap_find_free(proc, start_fd_index);
		if (new_fd_index != -1u) {
			assert(proc->fd_table[new_fd_index] == NULL);
			proc->fd_table[new_fd_index] = fd;
			fd_table_used_bitmap_set(proc, new_fd_index, 1);
			fd_table_cloexec_bitmap_set(proc, new_fd_index, cloexec);

			fd_lock(fd);
			fd->usage++;
			fd_unlock(fd);

			err = EOK;
		} else {
			/* fd_table full */
			err = EMFILE;
		}
	} else {
		/* fd not open */
		err = EBADF;
	}

	process_unlock(proc);

	if (err == EOK) {
		err = user_put_4(new_fd_index_user, new_fd_index);
	}

	return err;
}

/** dup2() functionality */
err_t sys_fd_dup2(
	ulong_t old_fd_index,
	ulong_t new_fd_index,
	ulong_t flags)
{
	struct process *proc;
	struct fd *close_fd;
	struct fd *fd;
	int cloexec;

	if ((flags & ~(SYS_FD_CLOEXEC)) != 0) {
		/* invalid attrib */
		return EINVAL;
	}
	cloexec = (flags & SYS_FD_CLOEXEC) != 0;

	proc = current_process();
	if (new_fd_index >= PROCESS_FD_NUM) {
		/* fd out of limits */
		return EBADF;
	}

	process_lock(proc);

	fd = fd_get(proc, old_fd_index);

	/* NOTE: for dup3() functionality, the user should catch
	 * the old_fd_index == new_fd_index case and return EINVAL
	 */

	if (fd == NULL) {
		/* fd not open */
		process_unlock(proc);
		return EBADF;
	}

	close_fd = proc->fd_table[new_fd_index];
	if (fd == close_fd) {
		/* both already point to the same file */
		/* nothing to do here except to update the cloexec flag */
		close_fd = NULL;
	} else {
		/* prevent race with close() */
		fd_lock(fd);
		fd->usage++;
		fd_unlock(fd);

		/* update fd_table */
		proc->fd_table[new_fd_index] = fd;
		fd_table_used_bitmap_set(proc, new_fd_index, 1);
	}

	fd_table_cloexec_bitmap_set(proc, new_fd_index, cloexec);

	process_unlock(proc);

	/* finally close close_fd */
	if (close_fd != NULL) {
		fd_close(close_fd);
		/* no need to clear the bitmap, etc -- we'll reuse the entry */
	}

	return EOK;
}

err_t sys_fd_cloexec_get(
	ulong_t fd_index,
	uint32_t *attrib_user)
{
	struct process *proc;
	unsigned int val = 0;
	struct fd *fd;
	err_t err;

	proc = current_process();
	err = fd_check_non_static(proc, fd_index);
	if (err != EOK) {
		return err;
	}

	process_lock(proc);

	fd = proc->fd_table[fd_index];
	if (fd != NULL) {
		/* no fd_lock needed */
		val = fd_table_cloexec_bitmap_get(proc, fd_index) ? SYS_FD_CLOEXEC : 0;

		err = EOK;
	} else {
		/* fd not open */
		err = EBADF;
	}

	process_unlock(proc);

	if ((err == EOK) && (attrib_user != NULL)) {
		err = user_put_4(attrib_user, val);
	}

	return err;
}

err_t sys_fd_cloexec_set(
	ulong_t fd_index,
	ulong_t attrib)
{
	struct process *proc;
	struct fd *fd;
	int cloexec;
	err_t err;

	if ((attrib & ~(SYS_FD_CLOEXEC)) != 0) {
		/* invalid attrib */
		return EINVAL;
	}
	cloexec = (attrib & SYS_FD_CLOEXEC) != 0;

	proc = current_process();
	err = fd_check_non_static(proc, fd_index);
	if (err != EOK) {
		return err;
	}

	process_lock(proc);

	fd = proc->fd_table[fd_index];
	if (fd != NULL) {
		/* no fd_lock needed */
		fd_table_cloexec_bitmap_set(proc, fd_index, cloexec);

		err = EOK;
	} else {
		/* fd not open */
		err = EBADF;
	}

	process_unlock(proc);

	return err;
}

////////////////////////////////////////////////////////////////////////////////

/* default errors for IPC-based in-kernel I/O operations */
static const unsigned char ipc_io_default_err[IPC_IO_NUM] = {
	ENOSYS,		/* IPC_IO_GETOFLAGS */
	ENOSYS,		/* IPC_IO_SETOFLAGS */
	ENOSYS,		/* IPC_IO_FS_OPEN */
	EBADF,		/* IPC_IO_STAT */
	EBADF,		/* IPC_IO_READ */
	EBADF,		/* IPC_IO_WRITE */
	ENOTTY,		/* IPC_IO_IOCTL */
	EACCES,		/* IPC_IO_MMAP */
	ENOSYS,		/* IPC_IO_MPROTECT */
	ENOSYS,		/* IPC_IO_MUNMAP */
	ESPIPE,		/* IPC_IO_SEEK */
	EINVAL,		/* IPC_IO_TRUNCATE */
	EINVAL,		/* IPC_IO_ALLOCATE */
	EINVAL,		/* IPC_IO_SYNC */
	EINVAL,		/* IPC_IO_ADVISE */
	EINVAL,		/* IPC_IO_LOCK */
	ENOSYS,		/* IPC_IO_STATVFS */
	ENOSYS,		/* IPC_IO_GETPATH */
	ENOTDIR,	/* IPC_IO_GETDENTS */
	ENOSYS,		/* IPC_IO_FS_ACCESS */
	ENOSYS,		/* IPC_IO_FS_STAT */
	ENOSYS,		/* IPC_IO_FS_RENAME */
	ENOSYS,		/* IPC_IO_FS_REMOVE */
	ENOSYS,		/* IPC_IO_FS_MKDIR */
	ENOSYS,		/* IPC_IO_FS_MKNOD */
	ENOSYS,		/* IPC_IO_FS_LINK */
	ENOSYS,		/* IPC_IO_FS_SYMLINK */
	ENOSYS,		/* IPC_IO_FS_READLINK */
	ENOSYS,		/* IPC_IO_FS_CHMOD */
	ENOSYS,		/* IPC_IO_FS_CHOWN */
	ENOSYS,		/* IPC_IO_FS_UTIME */
	ENOENT,		/* IPC_IO_CHMOD */
	ENOENT,		/* IPC_IO_CHOWN */
	ENOENT,		/* IPC_IO_UTIME */
	ENOTSOCK,	/* IPC_IO_SEND */
	ENOTSOCK,	/* IPC_IO_RECV */
	ENOTSOCK,	/* IPC_IO_ACCEPT */
	ENOTSOCK,	/* IPC_IO_CONNECT */
	ENOTSOCK,	/* IPC_IO_BIND */
	ENOTSOCK,	/* IPC_IO_GETPEERNAME */
	ENOTSOCK,	/* IPC_IO_GETSOCKNAME */
	ENOTSOCK,	/* IPC_IO_LISTEN */
	ENOTSOCK,	/* IPC_IO_SHUTDOWN */
	ENOTSOCK,	/* IPC_IO_GETSOCKOPT */
	ENOTSOCK,	/* IPC_IO_SETSOCKOPT */
};

err_t fd_ipc_io_default_err(struct fd *fd, struct thread *thr)
{
	size_t req;

	(void)fd;

	req = thr->ipc.msg.req;
	if (req < countof(ipc_io_default_err)) {
		thr->ipc.msg.err = ipc_io_default_err[req];
	} else {
		thr->ipc.msg.err = ENOSYS;
	}

	return EOK;
}

err_t fd_ipc_io_getoflags(struct fd *fd, struct thread *thr)
{
	thr->ipc.msg.err = EOK;
	thr->ipc.msg.h32 = fd->oflags;
	return EOK;
}

err_t fd_ipc_io_setoflags(struct fd *fd, struct thread *thr)
{
	fd->oflags &= ~fd->oflags_mask;
	fd->oflags |= thr->ipc.msg.h32 & fd->oflags_mask;

	thr->ipc.msg.err = EOK;
	thr->ipc.msg.h32 = fd->oflags;
	return EOK;
}

err_t fd_ipc_io_stat_dummy(struct fd *fd, struct thread *thr)
{
	/* copy of struct stat from sys/stat.h with kernel-compatible types */
	struct {
		uint64_t /* dev_t */  st_dev;
		uint64_t /* ino_t */  st_ino;
		uint32_t /* mode_t  */ st_mode;
		uint32_t /* nlink_t  */ st_nlink;
		uint32_t /* uid_t  */ st_uid;
		uint32_t /* gid_t  */ st_gid;
		uint64_t /* dev_t */  st_rdev;
		uint64_t /* off_t */ st_size;
		unsigned long st_blksize;
	#if __SIZEOF_LONG__ == 4
		unsigned long padding;
	#endif
		uint64_t /* blkcnt_t */ st_blocks;
		uint64_t /* struct timespec */ st_atim[2];
		uint64_t /* struct timespec */ st_mtim[2];
		uint64_t /* struct timespec */ st_ctim[2];
		uint32_t unused[4];
	} statbuf = { 0 };

	(void)fd;

	/* default settings for stat for a character device */
	statbuf.st_mode = S_IRUSR | S_IWUSR;
	statbuf.st_nlink = 1;
	statbuf.st_blksize = PAGE_SIZE;

	if ((thr->ipc.msg.flags != SYS_IPC_BUF0W) ||
	        (thr->ipc.msg.size0 != sizeof(statbuf))) {
		thr->ipc.msg.err = EFAULT;
		return EOK;
	}

	thr->ipc.msg.err = user_copy_out(thr->ipc.msg.buf0, &statbuf, sizeof(statbuf));
	return EOK;
}

err_t fd_ipc_io_seek_file(struct fd *fd, struct thread *thr)
{
	int64_t offset;

	switch (thr->ipc.msg.h8) {
	case SEEK_SET:
		offset = thr->ipc.msg.h64;
		break;
	case SEEK_CUR:
		offset = fd->file.offset + thr->ipc.msg.h64;
		break;
	case SEEK_END:
		offset = fd->file.length + thr->ipc.msg.h64;
		break;
	default:
		/* invalid whence */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	if (offset < 0) {
		/* negative offset */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	fd->file.offset = offset;

	/* provide new offset in any case */
	thr->ipc.msg.h64 = fd->file.offset;

	thr->ipc.msg.err = EOK;
	return EOK;
}

err_t fd_ipc_copy_in(struct thread *thr, void *dst_kern, size_t size)
{
	char *dst = dst_kern;
	struct sys_iov *iov;
	size_t iov_num;
	size_t chunk;
	err_t err;

	iov = thr->ipc.vec;
	iov_num = thr->ipc.iov_num;
	assert((iov_num >= 1) && (iov_num <= SYS_IOV_MAX));

	/* copy in chunks */
	while (size > 0) {
		chunk = size;
		while (iov->size == 0) {
			iov++;
			iov_num--;
			assert(iov_num > 0);
		}
		if (chunk > iov->size) {
			chunk = iov->size;
		}
		assert(chunk > 0);

		err = arch_user_memcpy(dst, iov->addr, chunk);
		if (err != EOK) {
			return err;
		}

		thr->ipc.msg.size0 += chunk;
		dst += chunk;
		size -= chunk;
	}

	return EOK;
}

err_t fd_ipc_copy_out(struct thread *thr, const void *src_kern, size_t size)
{
	const char *src = src_kern;
	struct sys_iov *iov;
	size_t iov_num;
	size_t chunk;
	err_t err;

	iov = thr->ipc.vec;
	iov_num = thr->ipc.iov_num;
	assert((iov_num >= 1) && (iov_num <= SYS_IOV_MAX));

	/* copy in chunks */
	while (size > 0) {
		chunk = size;
		while (iov->size == 0) {
			iov++;
			iov_num--;
			assert(iov_num > 0);
		}
		if (chunk > iov->size) {
			chunk = iov->size;
		}
		assert(chunk > 0);

		err = arch_user_memcpy(iov->addr, src, chunk);
		if (err != EOK) {
			return err;
		}

		thr->ipc.msg.size0 += chunk;
		src += chunk;
		size -= chunk;
	}

	return EOK;
}

static err_t (* const fd_ipc_io[])(struct fd *fd, struct thread *thr) = {
	fd_ipc_io_getoflags,
	fd_ipc_io_setoflags,
	fd_ipc_io_default_err, /* d_open, */
	fd_ipc_io_stat_dummy,
	fd_ipc_io_default_err, /* sentinel */
};

/** dispatcher for generic IPC-based I/O requests
 *
 * We use this dispatcher for all file descriptors that are not files,
 * but want to return something meaningful in fcntl() or stat() operations.
 * The callbacks do not depending on struct fd.
 */
err_t fd_generic_ipc_call(struct fd *fd)
{
	struct sys_ipc *recv_msg_user;
	struct thread *thr;
	size_t req;
	err_t err;

	assert(fd != NULL);
	assert_fd_locked(fd);

	thr = current_thread();

	req = thr->ipc.msg.req;
	if (req > countof(fd_ipc_io) - 1) {
		req = countof(fd_ipc_io) - 1;
	}

	err = fd_ipc_io[req](fd, thr);

	fd_unlock(fd);

	/* copy out message */
	if (err == EOK) {
		recv_msg_user = thr->cont_args.ipc_wait.recv_msg_user;
		assert(recv_msg_user != NULL);
		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
	}

	return err;
}
