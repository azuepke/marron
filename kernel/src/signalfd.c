/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * signalfd.c
 *
 * Signalfd implementation.
 *
 * The implementation covers the main use cases like in Linux: bind asynchronous
 * signals to a file descriptor and provide notifications via epoll.
 *
 * For notification and wakeup, we provide two functions to the outside:
 * - signalfd_notify_all(proc, sig) walks all signalfd instances
 *   of a process and checks if the readiness of a signalfd has changed.
 *   The low-level logic provides edge-triggered notifications,
 *   i.e. the epoll instance is notified each one of the signals is sent.
 * - signalfd_notify_thread(thr) checks if the target thread is currently
 *   in a blocking read operation on a signalfd, and it wakes the thread up
 *   if a signal becomes pending. The specific signal doesn't matter here.
 *
 * Note that there are corner cases with spurious wakeups: if one thread
 * is waiting for the signal in a blocking read operation, and another thread
 * is waiting on an epoll notification, both will be woken up, but only
 * one of them (the thread scheduled first) will be able to consume the signal.
 * The same is true if any other of the traditional signal handlers are used
 * in parallel to signalfd.
 *
 * azuepke, 2025-02-14: initial (from eventfd.c)
 */

#include <marron/error.h>
#include <assert.h>
#include <sys_proto.h>
#include <signalfd.h>
#include <uaccess.h>
#include <fd.h>
#include <thread.h>
#include <marron/signalfd.h>
#include <marron/fcntl.h>
#include <marron/ipc_io.h>
#include <sched.h>
#include <epoll.h>
#include <stdbool.h>
#include <stddef.h>
#include <thread.h>
#include <sig.h>
#include <process.h>


static_assert(offsetof(struct thread, cont_args.signalfd.recv_msg_user) ==
              offsetof(struct thread, cont_args.ipc_wait.recv_msg_user));

/** create a new signalfd instance */
err_t sys_signalfd_create(
	sys_sig_mask_t sig_mask,
	ulong_t flags,
	uint32_t *fd_index_user)
{
	struct process *proc;
	uint32_t fd_index;
	struct fd *fd;
	int cloexec;
	int nonblock;
	err_t err;

	if ((flags & ~(SYS_FD_CLOEXEC | SYS_FD_NONBLOCK)) != 0) {
		return EINVAL;
	}
	if (fd_index_user == NULL) {
		return EINVAL;
	}

	proc = current_process();
	fd = fd_alloc(proc);
	if (fd == NULL) {
		/* failed to allocate a new file descriptor */
		return ENFILE;
	}

	/* further initialization */
	fd->type = FD_TYPE_SIGNALFD;
	fd->cached_eventmask = 0;
	nonblock = (flags & SYS_FD_NONBLOCK) != 0;
	fd->oflags = O_RDONLY | (nonblock ? O_NONBLOCK : 0);
	fd->oflags_mask = O_NONBLOCK;
	fd->signalfd.mask = 0;
	fd->signalfd.process = proc;
	list_head_init(&fd->signalfd.signalfd_waitqh);

	process_lock(proc);

	cloexec = (flags & SYS_FD_CLOEXEC) != 0;
	err = fd_dup_locked(proc, fd, cloexec, &fd_index);
	if (err != EOK) {
		process_unlock(proc);
		fd->type = FD_TYPE_DEAD;
		fd_close(fd);
		return err;
	}

	list_node_init(&fd->signalfd.signalfdql);
	list_insert_last(&proc->sig.signalfdqh, &fd->signalfd.signalfdql);

	fd->signalfd.mask = sig_mask;
	if ((proc->sig.pending.cached_pending_mask & sig_mask) != 0) {
		fd->cached_eventmask = EPOLLIN;
	}

	process_unlock(proc);

	err = user_put_4(fd_index_user, fd_index);
	/* NOTE: any failure here is ignored and resources are not freed
	 * (the file descriptor can be accessed by user space nevertheless)
	 */

	return err;
}

static void signalfd_notify_one(struct process *proc, struct fd *fd, unsigned int sig)
{
	uint16_t eventmask;
	int level;
	int edge;

	assert(proc != NULL);
	assert_process_locked(proc);
	assert(fd != NULL);
	assert_fd_locked(fd);

	eventmask = 0;
	if ((proc->sig.pending.cached_pending_mask & fd->signalfd.mask) != 0) {
		eventmask = EPOLLIN;
	}

	level = (eventmask != fd->cached_eventmask);
	edge = ((SIG_TO_MASK(sig) & fd->signalfd.mask) != 0);
	if (level || edge) {
		epoll_notify(fd, eventmask);
	}
}

/** notify all signalfds (must be called with proc locked) */
void signalfd_notify_all(struct process *proc, unsigned int sig)
{
	struct fd *fd;
	list_t *node;

	assert(proc != NULL);
	assert_process_locked(proc);

	/* walk list of signalfds and check if new signals became pending */
	list_for_each(&proc->sig.signalfdqh, node) {
		fd = FD_FROM_SIGNALFDQ(node);
		assert(fd->type == FD_TYPE_SIGNALFD);
		fd_lock(fd);

		signalfd_notify_one(proc, fd, sig);

		fd_unlock(fd);
	}
}

err_t sys_signalfd_change(ulong_t fd_index, sys_sig_mask_t sig_mask, ulong_t flags)
{
	struct process *proc;
	struct fd *fd;
	err_t err;

	if (flags != 0) {
		return EINVAL;
	}

	proc = current_process();
	err = fd_get_type_and_lock(proc, fd_index, FD_TYPE_SIGNALFD, &fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	assert(fd->signalfd.process == proc);
	fd_relock_with_proc(proc, fd);

	fd->signalfd.mask = sig_mask;
	signalfd_notify_one(proc, fd, 0);

	fd_unlock_with_proc(proc, fd);

	return EOK;
}

/** wake a specific waiting thread */
static void signalfd_waitq_wake_one(
	struct thread *thr,
	err_t wakeup_code)
{
	assert(thr->state == THREAD_STATE_WAIT_SIGNALFD);

	sched_lock(thr->sched);
	thread_wakeup(thr);
	thr->wakeup_code = wakeup_code;
	thread_cancel_clear(thr);
	sched_unlock(thr->sched);
}

/** close signalfd file descriptor */
void signalfd_close(
	struct fd *fd)
{
	struct process *proc;
	struct thread *thr;
	list_t *node;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_SIGNALFD);
	assert_fd_locked(fd);

	list_for_each_remove_first(&fd->signalfd.signalfd_waitqh, node) {
		thr = THR_FROM_SIGNALFD_WAITQ(node);
		assert(thr->cont_args.signalfd.fd == fd);

		signalfd_waitq_wake_one(thr, ECANCEL);
	}

	proc = fd->signalfd.process;
	fd_relock_with_proc(proc, fd);

	list_remove(&fd->signalfd.signalfdql);

	/* we're the last one, and fd must remain locked on return */
	fd->usage--;
	process_unlock(proc);
}


/** convert struct sys_sig_info to struct signalfd_siginfo
 *
 * We map the kernel's compressed struct sys_sig_info format
 * to all possible combinations in struct signalfd_siginfo.
 */
static inline void signalfd_convert(
	const struct sys_sig_info *si,
	struct signalfd_siginfo *ssi)
{
	ssi->ssi_signo = si->sig;
	ssi->ssi_code = si->code;
	ssi->ssi_pid = si->status;
	ssi->ssi_uid = si->ex_type;
	ssi->ssi_fd = si->status;
	ssi->ssi_tid = si->ex_type;
	ssi->ssi_band = si->addr;
	ssi->ssi_overrun = si->status;
	ssi->ssi_trapno = si->status;
	ssi->ssi_status = si->addr;
	ssi->ssi_int = si->addr;
	ssi->ssi_ptr = si->addr;
	ssi->ssi_addr = si->addr;
}

/** get a pending signal (must be called unlocked; locks process_lock internally) */
static err_t signalfd_poll_pending(
	struct thread *thr,
	sys_sig_mask_t wait_mask)
{
	struct signalfd_siginfo ssi = { 0 };
	struct sys_sig_info info;
	err_t err;

	err = sig_poll_if_pending(thr, &info, wait_mask);
	if (err == EOK) {
		signalfd_convert(&info, &ssi);
		err = fd_ipc_copy_out(thr, &ssi, sizeof(ssi));
	}
	return err;
}

/** check if a signal from wait_mask is pending (called unlocked) */
static inline int signalfd_check_pending(
	struct thread *thr,
	sys_sig_mask_t wait_mask)
{
	sys_sig_mask_t proc_pending_mask;
	sys_sig_mask_t thr_pending_mask;
	sys_sig_mask_t sig_mask;

	/* check for pending signals */
	/* NOTE: unlocked access to the cached pending masks */
	proc_pending_mask = thr->process->sig.pending.cached_pending_mask;
	thr_pending_mask = thr->sig.pending.cached_pending_mask;

	sig_mask = (proc_pending_mask | thr_pending_mask) & wait_mask;
	return (sig_mask != 0);
}


/** notify a thread waiting on a signal fd (called with sched_lock) */
void signalfd_notify_thread(struct thread *thr)
{
	sys_sig_mask_t wait_mask;
	struct fd *fd;
	int pending;

	assert_sched_locked(thr->sched);
	assert(thr->state == THREAD_STATE_WAIT_SIGNALFD);

	fd = thr->cont_args.signalfd.fd;
	assert(fd != NULL);

	/* lock fd and relock sched */
	sched_unlock(thr->sched);
	fd_lock(fd);
	sched_lock(thr->sched);

	if (fd->type != FD_TYPE_SIGNALFD) {
		goto out_unlock_fd;
	}
	if (thr->cont_args.signalfd.fd != fd) {
		goto out_unlock_fd;
	}
	if (thr->state != THREAD_STATE_WAIT_SIGNALFD) {
		goto out_unlock_fd;
	}
	wait_mask = fd->signalfd.mask;
	pending = signalfd_check_pending(thr, wait_mask);
	if (!pending) {
		goto out_unlock_fd;
	}

	list_remove(&thr->cont_args.signalfd.signalfd_waitql);
	thread_wakeup(thr);
	thr->wakeup_code = EOK;
	thread_cancel_clear(thr);

out_unlock_fd:
	fd_unlock(fd);
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void signalfd_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	struct fd *fd;

	assert(thr != NULL);
	assert((wakeup_code == EINTR) ||
	       (wakeup_code == ECANCEL));

	fd = thr->cont_args.signalfd.fd;
	assert(fd != NULL);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	fd_lock(fd);
	list_remove(&thr->cont_args.signalfd.signalfd_waitql);
	fd_unlock(fd);
}

/** post wakeup things; mostly copying data out */
static void signalfd_wait_finish(struct thread *thr)
{
	struct sys_ipc *recv_msg_user;
	sys_sig_mask_t wait_mask;
	struct fd *fd;
	err_t err;

	assert(thr != NULL);

	/* in any case, restore original blocked mask */
	thr->sig.wait_mask = ~thr->sig.blocked_mask;

	err = thr->wakeup_code;
	if (err != EOK) {
		assert(err == EINTR);
	}

	if (err == EOK) {
		/* copy out read() data */
		fd = thr->cont_args.signalfd.fd;
		fd_lock(fd);

		wait_mask = 0;
		if (fd->type == FD_TYPE_SIGNALFD) {
			wait_mask = fd->signalfd.mask;
		}
		fd_unlock(fd);

		err = signalfd_poll_pending(thr, wait_mask);
		/* FIXME: Might return EAGAIN due to race conditions in fine grained
		 * locking. We should lock fd again and repeat.
		 */
		thr->ipc.msg.err = err;
		err = EOK;
	}

	/* copy out the IPC message in any case */
	recv_msg_user = thr->cont_args.signalfd.recv_msg_user;
	assert(recv_msg_user != NULL);
	err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));

	arch_syscall_retval(thr->regs, err);
}

static err_t signalfd_ipc_io_read(struct fd *fd, struct thread *thr)
{
	sys_sig_mask_t wait_mask;
	int pending;
	err_t err;

	err = EOK;

	if ((thr->ipc.msg.flags & SYS_IPC_BUF0W) == 0) {
		/* invalid message mode */
		thr->ipc.msg.err = EINVAL;
		goto out_unlock;
	}
	if (thr->ipc.msg.h32 != 0) {
		/* reserved flags */
		thr->ipc.msg.err = EINVAL;
		goto out_unlock;
	}

	/* seek position */
	if (thr->ipc.msg.h8 != SEEK_CUR) {
		/* readp() not read() */
		thr->ipc.msg.err = EINVAL;
		goto out_unlock;
	}

	/* size */
	if (thr->ipc.msg.size0 < sizeof(struct signalfd_siginfo)) {
		/* not read() but readp() */
		thr->ipc.msg.err = EINVAL;
		goto out_unlock;
	}
	thr->ipc.msg.size0 = 0;

	wait_mask = fd->signalfd.mask;
	pending = signalfd_check_pending(thr, wait_mask);

	if (pending) {
		/* non-blocking */
		fd_unlock(fd);

		err = signalfd_poll_pending(thr, wait_mask);
		/* FIXME: Might return EAGAIN due to race conditions in fine grained
		 * locking. We should lock fd again and repeat.
		 */
		thr->ipc.msg.err = err;
		err = EOK;
		goto out;
	} else /* value == 0 */ {
		/* blocking */
		if ((fd->oflags & O_NONBLOCK) != 0) {
			thr->ipc.msg.err = EAGAIN;
			err = EOK;
			goto out_unlock;
		}

		/* change accepted signals while waiting */
		thr->sig.wait_mask = wait_mask;

		/* wait */
		thr->cont_args.signalfd.fd = fd;
		list_node_init(&thr->cont_args.signalfd.signalfd_waitql);
		list_insert_last(&fd->signalfd.signalfd_waitqh, &thr->cont_args.signalfd.signalfd_waitql);

		sched_lock(thr->sched);
		thread_wait(thr, THREAD_STATE_WAIT_SIGNALFD, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
		thread_continue_at(thr, signalfd_wait_finish);
		thread_cancel_at(thr, signalfd_wait_cancel);
		sched_unlock(thr->sched);

		thr->ipc.msg.err = EINTR;
		err = EINTR;
		goto out_unlock;
	}

out_unlock:
	fd_unlock(fd);

out:
	return err;
}

static err_t (* const signalfd_ipc_io[])(struct fd *fd, struct thread *thr) = {
	fd_ipc_io_getoflags,
	fd_ipc_io_setoflags,
	fd_ipc_io_default_err, /* d_open, */
	fd_ipc_io_stat_dummy,
	signalfd_ipc_io_read,
	fd_ipc_io_default_err, /* sentinel */
};

/** dispatcher for IPC-based I/O requests */
err_t signalfd_ipc_call(struct fd *fd)
{
	struct sys_ipc *recv_msg_user;
	struct thread *thr;
	size_t req;
	err_t err;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_SIGNALFD);
	assert_fd_locked(fd);

	thr = current_thread();

	req = thr->ipc.msg.req;
	if (req > countof(signalfd_ipc_io) - 1) {
		req = countof(signalfd_ipc_io) - 1;
	}

	err = signalfd_ipc_io[req](fd, thr);

	/* NOTE: signalfd_ipc_io_read() already unlocked fd because of process_lock */
	if (req != IPC_IO_READ) {
		fd_unlock(fd);
	}

	/* copy out message */
	if (err == EOK) {
		recv_msg_user = thr->cont_args.signalfd.recv_msg_user;
		assert(recv_msg_user != NULL);
		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
	}

	return err;
}
