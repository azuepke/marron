/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2020, 2021 Alexander Zuepke */
/*
 * futex.c
 *
 * Futex implementation
 *
 * azuepke, 2018-03-19: initial
 * azuepke, 2018-04-19: simplified implementation
 * azuepke, 2020-02-02: lock and unlock primitives
 * azuepke, 2021-07-23: removal of flags for statistic counters
 * azuepke, 2021-07-23: lock/unlock: added acquire and release barriers
 */

#include <marron/error.h>
#include <futex.h>
#include <thread.h>
#include <current.h>
#include <uaccess.h>
#include <sched.h>
#include <process.h>
#include <assert.h>
#include <marron/atomic.h>
#include <stdbool.h>
#include <sys_proto.h>


/** Callback for cleanup during wakeup (timeout, thread deletion) */
static void futex_wait_cancel(struct thread *thr, err_t wakeup_code);

/** Block current thread on user space locking object */
err_t futex_wait(
	unsigned int *futex_user,
	unsigned int compare,
	unsigned int mask,
	unsigned int flags __unused,
	sys_timeout_t timeout,
	unsigned int clk_id_absflag)
{
	struct thread *thr;
	struct process *proc;
	unsigned int futex_value;
	err_t err;

	thr = current_thread();

	/* compare futex value */
	err = user_get_4_nocheck(futex_user, futex_value);
	if (unlikely(err != EOK)) {
		return err;
	}
	if ((futex_value & mask) != (compare & mask)) {
		return EAGAIN;
	}

	/* enqueue the current thread on the futex wait queue in priority order */
	thr->cont_args.futex.futex_user = futex_user;

	thr->prio = thread_prio_get(thr);

	proc = thr->process;
	list_node_init(&thr->cont_args.futex.futex_waitql);
	list_insert_ordered(&proc->futex_waitqh, &thr->cont_args.futex.futex_waitql,
	                    THR_FROM_FUTEX_WAITQ(__ITER__)->prio < thr->prio);

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_FUTEX, timeout, clk_id_absflag);
	thread_continue_at_wait_finish(thr);
	thread_cancel_at(thr, futex_wait_cancel);
	sched_unlock(thr->sched);

	return EOK;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void futex_wait_cancel(
	struct thread *thr,
	err_t wakeup_code)
{
	struct process *proc;

	assert(thr != NULL);

	proc = thr->process;

	/* remove thread from wait queue */
	process_lock(proc);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	/* FIXME: this might fail with a NULL pointer exception in a debug kernel
	 * in a rare race condition with timeout expiration.
	 */
	list_remove(&thr->cont_args.futex.futex_waitql);
	process_unlock(proc);
}

/** Wake threads waiting on user space locking object */
err_t futex_wake(
	unsigned int *futex_user,
	unsigned int count)
{
	struct thread *thr;
	struct process *proc;
	list_t *node, *tmp;

	proc = current_process();

	if (count > 0) {
		list_for_each_safe(&proc->futex_waitqh, node, tmp) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->cont_args.futex.futex_user == futex_user) {
				/* remove thread from wait queue */
				list_remove(&thr->cont_args.futex.futex_waitql);

				/* wake waiting thread with EOK */
				sched_lock(thr->sched);
				thread_wakeup(thr);
				/* target thread wakeup_code is already EOK */
				//thr->wakeup_code = EOK;
				thread_cancel_clear(thr);
				sched_unlock(thr->sched);

				count--;
				if (count == 0) {
					break;
				}
			}
		}
	}

	return EOK;
}

/** Wake and/or requeue threads waiting on user space locking object */
err_t futex_requeue(
	unsigned int *futex_user,
	unsigned int compare,
	unsigned int mask,
	unsigned int count,
	unsigned int *futex2_user,
	unsigned int count2)
{
	struct thread *thr;
	struct process *proc;
	unsigned int futex_value;
	unsigned int futex2_value;
	unsigned int requeued;
	list_t *node, *tmp;
	list_t tmp_head;
	err_t err;

	/* compare futex value */
	err = user_get_4_nocheck(futex_user, futex_value);
	if (unlikely(err != EOK)) {
		return err;
	}
	if ((futex_value & mask) != (compare & mask)) {
		return EAGAIN;
	}

	proc = current_process();

	/* wake up count threads */
	if (count > 0) {
		list_for_each_safe(&proc->futex_waitqh, node, tmp) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->cont_args.futex.futex_user == futex_user) {
				/* remove thread from wait queue */
				list_remove(&thr->cont_args.futex.futex_waitql);

				/* wake waiting thread with EOK */
				sched_lock(thr->sched);
				thread_wakeup(thr);
				/* target thread wakeup_code is already EOK */
				//thr->wakeup_code = EOK;
				thread_cancel_clear(thr);
				sched_unlock(thr->sched);

				count--;
				if (count == 0) {
					break;
				}
			}
		}
	}

	/* requeue count2 threads */
	if (count2 > 0) {
		/* walk wait queue again and remember threads to requeue in tmp_head */
		list_head_init(&tmp_head);
		requeued = 0;

		list_for_each_safe(&proc->futex_waitqh, node, tmp) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->cont_args.futex.futex_user == futex_user) {
				/* move thread from wait queue to tmp queue */
				list_remove(&thr->cont_args.futex.futex_waitql);
				list_insert_last(&tmp_head, &thr->cont_args.futex.futex_waitql);
				requeued++;

				count2--;
				if (count2 == 0) {
					break;
				}
			}
		}

		list_for_each_remove_first(&tmp_head, node) {
			thr = THR_FROM_FUTEX_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			/* release any timeout, but let thread continue to wait */
			thread_timeout_release(thr);

			thr->cont_args.futex.futex_user = futex2_user;

			list_node_init(&thr->cont_args.futex.futex_waitql);
			list_insert_ordered(&proc->futex_waitqh, &thr->cont_args.futex.futex_waitql,
			                    THR_FROM_FUTEX_WAITQ(__ITER__)->prio < thr->prio);
		}

		/* set waiters bit in mutex futex */
		if (requeued > 0) {
			/* FIXME: what to do on errors? */
			(void)user_get_4_nocheck(futex2_user, futex2_value);
			futex2_value |= SYS_FUTEX_WAITERS;
			(void)user_put_4_nocheck(futex2_user, futex2_value);
		}
	}

	return EOK;
}


/* Linux-like futex API -- FUTEX_LOCK_PI */
err_t futex_lock(
	unsigned int *futex_user,
	unsigned int flags __unused,
	sys_timeout_t timeout,
	unsigned int clk_id_absflag)
{
	struct thread *thr;
	struct process *proc;
	unsigned int futex_value;
	unsigned int new;
	unsigned int prev;
	err_t err;

	thr = current_thread();

	/* compare futex value */
again:
	err = user_get_4_nocheck(futex_user, futex_value);
	if (unlikely(err != EOK)) {
		return err;
	}

	/* fast path -- unlocked mutex */
	if (futex_value == 0) {
		new = thr->thr_id | SYS_FUTEX_LOCKED;
		err = user_cas_4_nocheck(futex_user, futex_value, new, prev);
		if (err == EAGAIN) {
			goto again;
		}
		if (unlikely(err != EOK)) {
			return err;
		}

		/* acquire barrier w.r.t. concurrent sys_futex_unlock() operations */
		atomic_acquire_barrier();
		return EOK;
	}

	/* short cut for non-blocking polling mode */
	if (timeout == TIMEOUT_NULL) {
		return EAGAIN;
	}

	/* set WAITERS bit */
	if ((futex_value & SYS_FUTEX_WAITERS) == 0) {
		new = futex_value | SYS_FUTEX_WAITERS;
		err = user_cas_4_nocheck(futex_user, futex_value, new, prev);
		if (err == EAGAIN) {
			goto again;
		}
		if (unlikely(err != EOK)) {
			return err;
		}
	}

	/* slow path */
	/* enqueue the current thread on the futex wait queue in priority order */
	thr->cont_args.futex.futex_user = futex_user;

	thr->prio = thread_prio_get(thr);

	proc = thr->process;
	list_node_init(&thr->cont_args.futex.futex_waitql);
	list_insert_ordered(&proc->futex_waitqh, &thr->cont_args.futex.futex_waitql,
	                    THR_FROM_FUTEX_WAITQ(__ITER__)->prio < thr->prio);

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_FUTEX, timeout, clk_id_absflag);
	thread_continue_at_wait_finish(thr);
	thread_cancel_at(thr, futex_wait_cancel);
	sched_unlock(thr->sched);

	return EOK;
}

/* Linux-like futex API -- FUTEX_UNLOCK_PI */
err_t futex_unlock(
	unsigned int *futex_user,
	unsigned int flags __unused)
{
	struct thread *thr;
	struct process *proc;
	list_t *node, *tmp;
	unsigned int futex_value;
	unsigned int tid;
	unsigned int woken;
	err_t err;

	tid = current_thread()->thr_id | SYS_FUTEX_LOCKED;

	/* compare futex value */
	err = user_get_4_nocheck(futex_user, futex_value);
	if (unlikely(err != EOK)) {
		return err;
	}
	if ((futex_value | SYS_FUTEX_WAITERS) != (tid | SYS_FUTEX_WAITERS)) {
		return ESTATE;
	}

	/* slow path */
	proc = current_process();
	futex_value = 0;
	woken = false;
	list_for_each_safe(&proc->futex_waitqh, node, tmp) {
		thr = THR_FROM_FUTEX_WAITQ(node);
		assert(thr != NULL);
		assert(thr->state == THREAD_STATE_WAIT_FUTEX);

		if (thr->cont_args.futex.futex_user == futex_user) {
			if (woken == false) {
				woken = true;

				/* remove thread from wait queue */
				list_remove(&thr->cont_args.futex.futex_waitql);

				/* wake waiting thread with EOK */
				sched_lock(thr->sched);
				thread_wakeup(thr);
				/* target thread wakeup_code is already EOK */
				//thr->wakeup_code = EOK;
				thread_cancel_clear(thr);
				sched_unlock(thr->sched);

				futex_value = thr->thr_id | SYS_FUTEX_LOCKED | SYS_FUTEX_WAITERS;
			} else {
				// more waiters
				futex_value |= SYS_FUTEX_WAITERS;
				break;
			}
		}
	}

	/* release barrier w.r.t. concurrent sys_futex_lock() operations */
	atomic_release_barrier();
	err = user_put_4_nocheck(futex_user, futex_value);

	return err;
}

////////////////////////////////////////////////////////////////////////////////

err_t sys_futex_wait(unsigned int *futex_user, ulong_t compare, ulong_t mask, ulong_t flags, sys_timeout_t timeout, ulong_t clk_id_absflag)
{
	err_t err;

	if (futex_user == NULL) {
		/* null pointer */
		return EINVAL;
	}
	if (!user_ptr_ok(futex_user)) {
		/* not accessible */
		return EINVAL;
	}
	if (flags != 0) {
		/* invalid flags -- must be zero */
		return EINVAL;
	}
	if ((clk_id_absflag & ~TIMEOUT_ABSOLUTE) >= NUM_CLK_IDS) {
		/* invalid clock ID */
		return EINVAL;
	}

	process_lock(current_process());
	/* function sets further error codes internally */
	err = futex_wait(futex_user, compare & 0xffffffff, mask & 0xffffffff, flags, timeout, clk_id_absflag);
	process_unlock(current_process());

	return err;
}

err_t sys_futex_wake(unsigned int *futex_user, ulong_t count)
{
	err_t err;

	if (futex_user == NULL) {
		/* null pointer */
		return EINVAL;
	}
	if (!user_ptr_ok(futex_user)) {
		/* not accessible */
		return EINVAL;
	}

	process_lock(current_process());
	err = futex_wake(futex_user, count & 0xffffffff);
	process_unlock(current_process());

	return err;
}

err_t sys_futex_requeue(unsigned int *futex_user, ulong_t compare, ulong_t mask, ulong_t count, unsigned int *futex2_user, ulong_t count2)
{
	err_t err;

	if (futex_user == NULL) {
		/* null pointer */
		return EINVAL;
	}
	if (!user_ptr_ok(futex_user)) {
		/* not accessible */
		return EINVAL;
	}

	if (futex2_user != NULL) {
		if (!user_ptr_ok(futex2_user)) {
			/* not accessible */
			return EINVAL;
		}
		if (futex_user == futex2_user) {
			/* same futex */
			return EINVAL;
		}
	}

	if ((count2 > 0) && (futex2_user == NULL)) {
		/* second futex is null pointer, but we have threads to wake up */
		return EINVAL;
	}

	process_lock(current_process());
	err = futex_requeue(futex_user, compare & 0xffffffff, mask & 0xffffffff, count & 0xffffffff, futex2_user, count2 & 0xffffffff);
	process_unlock(current_process());

	return err;
}

err_t sys_futex_lock(unsigned int *futex_user, ulong_t flags, sys_timeout_t timeout, ulong_t clk_id_absflag)
{
	err_t err;

	if (futex_user == NULL) {
		/* null pointer */
		return EINVAL;
	}
	if (!user_ptr_ok(futex_user)) {
		/* not accessible */
		return EINVAL;
	}
	if (flags != 0) {
		/* invalid flags */
		return EINVAL;
	}
	if ((clk_id_absflag & ~TIMEOUT_ABSOLUTE) >= NUM_CLK_IDS) {
		/* invalid clock ID */
		return EINVAL;
	}

	process_lock(current_process());
	/* function sets further error codes internally */
	err = futex_lock(futex_user, flags, timeout, clk_id_absflag);
	process_unlock(current_process());

	return err;
}

err_t sys_futex_unlock(unsigned int *futex_user, ulong_t flags)
{
	err_t err;

	if (futex_user == NULL) {
		/* null pointer */
		return EINVAL;
	}
	if (!user_ptr_ok(futex_user)) {
		/* not accessible */
		return EINVAL;
	}
	if ((flags & ~SYS_FUTEX_THREAD_EXIT) != 0) {
		/* invalid flags */
		return EINVAL;
	}

	process_lock(current_process());
	err = futex_unlock(futex_user, flags);
	process_unlock(current_process());

	if ((err == EOK) && ((flags & SYS_FUTEX_THREAD_EXIT) != 0)) {
		thread_defer_to(current_thread(), thread_exit_deferred);
	}

	return err;
}
