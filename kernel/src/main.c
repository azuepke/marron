/* SPDX-License-Identifier: MIT */
/* Copyright 2017-2025 Alexander Zuepke */
/*
 * main.c
 *
 * Marron Kernel
 *
 * azuepke, 2017-07-17: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-01-03: adding syscalls
 * azuepke, 2025-02-22: processes
 */

#include <arch.h>
#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <arch_context.h>
#include <thread.h>
#include <part.h>
#include <sched.h>
#include <sys_proto.h>
#include <marron/error.h>
#include <irq.h>
#include <current.h>
#include <sig.h>
#include <string.h>
#include <uaccess.h>
#include <adspace.h>
#include <memrq.h>
#include <kldd.h>
#include <marron/bit.h>
#include <timer.h>
#include <vma.h>
#include <ipc.h>
#include <time.h>
#include <process.h>


/** start of scheduling (for execution time statistics) */
/* NOTE: const after boot; 64-bit value, use arch_load64() */
sys_time_t kernel_start_of_scheduling;

////////////////////////////////////////////////////////////////////////////////

/** Idle loop entry point
 *
 * The kernel idle threads comprises a logical thread, but executes in kernel
 * space. The idle thread has the lowest possible priority in the system.
 */
static void kernel_idle(void)
{
	while (1) {
		bsp_idle();
	}
}

/* the kernel's main entry point */
__init void kernel_main(unsigned int cpu_id)
{
	unsigned int boot_cpu_id_cached;
	struct process *proc;
	struct thread *thr;
	sys_time_t now;

#ifdef SMP
	assert(cpu_id < cpu_num);
	boot_cpu_id_cached = bsp_boot_cpu_id;
	assert(boot_cpu_id_cached < cpu_num);
#else
	assert(cpu_id == 0);
	/* zero cpu_id on UP, this allows GCC to optimize the conditions away */
	cpu_id = 0;
	boot_cpu_id_cached = 0;
#endif

	/* at first initialize per-CPU data for all CPUs
	 * to make current_cpu_id() works as expected
	 */
	if (cpu_id == boot_cpu_id_cached) {
		console_lock_init();

		for (unsigned int i = 0; i < cpu_num; i++) {
			struct percpu *percpu = percpu_cfg[i].percpu;

			arch_percpu_init(&percpu->arch, i);
			percpu->current_thread = &thread_dyn[i];
			percpu->current_process = &process_dyn[0];
			percpu->current_part = &part_dyn[0];
			percpu->cpu_id = i;
		}
	}
	assert(current_cpu_id() == cpu_id);
#ifdef SMP
	assert(cpu_id == bsp_cpu_id());
#endif

	/* initialize BSP at first */
	/* NOTE: we do this on all cores */
	bsp_init(cpu_id);

	if (cpu_id == boot_cpu_id_cached) {
		printk("Marron kernel\n");
		printk("kernel build ID: %s %s", kernel_buildid, ARCH_BANNER_NAME);
#ifdef SMP
		printk(" SMP %u CPU(s)", bsp_cpu_num);
#else
		printk(" UP");
#endif
#ifndef NDEBUG
		printk(" DEBUG");
#else
		printk(" RELEASE");
#endif
		printk("\n");
		printk("BSP build ID: %s\n", bsp_buildid);
		printk("Timer: %llu ns\n", (unsigned long long)bsp_timer_resolution);
#ifdef SMP
		printk("booted on %u\n", boot_cpu_id_cached);
#endif

		/* initial offset between real-time clock and monotonic clock */
		clock_realtime_offset[CLK_ID_REALTIME] = 0x61000000ULL * 1000000000;
		assert(clock_realtime_offset[CLK_ID_MONOTONIC] == 0);

#ifndef NDEBUG
		/* check that exception recovery table is sorted */
		{
			const struct ex_table *start = &__ex_table_start;
			const struct ex_table *end = &__ex_table_end;

			while (start < end - 1) {
				assert(start[0].addr < start[1].addr);
				start++;
			}
		}
#endif

	}

#if 0
	console_lock();
	printk("CPU #%u: kernel stack 0x%p, percpu: 0x%p\n", cpu_id, &cpu_id, arch_percpu());
	console_unlock();
#endif
	assert(arch_percpu() == percpu_cfg[cpu_id].percpu);

	arch_init_exceptions();

	if (cpu_id == boot_cpu_id_cached) {
		memrq_init_all();
		irq_init_all();
		kldd_init_all();
		kldd_init_global();
	}
	kldd_init_per_cpu(cpu_id);
	if (cpu_id == boot_cpu_id_cached) {
		/* the initialization happens from inner to outer, and back */
		thread_init_all();
		process_init_all();
		part_init_all();
		arch_adspace_init_all();
		vma_init_all();
		ipc_init_all();

		/* bring up idle process */
		proc = process_create_idle(&part_dyn[0]);

		/* create idle threads and initialize scheduler on all CPUs */
		assert(cpu_num == sched_num);
		for (unsigned int i = 0; i < cpu_num; i++) {
			thr = thread_create_idle(proc);

			/* idle thread starts in RUNNING state */
			thr->prio = -1;
			thr->state = THREAD_STATE_CURRENT;
			thr->sched = sched_per_cpu(i);
			arch_reg_init_idle(thr->regs,
			                   (addr_t)kernel_idle,
			                   (addr_t)percpu_cfg[i].idle_stack_top);

			sched_init(sched_per_cpu(i), thr, i);
		}

#ifdef SMP
		printk("SMP: number of CPUs: %u/%u\n", bsp_cpu_num, cpu_num);
		if (bsp_cpu_num != cpu_num) {
			panic("Number of CPUs mismatch\n");
		}

		/* start all secondary CPUs */
		bsp_cpu_start_secondary();
#endif
	}

#ifdef SMP
	bsp_cpu_up(cpu_id);
#endif

	/* start partitions on the cores */
	for (unsigned int i = 1; i < part_num; i++) {
		const struct part_cfg *cfg = &part_cfg[i];
		unsigned int first_cpu = bit_flsl(cfg->cpu_mask);

		if ((cfg->initial_state != PART_STATE_IDLE) && (first_cpu == cpu_id)) {
			part_start(cfg->part);
		}
	}

	/* start time accounting */
	now = bsp_timer_get_time();
	current_thread()->sched->stat_last_switch = now;
	if (cpu_id == boot_cpu_id_cached) {
		kernel_start_of_scheduling = now;
	}

	/* start execution of first thread */
#if 0
	console_lock();
	printk("CPU #%u: go!\n", cpu_id);
	console_unlock();
#endif
}

/* Kernel IRQ callback. "irq_id" is the interrupt dispatched by the BSP.
 * The kernel should wake waiting threads, for example.
 * When kernel_irq() is called, the interrupt source is masked.
 */
void kernel_irq(unsigned int irq_id)
{
	struct irq *irq;

	assert(irq_id < irq_table_num);
	irq = irq_by_id(irq_id);
	assert(irq != NULL);

	irq_handle(irq);
}

#ifdef SMP
/* Kernel SMP rescheduling callback
 * The kernel shall handle IPIs and, possibly, reschedule.
 */
void kernel_ipi(void)
{
	/* this handler is empty, rescheduling happens on return from interrupt */
}
#endif

/** print error header */
void kernel_print_current(void)
{
	struct thread *thr;

	thr = current_thread();
	printk("### partition '%s' process %u thread %d ",
	       thr->part->part_cfg->name,
	       thr->process->proc_id,
	       thr->thr_id);
}

/* exception_type_name() */
static const char *exception_type_name(unsigned int type)
{
	switch (type) {
	case EX_TYPE_ILLEGAL:		return "illegal instruction";
	case EX_TYPE_DEBUG:			return "debug exception";
	case EX_TYPE_TRAP:			return "trap or breakpoint instruction";
	case EX_TYPE_ARITHMETIC:	return "arithmetic error / overflow";
	case EX_TYPE_PAGEFAULT:		return "pagefault";
	case EX_TYPE_ALIGN:			return "alignment error";
	case EX_TYPE_BUS:			return "bus error";
	case EX_TYPE_FPU_UNAVAIL:	return "FPU unavailable / disabled";
	case EX_TYPE_FPU_ERROR:		return "FPU error";
	case EX_TYPE_SYSCALL:		return "system call";
	default:					return "???";
	}
}

/** Kernel exception entry point */
void kernel_exception(
	struct regs *regs,
	unsigned int sig,
	unsigned int si_code,
	unsigned int ex_info,
	unsigned int status,
	unsigned long addr)
{
	struct sys_sig_info info;
	struct thread *thr;
	unsigned int ex_type;

	/* prepare signal information */
	info.sig = sig & 0xff;
	info.code = si_code;
	info.ex_type = ex_info & 0xffff;
	info.status = status;
	info.addr = addr;

	thr = current_thread();

	ex_type = EX_TYPE(ex_info);
	assert(ex_type > 0);

	if (ex_type == EX_TYPE_FPU_UNAVAIL) {
		assert(thr->fpu_state != FPU_STATE_ON);
		if (thr->fpu_state == FPU_STATE_AUTO) {
			/* lazily enable FPU */
			thr->fpu_state = FPU_STATE_ON;
			arch_fpu_enable();
			arch_reg_fpu_enable(regs);
			arch_fpu_restore(regs);
			return;
		}
	}

	// FIXME: debug output to help debugging
	console_lock();
	kernel_print_current();
	printk("%s", exception_type_name(ex_type));
	if ((ex_type == EX_TYPE_PAGEFAULT) ||
	    (ex_type == EX_TYPE_ALIGN) ||
	    (ex_type == EX_TYPE_BUS)) {
		printk(" at 0x%lx", addr);
	}
	if ((ex_type == EX_TYPE_DEBUG) ||
	    (ex_type == EX_TYPE_PAGEFAULT) ||
	    (ex_type == EX_TYPE_ALIGN) ||
	    (ex_type == EX_TYPE_BUS)) {
		printk(" %c%c%c",
		       (ex_info & EX_READ)  ? 'r' : '-',
		       (ex_info & EX_WRITE) ? 'w' : '-',
		       (ex_info & EX_EXEC)  ? 'x' : '-');
	}
	if (ex_type == EX_TYPE_PAGEFAULT) {
		if (ex_info & EX_PERM) {
			printk(" perm");
		}
	}
	printk("\n");
	arch_dump_registers(regs, status, addr);
	console_unlock();

	sig_exception(thr, sig, &info);
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Handling of deferred system calls

void kernel_deferred_syscall(void)
{
	void (*syscalldefer)(struct thread *);
	struct thread *thr;

	thr = current_thread();
	syscalldefer = thr->syscalldefer;
	if (unlikely(syscalldefer != NULL)) {
		thr->syscalldefer = NULL;
		syscalldefer(thr);
	}
}

// System calls

err_t sys_bsp_shutdown(ulong_t mode)
{
	if ((current_part()->part_cfg->perm & PART_PERM_SHUTDOWN) == 0) {
		return EPERM;
	}

	switch (mode) {
	case 0:
		bsp_halt(BOARD_RESET);
		unreachable();

	case 1:
		bsp_halt(BOARD_HALT);
		unreachable();

	case 2:
		bsp_halt(BOARD_POWEROFF);
		unreachable();

	default:
		return EINVAL;
	}
}

err_t sys_bsp_name(char *name_user, size_t size_user)
{
	size_t size;

	size = strlen(bsp_name) + 1;
	if (size > size_user) {
		return ELIMIT;
	}

	return user_copy_out(name_user, bsp_name, size);
}

err_t sys_cache(ulong_t op, addr_t start, size_t size, addr_t alias)
{
	err_t err;

	if (!USER_CHECK_RANGE(start, size)) {
		return EINVAL;
	}
	/* "alias" is not further checked */

	switch (op) {
	case CACHE_OP_INVAL_ICACHE_RANGE:
	case CACHE_OP_FLUSH_DCACHE_RANGE:
	case CACHE_OP_CLEAN_DCACHE_RANGE:
	case CACHE_OP_INVAL_DCACHE_RANGE:
		err = bsp_cache(op, start, size, alias);
		break;

	default:
		err = EINVAL;
		break;
	}

	return err;
}

err_t sys_ni_syscall(void)
{
	return ENOSYS;
}
