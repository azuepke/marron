/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * kldd.c
 *
 * Kernel-level device drivers (KLDD) implementation
 *
 * azuepke, 2021-05-07: initial
 * azuepke, 2023-08-07: driver init calls
 */

#include <marron/error.h>
#include <kldd.h>
#include <kernel.h>
#include <bsp.h>
#include <assert.h>
#include <sys_proto.h>
#include <part.h>
#include <string.h>
#include <uaccess.h>


/** initialize the private data of all KLDD drivers at boot time */
__init void kldd_init_all(void)
{
	for (unsigned int i = 0; i < kldd_num; i++) {
		struct kldd_priv *priv = &kldd_priv[i];

		priv->data = 0;
	}
}

/** call registered KLDD drivers' global initializers at boot time */
__init void kldd_init_global(void)
{
	for (unsigned int i = 0; i < kldd_init_global_num; i++) {
		kldd_init_global_cfg[i]();
	}
}

/** call registered KLDD drivers' per-CPU initializers at boot time */
__init void kldd_init_per_cpu(unsigned int cpu_id)
{
	for (unsigned int i = 0; i < kldd_init_per_cpu_num; i++) {
		kldd_init_per_cpu_cfg[i](cpu_id);
	}
}

err_t sys_kldd_name(ulong_t id, char *name_user, size_t size_user)
{
	const struct part_cfg *cfg;
	const struct kldd_cfg *kldd;
	size_t size;

	cfg = current_part()->part_cfg;
	if (id >= cfg->kldd_num) {
		/* out of bounds */
		return ELIMIT;
	}
	kldd = &cfg->kldd_cfg[id];

	size = strlen(kldd->name) + 1;
	if (size > size_user) {
		return ELIMIT;
	}

	return user_copy_out(name_user, kldd->name, size);
}

err_t sys_kldd_call(ulong_t id, ulong_t arg1, ulong_t arg2, ulong_t arg3, ulong_t arg4)
{
	const struct part_cfg *cfg;
	const struct kldd_cfg *kldd;
	err_t err;

	cfg = current_part()->part_cfg;
	if (id >= cfg->kldd_num) {
		/* out of bounds */
		return ELIMIT;
	}
	kldd = &cfg->kldd_cfg[id];

	assert(kldd->handler != NULL);
	assert(kldd->priv != NULL);
	err = kldd->handler(kldd->priv, arg1, arg2, arg3, arg4);
	return err;
}
