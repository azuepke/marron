/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * eventfd.c
 *
 * Eventfd implementation.
 *
 * For an eventfd, we store all information in struct fd and use the fd_lock.
 * An eventfd comprises an internal 64-bit counter and operates either
 * in counter or in semaphore mode. The difference is on the reader side,
 * when consuming events: Semaphores reduce the event counter by one,
 * while in event mode the counter value is set to zero. In both cases,
 * we return the consumed value. The private information in struct fd
 * comprises the current event/semaphore counter value,
 * and two wait queues for pending readers and writers.
 * One wait queue would be sufficient, but using two speeds up processing.
 *
 * In the read and write IPC calls, we handle the non-blocking cases immediately
 * and defer the blocking cases to the "kick" function:
 * eventfd_ipc_io_kick() is called after any reads or writes and checks
 * if any of the blocked threads can be served, and it does this recursively,
 * as readers can wake up writers and vice versa, but this quickly converges.
 * The kick function consumes the events and records the consumed event count
 * in the thread's continuation data for later copy-out during kernel exit.
 *
 * azuepke, 2024-12-29: initial
 */

#include <marron/error.h>
#include <assert.h>
#include <sys_proto.h>
#include <eventfd.h>
#include <uaccess.h>
#include <fd.h>
#include <thread.h>
#include <marron/fcntl.h>
#include <sched.h>
#include <epoll.h>
#include <stdbool.h>
#include <stddef.h>
#include <part_types.h>


static_assert(offsetof(struct thread, cont_args.eventfd.recv_msg_user) ==
              offsetof(struct thread, cont_args.ipc_wait.recv_msg_user));

/** retrieve epoll events for eventfd */
static inline uint32_t eventfd_epoll_events(
	uint64_t value)
{
	uint32_t eventmask;

	eventmask = 0;
	if (value > 0) {
		eventmask |= EPOLLIN;
	}
	if (value < 0xfffffffffffffffeull) {
		eventmask |= EPOLLOUT;
	}
	if (value == 0xffffffffffffffffull) {
		eventmask |= EPOLLERR;
	}

	return eventmask;
}

/** create a new eventfd instance */
err_t sys_eventfd_create(
	uint64_t initval,
	ulong_t flags,
	uint32_t *fd_index_user)
{
	uint32_t fd_index;
	struct fd *fd;
	int cloexec;
	int nonblock;
	int semaphore;
	err_t err;

	if (initval == 0xffffffffffffffffull) {
		return EINVAL;
	}
	if ((flags & ~(SYS_FD_CLOEXEC | SYS_FD_NONBLOCK | SYS_FD_SPECIAL)) != 0) {
		return EINVAL;
	}
	if (fd_index_user == NULL) {
		return EINVAL;
	}

	fd = fd_alloc(current_process());
	if (fd == NULL) {
		/* failed to allocate a new file descriptor */
		return ENFILE;
	}

	/* further initialization */
	fd->type = FD_TYPE_EVENTFD;
	fd->cached_eventmask = eventfd_epoll_events(initval);
	nonblock = (flags & SYS_FD_NONBLOCK) != 0;
	fd->flags = FD_FLAG_SHAREABLE;
	fd->oflags = O_RDWR | (nonblock ? O_NONBLOCK : 0);
	fd->oflags_mask = O_NONBLOCK;
	semaphore = (flags & SYS_FD_SPECIAL) != 0;
	fd->priv = semaphore;
	fd->eventfd.value = initval;
	list_head_init(&fd->eventfd.eventfd_reader_waitqh);
	list_head_init(&fd->eventfd.eventfd_writer_waitqh);

	cloexec = (flags & SYS_FD_CLOEXEC) != 0;
	err = fd_dup(current_process(), fd, cloexec, &fd_index);
	if (err != EOK) {
		fd->type = FD_TYPE_DEAD;
		fd_close(fd);
		return err;
	}

	err = user_put_4(fd_index_user, fd_index);
	/* NOTE: any failure here is ignored and resources are not freed
	 * (the file descriptor can be accessed by user space nevertheless)
	 */

	return err;
}

/** wake a specific waiting thread */
static void eventfd_waitq_wake_one(
	struct thread *thr,
	err_t wakeup_code)
{
	assert(thr->state == THREAD_STATE_WAIT_EVENTFD);

	sched_lock(thr->sched);
	thread_wakeup(thr);
	thr->wakeup_code = wakeup_code;
	thread_cancel_clear(thr);
	sched_unlock(thr->sched);
}

/** close eventfd file descriptor */
void eventfd_close(
	struct fd *fd)
{
	struct thread *thr;
	list_t *node;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_EVENTFD);
	assert_fd_locked(fd);

	list_for_each_remove_first(&fd->eventfd.eventfd_reader_waitqh, node) {
		thr = THR_FROM_EVENTFD_WAITQ(node);
		assert(thr->cont_args.eventfd.fd == fd);

		eventfd_waitq_wake_one(thr, ECANCEL);
	}

	list_for_each_remove_first(&fd->eventfd.eventfd_writer_waitqh, node) {
		thr = THR_FROM_EVENTFD_WAITQ(node);
		assert(thr->cont_args.eventfd.fd == fd);

		eventfd_waitq_wake_one(thr, ECANCEL);
	}
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void eventfd_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	struct fd *fd;

	assert(thr != NULL);
	assert((wakeup_code == EINTR) ||
	       (wakeup_code == ECANCEL));

	fd = thr->cont_args.eventfd.fd;
	assert(fd != NULL);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	fd_lock(fd);
	list_remove(&thr->cont_args.eventfd.eventfd_waitql);
	fd_unlock(fd);
}

/** post wakeup things; mostly copying data out */
static void eventfd_wait_finish(struct thread *thr)
{
	struct sys_ipc *recv_msg_user;
	err_t err;

	assert(thr != NULL);

	err = thr->wakeup_code;
	if (err != EOK) {
		assert(err == EINTR);
	}

	if (err == EOK) {
		/* copy out read() data */
		if (thr->cont_args.eventfd.is_read) {
			uint64_t *value_p = &thr->cont_args.eventfd.value;
			err = fd_ipc_copy_out(thr, value_p, sizeof(*value_p));
		}
		/* fixup IPC error code for both reads and writes */
		thr->ipc.msg.err = err;
		err = EOK;
	}

	/* copy out the IPC message in any case */
	recv_msg_user = thr->cont_args.eventfd.recv_msg_user;
	assert(recv_msg_user != NULL);
	err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));

	arch_syscall_retval(thr->regs, err);
}

static err_t eventfd_ipc_io_read(struct fd *fd, struct thread *thr)
{
	uint64_t value;
	err_t err;

	if ((thr->ipc.msg.flags & SYS_IPC_BUF0W) == 0) {
		/* invalid message mode */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	if (thr->ipc.msg.h32 != 0) {
		/* reserved flags */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	/* seek position */
	if (thr->ipc.msg.h8 != SEEK_CUR) {
		/* readp() not read() */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	/* size */
	if (thr->ipc.msg.size0 < sizeof(uint64_t)) {
		/* not read() but readp() */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	thr->ipc.msg.size0 = 0;

	value = fd->eventfd.value;
	if (value != 0) {
		/* non-blocking */
		if (fd->priv) {
			/* semaphore mode */
			value = 1;
		}
		fd->eventfd.value -= value;
		err = fd_ipc_copy_out(thr, &value, sizeof(value));

		thr->ipc.msg.err = err;
		return EOK;
	} else /* value == 0 */ {
		/* blocking */
		if ((fd->oflags & O_NONBLOCK) != 0) {
			thr->ipc.msg.err = EAGAIN;
			return EOK;
		}

		/* wait */
		thr->cont_args.eventfd.fd = fd;
		thr->cont_args.eventfd.is_read = 1;
		thr->cont_args.eventfd.value = 0;
		list_node_init(&thr->cont_args.eventfd.eventfd_waitql);
		list_insert_last(&fd->eventfd.eventfd_reader_waitqh, &thr->cont_args.eventfd.eventfd_waitql);

		sched_lock(thr->sched);
		thread_wait(thr, THREAD_STATE_WAIT_EVENTFD, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
		thread_continue_at(thr, eventfd_wait_finish);
		thread_cancel_at(thr, eventfd_wait_cancel);
		sched_unlock(thr->sched);

		thr->ipc.msg.err = EINTR;
		return EINTR;
	}
}

static err_t eventfd_ipc_io_write(struct fd *fd, struct thread *thr)
{
	uint64_t value;
	err_t err;

	if ((thr->ipc.msg.flags & SYS_IPC_BUF0R) == 0) {
		/* invalid message mode */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	if (thr->ipc.msg.h32 != 0) {
		/* reserved flags */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	/* seek position */
	if (thr->ipc.msg.h8 != SEEK_CUR) {
		/* writep() not write() */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	/* size */
	if (thr->ipc.msg.size0 < sizeof(value)) {
		/* not read() but readp() */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	thr->ipc.msg.size0 = 0;

	err = fd_ipc_copy_in(thr, &value, sizeof(value));
	if (err != EOK) {
		thr->ipc.msg.err = err;
		return EOK;
	}

	if (value == 0xffffffffffffffffull) {
		/* invalid value */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	if (value == 0) {
		/* writing a zero value is a NOP */
		thr->ipc.msg.err = EOK;
		return EOK;
	}

	if ((value + fd->eventfd.value >= fd->eventfd.value) &&
	    (value + fd->eventfd.value < 0xffffffffffffffffull)) {
		/* non-blocking */
		fd->eventfd.value += value;

		thr->ipc.msg.err = EOK;
		return EOK;
	} else {
		/* blocking */
		if ((fd->oflags & O_NONBLOCK) != 0) {
			thr->ipc.msg.err = EAGAIN;
			return EOK;
		}

		/* wait */
		thr->cont_args.eventfd.fd = fd;
		thr->cont_args.eventfd.is_read = 0;
		thr->cont_args.eventfd.value = value;
		list_node_init(&thr->cont_args.eventfd.eventfd_waitql);
		list_insert_last(&fd->eventfd.eventfd_writer_waitqh, &thr->cont_args.eventfd.eventfd_waitql);

		sched_lock(thr->sched);
		thread_wait(thr, THREAD_STATE_WAIT_EVENTFD, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
		thread_continue_at(thr, eventfd_wait_finish);
		thread_cancel_at(thr, eventfd_wait_cancel);
		sched_unlock(thr->sched);

		thr->ipc.msg.err = EINTR;
		return EINTR;
	}
}

static void eventfd_ipc_io_kick(struct fd *fd)
{
	list_t *node, *tmp;
	struct thread *thr;
	uint64_t value;
	int woken;

again:
	woken = false;

	/* wake readers in FIFO order as long as there is something to consume */
	list_for_each_safe(&fd->eventfd.eventfd_reader_waitqh, node, tmp) {
		thr = THR_FROM_EVENTFD_WAITQ(node);
		assert(thr->cont_args.eventfd.fd == fd);
		assert(thr->cont_args.eventfd.is_read == 1);
		assert(thr->cont_args.eventfd.value == 0);

		value = fd->eventfd.value;
		if (value == 0) {
			break;
		}

		if (fd->priv) {
			/* semaphore mode */
			value = 1;
		}
		fd->eventfd.value -= value;

		thr->cont_args.eventfd.value = value;
		list_remove(&thr->cont_args.eventfd.eventfd_waitql);
		eventfd_waitq_wake_one(thr, EOK);

		woken = true;
	}

	/* wake writers that can write (value fits) */
	list_for_each_safe(&fd->eventfd.eventfd_writer_waitqh, node, tmp) {
		thr = THR_FROM_EVENTFD_WAITQ(node);
		assert(thr->cont_args.eventfd.fd == fd);
		assert(thr->cont_args.eventfd.is_read == 0);
		assert(thr->cont_args.eventfd.value != 0);

		value = thr->cont_args.eventfd.value;
		if ((value + fd->eventfd.value < fd->eventfd.value) ||
		    (value + fd->eventfd.value >= 0xffffffffffffffffull)) {
			continue;
		}

		fd->eventfd.value += value;

		list_remove(&thr->cont_args.eventfd.eventfd_waitql);
		eventfd_waitq_wake_one(thr, EOK);

		woken = true;
	}

	if (woken) {
		goto again;
	}

	epoll_notify(fd, eventfd_epoll_events(fd->eventfd.value));
}

static err_t (* const eventfd_ipc_io[])(struct fd *fd, struct thread *thr) = {
	fd_ipc_io_getoflags,
	fd_ipc_io_setoflags,
	fd_ipc_io_default_err, /* d_open, */
	fd_ipc_io_stat_dummy,
	eventfd_ipc_io_read,
	eventfd_ipc_io_write,
	fd_ipc_io_default_err, /* sentinel */
};

/** dispatcher for IPC-based I/O requests */
err_t eventfd_ipc_call(struct fd *fd)
{
	struct sys_ipc *recv_msg_user;
	struct thread *thr;
	size_t req;
	err_t err;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_EVENTFD);
	assert_fd_locked(fd);

	thr = current_thread();

	req = thr->ipc.msg.req;
	if (req > countof(eventfd_ipc_io) - 1) {
		req = countof(eventfd_ipc_io) - 1;
	}

	err = eventfd_ipc_io[req](fd, thr);

	eventfd_ipc_io_kick(fd);

	fd_unlock(fd);

	/* copy out message */
	if (err == EOK) {
		recv_msg_user = thr->cont_args.eventfd.recv_msg_user;
		assert(recv_msg_user != NULL);
		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
	}

	return err;
}
