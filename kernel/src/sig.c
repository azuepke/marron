/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2021, 2025 Alexander Zuepke */
/*
 * sig.c
 *
 * Signal and exception handling.
 *
 * azuepke, 2018-03-16: initial
 * azuepke, 2021-08-04: queued signals
 * azuepke, 2021-08-06: synchronous waiting for signals
 * azuepke, 2025-02-14: signalfd
 * azuepke, 2025-02-22: processes
 */

#include <marron/regs.h>
#include <marron/error.h>
#include <marron/signal.h>
#include <sig.h>
#include <kernel.h>
#include <assert.h>
#include <stddef.h>
#include <current.h>
#include <thread.h>
#include <sched.h>
#include <arch_context.h>
#include <marron/bit.h>
#include <uaccess.h>
#include <part.h>
#include <timer.h>
#include <sys_proto.h>
#include <fd.h>
#include <signalfd.h>
#include <process.h>


/* forward */
static err_t sig_notify(struct process *proc, struct thread *thr, unsigned int sig);
static void sig_wait_cancel(struct thread *thr, err_t wakeup_code);
static void sig_wait_finish(struct thread *thr);
static void sig_suspend_cancel(struct thread *thr, err_t wakeup_code);
static void sys_abort_deferred(struct thread *thr);
static void sys_sig_return_deferred(struct thread *thr);


/** allocate a sig queue node */
/* NOTE: proc_lock must not be taken; locks part_lock internally */
static inline struct sig_queue_node *sig_alloc_sig_queue_node(struct part *part)
{
	struct sig_queue_node *sigqn;
	list_t *node;

	assert(part != NULL);

	part_lock(part);

	node = list_remove_first(&part->free_sigqh);
	if (node != NULL) {
		sigqn = SIG_FROM_SIGQ(node);
		assert(sigqn->timer == NULL);
	} else {
		sigqn = NULL;
	}

	part_unlock(part);

	return sigqn;
}

/** free a sig queue node */
/* NOTE: proc_lock must not be taken; locks part_lock internally */
static inline void sig_free_sig_queue_node(struct part *part, struct sig_queue_node *sigqn)
{
	assert(part != NULL);
	assert(sigqn != NULL);

	/* we can only free sig queue nodes that are not bound to a timer */
	if (sigqn->timer != NULL) {
		sigqn->timer->sig_enqueued = 0;
	} else {
		part_lock(part);
		list_insert_last(&part->free_sigqh, &sigqn->sigql);
		part_unlock(part);
	}
}

/** register a signal handler */
static inline err_t sig_register(struct process *proc, unsigned int sig, addr_t handler, sys_sig_mask_t blocked_mask, unsigned int flags)
{
	assert(proc != NULL);
	assert(sig < NUM_SIGS);

	process_lock(proc);

	proc->sig.handler[sig] = handler;
	proc->sig.blocked_mask[sig] = blocked_mask;
	proc->sig.flags[sig] = flags;

	process_unlock(proc);

	return EOK;
}

/** send a signal or exception to the current thread */
static void sig_send_now(
	struct thread *thr,
	unsigned int sig,
	struct sys_sig_info *info)
{
	struct sys_sig_info *info_user;
	struct process *proc;
	struct regs *regs;
	addr_t user_frame;
	unsigned int flags;
	sys_sig_mask_t blocked_mask;
	sys_sig_mask_t wait_mask;
	addr_t handler;
	err_t err;

	assert(thr != NULL);
	assert(thr == current_thread());
	assert((sig != 0) && (sig < NUM_SIGS));
	assert(info != NULL);

	proc = thr->process;
	handler = proc->sig.handler[sig];

	if (handler == 0) {
		/* no registered handler */
		goto escalate;
	}

	blocked_mask = current_thread()->sig.blocked_mask;
	wait_mask = current_thread()->sig.wait_mask;

	if ((wait_mask & SIG_TO_MASK(sig)) == 0) {
		/* exception is currently blocked */
		goto escalate;
	}

	/* update exception info and save FPU registers */
	regs = thr->regs;
	if (thr->fpu_state == FPU_STATE_ON) {
		info->ex_type |= EX_FPU_ENABLED;
		arch_fpu_save(regs);
	}

	/* setup register frame */
	flags = proc->sig.flags[sig];
	if ((flags & SYS_SIG_STACK) != 0) {
		user_frame = arch_signal_alloc_frame(regs, thr->sig.stack_base, thr->sig.stack_size);
	} else {
		user_frame = arch_signal_alloc_frame(regs, 0, 0);
	}

	arch_signal_prepare(regs);

	err = user_copy_out_aligned((void*)user_frame, regs, sizeof(*regs));
	if (err != EOK) {
		/* setting up the register context failed */
		goto escalate;
	}


	info_user = (struct sys_sig_info *)(user_frame - sizeof(*info));
	err = user_copy_out_aligned(info_user, info, sizeof(*info));
	if (err != EOK) {
		/* setting up the signal information failed */
		goto escalate;
	}

	/* sig_suspend(): let the handler restore the originally blocked signals */
	arch_signal_init(regs, handler, user_frame, blocked_mask, info_user);

	if (thr->fpu_state == FPU_STATE_ON) {
		/* disable FPU for signal handler */
		thr->fpu_state = FPU_STATE_AUTO;
		arch_reg_fpu_init_disabled(regs);
		arch_fpu_disable();
	}

	/* block current signal while executing in handler */
	blocked_mask |= SIG_TO_MASK(sig);
	/* additionally block signals registered next to the handler */
	blocked_mask |= proc->sig.blocked_mask[sig];
	thr->sig.blocked_mask = blocked_mask;
	thr->sig.wait_mask = ~blocked_mask;
	return;

escalate:
	/* escalate error to process error */
	process_error(proc, sig);
}


/** immediately raise an exception to the current thread
 * NOTE: the architecture layer and sys_abort() calls this,
 * and exceptions are never set to "pending"
 */
void sig_exception(
	struct thread *thr,
	unsigned int sig,
	struct sys_sig_info *info)
{
	assert(thr == current_thread());
	assert((sig != 0) && (sig < NUM_SIGS));

	sig_send_now(thr, sig, info);
}

/** send an asynchronous signal to a thread (thr_id!=THR_ID_ALL) or to a process (thr_id==THR_ID_ALL) */
static err_t sig_send_info(struct process *proc, unsigned int thr_id, unsigned int sig, struct sys_sig_info *info)
{
	struct sig_queue_root *pending;
	struct sig_queue_node *sigqn;
	struct thread *thr;
	err_t err;

	assert(proc != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));

	sigqn = NULL;
	if (info != NULL) {
		/* NOTE: proc_lock must not be taken; locks part_lock internally */
		sigqn = sig_alloc_sig_queue_node(proc->part);
		if (unlikely(sigqn == NULL)) {
			return EAGAIN;
		}

		/* copy signal info over */
		sigqn->info = *info;
	}

	process_lock(proc);

	if (thr_id == THR_ID_ALL) {
		/* send signal to partition */
		pending = &proc->sig.pending;
		thr = NULL;
	} else {
		err = thread_lookup_locked(proc, thr_id, &thr);
		if (err != EOK) {
			process_unlock(proc);
			goto out_free_sigqn;
		}
		pending = &thr->sig.pending;
	}

	if (info != NULL) {
		list_insert_last(&pending->sigqh, &sigqn->sigql);
	} else {
		pending->unqueued_pending_mask |= SIG_TO_MASK(sig);
	}
	pending->cached_pending_mask |= SIG_TO_MASK(sig);
	signalfd_notify_all(proc, sig);

	process_unlock(proc);

	return sig_notify(proc, thr, sig);

out_free_sigqn:
	if (sigqn != NULL) {
		/* NOTE: proc_lock must not be taken; locks part_lock internally */
		sig_free_sig_queue_node(proc->part, sigqn);
	}

	return err;
}

/** send an asynchronous signal to a thread (thr_id!=THR_ID_ALL) or to a process (thr_id==THR_ID_ALL) */
err_t sig_send_sigqn(struct process *proc, unsigned int thr_id, unsigned int sig, struct sig_queue_node *sigqn)
{
	struct sig_queue_root *pending;
	struct thread *thr;

	assert(proc != NULL);
	assert_process_locked(proc);
	assert((sig != 0) && (sig < NUM_SIGS));
	assert(sigqn != NULL);

	thr = NULL;
	if (thr_id != THR_ID_ALL) {
		(void)thread_lookup_locked(proc, thr_id, &thr);
		/* the signal will be sent to the process if the thread no longer exists */
	}
	if (thr != NULL) {
		pending = &thr->sig.pending;
	} else {
		pending = &proc->sig.pending;
	}

	list_insert_last(&pending->sigqh, &sigqn->sigql);
	pending->cached_pending_mask |= SIG_TO_MASK(sig);
	signalfd_notify_all(proc, sig);

	return sig_notify(proc, thr, sig);
}

static err_t sig_notify(struct process *proc, struct thread *thr, unsigned int sig)
{
	list_t *node;

	assert(proc != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));

	/* 1. find thread waiting for the signal, if any */
	if (thr == NULL) {
		process_lock(proc);
		list_for_each(&proc->thread_activeqh, node) {
			thr = THR_FROM_ACTIVEQ(node);
			if ((thr->sig.wait_mask & SIG_TO_MASK(sig)) != 0) {
				break;
			}
			thr = NULL;
		}
		process_unlock(proc);
	}

	/* 2. find thread having not blocked the signal, if any */
	if (thr == NULL) {
		process_lock(proc);
		list_for_each(&proc->thread_activeqh, node) {
			thr = THR_FROM_ACTIVEQ(node);
			if ((thr->sig.blocked_mask & SIG_TO_MASK(sig)) == 0) {
				break;
			}
			thr = NULL;
		}
		process_unlock(proc);
	}

	if (thr != NULL) {
		/* unblock target thread */
		if ((thr->sig.wait_mask & SIG_TO_MASK(sig)) != 0) {
			sched_lock(thr->sched);
			if (thr->state == THREAD_STATE_WAIT_SIG) {
				thread_wakeup(thr);
				thr->wakeup_code = EINTR;
				thread_cancel_clear(thr);
			}
			if (thr->state == THREAD_STATE_WAIT_SIGNALFD) {
				signalfd_notify_thread(thr);
			}
			sched_unlock(thr->sched);
		}

#ifdef SMP
		if ((thr != current_thread()) && (thr->state == THREAD_STATE_CURRENT)) {
			/* target thread is current on other CPU, send IPI */
			bsp_cpu_reschedule(thr->sched->cpu);
		}
#endif
	}

	return EOK;
}

/** get the next queued signal
 * NOTE: This operates either on process or thread-specific signal data.
 * We now perform all checks under the process lock.
 */
static inline int sig_dequeue(
	struct process *proc,
	struct sig_queue_root *pending,
	unsigned int sig,
	struct sys_sig_info *info)
{
	struct sig_queue_node *sigqn_first;
	struct sig_queue_node *sigqn;
	list_t *node;
	int found;

	assert(proc != NULL);
	assert(pending != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));
	assert(info != NULL);

	process_lock(proc);

	found = 0;
	sigqn_first = NULL;
	if ((SIG_TO_MASK(sig) & pending->unqueued_pending_mask) != 0) {
		/* Pending unqueued signal.
		 *
		 * POSIX does not specify precedence rules when mixing
		 * unqueued (i.e. kill()) and queued (i.e. sigqueue()) signal types.
		 * But POSIX describes that traditional signals are served
		 * before real-time signals, so we serve unqueued signals first.
		 */
		pending->unqueued_pending_mask &= ~SIG_TO_MASK(sig);

		/* prepare signal information */
		info->sig = sig & 0xff;
		info->code = 0;
		info->ex_type = 0;
		info->status = 0;
		info->addr = 0;

		found = 1;

		/* Scan signal queue if more signals are pending */
		list_for_each(&pending->sigqh, node) {
			sigqn = SIG_FROM_SIGQ(node);

			if (sigqn->info.sig == sig) {
				found = 2;
				break;
			}
		}
	} else {
		/* Pending queued signal.
		 *
		 * We scan the signal queue for a matching signal,
		 * and then keep scanning if there is another matching signal.
		 */
		list_for_each(&pending->sigqh, node) {
			sigqn = SIG_FROM_SIGQ(node);

			if (sigqn->info.sig == sig) {
				found++;
				if (found == 1) {
					sigqn_first = sigqn;
				}
				if (found > 1) {
					break;
				}
			}
		}

		if (sigqn_first != NULL) {
			*info = sigqn_first->info;

			list_remove(&sigqn_first->sigql);
		}
	}

	if (found == 1) {
		pending->cached_pending_mask &= ~SIG_TO_MASK(sig);

		signalfd_notify_all(proc, 0);
	}

	process_unlock(proc);

	if (sigqn_first != NULL) {
		/* NOTE: proc_lock must not be taken; locks part_lock internally */
		sig_free_sig_queue_node(proc->part, sigqn_first);
	}

	return found > 0;
}

/** if the thread has pending signals, send them now
 * NOTE: the scheduler calls this function at kernel exit
 */
struct regs *sig_send_if_pending(
	struct thread *thr)
{
	struct sig_queue_root *pending;
	sys_sig_mask_t proc_pending_mask;
	sys_sig_mask_t thr_pending_mask;
	sys_sig_mask_t blocked_mask;
	struct sys_sig_info info;
	sys_sig_mask_t sig_mask;
	unsigned int sig;
	int found;

	assert(thr == current_thread());

again:
	/* check for pending signals */
	/* NOTE: unlocked access to the cached pending masks */
	proc_pending_mask = thr->process->sig.pending.cached_pending_mask;
	thr_pending_mask = thr->sig.pending.cached_pending_mask;
	blocked_mask = thr->sig.blocked_mask;

	sig_mask = (proc_pending_mask | thr_pending_mask) & ~blocked_mask;
	if (likely(sig_mask == 0)) {
		return thr->regs;
	}

	/*
	 * We send all signals which are pending, but not masked (yet).
	 * The loop terminates when all these signals have been delivered
	 * to user space, which means at most NUM_SIGS + num_queued iterations.
	 * Note that the user can specify additional signals which become masked
	 * while a signal handler is active.
	 */

	/* pending signal, the ones with lowest bits have higher priority */
	sig = bit_fls64(sig_mask);
	if ((SIG_TO_MASK(sig) & proc_pending_mask) != 0) {
		pending = &thr->process->sig.pending;
	} else {
		pending = &thr->sig.pending;
	}

	/* NOTE: sig_dequeue() checks again with the process lock */
	found = sig_dequeue(thr->process, pending, sig, &info);
	if (found) {
		/* send this signal */
		sig_send_now(thr, sig, &info);
	}

	goto again;
}

/** if the thread has pending signals, retrieve them synchronously now */
err_t sig_poll_if_pending(
	struct thread *thr,
	struct sys_sig_info *info,
	sys_sig_mask_t wait_mask)
{
	struct sig_queue_root *pending;
	sys_sig_mask_t proc_pending_mask;
	sys_sig_mask_t thr_pending_mask;
	sys_sig_mask_t sig_mask;
	unsigned int sig;
	int found;
#ifndef NDEBUG
	unsigned int loops = 0;
#endif

	assert(thr == current_thread());
	assert(info != NULL);

again:
	/* check for pending signals */
	/* NOTE: unlocked access to the cached pending masks */
	proc_pending_mask = thr->process->sig.pending.cached_pending_mask;
	thr_pending_mask = thr->sig.pending.cached_pending_mask;

	sig_mask = (proc_pending_mask | thr_pending_mask) & wait_mask;
	if (likely(sig_mask == 0)) {
		return EAGAIN;
	}

	/* pending signal, the ones with lowest bits have higher priority */
	sig = bit_fls64(sig_mask);
	if ((SIG_TO_MASK(sig) & proc_pending_mask) != 0) {
		pending = &thr->process->sig.pending;
	} else {
		pending = &thr->sig.pending;
	}

	/* NOTE: sig_dequeue() checks again with the process lock */
	found = sig_dequeue(thr->process, pending, sig, info);
	if (found) {
		return EOK;
	}

#ifndef NDEBUG
	loops++;
	assert(loops < 10);
#endif
	goto again;
}

/** return from a signal handler */
void sig_return(
	struct thread *thr,
	const struct regs *regs_user,
	sys_sig_mask_t blocked_mask)
{
	struct regs *regs;
	err_t err;

	assert(thr == current_thread());

	regs = thr->regs;
	err = user_copy_in_aligned(regs, regs_user, sizeof(*regs));

	/* fixup registers in any case */
	arch_signal_fixup(regs);

	/* restore FPU registers */
	if (thr->fpu_state != FPU_STATE_OFF) {
		/* FPU must be ON or AUTO */
		if (arch_reg_fpu_enabled(regs)) {
			thr->fpu_state = FPU_STATE_ON;
			arch_fpu_enable();
			arch_fpu_restore(regs);
		} else {
			thr->fpu_state = FPU_STATE_AUTO;
			arch_fpu_disable();
		}
	}

	if (err != EOK) {
		/* restore failed, escalate error to process error */
		process_error(thr->process, SIGSEGV);
		return;
	}

	/* restore signal mask */
	thr->sig.blocked_mask = blocked_mask;
	thr->sig.wait_mask = ~blocked_mask;
}

/** change the current thread's signal mask */
sys_sig_mask_t sig_mask(
	struct thread *thr,
	sys_sig_mask_t clear_mask,
	sys_sig_mask_t set_mask)
{
	sys_sig_mask_t previous_blocked_mask;
	sys_sig_mask_t blocked_mask;

	assert(thr == current_thread());

	previous_blocked_mask = thr->sig.blocked_mask;

	blocked_mask = thr->sig.blocked_mask;
	blocked_mask &= ~clear_mask;
	blocked_mask |= set_mask;
	thr->sig.blocked_mask = blocked_mask;
	thr->sig.wait_mask = ~blocked_mask;

	return previous_blocked_mask;
}

/** wait for signals and retrieve signal information */
err_t sig_wait(
	sys_sig_mask_t wait_mask,
	unsigned int flags __unused,
	sys_timeout_t timeout,
	unsigned int clk_id_absflag,
	struct sys_sig_info *info_user)
{
	struct sys_sig_info info;
	struct thread *thr;
	err_t err;

	thr = current_thread();

	err = sig_poll_if_pending(thr, &info, wait_mask);
	if (err == EOK) {
		return user_copy_out_aligned(info_user, &info, sizeof(info));
	}
	if (err != EAGAIN) {
		return err;
	}

	/* short cut for non-blocking polling mode */
	if (timeout == TIMEOUT_NULL) {
		return EAGAIN;
	}

	/* must wait -- prepare arguments for continuation after waiting */
	thr->cont_args.sig_wait.info_user = info_user;

	/* unblock the signals to wait for */
	assert(thr->sig.wait_mask == ~thr->sig.blocked_mask);
	thr->sig.wait_mask = wait_mask;

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_SIG, timeout, clk_id_absflag);
	thread_continue_at(thr, sig_wait_finish);
	thr->wakeup_code = EAGAIN;	/* EAGAIN */
	thread_cancel_at(thr, sig_wait_cancel);
	sched_unlock(thr->sched);

	return EAGAIN;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void sig_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	assert(thr != NULL);
	assert((wakeup_code == EINTR) ||
	       (wakeup_code == ETIMEDOUT) ||
	       (wakeup_code == ECANCEL));

	/* set error code to EAGAIN instead of ETIMEDOUT */
	if (wakeup_code == ETIMEDOUT) {
		/* target thread wakeup_code is already EAGAIN */
		//thr->wakeup_code = EAGAIN;
		return;
	}

	sched_lock(thr->sched);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	sched_unlock(thr->sched);
}

static void sig_wait_finish(struct thread *thr)
{
	sys_sig_mask_t wait_mask;
	struct sys_sig_info info;
	err_t err;

	/* restore original blocked mask */
	wait_mask = thr->sig.wait_mask;
	thr->sig.wait_mask = ~thr->sig.blocked_mask;

	err = sig_poll_if_pending(thr, &info, wait_mask);
	if (err == EOK) {
		err = user_copy_out_aligned(thr->cont_args.sig_wait.info_user,
		                            &info, sizeof(info));
	}

	if (err == EAGAIN) {
		/* return original wakeup code (probably EAGAIN) */
		err = thr->wakeup_code;
	}

	arch_syscall_retval(thr->regs, err);
}

/* wait for signals and execute signal handler */
err_t sig_suspend(sys_sig_mask_t wait_mask)
{
	struct thread *thr;

	thr = current_thread();

	/* unblock the signals to wait for
	 * (the original blocked signals will be restored by sys_sig_restore())
	 */
	assert(thr->sig.wait_mask == ~thr->sig.blocked_mask);
	thr->sig.wait_mask = wait_mask;

	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_SIG, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
	thread_continue_at_wait_finish(thr);
	thr->wakeup_code = EINTR;
	thread_cancel_at(thr, sig_suspend_cancel);
	sched_unlock(thr->sched);

	/* NOTE: the return type of the system call is void (error code ignored) */
	return EINTR;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void sig_suspend_cancel(struct thread *thr, err_t wakeup_code)
{
	assert((wakeup_code == EINTR) || (wakeup_code == ECANCEL));

	sched_lock(thr->sched);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	sched_unlock(thr->sched);
}

/** drain thread/process's queued signals (locks process_lock internally) */
void sig_drain(
	struct process *proc,
	struct sig_queue_root *pending)
{
	struct sig_queue_node *sigqn;
	list_t *node;

	assert(proc != NULL);
	assert(pending != NULL);

	process_lock(proc);

	list_for_each_remove_first(&pending->sigqh, node) {
		sigqn = SIG_FROM_SIGQ(node);

		process_unlock(proc);

		/* NOTE: proc_lock must not be taken; locks part_lock internally */
		sig_free_sig_queue_node(proc->part, sigqn);

		process_lock(proc);
	}

	process_unlock(proc);
}

////////////////////////////////////////////////////////////////////////////////

err_t sys_abort(ulong_t caller)
{
	struct thread *thr;

	// FIXME: debug output to help debugging
	console_lock();
	kernel_print_current();
	printk("abort called from 0x%lx\n", caller);
	console_unlock();

#ifdef AUTOBUILD
	console_lock();
	printk("AUTOBUILD: halt on sys_abort()\n");
	console_unlock();
	bsp_halt(BOARD_POWEROFF);
#endif
#ifdef COVBUILD
	console_lock();
	printk("COVBUILD: halt on sys_abort()\n");
	console_unlock();
	bsp_halt(BOARD_POWEROFF);
#endif

	/* defer to not mess up any arguments when the thread is restarted */
	thr = current_thread();
	thr->cont_args.abort_caller = caller;
	thread_defer_to(thr, sys_abort_deferred);

	return EOK;
}

static void sys_abort_deferred(struct thread *thr)
{
	struct sys_sig_info info;

	/* prepare signal information */
	info.sig = SIGABRT;
	info.code = 0;
	info.ex_type = 0;
	info.status = 0;
	info.addr = thr->cont_args.abort_caller;

	/* clear exception information when sending a software signal */
	sig_exception(thr, SIGABRT, &info);
}

err_t sys_sig_return(const struct regs *regs_user, sys_sig_mask_t sig_mask)
{
	struct thread *thr;

	/* defer to not mess up the register context */
	thr = current_thread();
	thr->cont_args.sig_return.regs_user = regs_user;
	thr->cont_args.sig_return.sig_mask = sig_mask;
	thread_defer_to(thr, sys_sig_return_deferred);

	return EOK;
}

static void sys_sig_return_deferred(struct thread *thr)
{
	sig_return(thr, thr->cont_args.sig_return.regs_user,
	                thr->cont_args.sig_return.sig_mask);
	/* at kernel exit, the kernel checks for new pending signals */
}

err_t sys_sig_register(ulong_t sig, ulong_t handler, sys_sig_mask_t sig_mask, ulong_t flags)
{
	if (sig >= NUM_SIGS) {
		/* invalid signal ID */
		return EINVAL;
	}

	if ((flags & ~SYS_SIG_STACK) != 0) {
		/* invalid flags */
		return EINVAL;
	}

	return sig_register(current_process(), sig, handler, sig_mask, flags);
}

err_t sys_sig_stack(addr_t base, size_t size)
{
	struct thread *thr;
	size_t needed;

	if (base != 0) {
		needed = sizeof(struct regs) + sizeof(struct sys_sig_info) + 64;
		if ((size < needed) || !USER_CHECK_RANGE(base, size)) {
			/* not valid user space region */
			return EINVAL;
		}
	} else {
		if (size != 0) {
			/* both must be NULL resp. 0 */
			return EINVAL;
		}
	}

	thr = current_thread();
	thr->sig.stack_base = base;
	thr->sig.stack_size = size;
	return EOK;
}

ulong_t sys_sig_mask(sys_sig_mask_t clear_mask, sys_sig_mask_t set_mask)
{
	sys_sig_mask_t previous_mask;
	struct thread *thr;

	thr = current_thread();
	previous_mask = sig_mask(thr, clear_mask, set_mask);

	/* On 32-bit systems, we play tricks with the ABI to return 64-bit values:
	 * the syscall stub accepts only a 32-bit return value
	 * and discards the second return register.
	 * We set the full 64-bit value here and return the related 32-bit part,
	 * so that the syscall stub will overwrite it again.
	 */
	return arch_syscall_return_64bit(thr->regs, previous_mask);
	/* at kernel exit, the kernel checks for new pending signals */
}

err_t sys_sig_send(ulong_t thr_id, ulong_t sig, const struct sys_sig_info *info_user)
{
	struct sys_sig_info info_kern;
	err_t err;

	if (sig >= NUM_SIGS) {
		/* invalid signal ID */
		return EINVAL;
	}

	if (info_user != NULL) {
		err = user_copy_in_aligned(&info_kern, info_user, sizeof(info_kern));
		if (err != EOK) {
			/* copy error */
			return err;
		}
		/* sanitize */
		info_kern.sig = sig;
	}

	err = sig_send_info(current_process(), thr_id, sig,
	                    (info_user != NULL) ? &info_kern : NULL);

	return err;
	/* at kernel exit, the kernel checks for new pending signals */
}

ulong_t sys_sig_pending(void)
{
	sys_sig_mask_t pending_mask;
	struct thread *thr;

	thr = current_thread();
	pending_mask = thr->process->sig.pending.cached_pending_mask;
	pending_mask |= thr->sig.pending.cached_pending_mask;

	/* On 32-bit systems, we play tricks with the ABI to return 64-bit values:
	 * the syscall stub accepts only a 32-bit return value
	 * and discards the second return register.
	 * We set the full 64-bit value here and return the related 32-bit part,
	 * so that the syscall stub will overwrite it again.
	 */
	return arch_syscall_return_64bit(thr->regs, pending_mask);
}

err_t sys_sig_wait(sys_sig_mask_t wait_mask, ulong_t flags, sys_timeout_t timeout, ulong_t clk_id_absflag, struct sys_sig_info *info_user)
{
	if (info_user == NULL) {
		/* output parameter required */
		return EINVAL;
	}
	if (flags != 0) {
		/* invalid flags -- must be zero */
		return EINVAL;
	}
	if ((clk_id_absflag & ~TIMEOUT_ABSOLUTE) >= NUM_CLK_IDS) {
		/* invalid clock ID */
		return EINVAL;
	}

	return sig_wait(wait_mask, flags, timeout, clk_id_absflag, info_user);
}

err_t sys_sig_suspend(sys_sig_mask_t wait_mask)
{
	return sig_suspend(wait_mask);
}
