/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2020, 2021, 2024, 2025 Alexander Zuepke */
/*
 * thread.c
 *
 * Threading
 *
 * azuepke, 2018-01-02: initial
 * azuepke, 2025-02-22: processes
 */

#include <thread.h>
#include <part.h>
#include <sched.h>
#include <current.h>
#include <percpu.h>
#include <adspace.h>
#include <sig.h>
#include <irq.h>
#include <futex.h>
#include <kernel.h>
#include <uaccess.h>
#include <marron/bit.h>
#include <stdbool.h>
#include <sys_proto.h>
#include <time.h>
#include <process.h>


/* forward declaration */
static void thread_init(
	struct thread *thr);
static struct regs *switch_threads(
	struct thread *old_thr,
	struct thread *new_thr);
static struct regs *switch_threads_fini(
	struct thread *thr);
static void sys_cpu_set_deferred(
	struct thread *thr);

/** Callback for cleanup during wakeup (timeout, thread deletion) */
static void thread_suspend_wait_cancel(struct thread *thr, err_t wakeup_code);

/** initialize all thread objects at boot time to be blocked in STATE_DEAD */
__init void thread_init_all(void)
{
	for (unsigned int i = 0; i < thread_num; i++) {
		struct thread *thr = &thread_dyn[i];

		/* thr->part is set in part_init_all() */
		thr->process = NULL;
		thr->regs = &thread_regs[i];
		thr->sched = NULL;
		thr->global_thr_id = i;
		/* thr->max_prio is set in part_init_all() */
		list_head_init(&thr->sig.pending.sigqh);
		thr->state = THREAD_STATE_DEAD;
		thr->ipc.rhnd = 0;
	}
}


/** allocate a thread (locks part_lock internally) */
struct thread *thread_alloc(struct part *part)
{
	struct thread *thr;
	list_t *node;

	assert(part != NULL);

	part_lock(part);

	node = list_remove_first(&part->thread_freeqh);
	if (node != NULL) {
		thr = THR_FROM_FREEQ(node);
		assert(thr->part == part);
		assert(thr->process == NULL);
		assert(thr->sched == NULL);
	} else {
		thr = NULL;
	}

	part_unlock(part);

	return thr;
}

/** free a thread (NOTE: locks part internally) */
static void thread_free(struct part *part, struct thread *thr)
{
	assert(part != NULL);
	assert(thr != NULL);

	part_lock(part);

	assert(thr->process != NULL);
	thr->process = NULL;
	thr->sched = NULL;
	list_node_init(&thr->thread_freeql);
	list_insert_last(&part->thread_freeqh, &thr->thread_freeql);

	part_unlock(part);
}

/** make thread active and visible, and assigns the final thread ID (locks process_lock internally) */
static err_t thread_make_active(struct process *proc, struct thread *thr)
{
	unsigned int thr_id;
	err_t err;

	assert(proc != NULL);
	assert(thr != NULL);
	assert(thr->process == NULL);

	process_lock(proc);

	for (thr_id = 0; thr_id < PROCESS_THREAD_NUM; thr_id++) {
		if (proc->thread[thr_id] == NULL) {
			break;
		}
	}
	if (thr_id == PROCESS_THREAD_NUM) {
		err = EAGAIN;
		goto out_unlock;
	}

	proc->thread[thr_id] = thr;
	thr->process = proc;
	thr->thr_id = thr_id;

	list_node_init(&thr->thread_activeql);
	list_insert_last(&proc->thread_activeqh, &thr->thread_activeql);

	err = EOK;

out_unlock:
	process_unlock(proc);

	return err;
}

/** make thread inactive (must be called with process_lock) */
static void thread_make_inactive(struct process *proc, struct thread *thr)
{
	unsigned int thr_id;

	assert(proc != NULL);
	assert_process_locked(proc);
	assert(thr != NULL);

	thr_id = thr->thr_id;
	assert(proc->thread[thr_id] == thr);
	proc->thread[thr_id] = NULL;

	list_remove(&thr->thread_activeql);
}

/** lookup thread in process (process_lock is locked) */
err_t thread_lookup_locked(struct process *proc, ulong_t thr_id, struct thread **thr_p)
{
	struct thread *thr;

	assert(proc != NULL);
	assert(thr_p != NULL);

	if (thr_id >= PROCESS_THREAD_NUM) {
		/* thread ID out of limits */
		return ELIMIT;
	}

	thr = proc->thread[thr_id];
	if (thr == NULL) {
		/* thread not active */
		return ENOENT;
	}

	*thr_p = thr;
	return EOK;
}

/** lookup thread in process (unlocked) */
err_t thread_lookup(struct process *proc, ulong_t thr_id, struct thread **thr_p)
{
	err_t err;

	process_lock(proc);

	err = thread_lookup_locked(proc, thr_id, thr_p);

	process_unlock(proc);

	return err;
}

/** create idle thread at boot time */
__init struct thread *thread_create_idle(struct process *proc)
{
	struct thread *thr;
	err_t err;

	thr = thread_alloc(proc->part);
	assert(thr != NULL);

	thread_init(thr);

	err = thread_make_active(proc, thr);
	assert(err == EOK);
	(void)err;

	assert(thr == &thread_dyn[thr->thr_id]);

	return thr;
}


/** initialize a thread object */
static void thread_init(struct thread *thr)
{
	assert(thr->part != NULL);
	assert(thr->regs != NULL);
	assert(thr->sched == NULL);

	thr->syscalldefer = NULL;
	thr->continuation = NULL;
	thr->cancellation = NULL;

	/* reset thread state */
	thr->state = THREAD_STATE_DEAD;
	thr->fpu_state = FPU_STATE_AUTO;
	thr->suspend_wakeup = 0;

	thr->prio = 0;
	thr->timeout = TIMEOUT_INFINITE;
	thr->cpu_mask = 0;

	list_head_init(&thr->sig.pending.sigqh);
	thr->sig.pending.unqueued_pending_mask = 0;
	thr->sig.pending.cached_pending_mask = 0;
	thr->sig.blocked_mask = SIG_MASK_ALL;
	thr->sig.wait_mask = SIG_MASK_NONE;
	thr->sig.stack_base = 0;
	thr->sig.stack_size = 0;

	thr->user_tls = NULL;
	thr->stat_exec_time = 0;

	thr->ipc.rhnd = 0;
}

/** update next priority in user space */
static inline void update_next_prio(
	struct thread *thr)
{
	int next_prio;

	assert(thr != NULL);
	assert(thr->sched->current == thr);

	if (thr->user_tls != NULL) {
		next_prio = thr->sched->next_highest_prio;
		if (next_prio < 0) {
			next_prio = 0;
		}
		(void)user_put_1_nocheck(&thr->user_tls->next_prio, next_prio & 0xff);
	}
}

////////////////////////////////////////////////////////////////////////////////

/* context switch core code */

/* perform context switch on kernel exit if necessary
 *
 * NOTE: we keep the real switch in a dedicated function,
 * this helps to the compiler to optimize better
 *
 * NOTE: we either return a register context, or NULL if we need to run again;
 * this is necessary to execute continuations once again
 */
struct regs *kernel_schedule(void)
{
	void (*continuation)(struct thread *);
	struct thread *current;
	struct thread *next;
	struct sched *sched;
	sys_time_t now;

	/* if any interrupts are pending, take them now
	 * the interrupt handler sets sched->reschedule below
	 */
	if (arch_irq_pending()) {
		bsp_irq_poll();
	}

	current = current_thread();
	sched = current->sched;

	if (sched->reschedule == 0) {
		/* continuation callback */
		continuation = current->continuation;
		if (continuation != NULL) {
			current->continuation = NULL;
			continuation(current);
			return NULL;
		}

		/* shortcut: immediately return to thread, send signals if any */
		return sig_send_if_pending(current);
	}

	current->prio = thread_prio_get(current);
	now = bsp_timer_get_time();

	sched_lock(sched);
	sched_preempt_or_yield(now, sched);
	sched_readyq_next(sched);
	sched_replenish_quantum(now, sched);
	sched_unlock(sched);

	next = sched->current;

	if (next != current) {
		return switch_threads(current, next);
	}

	return switch_threads_fini(current);
}

/** perform context switch */
/* NOTE: SMP: runs unlocked */
static struct regs *switch_threads(
	struct thread *old_thr,
	struct thread *new_thr)
{
	assert(old_thr != NULL);
	assert(new_thr != NULL);
	assert(new_thr != old_thr);
	assert(new_thr->state == THREAD_STATE_CURRENT);

	// switch additional registers
	arch_reg_save(old_thr->regs);

	if (old_thr->fpu_state == FPU_STATE_ON) {
		arch_fpu_save(old_thr->regs);
		arch_fpu_disable();
	}

	arch_reg_switch();
	percpu_thread_switch(new_thr);

	/* switch address space when partition changes */
	if (new_thr->part != old_thr->part) {
		arch_adspace_switch(old_thr->process->adspace, new_thr->process->adspace);
	}

	arch_reg_restore(new_thr->regs);

	if (new_thr->fpu_state == FPU_STATE_ON) {
		arch_fpu_enable();
		arch_fpu_restore(new_thr->regs);
	}

	return switch_threads_fini(new_thr);
}

/** finalize context switch */
/* NOTE: SMP: runs unlocked */
static struct regs *switch_threads_fini(
	struct thread *thr)
{
	void (*continuation)(struct thread *);

	assert(thr != NULL);

	/* continuation callback */
	continuation = thr->continuation;
	if (continuation != NULL) {
		thr->continuation = NULL;
		continuation(thr);
		return NULL;
	}

	/* update next_prio in user space after address space switch */
	update_next_prio(thr);

	/* return to thread, send signals if any */
	return sig_send_if_pending(thr);
}

////////////////////////////////////////////////////////////////////////////////

/** Continuation callback to set the wakeup code after waiting */
void thread_wait_finish(struct thread *thr)
{
	err_t wakeup_code;

	wakeup_code = thr->wakeup_code;
	arch_syscall_retval(thr->regs, wakeup_code);

	if (wakeup_code == ERESTARTSYS) {
		/* restart system call:
		 * this restore the original system call argument
		 * clobbered by the return value,
		 and unwinds the instruction pointer to restart the system call
		 */
		arch_syscall_restart(thr->regs);
	}
}

/** setup user space TLS of a thread */
err_t thread_setup_user_tls(
	struct thread *thr,
	struct sys_tls *tls)
{
	struct sys_tls tls_data = { 0 };
	int next_prio;
	err_t err;

	assert(thr != NULL);
	assert(tls != NULL);

	thr->user_tls = tls;

	/* initial setup of TLS data */
	tls_data.tls = tls;
	tls_data.thr_id = thr->thr_id;
	tls_data.user_prio = thr->prio & 0xffu;
	next_prio = thr->sched->next_highest_prio;
	if (next_prio < 0) {
		next_prio = 0;
	}
	tls_data.next_prio = next_prio & 0xffu;
	tls_data.max_prio = thr->max_prio;
	tls_data.cpu_id = thr->sched->cpu;

	/* user TLS pointer is a valid user space pointer, but might be unmapped */
	err = user_copy_out_aligned(tls, &tls_data, sizeof(tls_data));
	return err;
}

/** setup user space TLS on the first run of a thread */
static void thread_setup_user_tls_first_run(
	struct thread *thr)
{
	assert(thr == current_thread());

	(void)thread_setup_user_tls(thr, thr->cont_args.ptr);
	/* NOTE: any errors to setup a thread's TLS are ignored */
}

/** start a thread to execute a continuation in the kernel */
void thread_start_kern(
	struct process *proc,
	struct thread *thr,
	void (*continuation)(struct thread *),
	void *arg,
	int prio,
	unsigned int quantum,
	unsigned long cpu_mask)
{
	unsigned int first_cpu;
	err_t err;

	assert(proc != NULL);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_DEAD);

	thread_init(thr);

	/* run continuation before switching to user space */
	thr->cont_args.ptr = arg;
	thread_continue_at(thr, continuation);

	/* set prio + CPU to given values */
	thr->prio = prio;
	thr->quantum = quantum;
	thr->cpu_mask = cpu_mask;
	first_cpu = bit_flsl(cpu_mask);
	thr->sched = sched_per_cpu(first_cpu);

	/* the thread's user space registers remain unmodified */

	/* initialize FPU registers */
	thr->fpu_state = FPU_STATE_AUTO;
	arch_reg_fpu_init_disabled(thr->regs);

	/* become visible as active thread */
	err = thread_make_active(proc, thr);
	assert(err == EOK);
	(void)err;

	sched_lock(thr->sched);
	thread_wakeup(thr);
	sched_unlock(thr->sched);
}

/** start a thread in user mode */
static void thread_start_user(
	struct process *proc,
	struct thread *thr,
	ulong_t entry,
	ulong_t arg,
	ulong_t stack,
	ulong_t tls,
	int prio,
	unsigned int quantum,
	unsigned long cpu_mask)
{
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_DEAD);

	/* setup user regs */
	arch_reg_init(thr->regs, entry, stack, tls, arg, 0, 0, 0, 0);

	/* initialize TLS on first run via thread_setup_user_tls_first_run() */
	thread_start_kern(proc, thr,
	                  thread_setup_user_tls_first_run, (struct sys_tls *)tls,
	                  prio, quantum, cpu_mask);
}

/** kill a thread in any state (called with proc_lock) */
void thread_kill(struct thread *thr)
{
	void (*cancellation)(struct thread *, err_t wakeup_code);
	unsigned int prev_thr_state;
	struct process *proc;
	struct sched *sched;
	sys_time_t now;

	assert(thr != NULL);
	proc = thr->process;
	assert_process_locked(proc);

	thread_make_inactive(proc, thr);

	sched = thr->sched;
	sched_lock(sched);

	cancellation = thr->cancellation;
	thr->cancellation = NULL;

	thr->continuation = NULL;

	prev_thr_state = thr->state;
	thr->state = THREAD_STATE_DEAD;

	switch (prev_thr_state) {
	case THREAD_STATE_CURRENT:
		/* CURRENT -> DEAD */
		// FIXME: not true when killing threads on other CPUs!
		assert(thr == current_thread());
		thr->timeout = TIMEOUT_INFINITE;

		/* The current thread dies:
		 * Perform a fake switch to the idle thread first
		 * to save our registers, then change the state to dead,
		 * so we can be restarted properly afterwards.
		 */
		now = bsp_timer_get_time();
		thr->stat_exec_time += now - sched->stat_last_switch;
		sched->stat_last_switch = now;
		sched->current = sched->idle;
		sched->idle->state = THREAD_STATE_CURRENT;
		switch_threads(thr, sched->idle);
		sched->reschedule = 1;
		break;

	case THREAD_STATE_READY:
		/* READY -> DEAD */
		sched_remove(sched, thr);
		break;

	case THREAD_STATE_WAIT_SLEEP:
		/* WAIT_SLEEP -> DEAD */
		/* clear any pending timeout */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_SUSPEND:
		/* WAIT_SUSPEND -> DEAD */
		/* no timeout here, just change state */
		assert(thr->timeout == TIMEOUT_INFINITE);
		break;

	case THREAD_STATE_WAIT_IRQ:
		/* WAIT_IRQ -> DEAD */
		/* clear any pending timeout, then release and mask IRQ */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_FUTEX:
		/* WAIT_FUTEX -> DEAD */
		/* clear any pending timeout, then remove from futex queue */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_SIG:
		/* WAIT_SIG -> DEAD */
		/* clear any pending timeout */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_TIMER:
		/* WAIT_TIMER -> DEAD */
		/* no timeout here; cancellation removes thread from timer wait queue */
		assert(thr->timeout == TIMEOUT_INFINITE);
		break;

	case THREAD_STATE_WAIT_IPC_SEND:
	case THREAD_STATE_WAIT_IPC_REPLY:
	case THREAD_STATE_WAIT_IPC_WAIT:
		/* no timeout here; cancellation removes thread from IPC wait queue */
		assert(thr->timeout == TIMEOUT_INFINITE);
		break;

	case THREAD_STATE_WAIT_EPOLL:
		/* clear any pending timeout */
		sched_timeout_release(sched, thr);
		break;

	case THREAD_STATE_WAIT_EVENTFD:
		/* no timeout here; cancellation removes thread from eventfd wait queue */
		assert(thr->timeout == TIMEOUT_INFINITE);
		break;

	case THREAD_STATE_WAIT_SIGNALFD:
		/* no timeout here; cancellation removes thread from signalfd wait queue */
		assert(thr->timeout == TIMEOUT_INFINITE);
		break;

	default:
		assert(prev_thr_state == THREAD_STATE_DEAD);
		/* already DEAD */
		break;
	}

	sched_unlock(sched);
	process_unlock(proc);

	/* NOTE: SMP: the following runs unlocked */
	if (cancellation != NULL) {
		cancellation(thr, ECANCEL);
	}

	assert(thr->state == THREAD_STATE_DEAD);
	assert(thr->ipc.rhnd == 0);

	/* drain the thread's signals */
	sig_drain(proc, &thr->sig.pending);

	thread_free(proc->part, thr);

	/* the caller expects process_lock on return */
	process_lock(proc);
}

/** let current thread wait in wait state with timeout and given wait priority */
void thread_wait_prio(struct thread *thr, unsigned int state, sys_time_t timeout, unsigned int clk_id_absflag, unsigned int wait_prio)
{
	assert(thr != NULL);
	assert(thr == current_thread());
	assert(state > THREAD_STATE_READY);
	assert_sched_locked(thr->sched);

	thr->state = state & 0xff;
	thr->prio = wait_prio;

	/* decode timeout */
	if ((clk_id_absflag & TIMEOUT_ABSOLUTE) != 0) {
		/* absolute timeout */
		thr->timeout = timeout;
		clk_id_absflag &= ~TIMEOUT_ABSOLUTE;
	} else {
		/* relative timeout */
		assert(clk_id_absflag < NUM_CLK_IDS);
		thr->timeout = time_add(timeout, bsp_timer_get_time());
		thr->timeout += arch_load64(&clock_realtime_offset[clk_id_absflag]);
	}

	assert(clk_id_absflag < NUM_CLK_IDS);
	thr->clk_id = clk_id_absflag;

	sched_wait(thr->sched, thr);
}

/** wake a thread and make it READY again */
void thread_wakeup(struct thread *thr)
{
	struct sched *sched;

	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);
	assert_sched_locked(thr->sched);

	sched = thr->sched;

	sched_wakeup(sched, thr);

	/* ready queue changed -- update next priority in user space */
	update_next_prio(sched->current);

#ifdef SMP
	if (sched->cpu != current_cpu_id()) {
		/* notify target CPU */
		bsp_cpu_reschedule(sched->cpu);
	}
#endif
}

/** release the timeout of a blocked thread */
void thread_timeout_release(struct thread *thr)
{
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	sched_lock(thr->sched);
	sched_timeout_release(thr->sched, thr);
	sched_unlock(thr->sched);
}

/** preempt current thread (enforce priority changes) */
/* NOTE: SMP: this runs unlocked */
err_t thread_preempt(struct thread *thr)
{
	assert(thr == current_thread());

	thr->prio = thread_prio_get(thr);
	//sched_check_preempt(thr->sched);
	/* FIXME: enforce scheduling to update next_prio in user space */
	thr->sched->reschedule = 1;
	/* NOTE: we schedule anyway, no need to sync next priority in user space */
	return EOK;
}

/** yield current thread */
err_t thread_yield(struct thread *thr)
{
	assert(thr == current_thread());

	thr->prio = thread_prio_get(thr);
	sched_lock(thr->sched);
	sched_yield(thr->sched, thr);
	sched_unlock(thr->sched);
	/* NOTE: we schedule anyway, no need to sync next priority in user space */
	return EOK;
}

/** get bounded user priority of current thread */
/* NOTE: SMP: this runs unlocked */
int thread_prio_get(struct thread *thr)
{
	int user_prio;
	err_t err;

	assert(thr == current_thread());

	if (thr->user_tls == NULL) {
		return thr->prio;
	}

	/* bound priority in 0..thr->max_prio */
	/* NOTE: the user priority is of type uint8_t */
	err = user_get_1_nocheck(&thr->user_tls->user_prio, user_prio);
	if (err != EOK) {
		/* access failure */
		return 0;
	}
	if (user_prio > thr->max_prio) {
		user_prio = thr->max_prio;
	}
	assert(user_prio >= 0);
	assert(user_prio < NUM_PRIOS);

	return user_prio;
}

/** change priority of current thread */
/* NOTE: SMP: this runs unlocked */
int thread_prio_change(struct thread *thr, int new_prio)
{
	int old_prio;

	assert(thr == current_thread());

	/* bound priority in 0..thr->max_prio */
	/* NOTE: assume priority as unsigned and map negative ones to max */
	if ((new_prio < 0) || (new_prio > thr->max_prio)) {
		new_prio = thr->max_prio;
	}
	assert(new_prio >= 0);
	assert(new_prio < NUM_PRIOS);

	old_prio = thread_prio_get(thr);

	thr->prio = new_prio;
	if (thr->user_tls != NULL) {
		(void)user_put_1_nocheck(&thr->user_tls->user_prio, new_prio & 0xffu);
	}

	// NOTE: SMP: no sched_lock here
	sched_check_preempt(thr->sched);

	return old_prio;
}

/** change priority + quantum of any thread */
err_t thread_prio_quantum_change(struct thread *thr, struct sys_sched_param *new_sched, struct sys_sched_param *old_sched)
{
	err_t err;

	assert(thr != NULL);
	assert(thr->process == current_thread()->process);
	assert(old_sched != NULL);

	sched_lock(thr->sched);

	if (thr->state == THREAD_STATE_DEAD) {
		/* thread is dead */
		err = ESTATE;
	} else {
		old_sched->prio = thr->prio & 0xffu;
		old_sched->quantum = thr->quantum;

		if (new_sched != NULL) {
			/* apply new parameters */
			if (thr->user_tls != NULL) {
				(void)user_put_1_nocheck(&thr->user_tls->user_prio, new_sched->prio & 0xffu);
			}
			sched_prio_quantum_change(thr->sched, thr, new_sched->prio, new_sched->quantum);
		}
		err = EOK;
	}

	sched_unlock(thr->sched);

	return err;
}

/** change CPU of current thread */
void thread_cpu_migrate(struct thread *thr, unsigned int new_cpu)
{
	struct sched *old_sched;
	struct sched *new_sched;
	sys_time_t now;

	assert(thr == current_thread());
	assert(new_cpu < cpu_num);

	/* sync user space priority before migration */
	thr->prio = thread_prio_get(thr);

	/* We must save all our registers before migration,
	 * otherwise we could we woken up too early on the new CPU.
	 * So we perform a fake switch to the idle thread first
	 * to save our registers, then we migrate,
	 * and finally reschedule on our CPU.
	 */
	old_sched = thr->sched;
	new_sched = sched_per_cpu(new_cpu);
	if (new_sched == old_sched) {
		return;
	}

	/* update TLS for sys_cpu_get() */
	if (thr->user_tls != NULL) {
		(void)user_put_1_nocheck(&thr->user_tls->cpu_id, new_cpu & 0xffu);
	}

	now = bsp_timer_get_time();

	/* lock both CPU's scheduling state for a smooth transition */
	if (old_sched < new_sched) {
		sched_lock(old_sched);
		sched_lock(new_sched);
	} else {
		sched_lock(new_sched);
		sched_lock(old_sched);
	}

	thr->stat_exec_time += now - old_sched->stat_last_switch;
	old_sched->stat_last_switch = now;
	old_sched->current = old_sched->idle;
	old_sched->idle->state = THREAD_STATE_CURRENT;
	switch_threads(thr, old_sched->idle);

	old_sched->reschedule = 1;

	sched_migrate(new_sched, thr);

	sched_unlock(old_sched);
	sched_unlock(new_sched);

	/* NOTE: we schedule anyway, no need to sync next priority in user space */
}

/** change CPU mask of any thread */
err_t thread_cpu_mask_change(struct thread *thr, ulong_t new_cpu_mask, ulong_t *old_cpu_mask)
{
	err_t err;

	assert(thr != NULL);
	assert(thr->process == current_thread()->process);
	assert(old_cpu_mask != NULL);

	sched_lock(thr->sched);

	if (thr->state == THREAD_STATE_DEAD) {
		/* thread is dead */
		err = ESTATE;
	} else {
		*old_cpu_mask = thr->cpu_mask;
		err = EOK;

		if (new_cpu_mask != 0) {
			/* FIXME: case not implemented */
#if 0
			thr->cpu_mask = new_cpu_mask;
			/* check if migration is required */
			if (((1UL << thr->sched->cpu) & new_cpu_mask) == 0) {
				/* FIXME: migrate */
				/* assign thread to first CPU in affinity mask */
				first_cpu = bit_flsl(new_cpu_mask);
			}
#endif
			err = ESTATE;
		}
	}

	sched_unlock(thr->sched);

	return err;
}

/** suspend current thread */
err_t thread_suspend(struct thread *thr)
{
	err_t err;

	assert(thr == current_thread());

	sched_lock(thr->sched);
	if (thr->suspend_wakeup != 0) {
		/* clear pending suspension wakeup request */
		thr->suspend_wakeup = 0;
		err = EAGAIN;
	} else {
		/* suspend thread */
		thread_wait(thr, THREAD_STATE_WAIT_SUSPEND, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
		thread_continue_at_wait_finish(thr);
		thread_cancel_at(thr, thread_suspend_wait_cancel);
		err = EOK;
	}
	sched_unlock(thr->sched);

	return err;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void thread_suspend_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	/* no timeout here -- this is only called during thread deletion */
	assert(wakeup_code == ECANCEL);

	sched_lock(thr->sched);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	sched_unlock(thr->sched);
}

/** resume suspended thread */
err_t thread_resume(struct thread *thr)
{
	err_t err;

	assert(thr != NULL);

	sched_lock(thr->sched);
	if (thr->state == THREAD_STATE_DEAD) {
		/* thread is dead */
		err = ESTATE;
	} else if (thr->state != THREAD_STATE_WAIT_SUSPEND) {
		/* set pending suspension wakeup request */
		thr->suspend_wakeup = 1;
		err = EOK;
	} else {
		/* thread is suspended */
		thread_wakeup(thr);
		/* target thread wakeup_code is already EOK */
		//thr->wakeup_code = EOK;
		thread_cancel_clear(thr);
		err = EOK;
	}
	sched_unlock(thr->sched);

	return err;
}

/* Kernel timer callback, "now" refers to the current system time
 * The kernel should reprogram the next timer expiry.
 * The timer interrupt is never masked.
 */
void kernel_timer(uint64_t now)
{
	struct thread *thr;

	thr = current_thread();

	sched_timer(thr->sched, now);

	/* ready queue might have changed -- update next priority in user space */
	update_next_prio(thr);
}

////////////////////////////////////////////////////////////////////////////////

err_t sys_thread_create(
	uint32_t *thr_id_user,
	addr_t entry,
	addr_t arg,
	addr_t stack,
	addr_t tls_ptr,
	const struct sys_sched_param *sched_user,
	ulong_t cpu_mask)
{
	struct sys_sched_param sched;
	unsigned int thr_id;
	struct thread *thr;
	err_t err;

	err = user_copy_in_aligned(&sched, sched_user, sizeof(sched));
	if (err != EOK) {
		/* copy error */
		return err;
	}

	/* bound priority in 0..thr->max_prio */
	/* NOTE: assume priority as unsigned and map negative ones to max */
	if (sched.prio > current_thread()->max_prio) {
		sched.prio = current_thread()->max_prio;
	}

	cpu_mask &= current_part()->part_cfg->cpu_mask;
	if (cpu_mask == 0) {
		/* empty set of CPUs in affinity mask */
		return EINVAL;
	}

	if (tls_ptr == 0) {
		/* Invalid TLS pointer */
		return EINVAL;
	}
	if (!user_ptr_ok((struct sys_tls *)tls_ptr)) {
		/* access error */
		return EINVAL;
	}

	/* search for a dead thread to activate */
	err = ELIMIT;
	thr_id = 0;
	thr = thread_alloc(current_part());
	if (thr != NULL) {
		assert(thr->state == THREAD_STATE_DEAD);
		thread_start_user(current_process(), thr,
		                  entry, arg, stack, tls_ptr,
		                  sched.prio, sched.quantum, cpu_mask);
		err = EOK;
		thr_id = thr->thr_id;
	}

	if ((err == EOK) && (thr_id_user != NULL)) {
		err = user_put_4(thr_id_user, thr_id);
	}

	return err;
}

err_t sys_thread_exit(void)
{
	/* defer to not mess up any arguments when the thread is restarted */
	thread_defer_to(current_thread(), thread_exit_deferred);

	return EOK;
}

void thread_exit_deferred(struct thread *thr)
{
	struct process *proc;

	proc = thr->process;

	// FIXME: XXX: the last exiting thread should put the process into an error state

	process_lock(proc);
	thread_kill(thr);
	process_unlock(proc);
}

uint32_t sys_thread_self_syscall(void)
{
	return current_thread()->thr_id;
}

uint32_t sys_prio_get_syscall(void)
{
	return thread_prio_get(current_thread());
}

uint32_t sys_prio_set_syscall(ulong_t prio)
{
	return thread_prio_change(current_thread(), (int)prio);
}

uint32_t sys_prio_max_syscall(void)
{
	return current_thread()->max_prio;
}

uint32_t sys_cpu_get_syscall(void)
{
	return current_thread()->sched->cpu;
}

err_t sys_cpu_set(ulong_t cpu)
{
	struct thread *thr;

	if ((cpu >= cpu_num) ||
	        (((1UL << cpu) & current_part()->part_cfg->cpu_mask) == 0)) {
		/* CPU out of bounds or not available to partition */
		return ELIMIT;
	}

	/* defer the actual migration after we have saved the return code */
	thr = current_thread();
	thr->cont_args.new_cpu = cpu;
	thread_defer_to(thr, sys_cpu_set_deferred);
	return EOK;
}

static void sys_cpu_set_deferred(struct thread *thr)
{
	unsigned int cpu = thr->cont_args.new_cpu;

	thread_cpu_migrate(thr, cpu);
}

ulong_t sys_cpu_mask(void)
{
	return current_part()->part_cfg->cpu_mask;
}

uint32_t sys_fpu_state_get(void)
{
	return current_thread()->fpu_state;
}

err_t sys_fpu_state_set(ulong_t new_fpu_state)
{
	struct thread *thr;

	if ((new_fpu_state != FPU_STATE_OFF) &&
	    (new_fpu_state != FPU_STATE_AUTO) &&
	    (new_fpu_state != FPU_STATE_ON)) {
		/* invalid FPU state */
		return EINVAL;
	}

	thr = current_thread();

	if (new_fpu_state != thr->fpu_state) {
		/* the FPU state changes */
		/* make sure to disable the FPU if enabled */
		if (thr->fpu_state == FPU_STATE_ON) {
			arch_fpu_save(thr->regs);
			arch_reg_fpu_disable(thr->regs);
			arch_fpu_disable();
		}
		thr->fpu_state = new_fpu_state & 0xff;
		if (new_fpu_state == FPU_STATE_ON) {
			/* FPU was disabled before */
			arch_fpu_enable();
			arch_reg_fpu_enable(thr->regs);
			arch_fpu_restore(thr->regs);
		}
	}

	return EOK;
}

err_t sys_preempt(void)
{
	return thread_preempt(current_thread());
}

err_t sys_yield(void)
{
	return thread_yield(current_thread());
}

err_t sys_thread_suspend(void)
{
	/* function sets further error codes internally */
	return thread_suspend(current_thread());
}

err_t sys_thread_resume(ulong_t thr_id)
{
	struct thread *thr;
	err_t err;

	err = thread_lookup(current_process(), thr_id, &thr);
	if (err != EOK) {
		return err;
	}

	return thread_resume(thr);
}

err_t sys_thread_sched(
	ulong_t thr_id,
	ulong_t flags,
	const struct sys_sched_param *new_sched_user,
	struct sys_sched_param *old_sched_user)
{
	struct sys_sched_param new_sched, old_sched;
	struct thread *thr;
	err_t err;

	err = thread_lookup(current_process(), thr_id, &thr);
	if (err != EOK) {
		return err;
	}

	if (flags != 0) {
		/* invalid flags -- must be zero */
		return EINVAL;
	}

	if (new_sched_user != NULL) {
		err = user_copy_in_aligned(&new_sched, new_sched_user, sizeof(new_sched));
		if (err != EOK) {
			/* copy error */
			return err;
		}

		/* bound priority in 0..thr->max_prio */
		/* NOTE: assume priority as unsigned and map negative ones to max */
		if (new_sched.prio > thr->max_prio) {
			new_sched.prio = thr->max_prio;
		}
	}

	err = thread_prio_quantum_change(thr,
	                                 (new_sched_user != NULL) ? &new_sched : NULL,
	                                 &old_sched);
	if (err != EOK) {
		return err;
	}

	if (old_sched_user != NULL) {
		err = user_copy_out_aligned(old_sched_user, &old_sched, sizeof(old_sched));
		if (err != EOK) {
			/* copy error */
			return err;
		}
	}

	return err;
}

err_t sys_thread_cpu_mask(
	ulong_t thr_id,
	ulong_t flags,
	ulong_t new_cpu_mask,
	ulong_t *old_cpu_mask_user)
{
	ulong_t old_cpu_mask;
	struct thread *thr;
	err_t err;

	err = thread_lookup(current_process(), thr_id, &thr);
	if (err != EOK) {
		return err;
	}

	if (flags != 0) {
		/* invalid flags -- must be zero */
		return EINVAL;
	}

	if (new_cpu_mask != 0) {
		new_cpu_mask &= current_part()->part_cfg->cpu_mask;
		if (new_cpu_mask == 0) {
			/* empty set of CPUs in affinity mask */
			return EINVAL;
		}
	}

	err = thread_cpu_mask_change(thr, new_cpu_mask, &old_cpu_mask);
	if (err != EOK) {
		return err;
	}

	if (old_cpu_mask_user != NULL) {
		err = user_put_p(old_cpu_mask_user, old_cpu_mask);
		if (err != EOK) {
			/* copy error */
			return err;
		}
	}

	return err;
}

err_t sys_tls_set(struct sys_tls *tls_user)
{
	struct thread *thr;
	err_t err;

	if (tls_user == NULL) {
		/* Invalid TLS pointer */
		return EINVAL;
	}
	if (!user_ptr_ok(tls_user)) {
		/* access error */
		return EINVAL;
	}

	thr = current_thread();
	err = thread_setup_user_tls(thr, tls_user);

	arch_tls_set(thr->regs, (addr_t)tls_user);

	return err;
}
