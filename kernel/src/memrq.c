/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * memrq.c
 *
 * Memory requirement / pool allocation.
 *
 * azuepke, 2021-04-30: initial
 * azuepke, 2021-09-17: more sophisticated memory allocations
 */

#include <memrq.h>
#include <assert.h>
#include <marron/macros.h>
#include <stddef.h>
#include <marron/arch_defs.h>


/** initialize all memrq objects at boot time */
__init void memrq_init_all(void)
{
	const struct memrq_cfg *cfg;
	struct memrq *m;
	struct page *page;
	size_t num_pages;

	for (unsigned int i = 0; i < memrq_num; i++) {
		cfg = &memrq_cfg[i];
		m = &memrq_dyn[i];

		assert((cfg->phys & (PAGE_SIZE - 1)) == 0);
		assert((cfg->size & (PAGE_SIZE - 1)) == 0);
		assert(cfg->size > 0);

		num_pages = cfg->size / PAGE_SIZE;

		list_head_init(&m->freepageqh);
		m->free_pages = num_pages;
		spin_init(&m->lock);

		for (size_t j = 0; j < num_pages; j++) {
			page = &cfg->page[j];

			page->memrq_cfg = cfg;
			list_node_init(&page->freepageql);
			list_insert_last(&m->freepageqh, &page->freepageql);
			page->refcount = 0;
		}
	}
}

/** reset order of free pages in memrq at partition startup */
err_t memrq_page_reinit(const struct memrq_cfg *cfg)
{
	struct memrq *m;
	struct page *page;
	size_t num_pages;

	assert(cfg != NULL);

	m = cfg->memrq;
	num_pages = cfg->size / PAGE_SIZE;

	spin_lock(&m->lock);

	if (m->free_pages != num_pages) {
		spin_unlock(&m->lock);
		return ESTATE;
	}

	list_head_init(&m->freepageqh);
	for (size_t j = 0; j < num_pages; j++) {
		page = &cfg->page[j];

		list_node_init(&page->freepageql);
		list_insert_last(&m->freepageqh, &page->freepageql);
		assert(page->refcount == 0);
	}

	spin_unlock(&m->lock);

	return EOK;
}

/** allocate a page from memrq */
phys_addr_t memrq_page_alloc(const struct memrq_cfg *cfg)
{
	unsigned long pageno;
	struct memrq *m;
	struct page *page;
	list_t *node;

	assert(cfg != NULL);
	m = cfg->memrq;

	spin_lock(&m->lock);

	if (m->free_pages == 0) {
		spin_unlock(&m->lock);
		return PART_MEMRQ_ALLOC_FAIL;
	}
	m->free_pages--;

	node = list_remove_first_nonempty(&m->freepageqh);
	assert(node != NULL);

	page = PAGE_FROM_FREEPAGEQ(node);
	pageno = page - cfg->page;
	assert(pageno * PAGE_SIZE < cfg->size);

	assert(page->refcount == 0);
	page->refcount = 1;

	spin_unlock(&m->lock);

	return cfg->phys + pageno * PAGE_SIZE;
}

/** increment reference counter */
static inline void memrq_page_ref_inc(struct page *page)
{
	struct memrq *m;

	assert(page != NULL);

	assert(page->refcount < -1ul);
	if (page->refcount == 0) {
		m = page->memrq_cfg->memrq;

		/* remove page from free list */
		list_remove(&page->freepageql);

		assert(m->free_pages > 0);
		m->free_pages--;
	}
	page->refcount++;
}

/** decrement reference counter and free page if ref-count becomes zero */
static inline void memrq_page_ref_dec(struct page *page)
{
	struct memrq *m;

	assert(page != NULL);

	assert(page->refcount > 0);
	page->refcount--;
	if (page->refcount == 0) {
		m = page->memrq_cfg->memrq;

		/* insert at head to keep it hot in caches */
		list_insert_first(&m->freepageqh, &page->freepageql);

		assert(m->free_pages < page->memrq_cfg->size / PAGE_SIZE);
		m->free_pages++;
	}
}

/** ref a page from memrq */
void memrq_page_ref(const struct memrq_cfg *cfg, phys_addr_t phys)
{
	unsigned long pageno;
	struct page *page;

	assert(cfg != NULL);
	assert(phys >= cfg->phys);
	assert(phys < cfg->phys + cfg->size);

	pageno = (phys - cfg->phys) / PAGE_SIZE;
	page = &cfg->page[pageno];

	memrq_page_ref_inc(page);
}

/** unref a page from memrq */
void memrq_page_unref(const struct memrq_cfg *cfg, phys_addr_t phys)
{
	unsigned long pageno;
	struct page *page;

	assert(cfg != NULL);
	assert(phys >= cfg->phys);
	assert(phys < cfg->phys + cfg->size);

	pageno = (phys - cfg->phys) / PAGE_SIZE;
	page = &cfg->page[pageno];

	memrq_page_ref_dec(page);
}

/** get struct page from <memrq> by physical address */
struct page *memrq_addr2page(const struct memrq_cfg *cfg, phys_addr_t phys)
{
	unsigned long pageno;

	assert(cfg != NULL);

	if ((phys >= cfg->phys) && (phys < cfg->phys + cfg->size)) {
		pageno = (phys - cfg->phys) / PAGE_SIZE;
		return &cfg->page[pageno];
	}

	return NULL;
}

/** get struct page from <mem> by physical address */
struct page *mem_addr2page(phys_addr_t phys)
{
	const struct mem_cfg *cfg;
	unsigned long pageno;

	for (unsigned int i = 0; i < mem_num; i++) {
		cfg = &mem_cfg[i];

		if ((phys >= cfg->phys) && (phys < cfg->phys + cfg->size)) {
			pageno = (phys - cfg->phys) / PAGE_SIZE;
			return &cfg->page[pageno];
		}
	}

	return NULL;
}
