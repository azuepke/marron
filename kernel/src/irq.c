/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2025 Alexander Zuepke */
/*
 * irq.c
 *
 * IRQ handling.
 *
 * azuepke, 2018-01-13: initial
 * azuepke, 2025-01-06: irqfd
 * azuepke, 2025-01-08: epoll
 */

#include <marron/error.h>
#include <irq.h>
#include <marron/compiler.h>
#include <bsp.h>
#include <assert.h>
#include <stddef.h>
#include <thread.h>
#include <current.h>
#include <sched.h>
#include <stdbool.h>
#include <sys_proto.h>
#include <uaccess.h>
#include <part.h>
#include <string.h>
#include <fd.h>
#include <marron/irq.h>
#include <marron/fcntl.h>
#include <marron/ipc_io.h>
#include <epoll.h>
#include <process.h>


/** Callback for cleanup during wakeup (timeout, thread deletion) */
static void irqfd_wait_cancel(struct thread *thr, err_t wakeup_code);
static void irqfd_wait_finish(struct thread *thr);

static_assert(offsetof(struct thread, cont_args.irq.recv_msg_user) ==
              offsetof(struct thread, cont_args.ipc_wait.recv_msg_user));

/** initialize all IRQ objects at boot time */
__init void irq_init_all(void)
{
	const struct irq_cfg *cfg;
	struct irq *irq;

	assert(irq_table_num <= bsp_irq_num);

	for (unsigned int i = 0; i < irq_num; i++) {
		cfg = &irq_cfg[i];
		irq = &irq_dyn[i];
		assert(cfg->irq_id < irq_table_num);
		assert(irq_table[cfg->irq_id] == irq);

		/* irq->part is assigned later */
		irq->irq_id = cfg->irq_id;
		irq->fd = NULL;
		irq->enabled = false;
		irq->ctl_enabled = false;
		irq->fired = false;
		irq->cpu_id = 0;
		irq->waiter = NULL;
		irq->count = 0;

		spin_init(&irq->lock);
	}
}

/** wake thread waiting for interrupt */
static void irq_wake(struct irq *irq, err_t wakeup_code)
{
	struct thread *thr;

	assert(irq != NULL);
	assert((wakeup_code == EOK) || (wakeup_code == ECANCEL));
	assert_irq_locked(irq);

	thr = irq->waiter;
	assert(thr != NULL);
	irq->waiter = NULL;

	/* wake waiting thread */
	assert(thr->state == THREAD_STATE_WAIT_IRQ);

	sched_lock(thr->sched);
	thread_wakeup(thr);
	thr->wakeup_code = wakeup_code;
	thread_cancel_clear(thr);
	sched_unlock(thr->sched);
}

/** handle an IRQ */
void irq_handle(struct irq *irq)
{
	struct fd *fd;
	int waiting;

	assert(irq != NULL);

	/* wake waiting thread */
	irq_lock(irq);

	irq->enabled = false;
	irq->fired = true;
	irq->count++;

	waiting = 0;
	if (irq->waiter != NULL) {
		waiting = 1;
		irq_wake(irq, EOK);
	}

	fd = irq->fd;

	irq_unlock(irq);

	if (!waiting && (fd != NULL)) {
		fd_lock(fd);
		epoll_notify(fd, EPOLLIN);
		fd_unlock(fd);
	}
}

/** configure interrupt mode and enable interrupt */
static err_t irq_attach(struct irq *irq, struct fd *fd, unsigned int irq_mode)
{
	err_t err;

	assert(irq != NULL);
	assert(fd != NULL);

	irq_lock(irq);

	if (irq->fd != NULL) {
		/* IRQ already enabled */
		err = ESTATE;
		goto out;
	}

	err = bsp_irq_config(irq->irq_id, irq_mode);
	if (err != EOK) {
		/* IRQ mode not available */
		goto out;
	}

	/* mark IRQ as enabled */
	irq->fd = fd;
	assert(irq->waiter == NULL);
	assert(irq->enabled == false);
	irq->ctl_enabled = false;
	irq->fired = false;
	err = EOK;

out:
	irq_unlock(irq);

	return err;
}

/** detach specific thread from interrupt */
static void irq_detach(struct irq *irq, struct fd *fd)
{
	assert(irq != NULL);
	assert(fd != NULL);

	irq_lock(irq);

	if (irq->fd == fd) {
		/* IRQ was used by the specific fd */
		irq->fd = NULL;

		/* disable IRQ in interrupt controller and wake waiter */
		if (irq->enabled) {
			irq->enabled = false;
			bsp_irq_disable(irq->irq_id, irq->cpu_id);
		}
		if (irq->waiter != NULL) {
			irq_wake(irq, ECANCEL);
		}
	}

	irq_unlock(irq);
}

/** kill interrupt at partition shutdown */
void irq_shutdown(struct irq *irq)
{
	struct fd *fd;

	assert(irq != NULL);

	irq_lock(irq);

	fd = irq->fd;
	if (fd != NULL) {
		/* IRQ was used; unlink from fd (might be outside of partition) */
		irq->fd = NULL;

		/* disable IRQ in interrupt controller and wake waiter */
		if (irq->enabled) {
			irq->enabled = false;
			bsp_irq_disable(irq->irq_id, irq->cpu_id);
		}
		if (irq->waiter != NULL) {
			irq_wake(irq, ECANCEL);
		}
	}

	irq_unlock(irq);
}

static err_t irq_ctl(struct irq *irq, struct fd *fd, ulong_t op)
{
	int epoll_clear;
	err_t err;

	epoll_clear = false;

	irq_lock(irq);

	if (irq->fd != fd) {
		/* IRQ not enabled */
		err = ESTATE;
		goto out;
	}

	switch (op) {
	case IRQ_CTL_UNMASK:
		irq->ctl_enabled = true;
		if (!irq->enabled) {
			irq->enabled = true;
			irq->cpu_id = current_cpu_id();
			bsp_irq_enable(irq->irq_id, irq->cpu_id);
		}
		err = EOK;
		break;

	case IRQ_CTL_MASK:
		irq->ctl_enabled = false;
		if (irq->enabled) {
			irq->enabled = false;
			bsp_irq_disable(irq->irq_id, irq->cpu_id);
		}
		err = EOK;
		break;

	case IRQ_CTL_CLEAR:
		if (irq->fired) {
			irq->fired = false;
			epoll_clear = true;

			if (irq->ctl_enabled && !irq->enabled) {
				irq->enabled = true;
				irq->cpu_id = current_cpu_id();
				bsp_irq_enable(irq->irq_id, irq->cpu_id);
			}
		}
		err = EOK;
		break;

	default:
		err = EINVAL;
		break;
	}

out:
	irq_unlock(irq);

	if (epoll_clear) {
		epoll_notify(fd, 0);
	}

	return err;
}

////////////////////////////////////////////////////////////////////////////////

err_t sys_irq_iter_id(ulong_t id, unsigned int *id_user)
{
	const struct irq_cfg *cfg;

	if (id >= current_part()->part_cfg->irq_num) {
		/* out of bounds */
		return ELIMIT;
	}
	cfg = &current_part()->part_cfg->irq_cfg[id];

	return user_put_4(id_user, cfg->irq_id);
}

err_t sys_irq_iter_name(ulong_t id, char *name_user, size_t size_user)
{
	const struct irq_cfg *cfg;
	size_t size;

	if (id >= current_part()->part_cfg->irq_num) {
		/* out of bounds */
		return ELIMIT;
	}
	cfg = &current_part()->part_cfg->irq_cfg[id];

	size = strlen(cfg->name) + 1;
	if (size > size_user) {
		return ELIMIT;
	}

	return user_copy_out(name_user, cfg->name, size);
}

err_t sys_irq_attach(ulong_t irq_id, ulong_t irq_mode, ulong_t flags, uint32_t *fd_index_user)
{
	uint32_t fd_index;
	struct irq *irq;
	struct fd *fd;
	int nonblock;
	int cloexec;
	err_t err;

	if (irq_id >= irq_table_num) {
		/* IRQ out of bounds */
		return ELIMIT;
	}
	if ((flags & ~(SYS_FD_NONBLOCK | SYS_FD_CLOEXEC)) != 0) {
		/* invalid flags */
		return EINVAL;
	}
	if (fd_index_user == NULL) {
		/* invalid pointer */
		return EINVAL;
	}
	if (irq_mode > IRQ_MODE_LEVEL_HIGH) {
		/* invalid mode (IRQ_MODE_LEVEL_HIGH is the last valid mode) */
		return EINVAL;
	}
	if (irq_mode < IRQ_MODE_EDGE_DEFAULT) {
		irq_mode = IRQ_MODE_DEFAULT;
	}

	irq = irq_by_id(irq_id);
	if ((irq == NULL) || (irq->part != current_part())) {
		/* IRQ not available to partition */
		return ELIMIT;
	}

	fd = fd_alloc(current_process());
	if (fd == NULL) {
		/* failed to allocate a new file descriptor */
		return ENFILE;
	}

	/* further initialization */
	fd->type = FD_TYPE_IRQFD;
	fd->cached_eventmask = 0;
	fd->flags = FD_FLAG_SHAREABLE;
	nonblock = (flags & SYS_FD_NONBLOCK) != 0;
	fd->oflags = O_RDWR | (nonblock ? O_NONBLOCK : 0);
	fd->oflags_mask = O_NONBLOCK;
	fd->irqfd.irq = irq;

	/* try to enable IRQ */
	err = irq_attach(irq, fd, irq_mode);
	if (err != EOK) {
		goto out_free_fd;
	}

	cloexec = (flags & SYS_FD_CLOEXEC) != 0;
	err = fd_dup(current_process(), fd, cloexec, &fd_index);
	if (err != EOK) {
		goto out_irq_detach;
	}

	err = user_put_4(fd_index_user, fd_index);
	/* NOTE: any failure here is ignored and resources are not freed
	 * (the file descriptor can be accessed by user space nevertheless)
	 */

	return err;

out_irq_detach:
	irq_detach(irq, fd);

out_free_fd:
	/* free fd */
	fd->type = FD_TYPE_DEAD;
	fd_close(fd);
	return err;
}

err_t sys_irq_ctl(ulong_t fd_index, ulong_t op, ulong_t flags)
{
	struct irq *irq;
	struct fd *fd;
	err_t err;

	if (flags != 0) {
		/* invalid flags -- must be zero */
		return EINVAL;
	}

	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_IRQFD, &fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	if ((fd->oflags & O_NONBLOCK) == 0) {
		/* fd is not in non-blocking mode */
		err = EAGAIN;
		goto out;
	}

	irq = fd->irqfd.irq;
	if (irq == NULL) {
		/* IRQ no longer linked */
		err = ESTATE;
		goto out;
	}

	err = irq_ctl(irq, fd, op);

out:
	fd_unlock(fd);

	return err;
}

/** close callback for irqfd */
void irqfd_close(
	struct fd *fd)
{
	struct irq *irq;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_IRQFD);
	assert_fd_locked(fd);

	irq = fd->irqfd.irq;
	if (irq == NULL) {
		/* IRQ already disconnected */
		return;
	}

	fd->irqfd.irq = NULL;
	//fd_unlock(fd);

	irq_lock(irq);
	if (irq->fd == fd) {
		/* IRQ was used by the specific fd */
		irq->fd = NULL;

		/* disable IRQ in interrupt controller and wake waiter */
		if (irq->enabled) {
			irq->enabled = false;
			bsp_irq_disable(irq->irq_id, irq->cpu_id);
		}
		if (irq->waiter != NULL) {
			irq_wake(irq, ECANCEL);
		}
	}
	irq_unlock(irq);

	//fd_lock(fd);
}

////////////////////////////////////////////////////////////////////////////////

/** handle peculiarities of blocking <-> non-blocking changes */
static err_t irqfd_ipc_io_setoflags(struct fd *fd, struct thread *thr)
{
	struct irq *irq;

	fd->oflags &= ~O_NONBLOCK;
	fd->oflags |= thr->ipc.msg.h32 & O_NONBLOCK;

	irq = fd->irqfd.irq;
	if (irq != NULL) {
		irq_lock(irq);

		/* disable IRQ in interrupt controller and wake waiter */
		if (irq->enabled) {
			irq->enabled = false;
			bsp_irq_disable(irq->irq_id, irq->cpu_id);
		}
		if (irq->waiter != NULL) {
			irq_wake(irq, ECANCEL);
		}
		irq->ctl_enabled = false;

		irq_unlock(irq);
	}

	thr->ipc.msg.err = EOK;
	thr->ipc.msg.h32 = fd->oflags;
	return EOK;
}

static err_t irqfd_ipc_io_read(struct fd *fd, struct thread *thr)
{
	uint64_t irq_count;
	struct irq *irq;
	err_t err;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_IRQFD);
	assert(thr == current_thread());

	if ((thr->ipc.msg.flags & SYS_IPC_BUF0W) == 0) {
		/* invalid message mode */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	/* clear flags for reply, flags no longer needed */
	thr->ipc.msg.flags = 0;

	if (thr->ipc.msg.h32 != 0) {
		/* reserved flags */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	if (thr->ipc.msg.h8 != SEEK_CUR) {
		/* pread() not supported */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}

	if (thr->ipc.msg.size0 < sizeof(irq_count)) {
		/* receive buffer too small */
		thr->ipc.msg.err = EINVAL;
		return EOK;
	}
	thr->ipc.msg.size0 = 0;

	irq = fd->irqfd.irq;
	if (irq == NULL) {
		/* IRQ no longer attached */
		thr->ipc.msg.err = ESTATE;
		return EOK;
	}

	irq_lock(irq);
	if (irq->fd != fd) {
		/* IRQ no longer attached */
		irq_unlock(irq);
		thr->ipc.msg.err = ESTATE;
		return EOK;
	}

	irq_count = 0;
	if (irq->fired) {
		/* IRQ already expired in the past */
		irq->fired = false;
		irq_count = 1;

		if (irq->ctl_enabled && !irq->enabled) {
			irq->enabled = true;
			irq->cpu_id = current_cpu_id();
			bsp_irq_enable(irq->irq_id, irq->cpu_id);
		}

		thr->ipc.msg.err = EOK;
		err = EOK;
	} else if ((fd->oflags & O_NONBLOCK) != 0) {
		/* non-blocking read */
		thr->ipc.msg.err = EAGAIN;
		err = EOK;
	} else if (irq->waiter != NULL) {
		/* waiting, but other thread already waiting */
		thr->ipc.msg.err = EAGAIN;
		err = EOK;
	} else {
		/* actual waiting */
		thr->cont_args.irq.irq = irq;
		irq->waiter = thr;

		sched_lock(thr->sched);
		thread_wait(thr, THREAD_STATE_WAIT_IRQ, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
		thread_continue_at(thr, irqfd_wait_finish);
		thr->wakeup_code = EINTR;
		thread_cancel_at(thr, irqfd_wait_cancel);
		sched_unlock(thr->sched);

		if (!irq->enabled) {
			irq->enabled = true;
			irq->cpu_id = thr->sched->cpu;
			bsp_irq_enable(irq->irq_id, irq->cpu_id);
		}

		thr->ipc.msg.err = EINTR;
		err = EINTR;
	}

	irq_unlock(irq);

	if (irq_count != 0) {
		/* reading successfully also clears and pending epoll notifications */
		epoll_notify(fd, 0);

		assert(err == EOK);
		err = fd_ipc_copy_out(thr, &irq_count, sizeof(irq_count));
		thr->ipc.msg.err = err;
		return EOK;
	}

	assert((err == EINTR) || (err == EOK));
	return err;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void irqfd_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	struct irq *irq;

	assert(thr != NULL);
	irq = thr->cont_args.irq.irq;
	assert(irq != NULL);

	irq_lock(irq);
	if (irq->waiter == thr) {
		assert(irq->fd != NULL);

		thr->wakeup_code = wakeup_code;

		/* disable IRQ in interrupt controller, but do not wake waiter */
		irq->waiter = NULL;
		assert(irq->enabled);
		irq->enabled = false;
		bsp_irq_disable(irq->irq_id, irq->cpu_id);
	}
	irq_unlock(irq);
}

static void irqfd_wait_finish(struct thread *thr)
{
	uint64_t value;
	struct sys_ipc *recv_msg_user;
	err_t err;

	err = thr->wakeup_code;
	if (err != EOK) {
		assert(err == EINTR);
	}

	if (err == EOK) {
		/* copy out read() data */
		value = 1;
		err = fd_ipc_copy_out(thr, &value, sizeof(value));
		thr->ipc.msg.err = err;
		err = EOK;
	}

	/* copy out the IPC message in any case */
	recv_msg_user = thr->cont_args.irq.recv_msg_user;
	assert(recv_msg_user != NULL);
	err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));

	arch_syscall_retval(thr->regs, err);
}

/* NOTE: irqfd has no support for write(), only for read() */
static err_t (* const irqfd_ipc_io[])(struct fd *fd, struct thread *thr) = {
	fd_ipc_io_getoflags,
	irqfd_ipc_io_setoflags,
	fd_ipc_io_default_err, /* d_open, */
	fd_ipc_io_stat_dummy,
	irqfd_ipc_io_read,
	fd_ipc_io_default_err, /* sentinel */
};

/** dispatcher for IPC-based I/O requests */
err_t irqfd_ipc_call(struct fd *fd)
{
	struct sys_ipc *recv_msg_user;
	struct thread *thr;
	size_t req;
	err_t err;

	assert(fd != NULL);
	assert(fd->type == FD_TYPE_IRQFD);
	assert_fd_locked(fd);

	thr = current_thread();

	req = thr->ipc.msg.req;
	if (req > countof(irqfd_ipc_io) - 1) {
		req = countof(irqfd_ipc_io) - 1;
	}

	err = irqfd_ipc_io[req](fd, thr);

	fd_unlock(fd);

	/* copy out message (no blocking in this cases) */
	if (err == EOK) {
		recv_msg_user = thr->cont_args.irq.recv_msg_user;
		assert(recv_msg_user != NULL);
		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
	}

	return err;
}
