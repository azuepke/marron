/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * process.c
 *
 * Processes.
 *
 * azuepke, 2025-02-22: initial
 */

#include <kernel.h>
#include <assert.h>
#include <process.h>
#include <part.h>
#include <thread.h>
#include <adspace_types.h>
#include <elf.h>
#include <vma.h>
#include <marron/macros.h>
#include <adspace.h>
#include <panic.h>
#include <timer.h>
#include <sig.h>
#include <fd.h>
#include <sys_proto.h>
#include <string.h>
#include <uaccess.h>


/* forward declaraion */
static void sys_process_exit_deferred(struct thread *thr);

/** initialize all process objects at boot time  */
__init void process_init_all(void)
{
	struct process *proc;

	for (unsigned int i = 0; i < process_num; i++) {
		proc = &process_dyn[i];

		/* proc->part set later in part_init_all() */
		proc->adspace = &adspace_dyn[i];
		spin_init(&proc->lock);
		/* proc->proc_id set later in process_make_active() */
		proc->state = PROCESS_STATE_DEAD;

		/* signal queueing */
		list_head_init(&proc->sig.pending.sigqh);

		/* signalfd */
		list_head_init(&proc->sig.signalfdqh);

		/* empty futex wait queue */
		list_head_init(&proc->futex_waitqh);

		/* timers */
		list_head_init(&proc->timer_activeqh);

		/* clear thread table */
		list_head_init(&proc->thread_activeqh);
		for (unsigned int j = 0; j < PROCESS_THREAD_NUM; j++) {
			proc->thread[j] = NULL;
		}

		/* clear fd_table */
		for (unsigned int j = 0; j < PROCESS_FD_NUM; j++) {
			proc->fd_table[j] = NULL;
		}

		/* clear bitmaps */
		for (unsigned int j = 0; j < FD_BITMAP_WORDS(PROCESS_FD_NUM); j++) {
			proc->fd_table_used_bitmap[j] = 0;
			proc->fd_table_cloexec_bitmap[j] = 0;
		}
	}
}

/** re-initialize process */
/* NOTE: except for the signal information, all data must be like in the initial state */
static inline void process_init(struct process *proc)
{
	assert(proc->state == PROCESS_STATE_DEAD);

	proc->name[0] = '\0';

	assert(list_is_empty(&proc->sig.pending.sigqh));
	proc->sig.pending.unqueued_pending_mask = 0;
	proc->sig.pending.cached_pending_mask = 0;

	/* clear all signal handlers */
	for (unsigned int sig = 0; sig < NUM_SIGS; sig++) {
		proc->sig.handler[sig] = 0;
		proc->sig.blocked_mask[sig] = SIG_MASK_NONE;
		proc->sig.flags[sig] = 0;
	}

	assert(list_is_empty(&proc->sig.pending.sigqh));
	assert(list_is_empty(&proc->sig.signalfdqh));
	assert(list_is_empty(&proc->futex_waitqh));
	assert(list_is_empty(&proc->timer_activeqh));
	assert(list_is_empty(&proc->thread_activeqh));

	for (unsigned int j = 0; j < PROCESS_THREAD_NUM; j++) {
		assert(proc->thread[j] == NULL);
	}

	for (unsigned int j = 0; j < PROCESS_FD_NUM; j++) {
		assert(proc->fd_table[j] == NULL);
	}

	for (unsigned int j = 0; j < FD_BITMAP_WORDS(PROCESS_FD_NUM); j++) {
		assert(proc->fd_table_used_bitmap[j] == 0);
		assert(proc->fd_table_cloexec_bitmap[j] == 0);
	}
}

/** allocate a process (locks part_lock internally) */
static struct process *process_alloc(struct part *part)
{
	struct process *proc;
	list_t *node;

	assert(part != NULL);

	part_lock(part);

	node = list_remove_first(&part->process_freeqh);
	if (node != NULL) {
		proc = PROCESS_FROM_FREEQ(node);
		assert(proc->part == part);
	} else {
		proc = NULL;
	}

	part_unlock(part);

	if (proc != NULL) {
		process_init(proc);
	}

	return proc;
}

/** free a process (locks part_lock internally) */
static inline void process_free(struct part *part, struct process *proc)
{
	assert(part != NULL);
	assert(proc != NULL);

	part_lock(part);

	list_node_init(&proc->process_freeql);
	list_insert_last(&part->process_freeqh, &proc->process_freeql);

	part_unlock(part);
}

/** make process active and visible; also assigns final proc_id (locks part_lock) */
static void process_make_active(struct part *part, struct process *proc)
{
	ulong_t proc_id;

	assert(part != NULL);
	assert(proc != NULL);
	assert(proc->proc_id < part->part_cfg->process_num);

	part_lock(part);

	/* find an unused proc_id slot */
	for (proc_id = 0; proc_id < part->part_cfg->process_num; proc_id++) {
		if (part->part_cfg->proc_table[proc_id] == NULL) {
			break;
		}
	}
	assert(proc_id < part->part_cfg->process_num);

	proc->proc_id = proc_id;
	assert(part->part_cfg->proc_table[proc_id] == NULL);
	part->part_cfg->proc_table[proc_id] = proc;

	list_node_init(&proc->process_activeql);
	list_insert_last(&part->process_activeqh, &proc->process_activeql);

	part_unlock(part);
}

/** make process inactive (locks part_lock) */
static void process_make_inactive(struct part *part, struct process *proc)
{
	assert(part != NULL);
	assert(proc != NULL);
	assert(proc->proc_id < part->part_cfg->process_num);

	part_lock(part);

	assert(part->part_cfg->proc_table[proc->proc_id] == proc);
	part->part_cfg->proc_table[proc->proc_id] = NULL;
	list_remove(&proc->process_activeql);

	part_unlock(part);
}

/** lookup process in partition (locks part_lock) */
struct process *process_lookup(struct part *part, ulong_t proc_id)
{
	struct process *proc;

	assert(part != NULL);

	part_lock(part);

	proc = NULL;
	if (proc_id < part->part_cfg->process_num) {
		proc = part->part_cfg->proc_table[proc_id];
	}

	part_unlock(part);

	return proc;
}

/** create idle process at boot time */
__init struct process *process_create_idle(struct part *part)
{
	struct process *proc;

	proc = process_alloc(part);
	assert(proc == &process_dyn[0]);
	process_make_active(part, proc);

	return proc;
}


/** create a new process (called unlocked) */
struct process *process_create(struct part *part)
{
	struct process *proc;
	err_t err;

	proc = process_alloc(part);
	if (proc == NULL) {
		goto out_no_proc;
	}
	proc->critical_process = 0;

	err = arch_adspace_init(proc->adspace);
	if (err != EOK) {
		goto out_free_proc;
	}
	vma_adspace_init(proc->adspace);

#ifndef NDEBUG
	if (part->part_id == 1) {
		vma_test_run_once(proc->adspace);
	}
#endif

	process_make_active(part, proc);

	return proc;

out_free_proc:
	process_free(part, proc);

out_no_proc:
	return NULL;
}

/** setup process in the context of the first thread */
static void process_start_first_run(
	struct thread *thr)
{
	const struct process_cfg *proc_cfg;
	const struct part_cfg *cfg;
	struct process *proc;
	addr_t entry, stack, tls;
	size_t name_len;

	assert(thr == current_thread());

	/* setup the partition adspace */
	cfg = thr->part->part_cfg;
	proc_cfg = thr->cont_args.ptr;
	proc = thr->process;

	/* process name */
	name_len = strlen(proc_cfg->name);
	if (name_len > sizeof(proc->name) - 1) {
		name_len = sizeof(proc->name) - 1;
	}
	for (size_t i = 0; i < name_len; i++) {
		proc->name[i] = proc_cfg->name[i];
	}
	proc->name[name_len] = '\0';

	/* allocate + map + load ELF file to memory */
	// FIXME: instead of panic, shutdown partition on failure
	entry = elf_load(proc, cfg->elf_memrq, proc_cfg->elf_image);
	elf_heap(proc, cfg->heap_memrq, proc_cfg->heap_base, proc_cfg->heap_size);
	elf_stack(proc, cfg->stack_memrq, proc_cfg->stack_base, proc_cfg->stack_size);
	vma_adspace_set_hint(proc->adspace, proc_cfg->anon_mmap_base);

	/* NOTE: assumes that stack grows down */
	stack = ALIGN_DOWN(proc_cfg->stack_base + proc_cfg->stack_size - 1, ARCH_ELF_STKALN);

	/* NOTE: no TLS here -- the thread will set up its TLS via sys_tls_set() */
	tls = 0;

	/* setup user regs: we pass start and size of heap and stack as arguments */
	arch_reg_init(thr->regs, entry, stack, tls,
	              proc_cfg->heap_base, proc_cfg->heap_size,
	              proc_cfg->stack_base, proc_cfg->stack_size,
	              proc_cfg->anon_mmap_base);

	/* re-load life registers like TLS */
	arch_reg_restore(thr->regs);
}

/** start first thread in new process with config (called unlocked) */
struct thread *process_create_first_thread(struct process *proc, const struct process_cfg *proc_cfg)
{
	struct thread *thr;

	/* start first thread at the partition entry point */
	thr = thread_alloc(proc->part);
	if (thr == NULL) {
		return NULL;
	}

	thread_start_kern(proc, thr,
	                  process_start_first_run, (void*)(addr_t)proc_cfg,
	                  proc_cfg->max_prio, 0 /* infinite quantum */, proc_cfg->cpu_mask);

	return thr;
}

/** kill a process and all its threads */
void process_destroy(struct process *proc)
{
	struct part *part;
	list_t *node;

	assert(proc != NULL);
	part = proc->part;
	assert(part != NULL);
	assert(part->part_id != 0);

	process_lock(proc);
#if 0
	// FIXME: XXX: check process state
	if (process->state == ...) {

	}
#endif

	/* stop all timers */
	/* NOTE: do this before killing threads and draining any signals,
	 * as a timer could fire and insert a signal again
	 */
	while ((node = list_first(&proc->timer_activeqh))) {
		struct timer *timer = TIMER_FROM_TIMER_ACTIVEQ(node);

		timer_disable(timer);
	}

	/* kill all threads */
	while ((node = list_first(&proc->thread_activeqh))) {
		struct thread *thr = THR_FROM_ACTIVEQ(node);

		thread_kill(thr);
	}

	process_unlock(proc);

	/* drain the process' signals */
	sig_drain(proc, &proc->sig.pending);

	/* close all open file descriptors from the file descriptor table */
	for (unsigned int i = 0; i < PROCESS_FD_NUM; i++) {
		fd_table_close(proc, i);
	}
	/* NOTE: we cannot reclaim file descriptors shared with other processes */

	part_lock(part);

	/* finally, cleanup the adspace */
	vma_adspace_destroy(proc->adspace);
	arch_adspace_destroy(proc->adspace);

	part_unlock(part);

	// FIXME: XXX: move process_make_inactive() earlier
	process_make_inactive(part, proc);
	process_free(part, proc);
}

/** raise a process error (always to currently running process) */
void process_error(struct process *proc, unsigned int sig)
{
	assert(proc != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));

	// FIXME: debug output to help debugging
	kernel_print_current();
	printk("partition %s process %u signal %u, process halted\n",
	       proc->part->part_cfg->name, proc->proc_id, sig);

	process_destroy(proc);

	/* escalate to partition error if necessary */
	if (proc->critical_process) {
		part_error(proc->part, sig);
	}
}

////////////////////////////////////////////////////////////////////////////////

uint32_t sys_process_self(void)
{
	return current_process()->proc_id;
}

err_t sys_process_name(char *name_user, size_t size_user)
{
	const char *process_name;
	size_t size;

	process_name = current_process()->part->part_cfg->name;
	size = strlen(process_name) + 1;
	if (size > size_user) {
		return ELIMIT;
	}

	return user_copy_out(name_user, process_name, size);
}

err_t sys_process_exit(ulong_t status)
{
	struct thread *thr;

	// FIXME: XXX: ignored for now
	(void)status;

	/* defer to not mess up any arguments when the thread is restarted */
	thr = current_thread();
	thr->cont_args.proc = thr->process;
	thread_defer_to(thr, sys_process_exit_deferred);

	return EOK;
}

static void sys_process_exit_deferred(struct thread *thr)
{
	process_destroy(thr->cont_args.proc);
}
