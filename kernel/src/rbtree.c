/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * rbtree.c
 *
 * Instance of rbtree.
 *
 * azuepke, 2021-09-22: initial
 */

/* NOTE: this instantiates the code modules inside rbtree.h */
#define RBTREE_HEADER_INSTANTIATE
#include <marron/rbtree.h>
