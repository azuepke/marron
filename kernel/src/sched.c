/* SPDX-License-Identifier: MIT */
/* Copyright 2017-2021 Alexander Zuepke */
/*
 * sched.c
 *
 * Scheduler
 *
 * azuepke, 2017-12-31: initial
 */

#include <sched.h>
#include <thread.h>
#include <timer.h>
#include <kernel.h>
#include <bsp.h>
#include <marron/bit.h>
#include <time.h>


/** initialize per-CPU scheduling data, assuming only the idle thread is active */
__init void sched_init(struct sched *sched, struct thread *idle, unsigned int cpu)
{
	unsigned int i;

	assert(sched != NULL);
	assert(idle != NULL);
	assert(idle->state == THREAD_STATE_CURRENT);
	assert(idle->prio == -1);

	spin_init(&sched->lock);
	sched->current = idle;
	sched->quantum_expiry = TIMEOUT_INFINITE;
	sched->idle = idle;
	sched->cpu = cpu & 0xff;
	sched->reschedule = 0;

	sched->next_highest_prio = -1;

	for (i = 0; i < countof(sched->readyqh); i++) {
		list_head_init(&sched->readyqh[i]);
	}
	for (i = 0; i < countof(sched->tracking_bitmap); i++) {
		sched->tracking_bitmap[i] = 0;
	}

	for (i = 0; i < countof(sched->timeoutqh); i++) {
		list_head_init(&sched->timeoutqh[i]);
	}

	for (i = 0; i < countof(sched->timerqh); i++) {
		list_head_init(&sched->timerqh[i]);
	}

	bsp_timer_set_expiry(TIMEOUT_INFINITE, cpu);
}

/** reprogram BSP timer when the timeout queue changes */
static void sched_reprogram_timer_expiry(struct sched *sched)
{
	sys_time_t earliest, earliest_monotonic, earliest_realtime;
	struct timer *timer;
	struct thread *thr;
	list_t *node;

	/* reprogram timer to earliest expiry */
	earliest_monotonic = sched->quantum_expiry;

	node = list_first(&sched->timeoutqh[CLK_ID_MONOTONIC]);
	if (node != NULL) {
		thr = THR_FROM_TIMEOUTQ(node);
		assert(thr->state > THREAD_STATE_READY);
		assert(thr->timeout != TIMEOUT_INFINITE);
		if (thr->timeout < earliest_monotonic) {
			earliest_monotonic = thr->timeout;
		}
	}

	node = list_first(&sched->timerqh[CLK_ID_MONOTONIC]);
	if (node != NULL) {
		timer = TIMER_FROM_TIMERQ(node);
		assert(timer->next != TIMEOUT_NULL);
		if (timer->next < earliest_monotonic) {
			earliest_monotonic = timer->next;
		}
	}

	earliest_realtime = TIMEOUT_INFINITE;

	node = list_first(&sched->timeoutqh[CLK_ID_REALTIME]);
	if (node != NULL) {
		thr = THR_FROM_TIMEOUTQ(node);
		assert(thr->state > THREAD_STATE_READY);
		assert(thr->timeout != TIMEOUT_INFINITE);
		assert(earliest_realtime == TIMEOUT_INFINITE);
		earliest_realtime = thr->timeout;
	}

	node = list_first(&sched->timerqh[CLK_ID_REALTIME]);
	if (node != NULL) {
		timer = TIMER_FROM_TIMERQ(node);
		assert(timer->next != TIMEOUT_NULL);
		if (timer->next < earliest_realtime) {
			earliest_realtime = timer->next;
		}
	}

	assert(clock_realtime_offset[CLK_ID_MONOTONIC] == 0);
	earliest = time_sub(earliest_realtime, arch_load64(&clock_realtime_offset[CLK_ID_REALTIME]));
	if (earliest_monotonic < earliest) {
		earliest = earliest_monotonic;
	}

	bsp_timer_set_expiry(earliest, sched->cpu);
}

/** update priority tracking bitmap after a thread was added */
static void sched_tracking_bitmap_add(struct sched *sched, int prio)
{
	int word;
	int bit;

	assert(sched != NULL);
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	word = prio / SCHED_TRACKING_BITMAP_BITS_PER_WORD;
	bit = prio % SCHED_TRACKING_BITMAP_BITS_PER_WORD;
	sched->tracking_bitmap[word] |= (1U << bit);

	if (prio > sched->next_highest_prio) {
		sched->next_highest_prio = prio;
		/* change of the next_highest priority:
		 * trigger rescheduling when we leave the kernel,
		 * this also enforces an update the shadow value in user space
		 */
		sched->reschedule = 1;
	}
}

/** update priority tracking bitmap after a thread was removed */
static void sched_tracking_bitmap_remove(struct sched *sched, int prio)
{
	int word;
	int bit;

	assert(sched != NULL);
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	word = prio / SCHED_TRACKING_BITMAP_BITS_PER_WORD;
	bit = prio % SCHED_TRACKING_BITMAP_BITS_PER_WORD;
	sched->tracking_bitmap[word] &= ~(1U << bit);

	if (prio == sched->next_highest_prio) {
		/* need to find the next-highest priority */

		while ((word >= 0) && (sched->tracking_bitmap[word] == 0)) {
			word--;
		}
		if (word < 0) {
			sched->next_highest_prio = -1;
			return;
		}
		assert(sched->tracking_bitmap[word] != 0);
		bit = bit_fhs(sched->tracking_bitmap[word]);
		sched->next_highest_prio = word * SCHED_TRACKING_BITMAP_BITS_PER_WORD + bit;
	}
}

/** enqueue a thread at the head of its ready queue */
static void sched_readyq_enqueue_head(struct sched *sched, struct thread *thr)
{
	int prio;

	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_READY);

	if (thr == sched->idle) {
		return;
	}

	prio = thr->prio;
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	list_node_init(&thr->readyql);
	list_insert_first(&sched->readyqh[prio], &thr->readyql);
	sched_tracking_bitmap_add(sched, prio);
}

/** enqueue a thread at the tail of its ready queue */
static void sched_readyq_enqueue_tail(struct sched *sched, struct thread *thr)
{
	int prio;

	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_READY);

	if (thr == sched->idle) {
		return;
	}

	prio = thr->prio;
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	list_node_init(&thr->readyql);
	list_insert_last(&sched->readyqh[prio], &thr->readyql);
	sched_tracking_bitmap_add(sched, prio);
}

/** remove a thread from its ready queue */
static void sched_readyq_remove(struct sched *sched, struct thread *thr)
{
	int prio;

	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state >= THREAD_STATE_READY);
	assert(thr != sched->idle);

	prio = thr->prio;
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);
	assert(prio <= sched->next_highest_prio);

	list_remove(&thr->readyql);
	if (list_is_empty(&sched->readyqh[prio])) {
		sched_tracking_bitmap_remove(sched, prio);
	}
}

/** find and remove highest thread on ready queue */
void sched_readyq_next(struct sched *sched)
{
	struct thread *thr;
	list_t *node;
	int prio;

	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(sched->current != THREAD_STATE_CURRENT);

	prio = sched->next_highest_prio;
	if (prio < 0) {
		assert(prio == -1);
		thr = sched->idle;
		goto set_next;
	}

	node = list_remove_first_nonempty(&sched->readyqh[prio]);
	assert(node != NULL);
	thr = THR_FROM_READYQ(node);

	if (list_is_empty(&sched->readyqh[prio])) {
		sched_tracking_bitmap_remove(sched, prio);
	}

set_next:
	assert(thr->state == THREAD_STATE_READY);
	thr->state = THREAD_STATE_CURRENT;
	sched->current = thr;
	sched->reschedule = 0;
}


/** replenish scheduling quantum for thread */
void sched_replenish_quantum(sys_time_t now, struct sched *sched)
{
	struct thread *thr;

	assert(sched != NULL);
	assert_sched_locked(sched);

	thr = sched->current;
	assert(thr->state == THREAD_STATE_CURRENT);

	if (thr->quantum_remain == 0) {
		/* replenish */
		thr->quantum_remain = thr->quantum;
	}

	/* quantum expiry */
	if (thr->quantum_remain > 0) {
		sched->quantum_expiry = now + thr->quantum_remain;
	} else {
		sched->quantum_expiry = TIMEOUT_INFINITE;
	}
}

/** preempt or yield the current thread */
void sched_preempt_or_yield(sys_time_t now, struct sched *sched)
{
	struct thread *thr;

	assert(sched != NULL);
	assert_sched_locked(sched);

	thr = sched->current;
	thr->stat_exec_time += now - sched->stat_last_switch;
	sched->stat_last_switch = now;
	if (thr->state == THREAD_STATE_CURRENT) {
		thr->state = THREAD_STATE_READY;

		if (now < sched->quantum_expiry) {
			if (sched->quantum_expiry != TIMEOUT_INFINITE) {
				thr->quantum_remain = now - sched->quantum_expiry;
			} else {
				thr->quantum_remain = 0;
			}
			sched_readyq_enqueue_head(sched, thr);
		} else {
			thr->quantum_remain = 0;
			sched_readyq_enqueue_tail(sched, thr);
		}
	}
}

/** check if we need to preempt the current thread */
void sched_check_preempt(struct sched *sched)
{
	assert(sched != NULL);

	if (sched->current->prio < sched->next_highest_prio) {
		sched->reschedule = 1;
	}
}

/** current thread waits, with state, timeout and error code already set */
void sched_wait(struct sched *sched, struct thread *thr)
{
	list_t *head;

	/* The current thread is not kept on the ready queue,
	 * so we just need to enqueue it on the timeout queue.
	 * We assume that both new state and timeout value are already set.
	 */
	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(thr != NULL);
	assert(thr != sched->idle);
	assert(thr->state > THREAD_STATE_READY);

	if (thr->timeout != TIMEOUT_INFINITE) {
		/* Thread has a timeout: enqueue in sorted timeout queue */
		assert(thr->clk_id < NUM_CLK_IDS);
		head = &sched->timeoutqh[thr->clk_id];
		list_node_init(&thr->timeoutql);
		list_insert_ordered(head, &thr->timeoutql,
		                    THR_FROM_TIMEOUTQ(__ITER__)->timeout > thr->timeout);

		/* reprogram timer to first thread's timeout */
		sched_reprogram_timer_expiry(sched);
	}

	/* enforce scheduling */
	sched->reschedule = 1;
}

/** remove a thread from the ready queue
 * NOTE: thread_kill() calls this for threads in THREAD_STATE_READY state
 */
void sched_remove(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(thr != NULL);
	assert(thr != sched->idle);
	assert(thr->state == THREAD_STATE_DEAD);

	sched_readyq_remove(sched, thr);
}

/** release the timeout of a blocked thread
 * NOTE: thread_kill() also calls this for waiting threads
 */
void sched_timeout_release(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	if (thr->timeout != TIMEOUT_INFINITE) {
		thr->timeout = TIMEOUT_INFINITE;
		list_remove(&thr->timeoutql);

		/* reprogram timer to first thread's timeout */
		sched_reprogram_timer_expiry(sched);
	}
}

/** wake a thread and schedule */
void sched_wakeup(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	sched_timeout_release(sched, thr);

	thr->state = THREAD_STATE_READY;
	thr->quantum_remain = 0;	/* enforce replenishment */
	sched_readyq_enqueue_tail(sched, thr);
}

/** wake a thread during timeout processing */
static void sched_wakeup_timeout(struct sched *sched, struct thread *thr)
{
	void (*cancellation)(struct thread *, err_t wakeup_code);

	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);
	assert(thr->timeout != TIMEOUT_INFINITE);

	/* remove from timeout queue */
	thr->timeout = TIMEOUT_INFINITE;
	list_remove(&thr->timeoutql);

	cancellation = thr->cancellation;
	thr->cancellation = NULL;

	sched_unlock(sched);

	/* do cleanup of special waiting states (cancellation handlers) */
	if (cancellation != NULL) {
		cancellation(thr, ETIMEDOUT);
	}

	sched_lock(sched);

	/* wake up thread */
	thr->state = THREAD_STATE_READY;
	thr->quantum_remain = 0;	/* enforce replenishment */
	sched_readyq_enqueue_tail(sched, thr);
}

/** expire all currently pending timeouts and wake waiting threads */
static void sched_timeout_expire(struct sched *sched, sys_time_t now)
{
	struct thread *thr;
	sys_time_t offset;
	list_t *node;

	assert(sched != NULL);
	assert_sched_locked(sched);

	/* walk sorted timeout queue and wake all threads with expired timeout */
	for (unsigned int clk_id = 0; clk_id < NUM_CLK_IDS; clk_id++) {
		offset = arch_load64(&clock_realtime_offset[clk_id]);
		while ((node = list_first(&sched->timeoutqh[clk_id]))) {
			thr = THR_FROM_TIMEOUTQ(node);
			assert(thr->state > THREAD_STATE_READY);
			assert(thr->timeout != TIMEOUT_INFINITE);

			/* NOTE: "thr->timeout - offset" can underflow */
			if (thr->timeout > now + offset) {
				break;
			}

			/* NOTE: SMP: unlocks sched_lock internally */
			sched_wakeup_timeout(sched, thr);
		}
	}
}

/** reload pending timer and trigger timer action */
static void sched_timer_reload(struct sched *sched, struct timer *timer, sys_time_t now)
{
	uint64_t expiry_count;
	sys_time_t elapsed;
	list_t *head;

	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(timer != NULL);
	assert(timer->next != TIMEOUT_NULL);

	list_remove(&timer->timerql);

	if (timer->period != 0) {
		/* periodic timer -- set new expiry time in the future */
		assert(timer->next <= now);
		elapsed = now - timer->next;
#if __SIZEOF_LONG__ == 8
		/* native 64-bit division available */
		expiry_count = elapsed / timer->period;
#else
		/* fallback to software routine for division */
		expiry_count = udiv64_64(elapsed, timer->period);
#endif
		expiry_count += 1;
		/* NOTE: might overflow for large periods after the year 2518 */
		timer->next += expiry_count * timer->period;
		assert(timer->next > now);

		assert(timer->clk_id < NUM_CLK_IDS);
		head = &sched->timerqh[timer->clk_id];
		list_insert_ordered(head, &timer->timerql,
		                    TIMER_FROM_TIMERQ(__ITER__)->next > timer->next);
	} else {
		/* oneshot timer -- disable */
		expiry_count = 1;
		timer->next = 0;
	}

	sched_unlock(sched);

	timer_expiry_action(timer, expiry_count);

	sched_lock(sched);
}

/** expire all currently pending timers */
static void sched_timer_expire(struct sched *sched, sys_time_t now)
{
	struct timer *timer;
	sys_time_t offset;
	list_t *node;

	assert(sched != NULL);
	assert_sched_locked(sched);

	/* walk sorted timer queue and reprogram the already expired timers
	 * to expire again in the future
	 * NOTE: as we do not update "now", the loop eventually terminates
	 */
	for (unsigned int clk_id = 0; clk_id < NUM_CLK_IDS; clk_id++) {
		offset = arch_load64(&clock_realtime_offset[clk_id]);
		while ((node = list_first(&sched->timerqh[clk_id]))) {
			timer = TIMER_FROM_TIMERQ(node);
			assert(timer->next != TIMEOUT_NULL);

			/* NOTE: "timer->next - offset" can underflow */
			if (timer->next > now + offset) {
				break;
			}

			/* NOTE: SMP: unlocks sched_lock internally */
			sched_timer_reload(sched, timer, now + offset);
		}
	}
}

/** start timer */
void sched_timer_start(struct sched *sched, struct timer *timer)
{
	list_t *head;

	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(timer != NULL);
	assert(timer->sched == sched);
	assert(timer->state != TIMER_STATE_DISABLED);

	if (timer->next != 0) {
		assert(timer->clk_id < NUM_CLK_IDS);
		head = &sched->timerqh[timer->clk_id];
		list_insert_ordered(head, &timer->timerql,
		                    TIMER_FROM_TIMERQ(__ITER__)->next > timer->next);

		/* reprogram timer to first thread's timeout */
		sched_reprogram_timer_expiry(sched);
	}
}

/** stop timer */
void sched_timer_stop(struct sched *sched, struct timer *timer)
{
	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(timer != NULL);
	assert(timer->sched == sched);
	assert(timer->state != TIMER_STATE_DISABLED);

	if (timer->next != 0) {
		timer->next = 0;
		timer->period = 0;
		list_remove(&timer->timerql);

		/* reprogram timer to first thread's timeout */
		sched_reprogram_timer_expiry(sched);
	}
}

/* Scheduler timer interrupt callback, "now" refers to the current system time
 * We expire pending timeouts and timers,
 * and reprogram the expiration of the next timer interrupt.
 */
void sched_timer(struct sched *sched, uint64_t now)
{
	assert(sched != NULL);
	assert(sched == current_thread()->sched);

	sched_lock(sched);

	/* check quantum expiry */
	if (now >= sched->quantum_expiry) {
		sched->reschedule = 1;
	}

	/* NOTE: SMP: locks sched_lock internally */
	sched_timeout_expire(sched, now);

	sched_timer_expire(sched, now);

	/* reprogram next timer expiry */
	sched_reprogram_timer_expiry(sched);

	sched_unlock(sched);
}

/** yield current thread */
void sched_yield(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_CURRENT);

	thr->state = THREAD_STATE_READY;
	thr->quantum_remain = 0;	/* enforce replenishment */
	sched->reschedule = 1;
	sched_readyq_enqueue_tail(sched, thr);
}

/** change scheduling parameters of any thread */
/* NOTE: SMP: this runs with sched_lock */
void sched_prio_quantum_change(struct sched *sched, struct thread *thr, int new_prio, unsigned int quantum)
{
	unsigned int thr_state;

	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(thr != NULL);

	thr_state = thr->state;
	if (thr_state == THREAD_STATE_READY) {
		sched_readyq_remove(sched, thr);
	}

	thr->prio = new_prio;
	thr->quantum = quantum;
	thr->quantum_remain = 0;	/* enforce replenishment */

	if (thr_state == THREAD_STATE_READY) {
		sched_readyq_enqueue_tail(sched, thr);
	}

	if ((thr_state == THREAD_STATE_CURRENT) ||
	        (thr_state == THREAD_STATE_READY)) {
		sched->reschedule = 1;

#ifdef SMP
		if (sched->cpu != current_cpu_id()) {
			/* notify target CPU */
			bsp_cpu_reschedule(sched->cpu);
		}
#endif
	}
}

/** migrate current thread to a new CPU (and yield there) */
void sched_migrate(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert_sched_locked(sched);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_CURRENT);
	thr->sched = sched;

	thr->state = THREAD_STATE_READY;
	thr->quantum_remain = 0;	/* enforce replenishment */
	sched_readyq_enqueue_tail(sched, thr);

#ifdef SMP
	/* notify target CPU */
	bsp_cpu_reschedule(sched->cpu);
#endif
}
