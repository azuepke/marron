/* SPDX-License-Identifier: MIT */
/* Copyright 2017-2021, 2025 Alexander Zuepke */
/*
 * time.c
 *
 * Time and clock functions.
 *
 * azuepke, 2025-02-22: split clock and sleep functions from main.c and thread.c
 */

#include <time.h>
#include <sys_proto.h>
#include <sched.h>
#include <uaccess.h>
#include <part_types.h>
#include <thread.h>


/** offset in nanoseconds between real-time clock and monotonic clock */
/* NOTE: 64-bit value accessed without locks, use arch_load64() */
uint64_t clock_realtime_offset[NUM_CLK_IDS];


/** Callback for cleanup during wakeup (timeout, thread deletion) */
static void sleep_wait_cancel(struct thread *thr, err_t wakeup_code);

// System calls

err_t sys_clock_get(ulong_t clk_id, sys_time_t *time_user)
{
	sys_time_t val;
	err_t err;

	if (clk_id >= NUM_CLK_IDS) {
		/* invalid clock ID */
		return EINVAL;
	}

	val = bsp_timer_get_time();
	val += arch_load64(&clock_realtime_offset[clk_id]);

	err = EOK;
	if (time_user != NULL) {
		err = user_put_8(time_user, val);
	}

	return err;
}

err_t sys_clock_set(ulong_t clk_id, sys_time_t time)
{
	if ((current_part()->part_cfg->perm & PART_PERM_SET_TIME) == 0) {
		return EPERM;
	}

	if (clk_id != CLK_ID_REALTIME) {
		/* invalid clock ID to set the clock */
		return EINVAL;
	}

	arch_store64(&clock_realtime_offset[clk_id], time);
	return EOK;
}

err_t sys_clock_res(ulong_t clk_id, sys_time_t *res_user)
{
	sys_time_t val;
	err_t err;

	if (clk_id >= NUM_CLK_IDS) {
		/* invalid clock ID */
		return EINVAL;
	}

	/* both CLK_ID_MONOTONIC and CLK_ID_REALTIME have the same resolution */
	val = bsp_timer_resolution;

	err = EOK;
	if (res_user != NULL) {
		err = user_put_8(res_user, val);
	}

	return err;
}

err_t sys_sleep(sys_time_t timeout, ulong_t clk_id_absflag)
{
	struct thread *thr;

	if ((clk_id_absflag & ~TIMEOUT_ABSOLUTE) >= NUM_CLK_IDS) {
		/* invalid clock ID */
		return EINVAL;
	}

	thr = current_thread();
	sched_lock(thr->sched);
	thread_wait(thr, THREAD_STATE_WAIT_SLEEP, timeout, clk_id_absflag);
	thread_continue_at_wait_finish(thr);
	thread_cancel_at(thr, sleep_wait_cancel);
	sched_unlock(thr->sched);

	return EOK;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
static void sleep_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	/* set error code to EOK instead of ETIMEDOUT */
	if (wakeup_code == ETIMEDOUT) {
		/* target thread wakeup_code is already EOK */
		//thr->wakeup_code = EOK;
		return;
	}

	sched_lock(thr->sched);

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	sched_unlock(thr->sched);
}
