/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2025 Alexander Zuepke */
/*
 * elf.c
 *
 * ELF loader
 *
 * azuepke, 2013-04-06: initial
 * azuepke, 2021-04-27: adapted
 * azuepke, 2025-02-22: processes
 */

#include <elf.h>
#include <kernel.h>
#include <panic.h>
#include <marron/arch_defs.h>
#include <marron/elf.h>
#include <uaccess.h>
#include <marron/macros.h>
#include <bsp.h>
#include <marron/mapping.h>
#include <vm.h>
#include <process_types.h>


#undef DEBUG

/* type overrides for us */
#if __SIZEOF_LONG__ == 8
typedef struct elf64_header ehdr_t;
typedef struct elf64_phdr phdr_t;
typedef struct elf64_shdr shdr_t;
#define ELF_CLASS 2
#else
typedef struct elf32_header ehdr_t;
typedef struct elf32_phdr phdr_t;
typedef struct elf32_shdr shdr_t;
#define ELF_CLASS 1
#endif

static err_t elf_copy(
	struct process *proc,
	const struct memrq_cfg *memrq,
	addr_t base,
	const phdr_t *phdr)
{
	addr_t va_src, va_dst, va_bss, va_end;
	unsigned int prot;
	addr_t map_addr;
	err_t err;

#ifdef DEBUG
	printk("ELF PHDR %zx to %zx, sizes %zx %zx, %c%c%c\n",
	       base + phdr->p_offset, phdr->p_vaddr,
	       phdr->p_filesz, phdr->p_memsz,
	       phdr->p_flags & PF_R ? 'R' : '-',
	       phdr->p_flags & PF_W ? 'W' : '-',
	       phdr->p_flags & PF_X ? 'X' : '-');
#endif

	/* skip zero-sized segments */
	if (phdr->p_memsz == 0) {
		return EOK;
	}

	va_src = base + ALIGN_DOWN(phdr->p_offset, PAGE_SIZE);
	va_dst = ALIGN_DOWN(phdr->p_vaddr, PAGE_SIZE);
	va_bss = phdr->p_vaddr + phdr->p_filesz;
	va_end = ALIGN_UP(phdr->p_vaddr + phdr->p_memsz, PAGE_SIZE);

	/* first, we establish a mapping in user space with rwx permissions */
	prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	err = vm_alloc_map(proc->adspace, va_dst, va_end - va_dst, prot, prot, memrq,
	                   MAP_FIXED, &map_addr);
	if (err != EOK) {
#ifdef DEBUG
		printk("--- allocation of memory failed, error %u\n", err);
#endif
		return err;
	}

	/* then we copy data and clear the memory into the user space mapping */
	while (va_dst < va_end) {
		if (va_dst + PAGE_SIZE <= va_bss) {
			/* copy full page */
			fast_copy_page((void*)va_dst, (void*)va_src);
		} else if (va_dst < va_bss) {
			/* copy partial page */
			size_t copy_size = va_bss - va_dst;
			size_t zero_size = PAGE_SIZE - copy_size;

			assert(copy_size < PAGE_SIZE);
			assert(zero_size < PAGE_SIZE);
			err = arch_user_memcpy((void *)va_dst, (void *)va_src, copy_size);
			assert(err == EOK);
			err = arch_user_bzero((void *)(va_dst + copy_size), zero_size);
			assert(err == EOK);
		} else {
			/* clear full page */
			assert(va_dst >= va_bss);
			fast_clear_page((void *)va_dst);
		}

		if ((phdr->p_flags & PF_X) != 0) {
			err = bsp_cache(CACHE_OP_INVAL_ICACHE_RANGE, va_dst, PAGE_SIZE, va_dst);
			if (err != EOK) {
#ifdef DEBUG
				printk("--- cache invaliation failed %u\n", err);
#endif
				return err;
			}
		}

		va_src += PAGE_SIZE;
		va_dst += PAGE_SIZE;
	}

	/* lastly, we set the right page protection */
	va_dst = ALIGN_DOWN(phdr->p_vaddr, PAGE_SIZE);
	va_end = ALIGN_UP(phdr->p_vaddr + phdr->p_memsz, PAGE_SIZE);

	prot = 0;
	if ((phdr->p_flags & PF_R) != 0) {
		prot |= PROT_READ;
	}
	if ((phdr->p_flags & PF_W) != 0) {
		prot |= PROT_WRITE;
	}
	if ((phdr->p_flags & PF_X) != 0) {
		prot |= PROT_EXEC;
	}

	err = vm_protect(proc->adspace, va_dst, va_end - va_dst, prot);
	if (err != EOK) {
#ifdef DEBUG
		printk("--- mprotect of memory failed, error %u\n", err);
#endif
		return err;
	}

	return EOK;
}

/** load the (correct and trusted) ELF file behind "blob" in RAM
 * returns the program's entry point
 */
addr_t elf_load(struct process *proc, const struct memrq_cfg *memrq, const void *file)
{
	const ehdr_t *ehdr;
	const phdr_t *phdr;
	unsigned int i;
	err_t err;

	ehdr = (const ehdr_t *) file;
	if (!((ehdr->e_ident[0] == 0x7f) &&
	      (ehdr->e_ident[1] == 'E') &&
	      (ehdr->e_ident[2] == 'L') &&
	      (ehdr->e_ident[3] == 'F') &&

	      (ehdr->e_ident[4] == ARCH_ELF_CLASS) &&
	      (ehdr->e_ident[5] == ARCH_ELF_DATA) &&
	      (ehdr->e_ident[6] == 1) &&
	      (ehdr->e_type == 2) && /* executable file */
	      (ehdr->e_machine == ARCH_ELF_MACHINE) &&
	      (ehdr->e_version == 1) &&
	      (ehdr->e_ehsize == sizeof(*ehdr)) &&
	      (ehdr->e_phentsize == sizeof(*phdr)) &&
	      (ehdr->e_shentsize == sizeof(shdr_t)) &&
	      (ehdr->e_phnum > 0) )) {
		panic("invalid ELF file\n");
	}

#ifdef DEBUG
	printk("ELF entry point is %zx\n", ehdr->e_entry);
#endif

	for (i = 0; i < ehdr->e_phnum; i++) {
		phdr = (const phdr_t *)((addr_t)file + ehdr->e_phoff + i * sizeof(*phdr));
		if (phdr->p_type != PT_LOAD) {
			continue;
		}

		err = elf_copy(proc, memrq, (addr_t)file, phdr);
		if (err != EOK) {
			goto error;
		}
	}

	return ehdr->e_entry;

error:
	panic("could not map initial user space binary\n");
}

/** prepare the user space heap like an ELF segment */
void elf_heap(struct process *proc, const struct memrq_cfg *memrq, addr_t heap_base, size_t heap_size)
{
	phdr_t fake_phdr;
	err_t err;

	if (heap_size == 0) {
		return;
	}

	fake_phdr.p_offset = 0;
	fake_phdr.p_filesz = 0;
	fake_phdr.p_vaddr = heap_base;
	fake_phdr.p_memsz = heap_size;
	fake_phdr.p_flags = PF_R|PF_W;

	err = elf_copy(proc, memrq, 0, &fake_phdr);
	if (err != EOK) {
		panic("could not map initial user space heap\n");
	}
}

/** prepare the user space stack like an ELF segment */
void elf_stack(struct process *proc, const struct memrq_cfg *memrq, addr_t stack_base, size_t stack_size)
{
	phdr_t fake_phdr;
	err_t err;

	if (stack_size == 0) {
		return;
	}

	fake_phdr.p_offset = 0;
	fake_phdr.p_filesz = 0;
	fake_phdr.p_vaddr = stack_base;
	fake_phdr.p_memsz = stack_size;
	fake_phdr.p_flags = PF_R|PF_W;

	err = elf_copy(proc, memrq, 0, &fake_phdr);
	if (err != EOK) {
		panic("could not map initial user space stack\n");
	}
}
