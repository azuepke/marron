/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2020, 2021, 2024, 2025 Alexander Zuepke */
/*
 * part.c
 *
 * Partitioning
 *
 * azuepke, 2018-01-02: initial
 * azuepke, 2025-02-21: adspace rework
 * azuepke, 2025-02-22: processes
 */

#include <part.h>
#include <thread.h>
#include <marron/compiler.h>
#include <assert.h>
#include <irq.h>
#include <kernel.h>
#include <current.h>
#include <adspace.h>
#include <memrq.h>
#include <panic.h>
#include <kldd.h>
#include <timer.h>
#include <sys_proto.h>
#include <string.h>
#include <uaccess.h>
#include <fd.h>
#include <epoll_types.h>
#include <process.h>


/* forward */
static void sys_part_stop_deferred(struct thread *thr);
static void sys_part_restart_deferred(struct thread *thr);

/** initialize all partition objects at boot time */
__init void part_init_all(void)
{
	const struct part_cfg *cfg;
	struct part *part;

	for (unsigned int i = 0; i < part_num; i++) {
		cfg = &part_cfg[i];
		part = &part_dyn[i];

		spin_init(&part->lock);

		part->part_cfg = cfg;
		part->state = PART_STATE_IDLE;
		part->part_id = i;

		/* assign signal queue nodes to partition */
		list_head_init(&part->free_sigqh);
		for (unsigned int j = 0; j < cfg->sig_queue_num; j++) {
			struct sig_queue_node *sigqn = &cfg->sig_queue_node[j];

			list_node_init(&sigqn->sigql);
			list_insert_last(&part->free_sigqh, &sigqn->sigql);
		}

		/* assign IRQs to partition */
		for (unsigned int j = 0; j < cfg->irq_num; j++) {
			struct irq *irq = irq_by_id(cfg->irq_cfg[j].irq_id);

			irq->part = part;
		}

		/* bind all threads to the partition */
		list_head_init(&part->thread_freeqh);
		for (unsigned int j = 0; j < cfg->thread_num; j++) {
			struct thread *thr = &cfg->thread[j];

			thr->part = part;
			thr->max_prio = cfg->max_prio;
			list_node_init(&thr->thread_freeql);
			list_insert_last(&part->thread_freeqh, &thr->thread_freeql);
		}

		/* assign timers to partition */
		list_head_init(&part->timer_freeqh);
		for (unsigned int j = 0; j < cfg->timer_num; j++) {
			struct timer *timer = &cfg->timer[j];

			timer->part_id = i;
			list_node_init(&timer->timer_freeql);
			list_insert_last(&part->timer_freeqh, &timer->timer_freeql);
			timer->state = TIMER_STATE_DISABLED;
		}

		/* init file descriptors */
		list_head_init(&part->fd_freeqh);
		fd_init_all(part);

		/* fill up epoll_note free list */
		list_head_init(&part->epoll_note_freeqh);
		for (unsigned int j = 0; j < cfg->epoll_note_num; j++) {
			struct epoll_note *note = &cfg->epoll_note[j];

			list_node_init(&note->epoll_noteql);
			list_insert_last(&part->epoll_note_freeqh, &note->epoll_noteql);
		}

		/* bind adspace to partition */
		{
			struct adspace *adspace = &adspace_dyn[i];

			adspace->part = part;
		}

		/* assign KMEM for pagetables to kmempool */
		spin_init(&part->kmem_pagetables_lock);
		kmempool_init(&part->kmem_pagetables);

		/* NOTE: do not touch KMEM page tables of the kernel (handled by BSP) */
		if (i != 0) {
			char *kmem_ptr = cfg->kmem_pagetables_mem;
			for (unsigned int j = 0; j < cfg->kmem_pagetables_num; j++) {
				assert(kmem_ptr != NULL);
				kmempool_put(&part->kmem_pagetables, kmem_ptr);
				kmem_ptr += KMEM_PAGETABLE_SIZE;
			}
		}

		/* NOTE: initialization of VMA data is implemented in vma.c */

		/* setup processes */
		list_head_init(&part->process_freeqh);
		list_head_init(&part->process_activeqh);
		for (unsigned int j = 0; j < cfg->process_num; j++) {
			struct process *proc = &cfg->process[j];

			proc->part = part;
			proc->proc_id = j;
			list_node_init(&proc->process_freeql);
			list_insert_last(&part->process_freeqh, &proc->process_freeql);
		}
	}
}

/** start a partition and its processes */
void part_start(struct part *part)
{
	const struct process_cfg *proc_cfg;
	const struct part_cfg *cfg;
	struct process *proc;
	struct thread *thr;
	err_t err;

	assert(part != NULL);

	part_lock(part);

	if (part->state == PART_STATE_RUNNING) {
		part_unlock(part);
		return;
	}

	assert(part->state == PART_STATE_IDLE);
	part->state = PART_STATE_RUNNING;

	cfg = part->part_cfg;
	assert(cfg != NULL);

	/* reset all pool allocators */
	for (unsigned int i = 0; i < cfg->memrq_num; i++) {
		err = memrq_page_reinit(&cfg->memrq_cfg[i]);
		if (err != EOK) {
			panic("part_start: %s: memrq_page_reinit error %u\n", cfg->name, err);
		}
	}

	/* clear all KLDD private data */
	for (unsigned int i = 0; i < cfg->kldd_num; i++) {
		cfg->kldd_cfg[i].priv->data = 0;
	}

	part_unlock(part);

	/* start pre-configured processes */
	for (unsigned int i = 0; i < cfg->process_cfg_num; i++) {
		proc_cfg = &cfg->process_cfg[i];

		proc = process_create(part);
		if (proc == NULL) {
			panic("process_start: %s.%s failed\n", cfg->name, proc_cfg->name);
		}
		proc->critical_process = 1;

		thr = process_create_first_thread(proc, proc_cfg);
		if (thr == NULL) {
			panic("process_create_first_thread: %s.%s failed\n", cfg->name, proc_cfg->name);
		}
	}
}

/** stop a partition and its processes */
void part_stop(struct part *part)
{
	const struct part_cfg *cfg;
	list_t *node;

	assert(part != NULL);
	assert(part->part_id != 0);

	part_lock(part);
	if (part->state == PART_STATE_IDLE) {
		part_unlock(part);
		return;
	}

	assert(part->state == PART_STATE_RUNNING);
	part->state = PART_STATE_IDLE;
	part_unlock(part);

	cfg = part->part_cfg;

	/* kill all interrupts */
	for (unsigned int i = 0; i < cfg->irq_num; i++) {
		struct irq *irq = irq_by_id(cfg->irq_cfg->irq_id);

		irq_shutdown(irq);
	}

	/* kill all processes and their threads */
	while ((node = list_first(&part->process_activeqh))) {
		struct process *proc = PROCESS_FROM_ACTIVEQ(node);

		process_destroy(proc);
	}
}

/** restart partition */
void part_restart(struct part *part)
{
	assert(part != NULL);
	assert(part->part_id != 0);

	part_stop(part);
	assert(part->state == PART_STATE_IDLE);
	part_start(part);
	assert(part->state == PART_STATE_RUNNING);
}

/** raise a partition error (always to currently running partition) */
void part_error(struct part *part, unsigned int sig)
{
	assert(part != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));

	// FIXME: debug output to help debugging
	kernel_print_current();
	printk("partition %s signal %u, partition stopped\n",
	       part->part_cfg->name, sig);

	part_stop(part);
}

////////////////////////////////////////////////////////////////////////////////

err_t sys_part_name(char *name_user, size_t size_user)
{
	const char *part_name;
	size_t size;

	part_name = current_part()->part_cfg->name;
	size = strlen(part_name) + 1;
	if (size > size_user) {
		return ELIMIT;
	}

	return user_copy_out(name_user, part_name, size);
}

uint32_t sys_part_self(void)
{
	return current_part()->part_id;
}

uint32_t sys_part_state(void)
{
	return current_part()->state;
}

err_t sys_part_shutdown(void)
{
	struct thread *thr;

	/* defer to not mess up any arguments when the thread is restarted */
	thr = current_thread();
	thr->cont_args.part = thr->part;
	thread_defer_to(thr, sys_part_stop_deferred);

	return EOK;
}

static void sys_part_stop_deferred(struct thread *thr)
{
	part_stop(thr->cont_args.part);
}

err_t sys_part_restart(void)
{
	struct thread *thr;

	/* defer to not mess up any arguments when the thread is restarted */
	thr = current_thread();
	thr->cont_args.part = thr->part;
	thread_defer_to(thr, sys_part_restart_deferred);

	return EOK;
}

static void sys_part_restart_deferred(struct thread *thr)
{
	part_restart(thr->cont_args.part);
}

err_t sys_part_state_other(ulong_t part_id, unsigned int *state_user)
{
	struct part *part;

	if ((current_part()->part_cfg->perm & PART_PERM_PART_OTHER) == 0) {
		return EPERM;
	}

	if ((part_id < 1) || (part_id >= part_num)) {
		return ELIMIT;
	}

	part = &part_dyn[part_id];
	assert(part != NULL);

	return user_put_4(state_user, (unsigned int)part->state);
}

err_t sys_part_shutdown_other(ulong_t part_id)
{
	struct thread *thr;
	struct part *part;

	if ((current_part()->part_cfg->perm & PART_PERM_PART_OTHER) == 0) {
		return EPERM;
	}

	if ((part_id < 1) || (part_id >= part_num)) {
		return ELIMIT;
	}

	part = &part_dyn[part_id];
	assert(part != NULL);

	/* defer to not mess up any arguments when the thread is restarted */
	thr = current_thread();
	thr->cont_args.part = part;
	thread_defer_to(thr, sys_part_stop_deferred);

	return EOK;
}

err_t sys_part_restart_other(ulong_t part_id)
{
	struct thread *thr;
	struct part *part;

	if ((current_part()->part_cfg->perm & PART_PERM_PART_OTHER) == 0) {
		return EPERM;
	}

	if ((part_id < 1) || (part_id >= part_num)) {
		return ELIMIT;
	}

	part = &part_dyn[part_id];
	assert(part != NULL);

	/* defer to not mess up any arguments when the thread is restarted */
	thr = current_thread();
	thr->cont_args.part = part;
	thread_defer_to(thr, sys_part_restart_deferred);

	return EOK;
}
