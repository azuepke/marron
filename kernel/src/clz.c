/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * clz.c
 *
 * Bit counting (like in compiler support library).
 *
 * azuepke, 2023-09-02: initial
 */


/** Count leading zeros
 *
 * All instances of __builtin_clz*() call this function.
 *
 * algorithm from Hacker's Delight, Figure 5-11
 */
static int __clz(unsigned long x)
{
	int n;

	if (x == 0) {
		return __LONG_WIDTH__;
	}

	n = 0;
#if __LONG_WIDTH__ == 64
	if ((x >> (__LONG_WIDTH__ - 32)) == 0) {
		n += 32;
		x <<= 32;
	}
#endif
	if ((x >> (__LONG_WIDTH__ - 16)) == 0) {
		n += 16;
		x <<= 16;
	}
	if ((x >> (__LONG_WIDTH__ - 8)) == 0) {
		n += 8;
		x <<= 8;
	}
	if ((x >> (__LONG_WIDTH__ - 4)) == 0) {
		n += 4;
		x <<= 4;
	}
	if ((x >> (__LONG_WIDTH__ - 2)) == 0) {
		n += 2;
		x <<= 2;
	}
	if ((x >> (__LONG_WIDTH__ - 1)) == 0) {
		n += 1;
	}

	return n;
}

#if __LONG_WIDTH__ == 64

int __clzdi2(unsigned long x);
int __clzdi2(unsigned long x)
{
	return __clz(x);
}

#else /* 32-bit version */

int __clzsi2(unsigned int x);
int __clzsi2(unsigned int x)
{
	return __clz(x);
}

int __clzdi2(unsigned long long x);
int __clzdi2(unsigned long long x)
{
	unsigned long h = x >> 32;
	unsigned long l = x & 0xffffffff;

	if (h != 0) {
		return __clz(h);
	} else {
		return __clz(l) + 32;
	}
}

#endif
