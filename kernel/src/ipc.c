/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024, 2025 Alexander Zuepke */
/*
 * ipc.c
 *
 * Inter-process communication.
 *
 * azuepke, 2022-07-17: initial
 */

#include <ipc.h>
#include <ipc_types.h>
#include <kernel.h>
#include <assert.h>
#include <stddef.h>
#include <marron/error.h>
#include <part_types.h>
#include <part.h>
#include <uaccess.h>
#include <sys_proto.h>
#include <marron/mapping.h>
#include <fd.h>
#include <thread.h>
#include <sched.h>
#include <stddef.h>
#include <marron/atomic.h>
#include <adspace.h>
#include <vm.h>
#include <memrq_types.h>
#include <marron/macros.h>
#include <marron/fcntl.h>
#include <string.h>
#include <memrq.h>
#include <epoll.h>
#include <eventfd.h>
#include <timer.h>
#include <irq.h>
#include <signalfd.h>
#include <process.h>


/* forward declarations */
static void ipc_wait_cancel(struct thread *thr, err_t wakeup_code);
static void ipc_wait_finish(struct thread *thr);

/** initialize all IPC channel objects at boot time */
__init void ipc_init_all(void)
{
	/* in ipc_fd_dyn[], first come the servers, then the clients */
	assert(ipc_server_num <= ipc_fd_num);

	/* IPC client and server static fds are special:
	 * - the file descriptors start with a usage counter of 1
	 *   and thus cannot be closed
	 */

	for (unsigned int i = 0; i < ipc_server_num; i++) {
		const struct ipc_server_cfg *cfg = &ipc_server_cfg[i];
		struct fd *fd;

		fd = cfg->server_fd;
		assert(fd == &ipc_fd_dyn[i]);

		spin_init(&fd->lock);
		fd->usage = 1;
		fd->part_id = 0;
		fd->type = FD_TYPE_IPC_SERVER;
		fd->cached_eventmask = 0;
		fd->flags = FD_FLAG_STATIC_FD;
		list_head_init(&fd->epoll_fdqh);

		fd->ipc_server.client_fd = NULL;
		list_head_init(&fd->ipc_server.ipc_callqh);
		list_head_init(&fd->ipc_server.ipc_replyqh);
		list_head_init(&fd->ipc_server.ipc_serverqh);
	}

	for (unsigned int i = 0; i < ipc_client_num; i++) {
		const struct ipc_client_cfg *cfg = &ipc_client_cfg[i];
		struct fd *fd;

		fd = cfg->client_fd;
		if (fd == NULL) {
			continue;
		}

		spin_init(&fd->lock);
		fd->usage = 1;
		fd->part_id = 0;
		fd->type = FD_TYPE_IPC_CLIENT;
		fd->cached_eventmask = EPOLLNVAL;
		fd->flags = FD_FLAG_STATIC_FD;
		list_head_init(&fd->epoll_fdqh);

		assert(cfg->server_fd != NULL);
		fd->ipc_client.server_fd = cfg->server_fd;

		if (cfg->server_fd != NULL) {
			assert((cfg->server_fd->ipc_server.client_fd == NULL) ||
			       (cfg->server_fd->ipc_server.client_fd == fd));
			cfg->server_fd->ipc_server.client_fd = fd;
		}
	}
}

/** generate a new IPC reply handle for a thread */
static inline uint32_t new_rhnd(
	struct thread *thr,
	struct part *part)
{
	unsigned int global_thr_id;
	unsigned int server_part;
	unsigned int gen;

	server_part = part->part_id;
	assert(server_part <= 0xff);

	assert(thr != NULL);
	global_thr_id = thr->global_thr_id;
	assert(global_thr_id < 0x3fff);

	gen = thr->ipc.rhnd_generation;
	thr->ipc.rhnd_generation++;

	return 0x40000000u | (global_thr_id << 16) | (server_part << 8) | gen;
}

/** check an IPC reply handle for a partition; return client thread
 *
 * We check the server-side part first before checking the client-side part.
 * Any interference is minimized by just checking a thread's reply_handle.
 */
static inline struct thread *check_rhnd(
	struct part *part,
	uint32_t rhnd)
{
	size_t global_thr_id;
	struct thread *thr;

	assert(part != NULL);

	if ((rhnd & 0x40000000u) == 0) {
		return NULL;
	}

	if (((rhnd >> 8) & 0xff) != part->part_id) {
		return NULL;
	}

	global_thr_id = (rhnd >> 16) & 0x3fff;
	if (global_thr_id > thread_num) {
		return NULL;
	}

	thr = &thread_dyn[global_thr_id];
	if (thr->ipc.rhnd != rhnd) {
		return NULL;
	}

	return thr;
}

err_t sys_ipc_port_name(
	ulong_t type,
	ulong_t id,
	char *name_user,
	size_t size_user)
{
	const struct ipc_server_cfg *s;
	const struct ipc_client_cfg *c;
	const struct part_cfg *cfg;
	const char *name;
	size_t size;

	cfg = current_part()->part_cfg;
	switch (type) {
	case IPC_TYPE_SERVER:
		if (id >= cfg->ipc_server_num) {
			/* out of bounds */
			return ELIMIT;
		}
		s = &cfg->ipc_server_cfg[id];
		name = s->name;
		break;

	case IPC_TYPE_CLIENT:
		if (id >= cfg->ipc_client_num) {
			/* out of bounds */
			return ELIMIT;
		}
		c = &cfg->ipc_client_cfg[id];
		name = c->name;
		break;

	default:
		/* invalid type */
		return EINVAL;
	}

	size = strlen(name) + 1;
	if (size > size_user) {
		return ELIMIT;
	}

	return user_copy_out(name_user, name, size);
}

err_t sys_ipc_port_fd(
	ulong_t type,
	ulong_t id,
	uint32_t *fd_index_user)
{
	const struct ipc_server_cfg *s;
	const struct ipc_client_cfg *c;
	const struct part_cfg *cfg;
	uint32_t static_fd_index;

	cfg = current_part()->part_cfg;
	switch (type) {
	case IPC_TYPE_SERVER:
		if (id >= cfg->ipc_server_num) {
			/* out of bounds */
			return ELIMIT;
		}
		s = &cfg->ipc_server_cfg[id];
		static_fd_index = s->static_fd_index;
		break;

	case IPC_TYPE_CLIENT:
		if (id >= cfg->ipc_client_num) {
			/* out of bounds */
			return ELIMIT;
		}
		c = &cfg->ipc_client_cfg[id];
		if (c->client_fd == NULL) {
			/* client IPC not connected */
			return ESTATE;
		}
		static_fd_index = c->static_fd_index;
		break;

	default:
		/* invalid type */
		return EINVAL;
	}

	return user_put_4(fd_index_user, static_fd_index);
}

/** Create two connected IPC handles for client/server communication */
err_t sys_ipc_pair(
	ulong_t flags,
	uint32_t *fd0_index_user,
	uint32_t *fd1_index_user)
{
	uint32_t fd_indices[2];
	struct fd *server_fd;
	struct fd *client_fd;
	struct process *proc;
	int cloexec;
	err_t err;

	if ((flags & ~SYS_FD_CLOEXEC) != 0) {
		/* reserved flags */
		return EINVAL;
	}
	cloexec = (flags & SYS_FD_CLOEXEC) != 0;

	if ((fd0_index_user == NULL) || (fd1_index_user == NULL)) {
		/* NULL pointer */
		return EINVAL;
	}

	proc = current_process();
	server_fd = fd_alloc(proc);
	client_fd = fd_alloc(proc);
	if ((server_fd == NULL) || (client_fd == NULL)) {
		/* failed to allocate two file descriptors */
		err = ENFILE;
		goto out_free;
	}

	/* further initialization */
	server_fd->type = FD_TYPE_IPC_SERVER;
	server_fd->cached_eventmask = 0;
	server_fd->flags = FD_FLAG_SHAREABLE;
	server_fd->ipc_server.client_fd = client_fd;
	list_head_init(&server_fd->ipc_server.ipc_callqh);
	list_head_init(&server_fd->ipc_server.ipc_replyqh);
	list_head_init(&server_fd->ipc_server.ipc_serverqh);

	client_fd->type = FD_TYPE_IPC_CLIENT;
	client_fd->cached_eventmask = EPOLLNVAL;
	client_fd->flags = FD_FLAG_SHAREABLE;
	client_fd->ipc_client.server_fd = server_fd;

	err = EOK;

	err = fd_dup2(proc, server_fd, client_fd, cloexec, fd_indices);
	if (err != EOK) {
		goto out_free;
	}

	if (err == EOK) {
		err = user_put_4(fd0_index_user, fd_indices[0]);
	}
	if (err == EOK) {
		err = user_put_4(fd1_index_user, fd_indices[1]);
	}

	return err;

out_free:
	if (server_fd != NULL) {
		server_fd->type = FD_TYPE_DEAD;
		fd_close(server_fd);
	}
	if (client_fd != NULL) {
		client_fd->type = FD_TYPE_DEAD;
		fd_close(client_fd);
	}

	return err;
}

/** wake all client threads waiting to call */
static void ipc_wake_clients_in_call(
	struct fd *server_fd)
{
	struct thread *thr;
	list_t *node;

	assert(server_fd != NULL);
	assert(server_fd->type == FD_TYPE_IPC_SERVER);
	assert_fd_locked(server_fd);

	list_for_each_remove_first(&server_fd->ipc_server.ipc_callqh, node) {
		thr = THR_FROM_IPC_ANYQ(node);
		assert(thr != NULL);
		assert(thr->state == THREAD_STATE_WAIT_IPC_SEND);

		/* wake waiting thread with EPIPE */
		sched_lock(thr->sched);
		thread_wakeup(thr);
		thr->wakeup_code = EPIPE;
		thread_cancel_clear(thr);
		sched_unlock(thr->sched);
	}
}

/** wake all client threads waiting for a reply */
static void ipc_wake_clients_in_reply(
	struct fd *server_fd)
{
	struct thread *thr;
	list_t *node;

	assert(server_fd != NULL);
	assert(server_fd->type == FD_TYPE_IPC_SERVER);
	assert_fd_locked(server_fd);

	list_for_each_remove_first(&server_fd->ipc_server.ipc_replyqh, node) {
		thr = THR_FROM_IPC_ANYQ(node);
		assert(thr != NULL);
		assert(thr->state == THREAD_STATE_WAIT_IPC_REPLY);

		/* invalidate any reply handles */
		thr->ipc.rhnd = 0;

		/* wake waiting thread with EPIPE */
		sched_lock(thr->sched);
		thread_wakeup(thr);
		thr->wakeup_code = EPIPE;
		thread_cancel_clear(thr);
		sched_unlock(thr->sched);
	}
}

/** wake all server threads waiting for incoming IPC */
static void ipc_wake_servers_in_wait(
	struct fd *server_fd)
{
	struct thread *thr;
	list_t *node;

	assert(server_fd != NULL);
	assert(server_fd->type == FD_TYPE_IPC_SERVER);
	assert_fd_locked(server_fd);

	list_for_each_remove_first(&server_fd->ipc_server.ipc_serverqh, node) {
		thr = THR_FROM_IPC_ANYQ(node);
		assert(thr != NULL);
		assert(thr->state == THREAD_STATE_WAIT_IPC_WAIT);

		/* wake waiting thread with EPIPE */
		sched_lock(thr->sched);
		thread_wakeup(thr);
		thr->wakeup_code = EPIPE;
		thread_cancel_clear(thr);
		sched_unlock(thr->sched);
	}
}

/** close callback for client IPC endpoint */
struct fd *ipc_close_client(
	struct fd *client_fd)
{
	struct fd *server_fd;

	assert(client_fd != NULL);
	assert(client_fd->type == FD_TYPE_IPC_CLIENT);
	assert_fd_locked(client_fd);

	/* closing the client_fd is easy -- we just remove the link to server_fd */
	server_fd = client_fd->ipc_client.server_fd;
	assert(server_fd != NULL);
	client_fd->ipc_client.server_fd = NULL;

	/* due to locking reasons, further cleanup happens later in ipc_notify_server_close() */
	return server_fd;
}

/** notify server that the client IPC endpoint was closed */
void ipc_notify_server_close(
	struct fd *server_fd,
	struct fd *client_fd)
{
	assert(server_fd != NULL);
	assert(client_fd != NULL);

	fd_lock(server_fd);

	if ((server_fd->type == FD_TYPE_IPC_SERVER) &&
	       (server_fd->ipc_server.client_fd == client_fd)) {
		/* this is the right server */
		/* NOTE: SMP: there might be a small race window
		 * when closing an IPC fd pair and then re-creating it later ...
		 * we might end up closing the next generation.
		 */
		server_fd->ipc_server.client_fd = NULL;

		ipc_wake_servers_in_wait(server_fd);

		epoll_notify(server_fd, EPOLLERR | EPOLLHUP);
	}

	fd_unlock(server_fd);
}

/** close callback for server IPC endpoint */
void ipc_close_server(
	struct fd *server_fd)
{
	assert(server_fd != NULL);
	assert(server_fd->type == FD_TYPE_IPC_SERVER);
	assert_fd_locked(server_fd);

	/* closing the server_fd */
	server_fd->ipc_server.client_fd = NULL;

	ipc_wake_clients_in_call(server_fd);
	ipc_wake_clients_in_reply(server_fd);
	ipc_wake_servers_in_wait(server_fd);
}


/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the thread is deleted
 */
static void ipc_wait_cancel(struct thread *thr, err_t wakeup_code)
{
	struct fd *server_fd;

	assert(thr != NULL);
	assert((wakeup_code == EINTR) ||
	       (wakeup_code == ECANCEL));

	server_fd = thr->ipc.fd;
	assert(server_fd != NULL);

	/* remove thread from wait queue */
	fd_lock(server_fd);

	/* invalidate any reply handles */
	thr->ipc.rhnd = 0;

	// FIXME: this lacks a state check!
	thr->wakeup_code = wakeup_code;

	/* FIXME: this might fail with a NULL pointer exception in a debug kernel
	 * in a rare race condition with timeout expiration.
	 */
	list_remove(&thr->ipc.ipc_anyql);

	fd_unlock(server_fd);
}

/** Continuation after blocking: just copy out the IPC msg */
static void ipc_wait_finish(struct thread *thr)
{
	struct sys_ipc *recv_msg_user;
	uint32_t *recv_rhnd_user;
	err_t err;

	err = thr->wakeup_code;

	if (err == EOK) {
		recv_msg_user = thr->cont_args.ipc_wait.recv_msg_user;
		assert(recv_msg_user != NULL);
		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
	}
	if (err == EOK) {
		recv_rhnd_user = thr->cont_args.ipc_wait.recv_rhnd_user;
		if (recv_rhnd_user != NULL) {
			err = user_put_4(recv_rhnd_user, thr->cont_args.ipc_wait.recv_rhnd);
		}
	}

	arch_syscall_retval(thr->regs, err);
}

/** prepare (copy-in and validate) IPC message for IPC call */
static inline __always_inline err_t ipc_call_prepare_msg(
	struct thread *thr,
	const struct sys_ipc *send_msg_user)
{
	unsigned int num_vecs;
	size_t all_size;
	err_t err;

	err = user_copy_in_aligned(&thr->ipc.msg, send_msg_user, sizeof(thr->ipc.msg));
	if (err != EOK) {
		/* copy-in failed */
		return err;
	}
	/* validate IPC message */
	if ((thr->ipc.msg.flags & ~(SYS_IPC_MAP | SYS_IPC_IOV |
	                            SYS_IPC_BUF0R | SYS_IPC_BUF0W |
	                            SYS_IPC_BUF1R | SYS_IPC_BUF1W |
	                            SYS_IPC_BUF2R | SYS_IPC_BUF2W |
	                            SYS_IPC_FD0 | SYS_IPC_FD1 |
	                            SYS_IPC_FD_CLOEXEC)) != 0) {
		/* reserved flags */
		return EINVAL;
	}

	if ((thr->ipc.msg.flags & SYS_IPC_MAP) != 0) {
		/* request mapping */
		if ((thr->ipc.msg.flags & ~(SYS_IPC_MAP |
		                            SYS_IPC_BUF1R | SYS_IPC_BUF1W |
		                            SYS_IPC_BUF2R | SYS_IPC_BUF2W |
		                            SYS_IPC_FD_CLOEXEC)) != 0) {
			/* invalid IPC flags */
			return EINVAL;
		}

		if (((thr->ipc.msg.map_addr | thr->ipc.msg.map_size) & (PAGE_SIZE - 1)) != 0) {
			/* not page aligned */
			return EINVAL;
		}
		if (!USER_CHECK_RANGE(thr->ipc.msg.map_addr, thr->ipc.msg.map_size)) {
			/* not valid user space region */
			return EINVAL;
		}
		if (thr->ipc.msg.map_size == 0) {
			/* zero size is not OK */
			return EINVAL;
		}
		if ((thr->ipc.msg.map_prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) != 0) {
			/* invalid permissions */
			return EINVAL;
		}
		if ((thr->ipc.msg.map_flags & ~(MAP_TYPE |
		                                MAP_FIXED | MAP_EXCL)) != 0) {
			/* invalid mapping flags */
			return EINVAL;
		}
		switch (thr->ipc.msg.map_flags & MAP_TYPE) {
		case MAP_SHARED:
		case MAP_PRIVATE:
			break;
		default:
			/* invalid mapping type */
			return EINVAL;
		}
	} else if ((thr->ipc.msg.flags & (SYS_IPC_BUF0R | SYS_IPC_BUF0W)) != 0) {
		if ((thr->ipc.msg.flags & SYS_IPC_IOV) != 0) {
			/* vector mode */
			num_vecs = thr->ipc.msg.iov_num;
			if ((num_vecs == 0) || (num_vecs > SYS_IOV_MAX)) {
				return EINVAL;
			}
			thr->ipc.iov_num = num_vecs;

			err = user_copy_in_aligned(thr->ipc.vec, thr->ipc.msg.iov, num_vecs * sizeof(thr->ipc.vec[0]));
			if (err != EOK) {
				return err;
			}

			all_size = 0;
			for (unsigned int i = 0; i < num_vecs; i++) {
				if (!USER_CHECK_RANGE((addr_t)thr->ipc.vec[i].addr, thr->ipc.vec[i].size)) {
					/* not valid user space region */
					return EINVAL;
				}
#if __SIZEOF_LONG__ == 4
				if (all_size + thr->ipc.vec[i].size < all_size) {
					/* integer overflow */
					return EINVAL;
				}
#endif
				all_size += thr->ipc.vec[i].size;
			}
			/* update overall size in message */
			thr->ipc.msg.size0 = all_size;
		} else {
			/* buffer mode */
			if (!USER_CHECK_RANGE((addr_t)thr->ipc.msg.buf0, thr->ipc.msg.size0)) {
				/* not valid user space region */
				return EINVAL;
			}
			thr->ipc.iov_num = 1;
			thr->ipc.vec[0].addr = thr->ipc.msg.buf0;
			all_size = thr->ipc.msg.size0;
			thr->ipc.vec[0].size = all_size;
		}
#if __SIZEOF_LONG__ == 4
		if ((ssize_t)all_size < 0) {
			/* outside the range of ssize_t */
			return EINVAL;
		}
#else
		/* all_size must never overflow, USER_CHECK_RANGE() enforces a limit */
		assert((ssize_t)ARCH_USER_END > 0);
		assert((ssize_t)(ARCH_USER_END * SYS_IOV_MAX) > 0);
#endif
	}
	if ((thr->ipc.msg.flags & (SYS_IPC_BUF1R | SYS_IPC_BUF1W)) != 0) {
		if (!USER_CHECK_RANGE((addr_t)thr->ipc.msg.buf1, thr->ipc.msg.size1)) {
			/* not valid user space region */
			return EINVAL;
		}
		if ((ssize_t)thr->ipc.msg.size1 < 0) {
			/* outside the range of ssize_t */
			return EINVAL;
		}
	}
	if ((thr->ipc.msg.flags & (SYS_IPC_BUF2R | SYS_IPC_BUF2W)) != 0) {
		if (!USER_CHECK_RANGE((addr_t)thr->ipc.msg.buf2, thr->ipc.msg.size2)) {
			/* not valid user space region */
			return EINVAL;
		}
		if ((ssize_t)thr->ipc.msg.size2 < 0) {
			/* outside the range of ssize_t */
			return EINVAL;
		}
	}
	if ((thr->ipc.msg.flags & (SYS_IPC_FD0 | SYS_IPC_FD1)) == (SYS_IPC_FD1)) {
		/* FD0 bit missing */
		return EINVAL;
	}

	return EOK;
}

err_t sys_ipc_call(
	ulong_t fd_index,
	const struct sys_ipc *send_msg_user,
	struct sys_ipc *recv_msg_user)
{
	struct thread *server_thr;
	unsigned int thr_state;
	struct fd *client_fd;
	struct fd *server_fd;
	struct thread *thr;
	list_t *ipc_qh;
	uint32_t rhnd;
	list_t *node;
	err_t err;

	if ((send_msg_user == NULL) || (recv_msg_user == NULL)) {
		/* NULL pointer arguments */
		return EINVAL;
	}

	thr = current_thread();
	err = ipc_call_prepare_msg(thr, send_msg_user);
	if (err != EOK) {
		/* IPC message copying or validation failed */
		return err;
	}

	/* arguments for deferred IPC receive */
	thr->cont_args.ipc_wait.recv_msg_user = recv_msg_user;
	thr->cont_args.ipc_wait.recv_rhnd_user = NULL;
	thr->cont_args.ipc_wait.recv_rhnd = 0;
	thr->ipc.reply_flags = 0;
	thr->ipc.reply.h64 = 0;

	/* get and lock used fd */
	client_fd = fd_get_and_lock(current_process(), fd_index);
	if (client_fd == NULL) {
		/* invalid file descriptor */
		return EBADF;
	}

	/*
	 * NOTE: all called xxx_ipc_call() functions unlock "fd" internally.
	 */
	switch (client_fd->type) {
	case FD_TYPE_IPC_CLIENT:
		break;

	case FD_TYPE_MEMRQ:
		return memrq_ipc_call(client_fd);

	case FD_TYPE_EVENTFD:
		return eventfd_ipc_call(client_fd);

	case FD_TYPE_TIMERFD:
		return timerfd_ipc_call(client_fd);

	case FD_TYPE_IRQFD:
		return irqfd_ipc_call(client_fd);

	case FD_TYPE_SIGNALFD:
		return signalfd_ipc_call(client_fd);

	case FD_TYPE_IPC_SERVER:
	case FD_TYPE_EPOLL:
	default:
		return fd_generic_ipc_call(client_fd);
	}

	/* resolve IPC server */
	server_fd = client_fd->ipc_client.server_fd;
	fd_unlock(client_fd);

	if (server_fd == NULL) {
		/* other side is closed */
		return EPIPE;
	}

	/* NOTE: SMP: the unlock -> lock sequence is fine if the server still
	 * points to the same client. While waiting for the lock,
	 * the server_fd might have been closed and re-created ...
	 * but c'est la vie -- the outcome is the same.
	 */
	fd_lock(server_fd);
	if ((server_fd->type != FD_TYPE_IPC_SERVER) ||
	        (server_fd->ipc_server.client_fd != client_fd)) {
		fd_unlock(server_fd);
		/* other side is in a wrong state */
		return EPIPE;
	}

	/* register client at server_fd */
	thr->ipc.fd = server_fd;

	/* is there a server already waiting? */
	if (!list_is_empty(&server_fd->ipc_server.ipc_serverqh)) {
		/* yes, a server is waiting */
		node = list_remove_first(&server_fd->ipc_server.ipc_serverqh);
		assert(node != NULL);
		server_thr = THR_FROM_IPC_ANYQ(node);
		assert(server_thr->state == THREAD_STATE_WAIT_IPC_WAIT);

		/* generate new rhnd */
		/* NOTE: SMP: order writes to ipc.fd before ipc.rhnd */
		rhnd = new_rhnd(thr, server_thr->part);
		atomic_store_barrier();
		thr->ipc.rhnd = rhnd;

		/* message delivery */
		server_thr->cont_args.ipc_wait.recv_rhnd = rhnd;
		server_thr->ipc.msg = thr->ipc.msg;

		/* wake up server thread */
		sched_lock(server_thr->sched);
		thread_wakeup(server_thr);
		server_thr->wakeup_code = EOK;
		thread_cancel_clear(server_thr);
		sched_unlock(server_thr->sched);

		/* current thread now waits for IPC reply */
		ipc_qh = &server_fd->ipc_server.ipc_replyqh;
		thr_state = THREAD_STATE_WAIT_IPC_REPLY;
	} else {
		/* no server waiting */
		/* current thread now waits for IPC call */
		ipc_qh = &server_fd->ipc_server.ipc_callqh;
		thr_state = THREAD_STATE_WAIT_IPC_SEND;

		epoll_notify(server_fd, EPOLLIN);
	}

	/* common waiting */
	list_insert_last(ipc_qh, &thr->ipc.ipc_anyql);

	sched_lock(thr->sched);
	thread_wait(thr, thr_state, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
	thread_continue_at(thr, ipc_wait_finish);
	thr->wakeup_code = ETIMEDOUT;
	thread_cancel_at(thr, ipc_wait_cancel);
	sched_unlock(thr->sched);

	fd_unlock(server_fd);

	/* ETIMEDOUT indicates unfinished IPC (invalid error code for IPC) */
	return ETIMEDOUT;
}

static err_t ipc_reply(
	ulong_t send_rhnd,
	const struct sys_ipc *send_msg_user)
{
	struct thread *client_thr;
	struct fd *server_fd;
	struct thread *thr;
	err_t err;

	thr = current_thread();
	err = user_copy_in_aligned(&thr->ipc.msg, send_msg_user, sizeof(thr->ipc.msg));
	if (err != EOK) {
		/* copy-in failed */
		return err;
	}

	client_thr = check_rhnd(current_part(), send_rhnd);
	if (client_thr == NULL) {
		/* invalid reply handle */
		return ESRCH;
	}

	// FIXME: SMP: serialization with part_lock required, see ipc_copy()

	/* get server_fd from client thread */
	/* NOTE: SMP: order loads from ipc.fd before ipc.rhnd */
	atomic_load_barrier();
	server_fd = client_thr->ipc.fd;
	atomic_load_barrier();

	/* NOTE: SMP: double check */
	if (client_thr->ipc.rhnd != send_rhnd) {
		/* invalid reply handle (changed in the mean time) */
		return ESRCH;
	}

	/* NOTE: SMP: server_fd must be a valid pointer */
	assert(server_fd != NULL);
	fd_lock(server_fd);

	/* NOTE: SMP: triple check (fd_lock() acts as another load barrier) */
	if ((client_thr->ipc.rhnd != send_rhnd) ||
	        (client_thr->state != THREAD_STATE_WAIT_IPC_REPLY)) {
		/* invalid reply handle or thread state */
		fd_unlock(server_fd);
		return ESRCH;
	}

	/* NOTE: SMP: now we are sure that the client is involved in this IPC */
	assert(server_fd->type == FD_TYPE_IPC_SERVER);

	/* message delivery */
	client_thr->ipc.msg = thr->ipc.msg;
	client_thr->ipc.msg.flags = client_thr->ipc.reply_flags;
	if (client_thr->ipc.reply_flags != 0) {
		/* overwrite h64 only when necessary */
		client_thr->ipc.msg.h64 = client_thr->ipc.reply.h64;
	}

	/* invalidate the reply handle and remove from IPC queue */
	client_thr->ipc.rhnd = 0;
	list_remove(&client_thr->ipc.ipc_anyql);

	/* wake up client thread */
	sched_lock(client_thr->sched);
	thread_wakeup(client_thr);
	client_thr->wakeup_code = EOK;
	thread_cancel_clear(client_thr);
	sched_unlock(client_thr->sched);

	fd_unlock(server_fd);

	return EOK;
}

static err_t ipc_wait(
	ulong_t fd_index,
	uint32_t *recv_rhnd_user,
	struct sys_ipc *recv_msg_user)
{
	struct thread *client_thr;
	struct fd *server_fd;
	struct thread *thr;
	uint32_t rhnd;
	list_t *node;
	err_t err;

	/* get and lock used fd */
	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_IPC_SERVER, &server_fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	/* is there a client already waiting? */
	if (!list_is_empty(&server_fd->ipc_server.ipc_callqh)) {
		/* yes, a client is waiting */
		node = list_remove_first(&server_fd->ipc_server.ipc_callqh);
		assert(node != NULL);
		client_thr = THR_FROM_IPC_ANYQ(node);
		assert(client_thr->state == THREAD_STATE_WAIT_IPC_SEND);

		/* generate new rhnd */
		/* NOTE: SMP: order writes to ipc.fd before ipc.rhnd */
		rhnd = new_rhnd(client_thr, current_part());
		atomic_store_barrier();
		client_thr->ipc.rhnd = rhnd;

		/* put client thread on reply waiting queue */
		list_insert_last(&server_fd->ipc_server.ipc_replyqh, &client_thr->ipc.ipc_anyql);

		/* message delivery */
		thr = current_thread();
		thr->cont_args.ipc_wait.recv_rhnd = rhnd;
		thr->ipc.msg = client_thr->ipc.msg;

		/* change waiting state for client thread */
		sched_lock(client_thr->sched);
		assert(client_thr->state == THREAD_STATE_WAIT_IPC_SEND);
		client_thr->state = THREAD_STATE_WAIT_IPC_REPLY;
		sched_unlock(client_thr->sched);

		if (list_is_empty(&server_fd->ipc_server.ipc_callqh)) {
			epoll_notify(server_fd, 0);
		}

		fd_unlock(server_fd);

		err = user_copy_out_aligned(recv_msg_user, &thr->ipc.msg, sizeof(thr->ipc.msg));
		if ((err == EOK) && (recv_rhnd_user != NULL)) {
			err = user_put_4(recv_rhnd_user, rhnd);
		}

		return EOK;
	} else {
		/* no client waiting */
		if (server_fd->ipc_server.client_fd == NULL) {
			/* the client side was already closed, do not block */
			fd_unlock(server_fd);
			return EPIPE;
		}

		/* arguments for deferred IPC receive */
		thr = current_thread();
		thr->cont_args.ipc_wait.recv_msg_user = recv_msg_user;
		thr->cont_args.ipc_wait.recv_rhnd_user = recv_rhnd_user;
		thr->cont_args.ipc_wait.recv_rhnd = 0;

		/* current server thread now waits for next IPC call */
		thr->ipc.fd = server_fd;
		list_insert_last(&server_fd->ipc_server.ipc_serverqh, &thr->ipc.ipc_anyql);

		sched_lock(thr->sched);
		thread_wait(thr, THREAD_STATE_WAIT_IPC_WAIT, TIMEOUT_INFINITE, CLK_ID_MONOTONIC);
		thread_continue_at(thr, ipc_wait_finish);
		thr->wakeup_code = ETIMEDOUT;
		thread_cancel_at(thr, ipc_wait_cancel);
		sched_unlock(thr->sched);

		fd_unlock(server_fd);

		/* ETIMEDOUT indicates unfinished IPC (invalid error code for IPC) */
		return ETIMEDOUT;
	}
}

err_t sys_ipc_reply_wait(
	ulong_t send_rhnd,
	const struct sys_ipc *send_msg_user,
	ulong_t fd_index,
	uint32_t *recv_rhnd_user,
	struct sys_ipc *recv_msg_user)
{
	err_t err;

	if ((send_rhnd != 0) && (send_msg_user != NULL)) {
		err = ipc_reply(send_rhnd, send_msg_user);
		if (err != EOK) {
			/* IPC reply failed */
			return err;
		}
	}

	if (recv_msg_user != NULL) {
		return ipc_wait(fd_index, recv_rhnd_user, recv_msg_user);
	}

	return EOK;
}

/** internal IPC copy routine */
static err_t ipc_copy(
	struct adspace *adspace,
	struct sys_iov *iov,
	size_t iov_num,
	int write,
	addr_t buf_user,
	size_t size,
	size_t offset,
	size_t *transfer_size)
{
	size_t chunk;
	err_t err;

	assert((iov_num >= 1) && (iov_num <= SYS_IOV_MAX));
	assert(transfer_size != NULL);

	*transfer_size = 0;

	/* move to start offset in vector */
	while (offset >= iov->size) {
		offset -= iov->size;
		iov++;
		iov_num--;
		if (iov_num == 0) {
			return EOK;
		}
	}
	assert(offset < iov->size);
	iov->addr = (void*)((addr_t)iov->addr + offset);
	iov->size -= offset;

	/* copy in chunks */
	while (size > 0) {
		chunk = size;
		while (iov->size == 0) {
			iov++;
			iov_num--;
			if (iov_num == 0) {
				return EOK;
			}
		}
		if (chunk > iov->size) {
			chunk = iov->size;
		}
		assert(chunk > 0);

		if (write) {
			err = arch_adspace_write(adspace, (addr_t)iov->addr, buf_user, chunk, 0);
		} else {
			err = arch_adspace_read(adspace, buf_user, (addr_t)iov->addr, chunk);
		}
		if (err != EOK) {
			return err;
		}

		*transfer_size += chunk;
		buf_user += chunk;
		iov->addr = (void*)((addr_t)iov->addr + chunk);
		iov->size -= chunk;
		size -= chunk;
	}

	return EOK;
}

err_t sys_ipc_read(
	ulong_t rhnd,
	ulong_t id,
	void *buf_user,
	size_t size,
	size_t offset,
	size_t *read_size_user)
{
	struct thread *client_thr;
	struct process *remote_proc;
	struct adspace *adspace;
	size_t transfer_size;
	struct thread *thr;
	unsigned int flag;
	size_t iov_num;
	err_t err;

	if (id >= 3) {
		/* invalid buffer ID selected */
		return EINVAL;
	}
	if (!USER_CHECK_RANGE((addr_t)buf_user, size)) {
		/* not valid user space region */
		return EINVAL;
	}

	thr = current_thread();

	client_thr = check_rhnd(current_part(), rhnd);
	if (client_thr == NULL) {
		/* invalid reply handle */
		return ESRCH;
	}

	remote_proc = client_thr->process;

	/* prevent the thread from going away */
	process_lock(remote_proc);
	/* NOTE: SMP: double check */
	if (client_thr->ipc.rhnd != rhnd) {
		/* invalid reply handle (changed in the mean time) */
		process_unlock(remote_proc);
		return ESRCH;
	}

	assert(SYS_IPC_BUF1R == (SYS_IPC_BUF0R << 2));
	assert(SYS_IPC_BUF2R == (SYS_IPC_BUF0R << 4));
	flag = SYS_IPC_BUF0R << (2 * id);
	if ((client_thr->ipc.msg.flags & flag) == 0) {
		/* client did not allow the memory access */
		process_unlock(remote_proc);
		return ESTATE;
	}

	if ((id == 0) && ((client_thr->ipc.msg.flags & SYS_IPC_IOV) != 0)) {
		/* vector mode */
		iov_num = client_thr->ipc.iov_num;
		assert((iov_num >= 1) && (iov_num <= SYS_IOV_MAX));
		for (size_t i = 0; i < iov_num; i++) {
			thr->ipc.vec[i] = client_thr->ipc.vec[i];
		}
	} else if (id == 0) {
		/* buffer mode */
		thr->ipc.vec[0].addr = client_thr->ipc.msg.buf0;
		thr->ipc.vec[0].size = client_thr->ipc.msg.size0;
		iov_num = 1;
	} else if (id == 1) {
		thr->ipc.vec[0].addr = client_thr->ipc.msg.buf1;
		thr->ipc.vec[0].size = client_thr->ipc.msg.size1;
		iov_num = 1;
	} else {
		thr->ipc.vec[0].addr = client_thr->ipc.msg.buf2;
		thr->ipc.vec[0].size = client_thr->ipc.msg.size2;
		iov_num = 1;
	}

	adspace = remote_proc->adspace;

	process_unlock(remote_proc);

	/* From now on, we cannot guarantee that the client thread stays
	 * in REPLY state. We ignore any semantic problems and copy anyway.
	 */
	err = ipc_copy(adspace, thr->ipc.vec, iov_num, 0, (addr_t)buf_user, size, offset, &transfer_size);

	if ((err == EOK) && (read_size_user != NULL)) {
		err = user_put_p(read_size_user, transfer_size);
	}

	return err;
}

/* NOTE: sys_ipc_write() lacks the "const" for buf_user found in the user API */
err_t sys_ipc_write(
	ulong_t rhnd,
	ulong_t id,
	void *buf_user,
	size_t size,
	size_t offset,
	size_t *written_size_user)
{
	struct thread *client_thr;
	struct process *remote_proc;
	struct adspace *adspace;
	size_t transfer_size;
	struct thread *thr;
	unsigned int flag;
	size_t iov_num;
	err_t err;

	if (id >= 3) {
		/* invalid buffer ID selected */
		return EINVAL;
	}
	if (!USER_CHECK_RANGE((addr_t)buf_user, size)) {
		/* not valid user space region */
		return EINVAL;
	}

	thr = current_thread();

	client_thr = check_rhnd(current_part(), rhnd);
	if (client_thr == NULL) {
		/* invalid reply handle */
		return ESRCH;
	}

	remote_proc = client_thr->process;

	/* prevent the thread from going away */
	process_lock(remote_proc);
	/* NOTE: SMP: double check */
	if (client_thr->ipc.rhnd != rhnd) {
		/* invalid reply handle (changed in the mean time) */
		process_unlock(remote_proc);
		return ESRCH;
	}

	assert(SYS_IPC_BUF1W == (SYS_IPC_BUF0W << 2));
	assert(SYS_IPC_BUF2W == (SYS_IPC_BUF0W << 4));
	flag = SYS_IPC_BUF0W << (2 * id);
	if ((client_thr->ipc.msg.flags & flag) == 0) {
		/* client did not allow the memory access */
		process_unlock(remote_proc);
		return ESTATE;
	}

	if ((id == 0) && ((client_thr->ipc.msg.flags & SYS_IPC_IOV) != 0)) {
		/* vector mode */
		iov_num = client_thr->ipc.iov_num;
		assert((iov_num >= 1) && (iov_num <= SYS_IOV_MAX));
		for (size_t i = 0; i < iov_num; i++) {
			thr->ipc.vec[i] = client_thr->ipc.vec[i];
		}
	} else if (id == 0) {
		/* buffer mode */
		thr->ipc.vec[0].addr = client_thr->ipc.msg.buf0;
		thr->ipc.vec[0].size = client_thr->ipc.msg.size0;
		iov_num = 1;
	} else if (id == 1) {
		thr->ipc.vec[0].addr = client_thr->ipc.msg.buf1;
		thr->ipc.vec[0].size = client_thr->ipc.msg.size1;
		iov_num = 1;
	} else {
		thr->ipc.vec[0].addr = client_thr->ipc.msg.buf2;
		thr->ipc.vec[0].size = client_thr->ipc.msg.size2;
		iov_num = 1;
	}

	adspace = remote_proc->adspace;

	process_unlock(remote_proc);

	/* From now on, we cannot guarantee that the client thread stays
	 * in REPLY state. We ignore any semantic problems and copy anyway.
	 */
	err = ipc_copy(adspace, thr->ipc.vec, iov_num, 1, (addr_t)buf_user, size, offset, &transfer_size);

	if ((err == EOK) && (written_size_user != NULL)) {
		err = user_put_p(written_size_user, transfer_size);
	}

	return err;
}

/** IPC server-side operation: setup mapping in IPC client address space */
err_t sys_ipc_sharemap(
	ulong_t rhnd,
	ulong_t flags_arg,
	ulong_t fd_index,
	phys_addr_t offset)
{
	const struct memrq_cfg *m;
	struct adspace *adspace;
	uint32_t max_prot;
	size_t aligned_size;
	addr_t map_addr;

	struct thread *client_thr;
	struct process *remote_proc;
	unsigned int map_flags;
	unsigned int prot;
	unsigned int fd_oflags;
	unsigned int m_prot;
	struct fd *fd;
	addr_t addr;
	size_t size;
	err_t err;

	if ((offset & (PAGE_SIZE - 1)) != 0) {
		/* not page aligned */
		return EINVAL;
	}
	if ((flags_arg & ~(MAP_TYPE |
	                   MAP_FIXED | MAP_EXCL |
	                   MAP_ALLOW_MASK | MAP_IPC_MASK)) != 0) {
		/* invalid flags */
		return EINVAL;
	}

	/* map memory requirement */
	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_MEMRQ, &fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	fd_oflags = fd->oflags;
	m = fd->file.memrq_cfg;
	fd_unlock(fd);

	/* consider fd->oflags and restrict prot */
	m_prot = m->prot;
	if ((fd_oflags & __O_READ) == 0) {
		m_prot &= ~PROT_READ;
	}
	if ((fd_oflags & __O_WRITE) == 0) {
		m_prot &= ~PROT_WRITE;
	}

	client_thr = check_rhnd(current_part(), rhnd);
	if (client_thr == NULL) {
		/* invalid reply handle */
		return ESRCH;
	}

	remote_proc = client_thr->process;

	/* prevent the thread from going away */
	process_lock(remote_proc);
	/* NOTE: SMP: double check */
	if (client_thr->ipc.rhnd != rhnd) {
		/* invalid reply handle (changed in the mean time) */
		err = ESRCH;
		goto out_unlock_part;
	}

	if ((client_thr->ipc.msg.flags & SYS_IPC_MAP) == 0) {
		/* client did not opt-in to receive a mapping */
		err = ESTATE;
		goto out_unlock_part;
	}

	if ((client_thr->ipc.reply_flags & SYS_IPC_MAP) != 0) {
		/* client already received a mapping */
		err = ESTATE;
		goto out_unlock_part;
	}

	/* get client's mapping parameters */
	map_flags = client_thr->ipc.msg.map_flags;
	prot = client_thr->ipc.msg.map_prot;
	addr = client_thr->ipc.msg.map_addr;
	size = client_thr->ipc.msg.map_size;

	/* include remapping flags specified by the caller */
	assert((map_flags & (MAP_SHARED | MAP_PRIVATE)) != 0);
	assert((map_flags & MAP_ALLOW_MASK) == 0);
	assert((map_flags & MAP_IPC_MASK) == 0);
	map_flags |= flags_arg & (MAP_ALLOW_MASK | MAP_IPC_MASK);
	max_prot = prot | ((map_flags & MAP_ALLOW_MASK) >> MAP_ALLOW_SHIFT);

	if (((max_prot | m_prot) & ~m_prot) != 0) {
		/* permissions not matching */
		err = EPERM;
		goto out_unlock_part;
	}

	aligned_size = ALIGN_UP(m->size, PAGE_SIZE);
	if ((size > aligned_size) || (offset > aligned_size - size)) {
		/* range exceeds memory requirement */
		err = ESTATE;
		goto out_unlock_part;
	}

	adspace = remote_proc->adspace;

	// FIXME: fix locking in mapping
	//adspace_lock(adspace);

	if (m->memrq != NULL) {
		err = vm_ref_map(adspace, addr, size, prot, max_prot, m, offset, map_flags, &map_addr);
	} else {
		err = vm_map(adspace, addr, size, prot, max_prot, m, offset, map_flags, &map_addr);
	}

	//adspace_unlock(adspace);

	if (err == EOK) {
		client_thr->ipc.reply_flags |= SYS_IPC_MAP;
		client_thr->ipc.reply.map_start = map_addr;
	}

	/* error path */
out_unlock_part:
	process_unlock(remote_proc);

	return err;
}

/** IPC server-side operation: share file descriptor with IPC client */
err_t sys_ipc_sharefd(
	ulong_t rhnd,
	ulong_t fd_index,
	ulong_t flags)
{
	unsigned int remote_fd_index;
	struct thread *client_thr;
	struct process *remote_proc;
	struct fd *fd;
	int cloexec;
	err_t err;

	if ((flags & ~(SYS_IPC_CLOSE)) != 0) {
		/* invalid flags */
		return EINVAL;
	}

	/* NOTE: SMP: To transfer a file descriptor in a perfect world,
	 * we would have to lock both the current partition and the target partition
	 * (which can be the same partition) in ascending order,
	 * then lock the file descriptor.
	 *
	 * We therefore simplify the locking as follows:
	 * 1. We lock the file descriptor and increment its usage counter.
	 * 2. We lock the target partition and install the new file descriptor.
	 * 3. In case something goes wrong, we lock and decrement the counter again.
	 */

	/* get and lock first fd */
	fd = fd_get_and_lock(current_process(), fd_index);
	if (fd == NULL) {
		/* invalid file descriptor */
		return EBADF;
	}
	if ((fd->flags & FD_FLAG_SHAREABLE) == 0) {
		fd_unlock(fd);
		return EINVAL;
	}
	fd->usage++;
	fd_unlock(fd);

	client_thr = check_rhnd(current_part(), rhnd);
	if (client_thr == NULL) {
		/* invalid reply handle */
		err = ESRCH;
		goto out_decrement_fd;
	}

	remote_proc = client_thr->process;

	/* prevent the thread from going away */
	process_lock(remote_proc);
	/* NOTE: SMP: double check */
	if (client_thr->ipc.rhnd != rhnd) {
		/* invalid reply handle (changed in the mean time) */
		err = ESRCH;
		goto out_unlock_part_decrement_fd;
	}

	/* check for FD0 */
	if ((client_thr->ipc.msg.flags & SYS_IPC_FD0) == 0) {
		/* client did not opt-in to receive a file descriptor */
		err = ESTATE;
		goto out_unlock_part_decrement_fd;
	}

	if ((client_thr->ipc.reply_flags & SYS_IPC_FD0) != 0) {
		/* client already received a file descriptor */

		/* check for FD1 */
		if ((client_thr->ipc.msg.flags & SYS_IPC_FD1) == 0) {
			/* client did not opt-in to receive a second file descriptor */
			err = ESTATE;
			goto out_unlock_part_decrement_fd;
		}

		if ((client_thr->ipc.reply_flags & SYS_IPC_FD1) != 0) {
			/* client already received a second file descriptor */
			err = ESTATE;
			goto out_unlock_part_decrement_fd;
		}
	}

	cloexec = (client_thr->ipc.msg.flags & SYS_IPC_FD_CLOEXEC) != 0;
	err = fd_dup_locked(remote_proc, fd, cloexec, &remote_fd_index);
	if (err != EOK) {
		/* failure */
		assert(err == EMFILE);
		goto out_unlock_part_decrement_fd;
	}

	/* update reply message */
	if ((client_thr->ipc.reply_flags & SYS_IPC_FD0) == 0) {
		client_thr->ipc.reply_flags |= SYS_IPC_FD0;
		client_thr->ipc.reply.fd0 = remote_fd_index;
	} else {
		client_thr->ipc.reply_flags |= SYS_IPC_FD1;
		client_thr->ipc.reply.fd1 = remote_fd_index;
	}

	process_unlock(remote_proc);

	/* close source file descriptor in caller's partition */
	if ((flags & SYS_IPC_CLOSE) != 0) {
		(void)sys_fd_close(fd_index);
	}

	assert(err == EOK);
	return err;

	/* error path */
out_unlock_part_decrement_fd:
	process_unlock(remote_proc);

	/* error path: decrement fd use counters again */
out_decrement_fd:
	fd_close(fd);

	return err;
}

err_t sys_ipc_notify(
	ulong_t fd_index,
	ulong_t eventmask)
{
	struct fd *server_fd;
	struct fd *client_fd;
	err_t err;

	if ((eventmask & ~EPOLL_MASK_IPC_NOTIFY_EVENTS) != 0) {
		/* invalid events */
		return EINVAL;
	}

	/* get and lock used fd */
	err = fd_get_type_and_lock(current_process(), fd_index, FD_TYPE_IPC_SERVER, &server_fd);
	if (err != EOK) {
		/* invalid file descriptor OR invalid file type */
		return err;
	}

	client_fd = server_fd->ipc_server.client_fd;
	fd_unlock(server_fd);

	if (client_fd == NULL) {
		return EPIPE;
	}

	fd_lock(client_fd);

	if (client_fd->type != FD_TYPE_IPC_CLIENT) {
		/* wrong file type */
		err = EINVAL;
		goto out_unlock_client_fd;
	}

	if (client_fd->ipc_client.server_fd != server_fd) {
		err = EINVAL;
		goto out_unlock_client_fd;
	}

	epoll_notify(client_fd, eventmask);

	err = EOK;

out_unlock_client_fd:
	fd_unlock(client_fd);

	return err;
}
