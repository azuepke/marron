# SPDX-License-Identifier: MIT
# Copyright 2013, 2017 Alexander Zuepke
# cortex-a65.mk -- for Cortex-A65 (64-bit)
#
# ARM architecture specific build rules
#
# azuepke, 2017-09-20: imported

_ARCH := -mcpu=cortex-a76 # FIXME if GCC starts to support this!
_MODE := $(call cc-option,-mno-outline-atomics,)
_DEFS := -DARM_V8 -DARM_CORTEX_A65

ARCH_CFLAGS := $(_ARCH) $(_MODE) $(_DEFS) -mgeneral-regs-only

ARCH_CFLAGS_DEBUG := -Og
ARCH_CFLAGS_NDEBUG := -O2 -fomit-frame-pointer

ARCH_AFLAGS := $(_ARCH) $(_MODE) $(_DEFS)
ARCH_LDFLAGS := -maarch64linux

ARCH_MODS := entry64 exception64 adspace64 string64


# Recommended user compiler and linker flags
ARCH_USER_CFLAGS := $(_ARCH) $(_MODE)
ARCH_USER_AFLAGS := $(_ARCH) $(_MODE)
ARCH_USER_LDFLAGS := -maarch64linux
