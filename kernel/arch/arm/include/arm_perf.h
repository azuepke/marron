/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * arm_perf.h
 *
 * ARM performance monitoring.
 *
 * azuepke, 2014-03-03: initial
 */

#ifndef ARM_PERF_H_
#define ARM_PERF_H_

#ifdef __aarch64__
#include <arm_perf_v8.h>
#elif defined ARM_V6
#include <arm_perf_v6.h>
#else
#include <arm_perf_v7.h>
#endif

#endif
