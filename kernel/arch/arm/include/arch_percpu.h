/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Alexander Zuepke */
/*
 * arch_percpu.h
 *
 * Getter to per-CPU state.
 *
 * azuepke, 2018-03-05: initial
 */

#ifndef ARCH_PERCPU_H_
#define ARCH_PERCPU_H_

#include <arm_insn.h>

/* forward declarations */
struct percpu;

/** get pointer to per-CPU data */
static inline struct percpu *arch_percpu(void)
{
	return (struct percpu *)arm_get_tls2();
}

#endif
