/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2020 Alexander Zuepke */
/*
 * arch_spin.h
 *
 * ARM ticket spin locks
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef ARCH_SPIN_H_
#define ARCH_SPIN_H_

#include <arch_spin_types.h>
#include <marron/compiler.h>
#include <arm_insn.h>


/* increments */
#define ARCH_TICKET_INC 0x01000000
#define ARCH_SERVED_INC 0x00010000

/** spin lock initializer */
static inline void arch_spin_init(arch_spin_t *lock)
{
	lock->lock = 0;
}

/** try to lock spin lock, returns TRUE on success */
static inline int arch_spin_trylock(arch_spin_t *lock)
{
	uint32_t tmp, tmp2;
	arch_spin_t val;

#ifdef __aarch64__
	__asm__ volatile (
		"1:	ldaxr	%w0, %3			\n"
		"	eor		%w1, %w0, %w0, ror #24\n"
		"	and		%w1, %w1, #0xff000000\n"
		"	cbnz	%w1, 2f			\n"
		"	add		%w2, %w0, %w4	\n"
		"	stxr	%w1, %w2, %3	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w1, 1b			\n"
		"2:"
		: "=&r"(val), "=&r"(tmp), "=&r"(tmp2), "+Q"(*lock) : "Ir"(ARCH_TICKET_INC) : "memory", "cc");
#else
	__asm__ volatile (
		"1:	ldrex	%0, %3			\n"
		"	eor		%1, %0, %0, ror #24\n"
		"	and		%1, %1, #0xff000000\n"
		"	teq		%1, #0			\n"
		"	bne		2f				\n"
		"	add		%2, %0, %4		\n"
		"	strex	%1, %2, %3		\n"	/* returns 0 on success, 1 on failure */
		"	teq		%1, #1			\n"
		"	beq		1b				\n"
		"	dmb						\n"
		"2:"
		: "=&r"(val), "=&r"(tmp), "=&r"(tmp2), "+Q"(*lock) : "I"(ARCH_TICKET_INC) : "memory", "cc");
#endif

	/* memory barriers included above */
	return (tmp == 0);
}

/** lock spin lock */
static inline void arch_spin_lock(arch_spin_t *lock)
{
	uint32_t tmp, tmp2;
	arch_spin_t val;

#ifdef __aarch64__
	__asm__ volatile (
		"1:	ldaxr	%w0, %3			\n"
		"	add		%w2, %w0, %w4	\n"
		"	stxr	%w1, %w2, %3	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w1, 1b			\n"
		: "=&r"(val), "=&r"(tmp), "=&r"(tmp2), "+Q"(*lock) : "Ir"(ARCH_TICKET_INC) : "memory", "cc");
#else
	__asm__ volatile (
		"1:	ldrex	%0, %3			\n"
		"	add		%2, %0, %4		\n"
		"	strex	%1, %2, %3		\n"	/* returns 0 on success, 1 on failure */
		"	teq		%1, #1			\n"
		"	beq		1b				\n"
		: "=&r"(val), "=&r"(tmp), "=&r"(tmp2), "+Q"(*lock) : "Ir"(ARCH_TICKET_INC) : "memory", "cc");
#endif

	while (val.s.ticket != val.s.served) {
		arm_yield();
#ifdef __aarch64__
		__asm__ volatile (
			"	ldaxrb	%w0, %1			\n"
			: "=&r"(val.s.served) : "Q"(lock->s.served) : "memory");

#else
		val.s.served = access_once(lock->s.served);
#endif
	}

#ifdef __aarch64__
	/* ldaxr is a load-acquire barrier */
#else
	arm_dmb();
#endif
}

/** unlock spin lock */
static inline void arch_spin_unlock(arch_spin_t *lock)
{
#ifdef __aarch64__
	/* stlrb is a store-release barrier */
	__asm__ volatile (
		"	stlrb	%w1, %0			\n"
		: "=Q"(lock->s.served) : "r"(lock->s.served + 1) : "memory");
#else
	arm_dmb();
	lock->s.served++;
#endif
}

#endif
