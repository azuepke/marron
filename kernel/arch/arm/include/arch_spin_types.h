/* SPDX-License-Identifier: MIT */
/* Copyright 2020 Alexander Zuepke */
/*
 * arch_spin_types.h
 *
 * ARM ticket spin locks
 *
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef ARCH_SPIN_TYPES_H_
#define ARCH_SPIN_TYPES_H_

#include <stdint.h>

/** spin lock data type */
typedef union {
	uint32_t lock;
	struct {
		/* little endian order: ticket is as MSB position */
		uint16_t owner_cpu;
		uint8_t served;
		uint8_t ticket;
	} s;
} arch_spin_t;

/** invalid owner CPU (beyond 256 CPUs) */
#define ARCH_SPIN_INVALID_OWNER_CPU 0x100

/** spin lock static initializer */
#define ARCH_SPIN_INIT { 0 }
#define ARCH_SPIN_INIT_DEBUG { ARCH_SPIN_INVALID_OWNER_CPU << 0 }

#endif
