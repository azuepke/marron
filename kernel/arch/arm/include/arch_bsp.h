/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017 Alexander Zuepke */
/*
 * arch_bsp.h
 *
 * Architecture abstraction layer.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2017-09-29: imported and adapted
 */

#ifndef ARCH_BSP_H_
#define ARCH_BSP_H_

#include <stdint.h>

/** private interface between architecture layer and BSP */
struct arch_bsp {
	/* unused */
};

#endif
