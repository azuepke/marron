/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2017, 2021 Alexander Zuepke */
/*
 * arm_insn.h
 *
 * ARM architecture specific instructions.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-14: split into two files and added 64-bit support
 * azuepke, 2017-09-29: imported and merged again
 */

#ifndef ARM_INSN_H_
#define ARM_INSN_H_

#include <arm_cr.h>

/** Thumb ITE block macros for 32-bit ARM */
#ifdef __thumb__
#define THUMB_IT(c)			"it		" c ";\n"
#define THUMB_ITE(c)		"ite	" c ";\n"
#define THUMB_ITEE(c)		"itee	" c ";\n"
#define THUMB_ITEEE(c)		"iteee	" c ";\n"
#define THUMB_ITT(c)		"itt	" c ";\n"
#define THUMB_ITTE(c)		"itte	" c ";\n"
#define THUMB_ITTEE(c)		"ittee	" c ";\n"
#define THUMB_ITTT(c)		"ittt	" c ";\n"
#define THUMB_ITTTE(c)		"ittte	" c ";\n"
#define THUMB_ITTTT(c)		"itttt	" c ";\n"
#else
#define THUMB_IT(c)			";\n"
#define THUMB_ITE(c)		";\n"
#define THUMB_ITEE(c)		";\n"
#define THUMB_ITEEE(c)		";\n"
#define THUMB_ITT(c)		";\n"
#define THUMB_ITTE(c)		";\n"
#define THUMB_ITTEE(c)		";\n"
#define THUMB_ITTT(c)		";\n"
#define THUMB_ITTTE(c)		";\n"
#define THUMB_ITTTT(c)		";\n"
#endif

/** synchronization barriers */
#define _arm_dsb(o)	__asm__ volatile ("dsb " o : : : "memory")
#define _arm_dmb(o)	__asm__ volatile ("dmb " o : : : "memory")

#define arm_isb()	__asm__ volatile ("isb" : : : "memory")
#define arm_dsb()	__asm__ volatile ("dsb SY" : : : "memory")
#define arm_dmb()	__asm__ volatile ("dmb SY" : : : "memory")

#define arm_clrex()	__asm__ volatile ("clrex" : : : "memory")
#define arm_yield()	__asm__ volatile ("yield" : : : "memory")
#define arm_sev()	__asm__ volatile ("sev" : : : "memory")
#define arm_sevl()	__asm__ volatile ("sevl" : : : "memory")
#define arm_wfe()	__asm__ volatile ("wfe" : : : "memory")
#define arm_wfi()	__asm__ volatile ("wfi" : : : "memory")
#define arm_csdb()	__asm__ volatile ("hint #20" : : : "memory")


#ifdef __aarch64__

/** Set TTBR0_EL1 (user page table) + ASID */
/* NOTE: requires special synchronization! */
static inline void arm_set_ttbr0_lpae(unsigned long addr, unsigned long asid)
{
	unsigned long val = (asid << 48) | addr;
	__asm__ volatile ("msr TTBR0_EL1, %0" : : "r"(val) : "memory");
}

#else /* 32-bit ARM */

/** Set CONTEXT ID (ASID) */
/* NOTE: requires special synchronization! */
static inline void arm_set_asid(unsigned long val)
{
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 1" : : "r"(val) : "memory");
}

/** Set TTBR0 (user page table) */
/* NOTE: requires special synchronization! */
static inline void arm_set_ttbr0(unsigned long val)
{
	__asm__ volatile ("mcr p15, 0, %0, c2, c0, 0" : : "r"(val) : "memory");
}

#endif

/** Set VBAR_EL1 (exception vector base register) */
static inline void arm_set_vbar(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr VBAR_EL1, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c12, c0, 0" : : "r"(val) : "memory");
#endif
}

/** Enable exceptions (but not interrupts) */
static inline void arm_enable_exceptions(void)
{
#ifdef __aarch64__
	__asm__ volatile ("msr DAIFCLR, #0xd" : : : "memory");
#else
	__asm__ volatile ("cpsie af" : : : "memory");
#endif
}

/** Get MPIDR (multiprocessor ID register, for MPCore) */
static inline unsigned long arm_get_mpidr(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, MPIDR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c0, c0, 5" : "=r"(val));
#endif
	return val;
}

/** clean a data cache line */
static inline void arm_clean_dcache(void *addr)
{
#ifdef __aarch64__
	__asm__ volatile ("dc CVAC, %0" : : "r"(addr) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c7, c10, 1" : : "r"(addr) : "memory");
#endif
}


/** Get TPIDRURW / TPIDR_EL0 (user writable context register) */
static inline unsigned long arm_get_tls0(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, TPIDR_EL0" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c13, c0, 2" : "=r"(val));
#endif
	return val;
}

/** Set TPIDRURW / TPIDR_EL0 (user writable context register) */
static inline void arm_set_tls0(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr TPIDR_EL0, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 2" : : "r"(val) : "memory");
#endif
}

/** Get TPIDRURO / TPIDRRO_EL0 (user readable context register) */
static inline unsigned long arm_get_tls1(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, TPIDRRO_EL0" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c13, c0, 3" : "=r"(val));
#endif
	return val;
}

/** Set TPIDRURO / TPIDRRO_EL0 (user readable context register) */
static inline void arm_set_tls1(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr TPIDRRO_EL0, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 3" : : "r"(val) : "memory");
#endif
}


/** Get TPIDRPRW / TPIDR_EL1 (user inaccessible context register) */
static inline unsigned long arm_get_tls2(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, TPIDR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c13, c0, 4" : "=r"(val));
#endif
	return val;
}

/** Set TPIDRPRW / TPIDR_EL1 (user inaccessible context register) */
static inline void arm_set_tls2(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr TPIDR_EL1, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 4" : : "r"(val) : "memory");
#endif
}


#ifdef __arm__

/** get FPEXC (32-bit only) */
static inline unsigned long arm_get_fpexc(void)
{
	unsigned long val;
	__asm__ volatile ("mrc p10, 7, %0, c8, c0, 0" : "=r"(val));
	return val;
}

/** set FPEXC (32-bit only) */
static inline void arm_set_fpexc(unsigned long val)
{
	__asm__ volatile ("mcr p10, 7, %0, c8, c0, 0" : : "r"(val) : "memory");
	arm_isb();
}

#endif

#ifdef __aarch64__

/** Set CPACR (FPU access, 64-bit only) */
static inline void arm_set_cpacr(unsigned long val)
{
	__asm__ volatile ("msr CPACR_EL1, %0" : : "r"(val) : "memory");
}

/** Set counter access */
static inline void arm_set_cnt_access(unsigned long val)
{
	__asm__ volatile ("msr CNTKCTL_EL1, %0" : : "r"(val) : "memory");
}

/* Get Monitor Debug System Control Register */
static inline unsigned int arm_get_mdscr(void)
{
	unsigned int val;
	__asm__ volatile ("mrs %0, MDSCR_EL1" : "=r"(val));
	return val;
}

/* Set Monitor Debug System Control Register */
static inline void arm_set_mdscr(unsigned int val)
{
	__asm__ volatile ("msr MDSCR_EL1, %0" : : "r"(val) : "memory");
}

/* Get MDSCR.SS (single stepping) bit */
#define arm_get_mdscr_ss() (arm_get_mdscr() & MDSCR_SS)

/* Update MDSCR.SS (single stepping) bit */
static inline void arm_update_mdscr_ss(unsigned int ss)
{
	unsigned int mdscr;
	mdscr = arm_get_mdscr();
	if (((ss ^ mdscr) & MDSCR_SS) != 0) {
		/* MDSCR.SS bit flipped */
		mdscr ^= MDSCR_SS;
		arm_set_mdscr(mdscr);
	}
}

#endif

/** invalidate entire TLB */
static inline void arm_tlb_inval_all_locally(void)
{
#ifdef __aarch64__
	__asm__ volatile ("tlbi VMALLE1" : : : "memory");
#else /* 32-bit ARM */
	/* TLBIALL (no inner-shareable, since it targets the local TLB only) */
	__asm__ volatile ("mcr p15, 0, %0, c8, c7, 0" : : "r"(0) : "memory");
#endif
	_arm_dsb("ISH");
}

/** invalidate unified TLB by ASID */
static inline void arm_tlb_inval_asid(unsigned long asid)
{
#ifdef __aarch64__
	unsigned long val = (asid << 48);
	__asm__ volatile ("tlbi ASIDE1IS, %0" : : "r"(val) : "memory");
#else /* 32-bit ARM */
#ifdef ARM_CORTEX_A8
	/* TLBIASID */
	__asm__ volatile ("mcr p15, 0, %0, c8, c7, 2" : : "r"(asid) : "memory");
#else
	/* TLBIASIDIS */
	__asm__ volatile ("mcr p15, 0, %0, c8, c3, 2" : : "r"(asid) : "memory");
#endif
#endif
	_arm_dsb("ISH");
}

/** invalidate unified TLB entry by MVA, non last-level (format: asid << 48 | vaddr) */
/* NOTE: this uses the non-last-level form and invalidates TLB walk caches as well */
static inline void arm_tlb_inval_table(unsigned long addr, unsigned long asid)
{
#ifdef __aarch64__
	unsigned long val = (asid << 48) | (addr >> 12);
	__asm__ volatile ("tlbi VAE1IS, %0" : : "r"(val) : "memory");
	_arm_dsb("ISH");
#else
	/* not needed on 32-bit ARM or on LPAE */
	(void)addr;
	(void)asid;
#endif
}

/** invalidate unified TLB entry by MVA (format: asid << 48 | vaddr) */
static inline void arm_tlb_inval_page(unsigned long addr, unsigned long asid)
{
#ifdef __aarch64__
	unsigned long val = (asid << 48) | (addr >> 12);
	__asm__ volatile ("tlbi VALE1IS, %0" : : "r"(val) : "memory");
#else /* 32-bit ARM */
	unsigned long val = addr | asid;
#ifdef ARM_CORTEX_A8
	/* TLBIMVA */
	__asm__ volatile ("mcr p15, 0, %0, c8, c7, 1" : : "r"(val) : "memory");
#else
	/* TLBIMVAIS */
	__asm__ volatile ("mcr p15, 0, %0, c8, c3, 1" : : "r"(val) : "memory");
#endif
#endif
	_arm_dsb("ISH");
}

/** invalidate unified TLB entry by MVA all ASID local CPU only */
static inline void arm_tlb_inval_page_locally(unsigned long addr)
{
#ifdef __aarch64__
	unsigned long val = (addr >> 12);
	__asm__ volatile ("tlbi VAALE1, %0" : : "r"(val) : "memory");
#else /* 32-bit ARM */
	unsigned long val = addr;
#ifdef ARM_CORTEX_A8
	/* TLBIMVA (all-ASID varant not available on Cortex-A8) */
	__asm__ volatile ("mcr p15, 0, %0, c8, c7, 1" : : "r"(val) : "memory");
#else
	/* TLBIMVAA (no inner-shareable, since it targets the local TLB only) */
	__asm__ volatile ("mcr p15, 0, %0, c8, c7, 3" : : "r"(val) : "memory");
#endif
#endif
	_arm_dsb("ISH");
}


#ifdef __arm__
/** branch predictor invalidate all */
static inline void arm_bpiall(void)
{
	/* BPIALL */
	__asm__ volatile ("mcr p10, 7, %0, c7, c5, 6" : : "r"(0) : "memory");
}
#endif

#endif
