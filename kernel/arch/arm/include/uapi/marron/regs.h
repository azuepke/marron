/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2017-2018, 2021 Alexander Zuepke */
/*
 * marron/regs.h
 *
 * Register frame on ARM.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-09-29: imported and simplified
 * azuepke, 2018-03-16: split into arch_regs.h and arch_regs_types.h
 * azuepke, 2018-03-16: merge integer and FPU context into struct regs
 * azuepke, 2018-12-21: standalone version, rename to marron/regs.h
 */

#ifndef MARRON_REGS_H_
#define MARRON_REGS_H_

#include <marron/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/** exception register frame (user registers) */
struct regs {
#ifdef __aarch64__
	ulong_t r[30];
	ulong_t lr;		/* x30 */
	ulong_t sp;		/* x31 */
	ulong_t pc;
	ulong_t cpsr;	/* PSTATE */

	/* register frame saved up until here on exception entry */

	/* only valid for system calls */
	ulong_t orig_r0;	/* original x0 value on system call entry */

	/* saved on context switching */
	ulong_t tls0;	/* TPIDRURW */
	ulong_t tls1;	/* TPIDRURO */
	unsigned int mdscr;	/* only MDSCR_EL1.SS (bit 0 ) is accessible */
	unsigned int fp_state;	/* indicate FPU state (not a register) */

	/** FPU registers */
	unsigned int fpsr;
	unsigned int fpcr;
	unsigned long fp_padding;
	__uint128_t fpregs[32];
#else
	ulong_t r[13];
	ulong_t sp;		/* r13 */
	ulong_t lr;		/* r14 */
	ulong_t pc;		/* r15 */
	ulong_t cpsr;

	/* register frame saved up until here on exception entry */

	/* only valid for system calls */
	ulong_t orig_r0;	/* original x0 value on system call entry */

	/* saved on context switching */
	ulong_t tls0;	/* TPIDRURW */
	ulong_t tls1;	/* TPIDRURO */

	/** FPU registers */
	unsigned int fpscr;
	unsigned int fp_state;	/* indicate FPU state (not a register) */
	unsigned long long fp_padding;
	unsigned long long fpregs[32];
#endif
};

#ifdef __cplusplus
}
#endif

#endif
