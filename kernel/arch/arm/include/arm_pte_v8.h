/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2023 Alexander Zuepke */
/*
 * arm_pte_v8.h
 *
 * ARM page table bits (ARM v7 LPAE and v8 version).
 *
 * azuepke, 2013-11-20: initial LPAE support
 * azuepke, 2021-04-24: from exception.c
 * azuepke, 2023-07-29: move memory type defines here as well
 */

#ifndef ARM_PTE_V8_H_
#define ARM_PTE_V8_H_


/** 4K page size and masks */
/* PAGE_SIZE is defined in arch_defs.h */
#define PAGE_OFFSET			0x00000fffULL

/** 2M block size and masks (ARMv8, LPAE) */
#define BLOCK_SIZE			0x00200000ULL
#define BLOCK_OFFSET		0x001fffffULL

#define PTE_L(bits)			((bits)*1ULL)
#define PTE_U(bits)			(((bits)*1ULL) << 52)
#define PTE_ATTR_SHIFT		2

/** PTE type encoding */
#define PTE_P				PTE_L(0x001)	/* present bit */
#define PTE_TYPE_BLOCK		PTE_L(0x001)	/* type block */
#define PTE_TYPE_PT			PTE_L(0x003)	/* type page table */
#define PTE_TYPE_PAGE		PTE_L(0x003)	/* type 4K page */
#define PTE_TYPE_MASK		PTE_L(0x003)	/* type mask */

/** PTE bits -- lower 12 attributes bits */
#define PTE_ATTR0			PTE_L(0x004)	/* memory type bit 0 */
#define PTE_ATTR1			PTE_L(0x008)	/* memory type bit 1 */
#define PTE_ATTR2			PTE_L(0x010)	/* memory type bit 2 */
#define PTE_ATTR3			PTE_L(0x020)	/* memory type bit 3 (stage 2) */
#define PTE_NS				PTE_L(0x020)	/* not secure (stage 1) */
#define PTE_AP1				PTE_L(0x040)	/* AP1 (stage 2: S2AP0) */
#define PTE_AP2				PTE_L(0x080)	/* AP2 (stage 2: S2AP1) */
#define PTE_SH0				PTE_L(0x100)	/* shareable bit 0 */
#define PTE_SH1				PTE_L(0x200)	/* shareable bit 1 */
#define PTE_AF				PTE_L(0x400)	/* access flag */
#define PTE_NG				PTE_L(0x800)	/* non global (stage 1) */

/** PTE bits -- upper 12 attributes bits */
#define PTE_CONTIG			PTE_U(0x001)	/* contiguous hint */
#define PTE_PXN				PTE_U(0x002)	/* privileged execute never (stage 1) */
#define PTE_XN				PTE_U(0x004)	/* execute never */

#define PTE_PT_PXN			PTE_U(0x080)	/* privileged execute never page table */
#define PTE_PT_XN			PTE_U(0x100)	/* execute never page table */
#define PTE_PT_AP1			PTE_U(0x200)	/* AP1 mask page table (prohibit user) */
#define PTE_PT_AP2			PTE_U(0x400)	/* AP2 mask page table (read-only) */
#define PTE_PT_NS			PTE_U(0x800)	/* not secure page table */

/* 48 bits physical address space mask */
#define PTE_ADDR_MASK		0x0000fffffffff000ULL


/*
 * ARMv8 memory types:
 *
 * Given in MAIR order and following the MAP_XXX definition in mapping.h:
 *
 * bit MAIR memory type
 * 000  00  device nGnRnE "uncached strongly ordered"
 * 001  04  device nGnRE  "device"
 * 010  08  device nGRE
 * 011  0c  device GRE
 * 100  44  normal memory NC
 * 101  aa  normal memory WT + read-allocate
 * 110  ee  normal memory WB + read-allocate
 * 111  ff  normal memory WB + read-allocate + write-allocate
 *
 * Page table bits PTE_ATTR0..2 refer to the memory type in the MAIR register.
 *
 * NOTE: these MAIR settings are different to the ones in XEN or Linux
 */
#define MAIR0_DEFAULT		0x0c080400
#define MAIR1_DEFAULT		0xffeeaa44


/* Stage 2 memory types:
 *
 * bit   memory type
 * 0000  device nGnRnE
 * 0001  device nGnRE
 * 0010  device nGRE
 * 0011  device GRE
 * 0101  normal memory NC
 * 1010  normal memory WT
 * 1111  normal memory WB
 *
 * For normal memory, the encoding is "ooii", "oo" (outer) and "ii" (inner):
 *   01  non-cacheable
 *   10  write-through cacheable
 *   11  write-back cacheable
 *
 * Combined effect of stage 1 and stage 2 cacheability attributes:
 * - stage 1 or 2 non-cacheable overrides any other setting   -> non-cacheable
 * - stage 1 or 2 write-through overrides any cached mode     -> write-through
 *
 * Note: This is the same encoding as in the MAIR register, except that MAIR
 * also specifies read and write allocation strategies.
 */

/*
 * Shareability attributes:
 *
 * bit  meaning
 * 00   non-shareable
 * 01   reserved
 * 10   outer shareable
 * 11   inner shareable
 *
 * Note:
 * Shareability attributes are only relevant for normal cacheable memory.
 * Device and non-cacheable normal memory are always outer shareable.
 *
 * Combined effect of stage 1 and stage 2 shareability attributes:
 * - stage 1 or 2 outer shareable overrides any other setting -> outer shareable
 * - stage 1 or 2 inner shareable overrides non-cacheable     -> inner shareable
 */


/*  AP2..1 values (no AP0 on ARMv8)
 *
 *      kernel  user  usage
 *  00  rw      --    kernel RW
 *  01  rw      rw    user RW
 *  10  ro      --    kernel R-
 *  11  ro      ro    user R-
 */

/*
 * NOTE: the AF flag must be set for a mapping to be accessible at all,
 * so we clear the AF flag to get user mappings non-accessible
 * to both user and kernel:
 *
 *  prot   AP2 AP1  XN  AF PXN
 *  ---     .   .   x   .   x
 *  r--     x   x   x   x   x
 *  rw-     .   x   x   x   x
 *  r-x     x   x   .   x   x
 *  rwx     .   x   .   x   x
 *
 * We also always set the PXN bit for user space mapping.
 */

/* test various PTE flags */
#define PTE_IS_VALID(pte)	(((pte) & PTE_P) != 0)
#define PTE_IS_PAGE(pte)	(((pte) & PTE_TYPE_MASK) == PTE_TYPE_PAGE)
#define PTE_IS_BLOCK(pte)	(((pte) & PTE_TYPE_MASK) == PTE_TYPE_BLOCK)
#define PTE_IS_PT(pte)		(((pte) & PTE_TYPE_MASK) == PTE_TYPE_PT)
#define PTE_IS_READ(pte)	(((pte) & PTE_AF) != 0)
#define PTE_IS_WRITE(pte)	(((pte) & PTE_AP2) == 0)
#define PTE_IS_EXEC(pte)	(((pte) & PTE_XN) == 0)

/* accessors for page table entries */
#define PTE_TO_PHYS(pte)	(((unsigned long long)((pte) & PTE_ADDR_MASK)))
#define PHYS_TO_PTE(phys)	((unsigned long long)(phys))

#define PTE_TO_VIRT(pte)	PHYS_TO_KERNEL(PTE_TO_PHYS(pte))
#define VIRT_TO_PTE(virt)	PHYS_TO_PTE(KERNEL_TO_PHYS(virt))

/*  S2AP1..0 values (mapped to PTE_AP2 and PTE_AP1)
 *
 *      access
 *  00  --
 *  01  ro
 *  10  wo
 *  11  rw
 *
 * Effectively, S2AP1 (PTE_AP2) is a write bit and S2AP0 (PTE_AP1) a read bit.
 */


/** a page table is always 4K sized and aligned */

/** number of elements in a level 3 page table */
#define PTE_L3_NUM 512
#define PTE_L3_ALIGN 0x1000

/** number of elements in a level 2 page table */
#define PTE_L2_NUM 512
#define PTE_L2_ALIGN 0x1000

/** number of elements in a level 1 page table */
#ifdef __aarch64__
#define PTE_L1_NUM 512
#define PTE_L1_ALIGN 0x1000
#else
#define PTE_L1_NUM 4
#define PTE_L1_ALIGN 0x20
#endif

/** number of elements in a level 0 page table */
#define PTE_L0_NUM 1

#ifndef __ASSEMBLER__

#include <stdint.h>

/*
 * The ARM architecture defines four levels of page tables of 512 entries each.
 * On 32-bit, only the last 3 levels are used, and the first level is
 * reduced to 4 entries.
 *
 *  TTBR -> root pointer          -- 64-bit --       -- 32-bit --
 *          level 0 page table    bit 47 .. 39        (not used)
 *          level 1 page table    bit 38 .. 30       bit 31 .. 30
 *          level 2 page table    bit 29 .. 21       bit 29 .. 21
 *          level 3 page table    bit 20 .. 12       bit 20 .. 12
 *
 * On 64-bit, we also only use 3 levels, but fully populate level 1 page tables
 * to get 39-bit virtual address space for user space. Also, we use TTBR1
 * exclusively for the kernel with a similar 39-bit virtual layout.
 */

/** page table entry (64-bit on all levels) */
typedef unsigned long long arm_pte_t;

/** get level 3 PTE index from address */
static inline unsigned int arm_addr_to_l3(addr_t addr)
{
	return (addr >> 12) & (PTE_L3_NUM - 1);
}

/** get level 2 PTE index from address */
static inline unsigned int arm_addr_to_l2(addr_t addr)
{
	return (addr >> 21) & (PTE_L2_NUM - 1);
}

/** get level 2 PTE index from address */
static inline unsigned int arm_addr_to_l1(addr_t addr)
{
	return (addr >> 30) & (PTE_L1_NUM - 1);
}

/** get level 0 PTE index from address */
static inline unsigned int arm_addr_to_l0(addr_t addr)
{
	return (addr >> 39) & (PTE_L0_NUM - 1);
}

/* writable pagetable for cross-copy mapping */
extern arm_pte_t arch_adspace_xmap_pagetable[PTE_L3_NUM];

#endif

#endif
