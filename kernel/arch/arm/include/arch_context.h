/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2017, 2021 Alexander Zuepke */
/*
 * arch_context.h
 *
 * ARM register context switching and helper functions
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-14: 64-bit port
 * azuepke, 2017-09-29: imported & adapted
 */

#ifndef ARCH_CONTEXT_H_
#define ARCH_CONTEXT_H_

#include <marron/regs.h>
#include <arm_cr.h>
#include <arm_insn.h>
#include <arm_private.h>
#include <ex_table.h>
#include <arch.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>
#include <stddef.h>


/** initialize an idle thread's register context */
static inline void arch_reg_init_idle(
	struct regs *regs,
	ulong_t entry,
	ulong_t stack)
{
#ifdef __aarch64__
	for (unsigned int i = 0; i < countof(regs->r); i++) {
		regs->r[i] = 0;
	}

	/* let function return to NULL address */
	regs->lr = 0;
	/* keep stack aligned to 16 bytes */
	regs->sp = stack & -16ul;
	regs->pc = entry;
	/* run in super visor mode, but use user mode stack */
	regs->cpsr = CPSR_USER64_BITS | CPSR_MODE_EL1t;
	regs->tls0 = 0;
	regs->tls1 = 0;
	regs->mdscr = 0;
	regs->fp_state = 0;
#else
	for (unsigned int i = 0; i < countof(regs->r); i++) {
		regs->r[i] = 0;
	}

	/* keep stack aligned to 8 bytes */
	regs->sp = stack & -8ul;
	/* let function return to NULL address */
	regs->lr = 0;
	regs->pc = entry;
	/* run in super visor mode, but use user mode stack */
	regs->cpsr = CPSR_USER32_BITS | CPSR_MODE_SYSTEM;
	if (entry & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
	regs->tls0 = 0;
	regs->tls1 = 0;
	regs->fp_state = 0;
#endif
}

/** initialize a user space thread's register context */
static inline void arch_reg_init(
	struct regs *regs,
	ulong_t entry,
	ulong_t stack,
	ulong_t tls,
	ulong_t arg1,
	ulong_t arg2,
	ulong_t arg3,
	ulong_t arg4,
	ulong_t arg5)
{
	regs->r[0] = arg1;
	regs->r[1] = arg2;
	regs->r[2] = arg3;
	regs->r[3] = arg4;
	regs->r[4] = arg5;
	for (unsigned int i = 5; i < countof(regs->r); i++) {
		regs->r[i] = 0;
	}
#ifdef __aarch64__
	/* let function return to NULL address */
	regs->lr = 0;
	/* keep stack aligned to 16 bytes */
	regs->sp = stack & -16ul;
	regs->pc = entry;
	/* run in user mode */
	regs->cpsr = CPSR_USER64_BITS;
	regs->tls0 = tls;
	regs->tls1 = 0;
	regs->mdscr = 0;
	regs->fp_state = 0;
#else
	/* keep stack aligned to 8 bytes */
	regs->sp = stack & -8ul;
	/* let function return to NULL address */
	regs->lr = 0;
	regs->pc = entry;
	/* run in user mode */
	regs->cpsr = CPSR_USER32_BITS;
	if (entry & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
	regs->tls0 = 0;
	regs->tls1 = tls;
	regs->fp_state = 0;
#endif
}

/** save additional registers, e.g. TLS */
static inline void arch_reg_save(struct regs *regs)
{
	regs->tls0 = arm_get_tls0();
	regs->tls1 = arm_get_tls1();
#ifdef __aarch64__
	regs->mdscr = arm_get_mdscr_ss();
#endif
}

/** restore additional registers, e.g. TLS */
static inline void arch_reg_restore(const struct regs *regs)
{
	arm_set_tls0(regs->tls0);
	arm_set_tls1(regs->tls1);
#ifdef __aarch64__
	arm_update_mdscr_ss(regs->mdscr);
#endif
}

/** additional cleanup on context switch */
static inline void arch_reg_switch(void)
{
	/* clear exclusive state */
	arm_clrex();
}


/** initialize a thread's FPU state to sane defaults */
static inline void arch_reg_fpu_init_disabled(struct regs *regs)
{
#ifdef __aarch64__
	regs->fp_state = 0;
	regs->fpsr = 0;
	regs->fpcr = 0;
	regs->fp_padding = 0;
	for (unsigned int i = 0; i < countof(regs->fpregs); i++) {
		regs->fpregs[i] = 0;
	}
#else
	regs->fpscr = 0;
	regs->fp_state = 0;
	regs->fp_padding = 0;
	for (unsigned int i = 0; i < countof(regs->fpregs); i++) {
		regs->fpregs[i] = 0;
	}
#endif
}

/** enable FPU in register context */
static inline void arch_reg_fpu_enable(struct regs *regs)
{
	regs->fp_state = 1;
}

/** disable FPU in register context */
static inline void arch_reg_fpu_disable(struct regs *regs)
{
	regs->fp_state = 0;
}

/** test if FPU is enabled in register context */
static inline int arch_reg_fpu_enabled(const struct regs *regs)
{
	return regs->fp_state != 0;
}

/** turn FPU on */
static inline void arch_fpu_enable(void)
{
#ifdef __aarch64__
	arm_set_cpacr(CPACR_FPEN_U);
#else
	arm_set_fpexc(FPEXC_EN);
#endif
}

/** turn FPU off */
static inline void arch_fpu_disable(void)
{
#ifdef __aarch64__
	arm_set_cpacr(CPACR_FPEN_K);
#else
	arm_set_fpexc(0);
#endif
}

/** save FPU state */
static inline void arch_fpu_save(struct regs *regs)
{
#ifdef __aarch64__
	arm_fpu_save(&regs->fpregs[0]);
#else
	arm_fpu_save(&regs->fpscr);
#endif
}

/** restore FPU state */
static inline void arch_fpu_restore(const struct regs *regs)
{
#ifdef __aarch64__
	arm_fpu_restore(&regs->fpregs[0]);
#else
	arm_fpu_restore(&regs->fpscr);
#endif
}

/** check if the thread is currently using the signal stack */
static inline int arch_on_stack(
	const struct regs *regs,
	void *stack_addr,
	size_t stack_size)
{
	if (stack_addr == NULL) {
		return 0;
	}
	return (((addr_t)regs->sp >  (addr_t)stack_addr) &&
	        ((addr_t)regs->sp <= (addr_t)stack_addr + stack_size));
}

/** allocate a register context frame on stack for a signal handler */
static inline addr_t arch_signal_alloc_frame(
	const struct regs *regs,
	addr_t sig_stack_base,
	size_t sig_stack_size)
{
	addr_t stack;

	/* default to current stack */
	stack = regs->sp;
	if (sig_stack_base != 0) {
		addr_t sig_stack_end = sig_stack_base + sig_stack_size;
		if ((stack <= sig_stack_base) || (stack > sig_stack_end)) {
			/* use signal stack */
			stack = sig_stack_end;
		}
	}

	/* allocate space for struct regs */
	stack -= sizeof(struct regs);

	/* align stack 16 bytes */
	stack &= -16ul;

	return stack;
}

/** make register context consistent for signal handling */
static inline void arch_signal_prepare(struct regs *regs)
{
	/* save TLS registers */
	arch_reg_save(regs);
}

/** setup registers to call signal handler of type sig_handler_t */
static inline void arch_signal_init(
	struct regs *regs,
	ulong_t handler,
	ulong_t frame,
	sys_sig_mask_t sig_mask,
	struct sys_sig_info *info_user)
{
	addr_t sp;

	/* data on stack (addresses growing down):
	 * - saved registers
	 * - struct sys_sig_info
	 * - stack arguments (info_user on 32-bit)
	 * set stack pointer to last saved structure
	 */
	sp = (addr_t)info_user;

#ifdef __aarch64__
	regs->r[0] = frame;
	regs->r[1] = sig_mask;
	regs->r[2] = (addr_t)info_user;
#else
	/* NOTE the alignment of register pairs on ARM! */
	regs->r[0] = frame;
	regs->r[1] = 0;
	regs->r[2] = sig_mask & 0xffffffff;
	regs->r[3] = sig_mask >> 32;
	/* must pass info_user on stack */
	sp -= 8;
	if (sp < ARCH_USER_END) {
		/* NOTE: we ignore any errors here */
		__asm__ volatile (
			"1:	str		%1, %0	\n"
			"2:	\n"
			EX_TABLE(1b, 2b)
			: "=m" (*(unsigned int*)sp)
			: "r" (info_user));
	}
#endif

	regs->sp = sp;

	/* let function return to NULL address */
	regs->lr = 0;

	/* keep stack as is -- already properly aligned */
	regs->pc = handler;

#ifdef __aarch64__
	regs->cpsr = CPSR_USER64_BITS;
#else
	regs->cpsr = CPSR_USER32_BITS;
	if (handler & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
#endif
}

/** fixup registers after signal handling */
static inline void arch_signal_fixup(struct regs *regs)
{
#ifdef __aarch64__
	/* fixup CPSR */
	regs->cpsr &= CPSR_USER64_MASK;
	regs->cpsr |= CPSR_USER64_BITS;
#else
	regs->cpsr &= CPSR_USER32_MASK;
	regs->cpsr |= CPSR_USER32_BITS;
	if (regs->pc & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
#endif

	/* restore TLS registers and MDSCR */
	arch_reg_restore(regs);
}

/** set current thread's TLS register */
static inline void arch_tls_set(struct regs *regs, addr_t tls)
{
#ifdef __aarch64__
	regs->tls0 = tls;
	arm_set_tls0(regs->tls0);
#else
	regs->tls1 = tls;
	arm_set_tls1(regs->tls1);
#endif
}


/** Correctly return a 64-bit value in system calls
 *
 * NOTE: This function is expected to be used in a tail-recursion fashion
 * at the end of a system call, i.e.:
 *   ulong_t sys_example(...)
 *   {
 *     ...
 *     return arch_syscall_return_64bit(current_thread()->regs, val);
 *   }
 * The function returns the bits that fit into the ulong_t return type.
 */
static inline ulong_t arch_syscall_return_64bit(struct regs *regs, uint64_t val)
{
#ifdef __aarch64__
	/* 64-bit values fit into the ulong_t return type */
	(void)regs;
	return val;
#else
	/* the high 32-bit are kept in r1 */
	regs->r[1] = (val >> 32);
	return val & 0xffffffffu;
#endif
}

/** Set the return value of a system call in the register context */
static inline void arch_syscall_retval(struct regs *regs, ulong_t val)
{
	regs->r[0] = val;
}

/** fixup registers to restart a system call after signal handling */
static inline void arch_syscall_restart(struct regs *regs)
{
	regs->r[0] = regs->orig_r0;

	/* rewind PC */
#ifdef __aarch64__
	/* SVC instruction in 64-bit mode is 4 bytes */
	regs->pc -= 4;
#else
	if ((regs->cpsr & CPSR_T) != 0) {
		/* SVC instruction in thumb mode is 2 bytes */
		regs->pc -= 2;
	} else {
		/* SVC instruction in ARM mode is 4 bytes */
		regs->pc -= 4;
	}
#endif
}

#endif
