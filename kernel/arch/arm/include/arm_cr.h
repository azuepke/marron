/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2017, 2021 Alexander Zuepke */
/*
 * arm_cr.h
 *
 * Bits in ARM specific control registers, safe to include from assembler.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-14: split into two files and added 64-bit support
 * azuepke, 2017-09-29: imported and merged again
 */

#ifndef ARM_CR_H_
#define ARM_CR_H_

/** CPU 32-bit modes in CPSRs */
#define CPSR_MODE_USER		0x10
#define CPSR_MODE_FIQ		0x11
#define CPSR_MODE_IRQ		0x12
#define CPSR_MODE_SVC		0x13
#define CPSR_MODE_MONITOR	0x16
#define CPSR_MODE_ABORT		0x17
#define CPSR_MODE_HYP		0x1a
#define CPSR_MODE_UNDEF		0x1b
#define CPSR_MODE_SYSTEM	0x1f
#define CPSR_MODE_MASK		0x1f

/** CPU 64-bit modes in CPSRs (bit 3..2: EL, bit 0: handler stack) */
#define CPSR_MODE_EL3h		0xd
#define CPSR_MODE_EL3t		0xc
#define CPSR_MODE_EL2h		0x9
#define CPSR_MODE_EL2t		0x8
#define CPSR_MODE_EL1h		0x5
#define CPSR_MODE_EL1t		0x4
#define CPSR_MODE_EL0t		0x0

/** CPSR bits */
#define CPSR_32BIT			0x00000010	/* 32-bit mode */
#define CPSR_T				0x00000020	/* Thumb enable */
#define CPSR_F				0x00000040	/* FIQ disable */
#define CPSR_I				0x00000080	/* IRQ disable */
#define CPSR_A				0x00000100	/* asynchronous abort disable */
#define CPSR_E				0x00000200	/* big endian */
#define CPSR_D				0x00000200	/* ARMv8: debug disable */

#define CPSR_IT1			0x0000fc00	/* Thumb2 IT flags */
#define CPSR_GE				0x000f0000	/* GE SIMD flags */

#define CPSR_IL				0x00100000	/* ARMv8: illegal execution state */
#define CPSR_SS				0x00200000	/* ARMv8: software step */
#define CPSR_J				0x01000000	/* Jazelle */
#define CPSR_IT2			0x06000000	/* Thumb2 IT flags */
#define CPSR_Q				0x08000000	/* Q flag */
#define CPSR_V				0x10000000	/* V flag */
#define CPSR_C				0x20000000	/* C flag */
#define CPSR_Z				0x40000000	/* Z flag */
#define CPSR_N				0x80000000	/* N flag */


/** CPSR default user bits */
#ifdef BIG_ENDIAN
#define CPSR_USER32_BITS	0x00000210	/* 32-bit user mode */
#define CPSR_USER64_BITS	0x00000200	/* 64-bit user mode on thread stack */
#else
#define CPSR_USER32_BITS	0x00000010	/* 32-bit user mode */
#define CPSR_USER64_BITS	0x00000000	/* 64-bit user mode on thread stack */
#endif

/** CPSR bits the user can change */
#define CPSR_USER32_MASK	0xfe0ffc20	/* all flags + Thumb, except Jazelle */
#define CPSR_USER64_MASK	0xf0200000	/* NZCV flags + single stepping */

/** FPEXC bits (32-bit only) */
#define FPEXC_EX			0x80000000	/* exception, save FPINST */
#define FPEXC_EN			0x40000000	/* enable */
#define FPEXC_DEX			0x20000000	/* unsupported vector instruction */
#define FPEXC_FP2V			0x10000000	/* exception, save FPINST2 */


/** ESR exception class encoding (64-bit only) */
#define ESR_EC_UNKNOWN		0x00	/* unknown reason */
#define ESR_EC_WFI_WFE		0x01	/* trapped WFI or WFE */
#define ESR_EC_MCR_CP15		0x03	/* 32-bit: trapped MCR or MRC to CP15 */
#define ESR_EC_MCRR_CP15	0x04	/* 32-bit: trapped MCRR or MRRC to CP15 */
#define ESR_EC_MCR_CP1		0x05	/* 32-bit: trapped MCR or MRC to CP14 */
#define ESR_EC_LDC_STC		0x06	/* 32-bit: trapped LDC or STC */
#define ESR_EC_FPU_UNAVAIL	0x07	/* trapped FPU instruction (FPU disabled) */
#define ESR_EC_VMRS			0x08	/* 32-bit: trapped VMRS; EL2+ */
#define ESR_EC_MCRR_CP14	0x0c	/* 32-bit: trapped MCRR or MRRC to CP14 */
#define ESR_EC_ILLEGAL		0x0e	/* illegal execution state */

#define ESR_EC_SVC32		0x11	/* 32-bit: SVC */
#define ESR_EC_HVC32		0x12	/* 32-bit: HVC; EL2+ */
#define ESR_EC_SMC32		0x13	/* 32-bit: SMC; EL2+ */
#define ESR_EC_SVC64		0x15	/* 64-bit: SVC */
#define ESR_EC_HVC64		0x16	/* 64-bit: HVC; EL2+ */
#define ESR_EC_SMC64		0x17	/* 64-bit: SMC; EL2+ */
#define ESR_EC_MSR_MRS		0x18	/* 64-bit: trapped MSR or MRS */
#define ESR_EC_IMPL_DEFINED	0x1f	/* implementation defined; EL3 only */

#define ESR_EC_IABT_USER	0x20	/* instruction abort from lower level */
#define ESR_EC_IABT_KERN	0x21	/* instruction abort from same level */
#define ESR_EC_PC_ALIGN		0x22	/* program counter alignment */
#define ESR_EC_DABT_USER	0x24	/* data abort from lower level */
#define ESR_EC_DABT_KERN	0x25	/* data abort from same level */
#define ESR_EC_SP_ALIGN		0x26	/* 64-bit: stack pointer alignment */
#define ESR_EC_FPU32		0x28	/* 32-bit: FPU exception */
#define ESR_EC_FPU64		0x2c	/* 64-bit: FPU exception */
#define ESR_EC_SERROR		0x2f	/* SError interrupt */

#define ESR_EC_BREAK_USER	0x30	/* breakpoint from lower level */
#define ESR_EC_BREAK_KERN	0x31	/* breakpoint from same level */
#define ESR_EC_STEP_USER	0x32	/* single-step from lower level */
#define ESR_EC_STEP_KERN	0x33	/* single-step from same level */
#define ESR_EC_WATCH_USER	0x34	/* watchpoint from lower level */
#define ESR_EC_WATCH_KERN	0x35	/* watchpoint from same level */
#define ESR_EC_BRK32		0x38	/* 32-bit: BKPT instruction */
#define ESR_EC_VECTOR_CATCH	0x3a	/* 32-bit: vector catch exception; EL2+ */
#define ESR_EC_BRK64		0x3c	/* 64-bit: BRK instruction */

#define ESR_EC_SHIFT		26


/** ESR instruction and data abort bits */
#define ESR_FAULT_FNV		0x200	/* FAR not valid */
#define ESR_FAULT_EA		0x200	/* external abort */
#define ESR_FAULT_CM		0x100	/* cache maintenance */
#define ESR_FAULT_WR		0x040	/* write (not read) fault */

#define ESR_FSC_MASK		0x03c	/* abort fault status code */
#define ESR_FSC_LEVEL_MASK	0x003	/* abort table walk level 0..3 */


/** ESR fault status code for instruction and data aborts */
#define ESR_FSC_ADDR_SIZE	0x00	/* address size fault, level 0..3 */
#define ESR_FSC_TRANSLATION	0x04	/* translation fault, level 0..3 */
#define ESR_FSC_ACCESS_FLAG	0x08	/* access flag fault, level 1..3 */
#define ESR_FSC_PERM		0x0c	/* permission fault, level 1..3 */
#define ESR_FSC_SYNCEA		0x10	/* synchronous external abort, normal access */
#define ESR_FSC_SYNCEA_PT	0x14	/* synchronous external abort, table walk, level 0..3 */
#define ESR_FSC_PARITY		0x18	/* synchronous parity or ECC error, normal access */
#define ESR_FSC_PARITY_PT	0x1c	/* synchronous parity or ECC error, table walk, level 0..3 */
#define ESR_FSC_ALIGN		0x20	/* alignment fault */
#define ESR_FSC_TLB_ERROR	0x30	/* TLB conflict abort */
#define ESR_FSC_IMPL_DEF	0x34	/* Impl. defined: lockdown or unsupported exclusive access */


/** ESR fault status code for trapped floating-point exceptions */
#define ESR_FPEXC_IOF		0x01	/* invalid operation */
#define ESR_FPEXC_DZF		0x02	/* divide by zero */
#define ESR_FPEXC_OFF		0x04	/* overflow */
#define ESR_FPEXC_UFF		0x08	/* underflow */
#define ESR_FPEXC_IXF		0x10	/* inexact */
#define ESR_FPEXC_IDF		0x80	/* input denormal */

#define ESR_FPEXC_TFV		0x800000	/* trapped fault valid bit */


/** CPACR -- Coprocessor Access Control Register (cp 15, c1, c0, 1) */
#define CPACR_CP10			0x00300000	/* enable access to CP10 */
#define CPACR_CP11			0x00c00000	/* enable access to CP11 */
#define CPACR_TTA			0x10000000	/* trap user access to trace regs (ARMv8) */
#define CPACR_D32DIS		0x40000000	/* disable D16-D31 (ARMv7) */
#define CPACR_ASEDIS		0x80000000	/* disable SIMD (ARMv7) */
#define CPACR_FPEN_K		0x00100000	/* enable FP access in EL1 only (ARMv8) */
#define CPACR_FPEN_U		0x00300000	/* enable FP access in EL0 and EL1 (ARMv8) */


/** DFSR and IFSR status bits */
#ifdef ARM_LPAE
#define FSR_FSMASK			0x003f
#else
#define FSR_FSMASK			0x000f
#define FSR_FSBIT4			0x0400
#endif
#define FSR_WRITE			0x0800
#define FSR_EXTABT			0x1000


/** SCTLR -- System Control Register (cp15, c1, c0, 0) */
/* NOTE: enables a feature when the bit is set, otherwise documented */
#define SCTLR_M				0x00000001	/* MMU */
#define SCTLR_A				0x00000002	/* strict alignment */
#define SCTLR_C				0x00000004	/* data/unified cache */
#define SCTLR_SA			0x00000008	/* enable stack alignment check (ARMv8) */
#define SCTLR_SA0			0x00000010	/* enable EL0 stack alignment check (ARMv8) */
#define SCTLR_CP15BEN		0x00000020	/* CP15 barrier enable */
#define SCTLR_ITD			0x00000080	/* IT instruction disable (ARMv8) */
#define SCTLR_SED			0x00000100	/* SETEND instruction disable (ARMv8) */
#define SCTLR_UMA			0x00000200	/* enable to mask interrupts in EL0 */
#define SCTLR_SW			0x00000400	/* SWP (ARMv7) */
#define SCTLR_Z				0x00000800	/* branch prediction enable (ARMv7) */
#define SCTLR_I				0x00001000	/* instruction cache */
#define SCTLR_V				0x00002000	/* high vectors (ARMv7) */
#define SCTLR_RR			0x00004000	/* round-robin replacement in caches (ARMv7) */
#define SCTLR_DZE			0x00004000	/* enable DC ZVA (dcache zero) instruction at EL0 (ARMv8) */
#define SCTLR_UCT			0x00008000	/* enable access to CTR at EL0 (ARMv8) */

#define SCTLR_nTWI			0x00010000	/* WFI non-trapping at EL0 (ARMv8) */
#define SCTLR_BR			0x00020000	/* MPU background region enable (MPU) */
#define SCTLR_HA			0x00020000	/* hardware access flag (ARMv7) */
#define SCTLR_nTWE			0x00040000	/* WFE non-trapping at EL0 (ARMv8) */

#define SCTLR_WXN			0x00080000	/* write implies execute never */
#define SCTLR_UWXN			0x00100000	/* user write implies execute never for EL1 (ARMv7) */
#define SCTLR_FI			0x00200000	/* low interrupt latency (ARMv7) */

#define SCTLR_VE			0x01000000	/* vectored interrupts (ARMv7) */
#define SCTLR_EE			0x02000000	/* big endian exception entry */

#define SCTLR_E0E			0x01000000	/* EL0 data access is big endian (ARMv8) */
#define SCTLR_UCI			0x04000000	/* user cache instructions (ARMv8) */
#define SCTLR_NMFI			0x08000000	/* non-maskable FIQs (ARMv7) */

#define SCTLR_TRE			0x10000000	/* TEX remapping (ARMv7) */
#define SCTLR_AFE			0x20000000	/* force AP bit (ARMv7) */
#define SCTLR_TE			0x40000000	/* thumb exception (ARMv7) */


/** SCTLR default bits */
#define SCTLR_CLR_32BIT		(SCTLR_A | SCTLR_V | \
							 SCTLR_TE | SCTLR_EE | \
							 SCTLR_AFE | SCTLR_HA | SCTLR_WXN | SCTLR_UWXN | \
							 SCTLR_NMFI | SCTLR_FI | SCTLR_VE | SCTLR_RR | \
							 SCTLR_CP15BEN)
#ifdef BIG_ENDIAN
#define SCTLR_SET_32BIT		(SCTLR_M | SCTLR_TRE | SCTLR_C | SCTLR_I | SCTLR_Z | SCTLR_EE)
#else
#define SCTLR_SET_32BIT		(SCTLR_M | SCTLR_TRE | SCTLR_C | SCTLR_I | SCTLR_Z)
#endif

#define SCTLR_CLR_64BIT		(SCTLR_A | \
							 SCTLR_EE | \
							 SCTLR_WXN | \
							 SCTLR_nTWI | SCTLR_UMA | \
							 SCTLR_SED | SCTLR_ITD | SCTLR_CP15BEN)
#ifdef BIG_ENDIAN
#define SCTLR_SET_64BIT		(SCTLR_M | SCTLR_C | SCTLR_I | \
							 SCTLR_nTWE | SCTLR_UCI | SCTLR_UCT | SCTLR_DZE | \
							 SCTLR_SA0 | SCTLR_SA | SCTLR_EE)
#else
#define SCTLR_SET_64BIT		(SCTLR_M | SCTLR_C | SCTLR_I | \
							 SCTLR_nTWE | SCTLR_UCI | SCTLR_UCT | SCTLR_DZE | \
							 SCTLR_SA0 | SCTLR_SA)
#endif


/* GIC-related system registers */
#define ICC_PMR_EL1		s3_0_c4_c6_0

#define ICC_IAR0_EL1	s3_0_c12_c8_0
#define ICC_EOIR0_EL1	s3_0_c12_c8_1
#define ICC_HPPIR0_EL1	s3_0_c12_c8_2
#define ICC_BPR0_EL1	s3_0_c12_c8_3

#define ICC_AP0R0_EL1	s3_0_c12_c8_4
#define ICC_AP0R1_EL1	s3_0_c12_c8_5
#define ICC_AP0R2_EL1	s3_0_c12_c8_6
#define ICC_AP0R3_EL1	s3_0_c12_c8_7

#define ICC_AP1R0_EL1	s3_0_c12_c9_0
#define ICC_AP1R1_EL1	s3_0_c12_c9_1
#define ICC_AP1R2_EL1	s3_0_c12_c9_2
#define ICC_AP1R3_EL1	s3_0_c12_c9_3

#define ICC_DIR_EL1		s3_0_c12_c11_1
#define ICC_RPR_EL1		s3_0_c12_c11_3
#define ICC_SGI1R_EL1	s3_0_c12_c11_5
#define ICC_ASGI1R_EL1	s3_0_c12_c11_6
#define ICC_SGI0R_EL1	s3_0_c12_c11_7

#define ICC_IAR1_EL1	s3_0_c12_c12_0
#define ICC_EOIR1_EL1	s3_0_c12_c12_1
#define ICC_HPPIR1_EL1	s3_0_c12_c12_2
#define ICC_BPR1_EL1	s3_0_c12_c12_3
#define ICC_CTLR_EL1	s3_0_c12_c12_4
#define ICC_SRE_EL1		s3_0_c12_c12_5
#define ICC_IGRPEN0_EL1	s3_0_c12_c12_6
#define ICC_IGRPEN1_EL1	s3_0_c12_c12_7

#define ICC_SRE_EL2		s3_4_c12_c9_5

#define ICC_CTLR_EL3	s3_6_c12_c12_4
#define ICC_SRE_EL3		s3_6_c12_c12_5
#define ICC_IGRPEN1_EL3	s3_6_c12_c12_7

#define ICH_HCR_AP0R0	s3_4_c12_c8_0
#define ICH_HCR_AP0R1	s3_4_c12_c8_1
#define ICH_HCR_AP0R2	s3_4_c12_c8_2
#define ICH_HCR_AP0R3	s3_4_c12_c8_3
#define ICH_HCR_AP1R0	s3_4_c12_c9_0
#define ICH_HCR_AP1R1	s3_4_c12_c9_1
#define ICH_HCR_AP1R2	s3_4_c12_c9_2
#define ICH_HCR_AP1R3	s3_4_c12_c9_3

#define ICH_HCR_EL2		s3_4_c12_c11_0
#define ICH_VTR_EL2		s3_4_c12_c11_1
#define ICH_MISR_EL2	s3_4_c12_c11_2
#define ICH_EISR_EL2	s3_4_c12_c11_3
#define ICH_ELRSR_EL2	s3_4_c12_c11_5
#define ICH_VMCR_EL2	s3_4_c12_c11_7

#define ICH_LR0_EL2		s3_4_c12_c12_0
#define ICH_LR1_EL2		s3_4_c12_c12_1
#define ICH_LR2_EL2		s3_4_c12_c12_2
#define ICH_LR3_EL2		s3_4_c12_c12_3
#define ICH_LR4_EL2		s3_4_c12_c12_4
#define ICH_LR5_EL2		s3_4_c12_c12_5
#define ICH_LR6_EL2		s3_4_c12_c12_6
#define ICH_LR7_EL2		s3_4_c12_c12_7
#define ICH_LR8_EL2		s3_4_c12_c13_0
#define ICH_LR9_EL2		s3_4_c12_c13_1
#define ICH_LR10_EL2	s3_4_c12_c13_2
#define ICH_LR11_EL2	s3_4_c12_c13_3
#define ICH_LR12_EL2	s3_4_c12_c13_4
#define ICH_LR13_EL2	s3_4_c12_c13_5
#define ICH_LR14_EL2	s3_4_c12_c13_6
#define ICH_LR15_EL2	s3_4_c12_c13_7

/** MDSCR bits of interest for an operating system kernel */
#define MDSCR_SS			0x00000001	/* software step enable */
#define MDSCR_TDCC			0x00001000	/* trap access to DCC registers */
#define MDSCR_KDE			0x00002000	/* local kernel debug enable */
#define MDSCR_MDE			0x00008000	/* monitor debug enable */

#endif
