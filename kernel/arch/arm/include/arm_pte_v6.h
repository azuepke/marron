/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2023 Alexander Zuepke */
/*
 * arm_pte_v6.h
 *
 * ARM page table bits (ARM v6 and v7 version).
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2021-04-24: adapted
 * azuepke, 2023-07-29: move memory type defines here as well
 */

#ifndef ARM_PTE_V6_H_
#define ARM_PTE_V6_H_

/** 4K page size and masks */
/* PAGE_SIZE is defined in arch.h */
#define PAGE_OFFSET			0x00000fffUL

/** 1M section size and masks */
#define SECTION_SIZE		0x00100000UL
#define SECTION_OFFSET		0x000fffffUL

/** PTE bits */
#define PTE_XN				0x001	/* execute never */
#define PTE_P				0x002	/* present, type: 4K page */
#define PTE_B				0x004	/* buffered */
#define PTE_C				0x008	/* cached */
#define PTE_AP0				0x010	/* AP0 */
#define PTE_AP1				0x020	/* AP1 */
#define PTE_TEX0			0x040	/* TEX0 */
#define PTE_TEX1			0x080	/* TEX1 */
#define PTE_TEX2			0x100	/* TEX2 */
#define PTE_AP2				0x200	/* AP2 */
#define PTE_S				0x400	/* shareable */
#define PTE_NG				0x800	/* non global */

/* 32 bits physical address space mask */
#define PTE_ADDR_MASK		0xfffff000UL

/** PDE type bits */
#define PDE_TYPE_PT			0x00001	/* type page table */
#define PDE_TYPE_SECT		0x00002	/* type section */
#define PDE_TYPE_MASK		0x00003	/* type mask */

/** PDE bits for sections */
#define PDE_B				0x00004	/* buffered */
#define PDE_C				0x00008	/* cached */
#define PDE_XN				0x00010	/* execute never */
#define PDE_AP0				0x00400	/* AP0 */
#define PDE_AP1				0x00800	/* AP1 */
#define PDE_TEX0			0x01000	/* TEX0 */
#define PDE_TEX1			0x02000	/* TEX1 */
#define PDE_TEX2			0x04000	/* TEX2 */
#define PDE_AP2				0x08000	/* AP2 */
#define PDE_S				0x10000	/* shareable */
#define PDE_NG				0x20000	/* non global */
#define PDE_SS				0x40000	/* super section */

/*
 * ARMv7 memory types:
 *
 * We use TEX remapping via the PRRR and NMRR registers.
 * This uses TEX0, C, and B bits to encode the mapping type.
 * However, encoding 110 is special and has an implementation defined meaning
 * and is therefore unusable in TEX remapping. We therefore cannot use
 * the same mapping as ARM v8 or as described in mapping.h, but we stick
 * to the default ARM v7 memory types:
 *
 * TEX C B  memory type           shareable
 *
 * 000 0 0  UC, strongly ordered  shareable
 * 000 0 1  UC, device            shareable
 * 000 1 0  WT, normal            S
 * 000 1 1  WB, normal            S
 * 001 0 0  UC, normal            S
 * 001 0 1  -- reserved --        -
 * 001 1 0  -- impl defined --    ?
 * 001 1 1  WBWA, normal          S
 *
 * We then simply map the entries for CACHE_TYPE_WT (5) and CACHE_TYPE_WB (6)
 * to 2 and 3.
 *
 * In PRRR, we also set that the S bit for device mappings means that it means
 * and we map shareable device by default.
 * We also set bits 24 to 31 to clear any outer shareable bits.
 */
#define PRRR_DEFAULT		0xff0a8aa4
#define NMRR_DEFAULT		0x44e048e0

/** default MMU domain settings */
#define DOMAIN_DEFAULT		0x55555555


/*  AP 2..0 values
 *
 *       kernel  user comments   usage
 *  000  --      --
 *  001  rw      --              kernel RW
 *  010  rw      ro
 *  011  rw      rw              user RW
 *  100  ??      ??
 *  101  ro      --              kernel R-
 *  110  ro      ro  ARMv6 only  user R-
 *  111  ro      ro  ARMv7 only  user R-
 */

/*
 * NOTE: the AP0 flag must be set for a mapping to be accessible at all,
 * so we clear the AP0 flag to get user mappings non-accessible
 * to both user and kernel:
 *
 *  prot   AP2 AP1 AP0  XN
 *  ---     .   .   .   x
 *  r--     x   x   x   x
 *  rw-     .   x   x   x
 *  r-x     x   x   x   .
 *  rwx     .   x   x   .
 */

/* test various PTE flags */
#define PTE_IS_VALID(pte)	(((pte) & PTE_P) != 0)
#define PTE_IS_READ(pte)	(((pte) & PTE_AP0) != 0)
#define PTE_IS_WRITE(pte)	(((pte) & PTE_AP2) == 0)
#define PTE_IS_EXEC(pte)	(((pte) & PTE_XN) == 0)

/* test various PDE flags */
#define PDE_IS_VALID(pde)	(((pde) & PDE_TYPE_MASK) != 0)
#define PDE_IS_PT(pde)		(((pde) & PDE_TYPE_MASK) == PDE_TYPE_PT)
#define PDE_IS_SECT(pde)	(((pde) & PDE_TYPE_MASK) == PDE_TYPE_SECT)

/* accessors for page table entries */
/* NOTE: page tables are NOT 4K sized, but 1K */
#define PDE_TO_PHYS(pde)	((unsigned long)(pde) & ~0x3fful)
#define PHYS_TO_PDE(phys)	((unsigned long)(phys))

#define PDE_TO_VIRT(pde)	PHYS_TO_KERNEL(PDE_TO_PHYS(pde))
#define VIRT_TO_PDE(virt)	PHYS_TO_PDE(KERNEL_TO_PHYS(virt))

#define PTE_TO_PHYS(pte)	(((unsigned long)((pte) & PTE_ADDR_MASK)))
#define PHYS_TO_PTE(phys)	((unsigned long)(phys))

#define PTE_TO_VIRT(pte)	PHYS_TO_KERNEL(PTE_TO_PHYS(pte))
#define VIRT_TO_PTE(virt)	PHYS_TO_PTE(KERNEL_TO_PHYS(virt))


/** number of elements in a page table */
#define PTE_NUM 256

/** a page table is 1K sized and aligned */
#define PT_SIZE 0x400
#define PT_ALIGN 0x400


/** number of elements in a page directory */
#define PDE_NUM 4096

/** a page directory is 16K sized and aligned */
#define PD_SIZE 0x4000
#define PD_ALIGN 0x4000

#ifndef __ASSEMBLER__

#include <stdint.h>

/* the ARM architecture defines two levels of page tables.
 *
 *    TTBR  root pointer
 * 1. PD    page directory
 * 2. PT    page table
 *
 * Entries in both tables are 32-bit wide.
 * We use arm_pte_t for both page directory and page table entries.
 */

/** page table entry (32-bit on all levels) */
typedef unsigned long arm_pte_t;

/** get PTE index from address */
static inline unsigned int arm_addr_to_pte(addr_t addr)
{
	return (addr >> 12) & (PTE_NUM - 1);
}

/** get PDE index from address */
static inline unsigned int arm_addr_to_pde(addr_t addr)
{
	return (addr >> 20) & (PDE_NUM - 1);
}

/* writable pagetable for cross-copy mapping */
extern arm_pte_t arch_adspace_xmap_pagetable[PTE_NUM];

/* convert page table flags of 4K PTE entry to 1M PDE entry */
static inline unsigned long arm_page_to_sect(unsigned long flags)
{
	unsigned long xn = (flags & 0x001) << 4;
	unsigned long cb = (flags & 0x00c);
	unsigned long typ = (flags & 0xff0) << 6;
	return typ | xn | cb | PDE_TYPE_SECT;
}

/* convert page table flags of 1M PDE entry to 4K PTE entry */
static inline unsigned long arm_sect_to_page(unsigned long flags)
{
	unsigned long xn = (flags & 0x00010) >> 4;
	unsigned long cb = (flags & 0x0000c);
	unsigned long typ = (flags & 0x3fc00) >> 6;
	return typ | xn | cb | PTE_P;
}

#endif

#endif
