/* SPDX-License-Identifier: MIT */
/* Copyright 2014 Alexander Zuepke */
/*
 * arm_perf_v6.h
 *
 * ARM performance monitoring on ARMv6.
 *
 * azuepke, 2014-03-03: initial
 */

#ifndef ARM_PERF_V6_H_
#define ARM_PERF_V6_H_

/* ARM1176JZFS implements a cycle counter and two counter registers 0 and 1 */

/* bits in (PMNC) control register */
#define ARM_PERF_PMCR_SRC0(pmnc)	((pmnc >> 20) & 0xff)	/* source of counter 0 */
#define ARM_PERF_PMCR_SRC1(pmnc)	((pmnc >> 12) & 0xff)	/* source of counter 1 */

#define ARM_PERF_PMCR_X		0x800	/* export of events to ETM */
#define ARM_PERF_PMCR_CCR	0x400	/* cycle counter overflow, w1c */
#define ARM_PERF_PMCR_CR1	0x200	/* count register 1 overflow, w1c */
#define ARM_PERF_PMCR_CR0	0x100	/* count register 0 overflow, w1c */

#define ARM_PERF_PMCR_ECC	0x040	/* cycle counter enable interrupt */
#define ARM_PERF_PMCR_EC1	0x020	/* counter 1 enable interrupt */
#define ARM_PERF_PMCR_EC0	0x010	/* counter 0 enable interrupt */

#define ARM_PERF_PMCR_D		0x008	/* cycle counter counts every 64th cycle */
#define ARM_PERF_PMCR_C		0x004	/* cycle counter reset */
#define ARM_PERF_PMCR_P		0x002	/* event counter reset */
#define ARM_PERF_PMCR_E		0x001	/* enable all counters */

/* user enable bit */
#define ARM_PERF_USEREN		0x00000001


#ifndef __ASSEMBLER__

/* access to performance monitor registers (ARMv6 / ARM1176JZFS) */

/** Get USEREN (User Enable) Register */
static inline unsigned int arm_perf_get_useren(void)
{
	unsigned int val;
	__asm__ volatile ("mrc p15, 0, %0, c15, c9, 0" : "=r"(val));
	return val;
}

/** Set USEREN (User Enable) Register */
static inline void arm_perf_set_useren(unsigned int val)
{
	__asm__ volatile ("mcr p15, 0, %0, c15, c9, 0" : : "r"(val) : "memory");
}


/** Get PMNC (Performance Monitor Control) Register */
static inline unsigned int arm_perf_get_ctrl(void)
{
	unsigned int val;
	__asm__ volatile ("mrc p15, 0, %0, c15, c12, 0" : "=r"(val));
	return val;
}

/** Set PMNC (Performance Monitor Control) Register */
static inline void arm_perf_set_ctrl(unsigned int val)
{
	__asm__ volatile ("mcr p15, 0, %0, c15, c12, 0" : : "r"(val) : "memory");
}


/** Get CCNT (Cycle Count) Register */
static inline unsigned int arm_perf_get_ccnt(void)
{
	unsigned int val;
	__asm__ volatile ("mrc p15, 0, %0, c15, c12, 1" : "=r"(val));
	return val;
}

/** Set CCNT (Cycle Count) Register */
static inline void arm_perf_set_ccnt(unsigned int val)
{
	__asm__ volatile ("mcr p15, 0, %0, c15, c12, 1" : : "r"(val) : "memory");
}


/** Get Count Register 0 */
static inline unsigned int arm_perf_get_count0(void)
{
	unsigned int val;
	__asm__ volatile ("mrc p15, 0, %0, c15, c12, 2" : "=r"(val));
	return val;
}

/** Set Count Register 0 */
static inline void arm_perf_set_count0(unsigned int val)
{
	__asm__ volatile ("mcr p15, 0, %0, c15, c12, 2" : : "r"(val) : "memory");
}


/** Get Count Register 1 */
static inline unsigned int arm_perf_get_count1(void)
{
	unsigned int val;
	__asm__ volatile ("mrc p15, 0, %0, c15, c12, 3" : "=r"(val));
	return val;
}

/** Set Count Register 1 */
static inline void arm_perf_set_count1(unsigned int val)
{
	__asm__ volatile ("mcr p15, 0, %0, c15, c12, 3" : : "r"(val) : "memory");
}
#endif

#endif
