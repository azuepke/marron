/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2021 Alexander Zuepke */
/*
 * arch_uaccess.h
 *
 * ARM specific copy in/out code
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2015-07-14: 64-bit port
 * azuepke, 2021-04-23: adapted
 */

#ifndef ARCH_UACCESS_H_
#define ARCH_UACCESS_H_

#ifndef UACCESS_H_
#error This file is included by uaccess.h
#endif

#include <ex_table.h>

#ifdef __aarch64__
#define LDR64 "ldr"
#define STR64 "str"
#define PFX32 "w"
#else
#define LDR64 "ldrd"
#define STR64 "strd"
#define PFX32 ""
#endif

/* getter and setter with inline exception handling (returns EOK or EFAULT) */
#define arch_user_get_1(addr, val, err)	\
	__asm__ volatile (	\
		"1:	ldrb	%" PFX32 "1, %2	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	mov		%0, %4	\n"	\
		"	b		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=&r" (err), "=&r" (val)	\
		: "m" (*(const unsigned char*)addr), "0" (EOK), "i" (EFAULT))

#define arch_user_get_4(addr, val, err)	\
	__asm__ volatile (	\
		"1:	ldr		%" PFX32 "1, %2	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	mov		%0, %4	\n"	\
		"	b		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=&r" (err), "=&r" (val)	\
		: "m" (*(const unsigned int*)addr), "0" (EOK), "i" (EFAULT))

#define arch_user_get_8(addr, val, err)	\
	__asm__ volatile (	\
		"1:	" LDR64 " %1, %2	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	mov		%0, %4	\n"	\
		"	b		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=&r" (err), "=&r" (val)	\
		: "m" (*(const unsigned long long*)addr), "0" (EOK), "i" (EFAULT))

#define arch_user_put_1(addr, val, err)	\
	__asm__ volatile (	\
		"1:	strb	%" PFX32 "2, %1	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	mov		%0, %4	\n"	\
		"	b		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=r" (err), "=m" (*(unsigned int*)addr)	\
		: "r" (val), "0" (EOK), "i" (EFAULT))

#define arch_user_put_4(addr, val, err)	\
	__asm__ volatile (	\
		"1:	str		%" PFX32 "2, %1	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	mov		%0, %4	\n"	\
		"	b		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=r" (err), "=m" (*(unsigned int*)addr)	\
		: "r" (val), "0" (EOK), "i" (EFAULT))

#define arch_user_put_8(addr, val, err)	\
	__asm__ volatile (	\
		"1:	" STR64 " %2, %1	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	mov		%0, %4	\n"	\
		"	b		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=r" (err), "=m" (*(unsigned long long*)addr)	\
		: "r" (val), "0" (EOK), "i" (EFAULT))

/* robust compare and exxchange */
#ifdef __aarch64__

#define arch_user_cas_4(addr, old, new, prev, err)	\
	__asm__ volatile (	\
		"1:	ldxr	%1, %2			\n"	\
		"	mov		%0, %6			\n"	\
		"	cmp		%1, %3			\n"	\
		"	b.ne	3f				\n"	\
		"2:	stxr	%w0, %4, %2		\n"	\
		"	cmp		%0, #0			\n"	\
		"	csel	%0, %5, %6, eq	\n"	\
		"3:							\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"4:	mov		%0, %7			\n"	\
		"	b		3b				\n"	\
		".popsection				\n"	\
		EX_TABLE(1b, 4b)	\
		EX_TABLE(2b, 4b)	\
		: "=&r" (err), "=&r" (prev), "+Q" (*addr)	\
		: "1" (old), "r" (new), "r" (EOK), "r" (EAGAIN), "i" (EFAULT)	\
		: "memory", "cc")

#else

#define arch_user_cas_4(addr, old, new, prev, err)	\
	__asm__ volatile (	\
		"1:	ldrex	%1, %2			\n"	\
		"	cmp		%1, %3			\n"	\
		THUMB_IT("eq")					\
		"2:	strexeq	%0, %4, %2		\n"	\
		THUMB_IT("eq")					\
		"	cmpeq	%0, #0			\n"	\
		THUMB_ITE("eq")					\
		"	moveq	%0, %5			\n"	\
		"	movne	%0, %6			\n"	\
		"3:							\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"4:	mov		%0, %7			\n"	\
		"	b		3b				\n"	\
		".popsection				\n"	\
		EX_TABLE(1b, 4b)	\
		EX_TABLE(2b, 4b)	\
		: "=&r" (err), "=&r" (prev), "+Q" (*addr)	\
		: "1" (old), "r" (new), "i" (EOK), "i" (EAGAIN), "i" (EFAULT)	\
		: "memory", "cc")

#endif

static inline err_t user_wordcpy_nocheck(void *dst, const void *src, size_t sz, size_t align)
{
	addr_t end = (addr_t)src + sz;

	if (__builtin_constant_p(sz) && ((sz % (2*sizeof(long))) == 0) && (align >= sizeof(long long))) {
		/* 32-bit ARM requires a 64-bit variable with hi/lo register parts,
		 * so we use the same technique on 64-bit ARM to keep the code readable
		 */
#ifdef __aarch64__
		__int128_t tmp;
#else
		unsigned long long tmp;
#endif

		/* dual word / pair copy */
		__asm__ volatile (
			"	b		3f	\n"
#ifdef __aarch64__
			"1:	ldp		%0, %H0, [%1], #16	\n"
			"2:	stp		%0, %H0, [%2], #16	\n"
			"3:	cmp		%1, %3	\n"
			"	b.cc	1b	\n"
#else /* 32-bit ARM */
			"1:	ldrd	%0, %H0, [%1], #8	\n"
			"2:	strd	%0, %H0, [%2], #8	\n"
			"3:	cmp		%1, %3	\n"
			"	bcc		1b	\n"
#endif
			"	mov		%0, %4	\n"
			"4:	\n"

			".pushsection .text.exception,\"ax\"	\n"
			"5:	mov		%0, %5	\n"
			"	b		4b	\n"
			".popsection	\n"
			EX_TABLE(1b, 5b)
			EX_TABLE(2b, 5b)
			: "=&r" (tmp), "+&r" (src), "+&r" (dst)
			: "r" (end), "i" (EOK), "i" (EFAULT)
			: "memory");

		return (err_t)tmp;
	} else {
		unsigned long tmp;

		/* single word copy */
		__asm__ volatile (
			"	b		3f	\n"
#ifdef __aarch64__
			"1:	ldr		%0, [%1], #8	\n"
			"2:	str		%0, [%2], #8	\n"
			"3:	cmp		%1, %3	\n"
			"	b.cc	1b	\n"
#else /* 32-bit ARM */
			"1:	ldr		%0, [%1], #4	\n"
			"2:	str		%0, [%2], #4	\n"
			"3:	cmp		%1, %3	\n"
			"	bcc		1b	\n"
#endif
			"	mov		%0, %4	\n"
			"4:	\n"

			".pushsection .text.exception,\"ax\"	\n"
			"5:	mov		%0, %5	\n"
			"	b		4b	\n"
			".popsection	\n"
			EX_TABLE(1b, 5b)
			EX_TABLE(2b, 5b)
			: "=&r" (tmp), "+&r" (src), "+&r" (dst)
			: "r" (end), "i" (EOK), "i" (EFAULT)
			: "memory");

		return (err_t)tmp;
	}
}

/* string.S */
err_t arch_user_strlcpy_in(char *dst, const char *src_user, size_t n);
err_t arch_user_bzero(void *s_user, size_t n);
err_t arch_user_memcpy(void *dst_user, const void *src_user, size_t n);

#endif
