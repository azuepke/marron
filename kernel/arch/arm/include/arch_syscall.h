/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2018, 2021 Alexander Zuepke */
/*
 * arch_syscall.h
 *
 * ARM-specific syscall stubs.
 *
 * azuepke, 2018-01-03: imported && reworked
 * azuepke, 2021-01-16: 64-bit system calls
 */

#ifndef ARCH_SYSCALL_H_
#define ARCH_SYSCALL_H_

#ifdef __aarch64__

#define SYSCALL_FUNC(typ, func, sys)	\
	.global func				;\
	.type func, %function		;\
	.balign 16					;\
	func:						;\
	_SYSCALL_ ## typ(sys)		;\
	.size func, . - func		;

/* ARM macros -- NOTE: due to the use of "#" in ARM assembler,
 * we cannot use the C preprocessor for these macros!
 */

/* uint64_t syscall(a, b, c, d, e, f, g, h); -- 8 arguments in registers */
.macro _SYSCALL_IN8, name
	mov 	x8, #\name
	svc		#0
	ret
.endm

#define _SYSCALL_IN0(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN1(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN2(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN3(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN4(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN5(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN6(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN7(name)	_SYSCALL_IN8(name)

/* void abort(caller_addr) _noreturn */
.macro _SYSCALL_ABORT1, name
	/* caller address */
	sub		x0, lr, #4
	mov 	x8, #\name
	svc		#0
.endm

#else /* 32-bit ARM */

#define SYSCALL_FUNC(typ, func, sys)	\
	.syntax unified				;\
	.global func				;\
	.type func, %function		;\
	.balign 16					;\
	func:						;\
	_SYSCALL_ ## typ(sys)		;\
	.size func, . - func		;

/* ARM macros -- NOTE: due to the use of "#" in ARM assembler,
 * we cannot use the C preprocessor for these macros!
 */

/* uint64_t syscall(a, b, c, d); -- up to 4 arguments in registers */
.macro _SYSCALL_IN4, name
	mov 	r12, #\name
	svc		#0
	bx		lr
.endm

#define _SYSCALL_IN0(name)	_SYSCALL_IN4(name)
#define _SYSCALL_IN1(name)	_SYSCALL_IN4(name)
#define _SYSCALL_IN2(name)	_SYSCALL_IN4(name)
#define _SYSCALL_IN3(name)	_SYSCALL_IN4(name)


/* uint64_t syscall(a, b, c, d, e, f); -- e and f are kept on stack */
.macro _SYSCALL_IN6, name
	push	{r4, r5}
	ldrd	r4, r5, [sp, #8]
	mov 	r12, #\name
	svc		#0
	pop		{r4, r5}
	bx		lr
.endm

#define _SYSCALL_IN5(name)	_SYSCALL_IN6(name)


/* uint64_t syscall(a, b, c, d, e, f, g, h); -- e to h are kept on stack */
.macro _SYSCALL_IN8, name
	push	{r4, r5, r6, r7}
	ldrd	r4, r5, [sp, #16]
	ldrd	r6, r7, [sp, #24]
	mov 	r12, #\name
	svc		#0
	pop		{r4, r5, r6, r7}
	bx		lr
.endm

#define _SYSCALL_IN7(name)	_SYSCALL_IN8(name)


/* void abort(caller_addr) _noreturn */
.macro _SYSCALL_ABORT1, name
	/* caller address */
	sub		r0, lr, #4
	mov 	r12, #\name
	svc		#0
.endm

#endif

#endif
