/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2017, 2021 Alexander Zuepke */
/*
 * arch.h
 *
 * Architecture abstraction layer.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-07-17: reduced version for new kernel
 * azuepke, 2017-09-29: imported and adapted
 * azuepke, 2021-08-18: split arch.h and arch_defs.h
 */

#ifndef ARCH_H_
#define ARCH_H_

/** unsigned 64-bit integer type */
#ifndef __DEFINED_uint64_t
#define __DEFINED_uint64_t
#if __SIZEOF_LONG__ == 8
typedef unsigned long int uint64_t;
#else
typedef unsigned long long int uint64_t;
#endif
#endif


/* forward declaration */
struct regs;
struct arch_percpu;
struct adspace;

/* exception.c and exception64.c */
void arch_dump_registers(struct regs *regs, unsigned int status, unsigned long addr);
void arch_percpu_init(struct arch_percpu *arch, unsigned int cpu_id);
void arch_init_exceptions(void);

/* string.S */
void fast_clear_page(void *page);
void fast_copy_page(void *dst, const void *src);


/* for kernel boot message */
#if defined(ARM_V8)
#define ARCH_BANNER_NAME "ARMv8"
#elif defined(ARM_V7) && defined(ARM_LPAE)
#define ARCH_BANNER_NAME "ARMv7 LPAE"
#else
#define ARCH_BANNER_NAME "ARMv7"
#endif

/* address conversion */
#define PHYS_TO_KERNEL(x)	((void *)((unsigned long)(x) + adspace_phys_to_kernel_offset))
#define KERNEL_TO_PHYS(x)	((unsigned long)(x) - adspace_phys_to_kernel_offset)

/* generated data */
extern const unsigned long adspace_phys_to_kernel_offset;

/** check if an interrupt is currently pending (for preemption by interrupts) */
static inline int arch_irq_pending(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, ISR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c12, c1, 0" : "=r"(val));
#endif
	/* the CPSR_I bit is set if an interrupt is currently pending to the core */
	return (val & 0x80) != 0;
}

/** read 64-bit values atomically */
static inline uint64_t arch_load64(uint64_t *addr)
{
	uint64_t val;

#ifdef __aarch64__
	__asm__ volatile ("ldr %0, %1" : "=r" (val) : "m" (*addr));
#elif defined(ARM_LPAE)
	/* NOTE: LDRD is atomic on ARMv7 with LPAE, but not on older ones */
	__asm__ volatile ("ldrd %0, %H0, %1" : "=&r" (val) : "m" (*addr));
#else
	__asm__ volatile ("ldrexd %0, %H0, %1" : "=&r" (val) : "Q" (*addr));
#endif

	return val;
}

/** write 64-bit values atomically */
static inline void arch_store64(uint64_t *addr, uint64_t val)
{
#ifdef __aarch64__
	__asm__ volatile ("str %1, %0" : "=m" (*addr) : "r" (val));
#elif defined(ARM_LPAE)
	/* NOTE: LDRD is atomic on ARMv7 with LPAE, but not on older ones */
	__asm__ volatile ("strd %1, %H1, %0" : "=m" (*addr) : "r" (val));
#else
	uint64_t tmp;

	__asm__ volatile (
		"1:	ldrexd	%0, %H0, %1\n"
		"	strexd	%0, %2, %H2, %1\n"
		"	cmp		%0, #1\n"
		"	beq		1b\n"
		: "=&r" (tmp), "+Q" (*addr) : "r" (val) : "cc");
#endif
}

#ifdef __arm__
/** 64 / 32 -> 32 bit division */
unsigned int udiv64_32(unsigned long long u, unsigned int v);
/** 64 / 64 -> 64 bit division */
unsigned long long udiv64_64(unsigned long long u, unsigned long long v);
#endif

#ifdef __arm__
#define KMEM_PAGETABLE_SIZE 0x400	/* PT_SIZE */
#else
#define KMEM_PAGETABLE_SIZE PAGE_SIZE
#endif

#endif
