/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2021 Alexander Zuepke */
/*
 * exception64.c
 *
 * Architecture specific exception handling
 *
 * azuepke, 2013-09-15: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2021-01-16: port to 64-bit ARM
 */

#include <arch.h>
#include <panic.h>
#include <kernel.h>
#include <bsp.h>
#include <assert.h>
#include <arm_private.h>
#include <arm_insn.h>
#include <arm_cr.h>
#include <arch_context.h>
#include <arm_perf.h>
#include <percpu.h>
#include <ex_table.h>
#include <marron/signal.h>


__cold void arch_dump_registers(struct regs *regs, unsigned int status, unsigned long addr)
{
	assert(regs != NULL);

	printk("x0-3        %lx %lx %lx %lx\n",
	       regs->r[0], regs->r[1], regs->r[2], regs->r[3]);
	printk("x4-7        %lx %lx %lx %lx\n",
	       regs->r[4], regs->r[5], regs->r[6], regs->r[7]);
	printk("x8-11       %lx %lx %lx %lx\n",
	       regs->r[8], regs->r[9], regs->r[10], regs->r[11]);
	printk("x12-15      %lx %lx %lx %lx\n",
	       regs->r[12], regs->r[13], regs->r[14], regs->r[15]);
	printk("x16-19      %lx %lx %lx %lx\n",
	       regs->r[16], regs->r[17], regs->r[18], regs->r[19]);
	printk("x20-23      %lx %lx %lx %lx\n",
	       regs->r[20], regs->r[21], regs->r[22], regs->r[23]);
	printk("x24-27      %lx %lx %lx %lx\n",
	       regs->r[24], regs->r[25], regs->r[26], regs->r[27]);
	printk("x28-lr,sp   %lx %lx %lx %lx\n",
	       regs->r[28], regs->r[29], regs->lr, regs->sp);
	printk("pc,sr,pf,ex %lx         %08x %lx         %08x\n",
	       regs->pc, (unsigned int)regs->cpsr, addr, status);
}

/** exception recovery (expects a sorted table!) */
static int try_exception_recovery(struct regs *regs)
{
	const struct ex_table *start = &__ex_table_start;
	const struct ex_table *end = &__ex_table_end;
	const struct ex_table *middle;

	assert(start <= end);
	do {
		middle = start + ((end - start) >> 1);

		if (middle->addr == regs->pc) {
			/* match */
			regs->pc = middle->cont;
			return 1;
		} else if (middle->addr > regs->pc) {
			/* search on left side */
			end = middle - 1;
		} else {
			/* search on right side */
			start = middle + 1;
		}
	} while (start <= end);

	return 0;
}

__cold void arm_handler_fiq(struct regs *regs)
{
	panic_regs(regs, 0, 0, "Unsupported FIQ\n");
}

__cold void arm_handler_serror(struct regs *regs, ulong_t esr, ulong_t far)
{
	panic_regs(regs, esr & 0xffffffff, far, "Unsupported SError exception\n");
}

void arm_handler_irq_user(struct regs *regs __unused)
{
	bsp_irq_dispatch(0);
}

__cold void arm_handler_irq_kern(struct regs *regs)
{
	panic_regs(regs, 0, 0, "Unsupported IRQ in kernel mode\n");
}

__cold void arm_handler_sync_kern(struct regs *regs, ulong_t esr, ulong_t far)
{
	/* FIXME: check for critical exceptions first! */
	if (try_exception_recovery(regs)) {
		return;
	}

	panic_regs(regs, esr & 0xffffffff, far, "Unsupported exception in kernel mode\n");
}

void arm_handler_sync_user(struct regs *regs, ulong_t esr, ulong_t far)
{
	unsigned long ec;
	unsigned int sig;
	unsigned int si_code;
	unsigned int ex_info;
	unsigned long addr;

	addr = 0;

	ec = esr >> ESR_EC_SHIFT;
	switch (ec) {
	case ESR_EC_IABT_USER:
	case ESR_EC_DABT_USER:
		if ((esr & ESR_FAULT_FNV) != 0) {
			addr = far;
		}

		/* instruction or data abort needs further dispatching */
		switch (esr & ESR_FSC_MASK) {
		case ESR_FSC_ADDR_SIZE:
		case ESR_FSC_TRANSLATION:
			sig = SIGSEGV;
			si_code = SEGV_MAPERR;
			ex_info = EX_TYPE_PAGEFAULT;
			break;

		case ESR_FSC_ACCESS_FLAG:
		case ESR_FSC_PERM:
			sig = SIGSEGV;
			si_code = SEGV_ACCERR;
			ex_info = EX_TYPE_PAGEFAULT | EX_PERM;
			break;

		case ESR_FSC_SYNCEA:
		case ESR_FSC_SYNCEA_PT:
		case ESR_FSC_PARITY:
		case ESR_FSC_PARITY_PT:
			sig = SIGBUS;
			si_code = BUS_OBJERR;
			ex_info = EX_TYPE_BUS;
			break;

		case ESR_FSC_ALIGN:
			sig = SIGBUS;
			si_code = BUS_ADRALN;
			ex_info = EX_TYPE_ALIGN;
			break;

		case ESR_FSC_IMPL_DEF:
			if ((esr & 0x3) == 0x1) {
				/* unsupported exclusive access */
				sig = SIGBUS;
				si_code = BUS_OBJERR;
				ex_info = EX_TYPE_BUS;
				break;
			}
			fallthrough;

		default:
			panic_regs(regs, esr & 0xffffffff, far, "Unsupported abort or TLB error\n");
		}

		/* Fault access type:
		 * cache maintenance are reported as write faults,
		 * but actually require read permissions.
		 * We report a read fault to user space.
		 */
		if (((esr & ESR_FAULT_WR) != 0) && ((esr & ESR_FAULT_CM) == 0)) {
			ex_info |= EX_WRITE;
		} else if (ec == ESR_EC_DABT_USER) {
			ex_info |= EX_READ;
		} else {
			ex_info |= EX_EXEC;
		}
		break;

	case ESR_EC_FPU_UNAVAIL:
		sig = SIGFPE;
		si_code = FPE_UNAVAIL;
		ex_info = EX_TYPE_FPU_UNAVAIL;
		break;

	case ESR_EC_FPU32:
	case ESR_EC_FPU64:
		sig = SIGFPE;
		si_code = FPE_FLTUNK;
		if ((esr & ESR_FPEXC_TFV) != 0) {
			if ((esr & ESR_FPEXC_IOF) != 0) {
				si_code = FPE_FLTINV;
			} else if ((esr & ESR_FPEXC_DZF) != 0) {
				si_code = FPE_FLTDIV;
			} else if ((esr & ESR_FPEXC_OFF) != 0) {
				si_code = FPE_FLTOVF;
			} else if ((esr & ESR_FPEXC_UFF) != 0) {
				si_code = FPE_FLTUND;
			} else if ((esr & ESR_FPEXC_IXF) != 0) {
				si_code = FPE_FLTRES;
			}
		}
		ex_info = EX_TYPE_FPU_ERROR;
		break;

	case ESR_EC_ILLEGAL:
		panic_regs(regs, esr & 0xffffffff, 0, "Illegal execution state\n");

	case ESR_EC_SVC32:
	case ESR_EC_SVC64:
		sig = SIGILL;
		si_code = ILL_ILLTRP;
		ex_info = EX_TYPE_SYSCALL;
		break;

	case ESR_EC_BREAK_USER:
		sig = SIGTRAP;
		si_code = TRAP_HWBKPT;
		ex_info = EX_TYPE_DEBUG | EX_EXEC;
		break;

	case ESR_EC_STEP_USER:
		sig = SIGTRAP;
		si_code = TRAP_TRACE;
		ex_info = EX_TYPE_DEBUG;
		break;

	case ESR_EC_WATCH_USER:
		addr = far;
		sig = SIGTRAP;
		si_code = TRAP_HWBKPT;
		if (((esr & ESR_FAULT_WR) != 0) && ((esr & ESR_FAULT_CM) == 0)) {
			ex_info = EX_TYPE_DEBUG | EX_WRITE;
		} else {
			ex_info = EX_TYPE_DEBUG | EX_READ;
		}
		break;

	case ESR_EC_BRK32:
	case ESR_EC_BRK64:
		sig = SIGTRAP;
		si_code = TRAP_BRKPT;
		ex_info = EX_TYPE_TRAP;
		break;

	case ESR_EC_PC_ALIGN:
		addr = far;
		sig = SIGBUS;
		si_code = BUS_ADRALN;
		ex_info = EX_TYPE_ALIGN | EX_EXEC;
		break;

	case ESR_EC_SP_ALIGN:
		/* The hardware does not provide a fault address,
		 * so we provide the stack pointer here.
		 */
		addr = regs->sp;
		sig = SIGBUS;
		si_code = BUS_ADRALN;
		ex_info = EX_TYPE_ALIGN | EX_READ | EX_WRITE;
		break;

	default:
		/* We map the rest as illegal instruction. */
		sig = SIGILL;
		si_code = ILL_ILLOPC;
		ex_info = EX_TYPE_ILLEGAL;
		break;
	}
	kernel_exception(regs, sig, si_code, ex_info, esr & 0xffffffff, addr);
}

/** initializer for architecture-specific per-CPU data (called from CPU #0) */
__init void arch_percpu_init(struct arch_percpu *arch, unsigned int cpu_id)
{
	const struct percpu_cfg *cfg = &percpu_cfg[cpu_id];

	arch->kern_stack_top   = cfg->kern_stack_top;
	arch->fiq_stack_top    = cfg->nmi_stack_top;
	arch->serror_stack_top = cfg->mce_stack_top;

	arch->current_adspace = NULL;
}

static __init void arm_enable_timer_access(void)
{
#define CNTKCTL_EL0PCTEN	0x001	/* enable user read of physical counter */
#define CNTKCTL_EL0VCTEN	0x002	/* enable user read of virtual counter */
#define CNTKCTL_EVNTEN		0x004	/* enable event generation */
#define CNTKCTL_EVNTDIR		0x008	/* event direction, 0:0->1, 1:1->0 */
#define CNTKCTL_EVNTI(x)	((x)<<4)	/* counter bit that triggers event */
#define CNTKCTL_EL0PTEN		0x100	/* enable user write to physical counter */
#define CNTKCTL_EL0VTEN		0x200	/* enable user write to virtual counter */

	/* allow user to read the virtual timer value */
	arm_set_cnt_access(CNTKCTL_EL0VCTEN);
}

static __init void arm_init_pmu(void)
{
	unsigned int num_counters;
	unsigned int ctrl;

	/*
	 * ARMv7 and v8 define a cycle counter and up to 31 individual counters.
	 * If available, we configure counters 0 and 2 to monitor cache
	 * and bus activity.
	 */
	num_counters = (arm_perf_get_ctrl() >> 11) & 0x1f;

	/* disable all IRQs and clear all overflows */
	arm_perf_int_mask(ARM_PERF_MASK_ALL);
	arm_perf_int_ack(ARM_PERF_MASK_ALL);

	/* enable L2 cache activity monitoring in counters 0 to 2 */
	if (num_counters >= 3) {
		arm_perf_type0(ARM_PERF_EVENT_DC2R);
		arm_perf_type1(ARM_PERF_EVENT_DC2W);
		arm_perf_type2(ARM_PERF_EVENT_BA);
		arm_perf_enable_counter((1<<0) | (1<<1) | (1<<2));
	}
	arm_perf_enable_counter(ARM_PERF_MASK_CCNT);

	/* enable cycle and event counters, do not reset any counters */
	ctrl = arm_perf_get_ctrl();
	ctrl &= ~ARM_PERF_PMCR_D;
	ctrl |= ARM_PERF_PMCR_C | ARM_PERF_PMCR_E;
	arm_perf_set_ctrl(ctrl);

	/* keep cycle counter running in EL2 */
	arm_perf_ccfilter(ARM_PERF_TYPER_NSH);

	/* enable read access to cycle and event counters from user space */
	arm_perf_set_useren(ARM_PERF_USERENR_CR | ARM_PERF_USERENR_ER);
}

static __init void arm_init_debugging(void)
{
	unsigned int mdscr;

	mdscr = arm_get_mdscr();

	/* let accesses to debug registers from EL0 trap */
	mdscr |= MDSCR_TDCC;
	/* disable debugging features by default */
	mdscr &= ~(MDSCR_SS | MDSCR_KDE | MDSCR_MDE);

	arm_set_mdscr(mdscr);
}

__init void arch_init_exceptions(void)
{
	/* finally switch to direct vectors, if supported */
	arm_set_vbar((unsigned long)arm_vectors_el1);

	arm_enable_timer_access();
	arm_init_pmu();
	arm_init_debugging();
}
