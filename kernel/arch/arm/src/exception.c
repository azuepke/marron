/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2021 Alexander Zuepke */
/*
 * exception.c
 *
 * Architecture specific exception handling
 *
 * azuepke, 2013-09-15: initial
 * azuepke, 2017-10-02: imported and adapted
 */

#include <arch.h>
#include <panic.h>
#include <kernel.h>
#include <bsp.h>
#include <assert.h>
#include <arm_private.h>
#include <arm_insn.h>
#include <arm_cr.h>
#include <arch_context.h>
#include <uaccess.h>
#include <arm_perf.h>
#include <percpu.h>
#include <ex_table.h>
#include <marron/signal.h>


__cold void arch_dump_registers(struct regs *regs, unsigned int status, unsigned long addr)
{
	assert(regs != NULL);

	printk("r0  %lx  r1  %lx  r2  %lx  r3  %lx\n",
	       regs->r[0], regs->r[1], regs->r[2], regs->r[3]);

	printk("r4  %lx  r5  %lx  r6  %lx  r7  %lx\n",
	       regs->r[4], regs->r[5], regs->r[6], regs->r[7]);

	printk("r8  %lx  r9  %lx  r10 %lx  r11 %lx\n",
	       regs->r[8], regs->r[9], regs->r[10], regs->r[11]);

	printk("r12 %lx  sp  %lx  lr  %lx  pc  %lx\n",
	       regs->r[12], regs->sp, regs->lr, regs->pc);

	printk("psr %lx  fsr %lx addr %lx  tls %lx\n",
	       regs->cpsr, (ulong_t)status, addr, regs->tls1);
}

/** exception recovery (expects a sorted table!) */
static int try_exception_recovery(struct regs *regs)
{
	const struct ex_table *start = &__ex_table_start;
	const struct ex_table *end = &__ex_table_end;
	const struct ex_table *middle;

	assert(start <= end);
	do {
		middle = start + ((end - start) >> 1);

		if (middle->addr == regs->pc) {
			/* match */
			regs->pc = middle->cont;
			return 1;
		} else if (middle->addr > regs->pc) {
			/* search on left side */
			end = middle - 1;
		} else {
			/* search on right side */
			start = middle + 1;
		}
	} while (start <= end);

	return 0;
}

void arm_handler_undef_user(struct regs *regs)
{
	unsigned long fpexc;
	unsigned long inst;
	unsigned int sig;
	unsigned int si_code;
	unsigned int ex_info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	/* robust copy of instruction from user space */
	__asm__ volatile (
		"	mov		%0, #0	\n"	/* do not leak kernel registers */
		"1:	ldr		%0, %1	\n"
		"2:	\n"
		EX_TABLE(1b, 2b)
		: "=&r" (inst)
		: "m" (*(const unsigned int*)regs->pc));

	/* SIGILL by default */
	sig = SIGILL;
	si_code = ILL_ILLOPC;
	ex_info = EX_TYPE_ILLEGAL;
	fpexc = arm_get_fpexc();
	if (fpexc & FPEXC_EN) {
		/* clear VFP exceptions */
		if (fpexc & FPEXC_EX) {
			arm_set_fpexc(FPEXC_EN);
			/* raise SIGFPE */
			sig = SIGFPE;
			si_code = FPE_FLTUNK;
			ex_info = EX_TYPE_FPU_ERROR;
		}
	} else if (regs->cpsr & CPSR_T) {
		/* decode thumb opcode:
		 * - VFP load store
		 * - VFP data op
		 * - coprocessor transfer to c10/c11
		 */
		if (((inst & 0x0000ff10) == 0x0000f900) ||
		    ((inst & 0x0000ef00) == 0x0000ef00) ||
		    ((inst & 0x0e00fc00) == 0x0a00ec00))
		{
			/* FPU is disabled */
			sig = SIGFPE;
			si_code = FPE_UNAVAIL;
			ex_info = EX_TYPE_FPU_UNAVAIL;
		}
	} else {
		/* decode ARM opcode:
		 * - VFP load store
		 * - VFP data op
		 * - coprocessor transfer to c10/c11
		 */
		if (((inst & 0xff100000) == 0xf4000000) ||
		    ((inst & 0xfe000000) == 0xf2000000) ||
		    ((inst & 0x0c000e00) == 0x0c000a00))
		{
			/* FPU is disabled */
			sig = SIGFPE;
			si_code = FPE_UNAVAIL;
			ex_info = EX_TYPE_FPU_UNAVAIL;
		}
	}
	kernel_exception(regs, sig, si_code, ex_info, 0, inst);
}

void arm_handler_undef_kern(struct regs *regs)
{
	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_SVC);

	panic_regs(regs, 0, 0, "KERNEL UNDEF\n");
}

#ifdef ARM_LPAE
/*
 * ARMv7-LPAE FSR encoding:
 * 0001 LL  Translation fault
 * 0010 LL  Access fault
 * 0011 LL  Permission fault
 * 0100 00  Synchronous external abort
 * 0110 00  Synchronous parity error
 * 0100 01  Asynchronous external abort
 * 0110 01  Asynchronous parity error
 * 0101 LL  Synchronous external abort on translation table walk
 * 0111 LL  Synchronous parity error on memory access on translation table walk
 * 1000 01  Alignment fault
 * 1000 10  Debug event
 */
void arm_handler_dabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned int sig;
	unsigned int si_code;
	unsigned int ex_info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	status = fsr & FSR_FSMASK;

	switch (status >> 2) {
	case 0x8:
		if (status == 0x21) {
			/* alignment fault */
			sig = SIGBUS;
			si_code = BUS_ADRALN;
			ex_info = EX_TYPE_ALIGN;
		} else {
			assert(status == 0x22);
			/* debug event */
			sig = SIGTRAP;
			si_code = TRAP_BRKPT;
			ex_info = EX_TYPE_TRAP;
		}
		break;
	case 0x2:
		/* access fault */
		sig = SIGSEGV;
		si_code = SEGV_MAPERR;
		ex_info = EX_TYPE_PAGEFAULT;
		break;
	case 0x3:
		/* permission fault */
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_PERM;
		break;
	default:
		panic_regs(regs, fsr, addr, "USER DATA ABORT at 0x%lx, fsr=%lx\n", addr, fsr);
		break;
		/* FIXME: ... add more encodings ... */
	}

	if (fsr & FSR_WRITE) {
		/* write access */
		ex_info |= EX_WRITE;
	} else {
		/* read access */
		ex_info |= EX_READ;
	}

	kernel_exception(regs, sig, si_code, ex_info, fsr, addr);
}

void arm_handler_pabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned int sig;
	unsigned int si_code;
	unsigned int ex_info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	status = fsr & FSR_FSMASK;

	switch (status >> 2) {
	case 0x8:
		if (status == 0x21) {
			/* alignment fault */
			sig = SIGBUS;
			si_code = BUS_ADRALN;
			ex_info = EX_TYPE_ALIGN;
		} else {
			assert(status == 0x22);
			/* debug event */
			sig = SIGTRAP;
			si_code = TRAP_BRKPT;
			ex_info = EX_TYPE_TRAP;
		}
		break;
	case 0x2:
		/* access fault */
		sig = SIGSEGV;
		si_code = SEGV_MAPERR;
		ex_info = EX_TYPE_PAGEFAULT;
		break;
	case 0x3:
		/* permission fault */
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_PERM;
		break;
	default:
		panic_regs(regs, fsr, addr, "USER DATA PREFETCH at 0x%lx, fsr=%lx\n", addr, fsr);
		break;
		/* FIXME: ... add more encodings ... */
	}

	/* ifetch is always type exec access */
	ex_info |= EX_EXEC;

	kernel_exception(regs, sig, si_code, ex_info, fsr, addr);
}

void arm_handler_dabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	/* FIXME: check for critical exceptions first! */
	if (try_exception_recovery(regs)) {
		return;
	}

	panic_regs(regs, fsr, addr, "KERNEL DATA ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}

void arm_handler_pabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	panic_regs(regs, fsr, addr, "KERNEL PREFETCH ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}

#else /* pre-LPAE ARMv7 version */

/*
 * The FSR status bits encoding that does not cause a panic is:
 *
 *  Status	DI	Ext	Description
 *  0 0001	D-	-	alignment fault
 *  0 0010	DI	-	debug event
 *  0 0100	D-	-	Icache maintenance fault
 *  0 01x1	DI	-	translation fault (no mapping)
 *  0 1000	DI	E	synchronous external abort (error on device access)
 *  0 11x1	DI	-	permission fault (due to APx or nX bits)
 *  1 1001	DI	E	synchronous parity error (error on device access)
 *
 * TODO:
 *  0 11?0	D?	E	L1/L2 translation precise external abort
 *  1 11?0	D?	E	L1/L2 translation precise parity error
 *  0 0011	D?	-	access fault, section
 *  0 0110	D?	-	access fault, page
 *  0 1001  D?	-	domain fault, section
 *  0 1011  D?	-	domain fault, page
 *  0 1101	D?	-	permission fault, section
 *  0 1111	D?	-	permission fault, page
 *  0 1000	D?	E	precise external abort, nontranslation
 *  1 0110	D?	E	imprecise external abort
 *  1 1000	D?	E	imprecise parity or ECC error
 */
#define FSR_PANIC	0xfdff5e49

void arm_handler_dabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned int sig;
	unsigned int si_code;
	unsigned int ex_info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	/* construct contiguous abort state */
	status = fsr & FSR_FSMASK;
	if (fsr & FSR_FSBIT4) {
		status |= 0x10;
	}

	{
		/* FIXME: ignore first external abort exception caused by bootloader */
		static int once = 1;
		if (once == 1 && FSR_PANIC & (1u << status)) {
			return;
		}
		once = 0;
	}

	/* catch bad faults */
	if (unlikely(FSR_PANIC & (1u << status))) {
		panic_regs(regs, fsr, addr, "USER DATA ABORT: bad fault at 0x%lx, fsr=%lx\n", addr, fsr);
	}

	if (status == 1) {
		/* alignment fault */
		sig = SIGBUS;
		si_code = BUS_ADRALN;
		ex_info = EX_TYPE_ALIGN;
	} else if (status == 2) {
		/* debug event */
		sig = SIGTRAP;
		si_code = TRAP_BRKPT;
		ex_info = EX_TYPE_TRAP;
	} else if (status & 0x4) {
		/* standard page fault or icache maintainance fault */
		sig = SIGSEGV;
		if (fsr & 0x8) {
			/* permission fault */
			si_code = SEGV_ACCERR;
			ex_info = EX_TYPE_PAGEFAULT | EX_PERM;
		} else {
			/* no mapping */
			si_code = SEGV_MAPERR;
			ex_info = EX_TYPE_PAGEFAULT;
		}
	} else {
		/* synchronous external abort or parity error */
		sig = SIGBUS;
		si_code = BUS_OBJERR;
		ex_info = EX_TYPE_BUS;
	}

	if (fsr & FSR_WRITE) {
		/* write access */
		ex_info |= EX_WRITE;
	} else {
		/* read access */
		ex_info |= EX_READ;
	}

	kernel_exception(regs, sig, si_code, ex_info, fsr, addr);
}

void arm_handler_pabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned int sig;
	unsigned int si_code;
	unsigned int ex_info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	/* construct contiguous abort state */
	status = fsr & FSR_FSMASK;
	if (fsr & FSR_FSBIT4) {
		status |= 0x10;
	}

	/* catch bad faults */
	if (unlikely(FSR_PANIC & (1u << status))) {
		panic_regs(regs, fsr, addr, "USER PREFETCH ABORT: bad fault at 0x%lx, fsr=%lx\n", addr, fsr);
	}

	if (status == 2) {
		/* debug event */
		sig = SIGTRAP;
		si_code = TRAP_BRKPT;
		ex_info = EX_TYPE_TRAP;
	} else if (status & 0x4) {
		/* standard page fault or icache maintainance fault */
		sig = SIGSEGV;
		if (fsr & 0x8) {
			/* permission fault */
			si_code = SEGV_ACCERR;
			ex_info = EX_TYPE_PAGEFAULT | EX_PERM;
		} else {
			/* no mapping */
			si_code = SEGV_MAPERR;
			ex_info = EX_TYPE_PAGEFAULT;
		}
	} else {
		/* synchronous external abort or parity error */
		sig = SIGBUS;
		si_code = BUS_OBJERR;
		ex_info = EX_TYPE_BUS;
	}

	/* ifetch is always type exec access */
	ex_info |= EX_EXEC;

	kernel_exception(regs, sig, si_code, ex_info, fsr, addr);
}

void arm_handler_dabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	/* FIXME: check for critical exceptions first! */
	if (try_exception_recovery(regs)) {
		return;
	}

	panic_regs(regs, fsr, addr, "KERNEL DATA ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}

void arm_handler_pabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	panic_regs(regs, fsr, addr, "KERNEL PREFETCH ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}
#endif

void arm_handler_irq(struct regs *regs __unused)
{
	bsp_irq_dispatch(0);
}

void arm_handler_fiq(struct regs *regs)
{
	panic_regs(regs, 0, 0, "FIQ\n");
}

/** initializer for architecture-specific per-CPU data (called from CPU #0) */
__init void arch_percpu_init(struct arch_percpu *arch, unsigned int cpu_id __unused)
{
	const struct percpu_cfg *cfg = &percpu_cfg[cpu_id];

	arch->kern_stack_top   = cfg->kern_stack_top;
	/* the FIQ stack was already set by arch_entry() */

	arch->current_adspace = NULL;
}

static __init void arm_init_pmu(void)
{
	unsigned int ctrl;

	/*
	 * ARMv7 and v8 define a cycle counter and up to 31 individual counters.
	 * We keep the individual counters disabled except for the cycle counter.
	 */

	/* disable all IRQs and clear all overflows */
	arm_perf_int_mask(ARM_PERF_MASK_ALL);
	arm_perf_int_ack(ARM_PERF_MASK_ALL);

	arm_perf_enable_counter(ARM_PERF_MASK_CCNT);

	/* enable cycle and event counters, do not reset any counters */
	ctrl = arm_perf_get_ctrl();
	ctrl &= ~ARM_PERF_PMCR_D;
	ctrl |= ARM_PERF_PMCR_C | ARM_PERF_PMCR_E;
	arm_perf_set_ctrl(ctrl);

	/* enable access to performance monitor registers from user space */
	/* NOTE: on ARMv7, this enables write access to all registers! */
	arm_perf_set_useren(ARM_PERF_USEREN);
}

__init void arch_init_exceptions(void)
{
	/* finally switch to direct vectors, if supported */
	arm_set_vbar((unsigned long)arm_vectors);

	arm_init_pmu();
}
