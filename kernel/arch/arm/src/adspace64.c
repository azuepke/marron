/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2021, 2023, 2025 Alexander Zuepke */
/*
 * adspace64.c
 *
 * Architecture specific address space handling
 *
 * azuepke, 2021-04-24: from exception64.c
 * azuepke, 2021-04-29: imported mapping code
 * azuepke, 2023-08-01: support large mappings in user page tables
 * azuepke, 2025-02-21: adspace rework
 */

#include <kernel.h>
#include <stddef.h>
#include <arch.h>
#include <assert.h>
#include <panic.h>
#include <adspace.h>
#include <arm_insn.h>
#include <arm_pte_v8.h>
#include <current.h>
#include <uaccess.h>
#include <part_types.h>
#include <stdbool.h>
#include <marron/compiler.h>
#include <marron/cache_type.h>
#include <part.h>


/* align vaddr to the start of the next table */
#define NEXT_TABLE(addr, align)		(((addr) + (align)) & ~((align) - 1ul))


/* kernel first-level page tables */
arm_pte_t arch_adspace_kern_pagetable_ttbr0[PTE_L1_NUM] __aligned(PTE_L1_ALIGN) __section(.bss..pagetables.l1);
arm_pte_t arch_adspace_kern_pagetable_ttbr1[PTE_L1_NUM] __aligned(PTE_L1_ALIGN) __section(.bss..pagetables.l1);

/* writable pagetable for cross-copy mapping */
arm_pte_t arch_adspace_xmap_pagetable[PTE_L3_NUM] __aligned(PTE_L3_ALIGN) __section(.bss..pagetables.rw);

/* special NULL first-level page table */
arm_pte_t arch_adspace_null_pagetable[PTE_L1_NUM] __aligned(PTE_L1_ALIGN) __section(.bss..pagetables.l1);


/** initialize all adspace objects at boot time */
__init void arch_adspace_init_all(void)
{
	for (unsigned int i = 0; i < adspace_num; i++) {
		struct adspace *adspace = &adspace_dyn[i];

		/* adspace->part is set in part.c */
		spin_init(&adspace->lock);
		adspace->asid = i;
		adspace->cpu_active_mask = 0;
		adspace->cpu_inval_mask = 0;
		if (i == 0) {
			adspace->pagetable = arch_adspace_kern_pagetable_ttbr0;
		} else {
			adspace->pagetable = NULL;
		}

		/* NOTE: initialization of VMA data is implemented in vma.c */
	}
}

/** change MMU page tables on context switch */
static inline void arch_adspace_switch_pagetable(void *pt, unsigned long asid)
{
	arm_set_ttbr0_lpae(KERNEL_TO_PHYS(pt), asid);
	arm_isb();
}

void arch_adspace_switch(struct adspace *prev, struct adspace *next)
{
	assert(prev != NULL);
	assert(next != NULL);

	arch_percpu()->arch.current_adspace = next;
	(void)prev;

	arch_adspace_switch_pagetable(next->pagetable, next->asid);
}


#ifdef DEBUG

/** dump an address space's page tables */
static inline void adspace_dump_pte(arm_pte_t pte)
{
	unsigned int sh;

	printk(" MT:%c%c%c",
	       (pte & PTE_ATTR2) ? '1' : '0',
	       (pte & PTE_ATTR1) ? '1' : '0',
	       (pte & PTE_ATTR0) ? '1' : '0');

	sh = (pte >> 8) & 0x3;
	printk(" SH_%cS", sh ? (sh == 2 ? 'O' : 'I') : 'N');

	printk(" %cr%c%c%c",
	       (pte & PTE_AP1)  ? 'u' : 'k',
	       (pte & PTE_AF)   ? 'r' : '-',
	       !(pte & PTE_AP2) ? 'w' : '-',
	       !(pte & PTE_XN)  ? 'x' : '-');

	if (pte & PTE_NG)
		printk(" NG");
	printk("\n");
}

static void adspace_dump(void *pagetable)
{
	unsigned int i, j, k;
	arm_pte_t *l1;
	arm_pte_t *l2;
	arm_pte_t *l3;

	printk("##### dump page table %p\n", pagetable);

	l1 = pagetable;
	for (i = 0; i < PTE_L1_NUM; i++) {
		/* valid? */
		if (l1[i] == 0) {
			continue;
		}

		/* block? */
		if ((l1[i] & PTE_TYPE_MASK) == PTE_TYPE_BLOCK) {
			printk("##### blck %x -> %llx", (i<<30), PTE_TO_PHYS(l1[i]));
			adspace_dump_pte(l1[i]);
			continue;
		}

		/* page table */
		assert(PTE_IS_PT(l1[i]));
		l2 = PTE_TO_VIRT(l1[i]);
		for (j = 0; j < PTE_L2_NUM; j++) {
			/* valid? */
			if (l2[j] == 0) {
				continue;
			}

			/* block? */
			if ((l2[j] & PTE_TYPE_MASK) == PTE_TYPE_BLOCK) {
				printk("##### blck %x -> %llx", (i<<30)|(j<<21), PTE_TO_PHYS(l2[j]));
				adspace_dump_pte(l2[j]);
				continue;
			}

			/* page table */
			assert(PTE_IS_PT(l2[j]));
			l3 = PTE_TO_VIRT(l2[j]);
			for (k = 0; k < PTE_L3_NUM; k++) {
				/* valid? */
				if (l3[k] == 0) {
					continue;
				}

				/* page */
				assert(PTE_IS_PAGE(l3[k]));
				if (PTE_IS_PAGE(l3[k])) {
					printk("##### page %x -> %llx", (i<<30)|(j<<21)|(k<<12), PTE_TO_PHYS(l3[k]));
					adspace_dump_pte(l3[k]);
					continue;
				}
			}
		}
	}
}

#endif

////////////////////////////////////////////////////////////////////////////////

/** write/update entry in page table */
static inline void arm_pte_set(arm_pte_t *ptep, arm_pte_t val)
{
	access_once(*ptep) = val;
	/* the barrier orders changes in the page tables w.r.t. page table walks */
	_arm_dsb("ISHST");
}


/** allocate a page from the pagetable KMEM page pool */
static inline void *alloc_table(struct adspace *adspace)
{
	void *page;

	spin_lock(&adspace->part->kmem_pagetables_lock);
	page = kmempool_get(&adspace->part->kmem_pagetables);
	spin_unlock(&adspace->part->kmem_pagetables_lock);

	if (page != NULL) {
		fast_clear_page(page);
	}

	assert(((addr_t)page & PAGE_OFFSET) == 0);
	return page;
}

/** put a page back to its pagetable KMEM page pool */
static inline void free_table(struct adspace *adspace, void *page)
{
	assert(page != NULL);
	assert(((addr_t)page & PAGE_OFFSET) == 0);

	spin_lock(&adspace->part->kmem_pagetables_lock);
	kmempool_put(&adspace->part->kmem_pagetables, page);
	spin_unlock(&adspace->part->kmem_pagetables_lock);
}


/** initializes a user address space */
err_t arch_adspace_init(struct adspace *adspace)
{
	adspace->pagetable = alloc_table(adspace);
	if (adspace->pagetable == NULL) {
		return ENOMEM;
	}

#ifndef NDEBUG
	/* the page table must be empty */
	for (unsigned int i = 0; i < PTE_L1_NUM; i++) {
		arm_pte_t *pt = adspace->pagetable;
		assert(pt[i] == 0);
	}
#endif

	return EOK;
}


/** internal routine to get a pointer to a level 3 table entry, with allocation */
static inline arm_pte_t *get_l3_table(struct adspace *adspace, arm_pte_t *l2_pte, addr_t va)
{
	arm_pte_t pte;
	arm_pte_t *pt;

	/* level 2 table */
	pte = *l2_pte;
	if (PTE_IS_PT(pte)) {
		pt = PTE_TO_VIRT(pte);
		goto hit;
	}

	/* no level 3 page table, allocate one */
	pt = alloc_table(adspace);
	if (unlikely(pt == NULL)) {
		return NULL;
	}

	/* ensure that new page table is visible in empty state */
	_arm_dsb("ISHST");

	/* put entry into l2 page table */
	assert(*l2_pte == 0);
	arm_pte_set(l2_pte, VIRT_TO_PTE(pt) | PTE_TYPE_PT | PTE_PT_PXN);

hit:
	return &pt[arm_addr_to_l3(va)];
}

/** internal routine to get a pointer to a level 2 table entry, with allocation */
static inline arm_pte_t *get_l2_table(struct adspace *adspace, arm_pte_t *l1_pte, addr_t va)
{
	arm_pte_t pte;
	arm_pte_t *pt;

	/* level 1 table */
	pte = *l1_pte;
	if (PTE_IS_PT(pte)) {
		pt = PTE_TO_VIRT(pte);
		goto hit;
	}

	/* no level 2 page table, allocate one */
	pt = alloc_table(adspace);
	if (unlikely(pt == NULL)) {
		return NULL;
	}

	/* ensure that new page table is visible in empty state */
	_arm_dsb("ISHST");

	/* put entry into l1 page table */
	assert(*l1_pte == 0);
	arm_pte_set(l1_pte, VIRT_TO_PTE(pt) | PTE_TYPE_PT | PTE_PT_PXN);

hit:
	return &pt[arm_addr_to_l2(va)];
}

/** internal routine to get a pointer to a level 1 table entry, no allocation */
static inline arm_pte_t *get_l1_table(struct adspace *adspace, addr_t va)
{
	arm_pte_t *pt;

	pt = adspace->pagetable;

	return &pt[arm_addr_to_l1(va)];
}

/** permission bits encoding */
static inline arm_pte_t prot_bits(unsigned int prot)
{
	arm_pte_t pte_bits;

	/* we set PTE_PXN for all user space mappings */
	pte_bits = PTE_PXN;
	if (prot != 0) {
		/* checking for prot != 0 instead of the PROT_READ bit
		 * enables "read permission implied" for all mappings
		 */
		pte_bits |= PTE_AF | PTE_AP1;
	}

	if ((prot & PROT_WRITE) == 0) {
		pte_bits |= PTE_AP2;
	}

	if ((prot & PROT_EXEC) == 0) {
		pte_bits |= PTE_XN;
	}

	return pte_bits;
}

/** cache mode encoding */
static inline arm_pte_t cache_bits(unsigned int cache)
{
	arm_pte_t pte_bits;

	/* the cache mode directly encodes to the PTE_ATTR bits */
	assert(cache < 8);
	pte_bits = cache << PTE_ATTR_SHIFT;

	/* for modes 0 .. 3 we also disable execution (I/O or device mappings) */
	if (cache < 4) {
		pte_bits |= PTE_XN;
	}

	/* inner shareable */
	pte_bits |= PTE_SH0 | PTE_SH1;

	return pte_bits;
}

/** create a mapping of a physical resource in an address space */
/* FIXME: does not handle large mapping at the moment */
err_t arch_adspace_map(struct adspace *adspace, addr_t va, size_t size,
	phys_addr_t pa, unsigned int prot, unsigned int cache)
{
	arm_pte_t *ptep;
	arm_pte_t pte;
	addr_t end;

	assert(adspace != NULL);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);

	pte = pa;
	pte |= PTE_NG | PTE_TYPE_PAGE;
	pte |= prot_bits(prot);
	pte |= cache_bits(cache);

	end = va + size;
	while (va < end) {
		ptep = get_l1_table(adspace, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		ptep = get_l2_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		ptep = get_l3_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		/* invalidate TLBs on overmap */
		if (PTE_IS_VALID(*ptep)) {
			/* NOTE: requires "break-before-make" */
			arm_pte_set(ptep, 0);
			arm_tlb_inval_page(va, adspace->asid);
		}

		/* update page table entry */
		arm_pte_set(ptep, pte);

		pte += PAGE_SIZE;
		va += PAGE_SIZE;
	}

	return EOK;
}


/** change access permission a mapping in an address space */
/* FIXME: does not handle large mapping at the moment */
err_t arch_adspace_protchange(struct adspace *adspace, addr_t va, size_t size,
	unsigned int prot)
{
	arm_pte_t *ptep;
	arm_pte_t clear;
	arm_pte_t set;
	arm_pte_t pte;
	addr_t end;

	assert(adspace != NULL);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);

	clear = PTE_AP2 | PTE_AP1 | PTE_XN | PTE_AF | PTE_PXN;
	set = prot_bits(prot);

	end = va + size;
	while (va < end) {
		ptep = get_l1_table(adspace, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		ptep = get_l2_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		ptep = get_l3_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		pte = *ptep;
		if (!PTE_IS_VALID(pte)) {
			return ENOMEM;
		}

		/* update page table entry */
		/* NOTE: "break-before-make" not required when changing permissions */
		pte &= ~clear;
		pte |= set;
		arm_pte_set(ptep, pte);

		/* invalidate TLBs on overmap */
		arm_tlb_inval_page(va, adspace->asid);

		va += PAGE_SIZE;
	}

	return EOK;
}


/** iterate level 3 table and unmap pages */
static void unmap_l3(struct adspace *adspace, arm_pte_t *l3, addr_t va, addr_t end)
{
	arm_pte_t pte;

	while (va < end) {
		pte = l3[arm_addr_to_l3(va)];
		if (PTE_IS_VALID(pte)) {
			assert(PTE_IS_PAGE(pte));

			arm_pte_set(&l3[arm_addr_to_l3(va)], 0);

			arm_tlb_inval_page(va, adspace->asid);
		}

		va += PAGE_SIZE;
	}
}

/** iterate level 2 table and unmap level 3 tables if necessary */
static void unmap_l2(struct adspace *adspace, arm_pte_t *l2, addr_t va, addr_t end)
{
	arm_pte_t pte;
	arm_pte_t *pt;
	addr_t next;

	while (va < end) {
		next = NEXT_TABLE(va, 1<<21);
		if (next > end) {
			next = end;
		}

		pte = l2[arm_addr_to_l2(va)];
		if (PTE_IS_PT(pte)) {
			pt = PTE_TO_VIRT(pte);

			if (next - va == 1<<21) {
				/* unmap full level 3 page table */
				arm_pte_set(&l2[arm_addr_to_l2(va)], 0);

				/* we invalidata the first TLB from the table to
				 * invalidate any related table walk caches as well.
				 */
				arm_tlb_inval_table(va, adspace->asid);
			}

			unmap_l3(adspace, pt, va, next);

			if (next - va == 1<<21) {
				free_table(adspace, pt);
			}
		} else if (PTE_IS_BLOCK(pte)) {
			/* large mapping */
			assert(next - va == 1<<21);
			/* unmap large page (tlbi VALE1IS not sufficient for large TLBs) */
			arm_pte_set(&l2[arm_addr_to_l2(va)], 0);
			arm_tlb_inval_table(va, adspace->asid);
		}

		va = next;
	}
}

/** iterate level 1 table and unmap level 2 tables if necessary */
static void unmap_l1(struct adspace *adspace, arm_pte_t *l1, addr_t va, addr_t end)
{
	arm_pte_t pte;
	arm_pte_t *pt;
	addr_t next;

	while (va < end) {
		next = NEXT_TABLE(va, 1<<30);
		if (next > end) {
			next = end;
		}

		pte = l1[arm_addr_to_l1(va)];
		if (PTE_IS_VALID(pte)) {
			assert(PTE_IS_PT(pte));
			pt = PTE_TO_VIRT(pte);

			if (next - va == 1<<30) {
				/* unmap full level 2 page table */
				arm_pte_set(&l1[arm_addr_to_l1(va)], 0);

				/* we invalidata the first TLB from the table to
				 * invalidate any related table walk caches as well.
				 */
				arm_tlb_inval_table(va, adspace->asid);
			}

			unmap_l2(adspace, pt, va, next);

			if (next - va == 1<<30) {
				free_table(adspace, pt);
			}
		}

		va = next;
	}
}

/** delete a mapping in an address space */
err_t arch_adspace_unmap(struct adspace *adspace, addr_t va, size_t size)
{
	unmap_l1(adspace, adspace->pagetable, va, va + size);

	return EOK;
}


/** iterate level 2 table and unmap all level 3 tables */
static inline void flush_l2(struct adspace *adspace, arm_pte_t *l2, addr_t va, addr_t end)
{
	arm_pte_t pte;
	arm_pte_t *pt;

	while (va < end) {
		pte = l2[arm_addr_to_l2(va)];
		if (PTE_IS_PT(pte)) {
			pt = PTE_TO_VIRT(pte);

			l2[arm_addr_to_l2(va)] = 0;

			/* unmap full level 3 page table */
			free_table(adspace, pt);
		} else if (PTE_IS_BLOCK(pte)) {
			/* large mapping */
			l2[arm_addr_to_l2(va)] = 0;
		}

		va += (1<<21);
	}
}

/** iterate level 1 table and unmap all level 2 tables */
static inline void flush_l1(struct adspace *adspace, arm_pte_t *l1, addr_t va, addr_t end)
{
	arm_pte_t pte;
	arm_pte_t *pt;

	while (va < end) {
		pte = l1[arm_addr_to_l1(va)];
		if (PTE_IS_VALID(pte)) {
			assert(PTE_IS_PT(pte));
			pt = PTE_TO_VIRT(pte);

			l1[arm_addr_to_l1(va)] = 0;

			flush_l2(adspace, pt, va, va + (1<<30));

			/* unmap full level 2 page table */
			free_table(adspace, pt);
		}

		va += (1<<30);
	}
}

/** destroy a user address space (complete unmap, but still active) */
void arch_adspace_destroy(struct adspace *adspace)
{
	arm_pte_t *pt;

	/* switch to NULL page table, then clean up */
	pt = adspace->pagetable;
	adspace->pagetable = arch_adspace_null_pagetable;

	if (arch_percpu()->arch.current_adspace == adspace) {
		arch_adspace_switch_pagetable(adspace->pagetable, adspace->asid);
	}

	arm_tlb_inval_asid(adspace->asid);

	/* the page table is no longer referenced in any table walk caches */
	flush_l1(adspace, pt, 0, ARCH_USER_END);

	/* unmap page table */
	free_table(adspace, pt);
}

////////////////////////////////////////////////////////////////////////////////

/** setup a per-CPU in-kernel mapping */
addr_t arch_kmap_percpu(phys_addr_t pa, unsigned int prot, unsigned int cache)
{
	arm_pte_t *pt = arch_adspace_xmap_pagetable;
	arm_pte_t pte;
	addr_t xmap_va;

	pte = pa;
	pte |= PTE_TYPE_PAGE | PTE_AF | PTE_PXN | PTE_XN;
	if ((prot & PROT_WRITE) == 0) {
		pte |= PTE_AP2;
	}
	pte |= cache_bits(cache);

	xmap_va = ARCH_PERCPU_XMAP(current_cpu_id());

	if (pt[arm_addr_to_l3(xmap_va)] != pte) {
		arm_pte_set(&pt[arm_addr_to_l3(xmap_va)], pte);

		arm_tlb_inval_page_locally(xmap_va);
		/* ISB required; kernel will access the updated mapping soon */
		arm_isb();
	}

	return xmap_va;
}

/** setup a per-CPU cross-address space mapping */
/** internal routine to get a user space page table entry (no blocks) */
static addr_t arch_adspace_xmap(struct adspace *adspace, addr_t va, int writable)
{
	arm_pte_t *pt;
	arm_pte_t pte;
	addr_t xmap_va;

	assert(va < ARCH_USER_END);

	/* level 1 table */
	pt = adspace->pagetable;
	pte = pt[arm_addr_to_l1(va)];
	if (!PTE_IS_PT(pte)) {
		/* no l2 page table */
		return 0;
	}

	/* level 2 table */
	pt = PTE_TO_VIRT(pte);
	pte = pt[arm_addr_to_l2(va)];
	if (PTE_IS_BLOCK(pte)) {
		/* large mapping */
		/* reconstruct missing bits from virtual address and assemble PTE */
		pte |= (va & 0x001ff000);
		goto hit;
	}
	if (!PTE_IS_PT(pte)) {
		/* no l3 page table */
		return 0;
	}

	/* level 3 table */
	pt = PTE_TO_VIRT(pte);
	pte = pt[arm_addr_to_l3(va)];
	if (!PTE_IS_VALID(pte)) {
		/* no mapping */
		return 0;
	}

hit:
	if (!PTE_IS_READ(pte) || (writable && !PTE_IS_WRITE(pte))) {
		/* permission violation */
		return 0;
	}

	/* sanitize user space PTE to global kernel PTE */
	pte &= ~(PTE_AP1 | PTE_NG | 0ull);
	pte |= PTE_PXN | PTE_XN;

	xmap_va = ARCH_PERCPU_XMAP(current_cpu_id()) + (va & PAGE_OFFSET);

	pt = arch_adspace_xmap_pagetable;
	if (pt[arm_addr_to_l3(xmap_va)] != pte) {
		arm_pte_set(&pt[arm_addr_to_l3(xmap_va)], pte);

		arm_tlb_inval_page_locally(xmap_va);
		/* ISB required; kernel will access the updated mapping soon */
		arm_isb();
	}

	return xmap_va;
}

/** read from a remote adspace */
err_t arch_adspace_read(
	struct adspace *adspace,
	addr_t dst_va, /* current adspace */
	addr_t src_va, /* remote adspace */
	size_t size)
{
	addr_t xmap_va;
	size_t chunk;
	err_t err;

	assert(adspace != NULL);

	while (size > 0) {
		xmap_va = arch_adspace_xmap(adspace, src_va, false);
		if (xmap_va == 0) {
			/* mapping in remote adspace not accessible */
			return EFAULT;
		}

		chunk = PAGE_SIZE - (xmap_va & PAGE_OFFSET);
		if (chunk > size) {
			chunk = size;
		}

		err = arch_user_memcpy((void*)dst_va, (void*)xmap_va, chunk);
		if (err != EOK) {
			return err;
		}

		dst_va += chunk;
		src_va += chunk;
		size -= chunk;
	}

	return EOK;
}

/** write to a remote adspace */
err_t arch_adspace_write(
	struct adspace *adspace,
	addr_t dst_va, /* remote adspace */
	addr_t src_va, /* current adspace */
	size_t size,
	int coherent) /* cache coherency required */
{
	addr_t xmap_va;
	size_t chunk;
	err_t err;

	assert(adspace != NULL);

	while (size > 0) {
		xmap_va = arch_adspace_xmap(adspace, dst_va, true);
		if (xmap_va == 0) {
			/* mapping in remote adspace not accessible */
			return EFAULT;
		}

		chunk = PAGE_SIZE - (xmap_va & PAGE_OFFSET);
		if (chunk > size) {
			chunk = size;
		}

		err = arch_user_memcpy((void*)xmap_va, (void*)src_va, chunk);
		if (err != EOK) {
			return err;
		}

		if (coherent) {
			err = bsp_cache(CACHE_OP_INVAL_ICACHE_RANGE, xmap_va, chunk, dst_va);
			if (err != EOK) {
				return err;
			}
		}

		dst_va += chunk;
		src_va += chunk;
		size -= chunk;
	}

	return EOK;
}

/** bzero to a remote adspace */
err_t arch_adspace_bzero(
	struct adspace *adspace,
	addr_t dst_va, /* remote adspace */
	size_t size) /* cache coherency required */
{
	addr_t xmap_va;
	size_t chunk;
	err_t err;

	assert(adspace != NULL);

	while (size > 0) {
		xmap_va = arch_adspace_xmap(adspace, dst_va, true);
		if (xmap_va == 0) {
			/* mapping in remote adspace not accessible */
			return EFAULT;
		}

		chunk = PAGE_SIZE - (xmap_va & PAGE_OFFSET);
		if (chunk > size) {
			chunk = size;
		}

		err = arch_user_bzero((void*)xmap_va, chunk);
		if (err != EOK) {
			return err;
		}

		dst_va += chunk;
		size -= chunk;
	}

	return EOK;
}

/** user space virt to phys operation -- specialized version of __get_pte() */
err_t arch_adspace_v2p(
	struct adspace *adspace,
	addr_t va,
	phys_addr_t *pa,
	unsigned int *prot,
	addr_t *next)
{
	arm_pte_t *pt;
	arm_pte_t pte;
	size_t size;

	assert(adspace != NULL);
	assert(va < ARCH_USER_END);
	assert(pa != NULL);
	assert(prot != NULL);
	assert(next != NULL);

	pt = adspace->pagetable;

	/* level 1 table */
	pte = pt[arm_addr_to_l1(va)];
	if (!PTE_IS_PT(pte)) {
		*next = NEXT_TABLE(va, 1<<30);
		return ESTATE;
	}
	pt = PTE_TO_VIRT(pte);

	/* level 2 table */
	pte = pt[arm_addr_to_l2(va)];
	if (PTE_IS_BLOCK(pte)) {
		/* large mapping */
		size = BLOCK_SIZE;
		goto hit;
	}
	if (!PTE_IS_PT(pte)) {
		*next = NEXT_TABLE(va, 1<<21);
		return ESTATE;
	}
	pt = PTE_TO_VIRT(pte);

	/* level 3 table */
	pte = pt[arm_addr_to_l3(va)];
	if (!PTE_IS_VALID(pte)) {
		*next = NEXT_TABLE(va, PAGE_SIZE);
		return ESTATE;
	}
	size = PAGE_SIZE;

hit:
	*pa = PTE_TO_PHYS(pte) | (va & (size - 1));
	*prot = (PTE_IS_READ(pte) ? PROT_READ : 0) |
	        (PTE_IS_WRITE(pte) ? PROT_WRITE : 0) |
	        (PTE_IS_EXEC(pte) ? PROT_EXEC : 0);
	*next = NEXT_TABLE(va, PAGE_SIZE);
	return EOK;
}
