# SPDX-License-Identifier: MIT
# Copyright 2022 Alexander Zuepke

case "$MARRON_SUBARCH" in
cortex-a8|cortex-a9|cortex-a15|cortex-a7)
	WHICH_GCC=$(which arm-linux-gnueabihf-gcc)
	if [ $? -ne 0 ]; then
		echo "No GCC (arm-linux-gnueabihf-gcc) found!"
		echo "install toolchain:"
		echo "  $ sudo apt install gcc-arm-linux-gnueabihf"
		return
	fi
	MARRON_CROSS_PREFIX=arm-linux-gnueabihf-
	;;

cortex-*)
	WHICH_GCC=$(which aarch64-linux-gnu-gcc)
	if [ $? -ne 0 ]; then
		echo "No GCC (aarch64-linux-gnu-gcc) found!"
		echo "install toolchain:"
		echo "  $ sudo apt install gcc-aarch64-linux-gnu"
		return
	fi
	MARRON_CROSS_PREFIX=aarch64-linux-gnu-
	;;
esac
