# SPDX-License-Identifier: MIT
# Copyright 2017, 2023 Alexander Zuepke
# rv32g.mk -- for RISC-V RV32G
#
# RISC-V architecture specific build rules
#
# azuepke, 2017-04-04: initial RISC-V port
# azuepke, 2023-09-02: imported

ARCH_CFLAGS := -march=rv32imac -mabi=ilp32 -msmall-data-limit=0 -mno-save-restore
ARCH_CFLAGS += -DRISCV_RV32G

ARCH_CFLAGS_DEBUG := -Og
ARCH_CFLAGS_NDEBUG := -O2 -fomit-frame-pointer

ARCH_AFLAGS := -march=rv32imafdc -mabi=ilp32
ARCH_AFLAGS += -DRISCV_RV32G
ARCH_LDFLAGS := -melf32lriscv

ARCH_MODS := entry exception adspace string clz div64


# Recommended user compiler and linker flags
ARCH_USER_CFLAGS := -march=rv32gc -mabi=ilp32d -msmall-data-limit=0
ARCH_USER_AFLAGS := -march=rv32gc -mabi=ilp32d
ARCH_USER_LDFLAGS := -melf32lriscv
