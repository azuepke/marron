/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * exception.c
 *
 * Architecture specific exception handling
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 */

#include <kernel.h>
#include <assert.h>
#include <ex_table.h>
#include <riscv_private.h>
#include <bsp.h>
#include <panic.h>
#include <arch_context.h>
#include <percpu.h>
#include <marron/signal.h>


__cold void arch_dump_registers(struct regs *regs, unsigned int status, unsigned long addr)
{
	assert(regs != NULL);

	printk("zr=%lx ra=%lx sp=%lx gp=%lx\n",
	       0ul, regs->ra, regs->sp, regs->gp);
	printk("tp=%lx t0=%lx t1=%lx t2=%lx\n",
	       regs->tp, regs->t0, regs->t1, regs->t2);
	printk("s0=%lx s1=%lx a0=%lx a1=%lx\n",
	       regs->s0, regs->s1, regs->a0, regs->a1);
	printk("a2=%lx a3=%lx a4=%lx a5=%lx\n",
	       regs->a2, regs->a3, regs->a4, regs->a5);
	printk("a6=%lx a7=%lx s2=%lx s3=%lx\n",
	       regs->a6, regs->a7, regs->s2, regs->s3);
	printk("s4=%lx s5=%lx s6=%lx s7=%lx\n",
	       regs->s4, regs->s5, regs->s6, regs->s7);
	printk("s8=%lx s9=%lx 10=%lx 11=%lx\n",
	       regs->s8, regs->s9, regs->s10, regs->s11);
	printk("t3=%lx t4=%lx t5=%lx t6=%lx\n",
	       regs->t3, regs->t4, regs->t5, regs->t6);
	printk("pc=%lx sr=%lx @@=%lx ex=%x %c-mode I%c\n",
	       regs->pc, regs->sr, addr, status,
	       (regs->sr & SR_SPP) ? 'S' : 'U',
	       (regs->sr & SR_SPIE) ? 'E' : 'D');
}

/** exception recovery (expects a sorted table!) */
static int try_exception_recovery(struct regs *regs)
{
	const struct ex_table *start = &__ex_table_start;
	const struct ex_table *end = &__ex_table_end;
	const struct ex_table *middle;

	assert(start <= end);
	do {
		middle = start + ((end - start) >> 1);

		if (middle->addr == regs->pc) {
			/* match */
			regs->pc = middle->cont;
			return 1;
		} else if (middle->addr > regs->pc) {
			/* search on left side */
			end = middle - 1;
		} else {
			/* search on right side */
			start = middle + 1;
		}
	} while (start <= end);

	return 0;
}

static const char *cause2str(unsigned int cause)
{
	switch (cause) {
	case CAUSE_INST_ALIGN:
		return "INST_ALIGN";
	case CAUSE_INST_ACCESS:
		return "INST_ACCESS";
	case CAUSE_ILLEGAL:
		return "ILLEGAL";
	case CAUSE_BREAKPOINT:
		return "BREAKPOINT";
	case CAUSE_LOAD_ALIGN:
		return "LOAD_ALIGN";
	case CAUSE_LOAD_ACCESS:
		return "LOAD_ACCESS";
	case CAUSE_STORE_ALIGN:
		return "STORE_ALIGN";
	case CAUSE_STORE_ACCESS:
		return "STORE_ACCESS";
	case CAUSE_ECALL_UMODE:
		return "ECALL_UMODE";
	case CAUSE_ECALL_SMODE:
		return "ECALL_SMODE";
	case 10:
		return "RSVD_10";
	case CAUSE_ECALL_MMODE:
		return "ECALL_MMODE";
	case CAUSE_INST_FAULT:
		return "INST_FAULT";
	case CAUSE_LOAD_FAULT:
		return "LOAD_FAULT";
	case 14:
		return "RSVD_14";
	case CAUSE_STORE_FAULT:
		return "STORE_FAULT";
	default:
		return "RSVD";
	}
}

static inline int regs_is_kern(const struct regs *regs)
{
	return (regs->sr & SR_SPP) != 0;
}

static inline int regs_is_32bit(const struct regs *regs)
{
#ifdef __LP64__
	return (regs->sr & SR_UXL_32) != 0;
#else
	(void)regs;
	return 1;
#endif
}

void riscv_handler_irq_kern(struct regs *regs, unsigned int vector)
{
	assert(regs_is_kern(regs));

	panic_regs(regs, vector, 0, "Unsupported IRQ in kernel mode\n");
}

void riscv_handler_irq_user(struct regs *regs __unused, unsigned int vector)
{
	bsp_irq_dispatch(vector);
}

void riscv_handler_exception_kern(struct regs *regs, ulong_t cause, ulong_t stval)
{
	assert(regs_is_kern(regs));

	if (likely(try_exception_recovery(regs))) {
		return;
	}

	panic_regs(regs, cause, stval, "Exception %s in kernel mode\n", cause2str(cause));
}

void riscv_handler_exception_user(struct regs *regs, ulong_t cause, ulong_t stval)
{
	unsigned int sig;
	unsigned int si_code;
	unsigned int ex_info;

	switch (cause) {
	case CAUSE_INST_ALIGN:
		sig = SIGBUS;
		si_code = BUS_ADRALN;
		ex_info = EX_TYPE_ALIGN | EX_EXEC;
		break;

	case CAUSE_INST_ACCESS:
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_PERM | EX_EXEC;
		break;

	case CAUSE_ILLEGAL:
		/* detect FPU instructions for EX_TYPE_FPU_UNAVAIL exceptions
		 *
		 * RVIMAFD encoding (last 7 LSBs)
		 *   00 001 11	LOAD-FP
		 *   01 001 11	STORE-FP
		 *   10 000 11	MADD
		 *   10 001 11	MSUB
		 *   10 010 11	NMSUB
		 *   10 011 11	NMADD
		 *   10 100 11	OP-FP
		 *   11 100 11	SYSTEM for FCSR access -> see below
		 *
		 * RVC encoding in 32-bit mode (bits 15-13 and bits 1-0)
		 *   001 -- 00	FLD
		 *   011 -- 00	FLW
		 *   101 -- 00	FSD
		 *   111 -- 00	FSW
		 *   001 -- 10	FLDSP
		 *   011 -- 10	FLWSP
		 *   101 -- 10	FSDSP
		 *   111 -- 10	FSWSP
		 *
		 * RVC encoding on 64-bit mode (bits 15-13 and bits 1-0)
		 *   001 -- 00	FLD
		 *   101 -- 00	FSD
		 *   001 -- 10	FLDSP
		 *   101 -- 10	FSDSP
		 *
		 * Also CSR registers 0x001, 0x002 and 0x003 map to FCSR.
		 * The 12-bit CSR number is encoded in bits 20 to 31 of the instruction.
		 */
		if (!arch_reg_fpu_enabled(regs) &&
		    ( ((stval & 0x005f) == 0x0007)                                    || /* LOAD-FP, STORE-FP */
		      ((stval & 0x0073) == 0x0043)                                    || /* MADD, MSUB, NMSUB, NMADD */
		      ((stval & 0x007f) == 0x0053)                                    || /* OP-FP */
		     (((stval & 0x007f) == 0x0073) && (((stval >> 20) - 1) <= 0x002)) || /* SYSTEM, CSRs 1..3 */
		      ((stval & 0x6001) == 0x2000)                                    || /* FLD, FSD, FLDSP, FSDSP */
		     (((stval & 0x6001) == 0x6000) && regs_is_32bit(regs))))             /* FLW, FSW, FLWSP, FSWSP */
		{
			/* FPU instruction detected */
			sig = SIGILL;
			si_code = ILL_ILLOPC;
			ex_info = EX_TYPE_FPU_UNAVAIL;
			break;
		}

		/* default */
		sig = SIGILL;
		si_code = ILL_ILLOPC;
		ex_info = EX_TYPE_ILLEGAL;
		break;

	case CAUSE_BREAKPOINT:
		sig = SIGTRAP;
		si_code = TRAP_BRKPT;
		ex_info = EX_TYPE_DEBUG | EX_EXEC;
		break;

	case CAUSE_LOAD_ALIGN:
		sig = SIGBUS;
		si_code = BUS_ADRALN;
		ex_info = EX_TYPE_ALIGN | EX_READ;
		break;

	case CAUSE_LOAD_ACCESS:
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_PERM | EX_READ;
		break;

	case CAUSE_STORE_ALIGN:
		sig = SIGBUS;
		si_code = BUS_ADRALN;
		ex_info = EX_TYPE_ALIGN | EX_WRITE;
		break;

	case CAUSE_STORE_ACCESS:
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_PERM | EX_WRITE;
		break;

	/* case CAUSE_ECALL: handled at assembler level */

	case CAUSE_INST_FAULT:
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_EXEC;
		break;

	case CAUSE_LOAD_FAULT:
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_READ;
		break;

	case CAUSE_STORE_FAULT:
		sig = SIGSEGV;
		si_code = SEGV_ACCERR;
		ex_info = EX_TYPE_PAGEFAULT | EX_WRITE;
		break;

	default:
		panic_regs(regs, cause, stval, "Unsupported exception in user mode\n");
		break;
	}
	kernel_exception(regs, sig, si_code, ex_info, cause, stval);
}

/** initializer for architecture-specific per-CPU data (called from CPU #0) */
__init void arch_percpu_init(struct arch_percpu *arch, unsigned int cpu_id)
{
	const struct percpu_cfg *cfg = &percpu_cfg[cpu_id];

	arch->scratch = 0;
	arch->current_regs = NULL;
	arch->kern_stack_top = cfg->kern_stack_top;
	arch->current_adspace = NULL;
}

__init void arch_init_exceptions(void)
{
	/* enable access to all counters */
	riscv_set_scounteren(~0);
}
