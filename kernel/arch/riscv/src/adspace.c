/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2021, 2023, 2025 Alexander Zuepke */
/*
 * adspace64.c
 *
 * RISC-V address space handling
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2021-04-29: imported mapping code
 * azuepke, 2023-09-02: imported & adapted from Arm64
 * azuepke, 2025-02-21: adspace rework
 */

#include <kernel.h>
#include <stddef.h>
#include <arch.h>
#include <assert.h>
#include <panic.h>
#include <adspace.h>
#include <riscv_insn.h>
#include <riscv_pte.h>
#include <current.h>
#include <uaccess.h>
#include <part_types.h>
#include <stdbool.h>
#include <marron/compiler.h>
#include <marron/cache_type.h>
#include <arch_bsp.h>
#include <marron/atomic.h>
#include <part.h>



/* align vaddr to the start of the next table */
#define NEXT_TABLE(addr, align)		(((addr) + (align)) & ~((align) - 1ul))


/* kernel first-level page table */
#ifdef __LP64__
riscv_pte_t arch_adspace_kern_pagetable[PTE_L1_NUM] __aligned(PTE_L1_ALIGN) __section(.bss..pagetables.l1);
#else /* 32-bit version */
riscv_pte_t arch_adspace_kern_pagetable[PTE_L2_NUM] __aligned(PTE_L2_ALIGN) __section(.bss..pagetables.l2);
#endif

/* writable pagetable for cross-copy mapping */
riscv_pte_t arch_adspace_xmap_pagetable[PTE_L3_NUM] __aligned(PTE_L3_ALIGN) __section(.bss..pagetables.rw);


/* effective ASID mask (number of ASIDs supported in hardware) */
static unsigned long satp_asid_mask;

/** initialize all adspace objects at boot time */
__init void arch_adspace_init_all(void)
{
	unsigned long orig_satp, test_satp;

	for (unsigned int i = 0; i < adspace_num; i++) {
		struct adspace *adspace = &adspace_dyn[i];

		/* adspace->part is set in part.c */
		spin_init(&adspace->lock);
		adspace->asid = i;
		adspace->cpu_active_mask = 0;
		adspace->cpu_inval_mask = 0;
		if (i == 0) {
			adspace->pagetable = arch_adspace_kern_pagetable;
		} else {
			adspace->pagetable = NULL;
		}

		/* NOTE: initialization of VMA data is implemented in vma.c */
	}

	/* determine number of ASIDs supported in hardware */
	orig_satp = riscv_get_satp();

	test_satp = orig_satp | (SATP_ASID_MASK << SATP_ASID_SHIFT);
	riscv_set_satp(test_satp);
	test_satp = riscv_get_satp();

	riscv_set_satp(orig_satp);
	riscv_tlb_inval_all_locally();

	satp_asid_mask = (test_satp >> SATP_ASID_SHIFT) & SATP_ASID_MASK;
	assert((satp_asid_mask == 0) || (adspace_num <= satp_asid_mask));
}

/** change MMU page tables on context switch */
static inline void arch_adspace_switch_pagetable(void *pt, unsigned long asid)
{
	unsigned long satp;

	satp = PHYS_TO_SATP(KERNEL_TO_PHYS(pt));
	satp |= SATP_ASID(asid);
#ifdef __LP64__
	satp |= SATP_MODE_SV39;
#else /* 32-bit version */
	satp |= SATP_MODE_SV32;
#endif

	riscv_set_satp(satp);
}

void arch_adspace_switch(struct adspace *prev, struct adspace *next)
{
	unsigned int cpu_mask = 1ul << current_cpu_id();
	unsigned long inval_mask;
	unsigned long self_mask;

	assert(prev != NULL);
	assert(next != NULL);

	atomic_clear_mask_relaxed(&prev->cpu_active_mask, cpu_mask);
	atomic_set_mask_relaxed(&next->cpu_active_mask, cpu_mask);

	arch_percpu()->arch.current_adspace = next;

	arch_adspace_switch_pagetable(next->pagetable, next->asid);

	/* invalidate TLBs locally if TLB invalidation is pending
	 * or  if the CPU does not support tagged TLBs
	 */
	inval_mask = atomic_load(&next->cpu_inval_mask);
	self_mask = current_cpu_mask();
	if (((inval_mask & self_mask) != 0) || (satp_asid_mask == 0)) {
		atomic_clear_mask_relaxed(&next->cpu_inval_mask, self_mask);
		riscv_tlb_inval_asid_locally(next->asid);
	}
}

/* invalidate TLBs for "va" of asid on given harts */
static inline void riscv_tlb_inval_page(struct adspace *adspace, addr_t va, unsigned long asid)
{
	unsigned long self_mask;
	unsigned long ipi_mask;

	ipi_mask = atomic_load(&adspace->cpu_active_mask);

	/* indicate pending TLB invalidation for others */
	atomic_set_mask_relaxed(&adspace->cpu_inval_mask, ~ipi_mask);

	/* invalidate locally */
	self_mask = current_cpu_mask();
	if ((ipi_mask & self_mask) != 0) {
		ipi_mask &= ~self_mask;
		riscv_tlb_inval_page_locally(va, asid);
	}

	/* invalidate remote */
	if (ipi_mask != 0) {
		bsp_remote_sfence_vma_asid(ipi_mask, va, PAGE_SIZE, asid);
	}
}

/* invalidate all TLBs of asid on given harts */
static inline void riscv_tlb_inval_asid(struct adspace *adspace, unsigned long asid)
{
	unsigned long self_mask;
	unsigned long ipi_mask;

	ipi_mask = atomic_load(&adspace->cpu_active_mask);

	/* indicate pending TLB invalidation for others */
	atomic_set_mask_relaxed(&adspace->cpu_inval_mask, ~ipi_mask);

	/* invalidate locally */
	self_mask = current_cpu_mask();
	if ((ipi_mask & self_mask) != 0) {
		ipi_mask &= ~self_mask;
		riscv_tlb_inval_asid_locally(asid);
	}

	/* invalidate remote */
	if (ipi_mask != 0) {
		bsp_remote_sfence_vma_asid(ipi_mask, 0, -1, asid);
	}
}


#ifdef DEBUG

/** dump an address space's page tables */
static inline void adspace_dump_pte(riscv_pte_t pte)
{
	printk(" %c%c%c%c %c%c%c%c\n",
	       (pte & PTE_D) ? 'd' : '-',
	       (pte & PTE_A) ? 'a' : '-',
	       (pte & PTE_G) ? 'g' : '-',
	       (pte & PTE_U) ? 'u' : '-',
	       (pte & PTE_X) ? 'x' : '-',
	       (pte & PTE_W) ? 'w' : '-',
	       (pte & PTE_R) ? 'r' : '-',
	       (pte & PTE_V) ? 'v' : '-');
}

static void adspace_dump(void *pagetable)
{
	unsigned long i, j, k;
	unsigned long va;	/* signed shift of the MSB to derive kernel addresses */
#ifdef __LP64__
	riscv_pte_t *l1;
#endif
	riscv_pte_t *l2;
	riscv_pte_t *l3;

	printk("##### dump page table %p\n", pagetable);

#ifdef __LP64__
	l1 = pagetable;
	for (i = 0; i < PTE_L1_NUM; i++) {
		/* valid? */
		if (l1[i] == 0) {
			continue;
		}

		/* block? */
		if (PTE_IS_PAGE(l1[i])) {
			va = (i << PTE_L1_SHIFT);
			va <<= 25;
			va = (long)va >> 25;

			printk("##### 1G page %lx -> %llx", va, PTE_TO_PHYS(l1[i]));
			adspace_dump_pte(l1[i]);
			continue;
		}

		/* page table */
		assert(PTE_IS_PT(l1[i]));
		l2 = PTE_TO_VIRT(l1[i]);
		for (j = 0; j < PTE_L2_NUM; j++) {
			/* valid? */
			if (l2[j] == 0) {
				continue;
			}

			/* block? */
			if (PTE_IS_PAGE(l2[j])) {
				va = (i << PTE_L1_SHIFT)|(j << PTE_L2_SHIFT);
				va <<= 25;
				va = (long)va >> 25;

				printk("##### 2M page %lx -> %llx", va, PTE_TO_PHYS(l2[j]));
				adspace_dump_pte(l2[j]);
				continue;
			}

			/* page table */
			assert(PTE_IS_PT(l2[j]));
			l3 = PTE_TO_VIRT(l2[j]);
			for (k = 0; k < PTE_L3_NUM; k++) {
				/* valid? */
				if (l3[k] == 0) {
					continue;
				}

				/* page */
				assert(PTE_IS_PAGE(l3[k]));
				if (PTE_IS_PAGE(l3[k])) {
					va = (i << PTE_L1_SHIFT)|(j << PTE_L2_SHIFT)|(k << PTE_L3_SHIFT);
					va <<= 25;
					va = (long)va >> 25;

					printk("##### 4K page %lx -> %llx", va, PTE_TO_PHYS(l3[k]));
					adspace_dump_pte(l3[k]);
					continue;
				}
			}
		}
	}
#else /* 32-bit version */
	(void)i;

	l2 = pagetable;
	for (j = 0; j < PTE_L2_NUM; j++) {
		/* valid? */
		if (l2[j] == 0) {
			continue;
		}

		/* block? */
		if (PTE_IS_PAGE(l2[j])) {
			va = (j << PTE_L2_SHIFT);

			printk("##### 4M page %lx -> %llx", va, PTE_TO_PHYS(l2[j]));
			adspace_dump_pte(l2[j]);
			continue;
		}

		/* page table */
		assert(PTE_IS_PT(l2[j]));
		l3 = PTE_TO_VIRT(l2[j]);
		for (k = 0; k < PTE_L3_NUM; k++) {
			/* valid? */
			if (l3[k] == 0) {
				continue;
			}

			/* page */
			assert(PTE_IS_PAGE(l3[k]));
			if (PTE_IS_PAGE(l3[k])) {
				va = (j << PTE_L2_SHIFT)|(k << PTE_L3_SHIFT);

				printk("##### 4K page %lx -> %llx", va, PTE_TO_PHYS(l3[k]));
				adspace_dump_pte(l3[k]);
				continue;
			}
		}
	}
#endif
}

#endif

////////////////////////////////////////////////////////////////////////////////

/** write/update entry in page table */
static inline void riscv_pte_set(riscv_pte_t *ptep, riscv_pte_t val)
{
	access_once(*ptep) = val;
}


/** allocate a page from the pagetable KMEM page pool */
static inline void *alloc_table(struct adspace *adspace)
{
	void *page;

	spin_lock(&adspace->part->kmem_pagetables_lock);
	page = kmempool_get(&adspace->part->kmem_pagetables);
	spin_unlock(&adspace->part->kmem_pagetables_lock);

	if (page != NULL) {
		fast_clear_page(page);
	}

	assert(((addr_t)page & PAGE_OFFSET) == 0);
	return page;
}

/** put a page back to its pagetable KMEM page pool */
static inline void free_table(struct adspace *adspace, void *page)
{
	assert(page != NULL);
	assert(((addr_t)page & PAGE_OFFSET) == 0);

	spin_lock(&adspace->part->kmem_pagetables_lock);
	kmempool_put(&adspace->part->kmem_pagetables, page);
	spin_unlock(&adspace->part->kmem_pagetables_lock);
}


/** initializes a user address space */
err_t arch_adspace_init(struct adspace *adspace)
{
	riscv_pte_t *boot_pt;
	riscv_pte_t *pt;
	unsigned int i;

	adspace->pagetable = alloc_table(adspace);
	if (adspace->pagetable == NULL) {
		return ENOMEM;
	}

	/* copy kernel part of page table */
	pt = adspace->pagetable;
	boot_pt = (riscv_pte_t *)adspace_dyn[0].pagetable;
#ifdef __LP64__
	for (i = riscv_addr_to_l1(ARCH_USER_END); i < PTE_L1_NUM; i++) {
		pt[i] = boot_pt[i];
	}
#else /* 32-bit version */
	for (i = riscv_addr_to_l2(ARCH_USER_END); i < PTE_L2_NUM; i++) {
		pt[i] = boot_pt[i];
	}
#endif

	return EOK;
}


/** internal routine to get a pointer to a level 3 table entry, with allocation */
static inline riscv_pte_t *get_l3_table(struct adspace *adspace, riscv_pte_t *l2_pte, addr_t va)
{
	riscv_pte_t *pt;
	riscv_pte_t pte;

	/* level 2 table */
	pte = *l2_pte;
	if (PTE_IS_PT(pte)) {
		pt = PTE_TO_VIRT(pte);
		goto hit;
	}

	/* no level 3 page table, allocate one */
	pt = alloc_table(adspace);
	if (unlikely(pt == NULL)) {
		return NULL;
	}

	/* put entry into l2 page table */
	assert(*l2_pte == 0);
	riscv_pte_set(l2_pte, VIRT_TO_PTE(pt) | PTE_V);

hit:
	return &pt[riscv_addr_to_l3(va)];
}

/** internal routine to get a pointer to a level 2 table entry, with allocation */
static inline riscv_pte_t *get_l2_table(struct adspace *adspace, riscv_pte_t *l1_pte __unused, addr_t va)
{
	riscv_pte_t *pt;

#ifdef __LP64__
	riscv_pte_t pte;

	/* level 1 table */
	pte = *l1_pte;
	if (PTE_IS_PT(pte)) {
		pt = PTE_TO_VIRT(pte);
		goto hit;
	}

	/* no level 2 page table, allocate one */
	pt = alloc_table(adspace);
	if (unlikely(pt == NULL)) {
		return NULL;
	}

	/* put entry into l1 page table */
	assert(*l1_pte == 0);
	riscv_pte_set(l1_pte, VIRT_TO_PTE(pt) | PTE_V);

hit:
#else /* 32-bit version */
	pt = adspace->pagetable;
#endif

	return &pt[riscv_addr_to_l2(va)];
}

#ifdef __LP64__
/** internal routine to get a pointer to a level 1 table entry, no allocation */
static inline riscv_pte_t *get_l1_table(struct adspace *adspace, riscv_pte_t *l0_pte __unused, addr_t va)
{
	riscv_pte_t *pt;

	pt = adspace->pagetable;

	return &pt[riscv_addr_to_l1(va)];
}
#endif

/** permission bits encoding */
static inline riscv_pte_t prot_bits(unsigned int prot)
{
	riscv_pte_t pte_bits;

	/* just PTE_U encodes a non-accesible mapping */
	pte_bits = PTE_U;

	if (prot != 0) {
		/* checking for prot != 0 instead of the PROT_READ bit
		 * enables "read permission implied" for all mappings
		 */
		pte_bits |= PTE_V | PTE_R | PTE_A;

		if ((prot & PROT_WRITE) != 0) {
			pte_bits |= PTE_W | PTE_D;
		}

		if ((prot & PROT_EXEC) != 0) {
			pte_bits |= PTE_X;
		}
	}

	return pte_bits;
}

/** cache mode encoding */
static inline riscv_pte_t cache_bits(unsigned int cache)
{
	riscv_pte_t pte_bits;

	assert(cache < 8);
	(void)cache;

	pte_bits = 0;

#if 0
	// FIXME: PBMT
	if (cache < CACHE_TYPE_WC) {
		pte_bits = 2ul << 61;
	} else if (cache == CACHE_TYPE_WC) {
		pte_bits = 1ul << 61;
	} else {
		pte_bits = 0ul << 61;
	}
#endif

	return pte_bits;
}

/** create a mapping of a physical resource in an address space */
/* FIXME: does not handle large mapping at the moment */
err_t arch_adspace_map(struct adspace *adspace, addr_t va, size_t size,
	phys_addr_t pa, unsigned int prot, unsigned int cache)
{
	riscv_pte_t *ptep;
	riscv_pte_t pte;
	addr_t end;

	assert(adspace != NULL);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);

	pte = PHYS_TO_PTE(pa);
	pte |= prot_bits(prot);
	pte |= cache_bits(cache);

	end = va + size;
	while (va < end) {
#ifdef __LP64__
		ptep = get_l1_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}
#endif

		ptep = get_l2_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		ptep = get_l3_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		/* invalidate TLBs on overmap */
		if (PTE_IS_VALID(*ptep)) {
			/* NOTE: uses "break-before-make" */
			riscv_pte_set(ptep, 0);

			riscv_tlb_inval_page(adspace, va, adspace->asid);
		}

		/* update page table entry */
		riscv_pte_set(ptep, pte);

		pte += PHYS_TO_PTE(PAGE_SIZE);
		va += PAGE_SIZE;
	}

	return EOK;
}


/** change access permission a mapping in an address space */
/* FIXME: does not handle large mapping at the moment */
err_t arch_adspace_protchange(struct adspace *adspace, addr_t va, size_t size,
	unsigned int prot)
{
	riscv_pte_t *ptep;
	riscv_pte_t clear;
	riscv_pte_t set;
	riscv_pte_t pte;
	addr_t end;

	assert(adspace != NULL);
	assert((prot & ~(PROT_READ | PROT_WRITE | PROT_EXEC)) == 0);

	clear = PTE_V | PTE_R | PTE_W | PTE_X | PTE_U | PTE_G | PTE_A | PTE_D;
	set = prot_bits(prot);

	end = va + size;
	while (va < end) {
#ifdef __LP64__
		ptep = get_l1_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}
#endif

		ptep = get_l2_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		ptep = get_l3_table(adspace, ptep, va);
		if (unlikely(ptep == NULL)) {
			return ENOMEM;
		}

		pte = *ptep;
		if (!PTE_IS_VALID(pte)) {
			return ENOMEM;
		}

		/* update page table entry */
		pte &= ~clear;
		pte |= set;
		riscv_pte_set(ptep, pte);

		/* invalidate TLBs on overmap */
		riscv_tlb_inval_page(adspace, va, adspace->asid);

		va += PAGE_SIZE;
	}

	return EOK;
}


/** iterate level 3 table and unmap pages */
static void unmap_l3(struct adspace *adspace, riscv_pte_t *l3, addr_t va, addr_t end)
{
	riscv_pte_t pte;

	while (va < end) {
		pte = l3[riscv_addr_to_l3(va)];
		if (PTE_IS_VALID(pte)) {
			assert(PTE_IS_PAGE(pte) || PTE_IS_NONE(pte));

			riscv_pte_set(&l3[riscv_addr_to_l3(va)], 0);

			riscv_tlb_inval_page(adspace, va, adspace->asid);
		}

		va += PAGE_SIZE;
	}
}

/** iterate level 2 table and unmap level 3 tables if necessary */
static void unmap_l2(struct adspace *adspace, riscv_pte_t *l2, addr_t va, addr_t end)
{
	riscv_pte_t pte;
	riscv_pte_t *pt;
	addr_t next;

	while (va < end) {
		next = NEXT_TABLE(va, 1ul << PTE_L2_SHIFT);
		if (next > end) {
			next = end;
		}

		pte = l2[riscv_addr_to_l2(va)];
		if (PTE_IS_PT(pte)) {
			pt = PTE_TO_VIRT(pte);

			if (next - va == 1ul << PTE_L2_SHIFT) {
				/* unmap full level 3 page table */
				riscv_pte_set(&l2[riscv_addr_to_l2(va)], 0);

				/* unmap_l3() invalidates any table walk caches as well */
			}

			unmap_l3(adspace, pt, va, next);

			if (next - va == 1ul << PTE_L2_SHIFT) {
				free_table(adspace, pt);
			}
		} else if (PTE_IS_PAGE(pte)) {
			/* large mapping */
			assert(next - va == 1ul << PTE_L2_SHIFT);
			/* unmap large page */
			riscv_pte_set(&l2[riscv_addr_to_l2(va)], 0);

			riscv_tlb_inval_page(adspace, va, adspace->asid);
		}

		va = next;
	}
}

#ifdef __LP64__
/** iterate level 1 table and unmap level 2 tables if necessary */
static void unmap_l1(struct adspace *adspace, riscv_pte_t *l1, addr_t va, addr_t end)
{
	riscv_pte_t pte;
	riscv_pte_t *pt;
	addr_t next;

	while (va < end) {
		next = NEXT_TABLE(va, 1ul << PTE_L1_SHIFT);
		if (next > end) {
			next = end;
		}

		pte = l1[riscv_addr_to_l1(va)];
		if (PTE_IS_VALID(pte)) {
			assert(PTE_IS_PT(pte));
			pt = PTE_TO_VIRT(pte);

			if (next - va == 1ul << PTE_L1_SHIFT) {
				/* unmap full level 2 page table */
				riscv_pte_set(&l1[riscv_addr_to_l1(va)], 0);

				/* unmap_l2() invalidates any table walk caches as well */
			}

			unmap_l2(adspace, pt, va, next);

			if (next - va == 1ul << PTE_L1_SHIFT) {
				free_table(adspace, pt);
			}
		}

		va = next;
	}
}
#endif

/** delete a mapping in an address space */
err_t arch_adspace_unmap(struct adspace *adspace, addr_t va, size_t size)
{
#ifdef __LP64__
	unmap_l1(adspace, adspace->pagetable, va, va + size);
#else /* 32-bit version */
	unmap_l2(adspace, adspace->pagetable, va, va + size);
#endif

	return EOK;
}


/** iterate level 2 table and unmap all level 3 tables */
static inline void flush_l2(struct adspace *adspace, riscv_pte_t *l2, addr_t va, addr_t end)
{
	riscv_pte_t pte;
	riscv_pte_t *pt;

	while (va < end) {
		pte = l2[riscv_addr_to_l2(va)];
		if (PTE_IS_PT(pte)) {
			pt = PTE_TO_VIRT(pte);

			l2[riscv_addr_to_l2(va)] = 0;

			/* unmap full level 3 page table */
			free_table(adspace, pt);
		} else if (PTE_IS_PAGE(pte)) {
			/* large mapping */
			l2[riscv_addr_to_l2(va)] = 0;
		}

		va += (1ul << PTE_L2_SHIFT);
	}
}

#ifdef __LP64__
/** iterate level 1 table and unmap all level 2 tables */
static inline void flush_l1(struct adspace *adspace, riscv_pte_t *l1, addr_t va, addr_t end)
{
	riscv_pte_t pte;
	riscv_pte_t *pt;

	while (va < end) {
		pte = l1[riscv_addr_to_l1(va)];
		if (PTE_IS_VALID(pte)) {
			assert(PTE_IS_PT(pte));
			pt = PTE_TO_VIRT(pte);

			l1[riscv_addr_to_l1(va)] = 0;

			flush_l2(adspace, pt, va, va + (1ul << PTE_L1_SHIFT));

			/* unmap full level 2 page table */
			free_table(adspace, pt);
		}

		va += (1ul << PTE_L1_SHIFT);
	}
}
#endif

/** destroy a user address space (complete unmap, but still active) */
void arch_adspace_destroy(struct adspace *adspace)
{
	riscv_pte_t *pt;

	/* switch to kernel page table, then clean up */
	pt = adspace->pagetable;
	adspace->pagetable = (riscv_pte_t *)adspace_dyn[0].pagetable;

	if (arch_percpu()->arch.current_adspace == adspace) {
		arch_adspace_switch_pagetable(adspace->pagetable, adspace->asid);
	}

	riscv_tlb_inval_asid(adspace, adspace->asid);

	/* the page table is no longer referenced in any table walk caches */
#ifdef __LP64__
	flush_l1(adspace, pt, 0, ARCH_USER_END);
#else /* 32-bit version */
	flush_l2(adspace, pt, 0, ARCH_USER_END);
#endif

	/* unmap page table */
	free_table(adspace, pt);
}

////////////////////////////////////////////////////////////////////////////////

/** setup a per-CPU in-kernel mapping */
addr_t arch_kmap_percpu(phys_addr_t pa, unsigned int prot, unsigned int cache)
{
	riscv_pte_t *pt = arch_adspace_xmap_pagetable;
	riscv_pte_t pte;
	addr_t xmap_va;

	pte = PHYS_TO_PTE(pa);
	pte |= PTE_V | PTE_R | PTE_A;
	if ((prot & PROT_WRITE) != 0) {
		pte |= PTE_W | PTE_D;
	}
	pte |= cache_bits(cache);

	xmap_va = ARCH_PERCPU_XMAP(current_cpu_id());

	if (pt[riscv_addr_to_l3(xmap_va)] != pte) {
		riscv_pte_set(&pt[riscv_addr_to_l3(xmap_va)], pte);

		riscv_tlb_inval_global_page_locally(xmap_va);
	}

	return xmap_va;
}

/** setup a per-CPU cross-address space mapping */
/** internal routine to get a user space page table entry (no blocks) */
static addr_t arch_adspace_xmap(struct adspace *adspace, addr_t va, int writable)
{
	riscv_pte_t *pt;
	riscv_pte_t pte;
	addr_t xmap_va;

	assert(va < ARCH_USER_END);

	/* level 1 table */
	pt = adspace->pagetable;
#ifdef __LP64__
	pte = pt[riscv_addr_to_l1(va)];
	if (!PTE_IS_PT(pte)) {
		/* no l2 page table */
		return 0;
	}

	/* level 2 table */
	pt = PTE_TO_VIRT(pte);
#endif
	pte = pt[riscv_addr_to_l2(va)];
	if (PTE_IS_PAGE(pte)) {
		/* large mapping */
		/* reconstruct missing bits from virtual address and assemble PTE */
		pte |= (va & 0x001ff000);
		goto hit;
	}
	if (!PTE_IS_PT(pte)) {
		/* no l3 page table */
		return 0;
	}

	/* level 3 table */
	pt = PTE_TO_VIRT(pte);
	pte = pt[riscv_addr_to_l3(va)];
	if (!PTE_IS_VALID(pte)) {
		/* no mapping */
		return 0;
	}

hit:
	if (!PTE_IS_READ(pte) || (writable && !PTE_IS_WRITE(pte))) {
		/* permission violation */
		return 0;
	}

	/* sanitize user space PTE to global kernel PTE */
	pte &= ~(PTE_U | PTE_X | 0ul);
	pte |= PTE_G;

	xmap_va = ARCH_PERCPU_XMAP(current_cpu_id()) + (va & PAGE_OFFSET);

	pt = arch_adspace_xmap_pagetable;
	if (pt[riscv_addr_to_l3(xmap_va)] != pte) {
		riscv_pte_set(&pt[riscv_addr_to_l3(xmap_va)], pte);

		riscv_tlb_inval_global_page_locally(xmap_va);
	}

	return xmap_va;
}

/** read from a remote adspace */
err_t arch_adspace_read(
	struct adspace *adspace,
	addr_t dst_va, /* current adspace */
	addr_t src_va, /* remote adspace */
	size_t size)
{
	addr_t xmap_va;
	size_t chunk;
	err_t err;

	assert(adspace != NULL);

	while (size > 0) {
		xmap_va = arch_adspace_xmap(adspace, src_va, false);
		if (xmap_va == 0) {
			/* mapping in remote adspace not accessible */
			return EFAULT;
		}

		chunk = PAGE_SIZE - (xmap_va & PAGE_OFFSET);
		if (chunk > size) {
			chunk = size;
		}

		err = arch_user_memcpy((void*)dst_va, (void*)xmap_va, chunk);
		if (err != EOK) {
			return err;
		}

		dst_va += chunk;
		src_va += chunk;
		size -= chunk;
	}

	return EOK;
}

/** write to a remote adspace */
err_t arch_adspace_write(
	struct adspace *adspace,
	addr_t dst_va, /* remote adspace */
	addr_t src_va, /* current adspace */
	size_t size,
	int coherent) /* cache coherency required */
{
	addr_t xmap_va;
	size_t chunk;
	err_t err;

	assert(adspace != NULL);

	while (size > 0) {
		xmap_va = arch_adspace_xmap(adspace, dst_va, true);
		if (xmap_va == 0) {
			/* mapping in remote adspace not accessible */
			return EFAULT;
		}

		chunk = PAGE_SIZE - (xmap_va & PAGE_OFFSET);
		if (chunk > size) {
			chunk = size;
		}

		err = arch_user_memcpy((void*)xmap_va, (void*)src_va, chunk);
		if (err != EOK) {
			return err;
		}

		if (coherent) {
			err = bsp_cache(CACHE_OP_INVAL_ICACHE_RANGE, xmap_va, chunk, dst_va);
			if (err != EOK) {
				return err;
			}
		}

		dst_va += chunk;
		src_va += chunk;
		size -= chunk;
	}

	return EOK;
}

/** bzero to a remote adspace */
err_t arch_adspace_bzero(
	struct adspace *adspace,
	addr_t dst_va, /* remote adspace */
	size_t size) /* cache coherency required */
{
	addr_t xmap_va;
	size_t chunk;
	err_t err;

	assert(adspace != NULL);

	while (size > 0) {
		xmap_va = arch_adspace_xmap(adspace, dst_va, true);
		if (xmap_va == 0) {
			/* mapping in remote adspace not accessible */
			return EFAULT;
		}

		chunk = PAGE_SIZE - (xmap_va & PAGE_OFFSET);
		if (chunk > size) {
			chunk = size;
		}

		err = arch_user_bzero((void*)xmap_va, chunk);
		if (err != EOK) {
			return err;
		}

		dst_va += chunk;
		size -= chunk;
	}

	return EOK;
}

/** user space virt to phys operation -- specialized version of __get_pte() */
err_t arch_adspace_v2p(
	struct adspace *adspace,
	addr_t va,
	phys_addr_t *pa,
	unsigned int *prot,
	addr_t *next)
{
	riscv_pte_t *pt;
	riscv_pte_t pte;
	size_t size;

	assert(adspace != NULL);
	assert(va < ARCH_USER_END);
	assert(pa != NULL);
	assert(prot != NULL);
	assert(next != NULL);

	pt = adspace->pagetable;

#ifdef __LP64__
	/* level 1 table */
	pte = pt[riscv_addr_to_l1(va)];
	if (!PTE_IS_PT(pte)) {
		*next = NEXT_TABLE(va, 1ul << PTE_L1_SHIFT);
		return ESTATE;
	}
	pt = PTE_TO_VIRT(pte);
#endif

	/* level 2 table */
	pte = pt[riscv_addr_to_l2(va)];
	if (PTE_IS_PAGE(pte)) {
		/* large mapping */
		size = 1ul << PTE_L2_SHIFT;
		goto hit;
	}
	if (!PTE_IS_PT(pte)) {
		*next = NEXT_TABLE(va, 1ul << PTE_L2_SHIFT);
		return ESTATE;
	}
	pt = PTE_TO_VIRT(pte);

	/* level 3 table */
	pte = pt[riscv_addr_to_l3(va)];
	if (!PTE_IS_VALID(pte)) {
		*next = NEXT_TABLE(va, PAGE_SIZE);
		return ESTATE;
	}
	size = PAGE_SIZE;

hit:
	*pa = PTE_TO_PHYS(pte) | (va & (size - 1));
	*prot = (PTE_IS_READ(pte) ? PROT_READ : 0) |
	        (PTE_IS_WRITE(pte) ? PROT_WRITE : 0) |
	        (PTE_IS_EXEC(pte) ? PROT_EXEC : 0);
	*next = NEXT_TABLE(va, PAGE_SIZE);
	return EOK;
}
