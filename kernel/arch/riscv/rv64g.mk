# SPDX-License-Identifier: MIT
# Copyright 2017, 2023 Alexander Zuepke
# rv64g.mk -- for RISC-V RV64G
#
# RISC-V architecture specific build rules
#
# azuepke, 2017-04-04: initial RISC-V port
# azuepke, 2023-09-02: imported

ARCH_CFLAGS := -march=rv64imac -mabi=lp64 -msmall-data-limit=0 -mno-save-restore
ARCH_CFLAGS += -mcmodel=medlow -DRISCV_RV64G

ARCH_CFLAGS_DEBUG := -Og
ARCH_CFLAGS_NDEBUG := -O2 -fomit-frame-pointer

ARCH_AFLAGS := -march=rv64imafdc -mabi=lp64
ARCH_AFLAGS += -DRISCV_RV64G
ARCH_LDFLAGS := -melf64lriscv

ARCH_MODS := entry exception adspace string clz


# Recommended user compiler and linker flags
ARCH_USER_CFLAGS := -march=rv64gc -mabi=lp64d -msmall-data-limit=0 -mcmodel=medany
ARCH_USER_AFLAGS := -march=rv64gc -mabi=lp64d
ARCH_USER_LDFLAGS := -melf64lriscv
