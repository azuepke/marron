# SPDX-License-Identifier: MIT
# Copyright 2023 Alexander Zuepke

case "$MARRON_SUBARCH" in
rv32*)
	WHICH_GCC=$(which riscv64-unknown-elf-gcc)
	if [ $? -ne 0 ]; then
		echo "No GCC (riscv64-unknown-elf-gcc) found!"
		echo "Yes, we're using the 64-bit toolchain for 32-bit as well!"
		echo "install toolchain:"
		echo "  $ sudo apt install gcc-riscv64-unknown-elf"
		return
	fi
	MARRON_CROSS_PREFIX=riscv64-unknown-elf-
	;;

rv64*)
	WHICH_GCC=$(which riscv64-unknown-elf-gcc)
	if [ $? -ne 0 ]; then
		echo "No GCC (riscv64-unknown-elf-gcc) found!"
		echo "install toolchain:"
		echo "  $ sudo apt install gcc-riscv64-unknown-elf"
		return
	fi
	MARRON_CROSS_PREFIX=riscv64-unknown-elf-
	;;
esac
