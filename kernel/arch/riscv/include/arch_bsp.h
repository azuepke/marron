/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * arch_bsp.h
 *
 * Architecture abstraction layer.
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported
 */

#ifndef ARCH_BSP_H_
#define ARCH_BSP_H_

#include <stdint.h>

/** private interface between architecture layer and BSP */
struct arch_bsp {
	/* unused */
};

/* TLB and cache management callbacks */
void bsp_remote_sfence_vma(unsigned long cpu_mask, addr_t start, size_t size);
void bsp_remote_sfence_vma_asid(unsigned long cpu_mask, addr_t start, size_t size, unsigned long asid);
void bsp_remote_fence_i(unsigned long cpu_mask);

#endif
