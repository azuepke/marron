/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * riscv_insn.h
 *
 * RISC-V architecture specific instructions.
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported
 */

#ifndef RISCV_INSN_H_
#define RISCV_INSN_H_

/** clear reservation */
static inline void riscv_clear_reservation(void)
{
	/* clear reservation by a dummy atomic store */
	unsigned int tmp;
	__asm__ volatile ("sc.w %0, zero, %1" : "=&r"(tmp), "+A"(tmp) : : "memory");
}

/** get SATP (supervisor address translation and protection register) */
static inline unsigned long riscv_get_satp(void)
{
	unsigned long val;
	__asm__ volatile ("csrr %0, satp" : "=r" (val) : : "memory");
	return val;
}

/** set SATP (supervisor address translation and protection register) */
static inline void riscv_set_satp(unsigned long val)
{
	__asm__ volatile ("csrw satp, %0" : : "r" (val) : "memory");
}

/** invalidate TLBs locally */
static inline void riscv_tlb_inval_all_locally(void)
{
	__asm__ volatile ("sfence.vma" : : : "memory");
}

/** invalidate specific address in TLB locally for all ASIDs */
static inline void riscv_tlb_inval_global_page_locally(unsigned long va)
{
	__asm__ volatile ("sfence.vma %0" : : "r" (va) : "memory");
}

/** invalidate specific address in given ASID in TLB locally */
static inline void riscv_tlb_inval_page_locally(unsigned long va, unsigned long asid)
{
	__asm__ volatile ("sfence.vma %0, %1" : : "r" (va), "r" (asid) : "memory");
}

/** invalidate specific ASID in TLB locally */
static inline void riscv_tlb_inval_asid_locally(unsigned long asid)
{
	__asm__ volatile ("sfence.vma zero, %0" : : "r" (asid) : "memory");
}

/** instruction-fetch fence */
static inline void riscv_fence_i(void)
{
	__asm__ volatile ("fence.i" : : : "memory");
}

/** pause instruction (Zihintpause extension) */
static inline void riscv_pause(void)
{
	__asm__ volatile (".word 0x0100000f" : : : "memory");	/* fence w,o (pause) */
}

/** set SCOUNTEREN (supervisor counter enable register) */
static inline void riscv_set_scounteren(unsigned long val)
{
	__asm__ volatile ("csrw scounteren, %0" : : "r" (val) : "memory");
}


/** read 64-bit cycle counter */
static inline unsigned long long riscv_rdcycle(void)
{
#ifdef __LP64__
	unsigned long long n;
	__asm__ volatile ("rdcycle %0" : "=r" (n));
	return n;
#else
	unsigned long h1;
	unsigned long lo;
	unsigned long h2;
	__asm__ volatile (
		"1:\n"
		"rdcycleh	%0\n"
		"rdcycle	%1\n"
		"rdcycleh	%2\n"
		"bne		%0, %2, 1b\n"
		: "=&r"(h1), "=&r"(lo), "=&r"(h2));
	return ((unsigned long long)h1 << 32) | lo;
#endif
}

/** read 64-bit time */
static inline unsigned long long riscv_rdtime(void)
{
#ifdef __LP64__
	unsigned long long n;
	__asm__ volatile ("rdtime %0" : "=r" (n));
	return n;
#else
	unsigned long u1;
	unsigned long lo;
	unsigned long u2;
	__asm__ volatile (
		"1:\n"
		"rdtimeh	%0\n"
		"rdtime		%1\n"
		"rdtimeh	%2\n"
		"bne		%0, %2, 1b\n"
		: "=&r"(u1), "=&r"(lo), "=&r"(u2));
	return ((unsigned long long)u1 << 32) | lo;
#endif
}

#endif
