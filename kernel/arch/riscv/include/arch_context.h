/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * arch_context.h
 *
 * RISC-V register context switching and helper functions
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 */

#ifndef ARCH_CONTEXT_H_
#define ARCH_CONTEXT_H_

#include <marron/regs.h>
#include <riscv_csr.h>
#include <riscv_insn.h>
#include <riscv_private.h>
#include <ex_table.h>
#include <arch.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>
#include <stddef.h>


/** initialize an idle thread's register context */
static inline void arch_reg_init_idle(
	struct regs *regs,
	ulong_t entry,
	ulong_t stack)
{
	regs->pc = entry;
	/* let function return to NULL address */
	regs->ra = 0;
	/* keep stack aligned to 16 bytes */
	regs->sp = stack & -16ul;
	regs->gp = 0;
	regs->tp = 0;
	regs->t0 = 0;
	regs->t1 = 0;
	regs->t2 = 0;
	regs->s0 = 0;
	regs->s1 = 0;
	regs->a0 = 0;
	regs->a1 = 0;
	regs->a2 = 0;
	regs->a3 = 0;
	regs->a4 = 0;
	regs->a5 = 0;
	regs->a6 = 0;
	regs->a7 = 0;
	regs->s2 = 0;
	regs->s3 = 0;
	regs->s4 = 0;
	regs->s5 = 0;
	regs->s6 = 0;
	regs->s7 = 0;
	regs->s8 = 0;
	regs->s9 = 0;
	regs->s10 = 0;
	regs->s11 = 0;
	regs->t3 = 0;
	regs->t4 = 0;
	regs->t5 = 0;
	regs->t6 = 0;
	/* run in super visor mode, but with interrupts enabled */
	regs->sr = SR_IDLE_BITS;
	regs->orig_a0 = 0;
}

/** initialize a user space thread's register context */
static inline void arch_reg_init(
	struct regs *regs,
	ulong_t entry,
	ulong_t stack,
	ulong_t tls,
	ulong_t arg1,
	ulong_t arg2,
	ulong_t arg3,
	ulong_t arg4,
	ulong_t arg5)
{
	regs->pc = entry;
	/* let function return to NULL address */
	regs->ra = 0;
	/* keep stack aligned to 16 bytes */
	regs->sp = stack & -16ul;
	regs->gp = 0;
	regs->tp = tls;
	regs->t0 = 0;
	regs->t1 = 0;
	regs->t2 = 0;
	regs->s0 = 0;
	regs->s1 = 0;
	regs->a0 = arg1;
	regs->a1 = arg2;
	regs->a2 = arg3;
	regs->a3 = arg4;
	regs->a4 = arg5;
	regs->a5 = 0;
	regs->a6 = 0;
	regs->a7 = 0;
	regs->s2 = 0;
	regs->s3 = 0;
	regs->s4 = 0;
	regs->s5 = 0;
	regs->s6 = 0;
	regs->s7 = 0;
	regs->s8 = 0;
	regs->s9 = 0;
	regs->s10 = 0;
	regs->s11 = 0;
	regs->t3 = 0;
	regs->t4 = 0;
	regs->t5 = 0;
	regs->t6 = 0;
	/* run in user mode */
	regs->sr = SR_USER_BITS;
	regs->orig_a0 = 0;
}

/** save additional registers, e.g. TLS */
static inline void arch_reg_save(struct regs *regs __unused)
{
	/* empty */
}

/** restore additional registers, e.g. TLS */
static inline void arch_reg_restore(const struct regs *regs __unused)
{
	/* empty */
}

/** additional cleanup on context switch */
static inline void arch_reg_switch(void)
{
	/* clear exclusive state */
	riscv_clear_reservation();
}

/** initialize a thread's FPU state to sane defaults */
static inline void arch_reg_fpu_init_disabled(struct regs *regs)
{
	regs->sr &= ~(1UL * SR_FS_BITS);
	regs->fcsr = 0;
	regs->fp_padding = 0;
	for (unsigned int i = 0; i < countof(regs->fpregs); i++) {
		regs->fpregs[i] = 0;
	}
}

/** enable FPU in register context */
static inline void arch_reg_fpu_enable(struct regs *regs)
{
	regs->sr |= SR_FS_BITS;
}

/** disable FPU in register context */
static inline void arch_reg_fpu_disable(struct regs *regs)
{
	regs->sr &= ~(1UL * SR_FS_BITS);
}

/** test if FPU is enabled in register context */
static inline int arch_reg_fpu_enabled(const struct regs *regs)
{
	return (regs->sr & SR_FS_BITS) != 0;
}

/** turn FPU on */
static inline void arch_fpu_enable(void)
{
	__asm__ volatile ("csrs sstatus, %0" : : "r" (SR_FS_BITS) : "memory");
}

/** turn FPU off */
static inline void arch_fpu_disable(void)
{
	__asm__ volatile ("csrc sstatus, %0" : : "r" (SR_FS_BITS) : "memory");
}

/** save FPU state */
static inline void arch_fpu_save(struct regs *regs)
{
	riscv_fpu_save(&regs->fpregs[0], &regs->fcsr);
}

/** restore FPU state */
static inline void arch_fpu_restore(const struct regs *regs)
{
	riscv_fpu_restore(&regs->fpregs[0], &regs->fcsr);
}

/** check if the thread is currently using the signal stack */
static inline int arch_on_stack(
	const struct regs *regs,
	void *stack_addr,
	size_t stack_size)
{
	if (stack_addr == NULL) {
		return 0;
	}
	return (((addr_t)regs->sp >  (addr_t)stack_addr) &&
	        ((addr_t)regs->sp <= (addr_t)stack_addr + stack_size));
}

/** allocate a register context frame on stack for a signal handler */
static inline addr_t arch_signal_alloc_frame(
	const struct regs *regs,
	addr_t sig_stack_base,
	size_t sig_stack_size)
{
	addr_t stack;

	/* default to current stack */
	stack = regs->sp;
	if (sig_stack_base != 0) {
		addr_t sig_stack_end = sig_stack_base + sig_stack_size;
		if ((stack <= sig_stack_base) || (stack > sig_stack_end)) {
			/* use signal stack */
			stack = sig_stack_end;
		}
	}

	/* allocate space for struct regs */
	stack -= sizeof(struct regs);

	/* align stack 16 bytes */
	stack &= -16ul;

	return stack;
}

/** make register context consistent for signal handling */
static inline void arch_signal_prepare(struct regs *regs __unused)
{
	/* empty */
}

/** setup registers to call signal handler of type sig_handler_t */
static inline void arch_signal_init(
	struct regs *regs,
	ulong_t handler,
	ulong_t frame,
	sys_sig_mask_t sig_mask,
	struct sys_sig_info *info_user)
{
	addr_t sp;

	/* data on stack (addresses growing down):
	 * - saved registers
	 * - struct sys_sig_info
	 * set stack pointer to last saved structure
	 */
	sp = (addr_t)info_user;

#ifdef __LP64__
	regs->a0 = frame;
	regs->a1 = sig_mask;
	regs->a2 = (addr_t)info_user;
#else
	/* NOTE: no alignment of register pairs on RISC-V */
	regs->a0 = frame;
	regs->a1 = sig_mask & 0xffffffff;
	regs->a2 = sig_mask >> 32;
	regs->a3 = (addr_t)info_user;
#endif

	regs->sp = sp;

	/* let function return to NULL address */
	regs->ra = 0;

	/* keep stack as is -- already properly aligned */
	regs->pc = handler;

	regs->sr = SR_USER_BITS;
}

/** fixup registers after signal handling */
static inline void arch_signal_fixup(struct regs *regs)
{
	/* fixup sstatus */
	regs->sr &= SR_USER_MASK;
	regs->sr |= SR_USER_BITS;
}

/** set current thread's TLS register */
static inline void arch_tls_set(struct regs *regs, addr_t tls)
{
	regs->tp = tls;
}


/** Correctly return a 64-bit value in system calls
 *
 * NOTE: This function is expected to be used in a tail-recursion fashion
 * at the end of a system call, i.e.:
 *   ulong_t sys_example(...)
 *   {
 *     ...
 *     return arch_syscall_return_64bit(current_thread()->regs, val);
 *   }
 * The function returns the bits that fit into the ulong_t return type.
 */
static inline ulong_t arch_syscall_return_64bit(struct regs *regs, uint64_t val)
{
#ifdef __LP64__
	/* 64-bit values fit into the ulong_t return type */
	(void)regs;
	return val;
#else
	/* the high 32-bit are kept in a1 */
	regs->a1 = (val >> 32);
	return val & 0xffffffffu;
#endif
}

/** Set the return value of a system call in the register context */
static inline void arch_syscall_retval(struct regs *regs, ulong_t val)
{
	regs->a0 = val;
}

/** fixup registers to restart a system call after signal handling */
static inline void arch_syscall_restart(struct regs *regs)
{
	regs->a0 = regs->orig_a0;

	/* rewind PC */
	/* SVC instruction is 4 bytes */
	regs->pc -= 4;
}

#endif
