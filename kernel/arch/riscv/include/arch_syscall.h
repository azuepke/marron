/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * arch_syscall.h
 *
 * RISC-V specific syscall definitions
 * NOTE: this file is included from the assembler syscall stubs
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported and adapted
 */

#ifndef ARCH_SYSCALL_H_
#define ARCH_SYSCALL_H_

#define SYSCALL_FUNC(typ, func, sys)	\
	.global func				;\
	.type func, %function		;\
	.balign 16					;\
	func:						;\
	_SYSCALL_ ## typ(sys)		;\
	.size func, . - func		;

/*
 * syscall args:
 *   ulong_t a0, a1, a2, a3, a4, a5, a6, a7; // x10..17
 *
 * syscall number:
 *   ulong_t t0; // x5
 */

#define _SYSCALL_IN8(name)		\
	li		t0, name			;\
	ecall						;\
	ret							;

#define _SYSCALL_IN0(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN1(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN2(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN3(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN4(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN5(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN6(name)	_SYSCALL_IN8(name)
#define _SYSCALL_IN7(name)	_SYSCALL_IN8(name)

#define _SYSCALL_ABORT1(name)	\
	/* caller address */		;\
	addi	a0, ra, -4			;\
	li		t0, name			;\
	ecall						;

#endif
