/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * arch_percpu_types.h
 *
 * RISC-V-specific per-CPU state.
 *
 * azuepke, 2023-09-02: adapted for RISC-V
 */

#ifndef ARCH_PERCPU_TYPES_H_
#define ARCH_PERCPU_TYPES_H_

#ifndef __ASSEMBLER__

/* forward declarations */
struct adspace;
struct regs;

/** per-CPU structure, kept in sscratch/tp. The kernel stack is put before */
struct arch_percpu {
	unsigned long scratch;
	struct regs *current_regs;
	void *kern_stack_top;
	struct adspace *current_adspace;
};

#endif

/* offsets of structure members referenced by assembler */
#define ARCH_PERCPU_SCRATCH_OFFSET			(0 * __SIZEOF_LONG__)
#define ARCH_PERCPU_CURRENT_REGS_OFFSET		(1 * __SIZEOF_LONG__)
#define ARCH_PERCPU_KERN_STACK_TOP_OFFSET	(2 * __SIZEOF_LONG__)

#endif
