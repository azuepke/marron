/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2017-2018, 2021, 2023 Alexander Zuepke */
/*
 * marron/arch_defs.h
 *
 * Architecture abstraction layer.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-07-17: reduced version for new kernel
 * azuepke, 2017-09-29: imported and adapted
 * azuepke, 2021-08-18: split arch.h and arch_defs.h
 * azuepke, 2023-09-02: adapted for RISC-V
 */

#ifndef MARRON_ARCH_DEFS_H_
#define MARRON_ARCH_DEFS_H_

/* for ELF loading */
#ifdef __LP64__
#define ARCH_ELF_CLASS		2	/* 64-bit */
#define ARCH_ELF_DATA		1	/* little endian */
#define ARCH_ELF_MACHINE	243	/* RISC-V */
#else
#define ARCH_ELF_CLASS		1	/* 32-bit */
#define ARCH_ELF_DATA		1	/* little endian */
#define ARCH_ELF_MACHINE	243	/* RISC-V */
#endif

#define ARCH_ELF_STKALN		16

/** architecture specific alignment, minimum a cache-line */
#define ARCH_ALIGN			64

/** architecture specific page size */
#define PAGE_SIZE			0x1000UL

/** architecture specific address space layout */
/* The ARCH_PERCPU_BASE area covers the last page table of the address space.
 * For each CPU, we use one cacheline in this page table for private mappings.
 * This means 16 pages for 16 CPUs on 32-bit, and 8 pages for 64 CPUs on 64-bit.
 * Right now, we only use one mapping for cross-copy operations.
 */
#ifdef __LP64__
#define ARCH_USER_BASE		0x0000000000000000UL
#define ARCH_USER_NULLGUARD	0x0000000000100000UL	/* 1M */
#define ARCH_USER_END		0x0000004000000000UL	/* 256G */
#define ARCH_KERNEL_BASE	0xffffffffc0000000UL	/* last 1G */
#define ARCH_IOMAP_BASE		0xfffffffff0000000UL	/* last 256M */
#define ARCH_PERCPU_BASE	0xffffffffffe00000UL	/* last 2M */
#define ARCH_PERCPU_XMAP(cpu)	(ARCH_PERCPU_BASE + (cpu)*0x8000UL)
#else
#define ARCH_USER_BASE		0x00000000UL
#define ARCH_USER_NULLGUARD	0x00100000UL	/* 1M */
#define ARCH_USER_END		0xc0000000UL	/* 3G */
#define ARCH_KERNEL_BASE	0xc0000000UL	/* last 1G */
#define ARCH_IOMAP_BASE		0xf0000000UL	/* last 256M */
#define ARCH_PERCPU_BASE	0xfff00000UL	/* last 1M */
#define ARCH_PERCPU_XMAP(cpu)	(ARCH_PERCPU_BASE + (cpu)*0x10000UL)
#endif

#endif
