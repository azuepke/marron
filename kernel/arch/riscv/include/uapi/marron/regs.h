/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * marron/regs.h
 *
 * Register frame on RISC-V.
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported
 */

#ifndef MARRON_REGS_H_
#define MARRON_REGS_H_

#include <marron/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/** exception register frame (user registers) */
struct regs {
	unsigned long pc;			/* sepc */
	unsigned long ra;			/* x1 */
	unsigned long sp;			/* x2 */
	unsigned long gp;			/* x3 */
	unsigned long tp;			/* x4 */
	unsigned long t0;			/* x5 */
	unsigned long t1;			/* x6 */
	unsigned long t2;			/* x7 */
	unsigned long s0;			/* x8 */
	unsigned long s1;			/* x9 */
	unsigned long a0;			/* x10 */
	unsigned long a1;			/* x11 */
	unsigned long a2;			/* x12 */
	unsigned long a3;			/* x13 */
	unsigned long a4;			/* x14 */
	unsigned long a5;			/* x15 */
	unsigned long a6;			/* x16 */
	unsigned long a7;			/* x17 */
	unsigned long s2;			/* x18 */
	unsigned long s3;			/* x19 */
	unsigned long s4;			/* x20 */
	unsigned long s5;			/* x21 */
	unsigned long s6;			/* x22 */
	unsigned long s7;			/* x23 */
	unsigned long s8;			/* x24 */
	unsigned long s9;			/* x25 */
	unsigned long s10;			/* x26 */
	unsigned long s11;			/* x27 */
	unsigned long t3;			/* x28 */
	unsigned long t4;			/* x29 */
	unsigned long t5;			/* x30 */
	unsigned long t6;			/* x31 */

	unsigned long sr;			/* sstatus */

	/* only valid for system calls */
	unsigned long orig_a0;		/* original a0 value on system call entry */

	/* register frame saved up until here on exception entry */

	/** FPU registers */
	unsigned long fcsr;			/* 32-bit only */
	unsigned long fp_padding;
	unsigned long long fpregs[32];
};

#ifdef __cplusplus
}
#endif

#endif
