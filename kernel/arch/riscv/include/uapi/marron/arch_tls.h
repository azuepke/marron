/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * marron/arch_tls.h
 *
 * RISC-V specific TLS code (for user space)
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported
 */

#ifndef MARRON_ARCH_TLS_H_
#define MARRON_ARCH_TLS_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

static inline unsigned long __tls_base(void)
{
	register unsigned long ___tp __asm__ ("tp");

#ifdef __clang__
	/* LLVM/Clang requires this, otherwise ___tp is uninitialized */
	__asm__ ("" : "=r" (___tp));
#endif

	return ___tp;
}

#define _TLS_PTR_1(offset) ((uint8_t  *)(__tls_base() + (offset)))
#define _TLS_PTR_2(offset) ((uint16_t *)(__tls_base() + (offset)))
#define _TLS_PTR_4(offset) ((uint32_t *)(__tls_base() + (offset)))
#define _TLS_PTR_8(offset) ((uint64_t *)(__tls_base() + (offset)))

/* getter */
static inline uint8_t tls_get_1(unsigned long offset)
{
	return *_TLS_PTR_1(offset);
}

static inline uint16_t tls_get_2(unsigned long offset)
{
	return *_TLS_PTR_2(offset);
}

static inline uint32_t tls_get_4(unsigned long offset)
{
	return *_TLS_PTR_4(offset);
}

static inline uint64_t tls_get_8(unsigned long offset)
{
	return *_TLS_PTR_8(offset);
}


/* setter */
static inline void tls_set_1(unsigned long offset, uint8_t val)
{
	*_TLS_PTR_1(offset)  = val;
}

static inline void tls_set_2(unsigned long offset, uint16_t val)
{
	*_TLS_PTR_2(offset)  = val;
}

static inline void tls_set_4(unsigned long offset, uint32_t val)
{
	*_TLS_PTR_4(offset)  = val;
}

static inline void tls_set_8(unsigned long offset, uint64_t val)
{
	*_TLS_PTR_8(offset)  = val;
}


/* pointers */
#ifdef __LP64__
#define tls_get_p(offset) ((void *)tls_get_8(offset))
#define tls_set_p(offset, val) tls_set_8((offset), (uint64_t)(val))
#else
#define tls_get_p(offset) ((void *)tls_get_4(offset))
#define tls_set_p(offset, val) tls_set_4((offset), (uint32_t)(val))
#endif

#ifdef __cplusplus
}
#endif

#endif
