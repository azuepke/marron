/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2015, 2017, 2021, 2023 Alexander Zuepke */
/*
 * arch.h
 *
 * Architecture abstraction layer.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-07-17: reduced version for new kernel
 * azuepke, 2017-09-29: imported and adapted
 * azuepke, 2021-08-18: split arch.h and arch_defs.h
 * azuepke, 2023-09-02: adapted for RISC-V
 */

#ifndef ARCH_H_
#define ARCH_H_

/** unsigned 64-bit integer type */
#ifndef __DEFINED_uint64_t
#define __DEFINED_uint64_t
#if __SIZEOF_LONG__ == 8
typedef unsigned long int uint64_t;
#else
typedef unsigned long long int uint64_t;
#endif
#endif


/* forward declaration */
struct regs;
struct arch_percpu;
struct adspace;

/* exception.c and exception64.c */
void arch_dump_registers(struct regs *regs, unsigned int status, unsigned long addr);
void arch_percpu_init(struct arch_percpu *arch, unsigned int cpu_id);
void arch_init_exceptions(void);

/* string.S */
void fast_clear_page(void *page);
void fast_copy_page(void *dst, const void *src);


/* for kernel boot message */
#if __SIZEOF_LONG__ == 8
#define ARCH_BANNER_NAME "RISC-V 64-bit"
#else
#define ARCH_BANNER_NAME "RISC-V 32-bit"
#endif

/* address conversion */
#define PHYS_TO_KERNEL(x)	((void *)((unsigned long)(x) + adspace_phys_to_kernel_offset))
#define KERNEL_TO_PHYS(x)	((unsigned long)(x) - adspace_phys_to_kernel_offset)

/* generated data */
extern const unsigned long adspace_phys_to_kernel_offset;

/** check if an interrupt is currently pending (for preemption by interrupts) */
static inline int arch_irq_pending(void)
{
	unsigned long pending_mask;
	unsigned long sip, sie;

	__asm__ volatile ("csrr %0, sip" : "=r" (sip));
	__asm__ volatile ("csrr %0, sie" : "=r" (sie));

	/* SIP indicates any interrupt pending to the hart, so filter by SIE */
	pending_mask = (sip & sie);

	return pending_mask != 0;
}

/** read 64-bit values atomically */
static inline uint64_t arch_load64(uint64_t *addr)
{
	/* NOTE: not atomic on 32-bit RISC-V! */
	return *(volatile uint64_t *)addr;
}

/** write 64-bit values atomically */
static inline void arch_store64(uint64_t *addr, uint64_t val)
{
	/* NOTE: not atomic on 32-bit RISC-V! */
	*(volatile uint64_t *)addr = val;
}

#if __SIZEOF_LONG__ == 4
/** 64 / 32 -> 32 bit division */
unsigned int udiv64_32(unsigned long long u, unsigned int v);
/** 64 / 64 -> 64 bit division */
unsigned long long udiv64_64(unsigned long long u, unsigned long long v);
#endif

#define KMEM_PAGETABLE_SIZE PAGE_SIZE

#endif
