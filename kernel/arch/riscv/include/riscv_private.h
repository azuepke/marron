/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * riscv_private.h
 *
 * RISC-V private functions
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 */

#ifndef RISCV_PRIVATE_H_
#define RISCV_PRIVATE_H_

#include <marron/regs.h>

/* entry.S */
void arch_entry(unsigned int cpu_id);
void riscv_fpu_save(void *fpregs, void *fcsr);
void riscv_fpu_restore(const void *fpregs, const void *fcsr);

/* exception.c */
void riscv_handler_irq_kern(struct regs *regs, unsigned int vector);
void riscv_handler_irq_user(struct regs *regs, unsigned int vector);
void riscv_handler_exception_kern(struct regs *regs, ulong_t cause, ulong_t stval);
void riscv_handler_exception_user(struct regs *regs, ulong_t cause, ulong_t stval);

#endif
