/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * riscv_pte.h
 *
 * RISC-V page table bits.
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 */

#ifndef RISCV_PTE_H_
#define RISCV_PTE_H_


/* PAGE_SIZE is defined in arch_defs.h */
#define PAGE_OFFSET			0x00000fffULL

/** entry bits of all page tables */
/* the upper bits (31..10) encode the phyiscal address 33..12 */
#define PTE_V				0x001		/* valid bit */
#define PTE_R				0x002		/* readable (always set for mapping) */
#define PTE_W				0x004		/* writeable */
#define PTE_X				0x008		/* executable */
#define PTE_U				0x010		/* user */
#define PTE_G				0x020		/* global */
#define PTE_A				0x040		/* accessed */
#define PTE_D				0x080		/* dirty */
#define PTE_OS1				0x100		/* operating system bit #1 */
#define PTE_OS2				0x200		/* operating system bit #2 */

#define PTE_FLAGS			0x3ffull
#ifdef __LP64__
#define PTE_ADDR_BITS		0x003ffffffffffc00ull
#else /* 32-bit */
#define PTE_ADDR_BITS		0xfffffc00ull
#endif

/* test various PTE flags */
#define PTE_IS_VALID(pte)	((pte) != 0)
#define PTE_IS_READ(pte)	(((pte) & PTE_R) != 0)
#define PTE_IS_WRITE(pte)	(((pte) & PTE_W) != 0)
#define PTE_IS_EXEC(pte)	(((pte) & PTE_X) != 0)
#define PTE_IS_NONE(pte)	(((pte) & PTE_U) != 0)	/* PROT_NONE encoding */

#define PTE_IS_PT(pte)		(((pte) & (PTE_V | PTE_R | PTE_W | PTE_X)) == PTE_V)
#define PTE_IS_PAGE(pte)	(((pte) & (PTE_V | PTE_R | PTE_W | PTE_X)) > PTE_V)

/* accessors for page table entries */
#define PTE_TO_PHYS(pte)	(((unsigned long long)((pte) & PTE_ADDR_BITS)) << 2)
#define PHYS_TO_PTE(phys)	((unsigned long)((phys) >> 2))

#define PTE_TO_VIRT(pte)	PHYS_TO_KERNEL(PTE_TO_PHYS(pte))
#define VIRT_TO_PTE(virt)	PHYS_TO_PTE(KERNEL_TO_PHYS(virt))

/* accessors for page table base register */
#ifdef __LP64__
#define SATP_TO_PHYS(x)		(((x) & 0x0000003ffffffffful) << 12)
#define PHYS_TO_SATP(x)		((x) >> 12)
#define SATP_ASID_MASK		0xfffful
#define SATP_ASID_SHIFT		44
#define SATP_ASID(x)		((((x) * 1ul) << SATP_ASID_SHIFT))
#define SATP_MODE_SV39		(0x8ul << 60)
#define SATP_MODE_SV48		(0x9ul << 60)
#define SATP_MODE_SV57		(0xaul << 60)
#else /* 32-bit */
#define SATP_TO_PHYS(x)		(((x) & 0x003ffffful) << 12)
#define PHYS_TO_SATP(x)		((x) >> 12)
#define SATP_ASID_MASK		0x1fful
#define SATP_ASID_SHIFT		22
#define SATP_ASID(x)		(((x) << SATP_ASID_SHIFT))
#define SATP_MODE_SV32		(0x1ul << 31)
#endif


/*
 * The RISC-V architecture uses 4K sized and aligned radix page tables.
 * 32-bit uses two levels, while 64-bit can use three, four or five levels.
 * We always stick to a four level model (similar to Arm)
 * and effectively only use three levels.
 *
 * Sv32 (2-level, 32-bit entries)
 *  SATP -> root pointer
 *          level 0 page table     (not used)
 *          level 1 page table     (not used)
 *          level 2 page table    bit 31 .. 22  (10 bit)
 *          level 3 page table    bit 21 .. 12  (10 bit)
 *
 * Sv39 (3-level, 64-bit entries)
 *  SATP -> root pointer
 *          level 0 page table     (not used)
 *          level 1 page table    bit 38 .. 30  (9 bit)
 *          level 2 page table    bit 29 .. 21  (9 bit)
 *          level 3 page table    bit 20 .. 12  (9 bit)
 */

#ifdef __LP64__

/** number of elements in a level 3 page table */
#define PTE_L3_NUM 512
#define PTE_L3_ALIGN 0x1000
#define PTE_L3_SHIFT 12

/** number of elements in a level 2 page table */
#define PTE_L2_NUM 512
#define PTE_L2_ALIGN 0x1000
#define PTE_L2_SHIFT 21

/** number of elements in a level 1 page table */
#define PTE_L1_NUM 512
#define PTE_L1_ALIGN 0x1000
#define PTE_L1_SHIFT 30

/** number of elements in a level 0 page table */
#define PTE_L0_NUM 1
#define PTE_L0_ALIGN 8
#define PTE_L0_SHIFT 39

#else /* 32-bit */

/** number of elements in a level 3 page table */
#define PTE_L3_NUM 1024
#define PTE_L3_ALIGN 0x1000
#define PTE_L3_SHIFT 12

/** number of elements in a level 2 page table */
#define PTE_L2_NUM 1024
#define PTE_L2_ALIGN 0x1000
#define PTE_L2_SHIFT 22

#endif

#ifndef __ASSEMBLER__

#include <stdint.h>

/** page table entry (32-bit on 32-bit or 64-bit on 64-bit on all levels) */
typedef unsigned long riscv_pte_t;

/** get level 3 PTE index from address */
static inline unsigned int riscv_addr_to_l3(addr_t addr)
{
	return (addr >> PTE_L3_SHIFT) & (PTE_L3_NUM - 1);
}

/** get level 2 PTE index from address */
static inline unsigned int riscv_addr_to_l2(addr_t addr)
{
	return (addr >> PTE_L2_SHIFT) & (PTE_L2_NUM - 1);
}

#ifdef __LP64__
/** get level 1 PTE index from address */
static inline unsigned int riscv_addr_to_l1(addr_t addr)
{
	return (addr >> PTE_L1_SHIFT) & (PTE_L1_NUM - 1);
}

/** get level 0 PTE index from address */
static inline unsigned int riscv_addr_to_l0(addr_t addr)
{
	return (addr >> PTE_L0_SHIFT) & (PTE_L0_NUM - 1);
}
#endif

/* writable pagetable for cross-copy mapping */
extern riscv_pte_t arch_adspace_xmap_pagetable[PTE_L3_NUM];

#endif

#endif
