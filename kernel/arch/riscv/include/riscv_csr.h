/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * riscv_csr.h
 *
 * Bits in RISC-V specific control registers, safe to include from assembler.
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 */

#ifndef RISCV_CSR_H_
#define RISCV_CSR_H_

/* SR (status register) bits */
#define SR_SIE		0x00000002	/**< s-mode interrupt enable */
#define SR_MIE		0x00000008	/**< m-mode interrupt enable */
#define SR_SPIE		0x00000020	/**< previous s-mode interrupt enable */
#define SR_UBE		0x00000040	/**< u-mode big endian */
#define SR_MPIE		0x00000080	/**< m-mode previous interrupt enable */
#define SR_SPP		0x00000100	/**< s-mode previous privilege: 0 u-mode, 1 s-mode */
#define SR_VS_BITS	0x00000600	/**< vector state: 00 off, 01 initial, 10 clean, 11 dirty */
#define SR_MPP_BITS	0x00001800	/**< m-mode previous privilege: 00 u-mode, 01 s-mode, 11 m-mode */
#define SR_FS_BITS	0x00006000	/**< FPU state: 00 off, 01 initial, 10 clean, 11 dirty */
#define SR_XS_BITS	0x00018000	/**< u-mode extension: 00 off, 01 on, 10 clean, 11 dirty */
#define SR_MPRV		0x00020000	/**< modify privilege (use mode from MPP) */
#define SR_SUM		0x00040000	/**< permit s-mode user memory access */
#define SR_MXR		0x00080000	/**< make executable readable */
#define SR_TVM		0x00100000	/**< trap virtual memory */
#define SR_TW		0x00200000	/**< timeout wait (trap WFI) */
#define SR_TSR		0x00400000	/**< strap SRET */
#define SR_UXL_BITS	(0x03l<<32)	/**< u-mode mode: 01 32-bit, 10 64-bit */
#define SR_UXL_32	(0x01l<<32)
#define SR_UXL_64	(0x02l<<32)
#define SR_SXL_BITS	(0x0cl<<32)	/**< s-mode mode: 01 32-bit, 10 64-bit */
#define SR_SBE		(0x10l<<32)	/**< s-mode big endian */
#define SR_MBE		(0x20l<<32)	/**< m-mode big endian */

/** SR default bits of idle thread */
#define SR_IDLE_BITS		(SR_SPIE | SR_SPP | SR_SUM)

#ifdef __LP64__
/** SR default user bits (64-bit) */
#define SR_USER_BITS		(SR_SPIE | SR_UXL_64 | SR_SUM)
#else
/** SR default user bits (32-bit) */
#define SR_USER_BITS		(SR_SPIE | SR_SUM)
#endif

/** SR bits the user can change */
#define SR_USER_MASK		(SR_FS_BITS)

/* SIP bits */
#define SIP_SSIP	0x00000002
#define SIP_STIP	0x00000020
#define SIP_SEIP	0x00000200

/* SIE bits */
#define SIE_SSIE	0x00000002
#define SIE_STIE	0x00000020
#define SIE_SEIE	0x00000200

/* exception cause */
#define CAUSE_INST_ALIGN	0	/**< instruction address misaligned */
#define CAUSE_INST_ACCESS	1	/**< instruction access fault */
#define CAUSE_ILLEGAL		2	/**< illegal instruction */
#define CAUSE_BREAKPOINT	3	/**< breakpoint */
#define CAUSE_LOAD_ALIGN	4	/**< load address misaligned */
#define CAUSE_LOAD_ACCESS	5	/**< load access fault */
#define CAUSE_STORE_ALIGN	6	/**< store address misaligned */
#define CAUSE_STORE_ACCESS	7	/**< store access fault */
#define CAUSE_ECALL_UMODE	8	/**< ecall from u-mode */
#define CAUSE_ECALL_SMODE	9	/**< ecall from s-mode */
#define CAUSE_ECALL_MMODE	11	/**< ecall from m-mode */
#define CAUSE_INST_FAULT	12	/**< instruction page fault */
#define CAUSE_LOAD_FAULT	13	/**< load page fault */
#define CAUSE_STORE_FAULT	15	/**< store page fault */

#endif
