/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2020, 2023 Alexander Zuepke */
/*
 * arch_spin.h
 *
 * RISC-V ticket spin locks
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2020-01-31: ticket locks
 * azuepke, 2023-09-02: imported
 */

#ifndef ARCH_SPIN_H_
#define ARCH_SPIN_H_

#include <arch_spin_types.h>
#include <marron/compiler.h>


/* increments */
#define ARCH_TICKET_INC 0x01000000
#define ARCH_SERVED_INC 0x00010000

/** spin lock initializer */
static inline void arch_spin_init(arch_spin_t *lock)
{
	lock->lock = 0;
}

/** try to lock spin lock, returns TRUE on success */
static inline int arch_spin_trylock(arch_spin_t *lock)
{
	uint32_t tmp, tmp2;
	arch_spin_t val;

	__asm__ volatile (
		"1:	lr.w	%0, %3			\n"
		"	srli	%1, %0, 24		\n"
		"	srli	%2, %0, 16		\n"
		"	xor		%1, %2, %1		\n"
		"	andi	%1, %1, 0xff	\n"
		"	bnez	%1, 2f			\n"
		"	add		%2, %0, %4		\n"
		"	sc.w	%1, %2, %3		\n"
		"	bnez	%1, 1b			\n"	/* lost reservation */
		"	fence	r,rw			\n"	/* success */
		"2:"
		: "=&r"(val), "=&r"(tmp), "=&r"(tmp2), "+A"(*lock) : "Ir"(ARCH_TICKET_INC) : "memory");


	/* memory barriers included above */
	return (tmp == 0);
}

/** lock spin lock */
static inline void arch_spin_lock(arch_spin_t *lock)
{
	uint32_t tmp, tmp2;
	arch_spin_t val;

	__asm__ volatile (
		"1:	lr.w	%0, %3			\n"
		"	add		%2, %0, %4		\n"
		"	sc.w	%1, %2, %3		\n"
		"	bnez	%1, 1b			\n"	/* lost reservation */
		: "=&r"(val), "=&r"(tmp), "=&r"(tmp2), "+A"(*lock) : "Ir"(ARCH_TICKET_INC) : "memory");

	while (val.s.ticket != val.s.served) {
		barrier();
		val.s.served = access_once(lock->s.served);
	}

	__asm__ volatile ("fence r,rw" : : : "memory");
}

/** unlock spin lock */
static inline void arch_spin_unlock(arch_spin_t *lock)
{
	__asm__ volatile ("fence rw,w" : : : "memory");
	lock->s.served++;
}

#endif
