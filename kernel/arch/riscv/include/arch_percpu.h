/* SPDX-License-Identifier: MIT */
/* Copyright 2023 Alexander Zuepke */
/*
 * arch_percpu.h
 *
 * Getter to per-CPU state.
 *
 * azuepke, 2023-09-02: adapted for RISC-V
 */

#ifndef ARCH_PERCPU_H_
#define ARCH_PERCPU_H_

/* forward declarations */
struct percpu;

/** get pointer to per-CPU data */
static inline struct percpu *arch_percpu(void)
{
	register struct percpu *___tp __asm__ ("tp");

#ifdef __clang__
	/* LLVM/Clang requires this, otherwise ___tp is uninitialized */
	__asm__ ("" : "=r" (___tp));
#endif

	return ___tp;
}

#endif
