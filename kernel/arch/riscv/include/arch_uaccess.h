/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2023 Alexander Zuepke */
/*
 * arch_uaccess.h
 *
 * RISC-V specific copy in/out code
 *
 * azuepke, 2017-04-04: initial RISC-V port
 * azuepke, 2023-09-02: imported & adapted
 */

#ifndef ARCH_UACCESS_H_
#define ARCH_UACCESS_H_

#ifndef UACCESS_H_
#error This file is included by uaccess.h
#endif

#include <ex_table.h>

#ifdef __LP64__
#define _LW "lwu"
#else
#define _LW "lw"
#endif

/* getter and setter with inline exception handling (returns EOK or EFAULT) */
#define arch_user_get_1(addr, val, err)	\
	__asm__ volatile (	\
		"1:	lbu		%1, %2	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	li		%0, %4	\n"	\
		"	j		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=&r" (err), "=&r" (val)	\
		: "m" (*(const unsigned char*)addr), "0" (EOK), "i" (EFAULT))

/* getter and setter with inline exception handling (returns EOK or EFAULT) */
#define arch_user_get_4(addr, val, err)	\
	__asm__ volatile (	\
		"1:	" _LW "	%1, %2	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	li		%0, %4	\n"	\
		"	j		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=&r" (err), "=&r" (val)	\
		: "m" (*(const unsigned int*)addr), "0" (EOK), "i" (EFAULT))

#ifdef __LP64__
#define arch_user_get_8(addr, val, err)	\
	__asm__ volatile (	\
		"1:	ld		%1, %2	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	li		%0, %4	\n"	\
		"	j		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=&r" (err), "=&r" (val)	\
		: "m" (*(const unsigned long*)addr), "0" (EOK), "i" (EFAULT))
#else
#define arch_user_get_8(addr, val, err)	\
	do {	\
		unsigned long __vall;	\
		unsigned long __valh;	\
		unsigned long long __val;	\
		__asm__ volatile (	\
			"1:	lw		%1, 0(%3)	\n"	\
			"2:	lw		%2, 4(%3)	\n"	\
			"3:	\n"	\
			".pushsection .text.exception,\"ax\"	\n"	\
			"4:	li		%0, %5	\n"	\
			"	j		3b	\n"	\
			".popsection	\n"	\
			EX_TABLE(1b, 4b)	\
			EX_TABLE(2b, 4b)	\
			: "=&r" (err), "=&r" (__vall), "=&r" (__valh)	\
			: "r" (addr), "0" (EOK), "i" (EFAULT));	\
		__val = __valh;	\
		__val <<= 32;	\
		__val |= __vall;	\
		(val) = __val;	\
	} while (0)
#endif

#define arch_user_put_1(addr, val, err)	\
	__asm__ volatile (	\
		"1:	sb		%2, %1	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	li		%0, %4	\n"	\
		"	j		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=r" (err), "=m" (*(unsigned int*)addr)	\
		: "r" (val), "0" (EOK), "i" (EFAULT))

#define arch_user_put_4(addr, val, err)	\
	__asm__ volatile (	\
		"1:	sw		%2, %1	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	li		%0, %4	\n"	\
		"	j		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=r" (err), "=m" (*(unsigned int*)addr)	\
		: "r" (val), "0" (EOK), "i" (EFAULT))

#ifdef __LP64__
#define arch_user_put_8(addr, val, err)	\
	__asm__ volatile (	\
		"1:	sd		%2, %1	\n"	\
		"2:	\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"3:	li		%0, %4	\n"	\
		"	j		2b	\n"	\
		".popsection	\n"	\
		EX_TABLE(1b, 3b)	\
		: "=r" (err), "=m" (*(unsigned long*)addr)	\
		: "r" (val), "0" (EOK), "i" (EFAULT))
#else
#define arch_user_put_8(addr, val, err)	\
	do {	\
		unsigned long long __val = (val);	\
		unsigned long __valh = __val >> 32;	\
		unsigned long __vall = __val;	\
		__asm__ volatile (	\
			"1:	sw		%2, 0(%1)	\n"	\
			"2:	sw		%3, 4(%1)	\n"	\
			"3:	\n"	\
			".pushsection .text.exception,\"ax\"	\n"	\
			"4:	li		%0, %5	\n"	\
			"	j		3b	\n"	\
			".popsection	\n"	\
			EX_TABLE(1b, 4b)	\
			EX_TABLE(2b, 4b)	\
			: "=r" (err)	\
			: "r" (addr), "r" (__vall), "r" (__valh), "0" (EOK), "i" (EFAULT));	\
	} while (0)
#endif

/* robust compare and exxchange */
#define arch_user_cas_4(addr, old, new, prev, err)	\
	__asm__ volatile (	\
		"1:	lr.w	%1, %2			\n"	\
		"	bne		%1, %3, 5f		\n"	\
		"2:	sc.w	%0, %4, %2		\n"	\
		"	bnez	%0, 1b			\n"	/* lost reservation */	\
		"	li		%0, %5			\n"	\
		"3:							\n"	\
		".pushsection .text.exception,\"ax\"	\n"	\
		"4:	li		%0, %7			\n"	\
		"	j		3b				\n"	\
		"5:	li		%0, %6			\n"	\
		"	j		3b				\n"	\
		".popsection				\n"	\
		EX_TABLE(1b, 4b)	\
		EX_TABLE(2b, 4b)	\
		: "=&r" (err), "=&r" (prev), "+A" (*addr)	\
		: "1" (old), "r" (new), "i" (EOK), "i" (EAGAIN), "i" (EFAULT)	\
		: "memory")

static inline err_t user_wordcpy_nocheck(void *dst, const void *src, size_t sz, size_t align)
{
	addr_t end = (addr_t)src + sz;

	unsigned long tmp;
	(void)align;

#ifdef __LP64__
	if (__builtin_constant_p(sz) && ((sz % sizeof(long)) == 0) && (align >= sizeof(long))) {
		/* copy in 64-bit steps */
		__asm__ volatile (
			"	j		3f	\n"
			"1:	ld		%0, (%1)	\n"
			"	addi	%1, %1, 8	\n"
			"2:	sd		%0, (%2)	\n"
			"	addi	%2, %2, 8	\n"
			"3:	bltu	%1, %3, 1b	\n"
			"	li		%0, %4	\n"
			"4:	\n"

			".pushsection .text.exception,\"ax\"	\n"
			"5:	li		%0, %5	\n"
			"	j		4b	\n"
			".popsection	\n"
			EX_TABLE(1b, 5b)
			EX_TABLE(2b, 5b)
			: "=&r" (tmp), "+&r" (src), "+&r" (dst)
			: "r" (end), "i" (EOK), "i" (EFAULT)
			: "memory");

		return (err_t)tmp;
	}
#endif

	/* copy in 32-bit steps */
	__asm__ volatile (
		"	j		3f	\n"
		"1:	lw		%0, (%1)	\n"
		"	addi	%1, %1, 4	\n"
		"2:	sw		%0, (%2)	\n"
		"	addi	%2, %2, 4	\n"
		"3:	bltu	%1, %3, 1b	\n"
		"	li		%0, %4	\n"
		"4:	\n"

		".pushsection .text.exception,\"ax\"	\n"
		"5:	li		%0, %5	\n"
		"	j		4b	\n"
		".popsection	\n"
		EX_TABLE(1b, 5b)
		EX_TABLE(2b, 5b)
		: "=&r" (tmp), "+&r" (src), "+&r" (dst)
		: "r" (end), "i" (EOK), "i" (EFAULT)
		: "memory");

	return (err_t)tmp;
}

/* string.S */
err_t arch_user_strlcpy_in(char *dst, const char *src_user, size_t n);
err_t arch_user_bzero(void *s_user, size_t n);
err_t arch_user_memcpy(void *dst_user, const void *src_user, size_t n);

#endif
