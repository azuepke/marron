/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * string_template.c
 *
 * Template for memcpy and string assembler functions.
 *
 * Compile:
 * $ gcc -c -Og  string_template.c && objdump -d string_template.o
 *
 * With -Og, gcc produces the most readable code, but doesn't merge tails.
 *
 * azuepke, 2025-02-01: initial
 */

#if __SIZEOF_LONG__ == 8
typedef __uint128_t uint128_t;
#endif
typedef unsigned long long uint64_t;
typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;
typedef unsigned long size_t;
typedef unsigned long uintptr_t;

static inline int copy_less_than_2(void *dst, const void *src, size_t n)
{
	if ((n & 1) != 0) {
		const uint8_t *s = (const uint8_t *)src;
		uint8_t *d = (uint8_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
	}

	return 0;
}

static inline int copy_less_than_4(void *dst, const void *src, size_t n)
{
	if ((n & 2) != 0) {
		const uint16_t *s = (const uint16_t *)src;
		uint16_t *d = (uint16_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
	}

	return copy_less_than_2(dst, src, n);
}

static inline int copy_less_than_8(void *dst, const void *src, size_t n)
{
	if ((n & 4) != 0) {
		const uint32_t *s = (const uint32_t *)src;
		uint32_t *d = (uint32_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
	}

	return copy_less_than_4(dst, src, n);
}

static inline int copy_less_than_16(void *dst, const void *src, size_t n)
{
	if ((n & 8) != 0) {
		const uint64_t *s = (const uint64_t *)src;
		uint64_t *d = (uint64_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
	}

	return copy_less_than_8(dst, src, n);
}

#ifdef __aarch64__
// aarch64-linux-gnu-gcc -c -Og string_template.c
int arch_user_memcpy(void *dst, const void *src, size_t n)
{
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x7) == 0) {
		while (n >= 16) {
			const uint128_t *s = (const uint128_t *)src;
			uint128_t *d = (uint128_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_16(dst, src, n);
		return 0;
	} else if ((((uintptr_t)dst | (uintptr_t)src) & 0x3) == 0) {
		while (n >= 4) {
			const uint32_t *s = (const uint32_t *)src;
			uint32_t *d = (uint32_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_4(dst, src, n);
		return 0;
	}

	while (n >= 1) {
		const uint8_t *s = (const uint8_t *)src;
		uint8_t *d = (uint8_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
		n -= sizeof(*s);
	}

	return 0;
}

void *memcpy(void *dst, const void *src, size_t n)
{
	void *orig_dst = dst;

	if ((((uintptr_t)dst | (uintptr_t)src) & 0x7) == 0) {
		while (n >= 16) {
			const uint128_t *s = (const uint128_t *)src;
			uint128_t *d = (uint128_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_16(dst, src, n);
		return orig_dst;
	} else if ((((uintptr_t)dst | (uintptr_t)src) & 0x3) == 0) {
		while (n >= 4) {
			const uint32_t *s = (const uint32_t *)src;
			uint32_t *d = (uint32_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_4(dst, src, n);
		return orig_dst;
	}

	while (n >= 1) {
		const uint8_t *s = (const uint8_t *)src;
		uint8_t *d = (uint8_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
		n -= sizeof(*s);
	}

	return orig_dst;
}

//static inline
uint128_t patterngen(int c8)
{
	uint16_t c16 = c8 << 8 | (c8 & 0xffu);
	uint32_t c32 = c16 << 16 | c16;
	uint64_t c64 = (uint64_t)c32 << 32 | c32;
	uint128_t c128 = (uint128_t)c64 << 64 | c64;
	return c128;
}

void *memset_small(void *s, int c8, size_t n)
{
	uint8_t *d = s;
	while (n--) {
		*d++ = c8;
	}
	return s;
}

void *memset(void *s, int c8, size_t n)
{
	void *orig_s = s;

	if (n < 1+2+4+16) {
		return memset_small(s, c8, n);
	}
	uint128_t c128 = patterngen(c8);
	if (((uintptr_t)s & 1) != 0) {
		uint8_t *d = s;
		*d++ = c128;
		s = d;
		n -= sizeof(*d);
	}
	if (((uintptr_t)s & 2) != 0) {
		uint16_t *d = s;
		*d++ = c128;
		s = d;
		n -= sizeof(*d);
	}
	if (((uintptr_t)s & 4) != 0) {
		uint32_t *d = s;
		*d++ = c128;
		s = d;
		n -= sizeof(*d);
	}

	/* we're now 8 byte aligned, sufficient for stp */
	while (n >= 16) {
		uint128_t *d = s;
		*d++ = c128;
		s = d;
		n -= sizeof(*d);
	}

	if ((n & 8) != 0) {
		uint64_t *d = s;
		*d++ = c128;
		s = d;
	}
	if ((n & 4) != 0) {
		uint32_t *d = s;
		*d++ = c128;
		s = d;
	}
	if ((n & 2) != 0) {
		uint16_t *d = s;
		*d++ = c128;
		s = d;
	}
	if ((n & 1) != 0) {
		uint8_t *d = s;
		*d++ = c128;
		s = d;
	}

	return orig_s;
}
#endif

#ifdef __arm__
// arm-linux-gnueabihf-gcc -marm -mgeneral-regs-only -c -Og string_template.c
int arch_user_memcpy(void *dst, const void *src, size_t n)
{
#if 0
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x7) == 0) {
		while (n >= 8) {
			const uint64_t *s = (const uint64_t *)src;
			uint64_t *d = (uint64_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_8(dst, src, n);
		return 0;
	}
	else
#endif
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x3) == 0) {
		while (n >= 4) {
			const uint32_t *s = (const uint32_t *)src;
			uint32_t *d = (uint32_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_4(dst, src, n);
		return 0;
	}

	while (n >= 1) {
		const uint8_t *s = (const uint8_t *)src;
		uint8_t *d = (uint8_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
		n -= sizeof(*s);
	}

	return 0;
}

void *memcpy(void *dst, const void *src, size_t n)
{
	void *orig_dst = dst;

#if 1
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x7) == 0) {
		while (n >= 8) {
			const uint64_t *s = (const uint64_t *)src;
			uint64_t *d = (uint64_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_8(dst, src, n);
		return orig_dst;
	}
	else
#endif
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x3) == 0) {
		while (n >= 4) {
			const uint32_t *s = (const uint32_t *)src;
			uint32_t *d = (uint32_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_4(dst, src, n);
		return orig_dst;
	}

	while (n >= 1) {
		const uint8_t *s = (const uint8_t *)src;
		uint8_t *d = (uint8_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
		n -= sizeof(*s);
	}

	return orig_dst;
}

static inline
uint64_t patterngen(int c8)
{
	uint16_t c16 = c8 << 8 | (c8 & 0xffu);
	uint32_t c32 = c16 << 16 | c16;
	uint64_t c64 = (uint64_t)c32 << 32 | c32;
	return c64;
}

void *memset_small(void *s, int c8, size_t n)
{
	uint8_t *d = s;
	while (n--) {
		*d++ = c8;
	}
	return s;
}

void *memset(void *s, int c8, size_t n)
{
	void *orig_s = s;

	if (n < 1+2+8) {
		return memset_small(s, c8, n);
	}
	uint64_t c64 = patterngen(c8);
	if (((uintptr_t)s & 1) != 0) {
		uint8_t *d = s;
		*d++ = c64;
		s = d;
		n -= sizeof(*d);
	}
	if (((uintptr_t)s & 2) != 0) {
		uint16_t *d = s;
		*d++ = c64;
		s = d;
		n -= sizeof(*d);
	}

	/* we're now 4 byte aligned, sufficient for strd */
	while (n >= 8) {
		uint64_t *d = s;
		*d++ = c64;
		s = d;
		n -= sizeof(*d);
	}

	if ((n & 4) != 0) {
		uint32_t *d = s;
		*d++ = c64;
		s = d;
	}
	if ((n & 2) != 0) {
		uint16_t *d = s;
		*d++ = c64;
		s = d;
	}
	if ((n & 1) != 0) {
		uint8_t *d = s;
		*d++ = c64;
		s = d;
	}

	return orig_s;

}
#endif

#ifdef __riscv
// riscv64-unknown-elf-gcc -march=rv64ima -mabi=lp64 -c -Og string_template.c
// riscv64-unknown-elf-gcc -march=rv32ima -mabi=ilp32 -c -Og string_template.c && objdump -d string_template.o
int arch_user_memcpy(void *dst, const void *src, size_t n)
{
#if __SIZEOF_LONG__ == 8
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x7) == 0) {
		while (n >= 8) {
			const uint64_t *s = (const uint64_t *)src;
			uint64_t *d = (uint64_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_8(dst, src, n);
		return 0;
	}
	else
#endif
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x3) == 0) {
		while (n >= 4) {
			const uint32_t *s = (const uint32_t *)src;
			uint32_t *d = (uint32_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_4(dst, src, n);
		return 0;
	}

	while (n >= 1) {
		const uint8_t *s = (const uint8_t *)src;
		uint8_t *d = (uint8_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
		n -= sizeof(*s);
	}

	return 0;
}

void *memcpy(void *dst, const void *src, size_t n)
{
	void *orig_dst = dst;

#if __SIZEOF_LONG__ == 8
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x7) == 0) {
		while (n >= 8) {
			const uint64_t *s = (const uint64_t *)src;
			uint64_t *d = (uint64_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_8(dst, src, n);
		return orig_dst;
	}
	else
#endif
	if ((((uintptr_t)dst | (uintptr_t)src) & 0x3) == 0) {
		while (n >= 4) {
			const uint32_t *s = (const uint32_t *)src;
			uint32_t *d = (uint32_t *)dst;
			*d++ = *s++;
			src = s;
			dst = d;
			n -= sizeof(*s);
		}
		copy_less_than_4(dst, src, n);
		return orig_dst;
	}

	while (n >= 1) {
		const uint8_t *s = (const uint8_t *)src;
		uint8_t *d = (uint8_t *)dst;
		*d++ = *s++;
		src = s;
		dst = d;
		n -= sizeof(*s);
	}

	return orig_dst;
}

static inline
uint64_t patterngen(unsigned long c)
{
	uint64_t c8 = c & 0xffu;
	uint64_t c16 = c8 << 8 | c8;
	uint64_t c32 = c16 << 16 | c16;
	uint64_t c64 = c32 << 32 | c32;
	return c64;
}

void *memset_small(void *s, int c8, size_t n)
{
	uint8_t *d = s;
	while (n--) {
		*d++ = c8;
	}
	return s;
}

void *memset(void *s, int c8, size_t n)
{
	void *orig_s = s;

	if (n < 1+2+4+16) {
		return memset_small(s, c8, n);
	}
	uint64_t c64 = patterngen(c8);
	if (((uintptr_t)s & 1) != 0) {
		uint8_t *d = s;
		*d++ = c64;
		s = d;
		n -= sizeof(*d);
	}
	if (((uintptr_t)s & 2) != 0) {
		uint16_t *d = s;
		*d++ = c64;
		s = d;
		n -= sizeof(*d);
	}
	if (((uintptr_t)s & 4) != 0) {
		uint32_t *d = s;
		*d++ = c64;
		s = d;
		n -= sizeof(*d);
	}

	/* we're now 8 byte aligned */
	while (n >= 8) {
		uint64_t *d = s;
		*d++ = c64;
		s = d;
		n -= sizeof(*d);
	}

	if ((n & 4) != 0) {
		uint32_t *d = s;
		*d++ = c64;
		s = d;
	}
	if ((n & 2) != 0) {
		uint16_t *d = s;
		*d++ = c64;
		s = d;
	}
	if ((n & 1) != 0) {
		uint8_t *d = s;
		*d++ = c64;
		s = d;
	}

	return orig_s;
}

#endif
