/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * ipc_types.h
 *
 * Inter-process communication data types.
 *
 * azuepke, 2022-07-17: initial
 */

#ifndef IPC_TYPES_H_
#define IPC_TYPES_H_

#include <marron/types.h>
#include <stdint.h>
#include <marron/list.h>
#include <spin.h>
#include <marron/compiler.h>
#include <fd_types.h>


/** ipc_client configuration data */
struct ipc_client_cfg {
	/** pointer to client_fd or NULL if the channel is not connected */
	struct fd *client_fd;
	/** pointer to server_fd or NULL if the channel is not connected */
	struct fd *server_fd;
	/** local name of the client IPC endpoint */
	const char *name;
	/** static fd index (to client_fd) */
	uint32_t static_fd_index;
};

/** ipc_server configuration data */
struct ipc_server_cfg {
	/** pointer to server_fd */
	struct fd *server_fd;
	/** local name of the server IPC endpoint */
	const char *name;
	/** static fd index (to server_fd) */
	uint32_t static_fd_index;
};

/* generated data */
extern const uint8_t ipc_client_num;
extern const struct ipc_client_cfg ipc_client_cfg[];

extern const uint8_t ipc_server_num;
extern const struct ipc_server_cfg ipc_server_cfg[];

extern const uint8_t ipc_fd_num;
extern struct fd ipc_fd_dyn[];

#endif
