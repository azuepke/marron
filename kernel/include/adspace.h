/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2021 Alexander Zuepke */
/*
 * adspace.h
 *
 * Adspace management.
 *
 * azuepke, 2021-04-28: initial
 */

#ifndef ADSPACE_H_
#define ADSPACE_H_

#include <adspace_types.h>
#include <spin.h>
#include <marron/mapping.h>

/* API */

/** initialize all adspace objects at boot time */
void arch_adspace_init_all(void);

/** change MMU page tables on context switch */
void arch_adspace_switch(struct adspace *prev, struct adspace *next);

/** initializes a user address space */
err_t arch_adspace_init(struct adspace *adspace);

/** create a mapping of a physical resource in an address space */
err_t arch_adspace_map(struct adspace *adspace, addr_t va, size_t size,
	phys_addr_t pa, unsigned int prot, unsigned int cache);

/** change access permission a mapping in an address space */
err_t arch_adspace_protchange(struct adspace *adspace, addr_t va, size_t size,
	unsigned int prot);

/** delete a mapping in an address space */
err_t arch_adspace_unmap(struct adspace *adspace, addr_t va, size_t size);

/** destroy a user address space (complete unmap, but still active) */
void arch_adspace_destroy(struct adspace *adspace);

/** setup a per-CPU in-kernel mapping */
addr_t arch_kmap_percpu(phys_addr_t pa, unsigned int prot, unsigned int cache);

/** read from a remote adspace */
err_t arch_adspace_read(
	struct adspace *adspace,
	addr_t dst_va, /* current adspace */
	addr_t src_va, /* remote adspace */
	size_t size);

/** write to a remote adspace */
err_t arch_adspace_write(
	struct adspace *adspace,
	addr_t dst_va, /* remote adspace */
	addr_t src_va, /* current adspace */
	size_t size,
	int coherent); /* cache coherency required */

/** bzero to a remote adspace */
err_t arch_adspace_bzero(
	struct adspace *adspace,
	addr_t dst_va, /* remote adspace */
	size_t size); /* cache coherency required */

/** user space virt to phys operation */
err_t arch_adspace_v2p(
	struct adspace *adspace,
	addr_t va,
	phys_addr_t *pa,
	unsigned int *prot,
	addr_t *next);

static inline void adspace_lock(struct adspace *adspace)
{
	spin_lock(&adspace->lock);
}

static inline void adspace_unlock(struct adspace *adspace)
{
	spin_unlock(&adspace->lock);
}

#endif
