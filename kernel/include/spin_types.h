/* SPDX-License-Identifier: MIT */
/* Copyright 2014, 2020 Alexander Zuepke */
/*
 * spin_types.h
 *
 * Common spin lock operations for kernel use using a Linux-like interface
 *
 * NOTE: The spinlocks are optimized out in UP builds.
 *
 * azuepke, 2014-08-26: initial
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef SPIN_TYPES_H_
#define SPIN_TYPES_H_

#include <arch_spin_types.h>

/** spin lock data type */
typedef struct {
#ifdef SMP
	arch_spin_t arch_lock;
#else
	int unused;
#endif
} spin_t;


/** spin lock static initializer */
#ifdef SMP
#ifndef NDEBUG
#define SPIN_INIT { ARCH_SPIN_INIT_DEBUG }
#else
#define SPIN_INIT { ARCH_SPIN_INIT }
#endif
#else
#define SPIN_INIT { 0 }
#endif

#endif
