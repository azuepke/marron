/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024, 2025 Alexander Zuepke */
/*
 * fd.h
 *
 * File descriptor management.
 *
 * azuepke, 2022-07-02: initial
 * azuepke, 2025-02-21: rework fd lookup
 */

#ifndef FD_H_
#define FD_H_

#include <marron/types.h>
#include <fd_types.h>
#include <spin.h>


/* forward declaration */
struct process;
struct thread;
struct part;

/* API */
/** initialize file descriptors of a partition at boot time */
void fd_init_all(
	struct part *part);

/** get and lock fd */
struct fd *fd_get_and_lock(
	struct process *proc,
	ulong_t fd_index);

/** get and lock fd of specific type */
err_t fd_get_type_and_lock(
	struct process *proc,
	ulong_t fd_index,
	unsigned int fd_type,
	struct fd **fd_p);

/** lock two fds (preventing deadlocks) */
void fd_lock2(
	struct fd *fd0,
	struct fd *fd1);

/** get and lock two used fds (preventing deadlocks) */
err_t fd_get_and_lock2(
	struct process *proc,
	ulong_t fd_index0,
	ulong_t fd_index1,
	struct fd **fd0_p,
	struct fd **fd1_p);


/** allocate an unused fd (the fd is unlocked; return ENFILE on failure) */
struct fd *fd_alloc(
	struct process *proc);

/** dup one unlocked fd (process is locked) */
err_t fd_dup_locked(
	struct process *proc,
	struct fd *fd,
	int cloexec,
	unsigned int *fd_index_p);

/** dup one unlocked fd (process is unlocked) */
err_t fd_dup(
	struct process *proc,
	struct fd *fd,
	int cloexec,
	unsigned int *fd_index_p);

/** atomical dup of two unlocked fds (process is unlocked) */
err_t fd_dup2(
	struct process *proc,
	struct fd *fd0,
	struct fd *fd1,
	int cloexec,
	unsigned int fd_indices[]);

/** close() implementation */
void fd_close(
	struct fd *fd);

/** close fd_table entries */
err_t fd_table_close(
	struct process *proc,
	ulong_t fd_index);

static inline void fd_lock(struct fd *fd)
{
	spin_lock(&fd->lock);
}

static inline void fd_unlock(struct fd *fd)
{
	spin_unlock(&fd->lock);
}

/** correct nested locking of first process, then fd */
void fd_relock_with_proc(struct process *proc, struct fd *fd);

/** nested unlocking of process and fd */
void fd_unlock_with_proc(struct process *proc, struct fd *fd);

#define assert_fd_locked(fd) assert(spin_locked(&(fd)->lock))
#define assert_fd_used(fd) assert((fd)->usage > 0)

/* default I/O operations for IPC-based in-kernel servers */
err_t fd_ipc_io_default_err(struct fd *fd, struct thread *thr);
err_t fd_ipc_io_getoflags(struct fd *fd, struct thread *thr);
err_t fd_ipc_io_setoflags(struct fd *fd, struct thread *thr);
err_t fd_ipc_io_stat_dummy(struct fd *fd, struct thread *thr);
err_t fd_ipc_io_seek_file(struct fd *fd, struct thread *thr);

/* generic IPC copy helper */
err_t fd_ipc_copy_in(struct thread *thr, void *dst_kern, size_t size);
err_t fd_ipc_copy_out(struct thread *thr, const void *src_kern, size_t size);

/** dispatcher for generic IPC-based I/O requests
 *
 * We use this dispatcher for all file descriptors that are not files,
 * but want to return something meaningful in fcntl() or stat() operations.
 * The callbacks do not depending on struct fd.
 */
err_t fd_generic_ipc_call(struct fd *fd);

#endif
