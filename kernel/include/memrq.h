/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2021 Alexander Zuepke */
/*
 * memrq.h
 *
 * Memory requirement / pool allocation.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 * azuepke, 2021-04-30: pool allocations
 */

#ifndef MEMRQ_H_
#define MEMRQ_H_

#include <memrq_types.h>
#include <marron/error.h>


/* forward declarations */
struct fd;


/* memory allocation failure (we cannot use 0, as it is a valid phys address) */
#define PART_MEMRQ_ALLOC_FAIL (-1ull)


/* API */
/** initialize all memrq objects at boot time */
__init void memrq_init_all(void);

/** reset order of free pages in memrq at partition startup */
err_t memrq_page_reinit(const struct memrq_cfg *cfg);

/** allocate a page from memrq */
phys_addr_t memrq_page_alloc(const struct memrq_cfg *cfg);

/** ref a page from memrq */
void memrq_page_ref(const struct memrq_cfg *cfg, phys_addr_t phys);

/** unref a page from memrq */
void memrq_page_unref(const struct memrq_cfg *cfg, phys_addr_t phys);

/** get struct page from <memrq> by physical address */
struct page *memrq_addr2page(const struct memrq_cfg *cfg, phys_addr_t phys);

/** get struct page from <mem> by physical address */
struct page *mem_addr2page(phys_addr_t addr);

/** dispatcher for IPC-based I/O requests */
err_t memrq_ipc_call(struct fd *fd);

/** close callback for memory requirements */
void memrq_close(
	struct fd *fd);

#endif
