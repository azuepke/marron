/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Alexander Zuepke */
/*
 * percpu_types.h
 *
 * Per-CPU data types.
 *
 * azuepke, 2018-03-05: initial
 */

#ifndef PERCPU_TYPES_H_
#define PERCPU_TYPES_H_

#ifndef __ASSEMBLER__

#include <marron/compiler.h>
#include <marron/arch_defs.h>
#include <arch_percpu_types.h>


/** per-CPU configuration data */
struct percpu_cfg {
	/** pointer to per-CPU data */
	struct percpu *percpu;
	/** pointer to top of per-CPU kernel stack */
	void *kern_stack_top;
	/** pointer to top of per-CPU NMI stack */
	void *nmi_stack_top;
	/** pointer to top of per-CPU MCE (machine check exception) stack */
	void *mce_stack_top;
	/** pointer to top of per-CPU idle thread stack */
	void *idle_stack_top;
};

/** per-CPU object */
struct percpu {
	/** architecture specific CPU-private data */
	struct arch_percpu arch;

	/** pointer to current thread */
	struct thread *current_thread;

	/** pointer to current process */
	struct process *current_process;

	/** pointer to current partition */
	struct part *current_part;

	/** current CPU ID (const after init) */
	unsigned int cpu_id;
} __aligned(ARCH_ALIGN);

/* generated data */
extern const struct percpu_cfg percpu_cfg[];
/* NOTE: the per-CPU dyn variables are not kept in an array,
 * but in CPU-specific linker sections
 */

#endif

#define PERCPU_CFG_PERCPU_OFFSET			(0 * __SIZEOF_LONG__)
#define PERCPU_CFG_KERN_STACK_TOP_OFFSET	(1 * __SIZEOF_LONG__)
#define PERCPU_CFG_NMI_STACK_TOP_OFFSET		(2 * __SIZEOF_LONG__)
#define PERCPU_CFG_MCE_STACK_TOP_OFFSET		(3 * __SIZEOF_LONG__)
#define PERCPU_CFG_IDLE_STACK_TOP_OFFSET	(4 * __SIZEOF_LONG__)
#define PERCPU_CFG_SIZE						(5 * __SIZEOF_LONG__)

#endif
