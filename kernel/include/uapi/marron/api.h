/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2020, 2021, 2022, 2024, 2025 Alexander Zuepke */
/*
 * marron/api.h
 *
 * Kernel user API.
 *
 * azuepke, 2018-01-02: initial
 * azuepke, 2018-01-04: cleaned up for API documentation
 * azuepke, 2018-12-21: standalone version
 * azuepke, 2020-02-01: changes for Kuri: TLS for fast priority switching
 * azuepke, 2020-02-02: changes for Kuri: futex lock and unlock operations
 * azuepke, 2020-02-02: changes for Kuri: light-weight synchronization
 * azuepke, 2020-02-02: changes for Kuri: mutex and condition variable syscalls
 * azuepke, 2021-07-23: new scheduling parameters
 * azuepke, 2021-08-16: new timer API
 * azuepke, 2021-08-18: revised time format with explicit clock IDs
 * azuepke, 2022-07-02: file descriptor handling
 * azuepke, 2022-07-17: IPC API
 * azuepke, 2022-10-19: timerfd
 * azuepke, 2024-12-08: epoll
 * azuepke, 2024-12-29: eventfd
 * azuepke, 2025-01-05: merge timerfd into timer
 * azuepke, 2025-01-06: irqfd
 * azuepke, 2025-02-14: signalfd
 * azuepke, 2025-02-22: processes
 */

#ifndef MARRON_API_H_
#define MARRON_API_H_

#include <marron/types.h>
#include <marron/error.h>

#ifdef __cplusplus
extern "C" {
#endif

/* compiler specific */
#define __noreturn __attribute__((__noreturn__))

/* forward */
struct regs;

/** Abort execution of current partition
 *
 * \note A call to this function does not return.
 */
void sys_abort(void) __noreturn;

/** Print a character to the system console
 *
 * This function tries to print a character to the system console.
 * If the system console is currently busy, e.g. the serial FIFO is full,
 * the function returns an error.
 *
 * \param [in] c			Character to print
 *
 * \retval EOK				Success
 * \retval EBUSY			Console is busy, try again later
 */
err_t sys_putc(unsigned char c);

/** Start a thread at runtime with variable parameters
 *
 * This function activates a new thread in the context of the calling partition
 * and returns the thread ID in "thr_id", if not NULL.
 * The thread's user space register context is initialized
 * with the given entry point, argument, stack pointer, and TLS pointer.
 * The parameter "sched" defines the thread's scheduling parameters.
 * The initial priority is limited to the partition's maximum priority.
 * The per-thread scheduling quantum controls round-robin scheduling
 * with the given quantum in nanoseconds, with zero being an infinite quantum.
 * The CPU affinity mask is restricted to the subset of available CPUs.
 * The thread is assigned to the lowest available CPU in the affinity mask.
 * The thread's FPU state is set to FPU_STATE_AUTO.
 * Mandatory fields in the thread's TLS state are initialized by the kernel.
 *
 * \param [out] thr_id		Thread ID
 * \param [in] entry		Entry point
 * \param [in] arg			Argument passed to thread
 * \param [in] stacḱ		Initial stack pointer
 * \param [in] tls			Pointer to TLS
 * \param [in] sched		Initial scheduling parameters
 * \param [in] cpu_mask		CPU affinity mask
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the maximum number of threads is reached
 * \retval EINVAL			If the intersection of the affinity mask and the partition's CPU mask is empty
 * \retval EINVAL			If the TLS pointer is NULL or not properly aligned
 * \retval EFAULT			Invalid address or page fault
 *
 * \note Thread IDs provided by the kernel are integer indices starting at 0,
 * e.g. the first thread in a partition always has the ID 0.
 * The maximum number of threads a partition is controlled by the configuration.
 *
 * \note Following the style of POSIX pthread_create(), this function
 * returns a thread ID as output argument in the first parameter,
 * rather than in the last parameters as the other kernel API functions do.
 *
 * \see sys_thread_exit()
 * \see sys_tls_set()
 */
err_t sys_thread_create(
	uint32_t *thr_id,
	void (*entry)(void *),
	void *arg,
	void *stack,
	void *tls,
	const struct sys_sched_param *sched,
	ulong_t cpu_mask);

/** Terminate the current thread
 *
 * This function terminates the current thread.
 *
 * \note A call to this function does not return.
 *
 * \see sys_thread_create()
 */
void sys_thread_exit(void) __noreturn;

/** Retrieve thread ID of calling thread
 *
 * This function returns the thread ID of the calling thread.
 *
 * \return Thread ID
 *
 * \see sys_part_self()
 * \see sys_process_self()
 */
uint32_t sys_thread_self(void);
uint32_t sys_thread_self_syscall(void);

/** Retrieve priority of calling thread
 *
 * This function returns the schedulign priority of the calling thread.
 *
 * \return Priority
 *
 * \see sys_prio_set()
 * \see sys_prio_max()
 * \see sys_cpu_get()
 * \see sys_cpu_set()
 * \see sys_cpu_mask()
 * \see sys_thread_sched()
 */
uint32_t sys_prio_get(void);
uint32_t sys_prio_get_syscall(void);

/** Change priority of calling thread
 *
 * This function changes the scheduling priority of the calling thread
 * to the given priority and returns its previous schedulign priority.
 * The priority is capped to the partition's maximum priority.
 *
 * \param [in] new_prio		New scheduling priority
 *
 * \return Priority
 *
 * \see sys_prio_get()
 * \see sys_prio_max()
 * \see sys_cpu_get()
 * \see sys_cpu_set()
 * \see sys_cpu_mask()
 * \see sys_thread_sched()
 */
uint32_t sys_prio_set(uint32_t new_prio);
uint32_t sys_prio_set_syscall(uint32_t new_prio);

/** Retrieve maximum priority of caller's partition
 *
 * This function returns the maximum scheduling priority of the caller's partition
 *
 * \return Priority
 *
 * \see sys_prio_get()
 * \see sys_prio_set()
 * \see sys_cpu_get()
 * \see sys_cpu_set()
 * \see sys_cpu_mask()
 * \see sys_thread_sched()
 */
uint32_t sys_prio_max(void);
uint32_t sys_prio_max_syscall(void);

/** Retrieve assigned CPU of calling thread
 *
 * This function returns the assigned CPU of the calling thread.
 *
 * \return CPU ID
 *
 * \see sys_cpu_set()
 * \see sys_cpu_mask()
 * \see sys_prio_get()
 * \see sys_prio_set()
 * \see sys_prio_max()
 * \see sys_thread_cpu_mask()
 */
uint32_t sys_cpu_get(void);
uint32_t sys_cpu_get_syscall(void);

/** Change assigned CPU of calling thread
 *
 * This function changes the assigned CPU of the calling thread.
 * The assigned CPU must be within the set of CPUs available to the partition.
 * After migration, the thread is enqueued
 * at the tail its priority's ready queue.
 *
 * \param [in] cpu_id		New assigned CPU
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given CPU ID is out of bounds
 *
 * \see sys_cpu_get()
 * \see sys_cpu_mask()
 * \see sys_prio_get()
 * \see sys_prio_set()
 * \see sys_prio_max()
 * \see sys_thread_cpu_mask()
 */
err_t sys_cpu_set(uint32_t cpu_id);

/** Retrieve bitmask of CPUs available to the caller's partition
 *
 * This function returns the bitmask of CPUs
 * available to the caller's partition.
 *
 * \return Bitmask of available CPU
 *
 * \see sys_cpu_get()
 * \see sys_cpu_set()
 * \see sys_prio_get()
 * \see sys_prio_set()
 * \see sys_prio_max()
 * \see sys_thread_cpu_mask()
 */
ulong_t sys_cpu_mask(void);

/** Change scheduling parameters of thread
 *
 * The function both retrieves the current scheduling parameters
 * and sets new the scheduling parameters for thread "thr_id".
 * If "old_sched" is not NULL, the function provides the scheduling priority
 * and the scheduling quantum of the given thread.
 * If "new_sched" is not NULL, the function sets the scheduling priority
 * and the scheduling quantum to the given values.
 * The new priority is limited to the partition's maximum priority.
 * The per-thread scheduling quantum controls round-robin scheduling
 * with the given quantum in nanoseconds, with zero being an infinite quantum.
 * The flags parameter is currently unused and must be set to 0.
 *
 * \param [in] thr_id		Thread ID
 * \param [in] flags		Flags (must be 0)
 * \param [in] new_sched	New scheduling parameters to set
 * \param [out] old_sched	Current scheduling parameters to retrieve
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given thread ID is out of bounds
 * \retval ESTATE			If the thread is dead
 * \retval EINVAL			If a non-zero flags parameter is set
 * \retval EFAULT			Invalid address or page fault
 *
 * \note Changing a currently running thread's scheduling parameters
 * interferes with other operations that also affect scheduling parameters,
 * such as concurrent calls to sys_prio_set() or real-time resource protocols.
 *
 * \note The scheduler considers a changed scheduling quantum the next time
 * it schedules the thread and replenishes the quantum.
 *
 * \see sys_prio_get()
 * \see sys_prio_set()
 * \see sys_prio_max()
 * \see sys_thread_cpu_mask()
 */
err_t sys_thread_sched(
	uint32_t thr_id,
	uint32_t flags,
	const struct sys_sched_param *new_sched,
	struct sys_sched_param *old_sched);

/** Change scheduling parameters of thread
 *
 * The function both retrieves the current CPU affinity mask
 * and sets a new affinity mask for thread "thr_id".
 * If "old_cpu_mask" is not NULL, the previous CPU affinity mask returned here.
 * If "new_cpu_mask" is not zero, the function sets a the CPU affinity mask.
 * The flags parameter is currently unused and must be set to 0.
 * The new CPU affinity mask is restricted to the subset of available CPUs.
 * If the thread's currently assigned CPU is no longer included
 * in the new affinity mask, the thread is migrated
 * to the lowest available CPU in the new affinity mask.
 *
 * \param [in] thr_id		Thread ID
 * \param [in] flags		Flags (must be 0)
 * \param [in] new_cpu_mask	New CPU affinity mask to set, if not zero
 * \param [out] old_cpu_mask	Current CPU affinity mask to retrieve
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given thread ID is out of bounds
 * \retval ESTATE			If the thread is dead
 * \retval EINVAL			If a non-zero flags parameter is set
 * \retval EINVAL			If the intersection of the affinity mask and the partition's CPU mask is empty
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_cpu_get()
 * \see sys_cpu_set()
 * \see sys_cpu_mask()
 * \see sys_thread_sched()
 */
err_t sys_thread_cpu_mask(
	uint32_t thr_id,
	uint32_t flags,
	ulong_t new_cpu_mask,
	ulong_t *old_cpu_mask);

/** Preempt calling thread
 *
 * This function preempts the calling thread on its current scheduling priority.
 * Other threads with the higher scheduling priority become eligible
 * for scheduling.
 */
void sys_preempt(void);

/** Yield calling thread
 *
 * This function yields the calling thread on its current scheduling priority.
 * Other threads with the same scheduling priority become eligible
 * for scheduling.
 */
void sys_yield(void);

/** Retrieve current system time
 *
 * This function returns the current time of clock "clk_id" in "time".
 * The returned value represents the time in nanoseconds
 * since the defined origin of the clock.
 *
 * \param [in] clk_id		Clock ID (TIMEOUT_ABSOLUTE flag not supported)
 * \param [out] time		System time in nanoseconds of the given clock
 *
 * \retval EOK				Success
 * \retval EINVAL			Invalid clock ID
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_clock_set()
 * \see sys_clock_res()
 * \see sys_sleep()
 */
err_t sys_clock_get(uint32_t clk_id, sys_time_t *time);

/** Set current time of real-time clock
 *
 * This function sets the current time of clock "clk_id".
 * Only the real-time clock supports setting a new time.
 *
 * This operation is privileged and requires the PART_PERM_SET_TIME privilege.
 *
 * \param [in] clk_id		Clock ID (TIMEOUT_ABSOLUTE flag not supported)
 * \param [in] time			System time in nanoseconds of the given clock
 *
 * \retval EOK				Success
 * \retval EINVAL			Invalid clock ID
 * \retval EPERM			Calling partition is not privileged
 *
 * \see sys_clock_get()
 * \see sys_clock_res()
 * \see sys_sleep()
 */
err_t sys_clock_set(uint32_t clk_id, sys_time_t time);

/** Retrieve resolution of clock
 *
 * This function returns the resolution of clock "clk_id" in nanoseconds.
 * The resolution is the same for both the monotonic and the real-time clock,
 * as the kernel implements both clocks using the same system timer.
 *
 * \param [in] clk_id		Clock ID (TIMEOUT_ABSOLUTE flag not supported)
 * \param [out] res			Resolution of the clock in nanoseconds
 *
 * \retval EOK				Success
 * \retval EINVAL			Invalid clock ID
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_clock_get()
 * \see sys_clock_set()
 * \see sys_sleep()
 */
err_t sys_clock_res(uint32_t clk_id, sys_time_t *res);

/** Sleep until timeout expires
 *
 * This function blocks the calling thread in the scheduler
 * until the given timeout expires.
 *
 * There is no special handling for TIMEOUT_NULL.
 * If the timeout is TIMEOUT_NULL or in the past, the thread at least yields.
 *
 * \param [in] timeout		Timeout
 * \param [in] clk_id_absflag	Clock ID OR-ed with optional TIMEOUT_ABSOLUTE flag
 *
 * \retval EOK				Success (given timeout expired)
 * \retval EINVAL			Invalid clock ID
 *
 * \see sys_clock_get()
 * \see sys_clock_set()
 * \see sys_clock_res()
 */
err_t sys_sleep(sys_time_t timeout, uint32_t clk_id_absflag);

/** Suspend calling thread
 *
 * Suspend the calling thread if there is no pending resume request.
 * In case of a pending resume request, consume the resume request
 * and return immediately.
 *
 * \retval EOK				Success (thread woken up from suspended state)
 * \retval EAGAIN			A suspension wakeup request was pending
 *
 * \see sys_thread_resume()
 */
err_t sys_thread_suspend(void);

/** Resume suspended thread
 *
 * Resume a suspended thread, or set a pending resume request if the thread
 * is not suspended.
 *
 * \retval EOK				Success
 * \retval ESTATE			If the thread is dead
 * \retval ELIMIT			If the given thread ID is out of bounds
 *
 * \see sys_thread_suspend()
 */
err_t sys_thread_resume(uint32_t thr_id);

/** Retrieve partition name of calling thread
 *
 * This function returns the partition name of the calling thread. The caller
 * must specify a string buffer which is large enough to hold the name.
 *
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_part_self()
 * \see sys_process_name()
 */
err_t sys_part_name(char *name, size_t size);

/** Retrieve partition ID of calling thread
 *
 * This function returns the partition ID of the calling thread.
 *
 * \return Partition ID
 *
 * \see sys_thread_self()
 * \see sys_part_name()
 * \see sys_process_self()
 */
uint32_t sys_part_self(void);

/** Retrieve caller's partition state
 *
 * A call to this function returns the status of the caller's partition.
 *
 * \return Partition state (0: IDLE, 1: RUNNING)
 */
uint32_t sys_part_state(void);

/** Shutdown caller's partition
 *
 * A call to this function shuts down the caller's partition.
 * The partition transitions to IDLE state.
 *
 * \note A call to this function does not return.
 */
void sys_part_shutdown(void) __noreturn;

/** Restart caller's partition
 *
 * A call to this function restarts the caller's partition.
 * The partition first transitions to IDLE state, then to RUNNING state again.
 *
 * \note A call to this function does not return.
 */
void sys_part_restart(void) __noreturn;

/** Retrieve partition state (privileged)
 *
 * A call to this function returns the status of the given partition.
 *
 * This operation is privileged and requires the PART_PERM_PART_OTHER privilege.
 *
 * \param [in] part_id		Partition ID
 * \param [out] state		Partition state (0: IDLE, 1: RUNNING)
 *
 * \retval EOK				Success
 * \retval EPERM			Calling partition is not privileged
 * \retval ELIMIT			If the given partition ID is out of bounds
 * \retval ELIMIT			If the partition ID references the idle partition
 * \retval EFAULT			Invalid address or page fault
 */
err_t sys_part_state_other(uint32_t part_id, uint32_t *state);

/** Shutdown a partition (privileged)
 *
 * A call to this function shuts down the given partition.
 * The partition transitions to IDLE state (if not already in this state).
 *
 * This operation is privileged and requires the PART_PERM_PART_OTHER privilege.
 *
 * \param [in] part_id		Partition ID
 *
 * \retval EOK				Success
 * \retval EPERM			Calling partition is not privileged
 * \retval ELIMIT			If the given partition ID is out of bounds
 * \retval ELIMIT			If the partition ID references the idle partition
 *
 * \note A call to this function does not return
 * if the caller's partition is affected.
 */
err_t sys_part_shutdown_other(uint32_t part_id);

/** Restart a partition (privileged)
 *
 * A call to this function restarts the given partition.
 * The partition first transitions to IDLE state (if not already in this state),
 * then to RUNNING state again.
 *
 * This operation is privileged and requires the PART_PERM_PART_OTHER privilege.
 *
 * \param [in] part_id		Partition ID
 *
 * \retval EOK				Success
 * \retval EPERM			Calling partition is not privileged
 * \retval ELIMIT			If the given partition ID is out of bounds
 * \retval ELIMIT			If the partition ID references the idle partition
 *
 * \note A call to this function does not return
 * if the caller's partition is affected.
 */
err_t sys_part_restart_other(uint32_t part_id);

/** Reset or shutdown the system (privileged)
 *
 * Calling this function resets or shuts down the system, depending on mode:
 * - If mode is 0, the system is reset.
 * - If mode is 1, the system is halted.
 * - If mode is 2, the system is powered off.
 *
 * This operation is privileged and requires the PART_PERM_SHUTDOWN privilege.
 *
 * \param [in] mode			Halt mode
 *
 * \retval EPERM			Calling partition is not privileged
 * \retval EINVAL			Invalid mode requested
 *
 * \note A call to this function does not return on success.
 */
err_t sys_bsp_shutdown(uint32_t mode);

/** Get BSP name
 *
 * This function returns the name of the BSP. The caller must specify
 * a string buffer which is large enough to hold the name.
 *
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 */
err_t sys_bsp_name(char *name, size_t size);

/** Iterate partition's interrupt IDs
 *
 * This function iterates the available interrupts of a partition,
 * starting at "index" 0, and returns the IRQ ID in "irq_id".
 *
 * \param [in] index		Index of assigned IRQ
 * \param [out] irq_id		IRQ ID
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given index is out of bounds
 * \retval EINVAL			If "irq_id" is NULL
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_irq_iter_name()
 * \see sys_irq_attach()
 */
err_t sys_irq_iter_id(uint32_t index, uint32_t *irq_id);

/** Iterate partition's interrupt names
 *
 * This function iterates the available interrupts of a partition,
 * starting at "index" 0, and returns the assigned name of the interrupt
 * in a string "name" of length "size".
 *
 * \param [in] index		Index of assigned IRQ
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given index is out of bounds
 * \retval EINVAL			If "name" is NULL
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_irq_iter_id()
 * \see sys_irq_attach()
 */
err_t sys_irq_iter_name(uint32_t index, char *name, size_t size);

/** Attach file descriptor to an interrupt
 *
 * Calling this function attaches a file descriptor returned in "fd"
 * to the interrupt "irq_id". The function first checks if the interrupt
 * is available to the calling partition and then configures the interrupt
 * in the hardware's interrupt controller with the given mode in "mode":
 * - IRQ_MODE_DEFAULT       Use IRQ default settings
 * - IRQ_MODE_EDGE_DEFAULT  Edge-triggered IRQ using a default edge setting
 * - IRQ_MODE_EDGE_FALLING  Edge-triggered IRQ on falling edge
 * - IRQ_MODE_EDGE_RISING   Edge-triggered IRQ on rising edge
 * - IRQ_MODE_EDGE_BOTH     Edge-triggered IRQ on both raising and falling edges
 * - IRQ_MODE_LEVEL_DEFAULT Level-triggered IRQ using a default level setting
 * - IRQ_MODE_LEVEL_LOW     Level-triggered IRQ on low level
 * - IRQ_MODE_LEVEL_HIGH    Level-triggered IRQ on high level
 * When unsure about the interrupt mode, use IRQ_MODE_DEFAULT.
 *
 * In "flags", the caller can specify the behavior of the file descriptor such
 * as non-blocking mode (SYS_FD_NONBLOCK) or close-on-exec (SYS_FD_CLOEXEC).
 *
 * Interrupt fds are normal file descriptors in the kernel API.
 * The caller must use an IPC-based read operation to receive interrupts.
 * The read operation returns the number of received interrupts (typically one)
 * as a 64-bit unsigned integer, and EPOLLIN indicates pending interrupts.
 *
 * If the file descriptor is opened in blocking mode (SYS_FD_NONBLOCK not set),
 * the interrupt is enabled (unmasked) automatically on a read operation,
 * and disabled (masked) automatically when an interrupt is received
 * and the read operation returns successfully.
 * Only one thread can wait in a read operation at a time.
 *
 * If the file descriptor is opened in non-blocking mode (SYS_FD_NONBLOCK set),
 * the caller must actively unmask the interrupt in sys_irq_ctl(IRQ_CTL_UNMASK)
 * before interrupts are received by the kernel in the background.
 * When interrupts are received, the kernel sets EPOLLIN on the file descriptor,
 * and a read operation returns successfully without blocking. Alternatively,
 * calling sys_irq_ctl(IRQ_CTL_CLEAR) clears the interrupt counter as well.
 * The caller can disable (mask) the interrupt with sys_irq_ctl(IRQ_CTL_MASK).
 *
 * Interrupt file descriptor are closed by calling sys_fd_close(). On closing,
 * any threads waiting for interrupts are woken up with error code ECANCEL
 * and the interrupt is disabled (masked) in the interrupt controller.
 * Interrupt file descriptor can be shared in IPC with other processes.
 *
 * \param [in] irq_id		IRQ ID
 * \param [in] mode			Interrupt mode
 * \param [in] flags		Flags, e.g. SYS_FD_NONBLOCK or SYS_FD_CLOEXEC
 * \param [out] fd			File descriptor
 *
 * \retval EOK				Success
 * \retval EINVAL			If "fd" is NULL
 * \retval EINVAL			If "mode" is invalid
 * \retval ELIMIT			If the given IRQ ID is not available to the partition
 * \retval ESTATE			If the interrupt is already enabled
 * \retval EBUSY			If the interrupt is not available on the platform
 * \retval ETYPE			If the requested interrupt mode is not supported
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval ENFILE			If the partition is out of free file descriptors
 * \retval EFAULT			Invalid address or page fault
 *
 * \note An interrupt fd can be used with epoll in non-blocking mode:
 * The EPOLLIN event indicates that an interrupt was received
 * and that the file descriptor can be read without blocking.
 *
 * \see sys_irq_iter_id()
 * \see sys_irq_iter_name()
 * \see sys_irq_ctl()
 * \see sys_fd_close()
 */
err_t sys_irq_attach(uint32_t irq_id, unsigned int mode, uint32_t flags, uint32_t *fd);

/** Control interrupt handling
 *
 * With this function, the calling thread can control interrupt reception
 * from the interrupt source associated with the interrupt file descriptor "fd".
 *
 * The operations are:
 * - IRQ_CTL_UNMASK: unmask (enable) IRQ
 * - IRQ_CTL_MASK:   mask (disable) IRQ
 * - IRQ_CTL_CLEAR:  clear pending interrupt count
 *
 * The flags parameter is currently unused and must be set to 0.
 *
 * To successfully mask or unmask the IRQ, or clear the pending interrupt count,
 * the interrupt file descriptor must be in non-blocked mode.
 * Clearing the interrupt count also clears the EPOLLIN event
 * if the file descriptor is monitored in epoll.
 *
 * \param [in] fd			Interrupt file descriptor
 * \param [in] op			Operation
 * \param [in] flags		Flags (must be 0)
 *
 * \retval EOK				Success
 * \retval EINVAL			If a non-zero flags parameter is set
 * \retval EINVAL			If "op" is invalid
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not an interrupt file descriptor
 * \retval EAGAIN			If "fd" is not in non-blocking mode
 * \retval ESTATE			If the interrupt was detached
 *
 * \see sys_irq_attach()
 */
err_t sys_irq_ctl(uint32_t fd, uint32_t op, uint32_t flags);

/** Cache management
 *
 * Perform a cache management operation on the given virtual memory region
 * denoted by "start" and "size". The "alias" argument is only used for
 * instruction cache handling to handle potential aliases in the instruction
 * caches and may refer to an address in a currently not active address space.
 * If the cache aliases, it is recommended to invalidate the whole instruction
 * cache if "alias" is not equal to "start".
 *
 * \param [in] op			Cache operation
 * \param [in] start		Start of memory region
 * \param [in] size			Size of memory region
 * \param [in] alias		Alias address in another address space
 *
 * \retval EOK				Success
 * \retval EFAULT			Invalid address or page fault
 * \retval EINVAL			If "op" refers to a restricted operation
 */
err_t sys_cache(sys_cache_op_t op, ulong_t start, size_t size, addr_t alias);


/** Signal and exception handler type
 *
 * The kernel invokes a registered signal or exception handler of this type.
 * Besides the cause of the signal or exception provided in "info",
 * the handler receives the previously interrupted register contexts
 * of the current thread and a mask of previously blocked signals as arguments.
 * For signals, the register context reflects the state where the thread
 * was interrupted.
 * For exceptions, the register context refers to the state where the thread
 * caused the exception.
 * The handler may change the register context or the signal mask.
 *
 * While an signal or exception handler is executing,
 * the currently handled signal is added to the list of blocked signals.
 * If the kernel encounters an exception which is currently blocked,
 * the exception is escalated to a partition error.
 *
 * \param [in] regs			Previous register context
 * \param [in] blocked_mask	Mask of blocked signals
 * \param [in] info			Information on current signal or exception
 *
 * \note The handler must not return, but finish its execution by tail-calling
 * sys_sig_return() to restore previous registers and the mask of previously
 * blocked signals.
 *
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_stack()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 * \see sys_sig_pending()
 * \see sys_sig_wait()
 * \see sys_sig_suspend()
 */
typedef void (*sig_handler_t)(struct regs *regs, sys_sig_mask_t blocked_mask, struct sys_sig_info *info);

/** Signal and exception context restorer
 *
 * A signal or exception handler uses this call to restore the register context
 * of the calling thread with the register context in "regs"
 * and the thread's previous mask of blocked signals in "mask".
 *
 * \param [in] regs			Register context to restore
 * \param [in] blocked_mask	Mask of blocked signals to restore
 *
 * \note A call to this function does not return.
 *
 * \note On failures restoring the register context,
 * the exception is escalated to a partition error.
 *
 * \see sig_handler_t()
 * \see sys_sig_register()
 * \see sys_sig_stack()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 * \see sys_sig_pending()
 * \see sys_sig_wait()
 * \see sys_sig_suspend()
 */
void sys_sig_return(const struct regs *regs, sys_sig_mask_t blocked_mask) __noreturn;

/** Register signal or exception handler function
 *
 * This function registers the given handler function "handler" for a signal
 * or exception "sig".
 * The registered handler is used by all threads in the calling partition.
 * If "handler" is NULL, any previously registered handler is overwritten,
 * and, if the particular signal or exception is raised,
 * the signal or exception is directly escalated to a partition error.
 * While the registered handler is executing, additionally to the signal
 * or exception "sig", other signals in "blocked_mask" are blocked.
 * If SYS_SIG_STACK set in "flags", the signal handler is invoked on the
 * signal stack set up by sys_sig_stack() instead of the normal thread stack.
 * If no signal stack is provided, the normal stack is used.
 *
 * \param [in] sig			ID of signal or exception to register a handler for
 * \param [in] handler		Handler callback for signal or exception
 * \param [in] blocked_mask	Mask of blocked signals while handler is active
 * \param [in] flags		Flags, i.e. SYS_SIG_STACK
 *
 * \retval EOK				Success
 * \retval EINVAL			If "sig" refers to an invalid signal or exception
 * \retval EINVAL			If invalid "flags" are set
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_stack()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 * \see sys_sig_pending()
 * \see sys_sig_wait()
 * \see sys_sig_suspend()
 * \see sys_signalfd_create()
 * \see sys_signalfd_change()
 */
err_t sys_sig_register(uint32_t sig, sig_handler_t handler, sys_sig_mask_t blocked_mask, uint32_t flags);

/** Set signal stack
 *
 * This function sets the signal stack of the current thread.
 * The signal stack is used during signal handling
 * if the SYS_SIG_STACK flag is set in sys_sig_register().
 * If base is NULL and "size" is zero, the signal stack is disabled,
 * and signal handling uses the current stack.
 * The kernel detects recursive invocation of signal handlers
 * and switches to the signal stack only once.
 * Its the responsiblity of the application to provide sufficient stack space.
 *
 * \param [in] base			Base address of signal stack
 * \param [in] size			Size of signal stack
 *
 * \retval EINVAL			If "base" is NULL or "size" is zero, but not both
 * \retval EOK				Success
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 * \see sys_sig_pending()
 * \see sys_sig_wait()
 * \see sys_sig_suspend()
 */
err_t sys_sig_stack(void *base, size_t size);

/** Change set of blocked signals
 *
 * This function changes the set of blocked signals for the current thread:
 * The parameter "clear_mask" describes the set of signals to clear/allow,
 * while the parameter "set_mask" describes the set of signals to set/block.
 * The kernel first clears the thread's signal mask with the signals
 * from "clear_mask", then sets the signals in "set_mask".
 *
 * \param [in] clear_mask	Set of signals to clear (allow)
 * \param [in] set_mask		Set of signals to set (block)
 *
 * \return Previous signal mask before modification
 *
 * \note This function can be used in different ways:
 * - To retrieve the current set of blocked signals,
 *   use sys_sig_mask(SIG_MASK_NONE, SIG_MASK_NONE).
 * - To additional block a signal "sig",
 *   use sys_sig_mask(SIG_MASK_NONE, SIG_TO_MASK(sig)).
 * - To allow the signal "sig" again,
 *   use sys_sig_mask(SIG_TO_MASK(sig), SIG_MASK_NONE).
 * - To restore a previously saved signal mask "previous_mask",
 *   use sys_sig_mask(SIG_MASK_ALL, previous_mask).
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_stack()
 * \see sys_sig_send()
 * \see sys_sig_pending()
 * \see sys_sig_wait()
 * \see sys_sig_suspend()
 * \see sys_signalfd_create()
 * \see sys_signalfd_change()
 */
sys_sig_mask_t sys_sig_mask(sys_sig_mask_t clear_mask, sys_sig_mask_t set_mask);

/** Send signal to one or all threads
 *
 * This function sends a signal "sig", either to a specific thread "thr_id",
 * or to the caller's partition if "thr_id" is THR_ID_ALL.
 *
 * Depending whether optional information on the signal is given in "info",
 * the signal is a queued signal where this information is queued,
 * or the signal is an unqueued signal which is simply set pending.
 * If "info" is not NULL, the signal is a queued signal.
 * It is queued to the thread or to the partition
 * if the limit of queued signals has not been reached,
 * and the custom signal information in "info" is passed to the signal handler.
 * If "info" is NULL, the signal is an unqueued signal.
 * It is set pending to the thread or the partition,
 * and signal information passed to the signal handler contains default values.
 *
 * The signal is recorded and marked as pending for the specific thread
 * or for all threads in the partition until one thread consumes the signal,
 * either by executing the signal handler or by waiting for the signal.
 * If an eligible target thread is waiting for the signal in sys_sig_wait()
 * or in sys_sig_suspend(), the thread is woken up to consume the signal.
 * Unlike POSIX, sending a signal does not unblock a thread
 * if the thread is waiting in other kernel system calls.
 *
 * \param [in] thr_id		Thread ID or THR_ID_ALL
 * \param [in] sig			ID of signal
 * \param [in] info			Optional information on current signal
 *
 * \retval EOK				Success
 * \retval EINVAL			If the given signal ID is invalid
 * \retval ELIMIT			If the given thread ID is out of bounds
 * \retval ESTATE			If the thread is dead
 * \retval EAGAIN			If the limit of queued signals is reached
 * \retval EFAULT			Invalid address or page fault accessing "info"
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_stack()
 * \see sys_sig_mask()
 * \see sys_sig_pending()
 * \see sys_sig_wait()
 * \see sys_sig_suspend()
 */
err_t sys_sig_send(uint32_t thr_id, uint32_t sig, struct sys_sig_info *info);

/** Retrieve the set of pending signals
 *
 * This function returns the set of pending and currently blocked signals
 * of the calling thread.
 *
 * \return Set of pending signals
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_stack()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 * \see sys_sig_wait()
 * \see sys_sig_suspend()
 */
sys_sig_mask_t sys_sig_pending(void);

/** Wait for specific signals and return signal information
 *
 * This function waits with the given timeout
 * for the signals in "wait_mask" to be signaled
 * and then returns with the information on the signal stored in "info"
 * instead of executing the signal handler.
 * If the timeout is TIMEOUT_NULL, the function returns immediately
 * when no signals are pending.
 *
 * The function temporarily includes the signals in "wait_mask"
 * to the set of blocked signals of the thread
 * and restores the original set of blocked signals after waiting.
 * Note that the thread can still be interrupted by a non-blocked signal
 * that is not included in "wait_mask". In this case,
 * the function returns EINTR, and the signal handler are invoked.
 *
 * The flags parameter is currently unused and must be set to 0.
 *
 * \param [in] wait_mask	Set of signals to wait for
 * \param [in] flags		Flags (must be 0)
 * \param [in] timeout		Timeout or TIMEOUT_NULL for polling
 * \param [in] clk_id_absflag	Clock ID OR-ed with optional TIMEOUT_ABSOLUTE flag
 * \param [out] info		Information on current signal or exception
 *
 * \retval EOK				Success
 * \retval EINVAL			If "info" is a NULL pointer
 * \retval EINVAL			Invalid flags
 * \retval EINVAL			Invalid clock ID
 * \retval EFAULT			Invalid address or page fault accessing futex
 * \retval EAGAIN			If timeout is TIMEOUT_NULL and no signal is pending
 * \retval EAGAIN			If the timeout expired before a signal was received
 * \retval EINTR			Interrupted by signal handler
 *
 * \note This function returns EAGAIN instead of ETIMEDOUT, like in POSIX.
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_stack()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 * \see sys_sig_pending()
 * \see sys_sig_suspend()
 * \see sys_signalfd_create()
 * \see sys_signalfd_change()
 */
err_t sys_sig_wait(sys_sig_mask_t wait_mask, uint32_t flags, sys_timeout_t timeout, uint32_t clk_id_absflag, struct sys_sig_info *info);

/** Wait for specific signals and execute signal handler
 *
 * This function waits for the signals in "wait_mask" to be signaled,
 * and then executes the signal handler and returns.
 *
 * The function temporarily replaces the set of blocked signals
 * of the calling thread with the complement of "wait_mask" while waiting
 * and restores the original set of blocked signals after waiting.
 *
 * \param [in] wait_mask	Set of signals to wait for
 *
 * \note Unlike sigsuspend() in POSIX, which specifies a set of blocked signals,
 * this function uses a set of allowed signals to wait for, like in sigwait().
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_stack()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 * \see sys_sig_pending()
 * \see sys_sig_wait()
 */
void sys_sig_suspend(sys_sig_mask_t wait_mask);


/** Block current thread on user space locking object
 *
 * This function lets the calling thread wait on the user space locking object
 * "futex" with the given timeout. Before waiting, the kernel compares
 * "futex" AND-ed with "mask" to the expected "compare" value AND-ed with "mask"
 * and prevents the thread from waiting if these values differ.
 *
 * Multiple threads waiting on the same user space locking object are queued
 * in the order of their scheduling priority. If multiple threads have the same
 * priority, they get ordered by the time of their arrival in the wait queue.
 *
 * There is no special handling for TIMEOUT_NULL.
 * If the timeout is TIMEOUT_NULL or in the past, the thread at least yields.
 *
 * \param [in] futex		User space lock object
 * \param [in] compare		Compare value
 * \param [in] mask			Mask value
 * \param [in] flags		Flags (must be 0)
 * \param [in] timeout		Timeout
 * \param [in] clk_id_absflag	Clock ID OR-ed with optional TIMEOUT_ABSOLUTE flag
 *
 * \retval EOK				Success
 * \retval EINVAL			If the futex address refers to a NULL pointer
 * \retval EINVAL			If the futex address is not aligned to 4 bytes
 * \retval EINVAL			Invalid flags
 * \retval EINVAL			Invalid clock ID
 * \retval EFAULT			Invalid address or page fault accessing futex
 * \retval EAGAIN			If the futex does not have the expected value
 * \retval ETIMEDOUT		If the timeout expired before the thread
 *							was woken up or requeued
 *
 * \see sys_futex_wake()
 * \see sys_futex_lock()
 * \see sys_futex_unlock()
 * \see sys_futex_requeue()
 */
err_t sys_futex_wait(
	const unsigned int *futex,
	uint32_t compare,
	uint32_t mask,
	uint32_t flags,
	sys_timeout_t timeout,
	uint32_t clk_id_absflag);

/** Wake threads waiting on user space locking object
 *
 * This function wakes up up to "count" threads
 * currently waiting on the user space locking object "futex".
 * The call succeeds even if no threads are woken up.
 *
 * Waiting threads with the highest scheduling priority are woken up first.
 * If multiple threads have the same scheduling priority,
 * they are woken up in order of the time of their arrival in the wait queue.
 *
 * \param [in] futex		User space lock object
 * \param [in] count		Number of threads to wake up
 *
 * \retval EOK				Success
 * \retval EINVAL			If the futex address refers to a NULL pointer
 * \retval EINVAL			If the futex address is not aligned to 4 bytes
 * \retval EFAULT			Invalid address or page fault accessing futex
 *
 * \note If no further threads are waiting on the user space locking object
 * after wakeup, the kernel frees the internal associated wait queue.
 *
 * \see sys_futex_wait()
 * \see sys_futex_lock()
 * \see sys_futex_unlock()
 * \see sys_futex_requeue()
 */
err_t sys_futex_wake(
	const unsigned int *futex,
	uint32_t count);


/** Lock a futex-based mutex for the current thread
 *
 * This function locks the user space mutex "futex" for the calling thread.
 * If the mutex is not available, the calling thread waits with the given
 * timeout. If the timeout is TIMEOUT_NULL, the function returns immediately.
 *
 * Multiple threads blocked on the same user space mutex are queued
 * in the order of their scheduling priority. If multiple threads have the same
 * priority, they get ordered by the time of their arrival in the wait queue.
 *
 * The kernel excepts the following futex protocol in user space:
 * - unlocked mutex: value zero
 * - locked mutex: the lock holder's thread ID OR-ed with SYS_FUTEX_LOCKED bit
 * - contention: the SYS_FUTEX_WAITERS bit is also set
 *
 * \param [in,out] futex	User space lock object
 * \param [in] flags		Flags (must be 0)
 * \param [in] timeout		Timeout or TIMEOUT_NULL for polling
 * \param [in] clk_id_absflag	Clock ID OR-ed with optional TIMEOUT_ABSOLUTE flag
 *
 * \retval EOK				Success
 * \retval EINVAL			Invalid flags
 * \retval EINVAL			If the futex address refers to a NULL pointer
 * \retval EINVAL			If the futex address is not aligned to 4 bytes
 * \retval EINVAL			Invalid clock ID
 * \retval EFAULT			Invalid address or page fault accessing futex
 * \retval EAGAIN			If timeout is TIMEOUT_NULL and locking failed
 * \retval ETIMEDOUT		If the timeout expired before the thread
 *							was woken up or requeued
 *
 * \see sys_futex_wait()
 * \see sys_futex_wake()
 * \see sys_futex_unlock()
 * \see sys_futex_requeue()
 */
err_t sys_futex_lock(
	unsigned int *futex,
	uint32_t flags,
	sys_timeout_t timeout,
	uint32_t clk_id_absflag);

/** Unlock a futex-based mutex
 *
 * This function unlocks the user space mutex "futex"
 * and wakes up a blocked thread, if any.
 * The call succeeds even if no threads are woken up.
 *
 * Blocked threads with the highest scheduling priority are woken up first.
 * If multiple threads have the same scheduling priority,
 * they are woken up in order of the time of their arrival in the wait queue.
 *
 * If SYS_FUTEX_THREAD_EXIT is set in "flags", the calling thread terminates
 * after succesfully releasing the lock. Otherwise, set "flags" to zero.
 *
 * \param [in,out] futex	User space lock object
 * \param [in] flags		Flags, i.e. SYS_FUTEX_THREAD_EXIT
 *
 * \retval EOK				Success
 * \retval EINVAL			If the futex address refers to a NULL pointer
 * \retval EINVAL			If the futex address is not aligned to 4 bytes
 * \retval EINVAL			Invalid flags
 * \retval EFAULT			Invalid address or page fault accessing futex
 *
 * \note If no further threads are blocked on the user space mutex
 * after wakeup, the kernel frees the internal associated wait queue.
 *
 * \note A successful call to this function does not return
 * if the SYS_FUTEX_THREAD_EXIT flag is set.
 *
 * \see sys_futex_wait()
 * \see sys_futex_wake()
 * \see sys_futex_lock()
 * \see sys_futex_requeue()
 */
err_t sys_futex_unlock(
	unsigned int *futex,
	uint32_t flags);

/** Wake and/or requeue threads waiting on user space locking object
 *
 * This function first checks
 * if the value of "futex" AND-ed with "mask"
 * still matches "compare" AND-ed with "mask.
 * Then it wakes up up to "count" threads
 * currently waiting on the user space locking object "futex"
 * After waking up the threads, the function additionally requeues (moves)
 * "count2" threads to a second user space locking object "futex2".
 * The call succeeds even if no threads are woken up or requeued.
 *
 * Waiting threads with the highest scheduling priority are woken up
 * or requeued first. On requeueing,
 * multiple threads waiting on the same user space locking object are queued
 * in the order of their scheduling priority. If multiple threads have the same
 * priority, they get ordered by the time of their arrival in the wait queue.
 * Also, requeued threads have their timeouts cleared.
 *
 * \param [in] futex		First user space lock object to wake/move threads from
 * \param [in] count		Number of threads to wake up
 * \param [in] compare		Compare value for first user space lock object
 * \param [in] mask			Mask value
 * \param [in,out] futex2	Second user space lock object to requeue threads to
 * \param [in] count2		Number of threads to requeue to futex2
 *
 * \retval EOK				Success
 * \retval EINVAL			If the first futex address refer to a NULL pointer
 * \retval EINVAL			If the second futex address refer to a NULL pointer,
 *							but "count2" is greater than zero
 * \retval EINVAL			If the futex addresses are not aligned to 4 bytes
 * \retval EINVAL			If "futex" and "futex2" refer to the same address
 * \retval EFAULT			Invalid address or page fault accessing futexes
 * \retval EAGAIN			If the first futex does not have the expected value
 * \retval EFAULT			Invalid address or page fault accessing futex
 *
 * \see sys_futex_wait()
 * \see sys_futex_wake()
 * \see sys_futex_lock()
 * \see sys_futex_unlock()
 */
err_t sys_futex_requeue(
	const unsigned int *futex,
	uint32_t compare,
	uint32_t mask,
	uint32_t count,
	unsigned int *futex2,
	uint32_t count2);

/** Retrieve current FPU state of calling thread
 *
 * This function returns the assigned CPU of the calling thread.
 *
 * \return FPU state (one of FPU_STATE_OFF, FPU_STATE_AUTO, or FPU_STATE_ON)
 *
 * \see sys_fpu_state_set()
 */
uint32_t sys_fpu_state_get(void);

/** Change FPU state of calling thread
 *
 * This function changes the FPU state of the calling thread.
 * The FPU state is one of the following value:
 * - FPU_STATE_OFF: 	disable the FPU
 * - FPU_STATE_AUTO:	automatically enable the FPU on demand
 * - FPU_STATE_ON:		enable the FPU
 * Any current FPU registers are preserved when changing the FPU state.
 * In FPU_STATE_OFF and FPU_STATE_AUTO state, the FPU is disabled
 * for the calling to shorten context switch times.
 *
 * \param [in] fpu_state	FPU state
 *
 * \retval EOK				Success
 * \retval EINVAL			If the FPU state is invalid
 *
 * \see sys_fpu_state_get()
 */
err_t sys_fpu_state_set(uint32_t fpu_state);

/** Change the TLS (thread local storage) for the current thread
 *
 * This function sets the TLS of the current thread to "tls".
 * It is intended to be called by the first thread of a partition to setup TLS.
 * Mandatory fields in the thread's TLS state are initialized by the kernel.
 *
 * \param [in] tls			Pointer to TLS
 *
 * \retval EOK				Success
 * \retval EINVAL			If the TLS pointer is NULL or not properly aligned
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_thread_create()
 */
err_t sys_tls_set(
	void *tls);


/** Iterate partition's memory requirements
 *
 * This function returns the specific name of a memory requirement.
 * "memrq_type" refers to the specific type of memory requirements,
 * i.e. MEMRQ_TYPE_MEM for <memrq>, MEMRQ_TYPE_IO for <map_io>,
 * MEMRQ_TYPE_SHM for <map_shm>, and MEMRQ_TYPE_FILE for <file_access>.
 * For each of the different types, "memrq_id" then refers to a specific
 * memory requirement with an index starting from zero.
 * The memory requirements follow the order specified in the XML configuration.
 *
 * If a memory requirement identified by type and ID exists,
 * the kernel returns the name of the memory requirement in "name". The caller
 * must specify a string buffer which is large enough to hold the name.
 *
 * \param [in] memrq_type	Memory requirement type
 * \param [in] memrq_id		Memory requirement ID
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval EINVAL			If the given memory type is invalid
 * \retval ELIMIT			If the memory requirement ID is out of bounds
 * \retval EINVAL			If "name" is NULL
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_memrq_state()
 * \see sys_vm_map()
 */
err_t sys_memrq_name(
	uint32_t memrq_type,
	uint32_t memrq_id,
	char *name,
	size_t size);

/** Query the state of a memory requirement
 *
 * This function returns the configured data of a memory requirement.
 * "memrq_type" refers to the specific type of memory requirements,
 * i.e. MEMRQ_TYPE_MEM for <memrq>, MEMRQ_TYPE_IO for <map_io>,
 * MEMRQ_TYPE_SHM for <map_shm>, and MEMRQ_TYPE_FILE for <file_access>.
 * For each of the different types, "memrq_id" then refers to a specific
 * memory requirement with an index starting from zero.
 * The memory requirements follow the order specified in the XML configuration.
 *
 * If a memory requirement identified by type and ID exists,
 * the kernel returns the current state of the memory requirement in "state".
 *
 * \param [in] memrq_type	Memory requirement type
 * \param [in] memrq_id		Memory requirement ID
 * \param [out] state		Memory requirement state
 *
 * \retval EOK				Success
 * \retval EINVAL			If the given memory type is invalid
 * \retval ELIMIT			If the memory requirement ID is out of bounds
 * \retval EINVAL			If "state" is NULL
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_memrq_name()
 * \see sys_vm_map()
 * \see sys_vm_v2p()
 */
err_t sys_memrq_state(
	uint32_t memrq_type,
	uint32_t memrq_id,
	struct sys_memrq_state *state);

/** Map a memory requirement into the virtual address space
 *
 * This function maps a memory requirement or a mapping guard
 * into the caller's adress space.
 *
 * One of the following mapping types must be set in "flags":
 * - MAP_SHARED: shared mapping
 * - MAP_PRIVATE:   private mapping
 * - MAP_GUARD:  mapping guard
 * The kernel does not distinguish between shared and private mappings,
 * as all mappings are shared by default.
 *
 * If MAP_ANON is set in "flags", the kernel allocates and maps
 * free (anynomous) pages from the memory requirement
 * speficied by "anon_mmap_memrq" in the configuration.
 * Otherwise, the kernel maps memory from the memory requirement given by "fd".
 *
 * The placement of the mapping is controlled by the following rules:
 * - addr == NULL: let the kernel pick a suitable address
 * - addr != NULL: take the given address as a hint
 * - addr != NULL and MAP_FIXED set in flags:
 *                 use given address and overmap any previous mappings
 * - addr != NULL and both MAP_FIXED and MAP_EXCL set in flags:
 *                 use given address and fail if there is already a mapping
 *
 * The following access permissions can be configured for the mapping:
 * - PROT_NONE: no access permission
 * - PROT_READ: read access permission
 * - PROT_WRITE: write access permission
 * - PROT_EXEC: executable access permission
 *
 * The "flags" parameter further encodes the remapping behavior
 * to change the current access permissions in later calls to sys_vm_protect():
 * - MAP_ALLOW_READ:  memory may be remapped with read access permission
 * - MAP_ALLOW_WRITE: memory may be remapped with write access permission
 * - MAP_ALLOW_EXEC:  memory may be remapped with executable access permission
 *
 * \note The parameter "fd" refers to a memory requirement file descriptor
 * opened by sys_memrq_open(). On success, the kernel maps
 * a region of the memory requirement described by "offset" and "size"
 * into the caller's virtual address space at the given address "addr"
 * with the access permissions set to "prot"
 * and cache attributes as configured for the memory requirement.
 *
 * The region described by "offset" and "size" must lie within the bounds
 * of the memory requirement.
 * Also, "addr" and "size" must be a valid region in the virtual address space.
 * The mapping permissions "prot" and the remapping permissions in "flags"
 * must be a subset of the configured permissions for the memory requirement
 * and the open mode (O_RDONLY, O_WRONLY, O_RDWR) of the file descriptor "fd".
 *
 * For a mapping guard, the access permissions are ignored.
 *
 * \param [in] addr			Hint to start address of the mapping
 * \param [in] size			Size of the mapping
 * \param [in] prot			Access permission of the mapping
 * \param [in] flags		Flags
 * \param [in] fd			Memory requirement file descriptor
 * \param [in] offset		Offset of the region within the memory requirement
 * \param [out] map_start	Resulting start address of the mapping
 *
 * \retval EOK				Success
 * \retval EINVAL			If the given memory type is invalid
 * \retval ELIMIT			If the memory requirement ID is out of bounds
 * \retval EINVAL			If "prot" is invalid
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "offset", "size", or "addr" are not page aligned
 * \retval EINVAL			If "addr" and "size" is not a valid user space region
 * \retval ESTATE			If "offset" and "size" exceed the memory requirement
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not an interrupt file descriptor
 * \retval EPERM			If "prot" is not a subset of the defined permissions
 * \retval ENOMEM			If the partition is out of memory for VMAs
 * \retval ENOMEM			If the partition is out of memory for page tables
 * \retval EEXIST			If both MAP_FIXED and MAP_EXCL are set,
 *							but a mapping already exists in the memory region
 * \retval EFAULT			Invalid address or page fault
 *
 * \note The function uses a low-level system call binding with a different
 *       order of arguments. "map_start" is placed before "fd" and "offset"
 *       due to ABI requirements on 32-bit systems.
 *
 * \see sys_vm_unmap()
 * \see sys_vm_protect()
 * \see sys_vm_v2p()
 * \see sys_vm_hint_addr()
 * \see sys_memrq_open()
 */
err_t sys_vm_map(
	addr_t addr,
	size_t size,
	uint32_t prot,
	uint32_t flags,
	uint32_t fd,
	phys_addr_t offset,
	addr_t *map_start);


/** Unmap a memory region in the virtual address space
 *
 * This function unmaps any mappings in the memory region
 * described by "addr" and "size" from the caller's adress space,
 * i.e. the memory region becomes inaccessible.
 * Note that the implementation gracefully skips any unmapped parts
 * within the memory region.
 *
 * \param [in] addr			Start address of the memory region to unmap
 * \param [in] size			Size of the memory region to unmap
 *
 * \retval EOK				Success
 * \retval EINVAL			If "addr" and "size" are not page aligned
 * \retval EINVAL			If "addr" and "size" is not a valid user space region
 * \retval ENOMEM			If the partition is out of memory for VMAs
 *
 * \note If the memory region or parts of it refer to a memory requirement
 * of type MEMRQ_TYPE_MEM, the memory of the memory requirement will be freed
 * if it is not used/mapped elsewhere.
 *
 * \note Unlike POSIX, this function can fail with the error ENOMEM
 * when trying to unmap a part "in the middle" of an existing mapping,
 * i.e. a part that is neither at the beginning nor at the end of the mapping.
 * In this case, the unmap operation splits the logical mapping
 * into two mappings, and it needs resources to track the extra mapping.
 * The operation fails if it would exceed
 * the maximum number of logical mappings for the caller's address space.
 * Note that this error condition can only happen for the first mapping
 * that this call encounters. Effectively, when ENOMEM is returned,
 * no memory was unmapped.
 * Note that an unmap operation that covers mappings in full never fails.
 *
 * \note If an unmap operation covers a memory region that is larger than
 * a leaf page table, the kernel will also free the leaf page table.
 * Likewise, higher levels in the page tables that are fully covered
 * will also be freed.
 *
 * \see sys_vm_map()
 * \see sys_vm_protect()
 * \see sys_vm_v2p()
 */
err_t sys_vm_unmap(
	addr_t addr,
	size_t size);

/** Change access permission to memory resources in the virtual address space
 *
 * This function changes the access permission of all mappings
 * in the memory region described by "addr" and "size" in the caller's
 * adress space to "prot" (PROT_NONE, PROT_READ, PROT_WRITE, PROT_EXEC).
 *
 * "addr" and "size" must be a valid region in the virtual address space
 * that is fully mapped, but it can comprise different mappings.
 *
 * The new access permissions in "prot" must be compatible
 * with the access permissions to the underlying memory storage.
 *
 * \param [in] addr			Start address of the memory region to change
 * \param [in] size			Size of the memory region to change
 * \param [in] prot			New access permission of the mappings
 *
 * \retval EOK				Success
 * \retval EINVAL			If "addr" and "size" are not page aligned
 * \retval EINVAL			If "addr" and "size" is not a valid user space region
 * \retval EINVAL			If "prot" is invalid
 * \retval EACCES			If "prot" is not allowed for the underlying memory
 * \retval ENOMEM			If the partition is out of memory for VMAs
 *
 * \note Changing the memory protection to PROT_NONE does not free
 * any underlying memory resources. The mapping to the memory
 * just becomes inaccessible until the access permissions are changed again.
 *
 * \note This function can fail with the error ENOMEM
 * when trying to change the access permissions of parts of an existing mapping.
 * In this case, the operation splits the logical mapping
 * into two or even three mappings with different access permissions.
 * The operation fails if it would exceed
 * the maximum number of logical mappings for the caller's address space.
 * Note that changing the access permission for a full mappings never fails.
 *
 * \note Both errors ENOMEM and EACCES can be raised for any mapping
 * in the memory region described by "addr" and "size.
 * In case of these errors, earlier parts of the mappings in the memory region
 * may have their access permissions changed, and later parts not.
 *
 * \see sys_vm_map()
 * \see sys_vm_unmap()
 * \see sys_vm_v2p()
 */
err_t sys_vm_protect(
	addr_t addr,
	size_t size,
	uint32_t prot);

/** Retrieve the physical address of a virtual address
 *
 * For an address "addr" in the virtual address space of the calling partition,
 * this function returns the underlying physical address
 * and the current access permissions (PROT_NONE, PROT_READ, PROT_WRITE, PROT_EXEC).
 * The intended use case is to query physical addresses for DMA operations.
 *
 * If a mapping exists, the physical address is returned in "phys"
 * and the current access permissions in "prot".
 * The same page offset in "addr" is also present in "phys".
 * If no mapping exists, the kernel returns the error (ESTATE).
 * In any case, the kernel provides a hint to the address of the next mapping
 * in the address space in "hint". The hint allows to quickly traverse
 * unmapped regions of the virtual address space.
 *
 * \param [in] addr			Address of a mapping to query
 * \param [out] phys		Physical address of mapping
 * \param [out] prot		Access permission
 * \param [out] next		Hint to start address of next mapping
 *
 * \retval EOK				Success
 * \retval EINVAL			If "addr" is not a valid user space address
 * \retval EINVAL			If "phys" or "prot" or "next" are NULL
 * \retval ESTATE			If "addr" refers to an unmapped address
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_memrq_state()
 * \see sys_vm_map()
 * \see sys_vm_unmap()
 * \see sys_vm_protect()
 */
err_t sys_vm_v2p(
	addr_t addr,
	phys_addr_t *phys,
	unsigned int *prot,
	addr_t *next);

/** Set start address in virtual address space for next mapping operation
 *
 * This function sets a start address for the next mapping operation.
 * The given address is used as a hint in case the caller passes a NULL address
 * to let the kernel pick a suitable address for the mapping.
 *
 * \param [in] addr			Hint to start address of the next mapping
 *
 * \retval EOK				Success
 * \retval EINVAL			If "addr" is not page aligned
 * \retval EINVAL			If "addr" is not a valid user space address
 *
 * \see sys_vm_map()
 */
err_t sys_vm_hint_addr(
	addr_t addr);


/** Iterate partition's kernel-level device driver
 *
 * This function iterates the available kernel-level device drivers (KLDD)
 * of a partition, starting at "kldd_id" 0, and returns the assigned name
 * of the kernel device in a string "name" of length "size".
 *
 * \param [in] kldd_id		Index of assigned kernel device
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given kernel device ID is out of bounds
 * \retval EINVAL			If "name" is NULL
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_kldd_call()
 */
err_t sys_kldd_name(uint32_t kldd_id, char *name, size_t size);

/** Call a kernel-level device driver
 *
 * This function calls a kernel-level device driver (KLDD).
 * A KLDD implements non-blocking low-level driver functionality
 * that can be only implemented in supervisor mode due to hardware constraints,
 * e.g. control of power domains or programming of GPIO pins.
 *
 * The kernel device is addressed by a partition-specific device ID.
 * If "kldd_id" is a valid device ID for the partition, the kernel invokes
 * a KLDD handler provided by the BSP with the remaining arguments.
 * The interpretation of the arguments depends on the actual KLDD.
 *
 * \param [in] kldd_id		Index of assigned kernel device
 * \param [in/out] arg1		First argument
 * \param [in/out] arg2		Second argument
 * \param [in/out] arg3		Third argument
 * \param [in/out] arg4		Fourth argument
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given device ID is out of bounds
 * \retval ENOSYS			Functionality not implemented
 * \retval EINVAL			Invalid argument
 * \retval EFAULT			Invalid address or page fault
 *
 * \note The kernel only raises ELIMIT. The KLDD may raise further errors.
 *
 * \see sys_kldd_name()
 */
err_t sys_kldd_call(uint32_t kldd_id, ulong_t arg1, ulong_t arg2, ulong_t arg3, ulong_t arg4);


/** Create a timer file descriptor and set notification mode
 *
 * This function creates a timer and sets the timer's clock ID
 * and notification mode. When a timer expires, the kernel
 * notifies the application either by a signal (signal mode)
 * or by waking up threads waiting for the timer to expire (waiting mode).
 * In signal mode, signals can be either queued to a specific thread,
 * or queued to all threads in the partition.
 * The timer is returned as a file descriptor in "fd".
 * Note that this funtion does not program's the timers expiry time.
 *
 * If "info" is not NULL, signal-based notification mode is used.
 * Then "info" provides information on the signal:
 * - "info.sig" defines the signal to be queued.
 * - "info.code" has a user defined value
 * - "info.ex_type" is set to the timer ID
 * - "info.status" is the overrun counter of the timer, see sys_timer_overrun()
 * - "info.addr" is a user defined value
 * When the timer expires, a signal will be queued
 * to the specific thread "thr_id"
 * or to the caller's partition if "thr_id" is THR_ID_ALL,
 * and the defined action of the signal will be executed.
 * If an eligible target thread is waiting for the signal in sys_sig_wait()
 * or in sys_sig_suspend(), the thread is woken up to consume the signal.
 * Unlike POSIX, sending a signal does not unblock a thread
 * if the thread is waiting in other kernel system calls.
 * When the timer exires again while a signal is still queued,
 * no new signal is queued, but the original signal remains pending.
 * Instead, the signal's overrun counter is incremented.
 * Note that the queueing of a signal for timer expiration never fails.
 *
 * If "info" is NULL, no signal will be queued, and "thr_id" is ignored.
 * The caller can use sys_timer_wait() to explicitly wait for timer expiration,
 * or use a blocking IPC-based "read" request to the file descriptor.
 * The read operation returns the number of expired timer events
 * as a 64-bit unsigned integer.
 *
 * In "flags", the caller can specify the behavior of the file descriptor such
 * as non-blocking mode (SYS_FD_NONBLOCK) or close-on-exec (SYS_FD_CLOEXEC).
 *
 * Next to the notification mechanism, the timer will be based on the clock
 * in "clk_id", i.e. the monotonic or the real-time clock of the kernel.
 * Note that enabling a timer also binds the timer to a specific CPU
 * where all future timer activities will happen.
 * This CPU is the current CPU of the given thread
 * in signal-based notification mode, or current CPU of the calling thread.
 *
 * After creation, neither the clock nor the notification mode of a timer
 * can be changed. This requires the creation of a new timer.
 *
 * \note Timer instances are normal file descriptors in the kernel API.
 * They can be closed by sys_fd_close(), but not be transferred outside
 * of the current process.
 *
 * \param [in] clk_id		Clock ID (TIMEOUT_ABSOLUTE flag not supported)
 * \param [in] flags		Flags, i.e. SYS_FD_NONBLOCK and SYS_FD_SPECIAL
 * \param [in] info			Optional information on current signal
 * \param [in] thr_id		Thread ID or THR_ID_ALL
 * \param [out] fd			File descriptor
 *
 * \retval EOK				Success
 * \retval EINVAL			If "clk_id" is not a valid clock ID
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "info.sig" refers to an invalid signal
 * \retval EINVAL			If "fd" is NULL
 * \retval ELIMIT			If info is not NULL and the given thread ID is out of bounds
 * \retval ELIMIT			If the maximum number of timers is reached
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval ENFILE			If the partition is out of free file descriptors
 * \retval EFAULT			Invalid address or page fault
 *
 * \note Creating a timer file descriptor consumes one of the timers assigned
 * to the partition.
 * The maximum number of timers per partition is a configuration option.
 *
 * \note A timer can be used with epoll. The EPOLLIN event indicates
 * that the timer expired and can be read without blocking.
 *
 * \note This function provides the functionality for POSIX timers
 * and Linux timerfd.
 *
 * \see sys_timer_set()
 * \see sys_timer_get()
 * \see sys_timer_overrun()
 * \see sys_timer_wait()
 * \see sys_fd_close()
 */
err_t sys_timer_create(
	uint32_t clk_id,
	uint32_t flags,
	const struct sys_sig_info *info,
	uint32_t thr_id,
	uint32_t *fd);

/** Set timer expiry and timer period
 *
 * For the timer "fd", this function sets the initial expiry time to "initial"
 * and the timer's periodicity to "period".
 *
 * If "initial" is not zero / TIMEOUT_NULL,
 * the timer is started and programmed to expire the first time at "initial",
 * which is an absolute or a relative time value
 * depending whether TIMEOUT_ABSOLUTE is set in "flags".
 * The corresponding clock of the timer is set by a call to sys_timer_create().
 * If "initial" is zero / TIMEOUT_NULL, an already started time is stopped.
 *
 * If "period" is non-zero, the timer operates in periodic mode
 * and will subsequently expire at multiples of "period"
 * after the initial expiration of the timer.
 * If "period" is zero, the timer operates in oneshot mode
 * and stops after the initial expiration.
 * The parameter "period" is ignored when "initial" is zero.
 *
 * The function allows to set an initial expiry time that is already
 * in the past. In this case, the timer expires immediately,
 * and the kernel will calculate a number of timer expirations
 * based on the fictitiously elapsed time.
 * Subsequent timer expirations then use multiples of "period"
 * relative to the specified initial expiry time.
 *
 * Likewise, the kernel also handles very short timer periods gracefully,
 * i.e. values for "period" below the resolution of the system timer
 * reported by sys_clock_res().
 * In this case, the kernel accounts the number of elapsed expirations
 * on timer expiration and programs the next expiration
 * based on multiples of "period".
 *
 * \note Both "initial" and "period" are encoded as timeouts
 *       to the origin of the timer's associated clock.
 *       While "initial" can be an absolute or a relative timeout
 *       by setting or clearing the TIMEOUT_ABSOLUTE flag,
 *       "period" is always a relative timeout.
 *
 * \note The timer period is limited to 2^60 - 1 nanoseconds (approx. 36 years).
 *
 * \param [in] fd			Timer file descriptor
 * \param [in] flags		Flags (must be 0 or TIMEOUT_ABSOLUTE)
 * \param [in] initial		Initial start in timeout format or 0 to stop
 * \param [in] period		Timer period or 0 for oneshot mode
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not a timer instance
 * \retval EINVAL			If period is too large (limited to 2^60 - 1)
 *
 * \see sys_clock_res()
 * \see sys_timer_create()
 * \see sys_timer_get()
 * \see sys_timer_overrun()
 * \see sys_timer_wait()
 */
err_t sys_timer_set(
	uint32_t fd,
	uint32_t flags,
	sys_timeout_t initial,
	sys_time_t period);

/** Get next timer expiry and timer period
 *
 * This function retrieves the remaining time to the next expiration
 * of a timer "fd" in "remaining", and the timer's period in "period".
 *
 * The value returned in "remaining" is always a relative timeout,
 * regardless of the setting of the TIMEOUT_ABSOLUTE flag
 * in a call to sys_timer_set() to program the timer.
 *
 * If no initial expiry and period have been set for the timer,
 * the timer was programmed in oneshot mode and has already expired,
 * or the timer is stopped,
 * the function returns both zero for "remaining" and "period".
 *
 * \param [in] fd			Timer file descriptor
 * \param [out] remaining	Remaining time until next timer expiration (if not NULL)
 * \param [out] period		Timer period (if not NULL)
 *
 * \note Both "remaining" and "period" are encoded as relative timeouts.
 *
 * \retval EOK				Success
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not a timer instance
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_clock_res()
 * \see sys_timer_create()
 * \see sys_timer_set()
 * \see sys_timer_overrun()
 * \see sys_timer_wait()
 */
err_t sys_timer_get(
	uint32_t fd,
	sys_timeout_t *remaining,
	sys_time_t *period);

/** Query timer overrun count
 *
 * This function retrieves the overrun count of a timer "fd"
 * that uses signals for notification.
 * An overrun of a timer happens when the timer expires (again)
 * and a signal is already enqueued and pending, but not received by a thread.
 * Overruns also happen when the timer is set to expire more frequently
 * than the kernel can handle timer expirations.
 *
 * At the time the signal is received by the thread,
 * a then current snapshot of the overrun counter
 * is provided "info.status" in the signal information.
 *
 * Retrieving the overrun counter has no effect on the expiration counter
 * reported by sys_timer_wait().
 *
 * \param [in] fd			Timer file descriptor
 * \param [out] overrun_count	Overrun count (if not NULL)
 *
 * \retval EOK				Success
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not a timer instance
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_timer_create()
 * \see sys_timer_set()
 * \see sys_timer_get()
 * \see sys_timer_wait()
 */
err_t sys_timer_overrun(
	uint32_t fd,
	unsigned int *overrun_count);

/** Wait for next timer expiration
 *
 * For timer "fd", this function returns in "expiry_count" the number
 * of expirations since starting the timer or the last call to this function,
 * and the timer's expiration counter is reset to zero.
 *
 * If the timer has not yet expired, the expiration count is zero. In this case,
 * the calling thread waits for expiration of the timer if "wait" is non-zero
 * or returns immediately with the error EAGAIN if "wait" is zero.
 * If multiple threads called sys_timer_wait() and wait on the same timer,
 * only one thread will be woken up on timer expiration.
 * After sucessful waiting,
 * "expiry_count" is set to the then current expiration count.
 *
 * Retrieving the expiration counter has no effect on the overrun counter
 * reported by sys_timer_overrun() or in the signal information.
 *
 * \param [in] fd			Timer file descriptor
 * \param [in] wait			Waiting mode if non-zero, polling mode if zero
 * \param [out] expiry_count	Expiration count (if not NULL)
 *
 * \retval EOK				Success
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not a timer instance
 * \retval EAGAIN			If "wait" is zero and the timer did not yet expire
 * \retval EFAULT			Invalid address or page fault
 *
 * \note This function returns EOK in both waiting and non-waiting cases.
 *
 * \see sys_timer_create()
 * \see sys_timer_set()
 * \see sys_timer_get()
 * \see sys_timer_overrun()
 */
err_t sys_timer_wait(
	uint32_t fd,
	int wait,
	uint64_t *expiry_count);

/** Close file descriptor
 *
 * This function closes the given file descriptor. Closing a file descriptor
 * removes the integer-based reference to the underlying file descriptor object.
 * When the last reference is removed, the file descriptor object is destroyed.
 *
 * Further behavior depends on the file descriptor object:
 * - IPC client-side endpoint: server threads will be notified with EPIPE.
 * - IPC server-side endpoint: client threads waiting for an IPC call or reply
 *                             and server threads will be notified with EPIPE.
 *
 * \param [in] fd			File descriptor
 *
 * \retval EOK				Success
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval ESTATE			If "fd" refers to a static file descriptor
 *
 * \note Static file descriptors cannot be closed.
 *
 * \see sys_fd_dup()
 * \see sys_fd_dup2()
 * \see sys_fd_cloexec_get()
 * \see sys_fd_cloexec_set()
 */
err_t sys_fd_close(
	uint32_t fd);

/** Duplicate file descriptor
 *
 * This function duplicates the file descriptor "fd" in the calling partition.
 * It tries to allocate an unused file descriptor starting at index "start_fd"
 * in the partition's set of file descriptors.
 * On success, the ID of the new file descriptor is returned in "new_fd".
 * The CLOEXEC attribute of the new file descriptor is not copied from "fd",
 * but set according to the SYS_FD_CLOEXEC flag.
 *
 * \param [in] fd			File descriptor
 * \param [in] start_fd		Start index for new file descriptor
 * \param [in] flags		Flags, i.e. SYS_FD_CLOEXEC
 * \param [out] new_fd		New file descriptor (if not NULL)
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval EMFILE			If no free file descriptor is found
 * \retval EFAULT			Invalid address or page fault
 *
 * \note The source file descriptor "fd" can be a static file descriptor.
 *
 * \note Like POSIX, sys_fd_dup() returns EMFILE if "start_fd" is out of range.
 *
 * \see sys_fd_close()
 * \see sys_fd_dup2()
 * \see sys_fd_cloexec_get()
 * \see sys_fd_cloexec_set()
 */
err_t sys_fd_dup(
	uint32_t fd,
	uint32_t start_fd,
	unsigned int flags,
	uint32_t *new_fd);

/** Duplicate file descriptor
 *
 * This function duplicates the file descriptor "fd" in the calling partition
 * to a new one with index "new_fd" in the partition's set of file descriptors.
 * Any previously opened file descriptor at "new_fd" will be closed beforehand.
 * The CLOEXEC attribute of the new file descriptor is not copied from "fd",
 * but set according to the SYS_FD_CLOEXEC flag.
 *
 * \param [in] fd			File descriptor
 * \param [in] new_fd		New file descriptor
 * \param [in] flags		Flags, i.e. SYS_FD_CLOEXEC
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EBADF			If "new_fd" refers to an invalid file descriptor
 * \retval ESTATE			If "new_fd" refers to a static file descriptor
 *
 * \note The source file descriptor "fd" can be a static file descriptor.
 *
 * \see sys_fd_close()
 * \see sys_fd_dup()
 * \see sys_fd_cloexec_get()
 * \see sys_fd_cloexec_set()
 */
err_t sys_fd_dup2(
	uint32_t fd,
	uint32_t new_fd,
	unsigned int flags);

/** Get CLOEXEC attribute of file descriptor
 *
 * This function returns the CLOEXEC attribute of file descriptor "fd"
 * in "attrib" (if not NULL).
 * The CLOEXEC attribute is indicated by the SYS_FD_CLOEXEC bit.
 *
 * \param [in] fd			File descriptor
 * \param [out] attrib		Attributes (if not NULL)
 *
 * \retval EOK				Success
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval ESTATE			If "fd" refers to a static file descriptor
 * \retval EFAULT			Invalid address or page fault
 *
 * \note Static file descriptors cannot be closed
 * and therefore do not posses SYS_FD_CLOEXEC attributes.
 *
 * \see sys_fd_close()
 * \see sys_fd_dup()
 * \see sys_fd_du2()
 * \see sys_fd_cloexec_set()
 */
err_t sys_fd_cloexec_get(
	uint32_t fd,
	uint32_t *attrib);

/** Set CLOEXEC attribute of file descriptor
 *
 * This function sets the CLOEXEC attribute of file descriptor "fd"
 * according to the SYS_FD_CLOEXEC flag in "attrib".
 *
 * \param [in] fd			File descriptor
 * \param [in] attrib		Attributes, i.e. SYS_FD_CLOEXEC
 *
 * \retval EOK				Success
 * \retval EINVAL			If "attrib" contains invalid attributes
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval ESTATE			If "fd" refers to a static file descriptor
 *
 * \note Static file descriptors cannot be closed
 * and therefore do not posses SYS_FD_CLOEXEC attributes.
 *
 * \see sys_fd_close()
 * \see sys_fd_dup()
 * \see sys_fd_du2()
 * \see sys_fd_cloexec_get()
 */
err_t sys_fd_cloexec_set(
	uint32_t fd,
	uint32_t attrib);

/** Retrieve name of IPC client or server port
 *
 * This function iterates the partition's available client and server ports.
 * The parameter "type" identifies the IPC endpoint type,
 * i.e. IPC_TYPE_CLIENT or IPC_TYPE_SERVER,
 * while the index "id" refers to a specific port ID of that type.
 * The returns the assigned name of the IPC endpoint
 * in a string "name" of length "size".
 * The endpoints are enumerable with indices from 0 to a configured limit.
 *
 * \param [in] type			Type of IPC endpoint
 * \param [in] id			Port ID of specific type
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval EINVAL			If "type" is invalid
 * \retval ELIMIT			If the given event port ID is out of bounds
 * \retval EINVAL			If "name" is NULL
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_ipc_port_fd()
 */
err_t sys_ipc_port_name(
	uint32_t type,
	uint32_t id,
	char *name,
	size_t size);

/** Retrieve static file descriptor of IPC client or server port
 *
 * This function iterates the partition's available client and server ports.
 * The parameter "type" identifies the IPC endpoint type,
 * i.e. IPC_TYPE_CLIENT or IPC_TYPE_SERVER,
 * while the index "id" refers to a specific port ID of that type.
 * The returns the assigned static file descriptor of the IPC endpoint in "fd".
 * The endpoints are enumerable with indices from 0 to a configured limit.
 * Note that unconnected IPC_TYPE_CLIENT endpoints do not have
 * an associated file descriptor.
 *
 * \param [in] type			Type of IPC endpoint
 * \param [in] id			Port ID of specific type
 * \param [out] fd			Static file descriptor
 *
 * \retval EOK				Success
 * \retval EINVAL			If "type" is invalid
 * \retval ELIMIT			If the given event port ID is out of bounds
 * \retval EINVAL			If "fd" is NULL
 * \retval ESTATE			If the client IPC endpoint is not connected
 *
 * \see sys_ipc_port_name()
 */
err_t sys_ipc_port_fd(
	uint32_t type,
	uint32_t id,
	uint32_t *fd);

/** Create two connected IPC handles for client/server communication
 *
 * This function creates a pair of connected IPC handles for client side (call)
 * and server side (wait, reply) operations. The server side handle
 * is provided in fd0, the client side handle in fd1.
 * The SYS_FD_CLOEXEC flag sets the CLOEXEC attribute of the file descriptors.
 * The IPC handles can be transferred to other partitions
 * via the IPC granting mechanism.
 *
 * \param [in] flags		Flags, i.e. SYS_FD_CLOEXEC
 * \param [out] fd0			Server file desciptor / IPC handle
 * \param [out] fd1			Client file desciptor / IPC handle
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "fd0" or "fd1" are NULL
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval ENFILE			If the partition is out of free file descriptors
 * \retval EFAULT			Invalid address or page fault
 *
 * \note IPC handles are waitable file descriptors in the kernel API.
 * They can be closed by calling sys_fd_close() and shared with other processes.
 *
 * \note IPC handles can be used with epoll. On the server side, the EPOLLIN
 * event indicates that clients are waiting, and EPOLLERR and EPOLLHUP
 * will be set when the client side closes its file descriptor.
 * The reported events for client side depend on the behavior of the IPC server.
 * The server uses sys_ipc_notify() on the server file descriptor
 * to set the currently pending events for the client file descriptor
 * By default, client side file descriptors do not support epoll.
 * The server must enable epoll support for the client side
 * by calling sys_ipc_notify() at least once.
 *
 * \see sys_fd_close()
 * \see sys_ipc_port_fd()
 * \see sys_ipc_call()
 * \see sys_ipc_reply_wait()
 * \see sys_fd_notify()
 */
err_t sys_ipc_pair(
	unsigned int flags,
	uint32_t *fd0,
	uint32_t *fd1);

/** IPC client-side operation: call (combined send and receive)
 *
 * This function performs a client-side IPC send and receive operation,
 * shortened IPC call, to the server endpoint identified by "fd".
 * The kernel sends the message "send_msg" to the server,
 * and receives a message from the server in "recv_msg".
 * The calling thread first waits for the server to receive "send_msg",
 * then waits for the reply of the server in "recv_msg".
 *
 * Further behavior of the IPC call is controlled by the "flags" field
 * in the outgoing message. The following combinations of flags are recognized:
 * - SYS_IPC_MAP: The caller expects to receive a mapping from the server.
 *   In this case, the caller must specify mapping parameters in the header:
 *   - h8/map_prot specifies the requested mapping permissions,
 *     i.e. a combination of PROT_READ, PROT_WRITE and PROT_EXEC.
 *   - h32/map_flags specifies the requested mapping type and mapping flags:
 *     - one of MAP_SHARED or MAP_PRIVATE must be set
 *     - type MAP_GUARD is not supported in IPC
 *     - flag MAP_ANON is not supported in IPC
 *     - MAP_FIXED and MAP_EXCL control the placement of the mapping
 *       in the callers address space
 *     - other mapping flags are not supported in IPC
 *   - arg0/map_addr is a page-aligned hint to the start address of the mapping
 *   - arg1/map_size is the non-zero page-aligned size of the mapping
 *   - h64 usually refers to a file offset to map (this depends on the server)
 *   Setting the following flags together with SYS_IPC_MAP generate an error:
 *   SYS_IPC_BUF0R, SYS_IPC_BUF0W, SYS_IPC_FD0 and SYS_IPC_FD1
 *   On reception, the flag SYS_IPC_MAP encodes if a mapping was transferred.
 * - SYS_IPC_BUF0R and SYS_IPC_BUF0W: Activates buffer 0. SYS_IPC_BUF0R grants
 *   read permissions to the server, and SYS_IPC_BUF0W write permissions.
 *   Both flags set at the same time grants both read and write permissions.
 *   - if SYS_IPC_IOV is set: buffer 0 is an I/O vector:
 *     - arg0/iov is a vector of memory segments (struct sys_iov)
 *     - arg1/iov_num is the number of entries in the vector (up to SYS_IOV_MAX)
 *   - if SYS_IPV_IOV is not set: buffer 0 as an indirect memory buffer:
 *     - arg0/buf0 is the start of memory buffer 0
 *     - arg1/size0 is the size of memory buffer 0
 *   Buffer mode and I/O vector mode are handled transparently for the server,
 *   e.g. the server can access the sender's I/O vector like a normal buffer.
 *   In vector mode, the kernel provides the overall size of all segments
 *   in the vector like in buffer mode in arg1/size0 to the server.
 *   Setting SYS_IPC_BUF0R and SYS_IPC_BUF0W together with SYS_IPC_MAP
 *   generates an error.
 * - setting SYS_IPC_IOV without SYS_IPC_BUF0R or SYS_IPC_BUF0W generates an error
 * - SYS_IPC_BUF1R and SYS_IPC_BUF1W: grants read or write permissions
 *   to buffer 1. Only the indirect memory buffer mode is supported here:
 *   - arg2/buf1 is the start of memory buffer 1
 *   - arg3/size1 is the size of memory buffer 1
 * - SYS_IPC_BUF2R and SYS_IPC_BUF2W: grants read or write permissions
 *   to buffer 2. Only the indirect memory buffer mode is supported here:
 *   - arg4/buf2 is the start of memory buffer 2
 *   - arg5/size2 is the size of memory buffer 2
 * - SYS_IPC_FD0 and SYS_IPC_FD1: request reception file descriptors.
 *   One (SYS_IPC_FD0) or two (both SYS_IPC_FD0 and SYS_IPC_FD1) file descriptors
 *   can be received in the IPC reply message in h64l/fd0 and h64h/fd1.
 *   On reception, the flags SYS_IPC_FD0 and SYS_IPC_FD1 indicate
 *   if one or two file descriptors were transferred.
 *   The following flags further control the received file descriptors:
 *   - SYS_IPC_FD_CLOEXEC enables CLOEXEC semantics
 *   Setting SYS_IPC_MAP with SYS_IPC_FD0 or SYS_IPC_FD1 generates an error.
 *   Setting SYS_IPC_FD1 without SYS_IPC_FD0 generates an error.
 *
 * When requesting to receive a mapping, the kernel checks the validity
 * of the specified parameters before sending the IPC message.
 * When the server creates a mapping, the mapping will be created with
 * the requested size and permissions in the caller's virtual address space.
 * The placement of the mapping is controlled by the following rules:
 * - map_addr == NULL: let the kernel pick a suitable address
 * - map_addr != NULL: take the given address as a hint
 * - map_addr != NULL and MAP_FIXED set in map_flags:
 *                 use given address and overmap any previous mappings
 * - map_addr != NULL and both MAP_FIXED and MAP_EXCL set in map_flags:
 *                 use given address and fail if there is already a mapping
 * The resulting address is reported in "map_start" in the received message.
 * Cache attributes of the mapping depend on the underlying memory requirement.
 *
 * Any memory buffers and I/O vector entries must refer to a valid part
 * of the caller's address space. The kernel checks the validity
 * of the address ranges before sending an IPC message, but the kernel does not
 * check if the referenced memory is mapped with the described permissions.
 * This is done at the time when the server accesses the memory buffers.
 *
 * In "recv_msg", the kernel controls the IPC "flags" field:
 * - SYS_IPC_MAP: The caller has received the requested mapping.
 *   h64/map_start has the resulting start address of the mapping.
 * - SYS_IPC_FD0: The caller has received a file descriptor in h64/fd0.
 * - SYS_IPC_FD1: The caller has received a file descriptor in h64/fd1.
 * - SYS_IPC_FD1 is never set without SYS_IPC_FD0
 * - SYS_IPC_MAP and SYS_IPC_FD0/SYS_IPC_FD1 are mutually exclusive.
 *
 * \param [in] fd			Client-side handle to call
 * \param [in] send_msg		Message to send
 * \param [out] recv_msg	Message to receive
 *
 * \retval EOK				Success
 * \retval EINVAL			If "send_msg" or "recv_msg" are NULL
 * \retval EFAULT			Invalid address or page fault when accessing "send_msg"
 * \retval EINVAL			For a mapping: If "map_prot" is invalid
 * \retval EINVAL			For a mapping: If "map_flags" are invalid
 * \retval EINVAL			For a mapping: If "map_addr" or "map_size" are not page aligned
 * \retval EINVAL			For a mapping: If "map_addr" and "map_size" is not a valid user space region
 * \retval EFAULT			For a memory buffer: If start and size of the buffers are invalid addresses
 * \retval EINVAL			For a vector: If the number of entries in the vector exceeds the maximum vector size
 * \retval EINVAL			If the overall size of buffer or of all segments in the vector overflows ssize_t
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EPIPE			If "fd" was closed by the server side
 * \retval EAGAIN			While sending: If the client was unblocked on the client side waiting for the server to receive the message
 * \retval EPIPE			While sending: If the client was unblocked because "fd" was closed by the server side
 * \retval ECANCEL			While receiving: If the client was unblocked on the client side waiting for the server to reply
 * \retval EPIPE			While receiving: If the client was unblocked because "fd" was closed by the server side
 * \retval EFAULT			Invalid address or page fault when accessing "recv_msg"
 *
 * \note The error codes EAGAIN and ECANCEL can be used to distinguish
 * the two waiting states of the function.
 * In case of EAGAIN, the server did not receive the message,
 * and the sending part of the IPC can be repeated.
 * In case of ECANCEL, the server received the message, but did not yet reply.
 *
 * \note Both "send_msg" and "recv_msg" can share the same data structure.
 * The kernel will not modify "recv_msg" until the reception of the message.
 *
 * \note IPC operations to a non-client side file descriptors do not fail,
 * but trigger the specific file descriptor's default IPC action in the kernel,
 * e.g. for eventfd, the kernel implements read and write operations internally.

 * \see sys_ipc_port_fd()
 * \see sys_ipc_pair()
 * \see sys_ipc_reply_wait()
 * \see sys_ipc_read()
 * \see sys_ipc_write()
 * \see sys_ipc_sharemap()
 * \see sys_ipc_sharefd()
 */
err_t sys_ipc_call(
	uint32_t fd,
	const struct sys_ipc *send_msg,
	struct sys_ipc *recv_msg);

/** IPC server-side operation: reply and wait (combined send and receive)
 *
 * This function performs a server-side IPC reply and wait operation.
 * At first, a server replies (sends) to a waiting client
 * identified by the reply handle "send_rhnd" with the message "send_msg".
 * Then the server waits for the next message on the server-side handle "fd",
 * and, when a message is received, returns a new reply handle in "recv_rhnd"
 * and the message in "recv_msg". Both the reply and wait parts can be skipped
 * by providing NULL pointers for "send_msg" or "recv_msg".
 * Additionally, the reply part can be skipped if "send-rhnd" has the value 0.
 *
 * When sending the reply, the kernel clears the "flags" field in "send_msg"
 * and sets SYS_IPC_MAP, SYS_IPC_FD0, or SYS_IPC_FD1 to indicate reception
 * of a mapping or file descriptors by previous calls to sys_ipc_sharemap()
 * or sys_ipc_sharefd(). The kernel also updates the h64/map_start, h64/fd0,
 * and h64/fd1 accordingly. The remainder of "send_msg" is copied unchanged.
 * After receiving the reply message, the original IPC caller is woken up.
 *
 * The call waits for the next message from the client-side handle "fd".
 * On arrival of a message from the client, the message is copied to "recv_msg",
 * and a new reply handle is provided in "recv_rfd".
 * Then a server can dispatch the message, access the indirect memory buffers
 * in the IPC message with sys_ipc_read() and sys_ipc_write(),
 * or transfer a mapping or file descriptors
 * with sys_ipc_sharemap() or sys_ipc_sharefd().
 *
 * \param [in] send_rhnd	Reply handle to send reply_msg to (reply skipped if value 0)
 * \param [in] send_msg		Message to reply (reply skipped if NULL)
 * \param [in] fd			Server-side handle to wait and receive messages on
 * \param [out] recv_rhnd	New reply handle (for later reply)
 * \param [out] recv_msg	New message to receive (receive skipped if NULL)
 *
 * \retval EOK				Success
 * \retval EFAULT			Invalid address or page fault when accessing "send_msg"
 * \retval ESRCH			If "send_rhnd" does not refer to a reply handle
 * \retval ESRCH			If the client side was unblocked while waiting for the reply
 * \retval ESRCH			If the client side died while the server was preparing the reply
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not an IPC server-side handle
 * \retval EPIPE			If "fd" was closed by the client side
 * \retval ECANCEL			While waiting: If the server was unblocked on the server side waiting for a new message
 * \retval EPIPE			While waiting: If "fd" was closed by the server side
 * \retval EFAULT			Invalid address or page fault when accessing "recv_msg" or "recv_rhnd"
 *
 * \note Reply handles "send_rhnd" and "recv_rhnd" are not file descriptors.
 * The ability to reply to a previously received IPC call is independent
 * of the state of the file descriptor on the client side,
 * i.e. if the client has already closed its file descriptor.
 * After observing the error code EPIPE, it is the responsibility of the server
 * to eventually reply to the clients before closing
 * the server-side file descriptor.
 *
 * \note Unlike IPC call, a server expects the client to be waiting for a reply,
 * so the function exposes only one waiting state when waiting for new messages.
 * But a waiting client can be unblocked by other means, or even get deleted,
 * so the reply (send) part of the server operation can fail.
 * The error codes ESRCH and ECANCEL can be used to distinguish
 * an earlier send failure from a later waiting failure.
 * The error code ECANCEL indicates that the send phase completed successfully,
 * and that waiting simply can be repeated.
 *
 * \note Both "send_msg" and "recv_msg" can share the same data structure.
 * The kernel will not modify "recv_msg" until the reception of a message.
 * Likewise, "recv_rhnd" is also not modified until receptionn.
 *
 * \see sys_ipc_port_fd()
 * \see sys_ipc_pair()
 * \see sys_ipc_call()
 * \see sys_ipc_reply()
 * \see sys_ipc_wait()
 * \see sys_ipc_read()
 * \see sys_ipc_write()
 * \see sys_ipc_sharemap()
 * \see sys_ipc_sharefd()
 */
err_t sys_ipc_reply_wait(
	uint32_t send_rhnd,
	const struct sys_ipc *send_msg,
	uint32_t fd,
	uint32_t *recv_rhnd,
	struct sys_ipc *recv_msg);

/** IPC server-side operation: reply only
 *
 * This function performs a server-side IPC reply operation, but not wait.
 *
 * \see sys_ipc_reply_wait()
 * \see sys_ipc_wait()
 */
err_t sys_ipc_reply(
	uint32_t send_rhnd,
	const struct sys_ipc *send_msg);

/** IPC server-side operation: wait only
 *
 * This function performs a server-side IPC wait operation, without a reply.
 *
 * \see sys_ipc_reply_wait()
 * \see sys_ipc_reply()
 */
err_t sys_ipc_wait(
	uint32_t fd,
	uint32_t *recv_rhnd,
	struct sys_ipc *recv_msg);

/** IPC server-side operation: read message buffers in IPC
 *
 * This function copies data in the indirect message buffers in an IPC message
 * from the remote IPC client address space
 * to the local caller (server) address space.
 * Availability and buffer sizes of the buffers are encoded in the IPC message:
 * - "in" selects the remote IPC buffers buf0, buf1 or buf2.
 * - "offset" refers to a byte offset in the remote buffer.
 * - "buf" and "size" refer to a local buffer to copy the data to
 * - the number of transferred bytes is returned in "read_size"
 * The function does not allow to read beyond the size of the remote buffer,
 * and reduces the number of transferred bytes accordingly.
 * Setting the offset beyond the size of the remote buffer or reading zero bytes
 * is indicates as successful copy operation with zero bytes in "read_size".
 *
 * \param [in] rhnd			Reply handle of message
 * \param [in] id			Remote buffer selection (0 to 2)
 * \param [in] buf			Local buffer address
 * \param [in] size			Local buffer size
 * \param [in] offset		Offset in remote buffer
 * \param [out] read_size	Transferred bytes (if not NULL)
 *
 * \retval EOK				Success
 * \retval ESRCH			If "rhnd" does not refer to a reply handle
 * \retval ESRCH			If the client side is no longer waiting for the IPC reply
 * \retval ESTATE			If the client did not agree on the memory access
 * \retval EFAULT			A page fault happened when accessing the remote buffer
 * \retval EFAULT			Invalid address or page fault when accessing "buf" or "read_size"
 *
 * \note In case of error codes EFAULT or ESTATE, a server should reply
 * with a message to indicate the EFAULT error to the IPC client.
 *
 * \see sys_ipc_call()
 * \see sys_ipc_reply_wait()
 * \see sys_ipc_read_exact()
 * \see sys_ipc_write()
 * \see sys_ipc_sharemap()
 * \see sys_ipc_sharefd()
 */
err_t sys_ipc_read(
	uint32_t rhnd,
	unsigned int id,
	void *buf,
	size_t size,
	size_t offset,
	size_t *read_size);

/** Wrapper for sys_ipc_read()
 *
 * IPC servers usually copy full buffers from and to clients' address spaces.
 * This wrapper to sys_ipc_read() ensures that the exact size is copied,
 * i.e. "read_size" equals "size" on success. Partial copies result in an error.
 *
 * \retval EOK				Success
 * \retval EFAULT			On all errors specified for sys_ipc_read()
 *
 * \see sys_ipc_read()
 * \see sys_ipc_write()
 * \see sys_ipc_write_exact()
 */
err_t sys_ipc_read_exact(
	uint32_t rhnd,
	unsigned int id,
	void *buf,
	size_t size,
	size_t offset,
	size_t *read_size);

/** IPC server-side operation: write message buffers in IPC
 *
 * This function copies data in the indirect message buffers in an IPC message
 * from the local caller (server) address space
 * to the remote IPC client address space.
 * Availability and buffer sizes of the buffers are encoded in the IPC message:
 * - "in" selects the remote IPC buffers buf0, buf1 or buf2.
 * - "offset" refers to a byte offset in the remote buffer.
 * - "buf" and "size" refer to a local buffer to copy the data from
 * - the number of transferred bytes is returned in "written_size"
 * The function does not allow to write beyond the size of the remote buffer,
 * and reduces the number of transferred bytes accordingly.
 * Setting the offset beyond the size of the remote buffer or reading zero bytes
 * is indicates as successful copy operation with zero bytes in "written_size".
 *
 * \param [in] rhnd			Reply handle of message
 * \param [in] id			Remote buffer selection (0 to 2)
 * \param [in] buf			Local buffer address
 * \param [in] size			Local buffer size
 * \param [in] offset		Offset in remote buffer
 * \param [out] written_size	Transferred bytes (if not NULL)
 *
 * \retval EOK				Success
 * \retval ESRCH			If "rhnd" does not refer to a reply handle
 * \retval ESRCH			If the client side is no longer waiting for the IPC reply
 * \retval ESTATE			If the client did not agree on the memory access
 * \retval EFAULT			A page fault happened when accessing the remote buffer
 * \retval EFAULT			Invalid address or page fault when accessing "buf" or "read_size"
 *
 * \note In case of error codes EFAULT or ESTATE, a server should reply
 * with a message to indicate the EFAULT error to the IPC client.
 *
 * \see sys_ipc_call()
 * \see sys_ipc_reply_wait()
 * \see sys_ipc_read()
 * \see sys_ipc_write_exact()
 * \see sys_ipc_sharemap()
 * \see sys_ipc_sharefd()
 */
err_t sys_ipc_write(
	uint32_t rhnd,
	unsigned int id,
	const void *buf,
	size_t size,
	size_t offset,
	size_t *written_size);

/** Wrapper for sys_ipc_write()
 *
 * IPC servers usually copy full buffers from and to clients' address spaces.
 * This wrapper to sys_ipc_write() to ensure that the exact size is copied,
 * i.e. "written_size" equals "size" on success. Partial copies result in an error.
 *
 * \retval EOK				Success
 * \retval EFAULT			On all errors specified for sys_ipc_write()
 *
 * \see sys_ipc_read()
 * \see sys_ipc_read_exact()
 * \see sys_ipc_write()
 */
err_t sys_ipc_write_exact(
	uint32_t rhnd,
	unsigned int id,
	const void *buf,
	size_t size,
	size_t offset,
	size_t *written_size);

/** IPC server-side operation: setup mapping in IPC client address space
 *
 * This function creates a mapping of the memory requirement "fd"
 * in the remote client address space specified by the IPC reply handle "rhnd".
 * The client has already specified the following attributes in the IPC message:
 * - an IPC message with SYS_IPC_MAP flag set
 * - h8/map_prot specifies the requested mapping permissions,
 *     i.e. a combination of PROT_READ, PROT_WRITE and PROT_EXEC.
 * - h32/map_flags specifies the requested mapping type and mapping flags:
 *   - one of MAP_SHARED or MAP_PRIVATE must be set
 *   - MAP_FIXED and MAP_EXCL control the placement of the mapping
 *     in the callers address space
 * - arg0/map_addr is a page-aligned hint to the start address of the mapping
 * - arg1/map_size is the non-zero page-aligned size of the mapping
 * - h64 usually refers to a file offset to map (this depends on the server)
 * The kernel has already checked the validity of the parameters in the message.
 * This function now additionally specifies the following attributes:
 * - fd refers to a memory requirement
 * - flags encode the remapping behavior:
 *   - MAP_ALLOW_READ:  memory may be remapped with read access permission
 *   - MAP_ALLOW_WRITE: memory may be remapped with write access permission
 *   - MAP_ALLOW_EXEC:  memory may be remapped with executable access permission
 *   - other valid mapping flags are silently ignored
 * - offset is a page-aligned offset within the memory requirement
 * The mapping will be created in the memory region specified by the client.
 * Cache attributes of the mapping depend on the underlying memory requirement
 * and will be copied over from the server mapping to the client mapping.
 *
 * After successfully sharing the mapping, the IPC client receive the resulting
 * start address of the mapping in h64/map_start in the reply message,
 * and the flag SYS_IPC_MAP will be set.
 * A server can share a mapping only once during an IPC reply;
 * afterwards, the mapping already exists in the remote address space.
 *
 * \note The parameter "fd" refers to a memory requirement file descriptor
 * opened by sys_memrq_open(). On success, the kernel maps
 * a region of the memory requirement described by "offset" and "size"
 * into the caller's virtual address space at the given address "addr"
 * with the access permissions set to "prot"
 * and cache attributes as configured for the memory requirement.
 *
 * The mapping permissions "prot" and the remapping permissions in "flags"
 * must be a subset of the configured permissions for the memory requirement
 * and the open mode (O_RDONLY, O_WRONLY, O_RDWR) of the file descriptor "fd".
 *
 * \param [in] rhnd			Reply handle of message
 * \param [in] flags		Flags
 * \param [in] fd			Memory requirement file descriptor
 * \param [in] offset		Offset of the region within the memory requirement
 *
 * \retval EOK				Success
 * \retval ESRCH			If "rhnd" does not refer to a reply handle
 * \retval ESRCH			If the client side is no longer waiting for the IPC reply
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "offset" is not page aligned
 * \retval ESTATE			If "offset" and mapping size exceed the memory requirement
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not an interrupt file descriptor
 * \retval EPERM			If "prot" is not a subset of the defined permissions
 * \retval ENOMEM			If the partition is out of memory for VMAs
 * \retval ENOMEM			If the partition is out of memory for page tables
 * \retval EEXIST			If both MAP_FIXED and MAP_EXCL are set,
 *							but a mapping already exists in the memory region
 *
 * \note The server usually passes ESTATE, EPERM, ENOMEM and EEXIST errors
 * to the client.
 *
 * \see sys_ipc_call()
 * \see sys_ipc_reply_wait()
 * \see sys_ipc_read()
 * \see sys_ipc_write()
 * \see sys_ipc_sharefd()
 * \see sys_memrq_open()
 */
err_t sys_ipc_sharemap(
	uint32_t rhnd,
	unsigned int flags,
	uint32_t fd,
	phys_addr_t offset);

/** IPC server-side operation: share file descriptor with IPC client
 *
 * This function shares the local file descriptors "fd"
 * with the remote client partition specified by the IPC reply handle "rhnd".
 * The client has agreed to receive file descriptors in the IPC message
 * by setting the SYS_IPC_FD0 and/or SYS_IPC_FD1 flags.
 * Depending on these flags, the caller can share up to two file descriptors:
 * - a first call to this function sets up the "fd0" if SYS_IPC_FD0 is enabled
 * - a second call to this function sets up the "fd1" if SYS_IPC_FD1 is enabled
 * Setting SYS_IPC_FD_CLOEXEC in the message flags enables CLOEXEC semantics
 * for the received file descriptors.
 * Additionally, the file descriptor can be closed for the local partition
 * by setting SYS_IPC_CLOSE in the "flags" parameter.
 *
 * New file descriptors will be created in the client partition
 * following the usual file descriptor allocation pattern.
 * The IPC client receive the resulting file descriptor IDs in its partition
 * in the h64/fd0 and h64/fd1 fields in the reply message,
 * and the SYS_IPC_FD0 and SYS_IPC_FD1 flags will be set accordingly.
 *
 * \param [in] rhnd			Reply handle of message
 * \param [in] fd			File descriptor to share
 * \param [in] flags		Flags, i.e. SYS_IPC_CLOSE
 *
 * \retval EOK				Success
 * \retval EINVAL			If an invalid combination of "flags" is used
 * \retval ESRCH			If "rhnd" does not refer to a reply handle
 * \retval ESRCH			If the client side is no longer waiting for the IPC reply
 * \retval ESTATE			If the client did not opt-in to receive the given amount of file descriptors
 * \retval ESTATE			If the client already received the requested amount of file descriptors
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is a file descriptor that cannot be shared
 * \retval EMFILE			If the client partition's file descriptor table is full
 *
 * \note The server usually passes EMFILE errors to the client.
 *
 * \see sys_ipc_call()
 * \see sys_ipc_reply_wait()
 * \see sys_ipc_read()
 * \see sys_ipc_write()
 * \see sys_ipc_sharemap()
 */
err_t sys_ipc_sharefd(
	uint32_t rhnd,
	uint32_t fd,
	unsigned int flags);

/** Retrieve handle to memory requirement
 *
 * This function returns a handle (file descriptor) to a memory requirement.
 * "memrq_type" refers to the specific type of memory requirements,
 * i.e. MEMRQ_TYPE_MEM for <memrq>, MEMRQ_TYPE_IO for <map_io>,
 * MEMRQ_TYPE_SHM for <map_shm>, and MEMRQ_TYPE_FILE for <file_access>.
 * For each of the different types, "memrq_id" then refers to a specific
 * memory requirement with an index starting from zero.
 * The memory requirements follow the order specified in the XML configuration.
 *
 * If a memory requirement identified by type and ID exists,
 * the kernel returns a file descriptor to the memory requirement in "fd_index".
 *
 * The access mode to the file descriptor can be further defined by "flags":
 * - SYS_MEMRQ_READ:    requests read access
 * - SYS_MEMRQ_WRITE:   write access
 * - SYS_MEMRQ_CLOEXEC: set CLOEXEC attribute
 *
 * \note Memory requirements are normal file descriptors in the kernel API.
 * They can be closed by calling sys_fd_close() and also be shared in IPC
 * with other processes.
 *
 * \param [in] memrq_type	Memory requirement type
 * \param [in] memrq_id		Memory requirement ID
 * \param [in] flags		Flags
 * \param [out] fd			File descriptor
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "fd" is NULL
 * \retval EINVAL			If the given memory type is invalid
 * \retval ELIMIT			If the memory requirement ID is out of bounds
 * \retval EACCES			If the requested access cannot be granted
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval ENFILE			If the partition is out of free file descriptors
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_memrq_name()
 * \see sys_memrq_state()
 * \see sys_vm_map()
 * \see sys_ipc_sharemap()
 */
err_t sys_memrq_open(
	uint32_t memrq_type,
	uint32_t memrq_id,
	unsigned int flags,
	uint32_t *fd);

/** IPC server-side operation: notify IPC clients about pending events
 *
 * This function notifies clients that I/O on file descriptors is now possible.
 * The events tell a client that new data can be read from or written
 * to their client-side descriptors. The server-side is expected to notify
 * its clients each time data arrives for reading, or free space for writing
 * becomes available. As such, the event notifications are transient hints
 * to the clients, and the event notifications will be cached in the kernel.
 * If clients are currently waiting for events in sys_epoll_wait(),
 * or call sys_epoll_wait() to wait for events, the clients will be woken up,
 * if the cached events in the kernel match events the clients are waiting for.
 * Then the clients are expected to read or write new data in non-blocking mode.
 * See sys_epoll_ctl() for more details on notifying and waking up threads.
 *
 * The events follow the definition for poll in POSIX and epoll in Linux:
 * - EPOLLIN:    data available for reading
 * - EPOLLPRI:   high priority data available for reading
 * - EPOLLOUT:   data may be written
 * - EPOLLERR:   error occurred
 * - EPOLLHUP:   device disconnected
 * - EPOLLRDHUP: socket connection closed by peer (Linux)
 *
 * POSIX additionally defines EPOLLRDNORM, EPOLLRDBAND, EPOLLWRNORM, EPOLLWRBAND
 * and EPOLLMSG for the obsolete STREAM interface.
 *
 * EPOLLNVAL cannot be signaled by this function, as this event
 * represents the initial state of each client-side IPC file descriptor.
 *
 * Note that the EPOLLxxx (Linux) and the POLLxxx (POSIX) definitions
 * refer to the same events and can be mixed interchangeably.
 *
 * The caller is free to choose any suitable combination of event flags,
 * but the most common combinations for device drivers to notify clients
 * following the spirit of POSIX are:
 * - EPOLLIN | EPOLLRDNORM: data available for reading
 * - EPOLLPRI | EPOLLRDBAND: priority data available for reading
 * - EPOLLOUT | EPOLLWRNORM | EPOLLWRBAND: data may be written
 * - EPOLLHUP: hangup, remote disconnected
 * - EPOLLERR: error condition
 *
 * If data is available for both reading and writing, the event combination of
 * (EPOLLIN | EPOLLRDNORM | EPOLLOUT | EPOLLWRNORM | EPOLLWRBAND) should be used.
 * If no data is available, the value 0 should passed.
 * For file descriptors representing data streams with pipe semantics,
 * EPOLLHUP resp. EPOLLERR indicate that the writer side
 * (producer) resp. the reader side (consumer) closed the connection.
 * For steam sockets, EPOLLHUP indicates that the peer closed its end
 * of the stream, EPOLLHUP might be combined with EPOLLIN | EPOLLRDNORM
 * if there is unread data. EPOLLPRI | EPOLLRDBAND indicates high-priority
 * events, and its use depends on the semantics of the file descriptor.
 * For sockets, the server should use the low-level poll events flags
 * and should follow the definitions for IP stacks and include EPOLLRDHUP.
 *
 * A server should provide up-to-date event notifications only for file that
 * have blocking semantics (e.g. pipes, sockets, character devices), but
 * not for regular files in a file system. Regular files do not support
 * blocking operations. This is indicated by the initial event state
 * of each file descriptor, which is set to the EPOLLNVAL event
 * and prevents sys_epoll_ctl() from attaching an interest list.
 *
 * A server that supports blocking semantics on its file descriptors
 * should clear the initial EPOLLNVAL event state during an open request.
 * The server should then update the current event notifications in two cases:
 * (1) during the processing of IPC, shortly before replying to an IPC request
 * and before waiting for the next IPC request, and
 * (2) during interrupt handling when new data arrives
 * or free space for sending becomes available.
 *
 * There is no way for the server to opt-out of performing event notifications
 * for file descriptor "fd" after sucessfully changing pending events once.
 *
 * \param [in] fd			Server-side handle to notify
 * \param [in] eventmask	Events to notify
 *
 * \retval EOK				Success
 * \retval EINVAL			Invalid combination of events
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not an IPC server-side handle
 * \retval EPIPE			If "fd" was closed by the client side
 *
 * \see sys_ipc_reply_wait()
 * \see sys_epoll_ctl()
 * \see sys_epoll_wait()
 */
err_t sys_ipc_notify(
	uint32_t fd,
	uint32_t eventmask);

/** Create new epoll instance
 *
 * This function creates a new epoll instance
 * and returns its file descriptor in epfd.
 *
 * The SYS_FD_CLOEXEC flag sets the CLOEXEC attribute of the file descriptor.
 *
 * \note Epoll instances are normal file descriptors in the kernel API.
 * They can be closed by calling sys_fd_close(), but not be transferred outside
 * of the current process.
 *
 * \param [in] flags		Flags, i.e. SYS_FD_CLOEXEC
 * \param [out] epfd		File descriptor
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "epfd" is NULL
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval ENFILE			If the partition is out of free file descriptors
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_fd_close()
 * \see sys_epoll_ctl()
 * \see sys_epoll_wait()
 */
err_t sys_epoll_create(
	uint32_t flags,
	uint32_t *epfd);

/** Change interest list of epoll instance
 *
 * This function changes the interest list of the epoll instance "epfd".
 * Depending on the operation in "op", the caller can add, change or remove
 * its interest in event notifications on another file descriptor "fd".
 * The parameter "event.eventmask" further specifies the event notification
 * the caller is interested in or that shall be changed.
 * Lastly, "event.data" is a user specified value
 * to be returned by sys_epoll_wait() when the event notification fires.
 *
 * Note that event handling is only enabled for file descriptors that implement
 * blocking semantics. For client-based IPC file descriptors,
 * the file descriptor "fd" must be enabled for event handling by the IPC server
 * by clearing the initial EPOLLNVAL event by calling sys_ipc_notify().
 *
 * The operations are:
 * - EPOLL_CTL_ADD: add file descriptor "fd" to the interest list
 * - EPOLL_CTL_MOD: change the events for file descriptor "fd"
 * - EPOLL_CTL_DEL: remove file descriptor "fd" from the interest list
 *
 * The event flags to wait for comprise:
 * - EPOLLIN: data available for reading
 * - EPOLLPRI: high priority data available for reading
 * - EPOLLOUT: data may be written
 * - EPOLLERR: error occurred
 * - EPOLLHUP: device disconnected
 * - EPOLLRDHUP: connection closed by peer (sockets, Linux specific)
 *
 * POSIX additionally defines EPOLLRDNORM, EPOLLRDBAND, EPOLLWRNORM, EPOLLWRBAND
 * and EPOLLMSG for the obsolete STREAM interface that should not be used.
 *
 * The event flags EPOLLHUP and EPOLLERR are always implicitly set.
 *
 * The event flag EPOLLNVAL is never reported by sys_epoll_wait(),
 * but by POSIX poll() and ppoll(). The flag cannot be set by this function.
 *
 * The following flags change the behavior of the interest and the notification:
 * - EPOLLET: event-triggered mode
 * - EPOLLONESHOT: oneshot mode
 * - EPOLLEXCLUSIVE: exclusive wakeup mode
 *
 * EPOLLONESHOT and EPOLLEXCLUSIVE modes are mutually exclusive.
 * Linux additionally defines EPOLLWAKEUP which is ignored by the kernel.
 *
 * The flags EPOLLEXCLUSIVE, EPOLLWAKEUP, EPOLLONESHOT and EPOLLET
 * control the behavior of sys_epoll_ctl()
 * and are never returned by sys_epoll_wait().
 *
 * In EPOLL_CTL_MOD, "event" can be a NULL pointer and its content is ignored.
 *
 * \param [in] epfd			Epoll file descriptor
 * \param [in] op			Operation
 * \param [in] fd			File descriptor to monitor
 * \param [in] event		Event structure with event flags and user-defined data
 *
 * \retval EOK				Success
 * \retval EINVAL			If "event" is NULL in EPOLL_CTL_DEL or EPOLL_CTL_MOD
 * \retval EINVAL			If "op" is invalid
 * \retval EINVAL			Invalid combination of events in "event.eventmask" for "op"
 * \retval EBADF			If "epfd" or "fd" refer to an invalid file descriptor
 * \retval EINVAL			If "epfd" is not an epoll instance
 * \retval EPERM			If "fd" does not support epoll
 * \retval ELOOP			If "fd" refers to an epoll instance
 * \retval EEXIST			If "fd" is already in the epoll set in EPOLL_CTL_ADD
 * \retval ENOENT			If "fd" is not in the epoll set in EPOLL_CTL_DEL or EPOLL_CTL_MOD
 * \retval ENOMEM			If the configured limit of interests is reached in EPOLL_CTL_ADD
 *
 * \note The restrictions in the event flags follow the definition in Linux.
 *
 * \note Unlike Linux, the kernel does not allow the nesting of epoll instances.
 *
 * \see sys_ipc_notify()
 * \see sys_epoll_create()
 * \see sys_epoll_wait()
 */
err_t sys_epoll_ctl(
	uint32_t epfd,
	uint32_t op,
	uint32_t fd,
	const struct sys_epoll *event);

/** Wait for event notifications on epoll instance
 *
 * This function waits for event notifications on the epoll instance "epfd"
 * for the given timeout. The function returns without waiting
 * if event notifications are already pending.
 *
 * Up to "max_events" event notifications are returned in the "events" array.
 * The number of retrieved event notifications is provided in "num_events".
 * For each event notification, the function sets "eventmask" and "data"
 * to the pending events and the user value set with sys_epoll_ctl().
 * Note that the flags EPOLLEXCLUSIVE, EPOLLWAKEUP, EPOLLONESHOT and EPOLLET
 * are never returned by sys_epoll_wait().
 *
 * If "blocked_mask" is not NULL, the function temporarily replaces
 * the set of blocked signals of the calling thread with "blocked_mask"
 * while waiting and restores the original set of blocked signals after waiting.
 *
 * The relative timeout uses CLK_ID_MONOTONIC as clock ID.
 *
 * \param [in] epfd			Epoll file descriptor
 * \param [in] max_events	Maximum number of events in array
 * \param [in] timeout		Timeout or TIMEOUT_NULL for polling
 * \param [in] blocked_mask	Set of signals to block while waiting
 * \param [out] events		Event list
 * \param [out] num_events	Actual number of returned events
 *
 * \retval EOK				Success
 * \retval EINVAL			If "events" or "num_events" are NULL
 * \retval EBADF			If "epfd" refers to an invalid file descriptor
 * \retval EINVAL			If "epfd" is not an epoll instance
 * \retval EFAULT			Invalid address or page fault
 * \retval EAGAIN			If timeout is TIMEOUT_NULL
 *							and no event notification is pending
 * \retval ETIMEDOUT		If the timeout expired before the thread
 *							was woken up by an event notification
 *
 * \note The API follows the spirit of epoll_pwait2() in Linux.
 *
 * \note The "blocked_mask" parameter follows the related parameter
 * in pselect() and ppoll() in POSIX.
 *
 * \see sys_ipc_notify()
 * \see sys_epoll_create()
 * \see sys_epoll_ctl()
 */
err_t sys_epoll_wait(
	uint32_t epfd,
	uint32_t max_events,
	sys_time_t timeout,
	const sys_sig_mask_t *blocked_mask,
	struct sys_epoll *events,
	uint32_t *num_events);

/** Create new eventfd instance
 *
 * This function creates a new eventfd instance and returns its file descriptor
 * in "fd".
 *
 * An eventfd consists of an internal unsigned 64-bit event counter
 * that is modified by read and write operations on the file descriptor.
 * The initial counter value is provided in "initval".
 *
 * If the counter value is non-zero, read operations reduce or consume
 * the counter value and the value the counter was reduced by is returned
 * by the read operation. If the counter value is zero, the read operation
 * blocks, until the counter is incremented by a successful write operation.
 * The value provided to the write operation is added to the counter value.
 *
 * The value to reduce the internal counter by in a read operation is defined
 * at initialization time. When using semaphore mode, the counter value
 * is reduced by one (and, likewise, write operations should increment
 * the counter value also only by one). When not using semaphore mode,
 * the eventfd operates in event mode and consumes the full counter value
 * (the internal event counter is effectively set to zero after reading).
 * Blocked readers will be served in FIFO order.
 *
 * Write operations can also block if the counter value would exceed
 * the internal counter limit of 0xfffffffffffffffe (highest value).
 * Blocked writers will be tried in FIFO order until the written value
 * can be successfully added to the internal counter value.
 *
 * The SYS_FD_CLOEXEC flag sets the CLOEXEC attribute of the file descriptor.
 * The SYS_FD_NONBLOCK flag enables non-blocking mode, and blocking reads
 * or writes will return EAGAIN instead of blocking.
 * The SYS_FD_SPECIAL flag enables semaphore mode (read operations consume one)
 * instead of event mode (read operations consume all).
 *
 * \note Eventfd instances are normal file descriptors in the kernel API.
 * They can be closed by calling sys_fd_close() and also be shared in IPC
 * with other processes.
 *
 * \param [in] initval		Initial value
 * \param [in] flags		Flags, e.g. SYS_FD_NONBLOCK or SYS_FD_SPECIAL
 * \param [out] fd			File descriptor
 *
 * \retval EOK				Success
 * \retval EINVAL			If "initval" is 0xffffffffffffffff
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "fd" is NULL
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval ENFILE			If the partition is out of free file descriptors
 * \retval EFAULT			Invalid address or page fault
 *
 * \note An eventfd can be used with epoll. The EPOLLIN and EPOLLOUT events
 * indicate that events can be read respectively written without blocking,
 * and EPOLLERR indicates when the event counter reached 0xffffffffffffffff.
 *
 * \note Unlike Linux, the initial value has a 64-bit type.
 *
 * \see sys_fd_close()
 */
err_t sys_eventfd_create(
	uint64_t initval,
	uint32_t flags,
	uint32_t *fd);

/** Create new signalfd instance
 *
 * This function creates a new signalfd instance and returns its file descriptor
 * in "fd".
 *
 * A signalfd allows to receive signals (but not exceptions) directed to
 * the current thread or its partition in an IPC-based read operation.
 * The set of signals is set in "sig_mask". Additionally, the caller must mask
 * these signals to disable signal reception with a signal handler.
 *
 * For each pending signal, the kernel returns a "struct signalfd_siginfo"
 * in the read operation. The structure contains exception-related information,
 * similar to "struct sys_sig_info" returned by sys_sig_wait(). However,
 * unlike sys_sig_wait(), the signal file descriptor "fd" allows to
 * (1) poll for signals (SYS_FD_NONBLOCK flag set) and
 * (2) wait for signals in an epoll instance (EPOLLIN event).
 * EPOLLIN will only be set for signals sent to the current thread's partition,
 * but not for signals set to the current thread itself,
 * as the signalfd instance is shared among all threads of a partition.
 * It is not possible to read the signals sent to other threads.
 *
 * Note that a read operation on the signalfd receives a signal
 * in an equivalent way as sys_sig_wait(). After a successful read,
 * the signal is no longer pending to the thread/partition. Similarly,
 * pending signals remain pending after closing the file descriptor "fd".
 *
 * The SYS_FD_CLOEXEC flag sets the CLOEXEC attribute of the file descriptor.
 * The SYS_FD_NONBLOCK flag enables non-blocking mode, and blocking reads
 * will return EAGAIN instead of blocking.
 *
 * \note Signalfd instances are normal file descriptors in the kernel API.
 * They can be closed by calling sys_fd_close(), but not be transferred outside
 * of the current process.
 *
 * \param [in] sig_mask		Set of signals to monitor and to receive
 * \param [in] flags		Flags, e.g. SYS_FD_NONBLOCK or SYS_FD_CLOEXEC
 * \param [out] fd			File descriptor
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EINVAL			If "fd" is NULL
 * \retval ENOMEM			If the partition is out of memory for signal handling // FIXME
 * \retval EMFILE			If the partition's file descriptor table is full
 * \retval ENFILE			If the partition is out of free file descriptors
 * \retval EFAULT			Invalid address or page fault
 *
 * \note The "signalfd_siginfo" structure returned by IPC-based read operations
 * is compatible to the same structure in Linux.
 *
 * \note Unlike Linux, the kernel implements signalfd in two system calls.
 *
 * \see sys_sig_register()
 * \see sys_sig_mask()
 * \see sys_sig_wait()
 * \see sys_signalfd_change()
 * \see sys_fd_close()
 */
err_t sys_signalfd_create(
	sys_sig_mask_t sig_mask,
	uint32_t flags,
	uint32_t *fd);

/** Change signals of signalfd instance
 *
 * The function changes the set of signals to be handled by a signalfd instance.
 * The parameter "fd" must be a valid signalfd instance, and "sig_mask"
 * is the set of signals to handle. Additionally, the caller must mask
 * these signals to disable signal reception with a signal handler.
 *
 * \param [in] fd			File descriptor
 * \param [in] sig_mask		Set of signals to monitor and to receive
 * \param [in] flags		Flags, must be zero
 *
 * \retval EOK				Success
 * \retval EINVAL			If "flags" are invalid
 * \retval EBADF			If "fd" refers to an invalid file descriptor
 * \retval EINVAL			If "fd" is not a signalfd instance
 *
 * \note Unlike Linux, the kernel implements signalfd in two system calls.
 *
 * \see sys_sig_register()
 * \see sys_sig_mask()
 * \see sys_sig_wait()
 * \see sys_signalfd_create()
 * \see sys_fd_close()
 */
err_t sys_signalfd_change(
	uint32_t fd,
	sys_sig_mask_t sig_mask,
	uint32_t flags);


/** Retrieve process ID of calling thread
 *
 * This function returns the process ID of the calling thread.
 *
 * \return Process ID
 *
 * \see sys_thread_self()
 * \see sys_part_self()
 * \see sys_process_name()
 */
uint32_t sys_process_self(void);

/** Retrieve process name of calling thread
 *
 * This function returns the partition name of the calling thread. The caller
 * must specify a string buffer which is large enough to hold the name.
 *
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 *
 * \see sys_process_self()
 */
err_t sys_process_name(char *name, size_t size);

/** Terminate the current process
 *
 * This function terminates the current process.
 *
 * \param [in] status		Exist status code
 *
 * \note A call to this function does not return.
 */
void sys_process_exit(int status) __noreturn;

#ifdef __cplusplus
}
#endif

#endif
