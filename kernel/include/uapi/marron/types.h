/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2020, 2021, 2022, 2024 Alexander Zuepke */
/*
 * marron/types.h
 *
 * Kernel types.
 *
 * azuepke, 2018-01-02: initial
 * azuepke, 2018-12-21: standalone version
 * azuepke, 2020-02-01: changes for Kuri: TLS for fast priority switching
 * azuepke, 2021-08-18: revised time format with explicit clock IDs
 * azuepke, 2022-07-17: IPC data format
 */

#ifndef MARRON_TYPES_H_
#define MARRON_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

/* thread IDs */
#define THR_ID_ALL	0xffffffff	/**< indicate "all threads" */

/* partition states */
#define PART_STATE_IDLE			0	/**< partition is idle */
#define PART_STATE_RUNNING		1	/**< partition is running */

/* thread FPU states */
#define FPU_STATE_OFF	0	/**< FPU disabled for thread */
#define FPU_STATE_AUTO	1	/**< automatically enable the FPU on demand */
#define FPU_STATE_ON	2	/**< FPU enabled for thread */

/** number of supported priorities */
#define NUM_PRIOS 256


/*
 * System time and timeout format
 *
 * The kernel manages two clock sources with nanosecond resolution,
 * a monotonic clock and a real-time clock. The monotonic clock counts
 * time since boot, and the real-time clock represents the wall clock time.
 *
 * The kernel derives both clock sources from the same hardware clock.
 * The main difference between the real-time clock and the monotonic clock
 * us that the real-time clock has an additional offset in nanoseconds
 * to yield the current wall-clock time. External services like an NTP daemon
 * keep the internal real-time clock synchronized to an external reference time,
 * however due to the synchronization, the real-time clock's offset changes
 * and users can observe "jumps" in time, while the monotonic clock is stable.
 * However, mechanisms to correct a drift in clock speed relative to a
 * reference clock can affect both the monotonic clock and the real-time clock.
 *
 * We define 1970-01-01 00:00:00 UTC as our origin of the real-time clock,
 * which is the same as the origin of POSIX time.
 * This allows easy conversion from POSIX timeouts to our timeout format.
 * Note that if the system starts and the current wall-time clock is not known,
 * the kernel's real-time clock starts at 2021-07-27 12:45:52 UTC,
 * which is POSIX time 1627389952 or 0x61000000 in hexadecimal. This allows
 * to distinguish timeouts from both clock sources during development.
 *
 * The timespan for real-time timeouts covers 2^64 nanoseconds, or 584 years,
 * from 2021 up to the year 2554.
 *
 * The kernel API encodes timeouts with 64-bit nanosecond resolution, and
 * a dedicated clock ID parameter that also encodes absolute/relative timeouts.
 * Absolute timeouts specific to a clock have the TIMEOUT_ABSOLUTE bit set.
 * Note that the POSIX specification demands that changes to the real-time clock
 * must not affect relative timeouts based on the same real-time clock,
 * so use the monotonic clock rather than the real-time clock for this use case.
 *
 * In the timeout API, there are two special cases for relative timeouts.
 * The value 0 (all bits cleared) encodes TIMEOUT_NULL
 * which means no timeout at all and enables polling mode in some services.
 * The value 0xffffffffffffffff (all bits set) encodes TIMEOUT_INFINITE,
 * that never expires.
 * When used as absolute timeouts, TIMEOUT_NULL is the earliest possible
 * point in time, and TIMEOUT_INFINITE is the latest possible point in time.
 */

/** Clock IDs */
#define CLK_ID_REALTIME 0
#define CLK_ID_MONOTONIC 1
#define NUM_CLK_IDS 2

/** absolute timeout (OR-ed together with a clock ID) */
#define TIMEOUT_ABSOLUTE 0x80000000u

/** null timeout (special timeout value for relative timeouts) */
#define TIMEOUT_NULL 0ull

/** infinite timeout (special timeout value for relative timeouts) */
#define TIMEOUT_INFINITE 0xffffffffffffffffull


/* non-standard types */

/** unsigned 8-bit integer type */
#ifndef __DEFINED_uint8_t
#define __DEFINED_uint8_t
typedef unsigned char uint8_t;
#endif

/** unsigned 16-bit integer type */
#ifndef __DEFINED_uint16_t
#define __DEFINED_uint16_t
typedef unsigned short uint16_t;
#endif

/** unsigned 32-bit integer type */
#ifndef __DEFINED_uint32_t
#define __DEFINED_uint32_t
typedef unsigned int uint32_t;
#endif

/** unsigned 64-bit integer type */
#ifndef __DEFINED_uint64_t
#define __DEFINED_uint64_t
#if __SIZEOF_LONG__ == 8
typedef unsigned long int uint64_t;
#else
typedef unsigned long long int uint64_t;
#endif
#endif

/** unsigned integer type with the width of a machine register */
#ifndef __DEFINED_ulong_t
#define __DEFINED_ulong_t
typedef unsigned long int ulong_t;
#endif

/** physical address type (always 64-bit) */
#ifndef __DEFINED_phys_addr_t
#define __DEFINED_phys_addr_t
#if __SIZEOF_LONG__ == 8
typedef unsigned long int phys_addr_t;
#else
typedef unsigned long long int phys_addr_t;
#endif
#endif

/** virtual address type (like uintptr_t) */
#ifndef __DEFINED_addr_t
#define __DEFINED_addr_t
#if __SIZEOF_LONG__ == 8
typedef unsigned long int addr_t;
#else
typedef unsigned int addr_t;
#endif
#endif

/** virtual address size type */
#ifndef __DEFINED_size_t
#define __DEFINED_size_t
#if __SIZEOF_LONG__ == 8
typedef unsigned long int size_t;
#else
typedef unsigned int size_t;
#endif
#endif

/** timeout and time types */
#if __SIZEOF_LONG__ == 8
typedef unsigned long int sys_timeout_t;
typedef unsigned long int sys_time_t;
#else
typedef unsigned long long int sys_timeout_t;
typedef unsigned long long int sys_time_t;
#endif

/** error type */
#ifndef __DEFINED_err_t
#define __DEFINED_err_t
typedef unsigned int err_t;
#endif


/** Cache operations */
typedef enum {
	/** Clean data cache content and invalidate instruction caches
	 * for application loading. The BSP shall handle the caches up
	 * to the "point of unification" in the memory hierarchy,
	 * i.e. where split data and instruction caches fall back to a single
	 * unified cache or memory.
	 */
	CACHE_OP_INVAL_ICACHE_RANGE = 0,
	/** Flush data cache content for DMA.
	 * The BSP shall handle the caches up to the "point of coherency"
	 * in the memory hierarchy, i.e. where other DMA busmasters access
	 * the memory.
	 */
	CACHE_OP_FLUSH_DCACHE_RANGE = 1,
	/** Clean data cache content up to the "point of coherency" for DMA. */
	CACHE_OP_CLEAN_DCACHE_RANGE = 2,
	/** Invalidate data cache content up to the "point of coherency" for DMA.
	 * The referenced memory region must be writable to perform invalidation.
	 * If not properly aligned, the beginning and/or the end of the affected
	 * memory region is flushed.
	 */
	CACHE_OP_INVAL_DCACHE_RANGE = 3,
	/** Flush all data cache content on the calling CPU.
	 * Privileged operation.
	 */
	CACHE_OP_FLUSH_DCACHE_ALL = 4,
	/** Invalidate all instruction cache content on the calling CPU.
	 * Privileged operation.
	 */
	CACHE_OP_INVAL_ICACHE_ALL = 5,
} sys_cache_op_t;


/** Thread local storage (TLS) structure
 *
 * The TLS data structure contains elements shared between user space
 * and the kernel. This data structure should be embedded into a user defined
 * TLs structure. Mandatory fields required by the kernel must be kept
 * at the outer TLS structure.
 *
 * Note that the kernel only needs to access the byte values, but we keep
 * TLS pointer and thread ID for alignment reasons on 64-bit systems.
 * This also helps during initial setup of a thread.
 */
struct sys_tls {
	/** Pointer to the beginning of the TLS data structure.
	 * Some architectures like x86 cannot derive the start address otherwise.
	 * This field is read by user space and written by the kernel.
	 * The kernel writes to this field on thread creation only.
	 */
	void *tls;

	/** Current thread ID.
	 * This field is read by user space and written by the kernel.
	 * The kernel writes to this field on thread creation only.
	 */
	unsigned int thr_id;

	/** Current user priority.
	 * This field is modified by both user space and the kernel.
	 * User space code updates its scheduling priority here.
	 * The kernel writes to this field on thread creation
	 * and in sys_prio_set_syscall().
	 */
	unsigned char user_prio;

	/** Priority of next thread on the ready queue.
	 * This field is read by user space and written by the kernel.
	 * The kernel updates this field on each scheduling operation
	 * with the priority of the next thread on the ready queue.
	 */
	unsigned char next_prio;

	/** Maximum priority of thread.
	 * This field is read by user space and written by the kernel.
	 * The kernel writes to this field on thread creation only.
	 */
	unsigned char max_prio;

	/** Current processor ID.
	 * This field is read by user space and written by the kernel.
	 * The kernel writes to this field on thread creation and on CPU migration.
	 */
	unsigned char cpu_id;

	/** Reserved space.
	 * The kernel writes to this field on thread creation only
	 * and initializes it with a zero value.
	 */
	unsigned long reserved1;

	/** Reserved space.
	 * The kernel writes to this field on thread creation only
	 * and initializes it with a zero value.
	 */
	unsigned int reserved2;

	/** Error value.
	 * The thread's private copy of the "errno" variable.
	 * This field fully belongs to user space; the kernel never accesses it.
	 * The kernel writes to this field on thread creation only
	 * and initializes it with a zero value.
	 */
	unsigned int errno_value;
};


/** Number of supported signals */
#define NUM_SIGS 64

/** signal mask type type */
typedef unsigned long long sys_sig_mask_t;

/** Set of all signals */
#define SIG_MASK_ALL	0xffffffffffffffffULL

/** Set of no signals */
#define SIG_MASK_NONE	0x0000000000000000ULL

/** Convert a signal to a signal mask value */
#define SIG_TO_MASK(sig) (1ULL<<(sig))

/** Flag to sys_sig_register() to use the signal stack during signal handling */
#define SYS_SIG_STACK	0x01

/** Signal information structure passed to signal handler */
struct sys_sig_info {
	/* signal number */
	unsigned char sig;
	/* signal code */
	signed char code;
	/* exception type */
	unsigned short ex_type;
	/* hardware-specific fault status or user-defined signal data */
	unsigned int status;
	/* fault address or user-defined signal data */
	ulong_t addr;
};

/* Exceptions
 *
 * For exceptions, the kernel further encodes information about the type of
 * the trap or exception in a virtual register named "exception":
 * - exception type (the exception types match their signal numbers)
 * - status flags, e.g. read, write, or execution faults
 *
 * The lower 4 bits encode the exception type.
 * Bits 4..15 decode common exception information bits.
 */
#define EX_TYPE_NONE		0	/**< No fault ocurred (default execution state) */
#define EX_TYPE_ILLEGAL		1	/**< Illegal instruction exception */
#define EX_TYPE_DEBUG		2	/**< Breakpoint or watchpoint excepion */
#define EX_TYPE_TRAP		3	/**< Trap or breakpoint instrucion exception */
#define EX_TYPE_ARITHMETIC	4	/**< Integer arithmetic error or overflow exception */
#define EX_TYPE_PAGEFAULT	5	/**< Invalid or unmapped memory address referenced exception */
#define EX_TYPE_ALIGN		6	/**< Unaligned memory access exception */
#define EX_TYPE_BUS			7	/**< Bus error (hardware reported exception) */
#define EX_TYPE_FPU_UNAVAIL	8	/**< FPU unavailable / disabled exception */
#define EX_TYPE_FPU_ERROR	9	/**< FPU error exception */
#define EX_TYPE_SYSCALL		10	/**< System call exception */

/** Extract exception type */
#define EX_TYPE(x) ((x) & 0x000f)

/** Exception on read memory access.
 * Always valid for: PAGEFAULT
 * Optionally valid for: ALIGN, BUS, DEBUG
 */
#define EX_READ				0x0010

/** Exception on write memory access.
 * Always valid for: PAGEFAULT
 * Optionally valid for: ALIGN, BUS, DEBUG
 */
#define EX_WRITE			0x0020

/** Exception on instruction fetch memory access.
 * Always valid for: PAGEFAULT
 * Optionally valid for: ALIGN, BUS, DEBUG
 */
#define EX_EXEC				0x0040

/** Permission exception on memory access.
 * A TLB entry or page table entry exists, but the current access permissions
 * forbit the current access.
 * Always valid for: PAGEFAULT
 */
#define EX_PERM				0x0080

/** FPU enabled.
 * In the register context of the thread, the FPU registers are enabled.
 * Always valid for all exceptions
 */
#define EX_FPU_ENABLED		0x0100

/* Bits encoded in Futex values */
/** Futex WAITERS bit */
#define SYS_FUTEX_WAITERS 0x80000000
/** Futex LOCKED bit (indicates a locked futex) */
#define SYS_FUTEX_LOCKED 0x00010000
/** Futex TID mask */
#define SYS_FUTEX_TID_MASK 0x0000ffff
/** Futex TID mask with LOCKED bit */
#define SYS_FUTEX_TID_LOCKED_MASK (SYS_FUTEX_LOCKED | SYS_FUTEX_TID_MASK)

/* Futex flags */
/** Flag for sys_futex_unlock() to terminate the calling thread after unlocking */
#define SYS_FUTEX_THREAD_EXIT 0x1


/* Event types */
#define EVENT_TYPE_WAIT	0
#define EVENT_TYPE_SEND	1


/* Memory requirement types */
#define MEMRQ_TYPE_MEM	0
#define MEMRQ_TYPE_IO	1
#define MEMRQ_TYPE_SHM	2
#define MEMRQ_TYPE_FILE	3

/** Memory requirement state
 *
 * This data structure describes a memory requirement's configuration
 * attributes. During system configuration, the configuration tool allocates
 * memory (for <memrq> and <shm_map>) or assigns I/O resources (for <io_map>)
 * to each partition. The elements in the structure reflect the configured
 * attributes: "phys" and "size" describe the underlying physical memory
 * region; "prot" (PROT_READ, PROT_WRITE, PROT_EXEC) and "cache" (CACHE_TYPE_*)
 * describe the access permissions and cache attributes.
 * For memory requirements that support pool allocation (type <memrq>),
 * "allocated" indicates the number of bytes already allocated from the pool.
 * Allocated is an offset (0 <= allocated <= size) in the pool.
 */
struct sys_memrq_state {
	/** physical start address */
	phys_addr_t phys;
	/** size of memory requirement */
	ulong_t size;
	/* unallocated (free) memory (if supports allocation) */
	ulong_t unallocated;
	/** access permissions (PROT_READ|PROT_WRITE|PROT_EXEC) */
	unsigned int prot;
	/** cache attributes (CACHE_TYPE_* value) */
	unsigned int cache;
};

/** Scheduling parameters
 *
 * This data structure describes the scheduling parameters of a thread.
 * It is used during thread creation and when changing or retrieving
 * a thread's scheduling parameters.
 * The kernel uses FIFO scheduling with a per-thread scheduling quantum
 * for automatic round-robin scheduling.
 * A quantum of zero refers to an infinite quantum,
 * effectively disabling automatic round-robin scheduling for the thread.
 */
struct sys_sched_param {
	/** scheduling priority */
	unsigned int prio;
	/** scheduling quantum in nanoseconds, value 0 is an infinite quantum */
	unsigned int quantum;
};


/** I/O vector */
struct sys_iov {
	/** start address of vector element */
	void *addr;
	/** size of vector element */
	size_t size;
};

/** maximum number of supported I/O vectors */
#define SYS_IOV_MAX 16


/* Flag for sys_fd API */
/* set CLOEXEC attribute of file descriptor */
#define SYS_FD_CLOEXEC	0x1
/* non-blocking file descriptor by default */
#define SYS_FD_NONBLOCK	0x2
/* special mode (semaphore mode in eventfd) */
#define SYS_FD_SPECIAL	0x4


/** IPC descriptor flags
 *
 * The user sets a combination of flags in an IPC call operation.
 * The kernel sets the flags in the IPC reply operation.
 */
#define SYS_IPC_MAP			0x0001	/**< map receive request (call) / mapping received (reply) */
#define SYS_IPC_IOV			0x0010	/**< buffer 0 vectored (in conjunction with SYS_IPC_B0R or SYS_IPC_B0W) */
#define SYS_IPC_BUF0R		0x0040	/**< buffer 0 readable */
#define SYS_IPC_BUF0W		0x0080	/**< buffer 0 writable */
#define SYS_IPC_BUF1R		0x0100	/**< buffer 1 readable */
#define SYS_IPC_BUF1W		0x0200	/**< buffer 1 writable */
#define SYS_IPC_BUF2R		0x0400	/**< buffer 2 readable */
#define SYS_IPC_BUF2W		0x0800	/**< buffer 2 writable */
#define SYS_IPC_FD0			0x1000	/**< file descriptor receive request 0 (call) / fd0 received (reply) */
#define SYS_IPC_FD1			0x2000	/**< file descriptor receive request 1 (call) / fd1 received (reply) */
#define SYS_IPC_FD_CLOEXEC	0x4000	/**< CLOEXEC semantics for received file descriptors (in conjunction with SYS_IPC_FD0 or SYS_IPC_FD1) */
#define SYS_IPC_KERNEL		0x8000	/**< kernel-raised IPC */


/** IPC descriptor
 *
 * The IPC descriptor comprises four parts, a header and three data parts.
 * The behavior of the parts differ between IPC calls and IPC replies,
 * and can be controlled by changing the message type in a flags field.
 * An IPC call copies an IPC descriptor as message of fixed size
 * (40 byte on a 32-bit system, 64 byte on a 64-bit system)
 * from the calling client to the server, and vice versa for an IPC reply.
 *
 * - IPC call
 * For an IPC call, the caller sets a request ID and the message type in flags.
 * Depending on the message type, the kernel interprets parts of the header,
 * otherwise the fields in the header contains arbitrary data.
 * The three data parts can contain arbitrary inline data (two longs each) or
 * references to three independent memory buffers in the caller address space.
 * The memory buffers are referenced by a pointer and a buffer size.
 * The first memory buffer can even refer to an I/O vector.
 * In this case, the buffer size refers to the number of elements in the vector.
 * On an IPC call, the kernel usually copies the full message to the server.
 * The kernel does not modify the message except when vectored I/O is used.
 * In this case, the kernel replaces the number of vector entries
 * with the sum of the sizes of all vector segments.
 * In the IPC flags, the caller can opt in to receive
 * up to two file descriptors (flags SYS_IPC_FD0 and SYS_IPC_FD1),
 * or to receive a mapping in the virtual address space (flag SYS_IPC_MAP).
 * For the file descriptors to receive, the caller can request CLOEXEC behavior
 * by setting the corresponding IPC flag.
 * In case of the mapping, h8 and h32 in the header carry a special meaning
 * to encode mapping permissions and mapping flags,
 * and the first data part describes a section of the virtual address space
 * for reception of the mapping instead of a memory buffer.
 *
 * - IPC reply
 * In an IPC reply, the behavior of a message depends on the previous IPC call.
 * The kernel does not further interpret the content of the message,
 * but overwrites part of the header fields. The kernel always clears the flags,
 * and only indicates if file descriptors or a mapping were transferred.
 * If file descriptors or a mapping were received in the reply, the kernel
 * provides indices to the file descriptors in the caller's partition
 * or the start address of the mapping in the caller's address space
 * in the related h64/h64a/h64b fields in the header.
 * The data parts of the message are neither interpreted nor changed.
 */
struct sys_ipc {
	/** IPC flags; call: interpreted by the kernel; reply: set by kernel */
	uint16_t flags;
	union {
		/** call: request (user defined) */
		uint8_t req;
		/** reply: error code (user defined) */
		uint8_t err;
	};
	union {
		/** arbitrary 8-bit data */
		uint8_t h8;
		/** call: SYS_IPC_MAP: mapping permission */
		uint8_t map_prot;
	};
	union {
		/** arbitrary 32-bit data */
		uint32_t h32;
		/** call: SYS_IPC_MAP: mapping flags */
		uint32_t map_flags;
	};
	union {
		/** arbitrary 64-bit data (usually file offsets) */
		uint64_t h64;
		struct {
			/** arbitrary 32-bit data (low part of h64) */
			uint32_t h64l;
			/** arbitrary 32-bit data (high part of h64) */
			uint32_t h64h;
		};
		struct {
			/** reply: SYS_IPC_MAP: resulting start address of the mapping */
			addr_t map_start;
		};
		struct {
			/** reply: SYS_IPC_FD0: first file descriptor (set by kernel) */
			uint32_t fd0;
			/** reply: SYS_IPC_FD0: second file descriptor (set by kernel) */
			uint32_t fd1;
		};
	};

	union {
		/** arbitrary 64-bit data (payload) */
		uint64_t a64;
		struct {
			/** arbitrary 32-bit data (low part of a64) */
			uint32_t a64l;
			/** arbitrary 32-bit data (high part of a64) */
			uint32_t a64h;
		};
		struct {
			/** arbitrary register-sized data */
			ulong_t arg0;
			/** arbitrary register-sized data */
			ulong_t arg1;
		};
		struct {
			/** call: SYS_IPC_BUF0R | SYS_IPC_BUF0W: buffer start address */
			union {
				void *buf0;
				const void *buf0_const;
			};
			/** call: SYS_IPC_BUF0R | SYS_IPC_BUF0W: buffer size;
			 *  the kernel puts in the overall size of all segments in a vector
			 *  if SYS_IPC_IOV is set
			 */
			size_t size0;
		};
		struct {
			/** call: (SYS_IPC_BUF0R | SYS_IPC_BUF0W) & SYS_IPC_IOV: iovec */
			struct sys_iov *iov;
			/** call: SYS_IPC_IOV: number of vectors;
			 *  the kernel puts in the overall size of all segments in a vector
			 *  (see size0)
			 */
			size_t iov_num;
		};
		struct {
			/** call: SYS_IPC_MAP: hint to start address of the mapping */
			addr_t map_addr;
			/** call: SYS_IPC_MAP: mapping size */
			size_t map_size;
		};
	};
	union {
		/** arbitrary 64-bit data (payload) */
		uint64_t b64;
		struct {
			/** arbitrary 32-bit data (low part of b64) */
			uint32_t b64l;
			/** arbitrary 32-bit data (high part of b64) */
			uint32_t b64h;
		};
		struct {
			/** arbitrary register-sized data */
			ulong_t arg2;
			/** arbitrary register-sized data */
			ulong_t arg3;
		};
		struct {
			/** call: SYS_IPC_BUF1R | SYS_IPC_BUF1W: buffer */
			union {
				void *buf1;
				const void *buf1_const;
			};
			/** call: SYS_IPC_BUF1R | SYS_IPC_BUF1W: buffer size */
			size_t size1;
		};
	};
	union {
		/** arbitrary 64-bit data (payload) */
		uint64_t c64;
		struct {
			/** arbitrary 32-bit data (low part of c64) */
			uint32_t c64l;
			/** arbitrary 32-bit data (high part of c64) */
			uint32_t c64h;
		};
		struct {
			/** arbitrary register-sized data */
			ulong_t arg4;
			/** arbitrary register-sized data */
			ulong_t arg5;
		};
		struct {
			/** call: SYS_IPC_BUF2R | SYS_IPC_BUF2W: buffer */
			union {
				void *buf2;
				const void *buf2_const;
			};
			/** call: SYS_IPC_BUF2R | SYS_IPC_BUF2W: buffer size */
			size_t size2;
		};
	};
};

/* flags for sys_ipc_sharefd() */
#define SYS_IPC_CLOSE	0x1	/**< close file descriptor */

/* IPC types */
#define IPC_TYPE_SERVER	0
#define IPC_TYPE_CLIENT	1

/* flags for sys_memrq_open() */
#define SYS_MEMRQ_READ		0x1
#define SYS_MEMRQ_WRITE		0x2
#define SYS_MEMRQ_CLOEXEC	0x8

/** Epoll event type
 *
 * This structure is used for both registration and reporting
 * of epoll event notifications.
 *
 * This implementation of the epoll event type follows the definition
 * in Linux at binary level. The explicit padding prevents data leakage.
 */
struct sys_epoll {
	/** epoll event mask (IN with and OUT without epoll control flags)
	 *
	 * common poll and epoll event notifications for drivers:
	 * EPOLLIN | EPOLLRDNORM: data available for reading
	 * EPOLLPRI | EPOLLRDBAND: priority data available for reading
	 * EPOLLOUT | EPOLLWRNORM | EPOLLWRBAND: data may be written
	 * EPOLLHUP: hangup, remote disconnected
	 * EPOLLERR: error condition
	 */
	uint32_t eventmask;
	uint32_t padding;
	/** user defined data */
	uint64_t data;
};

#ifdef __cplusplus
}
#endif

#endif
