/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2022 Alexander Zuepke */
/*
 * marron/cache_type.h
 *
 * Cache attribtes.
 *
 * azuepke, 2013-04-28: initial
 * azuepke, 2021-04-29: adapted
 * azuepke, 2022-10-16: cache attributes split from mapping.h
 */

#ifndef MARRON_CACHE_TYPE_H_
#define MARRON_CACHE_TYPE_H_

/* cache attributes
 *
 * We follow the ARM v8 architecture and distinguish between device and normal
 * memory. The first four memory types refer to device memory, the last four
 * to normal memory. For each access type, we have different constraints,
 * with usually 0 being the safest/slowest, and 3 begin the fastest.
 *
 * Conveniently, the eight types match their related eight MAIR attributes.
 */

/** uncached strongly-ordered mapping for I/O:
 * - I/O, strongly-ordered
 * - nGnRnE: Device non-Gathering, non-Reordering, no Early Write Acknowledgement
 * - ARM 32-bit TEX0_C_B_S: 000 1
 * - PowerPC WIMG: 0101
 */
#define CACHE_TYPE_UC     0

/** alias name for uncached I/O mappings */
#define CACHE_TYPE_IO     CACHE_TYPE_UC

/** uncached shareable device mapping for I/O:
 * - I/O, device type, ARM specific
 * - falls back to CACHE_TYPE_IO on other architectures
 * - nGnRE: Device non-Gathering, non-Reordering, Early Write Acknowledgement
 * - ARM 32-bit TEX0_C_B_S: 001 1
 * - PowerPC WIMG: 0100
 */
#define CACHE_TYPE_DEV    1

/* NOTE: the gap here is intended:
 * the ARM architecture defines two more device memory types nGRE and GRE
 * that have no meaningful names in the API
 */
#define CACHE_TYPE_2      2
#define CACHE_TYPE_3      3

/** write-combining mapping for memory:
 * - uncached memory, store-gathering, coherent
 * - falls back to CACHE_TYPE_IO if not supported by an architecture
 * - ARM 32-bit TEX0_C_B_1: 100 1
 * - PowerPC WIMG: 0110
 */
#define CACHE_TYPE_WC     4

/** write-through mapping for memory:
 * - cached write-through memory, read-allocation, store-gathering, coherent
 * - falls back to CACHE_TYPE_WB/CACHE_TYPE_WA if not supported by an architecture
 * - ARM 32-bit TEX0_C_B_S: 010 1
 * - PowerPC WIMG: 1010
 */
#define CACHE_TYPE_WT     5

/** write-back mapping for memory:
 * - cached write-back memory, read-allocation, store-gathering, coherent
 * - falls back to CACHE_TYPE_WT if the architecture does not support write-back memory
 * - ARM 32-bit TEX0_C_B_S: 011 1
 * - PowerPC WIMG: 0010
 */
#define CACHE_TYPE_WB     6

/** write-back with write-allocation mapping for memory:
 * - cached write-back memory, read-allocation, write-allocation, store-gathering, coherent
 * - write-allocation is optional
 * - falls back to CACHE_TYPE_WT if the architecture does not support write-back memory
 * - ARM 32-bit TEX0_C_B_S: 111 1
 * - PowerPC WIMG: 0010 (same as CACHE_TYPE_WB)
 */
#define CACHE_TYPE_WA     7

/** alias name for cached memory mappings */
#define CACHE_TYPE_RAM    CACHE_TYPE_WA

#endif
