/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021, 2022 Alexander Zuepke */
/*
 * marron/mapping.h
 *
 * Mapping specific defines and types.
 *
 * azuepke, 2013-04-28: initial
 * azuepke, 2021-04-29: adapted
 * azuepke, 2022-10-16: POSIX naming
 */

#ifndef MARRON_MAPPING_H_
#define MARRON_MAPPING_H_

/* access permissions
 *
 * We follow the usual "rwx" nomenclature and POSIX conventions.
 */

/** no access permission */
#define PROT_NONE   0x0

/** read access permission */
#define PROT_READ   0x1

/** write access permission */
#define PROT_WRITE  0x2

/** executable access permission */
#define PROT_EXEC   0x4


/* mapping type and flags
 *
 * The mapping flags comprise the kernel-supported set of "flags" to mmap().
 * The mapping type is encoded in the lowest 4 bits.
 */

/** memory is shared (effectively ignored, all memory is shared by default) */
#define MAP_SHARED  0x01

/** memory is private (effectively ignored, all memory is shared by default) */
#define MAP_PRIVATE 0x02

/** memory area used as a guard (Marron specific, fd and offset ignored) */
#define MAP_GUARD   0x04

/** mask for mapping types */
#define MAP_TYPE    0x0f

/** map at fixed address; replace previous mapping if MAP_EXCL is not set */
#define MAP_FIXED   0x10

/** disable replacing of mappings with MAP_FIXED and return an error (FreeBSD) */
#define MAP_EXCL    0x20

/** allocate anonymous memory */
#define MAP_ANON    0x40


/* memory requirement specific mapping flags for remapping */

/** memory may be remapped with read access permission */
#define MAP_ALLOW_READ  0x100

/** memory may be remapped with write access permission */
#define MAP_ALLOW_WRITE 0x200

/** memory may be remapped with executable access permission */
#define MAP_ALLOW_EXEC  0x400

/** remapping access permissions to PROT_XXX flags */
#define MAP_ALLOW_MASK  0x700
#define MAP_ALLOW_SHIFT 8


/* IPC specific mapping flags */

/** notify IPC server on sys_vm_protect() / mprotect() */
#define MAP_IPC_MPROTECT    0x10000

/** notify IPC server on sys_vm_unmap() / munmap() */
#define MAP_IPC_MUNMAP      0x20000

/** notify IPC server on sys_vm_advise() / posix_madvise() */
#define MAP_IPC_MADVISE     0x40000

/** notify IPC server on sys_vm_sync() / msync() */
#define MAP_IPC_MSYNC       0x80000

/** mask of IPC-specific flags */
#define MAP_IPC_MASK        0xf0000

#endif
