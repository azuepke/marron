/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * ipc.h
 *
 * Inter-process communication.
 *
 * azuepke, 2022-07-17: initial
 */

#ifndef IPC_H_
#define IPC_H_

#include <marron/types.h>


/* forward declaration */
struct fd;


/* API */
/** initialize all event objects at boot time */
void ipc_init_all(void);

/** close callback for client IPC endpoint */
struct fd *ipc_close_client(
	struct fd *client_fd);

/** notify server that the client IPC endpoint was closed */
void ipc_notify_server_close(
	struct fd *server_fd,
	struct fd *client_fd);

/** close callback for server IPC endpoint */
void ipc_close_server(
	struct fd *server_fd);

#endif
