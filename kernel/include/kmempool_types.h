/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * kmempool_types.h
 *
 * KMEM page pools
 *
 * We have a set of pages to be used by a partition.
 * House keeping is realized by a doubly linked list of pages.
 * The pages are handled in LIFO order, probably this gives "hotter caches".
 * The solution here can be embedded in other data structures.
 *
 * azuepke, 2013-04-09: initial
 * azuepke, 2021-04-28: adapted & simplified
 */

#ifndef KMEMPOOL_TYPES_H_
#define KMEMPOOL_TYPES_H_

#include <marron/list.h>
#include <assert.h>
#include <marron/arch_defs.h>

/** embedded KMEM page pool */
struct kmempool {
	/* page pool head */
	list_t head;

	/* number of free pages in this pool */
	unsigned long free;
};

/** initialize a specific KMEM page pool */
static inline void kmempool_init(struct kmempool *pool)
{
	assert(pool != NULL);

	list_head_init(&pool->head);
	pool->free = 0;
}

/** allocate a page from a KMEM page pool */
static inline void *kmempool_get(struct kmempool *pool)
{
	list_t *node;

	assert(pool != NULL);

	node = list_remove_first(&pool->head);
	if (node != NULL) {
		assert(pool->free > 0);
		pool->free--;
	}

	/* the memory returned is at least cacheline aligned */
	assert(((unsigned long)node & (ARCH_ALIGN - 1)) == 0);
	return node;
}

/** put a page back to its pool */
static inline void kmempool_put(struct kmempool *pool, void *page)
{
	list_t *node;

	assert(pool != NULL);
	assert(page != NULL);
	/* the memory returned is at least cacheline aligned */
	assert(((unsigned long)page & (ARCH_ALIGN - 1)) == 0);

	/* insert at head to keep it hot in caches */
	node = page;
	list_node_init(node);
	list_insert_first(&pool->head, node);

	pool->free++;
}

#endif
