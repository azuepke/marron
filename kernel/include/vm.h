/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * vm.h
 *
 * Higher-level layers of a POSIX-like virtual memory management.
 *
 * azuepke, 2021-11-03: initial
 */

#ifndef VM_H_
#define VM_H_

#include <stdint.h>
#include <marron/types.h>


/* forward declarations */
struct memrq_cfg;
struct adspace;


/* API */
/* non-allocating mmap() operation (like ioremap() in Linux) */
err_t vm_map(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot,
	unsigned int max_prot,
	const struct memrq_cfg *m,
	size_t offset,
	unsigned int flags,
	addr_t *map_addr);

/* non-allocating but ref-counting mmap() operation */
err_t vm_ref_map(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot,
	unsigned int max_prot,
	const struct memrq_cfg *m,
	size_t offset,
	unsigned int flags,
	addr_t *map_addr);

/* allocating mmap() operation (like in POSIX) */
err_t vm_alloc_map(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot,
	unsigned int max_prot,
	const struct memrq_cfg *m,
	unsigned int flags,
	addr_t *map_addr);

/* create a mapping guard */
err_t vm_guard(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int flags,
	addr_t *map_addr);

/* full-service (unref-ing) munmap() operation (like in POSIX) */
err_t vm_unmap(
	struct adspace *adspace,
	addr_t addr,
	size_t size);

/** change protection in virtual address space (mprotect()) */
err_t vm_protect(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot);

/** unref each mapped memory page in a memory area */
void vm_callback_unref(
	struct adspace *adspace,
	const struct memrq_cfg *m,
	addr_t addr,
	addr_t end);

/** unmap pages in page tables (no longer backed by a VMA) */
void vm_callback_unmap(
	struct adspace *adspace,
	addr_t addr,
	size_t size);

/** update changed permission in page tables */
void vm_callback_protchange(
	struct adspace *adspace,
	addr_t addr,
	size_t size,
	unsigned int prot);

/** resolve memory requirement by ID */
err_t vm_check_memrq(
	ulong_t memrq_type,
	ulong_t memrq_id,
	const struct memrq_cfg **m_p);

#endif
