/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * printk.h
 *
 * Console output handling functions
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported
 */

#ifndef PRINTK_H_
#define PRINTK_H__

#include <marron/compiler.h>
#include <stdarg.h>

/** initialize console lock */
void console_lock_init(void);

/** lock console lock */
void console_lock(void);

/** unlock console lock */
void console_unlock(void);


/** simple kernel vprintf() */
void vprintk(const char* format, va_list args);

/** simple kernel printf()
 *
 * field width:
 *  0   zero padding (ignored)
 *  nn  decimal field width
 *
 * length modifiers:
 *  l   long
 *  ll  long long
 *  z   size_t or uintptr_t, native register width
 *
 * conversion specifiers:
 *  c    char
 *  s    string
 *  p    pointer (implicit 'z')
 *  x    hex
 *  d    signed decimal
 *  u    unsigned decimal
 *
 * TIPS:
 * - use '%zx' to print register values
 *
 * NOTE:
 * - for hex numbers, a given field width truncates the number
 * - for decimals, a field width aligns to the right
 */
void printk(const char *format, ...) __printflike(1, 2);

#endif
