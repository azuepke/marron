/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2021 Alexander Zuepke */
/*
 * ex_table.h
 *
 * Exception table handling
 *
 * azuepke, 2013-07-09: pulled from linker.h
 * azuepke, 2021-04-23: adapted
 */

#ifndef EX_TABLE_H_
#define EX_TABLE_H_

#ifdef __ASSEMBLER__

#define EX_TABLE_32(addr, cont) \
	.pushsection __ex_table, "a" ;\
		.balign 4 ;\
		.long (addr), (cont) ;\
	.popsection

#define EX_TABLE_64(addr, cont) \
	.pushsection __ex_table, "a" ;\
		.balign 8 ;\
		.quad (addr), (cont) ;\
	.popsection

#else

#define EX_TABLE_32(addr, cont) \
	".pushsection __ex_table, \"a\"	;\n" \
	"	.balign 4					;\n" \
	"	.long " #addr ", " #cont" 	;\n" \
	".popsection					;\n"

#define EX_TABLE_64(addr, cont) \
	".pushsection __ex_table, \"a\"	;\n" \
	"	.balign 8					;\n" \
	"	.quad " #addr ", " #cont" 	;\n" \
	".popsection					;\n"

struct ex_table {
	unsigned long addr;
	unsigned long cont;
};

extern const struct ex_table __ex_table_start, __ex_table_end;

#endif

#if __SIZEOF_LONG__ == 8
#define EX_TABLE(addr, cont) EX_TABLE_64((addr), (cont))
#else
#define EX_TABLE(addr, cont) EX_TABLE_32((addr), (cont))
#endif

#endif
