/* SPDX-License-Identifier: MIT */
/* Copyright 2017-2021 Alexander Zuepke */
/*
 * sched_types.h
 *
 * Scheduler data types
 *
 * azuepke, 2017-12-31: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef SCHED_TYPES_H_
#define SCHED_TYPES_H_

#include <marron/types.h>
#include <marron/list.h>
#include <marron/compiler.h>
#include <marron/arch_defs.h>
#include <spin_types.h>


/*
 * How the scheduler works:
 * - we have an array of linked lists for each priority
 * - we keep threads in FIFO order in these per-priority lists
 * - the currently running thread is not kept on the ready queue
 *   (we assume frequent priority changes)
 * - the idle thread has a scheduling priority of -1
 * - we do not keep the idle thread in the ready queue as well
 * To find the next thread to schedule, we use a priority tracking bitmap
 * for each priority level. If the bit is set in the tracking bitmap,
 * we know that the priority's ready queue list is not empty.
 * We also cache the highest priority level which is not empty in "highest_prio"
 */

/** per-CPU scheduling data */
struct sched {
	/** the sched_lock */
	spin_t lock;

	/** assigned CPU (const after init) */
	uint8_t cpu;
	/** pending rescheduling */
	uint8_t reschedule;

	/** next highest priority, set to -1 if ready queues are empty */
	short next_highest_prio;

	/** currently scheduled thread */
	struct thread *current;
	/** the end of the current thread's assigned quantum (private to core) */
	sys_time_t quantum_expiry;

	/** idle thread to schedule if the ready queues are empty (const after init) */
	struct thread *idle;

	/** the ready queues */
	list_t readyqh[NUM_PRIOS];
	/** bitmask of active priority levels */
	unsigned int tracking_bitmap[NUM_PRIOS / (sizeof(unsigned int) * 8)];

	/** timeout queues */
	list_t timeoutqh[NUM_CLK_IDS];

	/** timer queues */
	list_t timerqh[NUM_CLK_IDS];

	/** statistics: time of last switch */
	sys_time_t stat_last_switch;
} __aligned(ARCH_ALIGN);

/** bits per word in tracking bitmap */
#define SCHED_TRACKING_BITMAP_BITS_PER_WORD (8*(int)sizeof(unsigned int))

/* generated data */
extern const uint8_t cpu_num;
extern const uint8_t sched_num;
extern struct sched sched_dyn[];

#endif
