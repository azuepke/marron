/* SPDX-License-Identifier: MIT */
/* Copyright 2014, 2020 Alexander Zuepke */
/*
 * spin.h
 *
 * Common spin lock operations for kernel use using a Linux-like interface
 *
 * NOTE: The spinlocks are optimized out in UP builds.
 *
 * azuepke, 2014-08-26: initial
 * azuepke, 2020-01-31: ticket locks
 */

#ifndef SPIN_H_
#define SPIN_H_

#include <spin_types.h>
#include <arch_spin.h>
#include <marron/compiler.h>
#include <current.h>
#include <assert.h>


#if defined(SMP) && !defined(NDEBUG)
#define spin_debug(stmt)		stmt
#else
#define spin_debug(stmt)		do { } while (0)
#endif

/** spin lock initializer */
#ifdef SMP
#define spin_init(spin)	do {	\
		spin_t *tmp_spin = (spin);	\
		arch_spin_init(&tmp_spin->arch_lock);	\
		spin_debug({	tmp_spin->arch_lock.s.owner_cpu = ARCH_SPIN_INVALID_OWNER_CPU;	});	\
	} while (0)
#else
#define spin_init(spin)	do {	\
		spin_t *tmp_spin = (spin);	\
		(tmp_spin)->unused = 0;	\
	} while (0)
#endif

/** lock spin lock */
#ifdef SMP
#define spin_lock(spin)	do {	\
		spin_t *tmp_spin = (spin);	\
		assert(tmp_spin->arch_lock.s.owner_cpu != current_cpu_id());	\
		arch_spin_lock(&tmp_spin->arch_lock);	\
		spin_debug({	tmp_spin->arch_lock.s.owner_cpu = current_cpu_id() & 0xffffu;	});	\
	} while (0)
#else
#define spin_lock(spin)	do {	\
		spin_t *tmp_spin = (spin);	\
		(void)(tmp_spin)->unused;	\
	} while (0)
#endif

/** try to lock spin lock, returns TRUE on success */
#ifdef SMP
#define spin_trylock(spin)	({	\
		spin_t *tmp_spin = (spin);	\
		int tmp_success;	\
		assert(tmp_spin->arch_lock.s.owner_cpu != current_cpu_id());	\
		success = arch_spin_trylock(&tmp_spin->arch_lock);	\
		arch_spin_lock(&tmp_spin->arch_lock);	\
		spin_debug({	if (success) {	tmp_spin->arch_lock.s.owner_cpu = current_cpu_id() & 0xffffu;	}	});	\
		tmp_success;	\
	})
#else
#define spin_trylock(spin)	({	\
		spin_t *tmp_spin = (spin);	\
		(void)(tmp_spin)->unused;	\
		1;	\
	})
#endif

/** unlock spin lock */
#ifdef SMP
#define spin_unlock(spin)	do {	\
		spin_t *tmp_spin = (spin);	\
		assert(tmp_spin->arch_lock.s.owner_cpu == current_cpu_id());	\
		spin_debug({	tmp_spin->arch_lock.s.owner_cpu = ARCH_SPIN_INVALID_OWNER_CPU;	});	\
		arch_spin_unlock(&tmp_spin->arch_lock);	\
	} while (0)
#else
#define spin_unlock(spin)	do {	\
		spin_t *tmp_spin = (spin);	\
		(void)(tmp_spin)->unused;	\
	} while (0)
#endif

/* test if spin lock is contended (for preemption on SMP) */
static inline int spin_contended(spin_t *spin __unused)
{
#ifdef SMP
	unsigned int ticket, served;

#ifndef NDEBUG
	assert(spin->arch_lock.s.owner_cpu == current_cpu_id());
#endif

	/* ticket can be modified by other CPUs, but served is owned by us */
	ticket = access_once(spin->arch_lock.s.ticket);
	served = spin->arch_lock.s.served;

	return ticket != ((served + 1) & 0xffu);
#else
	return 0;
#endif
}

/** test if spin lock is locked (for debugging) */
static inline int spin_locked(spin_t *spin __unused)
{
#ifdef SMP
#ifndef NDEBUG
	return (spin->arch_lock.s.owner_cpu == current_cpu_id());
#else
	/* no better default here */
	return 1;
#endif
#else
	/* no better default on SMP */
	return 1;
#endif
}

#endif
