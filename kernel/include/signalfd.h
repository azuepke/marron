/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * signalfd.h
 *
 * Signalfd implementation
 *
 * azuepke, 2025-02-14: initial
 */

#ifndef SIGNALFD_H_
#define SIGNALFD_H_

#include <marron/error.h>

/* forward declaration */
struct process;
struct thread;
struct fd;

/** notify all signalfds (must be called with process locked) */
void signalfd_notify_all(struct process *proc, unsigned int sig);

/** notify a thread waiting on a signal fd (called with sched_lock) */
void signalfd_notify_thread(struct thread *thr);

/** close signalfd file descriptor */
void signalfd_close(struct fd *fd);

/** dispatcher for IPC-based I/O requests */
err_t signalfd_ipc_call(struct fd *fd);

#endif
