/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2018 Alexander Zuepke */
/*
 * panic.h
 *
 * Kernel panic.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-03-05: moved from kernel.h
 */

#ifndef PANIC_H_
#define PANIC_H_

#include <marron/compiler.h>

/* forward declarations */
struct regs;

/** kernel panic! */
void panic(const char *format, ...) __noreturn __printflike(1, 2) __cold;
void panic_regs(struct regs *regs, unsigned int status, unsigned long addr, const char *format, ...) __noreturn __printflike(4, 5) __cold;

#endif
