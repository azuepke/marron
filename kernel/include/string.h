/* SPDX-License-Identifier: MIT */
/* Copyright 2013 Alexander Zuepke */
/*
 * string.h
 *
 * String handling functions (subset for kernel).
 *
 * azuepke, 2013-03-22
 */

#ifndef STRING_H_
#define STRING_H_

#include <stdint.h>

/* memset.c */
/** simple byte-wise memset() */
void *memset(void *s, int c, size_t n);

/* strlen.c */
/** simple strlen() */
size_t strlen(const char *s);

/* strcmp.c */
/** simple strcmp() */
int strcmp(const char *s1, const char *s2);

#endif
