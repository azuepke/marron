/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * process.h
 *
 * Processes.
 *
 * azuepke, 2025-02-22: initial
 */

#ifndef PROCESS_H_
#define PROCESS_H_

#include <process_types.h>
#include <assert.h>
#include <spin.h>

/* API */
/** initialize all process objects at boot time  */
void process_init_all(void);

/** lookup process in partition (locks part_lock) */
struct process *process_lookup(struct part *part, ulong_t proc_id);

/** create idle process at boot time */
struct process *process_create_idle(struct part *part);

/** create a new process (called unlocked) */
struct process *process_create(struct part *part);

/** start first thread in new process with config (called unlocked) */
struct thread *process_create_first_thread(struct process *proc, const struct process_cfg *proc_cfg);

/** kill a process and all its threads */
void process_destroy(struct process *proc);

/** raise a process error (always to currently running process) */
void process_error(struct process *proc, unsigned int sig);


static inline void process_lock(struct process *proc)
{
	spin_lock(&proc->lock);
}

static inline void process_unlock(struct process *proc)
{
	spin_unlock(&proc->lock);
}

#define assert_process_locked(proc) assert(spin_locked(&(proc)->lock))

#endif
