/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024, 2025 Alexander Zuepke */
/*
 * fd_types.h
 *
 * File descriptors.
 *
 * In the kernel, the term "file descriptor" can mean two things:
 * First and foremost, a file descriptor is an object of "struct fd"
 * that we keep in an array of file descriptors associated with a process
 * (like any other data objects in the kernel).
 * Second, it is an integer value to an entry in an indirection table
 * so we can index the file descriptors via "int fd" in the user space API.
 * The indirection table is named "fd_table" and is just an array of pointers
 * into the first array of file descriptors. Note that both arrays
 * can have a different size: usually, there are more entries in the fd_table
 * than actual file descriptors to support dup() and friends.
 * Either way, we access file descriptors by their object or user space index,
 * so the distinction should be clear.
 *
 * For both arrays, we track the free/used state in bitmaps.
 * We additionally track the CLOEXEC state of the fd index in a second bitmap.
 * Note that CLOEXEC is an attribute of the fd index, not the file descriptor.
 *
 * All starting points for traversal are the partition and process descriptors:
 * - part.fd_freeqh
 * - proc.fd_table[]
 * - proc.fd_table_used_bitmap[]
 * - proc.fd_table_cloexec_bitmap[]
 *
 * Locking: process_lock protects the consistency of the arrays and bitmaps.
 * The actual file descriptor objects come with their own lock, the "fd_lock".
 *
 * azuepke, 2022-07-02: initial
 * azuepke, 2022-07-17: IPC types
 * azuepke, 2024-12-08: epoll
 * azuepke, 2024-12-29: eventfd
 * azuepke, 2025-01-06: irqfd
 * azuepke, 2025-02-14: signalfd
 * azuepke, 2025-02-21: rework fd lookup
 */

#ifndef FD_TYPES_H_
#define FD_TYPES_H_

#include <marron/types.h>
#include <marron/list.h>
#include <spin_types.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>


#define FD_FROM_SIGNALFDQ(n)	list_entry((n), struct fd, signalfd.signalfdql)
#define FD_FROM_FREEQ(n)		list_entry((n), struct fd, dead.fd_freeql)

/* number of entries in file descriptor-related bitmaps */
#define FD_BITMAP_BITS_PER_WORD (8*(int)sizeof(unsigned int))
#define FD_BITMAP_WORDS(num) (((num) + FD_BITMAP_BITS_PER_WORD - 1u) / FD_BITMAP_BITS_PER_WORD)

/* file-descriptor types */
#define FD_TYPE_DEAD		0	/**< a dead file descriptor. just close it */
#define FD_TYPE_IPC_CLIENT	1	/**< client-side end of an IPC channel */
#define FD_TYPE_IPC_SERVER	2	/**< server-side end of an IPC channel */
#define FD_TYPE_MEMRQ		3	/**< memory requirement */
#define FD_TYPE_EPOLL		4	/**< epoll instance */
#define FD_TYPE_EVENTFD		5	/**< eventfd instance */
#define FD_TYPE_TIMERFD		6	/**< timerfd */
#define FD_TYPE_IRQFD		7	/**< irqfd */
#define FD_TYPE_SIGNALFD	8	/**< signalfd */
#define FD_TYPE_LAST		FD_TYPE_SIGNALFD

#define FD_FLAG_STATIC_FD	0x01	/**< fd is a static file descriptor */
#define FD_FLAG_SHAREABLE	0x02	/**< fd is shareable in IPC (IPC, memrq, eventfd, irqfd) */

#define FD_STATIC	0x40000000	/**< marker for static fd IDs */

/** file-descriptor objects */
struct fd {
	/** the fd_lock */
	spin_t lock;

	/** usage / lifetime counter */
	/* NOTE: SMP: protected by fd_lock */
	uint16_t usage;

	/** associated partition ID (const after init) */
	uint8_t part_id;

	/** fd type */
	/* NOTE: SMP: protected by fd_lock */
	uint8_t type;

	/** cached pending events for epoll
	 * (set by epoll_notify(), uses EPOLLNVAL to opt-out of epoll)
	 */
	/* NOTE: SMP: changes protected by fd_lock, but read locklessly by epoll */
	uint16_t cached_eventmask;

	/** private field (for whatever reason, otherwise padding) */
	/* NOTE: SMP: protected by fd_lock */
	uint8_t priv;

	/** fd flags */
	/* NOTE: SMP: protected by fd_lock */
	uint8_t flags;

	/** open flags from fcntl.h */
	/* NOTE: SMP: protected by fd_lock */
	uint16_t oflags;

	/** open flags the user can change */
	/* NOTE: SMP: protected by fd_lock, but const after initialization of fd */
	uint16_t oflags_mask;

	/** list of epoll notes associated to this file */
	/* NOTE: SMP: protected by fd_lock */
	list_t epoll_fdqh;

	/* --- the remainder of this structure depends on the actual type --- */
	union {
		struct {
			/** fd free queue node */
			list_t fd_freeql;
		} dead;
		struct {
			/** server-side IPC */
			/* NOTE: SMP: protected by fd_lock */
			struct fd *server_fd;
		} ipc_client;
		struct {
			/** client-side IPC */
			/* NOTE: SMP: protected by fd_lock */
			struct fd *client_fd;

			/** list of client threads waiting to call */
			/* NOTE: SMP: protected by fd_lock */
			list_t ipc_callqh;

			/** list of client threads waiting for reply */
			/* NOTE: SMP: protected by fd_lock */
			list_t ipc_replyqh;

			/** list of server threads waiting for incoming RPC (wait) */
			/* NOTE: SMP: protected by fd_lock */
			list_t ipc_serverqh;
		} ipc_server;
		struct {
			/** current file offset (if used) */
			/* NOTE: SMP: protected by fd_lock */
			int64_t offset;	/* off_t */

			/** file length (if used) */
			/* NOTE: SMP: protected by fd_lock */
			int64_t length;	/* off_t */
			union {
				const struct memrq_cfg *memrq_cfg;
			};
		} file;
		struct {
			/** list of all epoll notes */
			/* NOTE: SMP: protected by fd_lock */
			list_t epoll_noteqh;

			/** the epoll_waitq_lock */
			spin_t epoll_waitq_lock;

			/** epoll wait generation, incremented by each sys_epoll_wait() */
			/* NOTE: SMP: protected by epoll_waitq_lock */
			uint32_t epoll_wait_generation;

			/** list of threads waiting on this epoll instance */
			/* NOTE: SMP: protected by epoll_waitq_lock */
			list_t epoll_waitqh;

			/** list of pending epoll notes */
			/* NOTE: SMP: protected by epoll_waitq_lock */
			list_t epoll_pendingqh;
		} epoll;
		struct {
			/** current value */
			/* NOTE: SMP: protected by fd_lock */
			uint64_t value;

			/** list of threads waiting on this eventfd for reading */
			/* NOTE: SMP: protected by fd_lock */
			list_t eventfd_reader_waitqh;

			/** list of threads waiting on this eventfd for writing */
			/* NOTE: SMP: protected by fd_lock */
			list_t eventfd_writer_waitqh;
		} eventfd;
		struct {
			/** pointer to timer object */
			/* NOTE: SMP: protected by fd_lock */
			struct timer *timer;
		} timerfd;
		struct {
			/** pointer to IRQ object */
			/* NOTE: SMP: protected by fd_lock */
			struct irq *irq;
		} irqfd;
		struct {
			/** current value */
			/* NOTE: SMP: protected by fd_lock */
			sys_sig_mask_t mask;

			/** associated process */
			/* NOTE: SMP: protected by fd_lock */
			struct process *process;

			/** list of threads waiting on this signalfd */
			/* NOTE: SMP: protected by fd_lock */
			list_t signalfd_waitqh;

			/** queue node in process */
			/* NOTE: SMP: protected by process_lock */
			list_t signalfdql;
		} signalfd;
	};
} __aligned(ARCH_ALIGN);

#endif
