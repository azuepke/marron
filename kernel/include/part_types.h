/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2020, 2021, 2024, 2025 Alexander Zuepke */
/*
 * part_types.h
 *
 * Partition data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 * azuepke, 2025-02-21: rework fd lookup
 * azuepke, 2025-02-22: processes
 */

#ifndef PART_TYPES_H_
#define PART_TYPES_H_

#include <kmempool_types.h>
#include <marron/types.h>
#include <marron/list.h>
#include <spin_types.h>
#include <marron/arch_defs.h>
#include <sig_types.h>
#include <marron/compiler.h>
#include <fd_types.h>

/* partition permissions */
#define PART_PERM_SHUTDOWN		0x1	/* sys_bsp_shutdown() allowed */
#define PART_PERM_PART_OTHER	0x2	/* sys_part_*_other() allowed */
#define PART_PERM_SET_TIME		0x4	/* sys_time_set() allowed */

/** Partition configuration data */
struct part_cfg {
	/** pointer to runtime data */
	struct part *part;

	/** Partition name */
	const char *name;

	/** Available CPUs */
	unsigned long cpu_mask;
	/** Partition ID */
	uint8_t part_id;
	/** The partition's initial state (PART_STATE_xxx) */
	uint8_t initial_state;
	/** Permissions (PART_PERM_xxx) */
	uint8_t perm;
	/** Maximum priority */
	uint8_t max_prio;

	/** Number of memory requirements */
	uint8_t memrq_num;
	uint8_t io_map_num;
	uint8_t shm_map_num;
	uint8_t file_map_num;
	/** Number of assigned IRQ configs */
	uint8_t irq_num;

	/** Number of assigned ipc_server objects */
	uint8_t ipc_server_num;
	/** Number of assigned ipc_client configs */
	uint8_t ipc_client_num;
	/** Number of assigned KLDD configs */
	uint8_t kldd_num;

	/** Number of assigned timers */
	uint8_t timer_num;

	uint8_t padding[3];

	/** Number of threads */
	uint16_t thread_num;
	/** Number of assigned signal queue nodes */
	uint16_t sig_queue_num;

	/** Number of assigned file descriptors */
	uint16_t fd_num;
	/** Size of static file descriptors access table */
	uint16_t fd_static_table_num;
	/** Number of epoll_note objects */
	uint16_t epoll_note_num;

	/** Number of processes */
	uint16_t process_num;
	/** Number of process configs */
	uint16_t process_cfg_num;

	uint16_t padding2[1];

	/** Number of assigned page tables */
	uint32_t kmem_pagetables_num;

	/** Number of assigned VMAs */
	uint32_t vma_num;

	/** Array of memory requirements */
	const struct memrq_cfg *memrq_cfg;
	const struct memrq_cfg *io_map_cfg;
	const struct memrq_cfg *shm_map_cfg;
	const struct memrq_cfg *file_map_cfg;

	/* various memory requirements to allocate memory from */
	const struct memrq_cfg *elf_memrq;
	const struct memrq_cfg *heap_memrq;
	const struct memrq_cfg *stack_memrq;
	const struct memrq_cfg *anon_mmap_memrq;

	/** Array of available threads (for all processes) */
	struct thread *thread;
	/** Array of IRQ config data */
	const struct irq_cfg *irq_cfg;
	/** Array of ipc_server configs */
	const struct ipc_server_cfg *ipc_server_cfg;
	/** Array of ipc_client configs */
	const struct ipc_client_cfg *ipc_client_cfg;
	/** Array of KLDD config data */
	const struct kldd_cfg *kldd_cfg;
	/** Array of available signal queue nodes */
	struct sig_queue_node *sig_queue_node;
	/** Array of available timers */
	struct timer *timer;

	/** Array of available file descriptors */
	struct fd *fd;
	/** Static file descriptor access table (array of pointers to file descriptors) */
	struct fd *const *fd_static_table;

	/** Array of available epoll_note objects */
	struct epoll_note *epoll_note;

	/** Start of memory of kmem page tables */
	void *kmem_pagetables_mem;

	/** Array of available VMAs */
	struct vma *vma;

	/** Array of available processes */
	struct process *process;
	/** Array of process_cfg configs */
	const struct process_cfg *process_cfg;

	/** Processes access table (array of pointers to processes) */
	struct process **proc_table;
};

/** Partition object */
struct part {
	/** pointer to configuration data */
	const struct part_cfg *part_cfg;

	/** the part_lock */
	spin_t lock;

	/** Cached Partition ID (const after init) */
	uint8_t part_id;

	/** Partition state */
	uint8_t state;
	uint8_t padding[2];

	/** signal queue nodes free list head */
	list_t free_sigqh;

	/** List of free epoll notes */
	list_t epoll_note_freeqh;

	/** List of free timer objects */
	list_t timer_freeqh;

	/** List of free thread objects */
	list_t thread_freeqh;

	/** List of free fd objects */
	list_t fd_freeqh;

	/** the kmem_pagetables_lock */
	spin_t kmem_pagetables_lock;
	unsigned int padding2[1];

	/** KMEM page pool for pagetables */
	struct kmempool kmem_pagetables;

	/** List of free process objects */
	list_t process_freeqh;

	/** List of active process objects */
	list_t process_activeqh;
} __aligned(ARCH_ALIGN);

/* generated data */
extern const uint8_t part_num;
extern struct part part_dyn[];
extern const struct part_cfg part_cfg[];

#endif
