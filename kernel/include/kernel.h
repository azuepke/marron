/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2018, 2020, 2021, 2025 Alexander Zuepke */
/*
 * kernel.h
 *
 * Kernel common functions.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-03-09: more documentation
 * azuepke, 2025-02-22: split into multiple headers
 */

#ifndef KERNEL_H_
#define KERNEL_H_

#include <marron/types.h>
#include <stdint.h>
/* we also make printk() available everywhere */
#include <printk.h>

/* forward declaration */
struct regs;

/* buildid.c */
/** Kernel build ID string
 *
 * The build ID string is printed by the kernel at boot time.
 */
extern const char kernel_buildid[];

/* main.c */
/** Kernel C entry point
 *
 * The architecture layer calls the kernel entry point on each CPU.
 */
void kernel_main(unsigned int cpu_id);

/** Handling of deferred system calls
 *
 * This function calls (and clears) any deferred system call handlers.
 * Some system calls need a consistent register state of a thread,
 * e.g. to send a signal, but cannot run directly in the system call function,
 * as the system call function has an inconsistent or incomplete return code.
 * For these calls, the kernel offers a deferred system call mechanism
 * that consists of a second callback immediately after the actual system call.
 */
void kernel_deferred_syscall(void);

/** Scheduling function of the kernel
 *
 * This function schedules to another thread, if necessary,
 * and returns the next register context to return to.
 * The architecture layer calls this function on at kernel exit
 * from system calls, interrupts, and exceptions.
 */
struct regs *kernel_schedule(void);

/** Kernel timer callback
 *
 * The BSP calls this functions with the current system time when a timer
 * interrupt happens. The kernel reprograms the next timer expiry, if needed.
 * Also, the timer interrupt should never be masked during interrupt handling.
 *
 * This function must only be called during interrupt handling.
 */
void kernel_timer(uint64_t time);

/** Kernel IRQ callback
 *
 * When an interrupt happens, the BSP calls this function to notify the kernel
 * to wake waiting threads. The parameter "irq_id" refers to the interrupt ID.
 * The kernel should then in turn wake waiting threads, for example.
 * When the BSP calls kernel_irq(), the interrupt source must be masked.
 * The kernel will unmask the interrupt source again when the woken thread
 * starts waiting for the next interrupt.
 *
 * This function must only be called during interrupt handling.
 */
void kernel_irq(unsigned int irq_id);

#ifdef SMP
/** Kernel SMP rescheduling callback
 *
 * The BSP calls this function when the current CPU received
 * an IPI (inter-processor interrupt) requesting scheduling on a remote CPU.
 * Also, the IPI interrupt should never be masked during interrupt handling.
 *
 * This function must only be called during interrupt handling,
 * and this function is only necessary for multiprocessor systems.
 */
void kernel_ipi(void);
#endif

/** Kernel exception entry point
 *
 * The architecture layer calls this function to notify the kernel about
 * a synchronous exception "sig" in the current thread.
 * The parameters "si_code", "ex_info", "status", and "addr"
 * refer to exception information.
 */
void kernel_exception(
	struct regs *regs,
	unsigned int sig,
	unsigned int si_code,
	unsigned int ex_info,
	unsigned int status,
	unsigned long addr);

/** print error header */
void kernel_print_current(void);

#endif
