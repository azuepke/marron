/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2020, 2021, 2025 Alexander Zuepke */
/*
 * part.h
 *
 * Partitions.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 * azuepke, 2025-02-22: processes
 */

#ifndef PART_H_
#define PART_H_

#include <part_types.h>
#include <spin.h>

/* API */
/** initialize all partition objects at boot time */
void part_init_all(void);

/** start a partition and its processes */
void part_start(struct part *part);

/** stop a partition and its processes */
void part_stop(struct part *part);

/** restart partition */
void part_restart(struct part *part);

/** raise a partition error (always to currently running partition) */
void part_error(struct part *part, unsigned int sig);


static inline void part_lock(struct part *part)
{
	spin_lock(&part->lock);
}

static inline void part_unlock(struct part *part)
{
	spin_unlock(&part->lock);
}

#define assert_part_locked(part) assert(spin_locked(&(part)->lock))

#endif
