/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2017, 2018, 2020, 2021, 2025 Alexander Zuepke */
/*
 * elf.h
 *
 * ELF loader
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-03-09: more documentation
 * azuepke, 2025-02-22: split from kernel.h
 */

#ifndef ELF_H_
#define ELF_H_

#include <marron/types.h>
#include <stdint.h>

/* forward declaration */
struct memrq_cfg;
struct process;

/* elf.c */
/** load the (correct and trusted) ELF file behind "blob" in RAM */
addr_t elf_load(struct process *proc, const struct memrq_cfg *memrq, const void *file);
/** prepare the user space heap like an ELF segment */
void elf_heap(struct process *proc, const struct memrq_cfg *memrq, addr_t heap_base, size_t heap_size);
/** prepare the user space stack like an ELF segment */
void elf_stack(struct process *proc, const struct memrq_cfg *memrq, addr_t stack_base, size_t stack_size);

#endif
