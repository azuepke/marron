/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll.h
 *
 * Epoll implementation
 *
 * azuepke, 2024-12-08: initial
 */

#ifndef EPOLL_H_
#define EPOLL_H_

#include <epoll_types.h>
#include <stdint.h>

/* forward declaration */
struct fd;

/** update event notifications in a specific file (fd must be locked) */
void epoll_notify(
	struct fd *fd,
	uint32_t eventmask);

/** close epoll instance file descriptor */
void epoll_close(
	struct fd *epfd);

/** close file descriptor and cleanup epoll notes */
void epoll_cleanup_notes_on_close(
	struct fd *fd);

#endif
