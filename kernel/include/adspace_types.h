/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2021, 2025 Alexander Zuepke */
/*
 * adspace_types.h
 *
 * Adspace configuration data.
 *
 * azuepke, 2018-03-05: initial
 * azuepke, 2021-04-28: dynamic adspace structure
 * azuepke, 2021-09-22: VMA concept
 * azuepke, 2025-02-21: adspace rework
 */

#ifndef ADSPACE_TYPES_H_
#define ADSPACE_TYPES_H_

#include <spin_types.h>
#include <marron/types.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>
#include <marron/rbtree.h>
#include <marron/list.h>


/** Adspace object */
struct adspace {
	/** pointer to partition (const after init) */
	struct part *part;

	/** the adspace_lock */
	spin_t lock;

	/** address space ID (const after init) */
	uint32_t asid;

	/** mask of CPUs where the adspace is current active (atomic)
	 *
	 * NOTE: only used by architectures that need TLB shootdown in software
	 */
	ulong_t cpu_active_mask;

	/** mask of CPUs which need TLB invalidation on next switch (atomic)
	 *
	 * NOTE: only used by architectures that need TLB shootdown in software
	 */
	ulong_t cpu_inval_mask;

	/** pointer to root page table (const after init, but might be set to NULL page table) */
	void *pagetable;

	/** root of VMA tree */
	rbtree_root_t vma_tree_root;

	/** head of VMA list (helps augmentation) (const after init)
	 *
	 * NOTE: this always points to the NULL guard element
	 */
	struct vma *vma_first;

	/** end address of last allocated VMA that used a NULL address hint */
	addr_t hint_addr;

	/** head of VMA free list (using vma::vma_free_list_node) */
	list_t vma_free_list_head;
} __aligned(ARCH_ALIGN);


/* generated data */
extern const uint16_t adspace_num;
extern struct adspace adspace_dyn[];

#endif
