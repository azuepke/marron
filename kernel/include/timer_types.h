/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022, 2024, 2025 Alexander Zuepke */
/*
 * timer_types.h
 *
 * Timer data types.
 *
 * azuepke, 2021-08-13: initial
 * azuepke, 2022-10-19: timerfd
 * azuepke, 2024-12-31: epoll for timerfd
 * azuepke, 2025-01-05: merge timerfd into timer
 */

#ifndef TIMER_TYPES_H_
#define TIMER_TYPES_H_

#include <marron/types.h>
#include <sig_types.h>
#include <stdint.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>


/* timer states */
#define TIMER_STATE_DISABLED	0	/**< timer disabled */
#define TIMER_STATE_SIGNAL		1	/**< timer enabled, signal mode */
#define TIMER_STATE_WAIT		2	/**< timer enabled, waiting mode */
#define TIMER_STATE_CLOSED		3	/**< timer enabled, fd already closed */

/** get timer from free queue */
#define TIMER_FROM_TIMER_FREEQ(t)	list_entry((t), struct timer, timer_freeql)
/** get timer from active queue */
#define TIMER_FROM_TIMER_ACTIVEQ(t)	list_entry((t), struct timer, timer_activeql)

/** get timer object from timer queue node */
#define TIMER_FROM_TIMERQ(t)		list_entry((t), struct timer, timerql)

/** timer object */
struct timer {
	/* a timer object is free (linked in freeq) or in use (not linked) */
	union {
		/** list node of free timer objects */
		/* NOTE: SMP: protected by part_lock */
		list_t timer_freeql;
		/** list node of active timer objects */
		/* NOTE: SMP: protected by process_lock */
		list_t timer_activeql;
	};
	/** associated signal queue node (with embedded signal information):
	 * - sigqn.info.status is the overrun counter
	 */
	/* NOTE: SMP: protected by process_lock */
	struct sig_queue_node sigqn;
	/** associated process (const after init) */
	struct process *process;
	/** associated scheduling context and CPU */
	/* NOTE: SMP: protected by process_lock; set during timer initialization */
	struct sched *sched;
	/** associated fd */
	/* NOTE: SMP: protected by process_lock; set during timer initialization */
	struct fd *fd;

	/** timer queue link (either timerqh_monotonic or timerqh_realtime)
	 *
	 * the timer is enqueued on the timerq if next != 0
	 */
	/* NOTE: SMP: protected by sched_lock */
	list_t timerql;

	/** next expiry time */
	/* NOTE: SMP: protected by sched_lock */
	sys_time_t next;
	/** period == reload value (or zero if the timer fires just once) */
	/* NOTE: SMP: protected by sched_lock */
	sys_time_t period;

	/** number of unseen expired timeouts (set if timer starts in the past
	 * or if the timer expires multiple times between two timer interrupts)
	 */
	/* NOTE: SMP: protected by process_lock */
	uint64_t expiry_count;

	/** Timer wait queue head -- list of threads blocked on timer */
	/* NOTE: SMP: protected by process_lock */
	list_t timer_waitqh;

	/** timer state */
	/* NOTE: the state field is the only valid field in free state */
	/* NOTE: SMP: protected by process_lock */
	uint8_t state;
	/** clock ID this timer is based on */
	/* NOTE: SMP: protected by process_lock; set during timer initialization */
	uint8_t clk_id;
	/** if timer signal is enqueued */
	/* NOTE: SMP: protected by process_lock */
	uint8_t sig_enqueued;

	/** associated partition (const after init) */
	uint8_t part_id;

	/** target thread for signals or THR_ID_ALL for process-wide signals */
	/* NOTE: SMP: protected by process_lock; set during timer initialization */
	unsigned int thr_id;
} __aligned(ARCH_ALIGN);

/* generated data */
extern const uint16_t timer_num;
extern struct timer timer_dyn[];

#endif
