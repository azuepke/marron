/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * epoll_types.h
 *
 * Epoll implementation
 *
 * azuepke, 2024-12-08: initial
 * azuepke, 2024-12-24: pending epoll notes data model
 */

#ifndef EPOLL_TYPES_H_
#define EPOLL_TYPES_H_

#include <marron/epoll.h>
#include <marron/types.h>
#include <marron/list.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>


/* NOTE:
 * epoll event flags are kept in the lower 16 bits,
 * while epoll modes are kept in the upper 8 bits.
 * We use bits 16..23 for internal signaling of pending event-triggered events.
 */
#define EPOLLTRIGGERED	(1U<<16)	/**< event triggered */

/** all poll flags that describe events (valid on input) */
#define EPOLL_MASK_EVENTS	\
	(EPOLLIN | EPOLLPRI | EPOLLOUT | EPOLLERR |	\
	 EPOLLHUP | EPOLLNVAL | EPOLLRDNORM | EPOLLRDBAND |	\
	 EPOLLWRNORM | EPOLLWRBAND | EPOLLMSG | EPOLLRDHUP)

/** implicit poll flags (ignored on input, set by output) */
#define EPOLL_MASK_IMPLICIT	\
	(EPOLLERR | EPOLLHUP | EPOLLNVAL)

/** epoll mode flags (epoll only) */
#define EPOLL_MASK_MODES	\
	(EPOLLEXCLUSIVE | EPOLLWAKEUP | EPOLLONESHOT | EPOLLET)


/** poll flags for sys_ipc_notify() */
#define EPOLL_MASK_IPC_NOTIFY_EVENTS	\
	(EPOLLIN | EPOLLPRI | EPOLLOUT | EPOLLERR |	\
	 EPOLLHUP | 0 | EPOLLRDNORM | EPOLLRDBAND |	\
	 EPOLLWRNORM | EPOLLWRBAND | EPOLLMSG | EPOLLRDHUP)

/** poll flags for epoll_ctl(EPOLL_CTL_ADD) */
#define EPOLL_MASK_CTL_ADD_EVENTS	\
	(EPOLLIN | EPOLLPRI | EPOLLOUT | EPOLLERR |	\
	 EPOLLHUP | 0 | EPOLLRDNORM | EPOLLRDBAND |	\
	 EPOLLWRNORM | EPOLLWRBAND | EPOLLMSG | EPOLLRDHUP |	\
	 EPOLLEXCLUSIVE | EPOLLWAKEUP | EPOLLONESHOT | EPOLLET)

/** poll flags for epoll_ctl(EPOLL_CTL_ADD) when EPOLLEXCLUSIVE is set */
#define EPOLL_MASK_CTL_ADD_EXCLUSIVE_EVENTS	\
	(EPOLLIN | 0 | EPOLLOUT | EPOLLERR |	\
	 EPOLLHUP | 0 | 0 | 0 |	\
	 0 | 0 | 0 | 0 |	\
	 EPOLLEXCLUSIVE | EPOLLWAKEUP | 0 | EPOLLET)

/** poll flags for epoll_ctl(EPOLL_CTL_MOD) */
#define EPOLL_MASK_CTL_MOD_EVENTS	\
	(EPOLLIN | EPOLLPRI | EPOLLOUT | EPOLLERR |	\
	 EPOLLHUP | 0 | EPOLLRDNORM | EPOLLRDBAND |	\
	 EPOLLWRNORM | EPOLLWRBAND | EPOLLMSG | EPOLLRDHUP |	\
	 0 | EPOLLWAKEUP | EPOLLONESHOT | EPOLLET)


/** get epoll note object from epoll instance queue (or free queue) node */
#define EPOLL_NOTE_FROM_NOTEQL(s)	\
	list_entry((s), struct epoll_note, epoll_noteql)

/** get epoll note object from epoll fd queue node */
#define EPOLL_NOTE_FROM_FDQL(s)	\
	list_entry((s), struct epoll_note, epoll_fdql)

/** get epoll note object from epoll pending queue node */
#define EPOLL_NOTE_FROM_PENDINGQL(s)	\
	list_entry((s), struct epoll_note, epoll_pendingql)


/** epoll note that links an epoll instance and a file descriptor
 *
 * "fd_index" and "fd" will be used as key by sys_epoll_ctl()
 * to uniquely identify an epoll interest
 */
struct epoll_note {
	/** epoll instance or NULL for an epoll note on the free list */
	/* NOTE: SMP: protected by both epfd->fd_lock and fd->fd_lock,
	 *            or part_lock when in part->epoll_note_freeqh
	 */
	struct fd *epfd;
	/** Queue node in epoll instance queue */
	/* NOTE: SMP: protected by both epfd->fd_lock and fd->fd_lock,
	 *            or part_lock when in part->epoll_note_freeqh
	 */
	list_t epoll_noteql;

	/** associated fd or NULL when the file is closed */
	/* NOTE: SMP: protected by both epfd->fd_lock and fd->fd_lock */
	struct fd *fd;
	/** Queue node in the fd's epoll fd queue */
	/* NOTE: SMP: protected by both epfd->fd_lock and fd->fd_lock */
	list_t epoll_fdql;

	/** fd index used in sys_epoll_ctl(), used as part of the key */
	/* NOTE: SMP: protected by epfd->fd_lock */
	uint32_t fd_index;

	/** eventmask as set by sys_epoll_ctl() */
	/* NOTE: SMP: protected by epfd->fd_lock,
	 * but epoll_notify() sets eventmask_triggered/EPOLLTRIGGERED without lock
	 */
	union {
		uint32_t eventmask;
		struct {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
			uint16_t eventmask_events;
			uint8_t eventmask_triggered;
			uint8_t eventmask_modes;
#else /* __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__ */
			uint8_t eventmask_modes;
			uint8_t eventmask_triggered;
			uint16_t eventmask_events;
#endif
		};
	};

	/** user space data as set by sys_epoll_ctl() */
	/* NOTE: SMP: protected by epfd->fd_lock */
	uint64_t data;

	/** assigned process */
	/* NOTE: SMP: set during first allocation */
	struct part *part;

	/** Node in epfd's list of pending epoll notes */
	/* NOTE: SMP: protected by epfd->epoll_waitq_lock */
	list_t epoll_pendingql;

	/** Boolean indicator if enqueued in pending queue */
	uint32_t is_pending;

	/** Wait generation drawn from epfd when inserting into pending queue */
	uint32_t epoll_wait_generation;
} __aligned(ARCH_ALIGN);


/* generated data */
extern const uint16_t epoll_note_num;
extern struct epoll_note epoll_note[];

#endif
