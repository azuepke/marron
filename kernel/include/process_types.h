/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * process_types.h
 *
 * Process data types.
 *
 * azuepke, 2025-02-22: initial
 */

#ifndef PROCESS_TYPES_H_
#define PROCESS_TYPES_H_

#include <stdint.h>
#include <marron/list.h>
#include <marron/compiler.h>
#include <marron/arch_defs.h>
#include <sig_types.h>
#include <fd_types.h>
#include <part_types.h>


/* process state */
#define PROCESS_STATE_DEAD			0	/**< process is dead / not allocated */
#define PROCESS_STATE_ACTIVE		1	/**< process is active */

#define PROCESS_THREAD_NUM			128	/**< process-wide limit of active threads */
#define PROCESS_FD_NUM				128	/**< process-wide limit of open fds */

/* get process from various queue nodes */
#define PROCESS_FROM_ACTIVEQ(t)		list_entry((t), struct process, process_activeql)
#define PROCESS_FROM_FREEQ(t)		list_entry((t), struct process, process_freeql)

/** Process configuration data */
struct process_cfg {
	/** Process name */
	const char *name;

	/** Pointer to ELF image */
	const unsigned char *elf_image;

	/** Available CPUs */
	unsigned long cpu_mask;

	/** Process ID */
	uint32_t proc_id;

	/** Maximum priority */
	uint8_t max_prio;
	uint8_t padding[3];

	/** base address of the heap */
	addr_t heap_base;
	/** size of the heap */
	size_t heap_size;

	/** base address of the stack */
	addr_t stack_base;
	/** size of the stack */
	size_t stack_size;

	/** initial base address for mmap after ELF loading */
	addr_t anon_mmap_base;
};

/** Process object */
struct process {
	/** pointer to partition (const after init) */
	struct part *part;
	/** pointer to adspace (const after init) */
	struct adspace *adspace;

	/** the process_lock */
	spin_t lock;

	/** Process ID (set at activation time) */
	uint32_t proc_id;

	/** Available CPUs */
	unsigned long cpu_mask;

	/** Process state */
	uint8_t state;

	/** Process is a critical process */
	uint8_t critical_process;

	/** Maximum priority */
	uint8_t max_prio;

	uint8_t padding[5];

	/** list node of process on active or free list of partition */
	/* NOTE: SMP: protected by part_lock */
	union {
		list_t process_activeql;
		list_t process_freeql;
	};

	/** Process name (for debugging) */
	char name[32];

	struct {
		/** Set/queue of pending signals */
		struct sig_queue_root pending;

		/* Process specific attributes (signal and exception handling) */
		/** User registered handlers for signals and exceptions */
		addr_t handler[NUM_SIGS];
		/** Mask of blocked signals while executing a handler */
		sys_sig_mask_t blocked_mask[NUM_SIGS];
		/** Signal flags */
		unsigned int flags[NUM_SIGS];

		/** list of signalfd of this process */
		list_t signalfdqh;
	} sig;

	/** Futex wait queue head -- list of threads blocked on a futex */
	list_t futex_waitqh;

	/** List of active timers */
	list_t timer_activeqh;

	/** List of active thread objects */
	list_t thread_activeqh;

	/** Array of active threads */
	struct thread *thread[PROCESS_THREAD_NUM];

	/** File descriptor access table (array of pointers to file descriptors) */
	struct fd *fd_table[PROCESS_FD_NUM];

	/** Bitmap of used entries in file descriptor access table */
	unsigned int fd_table_used_bitmap[FD_BITMAP_WORDS(PROCESS_FD_NUM)];
	/** Bitmap of cloexec flags of entries of file descriptor access table */
	unsigned int fd_table_cloexec_bitmap[FD_BITMAP_WORDS(PROCESS_FD_NUM)];
} __aligned(ARCH_ALIGN);

/* generated data */
extern const uint16_t process_num;
extern struct process process_dyn[];
extern const struct process_cfg process_cfg[];

#endif
