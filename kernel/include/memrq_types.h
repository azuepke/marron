/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2021 Alexander Zuepke */
/*
 * memrq_types.h
 *
 * Memory requirement data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 * azuepke, 2021-04-30: pool allocations
 * azuepke, 2021-09-17: more sophisticated memory allocations
 */

#ifndef MEMRQ_TYPES_H_
#define MEMRQ_TYPES_H_

#include <marron/types.h>
#include <marron/mapping.h>
#include <marron/list.h>
#include <spin.h>
#include <stdint.h>


/** Memory requirement is eligible to use 1M/2M large pages or block mappings */
#define MEMRQ_FLAGS_LARGE_PAGE	0x01

/** Memory requirement configuration data */
struct memrq_cfg {
	/** pointer to runtime data */
	struct memrq *memrq;
	/** name of the mapping entity */
	const char *name;
	/** physical start address */
	union {
		phys_addr_t phys;

		/* hack to work around missing relocations on 64-bit values in 32-bit */
		struct {
#if (__SIZEOF_LONG__ == 4) && (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
			addr_t phys_hi;
#endif
			addr_t phys_lo;
#if (__SIZEOF_LONG__) == 4 && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
			addr_t phys_hi;
#endif
		};
	};
	/** start address of memory requirement */
	addr_t addr;
	/** size of memory requirement */
	size_t size;
	/** access permissions (PROT_READ|PROT_WRITE|PROT_EXEC) */
	uint8_t prot;
	/** cache attributes (CACHE_TYPE value, 0..7) */
	uint8_t cache;
	/** generic flags field */
	uint8_t flags;
	uint8_t padding[__SIZEOF_LONG__ - 3];
	/** first page in page array */
	struct page *page;
};

/** Memory requirement object */
struct memrq {
	/** list head of free pages (protected by memrq_lock)*/
	list_t freepageqh;
	/** number of free pages (protected by memrq_lock) */
	unsigned int free_pages;
	/** lock protecting memrq and associated pages (the memrq_lock) */
	spin_t lock;
};

/** get page object from free page queue node */
#define PAGE_FROM_FREEPAGEQ(p) list_entry((p), struct page, freepageql)

/** Page object */
struct page {
	/** associated memory pool and freelist (const after init) */
	const struct memrq_cfg *memrq_cfg;
	/** list node of free pages */
	list_t freepageql;
	/** reference count */
	unsigned long refcount;
};

/** Lookup table from <mem> configuration data */
struct mem_cfg {
	/** physical start address of memory */
	phys_addr_t phys;
	/** size of memory */
	size_t size;
	/** first page in page array */
	struct page *page;
};

/* generated data */
extern const uint8_t memrq_num;
extern const uint8_t io_map_num;
extern const uint8_t shm_map_num;
extern const uint8_t file_map_num;
extern const uint8_t mem_num;
extern const struct memrq_cfg memrq_cfg[];
extern const struct memrq_cfg io_map_cfg[];
extern const struct memrq_cfg shm_map_cfg[];
extern const struct memrq_cfg file_map_cfg[];
extern struct memrq memrq_dyn[];
extern const struct mem_cfg mem_cfg[];

#endif
