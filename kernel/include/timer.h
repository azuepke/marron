/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2022, 2025 Alexander Zuepke */
/*
 * timer.h
 *
 * Timers.
 *
 * azuepke, 2021-08-13: initial
 * azuepke, 2022-10-19: timerfd
 * azuepke, 2025-02-22: processes
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <marron/types.h>
#include <timer_types.h>


/* forward declaration */
struct fd;

/* API */
/** disable a timer -- wakes all threads, etc (process_lock) */
void timer_disable(
	struct timer *timer);

/** do timer expiry action (called from scheduler) */
void timer_expiry_action(
	struct timer *timer,
	uint64_t expiry_count);

/** dispatcher for IPC-based I/O requests */
err_t timerfd_ipc_call(struct fd *fd);

/** close callback for timerfd */
void timerfd_close(
	struct fd *fd);

#endif
