/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2020, 2021, 2024, 2025 Alexander Zuepke */
/*
 * thread_types.h
 *
 * Thread data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef THREAD_TYPES_H_
#define THREAD_TYPES_H_

#include <stdint.h>
#include <marron/types.h>
#include <marron/list.h>
#include <arch_context.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>
#include <sig_types.h>

/* thread states */
#define THREAD_STATE_CURRENT		0	/**< thread is currently scheduled (and not on the ready queue) */
#define THREAD_STATE_READY			1	/**< thread is enqueued on the ready queue */
/* blocking states */
#define THREAD_STATE_WAIT_SLEEP		2	/**< thread is sleeping */
#define THREAD_STATE_WAIT_SUSPEND	3	/**< thread is suspended */
#define THREAD_STATE_WAIT_IRQ		4	/**< thread is waiting for IRQ */
#define THREAD_STATE_WAIT_FUTEX		5	/**< thread is waiting on futex */
#define THREAD_STATE_WAIT_SIG		6	/**< thread is waiting for signal */
#define THREAD_STATE_WAIT_TIMER		7	/**< thread is waiting for timer */
#define THREAD_STATE_WAIT_IPC_SEND	8	/**< thread is waiting for IPC send (client) */
#define THREAD_STATE_WAIT_IPC_REPLY	9	/**< thread is waiting for IPC reply (client) */
#define THREAD_STATE_WAIT_IPC_WAIT	10	/**< thread is waiting for IPC wait (server) */
#define THREAD_STATE_WAIT_EPOLL		11	/**< thread is waiting for epoll */
#define THREAD_STATE_WAIT_EVENTFD	12	/**< thread is waiting for eventfd */
#define THREAD_STATE_WAIT_SIGNALFD	13	/**< thread is waiting for signalfd */
#define THREAD_STATE_DEAD			14	/**< thread is dead / uninitialized */

/** get thread from various queue nodes */
#define THR_FROM_ACTIVEQ(t)			list_entry((t), struct thread, thread_activeql)
#define THR_FROM_FREEQ(t)			list_entry((t), struct thread, thread_freeql)
#define THR_FROM_READYQ(t)			list_entry((t), struct thread, readyql)
#define THR_FROM_TIMEOUTQ(t)		list_entry((t), struct thread, timeoutql)
#define THR_FROM_IPC_ANYQ(t)		list_entry((t), struct thread, ipc.ipc_anyql)
#define THR_FROM_SIGNALFD_WAITQ(t)	list_entry((t), struct thread, cont_args.signalfd.signalfd_waitql)
#define THR_FROM_EPOLL_WAITQ(t)		list_entry((t), struct thread, cont_args.epoll.epoll_waitql)
#define THR_FROM_EVENTFD_WAITQ(t)	list_entry((t), struct thread, cont_args.eventfd.eventfd_waitql)
#define THR_FROM_FUTEX_WAITQ(t)		list_entry((t), struct thread, cont_args.futex.futex_waitql)
#define THR_FROM_TIMER_WAITQ(t)		list_entry((t), struct thread, cont_args.timer.timer_waitql)

/** thread object */
struct thread {
	/** pointer to thread's partition data (const after init) */
	struct part *part;
	/** pointer to thread's process (const after init) */
	struct process *process;
	/** pointer to register context (const after init) */
	struct regs *regs;
	/** pointer to scheduling data */
	struct sched *sched;

	/** list node of thread on active or free list */
	/* NOTE: SMP: protected by process_lock */
	union {
		list_t thread_activeql;
		list_t thread_freeql;
	};

	/* Various types of continuations:
	 * - The deferred syscall handler runs immediately after a syscall
	 *   and before any scheduling. We need this for ABI reasons,
	 *   i.e. have a complete register context for some operations.
	 * - The cancellation handler runs in whatever scope a thread is woken
	 *   up asynchronously, e.g. thread deletion or timeout handler.
	 *   It is used as cleanup mechanism while a thread is in a blocking state.
	 * - The continuation handler runs immediately after scheduling.
	 */

	/** pointer to deferred syscall handler */
	void (*syscalldefer)(struct thread *);
	/** pointer to continuation handler */
	void (*continuation)(struct thread *);
	/** pointer to cancellation handler */
	void (*cancellation)(struct thread *, err_t wakeup_code);
	/** private data of continuation handler */
	union {
		/** arbitrary pointer argument */
		void *ptr;

		/** deferred partition stop and restart */
		struct part *part;

		/** deferred process destroy */
		struct process *proc;

		/** deferred CPU migration */
		unsigned int new_cpu;

		/** deferred abort */
		unsigned long abort_caller;

		/** deferred sig_return() */
		struct {
			const struct regs *regs_user;
			sys_sig_mask_t sig_mask;
		} sig_return;

		/** deferred sig_wait() (after waiting) */
		struct {
			struct sys_sig_info *info_user;
		} sig_wait;

		/** deferred timer_wait() (after waiting) */
		struct {
			uint64_t *expiry_count_user;
		} timer_wait;

		/** deferred IPC messge receive (after waiting, IPC call and wait) */
		struct {
			struct sys_ipc *recv_msg_user;
			uint32_t *recv_rhnd_user;
			uint32_t recv_rhnd;
		} ipc_wait;

		struct {
			/** associated epoll instance */
			struct fd *epfd;
			/** epoll structure in user space to report epoll events to */
			struct sys_epoll *event_user;
			/** maximum number of reported epoll events */
			ulong_t max_events;
			/** actual number of reported epoll events */
			uint32_t *num_events_user;
			/** epoll wait queue link */
			list_t epoll_waitql;
		} epoll;

		struct {
			/** receive message after blocking */
			/* NOTE: must be in same slot as ipc_wait.recv_msg_user! */
			struct sys_ipc *recv_msg_user;
			/** associated eventfd instance */
			struct fd *fd;
			/** eventfd wait queue link */
			list_t eventfd_waitql;
			/** indicates a read operation (boolean) */
			ulong_t is_read;
			/** cached value  */
			uint64_t value;
		} eventfd;

		struct {
			/** receive message after blocking */
			/* NOTE: must be in same slot as ipc_wait.recv_msg_user! */
			struct sys_ipc *recv_msg_user;
			/** Timer wait queue node -- node in waiting queue */
			/* NOTE: SMP: protected by part_lock */
			list_t timer_waitql;
			/** number of unseen expired timeouts */
			uint64_t expiry_count;
		} timer;

		struct {
			/** receive message after blocking */
			/* NOTE: must be in same slot as ipc_wait.recv_msg_user! */
			struct sys_ipc *recv_msg_user;
			/** IRQ the thread is currently waiting on */
			/* NOTE: SMP: set before waiting */
			struct irq *irq;
		} irq;

		struct {
			/** Futex the thread is currently waiting on */
			/* NOTE: SMP: protected by process_lock */
			unsigned int *futex_user;
			/** Futex wait queue node -- node in waiting queue */
			/* NOTE: SMP: protected by process_lock */
			list_t futex_waitql;
		} futex;

		struct {
			/** receive message after blocking */
			/* NOTE: must be in same slot as ipc_wait.recv_msg_user! */
			struct sys_ipc *recv_msg_user;
			/** associated signalfd instance */
			struct fd *fd;
			/** signalfd wait queue link */
			list_t signalfd_waitql;
		} signalfd;
	} cont_args;

	/** pending wakeup error code (set under domain-specific lock) */
	uint8_t wakeup_code;	/* actually: err_t */

	/** thread state */
	uint8_t state;

	/** FPU state */
	uint8_t fpu_state;

	/** suspension wakeup flag */
	/* NOTE: SMP: protected by sched_lock */
	uint8_t suspend_wakeup;

	/** global thread ID (const after init) */
	/* NOTE: SMP: const after init */
	uint16_t global_thr_id;

	/** thread ID in process (const after init) */
	/* NOTE: SMP: const after thread becomes active */
	uint16_t thr_id;

	/** current scheduling priority */
	short prio;

	/** Maximum priority (cached from process) */
	uint8_t max_prio;

	/** clock ID used for timeouts */
	uint8_t clk_id;

	uint8_t padding[4];

	/** ready queue link */
	/* NOTE: SMP: protected by sched_lock */
	list_t readyql;
	/** timeout queue link (either timeoutqh_monotonic or timeoutqh_realtime) */
	/* NOTE: SMP: protected by sched_lock */
	list_t timeoutql;

	/** current timeout value if the thread is waiting with a timeout */
	/* NOTE: SMP: protected by sched_lock */
	sys_time_t timeout;

	/** CPU affinity mask */
	unsigned long cpu_mask;

	/** scheduling quantum in nanoseconds, value 0 is an infinite quantum */
	/* NOTE: SMP: protected by sched_lock */
	unsigned int quantum;
	/** remainder of previously assigned quantum (0: replenishment needed) */
	/* NOTE: SMP: private to the current core */
	unsigned int quantum_remain;

	struct {
		/** Set/queue of pending signals */
		struct sig_queue_root pending;

		/** Set of blocked signals */
		sys_sig_mask_t blocked_mask;
		/** Signals the thread is currently waiting for, typically ~sig.blocked_mask */
		sys_sig_mask_t wait_mask;

		/** Signal stack base */
		addr_t stack_base;
		/** Signal stack size */
		size_t stack_size;
	} sig;

	/** TLS area of thread in user space */
	struct sys_tls *user_tls;

	/** statistics: accumulated execution time (updated on scheduling) */
	/* NOTE: SMP: protected by sched_lock */
	sys_time_t stat_exec_time;

	struct {
		/** current message in IPC call */
		struct sys_ipc msg;
		/** message vectors for IPC call */
		struct sys_iov vec[SYS_IOV_MAX];
		/** associated IPC file-descriptor */
		/* NOTE: SMP: protected by fd_lock */
		struct fd *fd;
		/** Wait queue node when waiting on any IPC wait queue */
		/* NOTE: SMP: protected by fd_lock */
		list_t ipc_anyql;
		/** current reply handle */
		/* NOTE: SMP: checked without lock */
		uint32_t rhnd;
		/** reply handle generation (simple counter) */
		uint8_t rhnd_generation;
		/** number of valid vector entries in vec */
		uint8_t iov_num;
		/** flags when waiting for a reply (copied into flags on reply) */
		uint16_t reply_flags;
		/** state when waiting for IPC reply (copied into h64 on reply) */
		union {
			uint64_t h64;
			struct {
				uint32_t fd0;
				uint32_t fd1;
			};
			addr_t map_start;
		} reply;
	} ipc;
} __aligned(ARCH_ALIGN);

/* generated data */
extern const uint16_t thread_num;
extern struct thread thread_dyn[];
extern struct regs thread_regs[];

#endif
