/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2020, 2021 Alexander Zuepke */
/*
 * futex.h
 *
 * Futex implementation
 *
 * azuepke, 2018-03-19: initial
 * azuepke, 2018-04-19: simplified implementation
 */

#ifndef FUTEX_H_
#define FUTEX_H_

#include <marron/types.h>
#include <stdint.h>


/* API */
/** Block current thread on user space locking object */
err_t futex_wait(
	unsigned int *futex_user,
	unsigned int compare,
	unsigned int mask,
	unsigned int flags,
	sys_timeout_t timeout,
	unsigned int clk_id_absflag);

/** Wake threads waiting on user space locking object */
err_t futex_wake(
	unsigned int *futex_user,
	unsigned int count);

/** Wake and/or requeue threads waiting on user space locking object */
err_t futex_requeue(
	unsigned int *futex_user,
	unsigned int compare,
	unsigned int mask,
	unsigned int count,
	unsigned int *futex2_user,
	unsigned int count2);

/* Linux-like futex API -- FUTEX_LOCK_PI */
err_t futex_lock(
	unsigned int *futex_user,
	unsigned int flags,
	sys_timeout_t timeout,
	unsigned int clk_id_absflag);

/* Linux-like futex API -- FUTEX_UNLOCK_PI */
err_t futex_unlock(
	unsigned int *futex_user,
	unsigned int flags);

#endif
