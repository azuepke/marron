/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2025 Alexander Zuepke */
/*
 * irq.h
 *
 * IRQ handling.
 *
 * azuepke, 2018-01-13: initial
 * azuepke, 2025-01-06: irqfd
 */

#ifndef IRQ_H_
#define IRQ_H_

#include <irq_types.h>
#include <spin.h>

/* forward declaration */
struct fd;

/* API */
/** get IRQ object for a given IRQ ID */
static inline struct irq *irq_by_id(unsigned long irq_id)
{
	return irq_table[irq_id];
}

/** initialize all IRQ objects at boot time */
void irq_init_all(void);

/** handle an IRQ */
void irq_handle(struct irq *irq);

/** shutdown interrupt at partition shutdown */
void irq_shutdown(struct irq *irq);


static inline void irq_lock(struct irq *irq)
{
	spin_lock(&irq->lock);
}

static inline void irq_unlock(struct irq *irq)
{
	spin_unlock(&irq->lock);
}

#define assert_irq_locked(irq) assert(spin_locked(&(irq)->lock))


/** close callback for irqfd */
void irqfd_close(struct fd *fd);

/** dispatcher for IPC-based I/O requests */
err_t irqfd_ipc_call(struct fd *fd);

#endif
