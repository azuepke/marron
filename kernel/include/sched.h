/* SPDX-License-Identifier: MIT */
/* Copyright 2017-2021 Alexander Zuepke */
/*
 * sched.h
 *
 * Scheduler
 *
 * azuepke, 2017-12-31: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef SCHED_H_
#define SCHED_H_

#include <sched_types.h>
#include <assert.h>
#include <arch.h>
#include <spin.h>

/* forward declarations */
struct timer;


/* API */
/** get scheduler data for a given CPU */
static inline struct sched *sched_per_cpu(unsigned int cpu_id)
{
	assert(cpu_id < sched_num);
	return &sched_dyn[cpu_id];
}

/** initialize per-CPU scheduling data, assuming only the idle thread is active */
void sched_init(struct sched *sched, struct thread *idle, unsigned int cpu);

/** find and remove highest thread on ready queue */
void sched_readyq_next(struct sched *sched);

/** replenish scheduling quantum for thread */
void sched_replenish_quantum(sys_time_t now, struct sched *sched);

/** preempt or yield the current thread */
void sched_preempt_or_yield(sys_time_t now, struct sched *sched);

/** check if we need to preempt the current thread */
void sched_check_preempt(struct sched *sched);

/** current thread waits, with state, timeout and error code already set */
void sched_wait(struct sched *sched, struct thread *thr);

/** remove a thread from the ready queue */
void sched_remove(struct sched *sched, struct thread *thr);

/** release the timeout of a blocked thread */
void sched_timeout_release(struct sched *sched, struct thread *thr);

/** wake a thread and schedule */
void sched_wakeup(struct sched *sched, struct thread *thr);

/** start timer */
void sched_timer_start(struct sched *sched, struct timer *timer);

/** stop timer */
void sched_timer_stop(struct sched *sched, struct timer *timer);

/** Scheduler timer interrupt callback */
void sched_timer(struct sched *sched, uint64_t now);

/** yield current thread */
void sched_yield(struct sched *sched, struct thread *thr);

/** change scheduling parameters of any thread */
void sched_prio_quantum_change(struct sched *sched, struct thread *thr, int new_prio, unsigned int quantum);

/** migrate current thread to a new CPU */
void sched_migrate(struct sched *sched, struct thread *thr);

static inline void sched_lock(struct sched *sched)
{
	spin_lock(&sched->lock);
}

static inline void sched_unlock(struct sched *sched)
{
	spin_unlock(&sched->lock);
}

#define assert_sched_locked(sched) assert(spin_locked(&(sched)->lock))

#endif
