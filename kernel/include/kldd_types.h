/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * kldd_types.h
 *
 * Kernel-level device drivers (KLDD) data types.
 *
 * KLDDs are callbacks into kernel-level device drivers of type kldd_handler_t.
 * They are registered in hardware.xml and reference the handler function, e.g.
 *
 *   err_t kldd_foo(struct kldd_priv *priv,
 *                  unsigned long arg1, unsigned long arg2,
 *                  unsigned long arg3, unsigned long arg4);
 *
 * is referenced by the global KLDD name "foo":
 *
 *   <kldd name="foo" handler="kldd_foo"/>
 *
 * A permission to call a KLDD (e.g. "foo") under a local name (e.g. "foobar")
 * is granted to each partition as follows:
 *
 *   <kldd_call name="foobar" kldd="foo"/>
 *
 * Each call grant creates a private instance of the "priv" parameter,
 * with "priv.data" being initially zero.
 *
 * If a KLDD requires global initialization (on CPU 0) at boot time,
 * use the following module-specific initializer of type "kldd_init_global_t":
 *
 *   void foo_init(void)
 *   {
 *     ...
 *   }
 *
 * Similarly, a per-CPU at boot time (called on each CPU) is available as well:
 *
 *   void foo_init_per_cpu(unsigned int cpu_id)
 *   {
 *     ...
 *   }
 *
 * Both functions can be registered in hardware.xml as follows:
 *
 *   <kldd name="foo" ... init="foo_init" init_per_cpu="foo_init_per_cpu"/>
 *
 * Note that global initializers will be called before per-CPU initializers,
 * and the initializers will be called regardless if the KLDD is used or not.
 * Also, drivers are initialized by their order in hardware.xml.
 *
 * azuepke, 2021-05-07: initial
 * azuepke, 2023-08-07: driver init calls
 */

#ifndef KLDD_TYPES_H_
#define KLDD_TYPES_H_

#include <marron/types.h>
#include <stdint.h>


/** KLDD private state */
struct kldd_priv {
	unsigned long data;
};

/** KLDD callback function type */
typedef err_t (*kldd_handler_t)(
	struct kldd_priv *priv,
	unsigned long arg1,
	unsigned long arg2,
	unsigned long arg3,
	unsigned long arg4);

/** KLDD global initializer function type */
typedef void (*kldd_init_global_t)(void);

/** KLDD per-CPU initializer function type */
typedef void (*kldd_init_per_cpu_t)(unsigned int cpu_id);

/** KLDD configuration data */
struct kldd_cfg {
	/** registered handler function */
	kldd_handler_t handler;
	/** related private data */
	struct kldd_priv *priv;
	/** local name of this KLDD */
	const char *name;
};

/* generated data */
extern const uint8_t kldd_num;
extern const struct kldd_cfg kldd_cfg[];
extern struct kldd_priv kldd_priv[];

extern const uint8_t kldd_init_global_num;
extern const kldd_init_global_t kldd_init_global_cfg[];

extern const uint8_t kldd_init_per_cpu_num;
extern const kldd_init_per_cpu_t kldd_init_per_cpu_cfg[];

#endif
