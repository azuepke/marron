/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Alexander Zuepke */
/*
 * current.h
 *
 * Current per-CPU data.
 *
 * azuepke, 2018-03-05: moved current*() getters from kernel.h
 */

#ifndef CURRENT_H_
#define CURRENT_H_

#include <percpu_types.h>
#include <arch_percpu.h>
#include <bsp.h>

/** get current CPU via per-CPU area */
static inline unsigned int current_cpu_id(void)
{
#ifdef SMP
	return arch_percpu()->cpu_id;
#else
	return 0;
#endif
}

/** get current CPU ID as CPU mask */
static inline unsigned long current_cpu_mask(void)
{
	return 1ul << current_cpu_id();
}

/** get current thread */
static inline struct thread *current_thread(void)
{
	return arch_percpu()->current_thread;
}

/** get current process */
static inline struct process *current_process(void)
{
	return arch_percpu()->current_process;
}

/** get current partition */
static inline struct part *current_part(void)
{
	return arch_percpu()->current_part;
}

#endif
