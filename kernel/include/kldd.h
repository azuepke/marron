/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023 Alexander Zuepke */
/*
 * kldd.h
 *
 * Kernel-level device drivers (KLDD) handling.
 *
 * azuepke, 2021-05-07: initial
 * azuepke, 2023-08-07: driver init calls
 */

#ifndef KLDD_H_
#define KLDD_H_

#include <kldd_types.h>

/** initialize the private data of all KLDD drivers at boot time */
void kldd_init_all(void);

/** call registered KLDD drivers' global initializers at boot time */
void kldd_init_global(void);

/** call registered KLDD drivers' per-CPU initializers on the CPU at boot time */
void kldd_init_per_cpu(unsigned int cpu_id);

#endif
