/* SPDX-License-Identifier: MIT */
/* Copyright 2017-2021, 2025 Alexander Zuepke */
/*
 * time.h
 *
 * Time and clock functions.
 *
 * azuepke, 2025-02-22: split clock and sleep functions from main.c and thread.c
 */

#ifndef TIME_H_
#define TIME_H_

#include <marron/types.h>
#include <stdint.h>

/** offsets in nanoseconds of the different clocks */
extern uint64_t clock_realtime_offset[NUM_CLK_IDS];

/** add offset to time/timeout with saturation to TIMEOUT_INFINITE */
static inline sys_time_t time_add(sys_time_t value, sys_time_t offset)
{
	if (value + offset < value) {
		return TIMEOUT_INFINITE;
	}
	return value + offset;
}

/** subtract offset from time/timeout with saturation to TIMEOUT_NULL */
static inline sys_time_t time_sub(sys_time_t value, sys_time_t offset)
{
	if (value - offset > value) {
		return TIMEOUT_NULL;
	}
	return value - offset;
}

#endif
