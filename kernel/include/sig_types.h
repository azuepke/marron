/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2021 Alexander Zuepke */
/*
 * sig_types.h
 *
 * Tracking of queued signals in signal handling
 *
 * azuepke, 2021-08-04: initial
 */

#ifndef SIG_TYPES_H_
#define SIG_TYPES_H_

#include <marron/types.h>
#include <stdint.h>
#include <marron/list.h>


/** get thread object from ready queue node */
#define SIG_FROM_SIGQ(s) list_entry((s), struct sig_queue_node, sigql)

/** Queued signal information object */
struct sig_queue_node {
	/** list node */
	list_t sigql;
	/** signal information */
	struct sys_sig_info info;
	/** associated timer (if any, const after init) */
	struct timer *timer;
};

/** Sig queue object embedded in thread and process */
struct sig_queue_root {
	/** signal queue nodes list head */
	list_t sigqh;
	/** Unqueued pending signals */
	sys_sig_mask_t unqueued_pending_mask;
	/** Cached information on any pending signals.
	 * Union of all signals on the signal queue and the unqueued signals.
	 */
	sys_sig_mask_t cached_pending_mask;
};


/* generated data */
extern const uint16_t sig_queue_num;
extern struct sig_queue_node sig_queue_nodes[];

#endif
