/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Alexander Zuepke */
/*
 * percpu.h
 *
 * Per-CPU data.
 *
 * azuepke, 2018-03-05: moved current*() getters from kernel.h
 */

#ifndef PERCPU_H_
#define PERCPU_H_

#include <percpu_types.h>
#include <arch_percpu.h>
#include <thread_types.h>

/* "per-CPU" API */
/** update current thread, process and partition in per-CPU data */
static inline void percpu_thread_switch(struct thread *thr)
{
	struct percpu *percpu;

	percpu = arch_percpu();
	percpu->current_thread = thr;
	percpu->current_process = thr->process;
	percpu->current_part = thr->part;
}

#endif
