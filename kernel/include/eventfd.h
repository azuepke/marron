/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * eventfd.h
 *
 * Eventfd implementation
 *
 * azuepke, 2024-12-29: initial
 */

#ifndef EVENTFD_H_
#define EVENTFD_H_

#include <marron/error.h>

/* forward declaration */
struct fd;

/** close eventfd file descriptor */
void eventfd_close(
	struct fd *fd);

/** dispatcher for IPC-based I/O requests */
err_t eventfd_ipc_call(
	struct fd *fd);

#endif
