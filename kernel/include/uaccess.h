/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2014, 2021 Alexander Zuepke */
/*
 * uaccess.h
 *
 * Access to user space helpers.
 *
 * azuepke, 2013-05-03: initial
 * azuepke, 2021-04-23: adapted
 */

#ifndef UACCESS_H_
#define UACCESS_H_

#include <marron/arch_defs.h>
#include <arch.h>
#include <marron/compiler.h>
#include <marron/types.h>
#include <marron/error.h>
#include <arch_uaccess.h>
#include <assert.h>

/** check if memory range [x, x+sz) is a valid user space memory range */
#define USER_CHECK_RANGE(x, sz) ({	\
		typeof(x) _x = (x);	\
		typeof(sz) _sz = (sz);	\
		((_sz <= ARCH_USER_END) && (_x <= ARCH_USER_END - _sz));	\
	})

#define user_ptr_ok(ptr) ({	\
		typeof(ptr) _ptr = (ptr);	\
		USER_CHECK_RANGE((addr_t)_ptr, sizeof(*_ptr));	\
	})


/* non-validation variants */
#define user_put_1_nocheck(addr, val)	\
	({	\
		unsigned int __err;	\
		arch_user_put_1((addr), (val), __err);	\
		__err;	\
	})

#define user_put_4_nocheck(addr, val)	\
	({	\
		unsigned int __err;	\
		arch_user_put_4((addr), (val), __err);	\
		__err;	\
	})

#define user_put_8_nocheck(addr, val)	\
	({	\
		unsigned int __err;	\
		arch_user_put_8((addr), (val), __err);	\
		__err;	\
	})


#define user_get_1_nocheck(addr, val)	\
	({	\
		unsigned int __err;	\
		arch_user_get_1((addr), (val), __err);	\
		__err;	\
	})

#define user_get_4_nocheck(addr, val)	\
	({	\
		unsigned int __err;	\
		arch_user_get_4((addr), (val), __err);	\
		__err;	\
	})

#define user_get_8_nocheck(addr, val)	\
	({	\
		unsigned int __err;	\
		arch_user_get_8((addr), (val), __err);	\
		__err;	\
	})

#define user_cas_4_nocheck(addr, old, new, prev)	\
	({	\
		unsigned int __err;	\
		arch_user_cas_4((addr), (old), (new), (prev), __err);	\
		__err;	\
	})


/* address validation variants */
#define user_put_1(addr, val)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(addr), 1ul))) {	\
			arch_user_put_1((addr), (val), __err);	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_put_4(addr, val)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(addr), 4ul))) {	\
			arch_user_put_4((addr), (val), __err);	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_put_8(addr, val)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(addr), 8ul))) {	\
			arch_user_put_8((addr), (val), __err);	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})


#define user_get_1(addr, val)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(addr), 1ul))) {	\
			arch_user_get_1((addr), (val), __err);	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_get_4(addr, val)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(addr), 4ul))) {	\
			arch_user_get_4((addr), (val), __err);	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_get_8(addr, val)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(addr), 8ul))) {	\
			arch_user_get_8((addr), (val), __err);	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_cas_4(addr, old, new, prev)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(addr), 4ul))) {	\
			arch_user_cas_4((addr), (old), (new), (prev), __err);	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})


/* pointer types */
#if __SIZEOF_LONG__ == 8
#define user_get_p_nocheck(addr, val)	user_get_8_nocheck((addr), (val))
#define user_get_p(addr, val)	user_get_8((addr), (val))
#define user_put_p_nocheck(addr, val)	user_put_8_nocheck((addr), (val))
#define user_put_p(addr, val)	user_put_8((addr), (val))
#else
#define user_get_p_nocheck(addr, val)	user_get_4_nocheck((addr), (val))
#define user_get_p(addr, val)	user_get_4((addr), (val))
#define user_put_p_nocheck(addr, val)	user_put_4_nocheck((addr), (val))
#define user_put_p(addr, val)	user_put_4((addr), (val))
#endif


#define user_strlcpy_in(dst, src_user, n)	\
	({	\
		unsigned int __err;	\
		assert(n >= 1);	\
		if (likely(USER_CHECK_RANGE((addr_t)(src_user), (n)))) {	\
			__err = arch_user_strlcpy_in((dst), (src_user), (n));	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_bzero(s_user, n)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(s_user), (n)))) {	\
			__err = arch_user_bzero((s_user), (n));	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_copy_in(dst, src_user, n)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(src_user), (n)))) {	\
			__err = arch_user_memcpy((dst), (src_user), (n));	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_copy_out(dst_user, src, n)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(dst_user), (n)))) {	\
			__err = arch_user_memcpy((dst_user), (src), (n));	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_copy_in_aligned(dst, src_user, n)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(src_user), (n)))) {	\
			__err = user_wordcpy_nocheck((dst), (src_user), (n), alignof(n));	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#define user_copy_out_aligned(dst_user, src, n)	\
	({	\
		unsigned int __err;	\
		if (likely(USER_CHECK_RANGE((addr_t)(dst_user), (n)))) {	\
			__err = user_wordcpy_nocheck((dst_user), (src), (n), alignof(n));	\
		} else {	\
			__err = EINVAL;	\
		}	\
		__err;	\
	})

#endif
