/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * vma.h
 *
 * Virtual memory area handling.
 *
 * azuepke, 2021-09-22: initial
 */

#ifndef VMA_H_
#define VMA_H_

#include <vma_types.h>
#include <marron/arch_defs.h>
#include <assert.h>


/* forward declarations */
struct adspace;


/* API */
/** initialize the VMA-specific part of all adspace objects at boot time */
void vma_init_all(void);

#ifndef NDEBUG
/** built-in self test */
void vma_test_run_once(struct adspace *adspace);
#endif

/** reset/free all VMAs in address space */
void vma_adspace_init(struct adspace *adspace);

/** destroy all VMAs in address space */
void vma_adspace_destroy(struct adspace *adspace);

/** set mmap hint address */
void vma_adspace_set_hint(struct adspace *adspace, addr_t addr);

/** get a VMA's end address */
static inline addr_t vma_end(const struct vma *vma)
{
	addr_t this_end;

	assert(vma != NULL);

	this_end = vma->start + vma->size;
	assert(this_end <= ARCH_USER_END);

	return this_end;
}

/** get a VMA's next neighbor's start address */
static inline addr_t vma_next_start(const struct vma *vma)
{
	addr_t next_start;

	assert(vma != NULL);

	next_start = ARCH_USER_END;
	if (vma->vma_next != NULL) {
		next_start = vma->vma_next->start;
	}

	return next_start;
}

#if 0
/** find the VMA that covers the given address */
struct vma *vma_find_by_addr(struct adspace *adspace, addr_t addr);
#endif

/** find the VMA exactly at or before/left of a given address */
struct vma *vma_find_exact_addr_or_before(struct adspace *adspace, addr_t addr);

/** map (insert) a new VMA at or near the given memory area
 *
 * the allocation strategy follows mmap:
 * - fixed == 0:
 *   the address is taken as a hint
 * - fixed == 1 && overmap == 0:
 *   the address is fixed && the requested memory area must be free
 * - fixed == 1 && overmap == 1:
 *   the address is fixed, any previous VMAs in the area will be cut and overmapped
 *
 * returns new VMA or NULL on failure (out of memory)
 *
 * use case scenario:
 *
 *  struct vma *vma;
 *  addr_t map_addr;
 *  err_t err;
 *
 *  // try to create a new VMA (can fail with ENOMEM)
 *  vma = vma_create(adspace, ...);
 *  if (vma == NULL) {
 *      return ENOMEM;
 *  }
 *
 *  // remember start address of new mapping
 *  map_addr = vma->start;
 *
 *  // set the type of a VMA
 *  vma_set_type(vma, VMA_IO, ...);
 *
 *  // populate mapping (can fail)
 *  err = ...
 *
 *  // finalize mapping (performs cleanup on failure)
 *  err = vma_finalize(adspace, vma, err);
 *  // do not access vma afterwards!
 *  return err;
 *
 */
struct vma *vma_create(struct adspace *adspace, addr_t addr, size_t size, int fixed, int overmap);

/** set type and attributes of VMA */
void vma_set_type(struct vma *vma, unsigned int vma_type, unsigned int prot, unsigned int max_prot, unsigned int cache, const struct memrq_cfg *m, phys_addr_t offset);

/** finalizing mapping of a new VMA
 *
 * called as finishing step after populating a mapping
 *
 * returns the given error code (pass-through)
 */
err_t vma_finalize(struct adspace *adspace, struct vma *vma, err_t err);

/* unmap a single VMA
 *
 * returns:
 * - NULL on an error (out of memory when cutting a VMA in the middle)
 * - vma as left VMA (a new VMA added to the right)
 *
 * note:
 * - the VMA must be related to the hole
 */
struct vma *vma_unmap(struct adspace *adspace, struct vma *vma, addr_t addr, size_t size);

/** change protection of a a single VMA (mprotect())
 *
 * returns:
 * - NULL on an error (out of memory when cutting a VMA in the middle)
 * - vma as left VMA (a new VMA added to the right after splitting)
 *
 * note:
 * - the VMA must be related to the affected area
 */
struct vma *vma_protect(struct adspace *adspace, struct vma *vma, addr_t addr, size_t size, unsigned int prot);

#endif
