/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2021, 2025 Alexander Zuepke */
/*
 * sig.h
 *
 * Signal and exception handling.
 *
 * azuepke, 2018-03-16: initial
 * azuepke, 2021-08-04: queued signals
 * azuepke, 2021-08-06: synchronous waiting for signals
 * azuepke, 2025-02-14: initial
 * azuepke, 2025-02-22: processes
 */

#ifndef SIG_H_
#define SIG_H_

#include <marron/types.h>
#include <stdint.h>

/* forward declaration */
struct sig_queue_root;
struct sig_queue_node;
struct process;
struct thread;
struct regs;

/** immediately raise an exception to the current thread
 * NOTE: the kernel's exception handler and sys_abort() calls this,
 * and exceptions are never set to "pending"
 */
void sig_exception(
	struct thread *thr,
	unsigned int sig,
	struct sys_sig_info *info);

/** if the thread has pending signals, send them now
 * NOTE: the scheduler calls this function at kernel exit
 */
struct regs *sig_send_if_pending(
	struct thread *thr);

/** if the thread has pending signals, retrieve them synchronously now */
err_t sig_poll_if_pending(
	struct thread *thr,
	struct sys_sig_info *info,
	sys_sig_mask_t wait_mask);

/** return from a signal handler */
void sig_return(
	struct thread *thr,
	const struct regs *regs_user,
	sys_sig_mask_t blocked_mask);

/** change the current thread's signal mask */
sys_sig_mask_t sig_mask(
	struct thread *thr,
	sys_sig_mask_t clear_mask,
	sys_sig_mask_t set_mask);

/** send an asynchronous signal to a thread (thr_id!=THR_ID_ALL) or to a process (thr_id==THR_ID_ALL) */
err_t sig_send_sigqn(
	struct process *proc,
	unsigned int thr_id,
	unsigned int sig,
	struct sig_queue_node *sigqn);

/** wait for signals and retrieve signal information */
err_t sig_wait(
	sys_sig_mask_t wait_mask,
	unsigned int flags,
	sys_timeout_t timeout,
	unsigned int clk_id_absflag,
	struct sys_sig_info *info_user);

/** wait for signals and execute signal handler */
err_t sig_suspend(
	sys_sig_mask_t wait_mask);

/** drain thread/process's queued signals (locks process_lock internally) */
void sig_drain(
	struct process *proc,
	struct sig_queue_root *pending);

#endif
