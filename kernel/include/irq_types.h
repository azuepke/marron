/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2025 Alexander Zuepke */
/*
 * irq_types.h
 *
 * IRQ object data types.
 *
 * azuepke, 2018-01-13: initial
 * azuepke, 2025-01-06: irqfd
 */

#ifndef IRQ_TYPES_H_
#define IRQ_TYPES_H_

#include <marron/types.h>
#include <spin_types.h>
#include <stdint.h>
#include <marron/arch_defs.h>
#include <marron/compiler.h>


/** IRQ configuration data */
struct irq_cfg {
	/** local name of this IRQ */
	const char *name;
	/** global IRQ ID */
	unsigned int irq_id;
};

/** IRQ object */
struct irq {
	/** pointer to associated partition (const after init) */
	struct part *part;
	/** waiting thread */
	struct thread *waiter;
	/** associated fd (non-NULL if IRQ is enabled) */
	struct fd *fd;
	/** cached IRQ ID (const after init) */
	unsigned int irq_id;
	/** boolean if the interrupt is currently enabled at BSP-level */
	char enabled;
	/** boolean if the interrupt is enabled by sys_irq_ctl(IRQ_CTL_UNMASK) */
	char ctl_enabled;
	/** boolean if the interrupt has fired at least once */
	char fired;
	/** IRQ's currently assigned CPU ID (cached for paired disable calls) */
	unsigned char cpu_id;
	/** statistic counter */
	unsigned int count;

	/** the irq_lock */
	spin_t lock;
} __aligned(ARCH_ALIGN);

/* generated data */
extern const uint16_t irq_num;
extern const struct irq_cfg irq_cfg[];
extern struct irq irq_dyn[];

extern const uint16_t irq_table_num;
extern struct irq * const irq_table[];

#endif
