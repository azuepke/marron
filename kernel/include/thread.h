/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018, 2020, 2021, 2025 Alexander Zuepke */
/*
 * thread.h
 *
 * Threads.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 * azuepke, 2025-02-22: processes
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <thread_types.h>
#include <assert.h>

/* API */
/** initialize all thread objects at boot time to be blocked in STATE_DEAD */
void thread_init_all(void);

/** allocate a thread (locks part_lock internally) */
struct thread *thread_alloc(struct part *part);

/** lookup thread in process (process_lock is locked) */
err_t thread_lookup_locked(struct process *proc, ulong_t thr_id, struct thread **thr_p);

/** lookup thread in process (unlocked) */
err_t thread_lookup(struct process *proc, ulong_t thr_id, struct thread **thr_p);

/** create idle thread at boot time */
struct thread *thread_create_idle(struct process *proc);


/** setup user space TLS of a thread */
err_t thread_setup_user_tls(
	struct thread *thr,
	struct sys_tls *tls);

/** start a thread to execute a continuation in the kernel */
void thread_start_kern(
	struct process *proc,
	struct thread *thr,
	void (*continuation)(struct thread *),
	void *arg,
	int prio,
	unsigned int quantum,
	unsigned long cpu_mask);

/** kill a thread in any state (called with proc_lock) */
void thread_kill(struct thread *thr);

/** let current thread wait in wait state with timeout and given wait priority */
void thread_wait_prio(struct thread *thr, unsigned int state, sys_time_t timeout, unsigned int clk_id_absflag, unsigned int wait_prio);

/** wake a thread and make it READY again */
void thread_wakeup(struct thread *thr);

/** release the timeout of a blocked thread */
void thread_timeout_release(struct thread *thr);

/** preempt current thread (enforce priority changes) */
err_t thread_preempt(struct thread *thr);

/** yield current thread */
err_t thread_yield(struct thread *thr);

/** get bounded user priority of current thread */
int thread_prio_get(struct thread *thr);

/** let current thread wait in wait state with timeout */
static inline void thread_wait(struct thread *thr, unsigned int state, sys_time_t timeout, unsigned int clk_id_absflag)
{
	thread_wait_prio(thr, state, timeout, clk_id_absflag, thread_prio_get(thr));
}

/** change priority of current thread */
int thread_prio_change(struct thread *thr, int new_prio);

/** change priority + quantum of any thread */
err_t thread_prio_quantum_change(struct thread *thr, struct sys_sched_param *new_sched, struct sys_sched_param *old_sched);

/** change CPU of current thread */
void thread_cpu_migrate(struct thread *thr, unsigned int new_cpu);

/** change CPU mask of any thread */
err_t thread_cpu_mask_change(struct thread *thr, ulong_t new_cpu_mask, ulong_t *old_cpu_mask);

/** suspend current thread */
err_t thread_suspend(struct thread *thr);

/** resume suspended thread */
err_t thread_resume(struct thread *thr);

/** register deferred syscall handler */
static inline void thread_defer_to(
	struct thread *thr,
	void (*syscalldefer)(struct thread *))
{
	assert(thr != NULL);
	assert(thr->syscalldefer == NULL);

	thr->syscalldefer = syscalldefer;
}

/** register continuation handler */
static inline void thread_continue_at(
	struct thread *thr,
	void (*continuation)(struct thread *))
{
	assert(thr != NULL);
	assert(thr->continuation == NULL);

	thr->continuation = continuation;
}

/** Continuation callback to set the wakeup code after waiting
 *
 * After waiting in the kernel, this standard contination callback
 * sets the current wakeup code as return value of the last system call
 * of the current thread.
 */
void thread_wait_finish(struct thread *thr);

/** register continuation handler */
static inline void thread_continue_at_wait_finish(
	struct thread *thr)
{
	assert(thr != NULL);
	assert(thr->continuation == NULL);

	thr->continuation = thread_wait_finish;
	thr->wakeup_code = 0;	/* EOK */
}

/** register cancellation handler */
static inline void thread_cancel_at(
	struct thread *thr,
	void (*cancellation)(struct thread *, err_t))
{
	assert(thr != NULL);
	assert(thr->cancellation == NULL);

	thr->cancellation = cancellation;
}

/** unregister cancellation handler */
static inline void thread_cancel_clear(
	struct thread *thr)
{
	assert(thr != NULL);
	assert(thr->cancellation != NULL);

	thr->cancellation = NULL;
}

/* forward */
void thread_exit_deferred(struct thread *thr);

#endif
