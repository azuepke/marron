/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * vma_types.h
 *
 * Virtual memory area data type.
 *
 * azuepke, 2021-09-22: initial
 */

#ifndef VMA_TYPES_H_
#define VMA_TYPES_H_

#include <stdint.h>
#include <marron/arch_defs.h>
#include <marron/types.h>
#include <marron/compiler.h>
#include <marron/rbtree.h>
#include <marron/list.h>


#define VMA_NULL_GUARD	0	/**< VMA type: NULL guard area */
#define VMA_GUARD		1	/**< VMA type: guard area */
#define VMA_IO			2	/**< VMA type: I/O resource */
#define VMA_MEM			3	/**< VMA type: memory resource */
#define VMA_ANON		4	/**< VMA type: anonymous memory */

#define VMA_TYPE_GUARD(t)	(((t) == VMA_NULL_GUARD) || ((t) == VMA_GUARD))
#define VMA_TYPE_REFD(t)	(((t) == VMA_MEM) || ((t) == VMA_ANON))
#define VMA_TYPE_OFFSET(t)	(((t) == VMA_IO) || ((t) == VMA_MEM))


/** get VMA object from VMA list queue node or VMA free list queue node */
#define VMA_FROM_VMAFREELIST(v) list_entry((v), struct vma, vma_free_list_node)

/** get VMA object from VMA tree node */
#define VMA_FROM_VMATREE(v) rbtree_entry((v), struct vma, vma_tree_node)


/** Virtual memory area object
 *
 * We keep the VMAs in two data structures at the same time:
 * in a red-black tree (to search by address), and a linked list.
 *
 * The VMA tree/list has a special node, called NULL node, at the beginning
 * of the virtual address space to protect the NULL pointer from getting mapped.
 * The NULL node is denoted as a VMA to an IO memory area of size zero,
 * but it has a non-zero guardsize to indicate a reserved memory area
 * where no new entries can be inserted.
 */
struct vma {
	/** node in VMA tree */
	rbtree_node_t vma_tree_node;
	union {
		/** node in VMA list (helps augmentation)
		 *
		 * NOTE: We roll our own linked-list implementation here,
		 * as the list_t type requires the list head (sentinel)
		 * to distinguish if we have reached the beginning/end of the list.
		 * This would require an additional pointer to struct adspace.
		 * Also, a NULL pointer means we reached the first or last element.
		 */
		struct {
			struct vma *vma_next;
			struct vma *vma_prev;
		};
		/** node in VMA free list (adspace::vma_free_list_head)
		 *
		 * This is only used when the VMA is currently kept on the free list.
		 */
		list_t vma_free_list_node;
	};

	/** start and size of virtual memory area */
	addr_t start;
	size_t size;

	/** tracking of largest gap in virtual memory to right side in VMA tree */
	size_t largest_gap;

	/** VMA type (VMA_NULL_GUARD, VMA_GUARD, VMA_IO, VMA_NONE) */
	uint8_t type;

	/** current access permissions (PROT_READ|PROT_WRITE|PROT_EXEC or none) */
	uint8_t curr_prot;

	/** maximal access permissions (set when creating mapping) */
	uint8_t max_prot;

	/** cache attributes (CACHE_TYPE value, 0..7) */
	uint8_t cache;

	/** VMA flags */
	uint32_t flags;

	/** associated memory pool and freelist (VMA_MEM and VMA_ANON) */
	const struct memrq_cfg *memrq_cfg;

	/** file offset or physical address if I/O (VMA_IO and VMA_MEM) */
	phys_addr_t offset;
};

#endif
