/* SPDX-License-Identifier: MIT */
/* Copyright 2017 Alexander Zuepke */
/* Copyright 2018 Andreas Werner */
/*
 * test.c
 *
 * Example OS -- test code
 *
 * azuepke, 2017-07-17: initial
 */

#include <marron/api.h>
#include <marron/stack.h>
#include <marron/mapping.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert.h>
#include <marron/macros.h>
#include <marron/arch_tls.h>
#include <marron/arch_defs.h>
#include <marron/util.h>

/*#define VERBOSE*/
#ifdef VERBOSE
# define vprint(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
# define vprint(fmt, ...)
#endif

/* TLS areas */
struct sys_tls main_tls;

/* bare-metal application entry point */
/* NOTE: this entry point is usually private to the libc implementation */
void _start_c(void *arg1 __unused, size_t arg2 __unused, void *arg3 __unused, size_t arg4 __unused, void *arg5 __unused);
void _start_c(void *arg1 __unused, size_t arg2 __unused, void *arg3 __unused, size_t arg4 __unused, void *arg5 __unused)
{
	volatile uint32_t *shm1;
	err_t err;

	err = sys_tls_set(&main_tls);
	if (err != EOK) {
		sys_abort();
	}

	/* virtual memory area to map the SHM for test_partition() */
	shm1 = config_memrq_map("shm1", MEMRQ_TYPE_SHM, 0x1000);
	if (shm1 == (void *)-1) {
		sys_abort();
	}

	shm1[4]++;
	shm1[5] = 0;	/* ping */
	shm1[6] = 0;	/* pong */
	vprint("- Part 2 is in init run test. %u\n", shm1[3]);
	while (shm1[2] == 0x0) {
		sys_sleep(1000*1000, CLK_ID_MONOTONIC);
	}
	vprint("- Hello World from user space in Partition 2\n");
	switch(shm1[3]) {
		case 0:
			for (unsigned int i = 1; i <= 5; i++) {
				sys_time_t now;
				sys_clock_get(CLK_ID_MONOTONIC, &now);
				vprint("- loop %d: now it is %lld ns since boot\n", i, now);
				sys_sleep(now + 1000*1000*1000, CLK_ID_MONOTONIC | TIMEOUT_ABSOLUTE);
			}
			break;
		case 1:
			vprint("- Run endless Loop\n");
			for(;;);
			break;
		case 2:
			sys_sleep(3*1000*1000*1000ULL, CLK_ID_MONOTONIC);
			shm1[2] = 0x0; /* lock spinlock */
			vprint("- Restart my partition\n");
			sys_part_restart();
			break;
		case 3:
			vprint("- wait for ping\n");
			while (shm1[5] == 0) {
				sys_sleep(10*1000*1000, CLK_ID_MONOTONIC);
			}
			shm1[5] = 0;
			vprint("- send pong\n");
			shm1[6] = 1;
			sys_sleep(10*1000*1000, CLK_ID_MONOTONIC);
			break;
		case 4:
			{
				unsigned int state;
				printf("- Test try to get State from Part 1: ");
				err = sys_part_state_other(1, &state);
				assert(err == EPERM);
				printf("EPERM\n");
				printf("- Test try to restart Part 1: ");
				err = sys_part_restart_other(1);
				assert (err == EPERM);
				printf("EPERM\n");
				printf("- Test try to restart Part 1: ");
				err = sys_part_shutdown_other(1);
				assert (err == EPERM);
				printf("EPERM\n");
				printf("- Test try to shutdown board: ");
				err = sys_bsp_shutdown(2);
				assert (err == EPERM);
				printf("EPERM\n");
				shm1[6] = 1;
				sys_sleep(10*1000*1000, CLK_ID_MONOTONIC);
			}
			break;
		default:
			printf("Unkown test\n");
			assert(0);
	}
	vprint("- Partition Shutdown\n");

	sys_part_shutdown();
}
