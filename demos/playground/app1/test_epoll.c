/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * test_epoll.c
 *
 * Testcases for epoll.
 *
 * azuepke, 2024-12-14: initial
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <marron/epoll.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"
#include "bench.h"
#include <sys/epoll.h>


void test_epoll(void)
{
	struct sys_epoll events[10];
	struct sys_epoll e = { 0 };
	uint32_t ipc_server_fd;
	uint32_t ipc_client_fd;
	uint32_t epoll_fd;
	uint32_t epoll_fd2;
	uint32_t num;
	err_t err;

	print_header("epoll");

	printf("- basic API:\n");

	/* testing the interface for conformity:
	 * we can only add the IPC server by default,
	 * but once we do sys_ipc_notify(), we can also add the IPC client
	 */
	err = sys_ipc_pair(0, &ipc_server_fd, &ipc_client_fd);
	assert_eq(err, EOK);

	err = sys_epoll_create(0, &epoll_fd);
	assert_eq(err, EOK);

	err = sys_epoll_create(0, &epoll_fd2);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(ipc_server_fd, EPOLL_CTL_ADD, ipc_server_fd, &e);
	assert_eq(err, EINVAL);

	err = sys_epoll_ctl(ipc_server_fd, EPOLL_CTL_ADD, epoll_fd, &e);
	assert_eq(err, EINVAL);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_client_fd, &e);
	assert_eq(err, EPERM);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, epoll_fd2, &e);
	assert_eq(err, ELOOP);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_server_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_server_fd, &e);
	assert_eq(err, EEXIST);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_MOD, ipc_server_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_DEL, ipc_server_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_DEL, ipc_server_fd, &e);
	assert_eq(err, ENOENT);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_MOD, ipc_server_fd, &e);
	assert_eq(err, ENOENT);

	err = sys_ipc_notify(ipc_client_fd, 0);
	assert_eq(err, EINVAL);

	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_client_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_client_fd, &e);
	assert_eq(err, EEXIST);

	err = sys_epoll_wait(ipc_server_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EINVAL);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	err = sys_epoll_wait(epoll_fd, countof(events), 1, NULL, events, &num);
	assert_eq(err, ETIMEDOUT);

	err = sys_fd_close(epoll_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(epoll_fd2);
	assert_eq(err, EOK);

	err = sys_fd_close(ipc_server_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(ipc_client_fd);
	assert_eq(err, EOK);


	printf("- closing fds:\n");

	/* close fds while epoll notes are pending */
	err = sys_ipc_pair(0, &ipc_server_fd, &ipc_client_fd);
	assert_eq(err, EOK);

	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EOK);

	err = sys_epoll_create(0, &epoll_fd);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_server_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_client_fd, &e);
	assert_eq(err, EOK);

	err = sys_fd_close(epoll_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(ipc_server_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(ipc_client_fd);
	assert_eq(err, EOK);


	printf("- closing fds 2:\n");

	/* other direction of adding and closing */
	err = sys_ipc_pair(0, &ipc_server_fd, &ipc_client_fd);
	assert_eq(err, EOK);

	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EOK);

	err = sys_epoll_create(0, &epoll_fd);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_client_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_server_fd, &e);
	assert_eq(err, EOK);

	err = sys_fd_close(ipc_client_fd);
	assert_eq(err, EOK);

	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EPIPE);

	err = sys_fd_close(ipc_server_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(epoll_fd);
	assert_eq(err, EOK);


	printf("- waiting on epoll:\n");

	/* waiting on epoll:
	 * we add both client and server side of the IPC
	 * and test various waiting conditions with timeout
	 */
	err = sys_ipc_pair(0, &ipc_server_fd, &ipc_client_fd);
	assert_eq(err, EOK);

	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EOK);

	err = sys_epoll_create(0, &epoll_fd);
	assert_eq(err, EOK);

	e.eventmask = EPOLLIN | EPOLLOUT | EPOLLHUP | EPOLLERR;
	e.data = ipc_client_fd;
	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_client_fd, &e);
	assert_eq(err, EOK);

	e.eventmask = EPOLLIN | EPOLLOUT | EPOLLHUP | EPOLLERR;
	e.data = ipc_server_fd;
	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_server_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	/* the client should become pending by now */
	err = sys_ipc_notify(ipc_server_fd, EPOLLIN);
	assert_eq(err, EOK);

	num = -1;
	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EOK);
	assert(num == 1);

	num = -1;
	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EOK);
	assert(num == 1);
	assert(events[0].eventmask == EPOLLIN);
	assert(events[0].data == ipc_client_fd);

	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EOK);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	err = sys_fd_close(ipc_client_fd);
	assert_eq(err, EOK);

	num = -1;
	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EOK);
	assert(num == 1);
	assert(events[0].eventmask == (EPOLLERR | EPOLLHUP));
	assert(events[0].data == ipc_server_fd);

	num = -1;
	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EOK);
	assert(num == 1);
	assert(events[0].eventmask == (EPOLLERR | EPOLLHUP));
	assert(events[0].data == ipc_server_fd);

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_DEL, ipc_server_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	err = sys_fd_close(epoll_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(ipc_server_fd);
	assert_eq(err, EOK);


	printf("- event-triggered epoll:\n");

	/* waiting on epoll:
	 * we trigger events on the IPC client side
	 */
	err = sys_ipc_pair(0, &ipc_server_fd, &ipc_client_fd);
	assert_eq(err, EOK);

	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EOK);

	err = sys_epoll_create(0, &epoll_fd);
	assert_eq(err, EOK);

	e.eventmask = EPOLLIN | EPOLLOUT | EPOLLHUP | EPOLLERR | EPOLLET;
	e.data = ipc_client_fd;
	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, ipc_client_fd, &e);
	assert_eq(err, EOK);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	/* trigger: the client should become pending by now */
	err = sys_ipc_notify(ipc_server_fd, EPOLLIN);
	assert_eq(err, EOK);

	/* we can consume the event only once */
	num = -1;
	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EOK);
	assert(num == 1);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	/* "null" events should not trigger */
	err = sys_ipc_notify(ipc_server_fd, 0);
	assert_eq(err, EOK);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	/* trigger again */
	err = sys_ipc_notify(ipc_server_fd, EPOLLIN);
	assert_eq(err, EOK);

	/* we can consume the event only once */
	num = -1;
	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EOK);
	assert(num == 1);

	err = sys_epoll_wait(epoll_fd, countof(events), 0, NULL, events, &num);
	assert_eq(err, EAGAIN);

	err = sys_fd_close(ipc_client_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(ipc_server_fd);
	assert_eq(err, EOK);

	err = sys_fd_close(epoll_fd);
	assert_eq(err, EOK);

	printf("- OK!\n");
}
