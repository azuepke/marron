/* SPDX-License-Identifier: MIT */
/* Copyright 2024 Alexander Zuepke */
/*
 * test_eventfd.c
 *
 * Testcases for eventfd.
 *
 * azuepke, 2024-12-29: initial
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"
#include "bench.h"
#include <unistd.h>
#include <sys/eventfd.h>
#include <errno.h>


struct comm {
	uint32_t fd;
	volatile uint32_t step;
};

/* reader (dedicated thread) */
static void test_eventfd_reader(void *arg)
{
	struct comm *comm = arg;
	uint64_t value;
	ssize_t rw;

	assert_eq(comm->step, 0);
	comm->step = 1;

	/* read does not block */
	value = 5;
	errno = EOK;
	rw = read(comm->fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 1);

	assert_eq(comm->step, 1);
	comm->step = 2;

	/* read blocks */
	value = 5;
	errno = EOK;
	rw = read(comm->fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 7);

	assert_eq(comm->step, 5);
	comm->step = 6;

	/* read does not block */
	value = 5;
	errno = EOK;
	rw = read(comm->fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 13);

	assert_eq(comm->step, 6);
	comm->step = 7;

	/* read does not block */
	value = 5;
	errno = EOK;
	rw = read(comm->fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 0xfffffffffffffffe);

	assert_eq(comm->step, 7);
	comm->step = 8;
	sys_yield();

	comm->step = 999;
	sys_thread_exit();
}

/* writer (main thread) */
static void test_eventfd_writer(void *arg)
{
	struct comm *comm = arg;
	uint64_t value;
	ssize_t rw;

	assert_eq(comm->step, 2);
	comm->step = 3;

	/* write does not block */
	value = 7;
	errno = EOK;
	rw = write(comm->fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);

	assert_eq(comm->step, 3);
	comm->step = 4;

	/* write does not block */
	value = 13;
	errno = EOK;
	rw = write(comm->fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);

	assert_eq(comm->step, 4);
	comm->step = 5;

	/* write blocks */
	value = 0xfffffffffffffffe;
	errno = EOK;
	rw = write(comm->fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);

	assert_eq(comm->step, 8);
	comm->step = 9;

	sys_yield();
	assert_eq(comm->step, 999);
}

void test_eventfd(void)
{
	struct comm comm = { 0 };
	uint64_t value;
	uint32_t fd;
	ssize_t rw;
	err_t err;

	print_header("eventfd implementation");

	printf("- basic API (non-blocking counter):\n");
	err = sys_eventfd_create(42, SYS_FD_NONBLOCK, &fd);
	assert_eq(err, EOK);

	value = 5;
	rw = read(fd, &value, 7);
	assert_eq(rw, -1);
	assert_eq(errno, EINVAL);
	assert_eq(value, 5);

	errno = EOK;
	rw = read(fd, &value, 9);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 42);

	rw = read(fd, &value, 8);
	assert_eq(rw, -1);
	assert_eq(errno, EAGAIN);
	assert_eq(value, 42);

	rw = write(fd, &value, 7);
	assert_eq(rw, -1);
	assert_eq(errno, EINVAL);
	assert_eq(value, 42);

	errno = EOK;
	rw = write(fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 42);

	value = 5;
	rw = write(fd, &value, 9);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 5);

	rw = read(fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 47);

	err = sys_fd_close(fd);
	assert_eq(err, EOK);

	err = sys_fd_close(fd);
	assert_eq(err, EBADF);


	printf("- basic API (non-blocking semaphore):\n");
	err = sys_eventfd_create(2, SYS_FD_NONBLOCK | SYS_FD_SPECIAL, &fd);
	assert_eq(err, EOK);

	value = 5;
	errno = EOK;
	rw = read(fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 1);

	value = 5;
	rw = read(fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 1);

	value = 5;
	rw = read(fd, &value, 8);
	assert_eq(rw, -1);
	assert_eq(errno, EAGAIN);
	assert_eq(value, 5);

	value = 1;
	errno = EOK;
	rw = write(fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 1);

	value = 5;
	rw = read(fd, &value, 8);
	assert_eq(rw, 8);
	assert_eq(errno, EOK);
	assert_eq(value, 1);

	value = 5;
	rw = read(fd, &value, 8);
	assert_eq(rw, -1);
	assert_eq(errno, EAGAIN);
	assert_eq(value, 5);

	err = sys_fd_close(fd);
	assert_eq(err, EOK);


	printf("- blocking counter:\n");
	err = sys_eventfd_create(1, 0, &comm.fd);
	assert_eq(err, EOK);

	create_thread(1, test_eventfd_reader, &comm, sys_prio_get());
	sys_yield();

	test_eventfd_writer(&comm);

	err = sys_fd_close(comm.fd);
	assert_eq(err, EOK);

	printf("- OK!\n");
}
