/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2021 Alexander Zuepke */
/*
 * app.ld.S
 *
 * Application linker script
 *
 * azuepke, 2017-11-02: initial, from BSP linker script
 */

/* application image virtual start address */
#define APP_VADDR 0x400000

#if defined __aarch64__
OUTPUT_FORMAT("elf64-littleaarch64")
OUTPUT_ARCH("aarch64")
#elif defined __arm__
OUTPUT_FORMAT("elf32-littlearm")
OUTPUT_ARCH("arm")
#elif defined __riscv
OUTPUT_ARCH("riscv")
#endif

ENTRY(_start)

PHDRS
{
	text   PT_LOAD FLAGS(5);
	rodata PT_LOAD FLAGS(4);
	data   PT_LOAD FLAGS(6);
}

SECTIONS
{
	.text (APP_VADDR + SIZEOF_HEADERS) : {
		__text_start = .;
		*(.text.start)
		*(.text.init)
		*(.text.cold)
		*(.text)
		*(.text.*)
		__text_end = .;
	} : text

	.rodata ALIGN(4096) : {
		__rodata_start = .;
		*(.rodata)
		*(.rodata.*)
		*(.rodata1)
		*(.sdata2)
		*(.sbss2)
		__rodata_end = .;
	} : rodata

	.data ALIGN(4096) : {
		__data_start = .;
		*(.data)
		*(.data.*)
		*(.data1)
		*(.sdata)
		__data_end = .;
	} : data

	.bss ALIGN(16) : {
		__bss_start = .;
		*(.sbss)
		*(.scommon)

		*(.bss)
		*(.bss.*)
		*(COMMON)
		__bss_end = .;
	} : data

	/DISCARD/ : {
		*(.eh_frame)
		*(.ARM.exidx)
	}
}
