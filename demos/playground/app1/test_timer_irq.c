/* SPDX-License-Identifier: MIT */
/* Copyright 2013, 2018, 2025 Alexander Zuepke */
/*
 * test_timer_irq.c
 *
 * ARM SP804 timer driver
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0271d/DDI0271.pdf
 *
 * The SP804 hardware has two identical programmable "Free Running Counters".
 * The timers decrement down and fire at the 1 -> 0 transition.
 * We program them in 32-bit mode as periodic timers.
 *
 * azuepke, 2013-11-20: initial
 * azuepke, 2018-03-12: repurposed timer driver as interrupt test case
 * azuepke, 2022-02-03: enable test for Raspberry Pi
 * azuepke, 2025-01-08: epoll
 */

#include <marron/api.h>
#include <marron/mapping.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/irq.h>
#include <sys/epoll.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include <marron/util.h>
#include "app.h"

/** address of SP804 mapping */
static addr_t sp804_timer_base;

/** clock speed */
static unsigned int sp804_timer_clock;

/** timer IRQ ID */
static unsigned int sp804_timer_irq = -1;


/** Registers of the first timer. */
#define SP804_LOAD			0x000	/* load value */
#define SP804_VALUE			0x004	/* current timer value (read) */
#define SP804_CTRL			0x008	/* control register */
#define SP804_ACK			0x00c	/* interrupt clear register (write) */
#define SP804_RIS			0x010	/* raw interrupt status register */
#define SP804_MIS			0x014	/* masked interrupt status register */
#define SP804_BGLOAD		0x018	/* background load value */

/* Timer 2 starts at offset +0x20 to the first timer. */

/** Bits in control register */
#define SP804_CTRL_EN		0x80	/* enable timer */
#define SP804_CTRL_PERIODIC	0x40	/* periodic mode */
#define SP804_CTRL_INT		0x20	/* interrupt enable */
#define SP804_CTRL_PRE1		0x08	/* prescaler bit 1 */
#define SP804_CTRL_PRE0		0x04	/* prescaler bit 0 */
#define SP804_CTRL_32BIT	0x02	/* 32-bit mode */
#define SP804_CTRL_ONESHOT	0x01	/* oneshot mode */

/* prescaler bits:
 * 00: divide clock by 1
 * 01: divide clock by 16
 * 10: divide clock by 256
 */

/** Bits in ACK, RIS, MIS */
#define SP804_IRQ_BIT		0x01	/* IRQ bit */

/* IO accessors */
#if defined __aarch64__
#define LOAD "ldr %w0, %1"
#define STORE "str %0, %1"
#elif defined __arm__
#define LOAD "ldr %0, %1"
#define STORE "str %0, %1"
#else
#define LOAD "lw %0, %1"
#define STORE "sw %0, %1"
#endif

static inline uint32_t readl(const volatile void *addr)
{
	uint32_t ret;
	__asm__ volatile(LOAD : "=r" (ret)
	                 : "m" (*(const volatile uint32_t *)addr) : "memory");
	return ret;
}

static inline void writel(volatile void *addr, uint32_t val)
{
	__asm__ volatile (STORE : : "r" (val),
	                  "m" (*(volatile uint32_t *)addr) : "memory");
}


/* access to per-CPU specific registers */
static inline uint32_t sp804_read32(unsigned long reg)
{
	return readl((volatile void *)(sp804_timer_base + reg));
}

static inline void sp804_write32(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(sp804_timer_base + reg), val);
}

static void timer_tester(unsigned int freq)
{
	unsigned int reload;
	unsigned int err;
	unsigned int i;
	uint64_t value;
	uint32_t fd;
	ssize_t r;

	reload = sp804_timer_clock / freq;

	/* disable and initialize the timer, but do not start it yet */
	sp804_write32(SP804_CTRL, SP804_CTRL_32BIT);
	sp804_write32(SP804_LOAD, reload);

	/* enable timer in 32-bit periodic mode with interrupts */
	sp804_write32(SP804_CTRL, SP804_CTRL_EN | SP804_CTRL_PERIODIC |
	                          SP804_CTRL_INT | SP804_CTRL_32BIT);

	printf("# IRQ enable + wait for: ");

	/* enable timer interrupt */
	sp804_write32(SP804_ACK, SP804_IRQ_BIT);
	fd = -1;
	err = sys_irq_attach(sp804_timer_irq, 0, 0, &fd);
	if (err != EOK) printf("sys_irq_attach: %s\n", __strerror(err));
	assert_eq(err, EOK);

	/* wait for timer interrupt (unmasks it) */
	for (i = 0; i < 10; i++) {
		r = read(fd, &value, sizeof(value));
		if (r != sizeof(value)) printf("sys_irq_wait: %s\n", __strerror(errno));
		assert(r == sizeof(value));

		printf(".");
		sp804_write32(SP804_ACK, SP804_IRQ_BIT);
	}

	/* and we are done ... */
	err = sys_fd_close(fd);
	if (err != EOK) printf("sys_fd_close: %s\n", __strerror(err));
	assert_eq(err, EOK);

	/* enable timer in 32-bit periodic mode with interrupts */
	sp804_write32(SP804_CTRL, 0);

	printf("\n");
}

static void timer_tester_timerfd(void)
{
	struct epoll_event e = { 0 };
	uint64_t value;
	ssize_t r;
	int irqfd;
	int epfd;
	int ret;


	/* open IRQ in blocking mode for some basic API tests */
	irqfd = irq_attach(sp804_timer_irq, IRQ_MODE_DEFAULT, 0);
	if (irqfd == -1) printf("irq_attach: %s\n", __strerror(errno));
	assert(irqfd != -1);

	ret = irq_ctl(irqfd, IRQ_CTL_UNMASK, -1);
	assert_eq(ret, -1);
	assert_eq(errno, EINVAL);

	ret = irq_ctl(irqfd, IRQ_CTL_UNMASK, 0);
	assert_eq(ret, -1);
	assert_eq(errno, EAGAIN);

	/* add to epoll instance */
	epfd = epoll_create1(0);
	if (epfd == -1) printf("epoll_create1: %s\n", __strerror(errno));
	assert(epfd != -1);

	e.events = EPOLLIN;
	ret = epoll_ctl(epfd, EPOLL_CTL_ADD, irqfd, &e);
	assert_eq(ret, 0);

	/* try to attach interrupt a second time */
	ret = irq_attach(sp804_timer_irq, IRQ_MODE_DEFAULT, 0);
	assert_eq(ret, -1);
	assert_eq(errno, ESTATE);

	/* close in order irqfd, epfd */
	ret = close(irqfd);
	if (ret == -1) printf("close: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = close(epfd);
	if (ret == -1) printf("close: %s\n", __strerror(errno));
	assert_eq(ret, 0);


	/* second open IRQ in non-blocking mode */
	irqfd = irq_attach(sp804_timer_irq, IRQ_MODE_DEFAULT, IRQ_NONBLOCK);
	if (irqfd == -1) printf("irq_attach: %s\n", __strerror(errno));
	assert(irqfd != -1);

	r = read(irqfd, &value, sizeof(value));
	assert_eq(r, -1);
	assert_eq(errno, EAGAIN);

	ret = irq_ctl(irqfd, IRQ_CTL_CLEAR, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = irq_ctl(irqfd, IRQ_CTL_UNMASK, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = irq_ctl(irqfd, IRQ_CTL_CLEAR, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = irq_ctl(irqfd, IRQ_CTL_MASK, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = irq_ctl(irqfd, IRQ_CTL_CLEAR, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = irq_ctl(irqfd, IRQ_CTL_UNMASK, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = irq_ctl(irqfd, IRQ_CTL_CLEAR, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	/* add to epoll instance */
	epfd = epoll_create1(0);
	if (epfd == -1) printf("epoll_create1: %s\n", __strerror(errno));
	assert(epfd != -1);

	e.events = EPOLLIN;
	ret = epoll_ctl(epfd, EPOLL_CTL_ADD, irqfd, &e);
	assert_eq(ret, 0);

	/* close in order epfd, irqfd */
	ret = close(irqfd);
	if (ret == -1) printf("close: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = close(epfd);
	if (ret == -1) printf("close: %s\n", __strerror(errno));
	assert_eq(ret, 0);
}

static void timer_tester_epoll(unsigned int freq)
{
	struct epoll_event e = { 0 };
	unsigned int reload;
	unsigned int i;
	int irqfd;
	int epfd;
	int ret;

	reload = sp804_timer_clock / freq;

	/* disable and initialize the timer, but do not start it yet */
	sp804_write32(SP804_CTRL, SP804_CTRL_32BIT);
	sp804_write32(SP804_LOAD, reload);

	/* enable timer in 32-bit periodic mode with interrupts */
	sp804_write32(SP804_CTRL, SP804_CTRL_EN | SP804_CTRL_PERIODIC |
	                          SP804_CTRL_INT | SP804_CTRL_32BIT);

	printf("# IRQ enable + wait for: ");

	/* open IRQ in non-blocking mode */
	irqfd = irq_attach(sp804_timer_irq, IRQ_MODE_DEFAULT, IRQ_NONBLOCK);
	if (irqfd == -1) printf("irq_attach: %s\n", __strerror(errno));
	assert(irqfd != -1);

	/* add to epoll instance */
	epfd = epoll_create1(0);
	if (epfd == -1) printf("epoll_create1: %s\n", __strerror(errno));
	assert(epfd != -1);

	e.events = EPOLLIN;
	e.data.u32 = 1234;
	ret = epoll_ctl(epfd, EPOLL_CTL_ADD, irqfd, &e);
	assert_eq(ret, 0);

	/* enable IRQ in background */
	ret = irq_ctl(irqfd, IRQ_CTL_UNMASK, 0);
	if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	/* enable timer interrupt */
	sp804_write32(SP804_ACK, SP804_IRQ_BIT);

	/* wait for timer interrupt (unmasks it) */
	for (i = 0; i < 10; i++) {
		ret = epoll_wait(epfd, &e, 1, 2000);
		assert_eq(ret, 1);

		printf(".");
		sp804_write32(SP804_ACK, SP804_IRQ_BIT);

		ret = irq_ctl(irqfd, IRQ_CTL_CLEAR, 0);
		if (ret == -1) printf("irq_ctl: %s\n", __strerror(errno));
		assert_eq(ret, 0);
	}

	/* and we are done ... */
	ret = close(irqfd);
	if (ret == -1) printf("close: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	ret = close(epfd);
	if (ret == -1) printf("close: %s\n", __strerror(errno));
	assert_eq(ret, 0);

	/* enable timer in 32-bit periodic mode with interrupts */
	sp804_write32(SP804_CTRL, 0);

	printf("\n");
}

void test_timer_irq(void)
{
	char bsp_name[32];
	err_t err;

	print_header("IRQ-based Timer");

	printf("IRQs assigned to partition:\n");
	for (unsigned int i = 0; ; i++) {
		unsigned int irq_id;
		char irq_name[32];
		err_t err1, err2;

		err1 = sys_irq_iter_id(i, &irq_id);
		assert((err1 == EOK) || (err1 == ELIMIT));
		err2 = sys_irq_iter_name(i, irq_name, sizeof(irq_name));
		assert((err2 == EOK) || (err2 == ELIMIT));
		assert_eq(err1, err2);

		if (err1 != EOK) {
			break;
		}

		printf("- %u: \"%s\" IRQ: %u\n", i, irq_name, irq_id);

		/* look out for "sp804_timer" */
		if (strcmp(irq_name, "sp804_timer") == 0) {
			sp804_timer_irq = irq_id;
		}
	}

	if (sp804_timer_irq == -1u) {
		printf("# timer IRQ not mapped to partition, skipping test\n");
		return;
	}

	sp804_timer_base = (addr_t)config_memrq_map(IO_MAP_NAME, MEMRQ_TYPE_IO, IO_MAP_SIZE);
	assert_ne((void *)sp804_timer_base, (void *)-1);

	err = sys_bsp_name(bsp_name, sizeof(bsp_name));
	assert_eq(err, EOK);

	if (strcmp(bsp_name, "qemu-arm") == 0) {
		sp804_timer_clock = 1000000;	/* 1 MHz */
		sp804_timer_base += 0;	/* offset */
	}
	if (strcmp(bsp_name, "bcm2836") == 0) {
		sp804_timer_clock = 1000000;	/* 1 MHz */
		sp804_timer_base += 0x400;	/* offset */
	}
	if (strcmp(bsp_name, "bcm2837") == 0) {
		sp804_timer_clock = 1000000;	/* 1 MHz */
		sp804_timer_base += 0x400;	/* offset */
	}
	if (strcmp(bsp_name, "bcm2711") == 0) {
		sp804_timer_clock = 54000000;	/* 54 MHz */
		sp804_timer_base += 0x400;	/* offset */
	}

	if (sp804_timer_clock == 0) {
		printf("# timer not supported on %s, skipping test\n", bsp_name);
		return;
	}

	printf("# hardware timer available: IRQ %u, clock %u HZ\n", sp804_timer_irq, sp804_timer_clock);

	printf("# testing 1 Hz, 10 timer interrupts\n");
	timer_tester(1);
	printf("\n");

	printf("# testing timerfd API\n");
	timer_tester_timerfd();
	printf("\n");

	printf("# testing 1 Hz, 10 timer interrupts, epoll\n");
	timer_tester_epoll(1);
	printf("\n");

	err = sys_vm_unmap(sp804_timer_base, IO_MAP_SIZE);
	assert_eq(err, EOK);
	(void)err;
}
