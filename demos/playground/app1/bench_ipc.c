/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * bench_ipc.c
 *
 * IPC benchmark
 *
 * azuepke, 2022-09-03: initial
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <marron/mapping.h>
/* this is a benchmark, no not enable assert() unconditionally */
#include <assert_ext.h>
#include <marron/util.h>
#include "app.h"
#include "bench.h"


/* simple IPC server reply */
/* protocol:
 *
 * msg.req:
 * - 0: ping-ping
 * - 1: memory read
 * - 2: memory write
 * msg.h32:
 * - server-side offset into buffer for unaligned memory access overhead
 * msg.buf0 + msg.size0:
 * - dummy buffer
 */
static void bench_ipc_server(void *arg __unused)
{
	char buffer[4096+16] __aligned(16);
	struct sys_ipc msg;
	unsigned int fd;
	uint32_t rhnd;
	size_t size;
	err_t err;
	int port;

	port = config_ipc_port_id(IPC_SERVER_NAME, IPC_TYPE_SERVER);
	assert3(port, !=, -1);

	err = sys_ipc_port_fd(IPC_TYPE_SERVER, port, &fd);
	assert_eq(err, EOK);

	rhnd = 0;
	while (1) {
		err = sys_ipc_reply_wait(rhnd, &msg, fd, &rhnd, &msg);
		assert((err == EOK) || (err == EPIPE));
		if (err == EPIPE) {
			break;
		}

		switch (msg.req) {
		case 0:
			/* just ping-pong */
			break;

		case 1:
			/* memory read benchmark */
			err = sys_ipc_read(rhnd, 0, buffer + msg.h32, sizeof buffer, 0, &size);
			assert(err == EOK);
			break;

		case 2:
			/* memory write benchmark */
			err = sys_ipc_write(rhnd, 0, buffer + msg.h32, sizeof buffer, 0, &size);
			assert(err == EOK);
			break;

		default:
			goto out;
		}
	}

out:
	err = sys_ipc_reply_wait(rhnd, &msg, fd, NULL, NULL);
	assert_eq(err, EOK);

	sys_thread_exit();
}

void bench_ipc(void)
{
	char buffer[4096+16] __aligned(16);
	struct sys_ipc msg = { 0 };
	TIME_T before, after;
	unsigned int size;
	unsigned int runs;
	unsigned int fd;
	err_t err;
	int port;

	printf("\n*** IPC performance:\n");

	port = config_ipc_port_id(IPC_CLIENT_NAME, IPC_TYPE_CLIENT);
	assert3(port, !=, -1);

	err = sys_ipc_port_fd(IPC_TYPE_CLIENT, port, &fd);
	assert_eq(err, EOK);
	(void)err;

	create_thread(1, bench_ipc_server, NULL, sys_prio_get());
	sys_yield();


	printf("- IPC input validation: zero flags\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		err = sys_ipc_call(0xffff, &msg, &msg);
		assert_eq(err, EBADF);
	}
	after = GETTS();
	delta(before, after);


	printf("- IPC input validation: mapping\t\t\t\t");
	msg.flags = SYS_IPC_MAP;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	msg.map_prot = PROT_READ | PROT_WRITE;
	msg.map_flags = MAP_SHARED | MAP_FIXED;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		err = sys_ipc_call(0xffff, &msg, &msg);
		assert_eq(err, EBADF);
	}
	after = GETTS();
	delta(before, after);


	printf("- IPC input validation: buffer\t\t\t\t");
	msg.flags = SYS_IPC_BUF0R;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	msg.map_prot = PROT_READ | PROT_WRITE;
	msg.map_flags = MAP_SHARED | MAP_FIXED;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		err = sys_ipc_call(0xffff, &msg, &msg);
		assert_eq(err, EBADF);
	}
	after = GETTS();
	delta(before, after);


	printf("- IPC call/reply\t\t\t\t\t");
	msg.flags = 0;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		err = sys_ipc_call(fd, &msg, &msg);
		assert_eq(err, EOK);
	}
	after = GETTS();
	delta(before, after);


	printf("- IPC call/reply null read\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		msg.flags = SYS_IPC_BUF0R;
		msg.req = 1;
		msg.buf0 = buffer;
		msg.size0 = 0;

		err = sys_ipc_call(fd, &msg, &msg);
		assert_eq(err, EOK);
	}
	after = GETTS();
	delta(before, after);


	printf("- IPC call/reply null write\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		msg.flags = SYS_IPC_BUF0W;
		msg.req = 2;
		msg.buf0 = buffer;
		msg.size0 = 0;

		err = sys_ipc_call(fd, &msg, &msg);
		assert_eq(err, EOK);
	}
	after = GETTS();
	delta(before, after);


	for (size = 1; size < sizeof buffer; size *= 2) {
		printf("- IPC call/reply aligned    read  size: %4u\t\t", size);

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			msg.flags = SYS_IPC_BUF0R | SYS_IPC_BUF0W;
			msg.req = 1;
			msg.buf0 = buffer;
			msg.size0 = size;
			msg.h32 = 0;

			err = sys_ipc_call(fd, &msg, &msg);
			assert_eq(err, EOK);
		}
		after = GETTS();
		delta(before, after);
	}


	for (size = 1; size < sizeof buffer; size *= 2) {
		printf("- IPC call/reply aligned    write size: %4u\t\t", size);

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			msg.flags = SYS_IPC_BUF0R | SYS_IPC_BUF0W;
			msg.req = 2;
			msg.buf0 = buffer;
			msg.size0 = size;
			msg.h32 = 0;

			err = sys_ipc_call(fd, &msg, &msg);
			assert_eq(err, EOK);
		}
		after = GETTS();
		delta(before, after);
	}


	for (size = 1; size < sizeof buffer; size *= 2) {
		printf("- IPC call/reply unaligned1 read  size: %4u\t\t", size);

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			msg.flags = SYS_IPC_BUF0R | SYS_IPC_BUF0W;
			msg.req = 1;
			msg.buf0 = buffer +1;
			msg.size0 = size;
			msg.h32 = 0;

			err = sys_ipc_call(fd, &msg, &msg);
			assert_eq(err, EOK);
		}
		after = GETTS();
		delta(before, after);
	}


	for (size = 1; size < sizeof buffer; size *= 2) {
		printf("- IPC call/reply unaligned1 write size: %4u\t\t", size);

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			msg.flags = SYS_IPC_BUF0R | SYS_IPC_BUF0W;
			msg.req = 2;
			msg.buf0 = buffer+1;
			msg.size0 = size;
			msg.h32 = 0;

			err = sys_ipc_call(fd, &msg, &msg);
			assert_eq(err, EOK);
		}
		after = GETTS();
		delta(before, after);
	}


	for (size = 1; size < sizeof buffer; size *= 2) {
		printf("- IPC call/reply unaligned2 read  size: %4u\t\t", size);

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			msg.flags = SYS_IPC_BUF0R | SYS_IPC_BUF0W;
			msg.req = 1;
			msg.buf0 = buffer +1;
			msg.size0 = size;
			msg.h32 = 3;

			err = sys_ipc_call(fd, &msg, &msg);
			assert_eq(err, EOK);
		}
		after = GETTS();
		delta(before, after);
	}


	for (size = 1; size < sizeof buffer; size *= 2) {
		printf("- IPC call/reply unaligned2 write size: %4u\t\t", size);

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			msg.flags = SYS_IPC_BUF0R | SYS_IPC_BUF0W;
			msg.req = 2;
			msg.buf0 = buffer+1;
			msg.size0 = size;
			msg.h32 = 3;

			err = sys_ipc_call(fd, &msg, &msg);
			assert_eq(err, EOK);
		}
		after = GETTS();
		delta(before, after);
	}


	/* terminate server */
	msg.req = 99;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EOK);
	sys_yield();
}
