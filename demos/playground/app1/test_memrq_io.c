/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * test_memrq_io.c
 *
 * Testcases for IPC-based I/O.
 *
 * azuepke, 2022-09-02: initial
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"
#include "bench.h"
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>


#if 0
#define vprintf(x...) printf(x)
#define vvprintf(x...) printf(x)
#else
#define vprintf(x...) (void)0
#define vvprintf(x...) (void)0
#endif

void test_memrq_io(void)
{
	char buf[16];
	off_t length;
	off_t pos;
	ssize_t r;
	void *m;
	int ret;
	int fd;

	print_header("memrq I/O operations");

	printf("- open tests\n");

	fd = open("/cfg/file/this-file-does-not-exist", O_RDWR);
	assert_eq(fd, -1);
	assert_eq(errno, ENOENT);

	fd = open("/cfg/file/my_hello/x", 0);
	assert_eq(fd, -1);
	assert_eq(errno, ENOENT);

	fd = open("/cfg/file/my_hello", 0);
	assert_eq(fd, -1);
	assert_eq(errno, EINVAL);

	fd = open("/cfg/file/my_hello", O_RDWR);
	assert_eq(fd, -1);
	assert_eq(errno, EACCES);

	fd = open("/cfg/file/my_hello", O_RDONLY);
	assert(fd != -1);

	length = lseek(fd, 0, SEEK_END);
	assert(length > 0);
	assert((size_t)length < PAGE_SIZE-1);
	printf("- file size: %lld\n", (long long)length);

	pos = lseek(fd, 0, SEEK_SET);
	assert(pos == 0);
	(void)pos;

	printf("- read test (in pieces): ");
	while (1) {
		r = read(fd, buf, sizeof buf - 1);
		//printf("r: %zd\n", r);
		if (r < 0) {
			printf("errno: %d %s\n", errno, __strerror(errno));
		}
		if (r == 0) {
			break;
		}
		buf[r] = '\0';
		printf("<%s>", buf);
	}
	printf("\n");

	pos = lseek(fd, 0, SEEK_CUR);
	assert(pos == length);

	/* offset not page aligned */
	m = mmap(NULL, PAGE_SIZE, PROT_READ, MAP_SHARED, fd, 1);
	assert_eq(m, MAP_FAILED);
	assert_eq(errno, EINVAL);

	/* negative offset */
	m = mmap(NULL, PAGE_SIZE, PROT_READ, MAP_SHARED, fd, -0x1000);
	assert_eq(m, MAP_FAILED);
	assert_eq(errno, EINVAL);

	/* beyond end of file */
	m = mmap(NULL, PAGE_SIZE, PROT_READ, MAP_SHARED, fd, PAGE_SIZE);
	assert_eq(m, MAP_FAILED);
	assert_eq(errno, ENXIO);

	/* writable */
	m = mmap(NULL, PAGE_SIZE, PROT_WRITE, MAP_SHARED, fd, 0);
	assert_eq(m, MAP_FAILED);
	assert_eq(errno, EACCES);

	/* FIXME: not implemented */
	m = mmap(NULL, PAGE_SIZE, PROT_READ, MAP_SHARED, fd, 0);
	assert(m != MAP_FAILED);

	printf("- map test: 0x%p: <%s>", m, (char*)m);

	ret = munmap(m, PAGE_SIZE);
	assert_eq(ret, EOK);
	(void)ret;

	ret = close(fd);
	assert_eq(ret, EOK);

	printf("- OK!\n");
}
