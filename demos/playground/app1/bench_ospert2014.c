/* SPDX-License-Identifier: MIT */
/* Copyright 2014, 2020 Alexander Zuepke */
/*
 * bench_ospert2014.c
 *
 * Benchmark for OSPERT 2014 paper.
 *
 * azuepke, 2014-05-07: initial
 * azuepke, 2020-02-02: adapted to Kuri
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <marron/compiler.h>
/* this is a benchmark, no not enable assert() unconditionally */
#include <assert_ext.h>
#include "app.h"
#include "bench.h"
#include <marron/arch_tls.h>
#include <stdbool.h>

/*
 * The setup for the benchmark is as follows:
 *
 * highest     75
 * inner CS                            70..#....
 * higher      65                      |        |
 * outer CS                    60..#...^        v...#....
 * lower       55              |                        |
 * normal              50..#...^                        v...#...T
 * lowest      45      |                                        |
 * init task   40   ...^                                        v...
 *
 *
 * thread  prio  stack  name
 *   #0     40     1     init / main
 *   #1     45     2     lowest
 *   #2     55     3     lower
 *   #3     65     4     higher
 *   #4     75     5     highest
 */

/* Thread IDs */
#define INIT_TASK		0
#define LOWEST_TASK		1
#define LOWER_TASK		2
#define HIGHER_TASK		3
#define HIGHEST_TASK	4

/* Prios */
#define INIT_PRIO		40
#define LOWEST_PRIO		45
#define LOWER_PRIO		55
#define HIGHER_PRIO		65
#define HIGHEST_PRIO	75

/* Prios in critical sections */
#define NORMAL_PRIO		50
#define OUTER_CS_PRIO	60
#define INNER_CS_PRIO	70

#define TASKS_SINGLE		5	/* FIXME: task A does not make a difference to B */
#define TASKS_NESTED		5
#define TRIGGERS_SINGLE		3
#define TRIGGERS_NESTED		5

////////////////////////////////////////////////////////////////////////////////

static inline unsigned int enter_normal(void)
{
	/* NOTE: uses slow syscall here! */
	return sys_prio_set_syscall(NORMAL_PRIO);
}

////////////////////////////////////////////////////////////////////////////////

/* fast variants */
static inline unsigned int enter_outer_cs_fast(void)
{
	unsigned int old_prio;
	old_prio = sys_prio_set(OUTER_CS_PRIO);
	assert_eq(old_prio, NORMAL_PRIO);
	return old_prio;
}

static inline unsigned int enter_inner_cs_fast(void)
{
	unsigned int old_prio;
	old_prio = sys_prio_set(INNER_CS_PRIO);
	assert_eq(old_prio, OUTER_CS_PRIO);
	return old_prio;
}

static inline unsigned int leave_inner_cs_fast(void)
{
	unsigned int old_prio;
	old_prio = sys_prio_set(OUTER_CS_PRIO);
	assert_eq(old_prio, INNER_CS_PRIO);
	return old_prio;
}

static inline unsigned int leave_outer_cs_fast(void)
{
	unsigned int old_prio;
	old_prio = sys_prio_set(NORMAL_PRIO);
	assert_eq(old_prio, OUTER_CS_PRIO);
	return old_prio;
}

////////////////////////////////////////////////////////////////////////////////

/* slow variants */
static inline unsigned int enter_outer_cs_slow(void)
{
	return sys_prio_set_syscall(OUTER_CS_PRIO);
}

static inline unsigned int enter_inner_cs_slow(void)
{
	return sys_prio_set_syscall(INNER_CS_PRIO);
}

static inline unsigned int leave_inner_cs_slow(void)
{
	return sys_prio_set_syscall(OUTER_CS_PRIO);
}

static inline unsigned int leave_outer_cs_slow(void)
{
	return sys_prio_set_syscall(NORMAL_PRIO);
}

////////////////////////////////////////////////////////////////////////////////

static inline unsigned int leave_normal(void)
{
	/* NOTE: uses slow syscall here! */
	return sys_prio_set_syscall(INIT_PRIO);
}

////////////////////////////////////////////////////////////////////////////////

static unsigned int a, b, c, d;
static unsigned int benchmark_running;

static void highest(void *arg __unused)
{
	err_t err;

	assert_eq(sys_prio_get(), HIGHEST_PRIO);

	while (benchmark_running) {
		a++;
		err = sys_thread_suspend();
		assert_eq(err, EOK);
		(void)err;
	}

	/* no participation in any CS */
	sys_thread_exit();
}

static void higher(void *arg __unused)
{
	err_t err;

	assert_eq(sys_prio_get(), HIGHER_PRIO);

	while (benchmark_running) {
		b++;
		err = sys_thread_suspend();
		assert_eq(err, EOK);
		(void)err;
	}

	/* could enter inner CS */
	sys_thread_exit();
}

static void lower(void *arg __unused)
{
	err_t err;

	assert_eq(sys_prio_get(), LOWER_PRIO);

	while (benchmark_running) {
		c++;
		err = sys_thread_suspend();
		assert_eq(err, EOK);
		(void)err;
	}

	/* could enter outer and inner CS */
	sys_thread_exit();
}

static void lowest(void *arg __unused)
{
	err_t err;

	assert_eq(sys_prio_get(), LOWEST_PRIO);

	while (benchmark_running) {
		d++;
		err = sys_thread_suspend();
		assert_eq(err, EOK);
		(void)err;
	}

	/* could also enter outer and inner CS */
	sys_thread_exit();
}

static inline void wake(unsigned int task_id)
{
	unsigned int err;
	if (task_id != 0) {
		assert(task_id < TASKS_NESTED);
		err = sys_thread_resume(task_id);
		assert_eq(err, EOK);
		(void)err;
	} else {
		/* no one woken up */
	}
}

////////////////////////////////////////////////////////////////////////////////

static TIME_T bench_activation_preemption(unsigned int task_id)
{
	TIME_T before, after;

	assert(task_id < TASKS_SINGLE);

	enter_normal();
	before = GETTS();
	{
		wake(task_id);
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

////////////////////////////////////////////////////////////////////////////////

static TIME_T bench1_fast(unsigned int task_id, unsigned int trigger)
{
	TIME_T before, after;

	assert(task_id < TASKS_SINGLE);
	assert(trigger <= TRIGGERS_SINGLE);

	enter_normal();
	before = GETTS();
	{
		if (trigger == 1)
			wake(task_id);

		enter_outer_cs_fast();
		{
			if (trigger == 2)
				wake(task_id);
		}
		leave_outer_cs_fast();

		if (trigger == 3)
			wake(task_id);
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

static TIME_T bench2_fast(unsigned int task_id, unsigned int trigger)
{
	TIME_T before, after;

	assert(task_id < TASKS_NESTED);
	assert(trigger <= TRIGGERS_NESTED);

	enter_normal();
	before = GETTS();
	{
		if (trigger == 1)
			wake(task_id);

		enter_outer_cs_fast();
		{
			if (trigger == 2)
				wake(task_id);

			enter_inner_cs_fast();
			{
				if (trigger == 3)
					wake(task_id);
			}
			leave_inner_cs_fast();

			if (trigger == 4)
				wake(task_id);
		}
		leave_outer_cs_fast();

		if (trigger == 5)
			wake(task_id);
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

static TIME_T bench_just_cs1_fast(void)
{
	TIME_T before, after;

	enter_normal();
	before = GETTS();
	{
		enter_outer_cs_fast();
		leave_outer_cs_fast();
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

static TIME_T bench_just_cs2_fast(void)
{
	TIME_T before, after;

	enter_normal();
	before = GETTS();
	{
		enter_outer_cs_fast();
		{
			enter_inner_cs_fast();
			leave_inner_cs_fast();
		}
		leave_outer_cs_fast();
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

////////////////////////////////////////////////////////////////////////////////

static TIME_T bench1_slow(unsigned int task_id, unsigned int trigger)
{
	TIME_T before, after;

	assert(task_id < TASKS_SINGLE);
	assert(trigger <= TRIGGERS_SINGLE);

	enter_normal();
	before = GETTS();
	{
		if (trigger == 1)
			wake(task_id);

		enter_outer_cs_slow();
		{
			if (trigger == 2)
				wake(task_id);
		}
		leave_outer_cs_slow();

		if (trigger == 3)
			wake(task_id);
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

static TIME_T bench2_slow(unsigned int task_id, unsigned int trigger)
{
	TIME_T before, after;

	assert(task_id < TASKS_NESTED);
	assert(trigger <= TRIGGERS_NESTED);

	enter_normal();
	before = GETTS();
	{
		if (trigger == 1)
			wake(task_id);

		enter_outer_cs_slow();
		{
			if (trigger == 2)
				wake(task_id);

			enter_inner_cs_slow();
			{
				if (trigger == 3)
					wake(task_id);
			}
			leave_inner_cs_slow();

			if (trigger == 4)
				wake(task_id);
		}
		leave_outer_cs_slow();

		if (trigger == 5)
			wake(task_id);
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

static TIME_T bench_just_cs1_slow(void)
{
	TIME_T before, after;

	enter_normal();
	before = GETTS();
	{
		enter_outer_cs_slow();
		leave_outer_cs_slow();
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

static TIME_T bench_just_cs2_slow(void)
{
	TIME_T before, after;

	enter_normal();
	before = GETTS();
	{
		enter_outer_cs_slow();
		{
			enter_inner_cs_slow();
			leave_inner_cs_slow();
		}
		leave_outer_cs_slow();
	}
	after = GETTS();
	leave_normal();

	return after - before;
}

////////////////////////////////////////////////////////////////////////////////

void bench_ospert2014(void)
{
	unsigned int orig_prio;
	unsigned int task_id;
	unsigned int trigger;
	unsigned int run;
	TIME_T all_time;

	printf("\n\n");
	printf("=============== OSPERT 2014 paper benchmark START ===============\n");
	printf("buildid: %s\n", __buildid);
	printf("runs: %d\n\n", RUNS);

	orig_prio = sys_prio_set(INIT_PRIO);
	benchmark_running = true;
	create_thread(LOWEST_TASK,  lowest,  NULL, LOWEST_PRIO);
	create_thread(LOWER_TASK,   lower,   NULL, LOWER_PRIO);
	create_thread(HIGHER_TASK,  higher,  NULL, HIGHER_PRIO);
	create_thread(HIGHEST_TASK, highest, NULL, HIGHEST_PRIO);

	/* warm up caches */
	for (task_id = 0; task_id < TASKS_SINGLE; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_SINGLE; trigger++) {
			bench1_fast(task_id, trigger);
		}
	}
	for (task_id = 0; task_id < TASKS_NESTED; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_NESTED; trigger++) {
			bench2_fast(task_id, trigger);
		}
	}
	bench_just_cs1_fast();
	bench_just_cs2_fast();

	/* task activation overhead */
	printf("\ntask activation overhead:\n");
	for (task_id = 0; task_id < TASKS_SINGLE; task_id++) {
		printf("task %c, activate :\t\t\t\t", "-DCBA"[task_id]);
		all_time = 0;
		for (run = 0; run < RUNS; run++) {
			all_time += bench_activation_preemption(task_id);
		}
		delta(0, all_time);
	}

	printf("\n==================== OSPERT 2014 fast runs ======================\n\n");

	/* single critical sections -- no interruption */
	printf("*task -, trigger -:\t\t\t\t");
	all_time = 0;
	for (run = 0; run < RUNS; run++) {
		all_time += bench_just_cs1_fast();
	}
	delta(0, all_time);

	/* single critical sections -- no interruption */
	printf("\nsingle critical section -- no interruption:\n");
	for (task_id = 0; task_id < TASKS_SINGLE; task_id++) {
		printf("task %c, no trigger:\t\t\t\t", "-DCBA"[task_id]);
		all_time = 0;
		for (run = 0; run < RUNS; run++) {
			all_time += bench1_fast(task_id, 0);
		}
		delta(0, all_time);
	}

	/* single critical sections -- with interruption */
	printf("\nsingle critical section -- with interruption:\n");
	for (task_id = 0; task_id < TASKS_SINGLE; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_SINGLE; trigger++) {
			printf("task %c, trigger %u:\t\t\t\t", "-DCBA"[task_id], trigger);
			all_time = 0;
			for (run = 0; run < RUNS; run++) {
				all_time += bench1_fast(task_id, trigger);
			}
			delta(0, all_time);
		}
	}

	/* nested critical sections -- no interruption */
	printf("+task -, trigger -:\t\t\t\t");
	all_time = 0;
	for (run = 0; run < RUNS; run++) {
		all_time += bench_just_cs2_fast();
	}
	delta(0, all_time);

	/* nested critical sections -- no interruption */
	printf("\nnested critical section -- no interruption:\n");
	for (task_id = 0; task_id < TASKS_NESTED; task_id++) {
		printf("task %c, no trigger:\t\t\t\t", "-DCBA"[task_id]);
		all_time = 0;
		for (run = 0; run < RUNS; run++) {
			all_time += bench2_fast(task_id, 0);
		}
		delta(0, all_time);
	}

	/* nested critical sections -- with interruption */
	printf("\nnested critical section -- with interruption:\n");
	for (task_id = 0; task_id < TASKS_NESTED; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_NESTED; trigger++) {
			printf("task %c, trigger %u:\t\t\t\t", "-DCBA"[task_id], trigger);
			all_time = 0;
			for (run = 0; run < RUNS; run++) {
				all_time += bench2_fast(task_id, trigger);
			}
			delta(0, all_time);
		}
	}


	////////////////////////////////////////////////////////////////////////////


	/* warm up caches */
	for (task_id = 0; task_id < TASKS_SINGLE; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_SINGLE; trigger++) {
			bench1_slow(task_id, trigger);
		}
	}
	for (task_id = 0; task_id < TASKS_NESTED; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_NESTED; trigger++) {
			bench2_slow(task_id, trigger);
		}
	}
	bench_just_cs1_slow();
	bench_just_cs2_slow();

	printf("\n==================== OSPERT 2014 slow runs ======================\n\n");

	/* single critical sections -- no interruption */
	printf("*task -, trigger -:\t\t\t\t");
	all_time = 0;
	for (run = 0; run < RUNS; run++) {
		all_time += bench_just_cs1_slow();
	}
	delta(0, all_time);

	/* single critical sections -- no interruption */
	printf("\nsingle critical section -- no interruption:\n");
	for (task_id = 0; task_id < TASKS_SINGLE; task_id++) {
		printf("task %c, no trigger:\t\t\t\t", "-DCBA"[task_id]);
		all_time = 0;
		for (run = 0; run < RUNS; run++) {
			all_time += bench1_slow(task_id, 0);
		}
		delta(0, all_time);
	}

	/* single critical sections -- with interruption */
	printf("\nsingle critical section -- with interruption:\n");
	for (task_id = 0; task_id < TASKS_SINGLE; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_SINGLE; trigger++) {
			printf("task %c, trigger %u:\t\t\t\t", "-DCBA"[task_id], trigger);
			all_time = 0;
			for (run = 0; run < RUNS; run++) {
				all_time += bench1_slow(task_id, trigger);
			}
			delta(0, all_time);
		}
	}

	/* nested critical sections -- no interruption */
	printf("+task -, trigger -:\t\t\t\t");
	all_time = 0;
	for (run = 0; run < RUNS; run++) {
		all_time += bench_just_cs2_slow();
	}
	delta(0, all_time);

	/* nested critical sections -- no interruption */
	printf("\nnested critical section -- no interruption:\n");
	for (task_id = 0; task_id < TASKS_NESTED; task_id++) {
		printf("task %c, no trigger:\t\t\t\t", "-DCBA"[task_id]);
		all_time = 0;
		for (run = 0; run < RUNS; run++) {
			all_time += bench2_slow(task_id, 0);
		}
		delta(0, all_time);
	}

	/* nested critical sections -- with interruption */
	printf("\nnested critical section -- with interruption:\n");
	for (task_id = 0; task_id < TASKS_NESTED; task_id++) {
		for (trigger = 1; trigger <= TRIGGERS_NESTED; trigger++) {
			printf("task %c, trigger %u:\t\t\t\t", "-DCBA"[task_id], trigger);
			all_time = 0;
			for (run = 0; run < RUNS; run++) {
				all_time += bench2_slow(task_id, trigger);
			}
			delta(0, all_time);
		}
	}

	printf("=============== OSPERT 2014 paper benchmark  END  ===============\n");
	printf("\n\n");

	/* cleanup */
	benchmark_running = false;
	wake(LOWEST_TASK);
	wake(LOWER_TASK);
	wake(HIGHER_TASK);
	wake(HIGHEST_TASK);

	sys_prio_set(orig_prio);
}
