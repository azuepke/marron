/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * test_libc.c
 *
 * Testcases for libc functionality, e.g. optimized memcpy() and memset()
 *
 * azuepke, 2025-02-01: initial
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <marron/api.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"


#define ALIGN		16
#define LEN			48
#define BUFSIZE		(2 * ALIGN + LEN)
#define FILL_A		0
#define FILL_B		50


/* fill memory with incrementing pattern */
static void fill_inc(char *s, size_t n, int c)
{
	while (n--) {
		*s++ = c++;
	}
}

/* check pattern in memory
 *
 *  a_start    a_start+a_len
 *   |                    |
 *   AAAABBBBBBBBBBAAAAAAA
 *       |         |
 *  b_start    b_start+b_len
 *
 * general idea:
 * - A is filled with incremental pattern a_pattern
 * - B is filled with incremental pattern b_pattern (incremented or not)
 * - B is copied over parts of A
 * - check consistency of the memory
 * - return position of defect or -1 if no defect was found
 */
static int check_inc(char *a_start, size_t a_len, int a_pattern, size_t b_start, size_t b_len, int b_pattern, int b_inc)
{
	char *c;
	size_t i;

	assert(b_start+b_len <= a_len);

	c = a_start;
	i = 0;
	for (; i < b_start; i++, c++, a_pattern++) {
		if (*c != (char)a_pattern) {
			goto fail;
		}
	}
	for (; i < b_start + b_len; i++, c++, a_pattern++, b_pattern += b_inc) {
		if (*c != (char)b_pattern) {
			goto fail;
		}
	}
	for (; i < b_len; i++, c++, a_pattern++) {
		if (*c != (char)a_pattern) {
			goto fail;
		}
	}
	return -1;

fail:
	printf("check failed at %zu: *c %d, a_pattern %d, b_pattern %d, b_start %zu, b_len %zu, b_inc %d\n",
	        i, *c, a_pattern, b_pattern, b_start, b_len, b_inc);
	return i;
}

static void test_libc_memcpy(void)
{
	char A[BUFSIZE];
	char B[BUFSIZE];
	char *a;
	char *b;
	void *r;
	int ret;

	/* we copy "b" over "a" in A */
	printf("- libc memcpy: ");
	for (size_t a_start = 0; a_start < ALIGN; a_start++) {
		//printf("#");
		a = &A[a_start];
		for (size_t b_start = 0; b_start < ALIGN; b_start++) {
			//printf("*");
			b = &B[b_start];
			fill_inc(b, LEN, FILL_B);
			for (size_t b_len = 0; b_len < LEN; b_len++) {
				//printf(".");
				fill_inc(A, BUFSIZE, FILL_A);

				r = memcpy(a, b, b_len);
				assert(r == a);

				ret = check_inc(A, BUFSIZE, FILL_A, a_start, b_len, FILL_B, 1);
				assert(ret == -1);
			}
		}
	}
	printf("OK\n");
}

static void test_libc_memset(void)
{
	char A[BUFSIZE];
	char *b;
	void *r;
	int ret;

	/* we clear segment b, b+b_len with pattern B in A */
	printf("- libc memset: ");
	for (size_t b_start = 0; b_start < ALIGN; b_start++) {
		//printf("#");
		b = &A[b_start];
		for (size_t b_len = 0; b_len < LEN; b_len++) {
			//printf(".");
			fill_inc(A, BUFSIZE, FILL_A);

			r = memset(b, FILL_B, b_len);
			assert(r == b);

			ret = check_inc(A, BUFSIZE, FILL_A, b_start, b_len, FILL_B, 0);
			assert(ret == -1);
		}
	}
	printf("OK\n");
}

////////////////////////////////////////////////////////////////////////////////

struct job {
	uint32_t fd;
	uint32_t flags;
	char *buf;
	size_t size;
};

static void test_ipc_copy_thread(void *arg)
{
	struct sys_ipc msg = { 0 };
	struct job *job = arg;
	err_t err;

	msg.flags = job->flags;
	msg.buf0 = job->buf;
	msg.size0 = job->size;
	err = sys_ipc_call(job->fd, &msg, &msg);
	assert_eq(err, EPIPE);

	sys_thread_exit();
}

static uint32_t test_ipc_copy_prepare(uint32_t fds[2], void *buf, size_t size)
{
	struct sys_ipc msg = { 0 };
	struct job this_job;
	struct job *job = &this_job;
	uint32_t rhnd;
	err_t err;

	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	job->fd = fds[1];
	job->flags = SYS_IPC_BUF0W;
	job->buf = buf;
	job->size = size;

	/* create IPC client thread */
	create_thread(1, test_ipc_copy_thread, job, sys_prio_get());

	/* let client run */
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	return rhnd;
}

static void test_ipc_copy_cleanup(uint32_t fds[2])
{
	err_t err;

	/* close fds while the client is waiting in IPC REPLY to terminate thread */
	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EOK);

	/* let client run */
	sys_yield();
}

static void test_ipc_memcpy(void)
{
	char A[BUFSIZE];
	char B[BUFSIZE];
	//char *a;
	char *b;
	//void *r;
	int ret;

	uint32_t fds[2];
	uint32_t rhnd;
	size_t written_size;
	err_t err;

	rhnd = test_ipc_copy_prepare(fds, A, BUFSIZE);
	(void)rhnd;

	/* we copy "b" over "a" in A */
	printf("- IPC memcpy: ");
	for (size_t a_start = 0; a_start < ALIGN; a_start++) {
		//printf("#");
		for (size_t b_start = 0; b_start < ALIGN; b_start++) {
			//printf("*");
			b = &B[b_start];
			fill_inc(b, LEN, FILL_B);
			for (size_t b_len = 0; b_len < LEN; b_len++) {
				//printf(".");
				fill_inc(A, BUFSIZE, FILL_A);

				//r = memcpy(a, b, b_len);
				err = sys_ipc_write(rhnd, 0, b, b_len, a_start, &written_size);
				assert_eq(err, EOK);
				assert_eq(written_size, b_len);

				ret = check_inc(A, BUFSIZE, FILL_A, a_start, b_len, FILL_B, 1);
				assert(ret == -1);
			}
		}
	}

	test_ipc_copy_cleanup(fds);

	printf("OK\n");
}

void test_libc(void)
{
	print_header("libc implementation");

	test_libc_memcpy();
	test_libc_memset();
	test_ipc_memcpy();
}
