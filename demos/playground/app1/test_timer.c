/* SPDX-License-Identifier: MIT */
/* Copyright 2021 Alexander Zuepke */
/*
 * test_timer.c
 *
 * Test timer and clock implementation
 *
 * azuepke, 2021-11-15: initial
 */

#include <marron/api.h>
#include <marron/mapping.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"


void test_timer(void)
{
	struct sys_sig_info info = { 0 };
	unsigned int overrun_count;
	sys_timeout_t remaining;
	uint64_t expiry_count;
	unsigned int timer_id;
	sys_time_t min_delta;
	sys_time_t max_delta;
	sys_time_t period;
	sys_time_t start;
	sys_time_t delta;
	sys_time_t last;
	sys_time_t now;
	unsigned int i;
	err_t err;

	print_header("Kernel Timer Implementation");

	//----------------------------------------------------------------------

	printf("- monotonic clock: ");

	err = sys_clock_get(CLK_ID_MONOTONIC, &start);
	assert_eq(err, EOK);
	last = start;
	min_delta = 1000*1000*1000;
	max_delta = 0;
	do {
		err = sys_clock_get(CLK_ID_MONOTONIC, &now);
		assert_eq(err, EOK);
		assert3(now, >=, last);
		delta = now - last;
		if (delta < min_delta) {
			min_delta = delta;
		}
		if (delta > max_delta) {
			max_delta = delta;
		}
		last = now;
	} while (now < start + 1000*1000*1000);

	printf("OK, min_delta %llu ns, max_delta %llu ns\n",
	       (unsigned long long)min_delta,
	       (unsigned long long)max_delta);

	//----------------------------------------------------------------------

	printf("- basic timer API: ");

	assert_eq(CLK_ID_REALTIME, 0);
	assert_eq(CLK_ID_MONOTONIC, 1);
	assert_eq(NUM_CLK_IDS, 2);

	/* expected to work */
	err = sys_timer_create(0, 0, NULL, 0, &timer_id);
	assert_eq(err, EOK);
	assert(timer_id == 0);


	/* the following tests expect timers="3" in the partition config */
	/* second timer -- available */
	err = sys_timer_create(0, 0, NULL, 0, &timer_id);
	assert_eq(err, EOK);
	assert(timer_id == 1);

	/* third timer -- available */
	err = sys_timer_create(0, 0, NULL, 0, &timer_id);
	assert_eq(err, EOK);
	assert(timer_id == 2);

	/* fourth timer -- not available */
	err = sys_timer_create(0, 0, NULL, 0, &timer_id);
	assert_eq(err, ELIMIT);

	/* delete second timer */
	err = sys_fd_close(1);
	assert_eq(err, EOK);

	/* second timer -- available again */
	err = sys_timer_create(0, 0, NULL, 0, &timer_id);
	assert_eq(err, EOK);
	assert(timer_id == 1);

	/* delete second timer and third timer */
	err = sys_fd_close(1);
	assert_eq(err, EOK);
	err = sys_fd_close(2);
	assert_eq(err, EOK);


	/* invalid timer ID pointer */
	err = sys_timer_create(0, 0, NULL, 0, NULL);
	assert_eq(err, EINVAL);

	/* invalid clock */
	err = sys_timer_create(NUM_CLK_IDS, 0, NULL, 0, &timer_id);
	assert_eq(err, EINVAL);

	/* invalid flags */
	err = sys_timer_create(0, ~0, NULL, 0, &timer_id);
	assert_eq(err, EINVAL);

	/* invalid pointer */
	err = sys_timer_create(0, 0, (void*)0x1, 0, &timer_id);
	assert_eq(err, EFAULT);

	/* invalid signal */
	info.sig = NUM_SIGS;
	err = sys_timer_create(0, 0, &info, 0, &timer_id);
	assert_eq(err, EINVAL);

	/* invalid thread */
	info.sig = 0;
	err = sys_timer_create(0, 0, &info, 1000, &timer_id);
	assert_eq(err, ELIMIT);

	/* expected to work */
	err = sys_fd_close(0);
	assert_eq(err, EOK);

	/* timer not enabled */
	err = sys_fd_close(0);
	assert_eq(err, EBADF);


	/* timer not enabled */
	err = sys_timer_set(0, 0, 0, 0);
	assert_eq(err, EBADF);

	/* invalid flags */
	err = sys_timer_set(0, ~0, 0, 0);
	assert_eq(err, EINVAL);


	/* timer not enabled */
	err = sys_timer_get(0, NULL, NULL);
	assert_eq(err, EBADF);


	/* timer not enabled */
	err = sys_timer_overrun(0, NULL);
	assert_eq(err, EBADF);


	/* timer not enabled */
	err = sys_timer_wait(0, 0, NULL);
	assert_eq(err, EBADF);

	printf("OK\n");

	//----------------------------------------------------------------------

	printf("- monotonic timer in non-signal mode: ");

	/* expected to work */
	err = sys_timer_create(CLK_ID_MONOTONIC, 0, NULL, 0, &timer_id);
	assert_eq(err, EOK);
	assert(timer_id == 0);

	/* periodic 10ms timer after 100ms */
	err = sys_timer_set(0, 0, 100*1000*1000, 10*1000*1000);
	assert_eq(err, EOK);

	/* invalid pointer */
	err = sys_timer_get(0, (void*)0x1, NULL);
	assert_eq(err, EFAULT);

	/* invalid pointer */
	err = sys_timer_get(0, NULL, (void*)0x1);
	assert_eq(err, EFAULT);

	/* expected to work */
	err = sys_timer_get(0, &remaining, &period);
	assert_eq(err, EOK);
	assert3(remaining, <=, 100*1000*1000);
	assert_eq(period, 10*1000*1000);


	/* invalid pointer */
	err = sys_timer_overrun(0, (void*)0x1);
	assert_eq(err, EFAULT);

	/* expected to work */
	err = sys_timer_overrun(0, &overrun_count);
	assert_eq(err, EOK);
	assert_eq(overrun_count, 0);


	/* polling mode && invalid pointer (not reached) */
	err = sys_timer_wait(0, 0, (void*)0x1);
	assert_eq(err, EAGAIN);

	/* polling mode && expected to work */
	err = sys_timer_wait(0, 0, NULL);
	assert_eq(err, EAGAIN);

	/* polling mode && expected to work */
	err = sys_timer_wait(0, 0, &expiry_count);
	assert_eq(err, EAGAIN);


	printf("wait: ");

	/* waiting mode && expected to work */
	err = sys_timer_wait(0, 1, &expiry_count);
	assert_eq(err, EOK);
	assert_eq(expiry_count, 1);

	err = sys_clock_get(CLK_ID_MONOTONIC, &start);
	assert_eq(err, EOK);
	assert3(start, >, 0);

	/* wait for at least 100 timer expirations (1 second overall) */
	i = 0;
	while (i < 100) {
		/* waiting mode && expected to work */
		err = sys_timer_wait(0, 1, &expiry_count);
		assert_eq(err, EOK);
		/* NOTE: unstable timing on QEMU, accept delays up to 100ms */
		assert3(expiry_count, >=, 1);
		assert3(expiry_count, <=, 10);
		i += expiry_count;
	}

	err = sys_clock_get(CLK_ID_MONOTONIC, &now);
	assert_eq(err, EOK);
	assert3(now, >, 0);
	assert3(start, <, now);
	assert3(now - start, >=, (i-1)*10*1000*1000);
	assert3(now - start, <=, (i+1)*10*1000*1000);

	printf("disable: ");

	/* expected to work */
	err = sys_fd_close(0);
	assert_eq(err, EOK);

	printf("OK\n");

	//----------------------------------------------------------------------

	printf("- monotonic timer in signal mode: ");

	/* expected to work */
	info.sig = 14;	/* SIGALRM */
	info.code = 0xc;
	info.ex_type = 0xe;
	info.status = 0x5;
	info.addr = (ulong_t)&info;
	err = sys_timer_create(CLK_ID_MONOTONIC, 0, &info, 0, &timer_id);
	assert_eq(err, EOK);

	/* oneshot timer after 100ms */
	err = sys_timer_set(0, 0, 100*1000*1000, 0);
	assert_eq(err, EOK);

	printf("wait: ");

	err = sys_sig_wait(SIG_TO_MASK(info.sig), 0, TIMEOUT_INFINITE, CLK_ID_MONOTONIC, &info);
	assert_eq(err, EOK);
	assert_eq(info.sig, 14);		/* unchanged */
	assert_eq(info.code, 0xc);	/* unchanged */
	assert_eq(info.ex_type, 0);	/* set to timer ID (==0) */
	assert_eq(info.status, 0);	/* expiry_count (==0) */
	assert_eq(info.addr, (ulong_t)&info);

	/* timeout expires (returns EAGAIN instead of ETIMEDOUT) */
	err = sys_sig_wait(SIG_TO_MASK(info.sig), 0, 100*1000*1000, CLK_ID_MONOTONIC, &info);
	assert_eq(err, EAGAIN);

	printf("disable: ");

	/* expected to work */
	err = sys_fd_close(0);
	assert_eq(err, EOK);

	printf("OK\n");

	//----------------------------------------------------------------------

	printf("- real-time timer in non-signal mode: ");

	/* expected to work */
	err = sys_timer_create(CLK_ID_REALTIME, 0, NULL, 0, &timer_id);
	assert_eq(err, EOK);
	assert(timer_id == 0);

	/* current time to compose an absolute timeout */
	err = sys_clock_get(CLK_ID_REALTIME, &now);
	assert_eq(err, EOK);
	assert3(now, >, 0);

	/* absolute periodic 10ms timer after 100ms */
	err = sys_timer_set(0, TIMEOUT_ABSOLUTE, now + 100*1000*1000, 10*1000*1000);
	assert_eq(err, EOK);

	/* expected to work */
	err = sys_timer_get(0, &remaining, &period);
	assert_eq(err, EOK);
	assert3(remaining, <=, now + 100*1000*1000);
	assert_eq(period, 10*1000*1000);

	printf("wait: ");

	/* waiting mode && expected to work */
	err = sys_timer_wait(0, 1, &expiry_count);
	assert_eq(err, EOK);
	assert_eq(expiry_count, 1);

	err = sys_clock_get(CLK_ID_REALTIME, &start);
	assert_eq(err, EOK);
	assert3(start, >, 0);

	/* wait for at least 100 timer expirations (1 second overall) */
	i = 0;
	while (i < 100) {
		/* waiting mode && expected to work */
		err = sys_timer_wait(0, 1, &expiry_count);
		assert_eq(err, EOK);
		/* NOTE: unstable timing on QEMU, accept delays up to 100ms */
		assert3(expiry_count, >=, 1);
		assert3(expiry_count, <=, 10);
		i += expiry_count;
	}

	err = sys_clock_get(CLK_ID_REALTIME, &now);
	assert_eq(err, EOK);
	assert3(now, >, 0);
	assert3(start, <, now);
	assert3(now - start, >=, (i-1)*10*1000*1000);
	assert3(now - start, <=, (i+1)*10*1000*1000);

	printf("disable: ");

	/* expected to work */
	err = sys_fd_close(0);
	assert_eq(err, EOK);

	printf("OK\n");
}
