/* SPDX-License-Identifier: MIT */
/* Copyright 2017-2022 Alexander Zuepke */
/*
 * main.c
 *
 * Example OS -- test code
 *
 * azuepke, 2017-07-17: initial
 * azuepke, 2022-02-12: cycle counter handling
 */

#include <marron/api.h>
#include <marron/stack.h>
#include <marron/mapping.h>
#include <marron/arch_tls.h>
#include <marron/macros.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include <marron/util.h>
#include "app.h"
#include "my_tls.h"

/* stacks */
#define STACK_SIZE 8192
#define NUM_THREAD 17
SYS_STACK_DEFINE(thread_stack[NUM_THREAD], STACK_SIZE);

/* TLS areas */
struct my_tls thread_tls[NUM_THREAD];

void create_thread_cpu(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio, unsigned int cpu)
{
	struct sys_sched_param sched;
	unsigned long cpu_mask;
	unsigned int thr_id2;
	void *stack;
	void *tls;
	err_t err;

	assert(thr_id >= 1);
	assert(thr_id <= NUM_THREAD);

	stack = SYS_STACK_PTR(thread_stack[thr_id]);
	tls = &thread_tls[thr_id];

	sched.prio = prio;
	sched.quantum = 0;
	cpu_mask = 1ul << cpu;

	err = sys_thread_create(&thr_id2, func, arg, stack, tls, &sched, cpu_mask);
	assert_eq(err, EOK);
	assert(thr_id2 == thr_id);
}

void create_thread(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio)
{
	create_thread_cpu(thr_id, func, arg, prio, 0);
}

void join_thread(unsigned int thr_id)
{
	/* empty on Marron */
	(void)thr_id;
}

void print_line(void)
{
	printf("************************************************************************\n");
}

void print_header(const char *s)
{
	printf("\n");
	print_line();
	printf("*** Testcase: %s\n", s);
	print_line();
}

/** cycle counter frequency in HZ */
unsigned long get_tsc_freq(void)
{
	uint64_t t0, t1, t2;
	unsigned long before, after;
	uint32_t delta_t, delta_c;

	/* wait for first ticket increment */
	sys_clock_get(CLK_ID_MONOTONIC, &t0);
	do {
		sys_clock_get(CLK_ID_MONOTONIC, &t1);
	} while (t1 == t0);
	before = get_tsc();
	/* wait for at least 100ms */
	do {
		sys_clock_get(CLK_ID_MONOTONIC, &t2);
	} while ((t2 - t1) < 100*1000*1000);
	after = get_tsc();

	delta_t = t2 - t1;
	delta_c = after - before;

#if __SIZEOF_LONG__ == 8
	/* native 64-bit division available */
	return (unsigned long)delta_c * 1000000000 / delta_t;
#else
	/* fallback -- approximated result */
	(void)delta_t;
	return 10 * delta_c;
#endif
}

/** configure PMU counter */
err_t pmu_ctrl(unsigned int counter, unsigned int value)
{
	static int kldd_id = -1;
	err_t err;

	if (kldd_id == -1) {
		kldd_id = config_kldd_id("pmu_ctrl");
	}
	if (kldd_id == -1) {
		return ENOSYS;
	}

	err = sys_kldd_call(kldd_id, counter, value, 0, 0);
	return err;
}

////////////////////////////////////////////////////////////////////////////////

static unsigned long long var64 = 0x0123456789abcdef;

/* bare-metal application entry point */
/* NOTE: this entry point is usually private to the libc implementation */
void _start_c(void *arg1 __unused, size_t arg2 __unused, void *arg3 __unused, size_t arg4 __unused, void *arg5 __unused);
void _start_c(void *arg1 __unused, size_t arg2 __unused, void *arg3 __unused, size_t arg4 __unused, void *arg5 __unused)
{
	volatile uint32_t *shm1;
	char part_name[32];
	char proc_name[32];
	char bsp_name[32];
	char *rom_file;
	err_t err;

	/* minimal TLS setup */
	for (unsigned int i = 0; i < NUM_THREAD; i++) {
		thread_tls[i].tid = i;
	}

	err = sys_tls_set(&thread_tls[0]);
	if (err != EOK) {
		sys_abort();
	}


	/* welcome! */
	printf("\n");
	print_line();
	printf("*** TESTSUITE");
#ifdef COVBUILD
	printf(" +COVBUILD");
#endif
#ifdef AUTOBUILD
	printf(" +AUTOBUILD");
#endif
	printf("\n");
	print_line();

	/* access (obviously read-only) mapping of "my_hello" file in ROM image */
	printf("# ROM file ID: %d\n", config_memrq_id("my_hello", MEMRQ_TYPE_FILE));

	rom_file = config_memrq_map("my_hello", MEMRQ_TYPE_FILE, PAGE_SIZE);
	assert_ne(rom_file, (void *)-1);
	printf("# ROM file content: %s\n", rom_file);
	err = sys_vm_unmap((addr_t)rom_file, PAGE_SIZE);
	assert_eq(err, EOK);

	/*
	 * shm1[0] == Magic Word (0x42424242)
	 * shm1[1] == Test Counter
	 * shm1[2] == Spinlock for Part 2
	 * shm1[3] == Test Select for Part 2
	 * shm1[4] == Part 2 Test State Variable
	 */
	shm1 = config_memrq_map(SHM_MAP_NAME, MEMRQ_TYPE_SHM, SHM_MAP_SIZE);
	assert_ne(shm1, (void *)-1);
	printf("# SHM1 addess: %p\n", shm1);
	/* check shm is init */
	if (shm1[0] != 0x42424242) {
		/* init shm */
		shm1[0] = 0x42424242;
		shm1[1] = 0;
		shm1[2] = 0;
		shm1[3] = 0;
		shm1[4] = 0;
	}

	printf("# Hello World from user space\n");
	/* test crt0, i.e. if the data segment is set up properly */
	printf("# Variable var64 is 0x%llx\n", var64);
	assert_eq(var64, 0x0123456789abcdef);

	printf("# main() arg1: 0x%p, arg2: 0x%zx\n", arg1, arg2);
	printf("# main() arg3: 0x%p, arg4: 0x%zx\n", arg3, arg4);
	printf("# main() arg5: 0x%p\n", arg5);

	printf("# TLS: 0x%p, 0x%p, 0x%p, 0x%p\n",
	       &thread_tls[0], thread_tls[0].sys.tls, (void*)__tls_base(),
	       tls_get_p(offsetof(struct my_tls, sys.tls)));
	assert_eq(&thread_tls[0], thread_tls[0].sys.tls);
	assert_eq(&thread_tls[0], (void*)__tls_base());
	assert_eq(&thread_tls[0], tls_get_p(offsetof(struct my_tls, sys.tls)));

	printf("# priority: %u %u\n", sys_prio_get(), thread_tls[0].sys.user_prio);
	assert_eq(sys_prio_get(), thread_tls[0].sys.user_prio);

	err = sys_part_name(part_name, sizeof(part_name));
	assert_eq(err, EOK);

	printf("# partition ID %u '%s' state %u\n", sys_part_self(), part_name, sys_part_state());
	assert_eq(sys_part_state(), PART_STATE_RUNNING);

	err = sys_process_name(proc_name, sizeof(proc_name));
	printf("# process ID %u '%s'\n", sys_process_self(), proc_name);

	err = sys_bsp_name(bsp_name, sizeof(bsp_name));
	assert_eq(err, EOK);
	printf("# bsp name: %s\n", bsp_name);
	printf("\n");

#ifndef COVBUILD
	/* benchmarks */
	bench_cpu();
	bench_api();
	bench_ipc();
	bench_ospert2014();
#endif

	/* run tests */
	test_prio();
	test_sched();
	test_suspend_resume();
	test_suspend_resume_smp();
	test_sched_rr();
	test_timer();
	test_signals();
	test_signals_stacks();
	test_signals_queued();
	test_timer_irq();
	test_futex();
	test_mapping();
	test_ipc();
	test_ipc_io();
	test_memrq_io();
	test_epoll();
	test_eventfd();
	test_timerfd();
	test_signalfd();
	test_libc();
	test_kldd();
	test_partition(shm1);


	printf("### all tests done! ###\n");
	printf("Test Count: %u \n", shm1[1] + 1);
#ifdef COVBUILD
	printf("# Shutdown\n");
	/* poweroff */
	sys_bsp_shutdown(2);
#endif
#ifndef AUTOBUILD
	shm1[1]++;
	printf("# last Test restart partion self\n");
	sys_part_restart();
#else
	if (shm1[1]++ < 1) {
		printf("# last Test restart partion self\n");
		sys_part_restart();
	} else {
		printf("# Shutdown\n");
		printf("AUTOBUILD: OK\n");
		/* poweroff */
		sys_bsp_shutdown(2);
	}
#endif
}
