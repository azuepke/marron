/* SPDX-License-Identifier: MIT */
/* Copyright 2018, 2021, 2022 Alexander Zuepke */
/*
 * test_signals.c
 *
 * Signal testcases
 *
 * azuepke, 2018-03-19: initial
 * azuepke, 2021-11-16: queued signals
 * azuepke, 2022-01-25: architecture specific exceptions
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#include <marron/regs.h>
#include <marron/signal.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"


#define SIGCANCEL 16

static volatile unsigned int sig_expect;
static volatile unsigned int sig_pc_fixup;
static volatile unsigned int sig_last;
static volatile unsigned int sig_count;

/* raise an illegal instruction */
static void do_illegal(void)
{
	sig_expect = SIGILL;

#if defined __thumb__
	sig_pc_fixup = 4;
	__asm__ volatile (".inst.n 0xeee8, 0x1a10" : : : "memory");	/* vmsr fpexc, r1 */
#elif defined __arm__
	sig_pc_fixup = 4;
	__asm__ volatile (".inst 0xeee81a10" : : : "memory");	/* vmsr fpexc, r1 */
#elif defined __aarch64__
	sig_pc_fixup = 4;
	__asm__ volatile (".inst 0" : : : "memory");	/* illegal instruction */
#elif defined __riscv
	sig_pc_fixup = 2;
	__asm__ volatile (".half 0" : : : "memory");	/* illegal instruction */
#else
#error Adapt this file to your CPU architecture!
#endif

	sig_expect = 0;
}

/* raise a pagefault exception */
static void do_pagefault(void)
{
	addr_t addr = 0xffff0000;
	unsigned long tmp;

	sig_expect = SIGSEGV;

#if defined __thumb__ || defined __arm__ || defined __aarch64__
#if defined __thumb__
	sig_pc_fixup = 2;
#else
	sig_pc_fixup = 4;
#endif

	__asm__ volatile ("ldr	%0, [%1]" : "=&r"(tmp) : "r"(addr) : "memory");
#elif defined __riscv
	sig_pc_fixup = 4;
	__asm__ volatile (".option push\n"
	                  ".option norvc\n"
	                  "lb	%0, 0(%1)\n"
	                  ".option pop" : "=&r"(tmp) : "r"(addr) : "memory");
#else
#error Adapt this file to your CPU architecture!
#endif

	sig_expect = 0;
}

/* raise special breakpoint exception */
static void do_breakpoint(void)
{
	sig_expect = SIGTRAP;

#if defined __thumb__
	sig_pc_fixup = 2;
	__asm__ volatile ("bkpt #0" : : : "memory");	/* breakpoint instruction */
#elif defined __arm__
	sig_pc_fixup = 4;
	__asm__ volatile ("bkpt #0" : : : "memory");	/* breakpoint instruction */
#elif defined __aarch64__
	sig_pc_fixup = 4;
	__asm__ volatile ("brk #0" : : : "memory");		/* breakpoint instruction */
#elif defined __riscv
	sig_pc_fixup = 4;
	__asm__ volatile (".option push\n"
	                  ".option norvc\n"
	                  "ebreak\n"
	                  ".option pop" : : : "memory");		/* breakpoint instruction */
#else
#error Adapt this file to your CPU architecture!
#endif

	sig_expect = 0;
}

/* raise exception from halt instruction */
static void do_halt(void)
{
	#ifdef AUTOBUILD
	/* NOTE: does not work if JTAG is connected */
	sig_last = SIGILL;
	sig_count++;
	return;
	#endif

	sig_expect = SIGILL;

#if defined __thumb__
	sig_pc_fixup = 2;
	__asm__ volatile ("hlt #0" : : : "memory");	/* debug halt */
#elif defined __arm__
	sig_pc_fixup = 4;
	__asm__ volatile ("hlt #0" : : : "memory");	/* debug halt */
#elif defined __aarch64__
	sig_pc_fixup = 4;
	__asm__ volatile ("hlt #0" : : : "memory");	/* debug halt */
#elif defined __riscv
	sig_pc_fixup = 2;
	__asm__ volatile (".half 0" : : : "memory");	/* illegal instruction */
#else
#error Adapt this file to your CPU architecture!
#endif

	sig_expect = 0;
}

/* raise exception from debug instruction */
static void do_debug(void)
{
	sig_expect = SIGILL;

#if defined __thumb__
	sig_pc_fixup = 4;
	__asm__ volatile ("rfeia sp!" : : : "memory");	/* system instruction */
#elif defined __arm__
	sig_pc_fixup = 4;
	__asm__ volatile ("rfeia sp!" : : : "memory");	/* system instruction */
#elif defined __aarch64__
	sig_pc_fixup = 4;
	__asm__ volatile ("drps" : : : "memory");	/* debug restore previous state */
#elif defined __riscv
	sig_pc_fixup = 4;
	__asm__ volatile ("sret" : : : "memory");	/* system instruction */
#else
#error Adapt this file to your CPU architecture!
#endif

	sig_expect = 0;
}

static void handler(struct regs *regs, sys_sig_mask_t sig_mask, struct sys_sig_info *info)
{
	unsigned int sig = info->sig;

	sig_last = sig;
	sig_count++;
	printf("got signal %u %s\n", sig, __strsignal(sig));

	if (sig == sig_expect) {
		/* expected signal for exception signal test */
		assert(sig_expect > 0);
		assert(sig_pc_fixup > 0);
		regs->pc += sig_pc_fixup;
	} else {
		/* asynchronous signals */
		switch (sig) {
		case SIGCANCEL:
			/* just OK */
			break;
		case 30:
			/* just OK */
			break;
		case 31:
			/* just OK */
			break;
		default:
			printf("got signal %u %s\n", sig, __strsignal(sig));
			assert(0);
			break;
		}
	}

	sys_sig_return(regs, sig_mask);
	/* must not return */
	assert(0);
}

void test_signals(void)
{
	sys_sig_mask_t sig_mask;
	err_t err;

	print_header("Signal Handling");

	/* unmask all signal */
	sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);

	/* register handlers for SIGILL, SIGTRAP, SIGSEGV, SIGCANCEL, and signals #30, #31 */
	err = sys_sig_register(SIGILL, handler, SIG_MASK_NONE, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(SIGTRAP, handler, SIG_MASK_NONE, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(SIGSEGV, handler, SIG_MASK_NONE, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(SIGCANCEL, handler, SIG_MASK_NONE, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(30, handler, SIG_MASK_ALL, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(31, handler, SIG_MASK_NONE, 0);
	assert_eq(err, EOK);

	sig_count = 0;

	printf("* test illegal instruction: ");
	sig_last = 0;
	do_illegal();
	assert_eq(sig_last, SIGILL);
	assert_eq(sig_count, 1);
	printf("OK\n");

	printf("* test async signal #1: ");
	sig_last = 0;
	err = sys_sig_send(sys_thread_self(), SIGCANCEL, NULL);
	assert_eq(err, EOK);
	assert_eq(sig_last, SIGCANCEL);
	assert_eq(sig_count, 2);
	printf("OK\n");

	printf("* test async signal #2: ");
	sig_last = 0;
	err = sys_sig_send(sys_thread_self(), 31, NULL);
	assert_eq(err, EOK);
	assert_eq(sig_last,  31);
	assert_eq(sig_count, 3);
	printf("OK\n");

	printf("* mask signals: ");
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_TO_MASK(SIGCANCEL) | SIG_TO_MASK(31));
	assert_eq(sig_mask, 0);
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_MASK_NONE);
	assert_eq(sig_mask, (SIG_TO_MASK(SIGCANCEL) | SIG_TO_MASK(31)));
	printf("OK\n");

	printf("* test masked signals: ");
	sig_last = 0;
	err = sys_sig_send(sys_thread_self(), 31, NULL);
	assert_eq(err, EOK);
	assert_eq(sig_last,  0);
	assert_eq(sig_count, 3);
	err = sys_sig_send(sys_thread_self(), SIGCANCEL, NULL);
	assert_eq(err, EOK);
	/* no signal must be delivered while the signals are masked */
	assert_eq(sig_last,  0);
	assert_eq(sig_count, 3);
	printf("OK\n");

	printf("* test nested signals: ");
	/* unmask both signals, they get delivered in a nested fashion */
	sig_mask = sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);
	assert_eq(sig_mask, (SIG_TO_MASK(SIGCANCEL) | SIG_TO_MASK(31)));
	/* first, SIGCANCEL, then signal #31 is delivered */
	/* but signal #31 preempts SIGCANCEL, so SIGCANCEL is observed last */
	assert_eq(sig_last,  SIGCANCEL);
	assert_eq(sig_count, 5);
	printf("OK\n");

	printf("* test nested signals with other signals masked in handler: ");
	sig_last = 0;
	/* block all signals first */
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_MASK_ALL);
	assert_eq(sig_mask, SIG_MASK_NONE);
	/* send both signals 30 and 31 */
	err = sys_sig_send(sys_thread_self(), 30, NULL);
	assert_eq(err, EOK);
	err = sys_sig_send(sys_thread_self(), 31, NULL);
	assert_eq(err, EOK);
	/* unmask both signals, they get delivered in a nested fashion */
	sig_mask = sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);
	assert_eq(sig_mask, SIG_MASK_ALL);
	/* at first, signal #30 and then signal #31 is delivered,
	 * but this time signal #31 cannot preempts #30,
	 * so signal #31 is observed last
	 */
	assert_eq(sig_last,  31);
	assert_eq(sig_count, 7);
	printf("OK\n");

	printf("* test pagefault handler: ");
	sig_last = 0;
	do_pagefault();
	assert_eq(sig_last,  SIGSEGV);
	assert_eq(sig_count, 8);
	/* unregister handlers */
	printf("OK\n");

	printf("* test breakpoint instruction: ");
	sig_last = 0;
	do_breakpoint();
	assert_eq(sig_last, SIGTRAP);
	assert_eq(sig_count, 9);
	printf("OK\n");

	printf("* test halt instruction: ");
	sig_last = 0;
	do_halt();
	assert_eq(sig_last, SIGILL);
	assert_eq(sig_count, 10);
	printf("OK\n");

	printf("* test debug instruction: ");
	sig_last = 0;
	do_debug();
	assert_eq(sig_last, SIGILL);
	assert_eq(sig_count, 11);
	printf("OK\n");

	/* unregister handlers */
	err = sys_sig_register(SIGILL, NULL, 0x0, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(SIGTRAP, NULL, 0x0, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(SIGCANCEL, NULL, 0x0, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(SIGSEGV, NULL, 0x0, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(30, NULL, 0x0, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(31, NULL, 0x0, 0);
	assert_eq(err, EOK);

	/* restore signal mask */
	sig_mask = sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);
	assert_eq(sig_mask, 0);

	printf("\n");
}

static addr_t stack42, stack43, stack44;

static void handler2(struct regs *regs, sys_sig_mask_t sig_mask, struct sys_sig_info *info)
{
	assert((info->sig == 42) || (info->sig == 43) || (info->sig == 44));

	printf("- got signal %u on stack %p\n", info->sig, regs);

	switch (info->sig) {
	case 42:
		stack42 = (addr_t)regs;
		/* now enable delivery of signal 43 */
		printf("- enable signal 43\n");
		sys_sig_mask(SIG_TO_MASK(43), SIG_MASK_NONE);
		printf("- after enable signal 43\n");
		break;
	case 43:
		stack43 = (addr_t)regs;
		/* now enable delivery of signal 44 */
		printf("- enable signal 44\n");
		sys_sig_mask(SIG_TO_MASK(44), SIG_MASK_NONE);
		printf("- after enable signal 44\n");
		break;
	case 44:
		stack44 = (addr_t)regs;
		break;
	default:
		assert(0);
		break;
	}

	printf("- return from signal %u\n", info->sig);

	sys_sig_return(regs, sig_mask);
	/* must not return */
	assert(0);
}

void test_signals_stacks(void)
{
	sys_sig_mask_t sig_mask;
	char stack[0x1000];
	err_t err;

	print_header("Signal Stack Handling");

	stack42 = 0;
	stack43 = 0;
	stack44 = 0;

	printf("signal stack: %p--%p\n", stack, stack + sizeof(stack));

	/* mask all signal */
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_MASK_ALL);

	/* register handlers */
	err = sys_sig_register(42, handler2, SIG_MASK_ALL, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(43, handler2, SIG_MASK_ALL, SYS_SIG_STACK);
	assert_eq(err, EOK);
	err = sys_sig_register(44, handler2, SIG_MASK_ALL, 0);
	assert_eq(err, EOK);

	/* set a stack */
	err = sys_sig_stack(NULL, 0);
	assert_eq(err, EOK);

	err = sys_sig_stack(NULL, 1);
	assert_eq(err, EINVAL);

	err = sys_sig_stack((void *)0x1, 0);
	assert_eq(err, EINVAL);

	err = sys_sig_stack(stack, sizeof(stack));
	assert_eq(err, EOK);

	/* queue signals */
	err = sys_sig_send(0, 42, NULL);
	assert_eq(err, EOK);
	err = sys_sig_send(0, 43, NULL);
	assert_eq(err, EOK);
	err = sys_sig_send(0, 44, NULL);
	assert_eq(err, EOK);

	printf("- enable signal 42\n");

	/* allow first signal to happen */
	sys_sig_mask(SIG_TO_MASK(42), SIG_MASK_NONE);

	printf("- after enable signal 42\n");

	/* signal 42 delivered outside of the signal stack */
	assert(stack42 != 0);
	assert((stack42 <= (addr_t)stack) || (stack42 >= (addr_t)stack + sizeof(stack)));

	/* signal 43 delivered on the signal stack */
	assert(stack43 != 0);
	assert((stack43 >= (addr_t)stack) && (stack43 <= (addr_t)stack + sizeof(stack)));

	/* signal 44 delivered on the signal stack in a nested fashion */
	assert(stack44 != 0);
	assert((stack44 >= (addr_t)stack) && (stack44 <= (addr_t)stack + sizeof(stack)));
	assert(stack44 < stack43);

	/* unregister handlers */
	err = sys_sig_register(42, NULL, 0x0, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(43, NULL, 0x0, 0);
	assert_eq(err, EOK);
	err = sys_sig_register(44, NULL, 0x0, 0);
	assert_eq(err, EOK);

	/* disable signal stack */
	err = sys_sig_stack(NULL, 0);
	assert_eq(err, EOK);

	/* restore signal mask */
	sys_sig_mask(SIG_MASK_ALL, sig_mask);

	printf("\n");
}

void test_signals_queued(void)
{
	struct sys_sig_info info;
	sys_sig_mask_t sig_mask;
	sys_sig_mask_t pending;
	err_t err;

	print_header("Queued Signal Handling");

	/* mask all signal */
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_MASK_ALL);

	printf("- enqueue signals\n");

	/* enqueue up to 5 signals */
	info.sig = 0;	/* overwritten by kernel*/
	info.code = 0xc;
	info.ex_type = 0xe;
	info.status = 0x5;
	info.addr = 1;
	err = sys_sig_send(0, 42, &info);
	assert_eq(err, EOK);

	info.addr = 2;
	err = sys_sig_send(0, 43, &info);
	assert_eq(err, EOK);

	info.addr = 3;
	err = sys_sig_send(0, 42, &info);
	assert_eq(err, EOK);

	info.addr = 4;
	err = sys_sig_send(0, 43, &info);
	assert_eq(err, EOK);

	info.addr = 5;
	err = sys_sig_send(0, 42, &info);
	assert_eq(err, EOK);

	/* cannot enqueue more signals */
	info.addr = 6;
	err = sys_sig_send(0, 43, &info);
	assert_eq(err, EAGAIN);

	pending = sys_sig_pending();
	assert(pending == (SIG_TO_MASK(42) | SIG_TO_MASK(43)));

	printf("- poll queued signals\n");

	/* poll signals */
	err = sys_sig_wait(SIG_MASK_ALL, 0, TIMEOUT_NULL, 0, &info);
	assert_eq(err, EOK);
	assert_eq(info.sig, 42);	/* overridden by kernel */
	assert_eq(info.code, 0xc);	/* unchanged */
	assert_eq(info.ex_type, 0xe);	/* unchanged */
	assert_eq(info.status, 0x5);	/* unchanged */
	assert_eq(info.addr, 1);

	/* can enqueue signals again */
	info.addr = 6;
	err = sys_sig_send(0, 43, &info);
	assert_eq(err, EOK);

	/* cannot enqueue more signals */
	info.addr = 7;
	err = sys_sig_send(0, 42, &info);
	assert_eq(err, EAGAIN);

	/* continue polling signals */
	err = sys_sig_wait(SIG_MASK_ALL, 0, TIMEOUT_NULL, 0, &info);
	assert_eq(err, EOK);
	assert_eq(info.sig, 42);
	assert_eq(info.addr, 3);

	err = sys_sig_wait(SIG_MASK_ALL, 0, TIMEOUT_NULL, 0, &info);
	assert_eq(err, EOK);
	assert_eq(info.sig, 42);
	assert_eq(info.addr, 5);

	pending = sys_sig_pending();
	assert(pending == SIG_TO_MASK(43));

	err = sys_sig_wait(SIG_MASK_ALL, 0, TIMEOUT_NULL, 0, &info);
	assert_eq(err, EOK);
	assert_eq(info.sig, 43);
	assert_eq(info.addr, 2);

	pending = sys_sig_pending();
	assert(pending == SIG_TO_MASK(43));

	err = sys_sig_wait(SIG_MASK_ALL, 0, TIMEOUT_NULL, 0, &info);
	assert_eq(err, EOK);
	assert_eq(info.sig, 43);
	assert_eq(info.addr, 4);

	err = sys_sig_wait(SIG_MASK_ALL, 0, TIMEOUT_NULL, 0, &info);
	assert_eq(err, EOK);
	assert_eq(info.sig, 43);
	assert_eq(info.addr, 6);

	pending = sys_sig_pending();
	assert(pending == SIG_MASK_NONE);

	/* no more pending signals */
	err = sys_sig_wait(SIG_MASK_ALL, 0, TIMEOUT_NULL, 0, &info);
	assert_eq(err, EAGAIN);

	pending = sys_sig_pending();
	assert(pending == SIG_MASK_NONE);

	/* restore signal mask */
	sys_sig_mask(SIG_MASK_ALL, sig_mask);

	printf("\n");
}
