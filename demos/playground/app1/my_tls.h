/* SPDX-License-Identifier: MIT */
/* Copyright 2020 Alexander Zuepke */
/*
 * my_tls.h
 *
 * Application-specific TLS (for Uramaki Futexes)
 *
 * azuepke, 2020-02-02: initial
 */

#ifndef MY_TLS_H_
#define MY_TLS_H_

#include <marron/types.h>
#include <marron/compiler.h>
#include <marron/list.h>
#include <marron/arch_tls.h>

struct my_tls {
	/* system TLS */
	struct sys_tls sys;

	/* thread ID */
	uint32_t tid;
	/* user wait state */
	uint32_t ustate;

	/* wait queue information */
	list_t waitq_node;
	unsigned int waitq_prio;
} __aligned(64);

#endif
