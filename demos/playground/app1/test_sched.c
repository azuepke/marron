/* SPDX-License-Identifier: MIT */
/* Copyright 2017, 2018 Alexander Zuepke */
/*
 * test_sched.c
 *
 * Example OS -- test code
 *
 * azuepke, 2017-07-17: initial
 * azuepke, 2018-04-10: moved from main.c
 */

#include <marron/api.h>
#include <stdio.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"

static void thread_func1(void *arg __unused)
{
	sys_time_t now;

	printf("* Thread: Hi there!\n");
	sys_clock_get(CLK_ID_MONOTONIC, &now);
	for (unsigned int i = 1; i <= 10; i++) {
		printf("* Thread: loop %u\n", i);
		now += 500*1000*1000;
		sys_sleep(now, CLK_ID_MONOTONIC | TIMEOUT_ABSOLUTE);
	}
	printf("* Thread: exit\n");
	sys_thread_exit();
}

static void thread_func1b(void *arg __unused)
{
	assert_eq(sys_cpu_get_syscall(), 1);
	assert_eq(sys_cpu_get(), 1);
	thread_func1(arg);
}

static void thread_func2(void *arg __unused)
{
	unsigned int err;

	printf("* Thread: resume\n");
	err = sys_thread_resume(0);
	assert_eq(err, EOK);

	printf("* Thread: exit\n");
	sys_thread_exit();
}

static void thread_func2b(void *arg __unused)
{
	assert_eq(sys_cpu_get_syscall(), 1);
	assert_eq(sys_cpu_get(), 1);
	thread_func2(arg);
}

void test_sched(void)
{
	unsigned int old_cpu;
	unsigned int new_cpu;
	ulong_t cpu_mask;
	unsigned int prio;
	unsigned int fpu_state;
	err_t err;

	print_header("Basic Scheduler Tests");

	/* test yield */
	printf("# yield: ");
	sys_yield();
	printf("OK\n");

	/* test CPU changes */
	printf("# cpu_get: %u, %u\n", sys_cpu_get(), sys_cpu_get_syscall());
	assert_eq(sys_cpu_get(), sys_cpu_get_syscall());
	printf("# cpu_set: ");
	err = sys_cpu_set(sys_cpu_get());
	assert_eq(err, EOK);
	printf("OK\n");

	/* test CPU changes -> migrate to different CPU */
	old_cpu = sys_cpu_get();
	printf("# migrate %u -> %u: ", old_cpu, old_cpu + 1);
	cpu_mask = sys_cpu_mask();
	/* we need at least two CPUs */
	if ((cpu_mask & 0x3) == 0x3) {
		err = sys_cpu_set(old_cpu + 1);
		assert_eq(err, EOK);
		printf("OK\n");

		new_cpu = sys_cpu_get_syscall();
		printf("# cpu_get: %u\n", new_cpu);
		assert_eq(new_cpu, old_cpu + 1);
		assert_eq(new_cpu, sys_cpu_get());

		printf("# migrate %u -> %u: ", old_cpu + 1, old_cpu);
		err = sys_cpu_set(old_cpu);
		assert_eq(err, EOK);
		printf("OK\n");

		new_cpu = sys_cpu_get_syscall();
		printf("# cpu_get: %u\n", new_cpu);
		assert_eq(new_cpu, old_cpu);
		assert_eq(new_cpu, sys_cpu_get());
	} else {
		printf("skipped\n");
	}

	/* test prio changes */
	prio = sys_prio_get();
	printf("# prio_get: %u\n", prio);
	printf("# prio_set: ");
	err = sys_prio_set(prio);
	assert_eq(err, prio);
	assert_eq(prio, sys_prio_get());
	printf("OK\n");

	/* test FPU state changes */
	fpu_state = sys_fpu_state_get();
	printf("# fpu_state_get: %u\n", fpu_state);
	assert_eq(fpu_state, FPU_STATE_AUTO);
	printf("# fpu_state_set(OFF): ");
	err = sys_fpu_state_set(FPU_STATE_OFF);
	assert_eq(err, EOK);
	assert_eq(sys_fpu_state_get(), FPU_STATE_OFF);
	printf("OK\n");
	printf("# fpu_state_set(ON): ");
	err = sys_fpu_state_set(FPU_STATE_ON);
	assert_eq(err, EOK);
	assert_eq(sys_fpu_state_get(), FPU_STATE_ON);
	printf("OK\n");
	printf("# fpu_state_set(AUTO): ");
	err = sys_fpu_state_set(FPU_STATE_AUTO);
	assert_eq(err, EOK);
	assert_eq(sys_fpu_state_get(), FPU_STATE_AUTO);
	printf("OK\n");
	printf("\n");
}

void test_suspend_resume(void)
{
	sys_time_t now;
	err_t err;

	print_header("Thread Suspend / Resume");

	/* test timer + suspend + wakeup */
	create_thread(1, thread_func1, NULL, 50);

	for (unsigned int i = 1; i <= 10; i++) {
		sys_clock_get(CLK_ID_MONOTONIC, &now);
		printf("# loop %u: now it is %llu ns since boot\n", i, (unsigned long long)now);
		sys_sleep(now + 1000*1000*1000, CLK_ID_MONOTONIC | TIMEOUT_ABSOLUTE);
	}

	printf("\n");

	printf("# 2nd test:\n");
	create_thread(1, thread_func2, NULL, 99);

	printf("# suspending now\n");
	err = sys_thread_suspend();
	assert_eq(err, EOK);
	printf("# resumed -> sleep\n");

	sys_sleep(1000*1000*1000, CLK_ID_MONOTONIC);

	printf("# other should be dead by now\n");
	printf("\n");
}

void test_suspend_resume_smp(void)
{
	sys_time_t now;
	err_t err;

	print_header("Thread Suspend / Resume SMP");

	/* we need at least two CPUs */
	if ((sys_cpu_mask() & 0x3) != 0x3) {
		printf("test skipped, we need at least two CPUs\n");
		return;
	}

	/* test timer + suspend + wakeup */
	create_thread_cpu(1, thread_func1b, NULL, 50, 1);

	for (unsigned int i = 1; i <= 10; i++) {
		sys_clock_get(CLK_ID_MONOTONIC, &now);
		printf("# loop %u: now it is %llu ns since boot\n", i, (unsigned long long)now);
		sys_sleep(now + 1000*1000*1000, CLK_ID_MONOTONIC | TIMEOUT_ABSOLUTE);
	}

	printf("\n");

	printf("# 2nd test:\n");
	create_thread_cpu(1, thread_func2b, NULL, 99, 1);

	printf("# suspending now\n");
	err = sys_thread_suspend();
	assert((err == EOK) || (err == EAGAIN));
	printf("# resumed -> sleep\n");

	sys_sleep(1000*1000*1000, CLK_ID_MONOTONIC);

	printf("# other should be dead by now\n");
	printf("\n");
}


struct sync {
	volatile unsigned long counter[2];
	volatile int stop;
};

static void thread_func_rr(void *arg)
{
	struct sync *s = arg;
	unsigned int me;

	me = sys_thread_self();

	while (!s->stop) {
		/* waste some cycles */
		for (unsigned long i = 0; i < 65536; i++) {
			__asm__ volatile("" : : "r"(i) : "memory");
		}
		s->counter[me-1]++;
	}
	sys_thread_exit();
}

void test_sched_rr(void)
{
	struct sys_sched_param sp;
	struct sync s;
	err_t err;

	print_header("Round Robin Scheduling");

	/* initial sync to next timer tick */
	sys_sleep(1, CLK_ID_MONOTONIC);

	s.counter[0] = 0;
	s.counter[1] = 0;
	s.stop = 0;

	create_thread_cpu(1, thread_func_rr, &s, 50, 0);
	create_thread_cpu(2, thread_func_rr, &s, 50, 0);

	sp.prio = 49;
	sp.quantum = 10*1000*1000;

	err = sys_thread_sched(1, 0, &sp, NULL);
	assert_eq(err, EOK);

	err = sys_thread_sched(2, 0, &sp, NULL);
	assert_eq(err, EOK);

	/* let them run ... */
	sys_sleep(1000*1000*1000, CLK_ID_MONOTONIC);

	s.stop = 1;

	/* let them finish ... */
	sys_sleep(100*1000*1000, CLK_ID_MONOTONIC);

	printf("# count %lu %lu\n", s.counter[0], s.counter[1]);
	assert3(s.counter[0], >, 0);
	assert3(s.counter[1], >, 0);
	/* we accept 25% deviation of the counter values on QEMU */
	assert3(s.counter[0], <, (s.counter[1]*0x14)/0x10);
	assert3(s.counter[1], <, (s.counter[0]*0x14)/0x10);

	printf("\n");
}
