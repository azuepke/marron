/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023, 2025 Alexander Zuepke */
/*
 * test_mapping.c
 *
 * Memrq map and unmap testcases
 *
 * azuepke, 2021-05-02: initial
 * azuepke, 2023-08-02: large TLB testcase
 * azuepke, 2025-01-31: rebase sys_memrq_map() and sys_memrq_alloc() on sys_vm_map()
 */

#include <marron/api.h>
#include <marron/mapping.h>
#include <marron/cache_type.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include <marron/util.h>
#include "app.h"

/* likely page table size */
#define PT_SIZE 0x200000

void test_mapping(void)
{
	struct sys_memrq_state m;
	unsigned int prot;
	phys_addr_t phys;
	addr_t map_addr;
	char name[32];
	addr_t next;
	unsigned int num_pt;
	unsigned int i;
	int kldd_id_map;
	int kldd_id_unmap;
	addr_t addr;
	int test_id;
	err_t err;
	uint32_t fd;

	print_header("Memory Requirements and Mapping API Implementation");

	/* get ID of special memory requirement for testing */
	test_id = config_memrq_id(TEST_MAP_NAME, MEMRQ_TYPE_MEM);
	assert3(test_id, !=, -1);

	err = sys_memrq_open(MEMRQ_TYPE_MEM, test_id, SYS_MEMRQ_READ|SYS_MEMRQ_WRITE, &fd);
	assert_eq(err, EOK);

	printf("* testing sys_vm_unmap()\n");

	/* invalid addresses and sizes */
	err = sys_vm_unmap(0, 0);
	assert_eq(err, EINVAL);

	err = sys_vm_unmap(0, 1);
	assert_eq(err, EINVAL);
	err = sys_vm_unmap(1, 0);
	assert_eq(err, EINVAL);

	err = sys_vm_unmap(-0x1000UL, 0x1000);
	assert_eq(err, EINVAL);
	err = sys_vm_unmap(0x1000, -0x1000UL);
	assert_eq(err, EINVAL);


	printf("* testing sys_memrq_name()\n");
	for (unsigned int type = 0; ; type++) {
		unsigned int id;
		for (id = 0; ; id++) {
			printf("- memrq: %u %u: ", type, id);
			err = sys_memrq_name(type, id, name, sizeof(name));
			if (err == EOK) {
				printf("\"%s\" ", name);
			}
			printf("%s\n", __strerror(err));
			if ((type == MEMRQ_TYPE_MEM) ||
			        (type == MEMRQ_TYPE_IO) ||
			        (type == MEMRQ_TYPE_SHM)) {
				assert((err == EOK) || (err == ELIMIT));
			}
			if (err != EOK) {
				break;
			}
		}
		if (err == EINVAL) {
			break;
		}
	}

	printf("* testing sys_memrq_state()\n");
	for (unsigned int type = 0; ; type++) {
		unsigned int id;
		for (id = 0; ; id++) {
			printf("- memrq: %u %u: ", type, id);
			err = sys_memrq_state(type, id, &m);
			printf("%s\n", __strerror(err));
			if ((type == MEMRQ_TYPE_MEM) ||
			        (type == MEMRQ_TYPE_IO) ||
			        (type == MEMRQ_TYPE_SHM)) {
				assert((err == EOK) || (err == ELIMIT));
			}
			if (err != EOK) {
				break;
			}
		}
		if (err == EINVAL) {
			break;
		}
	}

	/* NULL pointer */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, NULL);
	assert_eq(err, EINVAL);

	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	printf("* test memrq:\n");
	printf("- phys address: 0x%llx\n", (unsigned long long)m.phys);
	printf("- size:         0x%lx\n", (unsigned long)m.size);
	assert_eq(m.size, TEST_MAP_SIZE);
	printf("- unallocated:  0x%lx\n", (unsigned long)m.unallocated);
	assert_eq(m.unallocated, TEST_MAP_SIZE);
	printf("- prot:         %u\n", m.prot);
	assert_eq(m.prot, (PROT_READ|PROT_WRITE));
	printf("- cache:        %u\n", m.cache);
	assert_eq(m.cache, CACHE_TYPE_WA);


	printf("* testing mmap()/mnumap() refcounting\n");

	/* nothing mapped/used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);

	/* map 1 page */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* 1 page used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - PAGE_SIZE);

	/* unmap 1 page */
	err = sys_vm_unmap(UNMAPPED_BASE, UNMAPPED_SIZE);
	assert_eq(err, EOK);

	/* nothing used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);

	/* map 1 page */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* 1 page used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - PAGE_SIZE);

	/* overmap 1 page */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* still 1 page used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - PAGE_SIZE);

	/* overmap 2 pages */
	err = sys_vm_map(UNMAPPED_BASE, 2*PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* now 2 pages used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - 2*PAGE_SIZE);

	/* overmap 1 page */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* still 2 pages used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - 2*PAGE_SIZE);

	/* unmap just 1 page */
	err = sys_vm_unmap(UNMAPPED_BASE, 1*PAGE_SIZE);
	assert_eq(err, EOK);

	/* 1 page used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - PAGE_SIZE);

	/* unmap last page */
	err = sys_vm_unmap(UNMAPPED_BASE, UNMAPPED_SIZE);
	assert_eq(err, EOK);

	/* map 3 pages */
	err = sys_vm_map(UNMAPPED_BASE, 3*PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* numap 1 page in the middle */
	err = sys_vm_unmap(UNMAPPED_BASE + PAGE_SIZE, PAGE_SIZE);
	assert_eq(err, EOK);

	/* unmap rest */
	err = sys_vm_unmap(UNMAPPED_BASE, UNMAPPED_SIZE);
	assert_eq(err, EOK);

	/* nothing mapped/used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);
	printf("- OK\n");


	printf("* testing sys_memrq_alloc() -> sys_vm_map()\n");

	/* unaligned size */
	err = sys_vm_map(UNMAPPED_BASE, 1, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* zero allocation -> fail */
	err = sys_vm_map(UNMAPPED_BASE, 0, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* allocation -> OK */
	err = sys_vm_map(UNMAPPED_BASE, TEST_MAP_SIZE/2, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* too large -> fail */
	err = sys_vm_map(UNMAPPED_BASE+TEST_MAP_SIZE/2, TEST_MAP_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, ESTATE);

	/* allocate to rest -> OK */
	err = sys_vm_map(UNMAPPED_BASE+TEST_MAP_SIZE/2, TEST_MAP_SIZE/2, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EOK);

	/* zero allocation -> fail */
	err = sys_vm_map(UNMAPPED_BASE, 0, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* overflow allocation -> fail */
	err = sys_vm_map(UNMAPPED_BASE, -TEST_MAP_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* overflow allocation -> fail */
	err = sys_vm_map(UNMAPPED_BASE, -PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED | MAP_ANON, -1, 0, &map_addr);
	assert_eq(err, EINVAL);


	/* stat again */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.size, TEST_MAP_SIZE);
	assert_eq(m.unallocated, 0);
	assert_eq(m.prot, (PROT_READ|PROT_WRITE));
	assert_eq(m.cache, CACHE_TYPE_WA);


	printf("* testing sys_memrq_map() -> sys_vm_map()\n");
	/* we skip testing the permutation */

	/* invalid mapping flags */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, 0 /* PROT_NONE */, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EOK);
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, 0xff, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_EXEC, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EPERM);

	/* unaligned offset, size, addr */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_EXEC, MAP_PRIVATE | MAP_FIXED, fd, 1, &map_addr);
	assert_eq(err, EINVAL);
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE+1, PROT_EXEC, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);
	err = sys_vm_map(UNMAPPED_BASE+1, PAGE_SIZE, PROT_EXEC, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* invalid offset, size, addr */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0x100UL, &map_addr);
	assert_eq(err, EINVAL);
	err = sys_vm_map(UNMAPPED_BASE, 0x100, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);
	err = sys_vm_map(UNMAPPED_BASE, -0x1000UL, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);
	err = sys_vm_map(0x100, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);
	err = sys_vm_map(-0x1000UL, PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* slightly too large */
	err = sys_vm_map(UNMAPPED_BASE, TEST_MAP_SIZE + PAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, ESTATE);

	/* zero size */
	err = sys_vm_map(UNMAPPED_BASE, 0, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* right size */
	err = sys_vm_map(UNMAPPED_BASE, TEST_MAP_SIZE, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EOK);

	/* overmap as read-write */
	err = sys_vm_map(UNMAPPED_BASE, TEST_MAP_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EOK);

	/* unmap */
	err = sys_vm_unmap(UNMAPPED_BASE, TEST_MAP_SIZE);
	assert_eq(err, EOK);


	printf("* testing sys_vm_map()\n");

	/* success */
	map_addr = 1234;
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EOK);
	assert_eq(map_addr, UNMAPPED_BASE);

	/* overmap */
	map_addr = 1234;
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EOK);
	assert_eq(map_addr, UNMAPPED_BASE);

	/* no overmapping */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_EXCL, fd, 0, &map_addr);
	/* NOTE: FreeBSD returns EINVAL, OpenBSD returns ENOMEM, Linux returns EEXIST */
	assert_eq(err, ENOMEM);

	/* unaligned address, size, offset */
	err = sys_vm_map(UNMAPPED_BASE+1, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE+1, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 1, &map_addr);
	assert_eq(err, EINVAL);

	/* invalid pointers */
	err = sys_vm_map(-0x1000, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* zero size */
	err = sys_vm_map(UNMAPPED_BASE, 0, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* invalid permissions */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, 0xf, MAP_PRIVATE|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* invalid type (encoded in flags) */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, 0|MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EINVAL);

	/* invalid ptrs for map_addr */
	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, 0xf, MAP_PRIVATE|MAP_FIXED, fd, 0, NULL);
	assert_eq(err, EINVAL);

	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, (void*)-1);
	assert_eq(err, EINVAL);

	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ, MAP_PRIVATE|MAP_FIXED, fd, 0, (void*)1);
	assert_eq(err, EFAULT);

	/* non-fixed placing */
	map_addr = 1234;
	err = sys_vm_map(UNMAPPED_BASE + PAGE_SIZE, PAGE_SIZE, PROT_READ, MAP_PRIVATE, fd, 0, &map_addr);
	assert_eq(err, EOK);
	assert_eq(map_addr, UNMAPPED_BASE + 1*PAGE_SIZE);

	/* auto placing */
	map_addr = 1234;
	err = sys_vm_map(0, PAGE_SIZE, PROT_READ, MAP_PRIVATE, fd, 0, &map_addr);
	assert_eq(err, EOK);
	assert_eq(map_addr, UNMAPPED_BASE + 2*PAGE_SIZE);

	/* guard */
	map_addr = 1234;
	err = sys_vm_map(0, PAGE_SIZE, PROT_READ, MAP_GUARD, fd, 0, &map_addr);
	assert_eq(err, EOK);
	assert_eq(map_addr, UNMAPPED_BASE + 3*PAGE_SIZE);

	/* unmap */
	err = sys_vm_unmap(UNMAPPED_BASE, TEST_MAP_SIZE);
	assert_eq(err, EOK);


	/* create dummy mappings until we run out of page tables */
	printf("* testing KMEM for pagetables\n");

	/* no pages must be used */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);

	printf("- first time: ");
	addr = UNMAPPED_BASE;
	for (i = 0; ; i++) {
		err = sys_vm_map(addr, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
		assert((err == EOK) || (err == ENOMEM));

		if (err == ENOMEM) {
			break;
		}

		addr += PT_SIZE;
		assert3(addr, <, UNMAPPED_BASE + UNMAPPED_SIZE);
	}
	printf("%u page tables used\n", i);
	num_pt = i;

	/* only one page must be used, as we map the same page over and over */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - PAGE_SIZE);

	/* unmap all mappings again */
	err = sys_vm_unmap(UNMAPPED_BASE, UNMAPPED_SIZE);
	assert_eq(err, EOK);

	/* all resources must be freed again */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);


	/* create dummy mappings until we run out of page tables */
	printf("- second time: ");
	addr = UNMAPPED_BASE;
	for (i = 0; ; i++) {
		err = sys_vm_map(addr, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
		assert((err == EOK) || (err == ENOMEM));

		if (err == ENOMEM) {
			break;
		}

		addr += PT_SIZE;
		assert3(addr, <, UNMAPPED_BASE + UNMAPPED_SIZE);
	}
	printf("%u page tables used\n", i);
	assert_eq(num_pt, i);

	/* only one page must be used, as we map the same page over and over */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - PAGE_SIZE);

	/* unmap all mappings again */
	err = sys_vm_unmap(UNMAPPED_BASE, UNMAPPED_SIZE);
	assert_eq(err, EOK);

	/* all resources must be freed again */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);


	printf("* testing sys_vm_v2p()\n");
	/* invalid pointers */
	err = sys_vm_v2p(UNMAPPED_BASE, NULL, NULL, NULL);
	assert_eq(err, EINVAL);

	err = sys_vm_v2p(UNMAPPED_BASE, &phys, &prot, NULL);
	assert_eq(err, EINVAL);

	err = sys_vm_v2p(UNMAPPED_BASE, &phys, NULL, &next);
	assert_eq(err, EINVAL);

	err = sys_vm_v2p(UNMAPPED_BASE, NULL, &prot, &next);
	assert_eq(err, EINVAL);

	/* no mapping */
	err = sys_vm_v2p(UNMAPPED_BASE, &phys, &prot, &next);
	assert_eq(err, ESTATE);

	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);

	err = sys_vm_map(UNMAPPED_BASE, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE | MAP_FIXED, fd, 0, &map_addr);
	assert_eq(err, EOK);

	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE - PAGE_SIZE);

	/* now with mapping */
	err = sys_vm_v2p(UNMAPPED_BASE, &phys, &prot, &next);
	assert_eq(err, EOK);
	assert_eq(phys, m.phys);
	assert_eq(prot, (PROT_READ|PROT_WRITE));
	assert_eq(next, UNMAPPED_BASE + PAGE_SIZE);

	/* offsets */
	err = sys_vm_v2p(UNMAPPED_BASE+1, &phys, &prot, &next);
	assert_eq(err, EOK);
	assert_eq(phys, m.phys+1);
	assert_eq(prot, (PROT_READ|PROT_WRITE));
	assert_eq(next, UNMAPPED_BASE + PAGE_SIZE);

	/* testing "next" */
	err = sys_vm_v2p(UNMAPPED_BASE + PAGE_SIZE, &phys, &prot, &next);
	assert_eq(err, ESTATE);
	assert_eq(next, UNMAPPED_BASE + 2*PAGE_SIZE);

	/* testing "prot" */
	err = sys_vm_protect(UNMAPPED_BASE, UNMAPPED_SIZE, PROT_READ);
	assert_eq(err, ENOMEM);

	err = sys_vm_protect(UNMAPPED_BASE, 1*PAGE_SIZE, PROT_READ);
	assert_eq(err, EOK);

	err = sys_vm_v2p(UNMAPPED_BASE, &phys, &prot, &next);
	assert_eq(err, EOK);
	assert_eq(phys, m.phys);
	assert_eq(prot, PROT_READ);
	assert_eq(next, UNMAPPED_BASE + PAGE_SIZE);

	/* unmap */
	err = sys_vm_unmap(UNMAPPED_BASE, UNMAPPED_SIZE);
	assert_eq(err, EOK);

	err = sys_vm_v2p(UNMAPPED_BASE + PAGE_SIZE, &phys, &prot, &next);
	assert_eq(err, ESTATE);
	assert3(next, >, UNMAPPED_BASE + 2*PAGE_SIZE);

	/* final stat: all resources must be freed again */
	err = sys_memrq_state(MEMRQ_TYPE_MEM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.unallocated, TEST_MAP_SIZE);


	printf("* testing large TLB (using kldd_tlb_map/kldd_tlb_unmap)\n");
	/* this optional testcase must come last */

	kldd_id_map = config_kldd_id("tlb_map");
	kldd_id_unmap = config_kldd_id("tlb_unmap");
	if ((kldd_id_map == -1) || (kldd_id_unmap == -1)) {
		printf("- tlb_mapper KLDD missing, skipped\n");
		goto out;
	}
	printf("- tlb_mapper available\n");

	test_id = config_memrq_id(BENCH_MAP_NAME, MEMRQ_TYPE_SHM);
	assert3(test_id, !=, -1);

	err = sys_memrq_state(MEMRQ_TYPE_SHM, test_id, &m);
	assert_eq(err, EOK);
	assert_eq(m.prot & PROT_READ, PROT_READ);
	assert_eq(m.prot & PROT_WRITE, PROT_WRITE);
	assert_eq(m.prot & PROT_EXEC, PROT_EXEC);

	if ((m.phys & (0x200000 - 1)) != 0) {
		printf("- wrong alignment (add '<shm name=\"%s\" ... align=\"0x200000\"/>'), skipped\n", BENCH_MAP_NAME);
		goto out;
	}

	/* map large page */
	err = sys_kldd_call(kldd_id_map, BENCH_MAP_BASE, BENCH_MAP_SIZE, MEMRQ_TYPE_SHM, test_id);
	assert_eq(err, EOK);

	printf("- large TLB access test: ");
	{
		char *c = (char *)BENCH_MAP_BASE;
		*c += 1;
	}
	printf("OK\n");

	/* check mapping properties and sys_vm_v2p() */
	err = sys_vm_v2p(BENCH_MAP_BASE, &phys, &prot, &next);
	assert_eq(err, EOK);
	assert_eq(phys, m.phys);
	assert_eq(prot, m.prot);
	assert_eq(next, BENCH_MAP_BASE + PAGE_SIZE);

	err = sys_vm_v2p(next, &phys, &prot, &next);
	assert_eq(err, EOK);
	assert_eq(phys, m.phys + PAGE_SIZE);
	assert_eq(prot, m.prot);
	assert_eq(next, BENCH_MAP_BASE + 2*PAGE_SIZE);

	/* unmap large page */
	err = sys_kldd_call(kldd_id_unmap, BENCH_MAP_BASE, BENCH_MAP_SIZE, 0, 0);
	assert_eq(err, EOK);

	err = sys_vm_v2p(BENCH_MAP_BASE, &phys, &prot, &next);
	assert_eq(err, ESTATE);
	assert3(next, >=, BENCH_MAP_BASE + 0x100000);

out:
	printf("OK\n");
	printf("\n");

	err = sys_fd_close(fd);
	assert_eq(err, EOK);
}
