/* SPDX-License-Identifier: MIT */
/* Copyright 2021, 2023, 2024 Alexander Zuepke */
/*
 * test_kldd.c
 *
 * Kernel-level device drivers (KLDD) testcase
 *
 * azuepke, 2021-05-07: initial
 * azuepke, 2023-08-08: cpuinfo
 * azuepke, 2024-01-02: console input
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include <marron/util.h>
#include "app.h"

static void con_putc(int kldd_id, int c)
{
	if (c == '\n') {
		con_putc(kldd_id, '\r');
	}

	/* print character using the KLDD */
	while (sys_kldd_call(kldd_id, 0, c, 0, 0) != EOK) {
		/* nothing -- just polling the console */
	}
}

static int con_getc(int kldd_id)
{
	int c;

	/* get character using the KLDD */
	if (sys_kldd_call(kldd_id, 1, (unsigned long)&c, 0, 0) != EOK) {
		/* nothing -- just polling the console for input */
		return 0;
	}

	return c;
}

#if defined __arm__ || defined __aarch64__
static void test_cpuinfo(int kldd_id)
{
	unsigned long last_val;
	unsigned long val;
	err_t err;

	err = sys_kldd_call(kldd_id, 0, 0, 0, (unsigned long)&val);
	assert_eq(err, EOK);
	printf("MIDR:      0x%08lx\n", val);

	err = sys_kldd_call(kldd_id, 1, 0, 0, (unsigned long)&val);
	assert_eq(err, EOK);
	printf("CTR:       0x%08lx\n", val);

	err = sys_kldd_call(kldd_id, 2, 0, 0, (unsigned long)&val);
	assert_eq(err, EOK);
	printf("REVIDR:    0x%08lx\n", val);

	err = sys_kldd_call(kldd_id, 3, 0, 0, (unsigned long)&val);
	assert_eq(err, EOK);
	printf("CLIDR:     0x%08lx\n", val);

	last_val = 0;
	for (unsigned int i = 0; i < 16; i++) {
		/* "i" encodes level and i-bit in CSSELR / CSSELR_EL1 */
		err = sys_kldd_call(kldd_id, 4, i, 0, (unsigned long)&val);
		assert_eq(err, EOK);
		printf("CCSIDR(%u): 0x%08lx\n", i, val);

		/* stop after reading two zero values */
		if ((val == 0) && (last_val == 0)) {
			break;
		}
		last_val = val;
	}
}
#endif

void test_kldd(void)
{
	int kldd_id;
	char name[32];
	err_t err;

	print_header("KDLD test");

	printf("KLDDs assigned to partition:\n");
	for (unsigned int i = 0; ; i++) {

		err = sys_kldd_name(i, name, sizeof(name));
		assert((err == EOK) || (err == ELIMIT));
		if (err != EOK) {
			break;
		}

		printf("- %u: KLDD \"%s\"\n", i, name);
	}

	kldd_id = config_kldd_id("doesnotexist");
	assert3(kldd_id, ==, -1);

	kldd_id = config_kldd_id("con");
	assert3(kldd_id, !=, -1);

	printf("Console output via KLDD: ");
	strcpy(name, "Hello World\n");
	for (unsigned int i = 0; name[i] != '\0'; i++) {
		con_putc(kldd_id, name[i]);
	}

	printf("Console input via KLDD (if any): ");
	while (1) {
		int c = con_getc(kldd_id);
		if (c == 0) {
			break;
		}
		printf("%c", c);
	}
	printf("\n");

	printf("CPU info: ");
	kldd_id = config_kldd_id("cpuinfo");
	if (kldd_id != -1) {
#if defined __arm__ || defined __aarch64__
		printf("available\n");
		test_cpuinfo(kldd_id);
#else
		printf("available, but unsupported architecture\n");
#endif
	} else {
		printf("not available\n");
	}
}
