/* SPDX-License-Identifier: MIT */
/* Copyright 2018-2022 Alexander Zuepke */
/*
 * app.h
 *
 * Application main include file
 *
 * azuepke, 2018-03-19: initial
 * azuepke, 2022-02-12: cycle counter handling
 */

#ifndef APP_H_
#define APP_H_

#include <marron/types.h>
#include <marron/arch_defs.h>
#include <stdint.h>

/* buildid.c */
extern const char __buildid[];

/* main.c */
void create_thread(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio);
void create_thread_cpu(unsigned int thr_id, void (*func)(void *), void *arg, unsigned int prio, unsigned int cpu);
void join_thread(unsigned int thr_id);
void print_line(void);
void print_header(const char *s);
err_t pmu_ctrl(unsigned int counter, unsigned int value);

/* cycle counter handling */
unsigned long get_tsc_freq(void);
static inline __attribute__((always_inline)) unsigned int get_tsc(void)
{
#if defined __i386__ || defined __x86_64__
	unsigned int eax, edx;
	__asm__ volatile ("rdtsc" : "=a" (eax), "=d" (edx) : : "memory");
	return eax;
#elif defined __arm__
	unsigned long val;
	__asm__ volatile ("isb; mrc p15, 0, %0, c9, c13, 0" : "=r" (val) : : "memory");
	return val;
#elif defined __aarch64__
	unsigned long val;
	__asm__ volatile ("isb; mrs %0, PMCCNTR_EL0" : "=r" (val) : : "memory");
	return val;
#elif defined __powerpc__
	unsigned long val;
	__asm__ volatile ("mftbl %0" : "=r" (val) : : "memory");
	return val;
#elif defined __riscv
	unsigned long val;
	__asm__ volatile ("rdcycle %0" : "=r" (val) : : "memory");
	return val;
#else
#error Adapt this file to your CPU architecture!
#endif
}

/* various */
void test_prio(void);
void test_sched(void);
void test_suspend_resume(void);
void test_suspend_resume_smp(void);
void test_sched_rr(void);
void test_timer(void);
void test_timer_irq(void);
void test_signals(void);
void test_signals_stacks(void);
void test_signals_queued(void);
void test_futex(void);
void test_mapping(void);
void test_ipc(void);
void test_ipc_io(void);
void test_memrq_io(void);
void test_epoll(void);
void test_eventfd(void);
void test_timerfd(void);
void test_signalfd(void);
void test_libc(void);
void test_kldd(void);
void test_partition(volatile uint32_t *shm1);

/* bench_* */
void bench_cpu(void);
void bench_api(void);
void bench_ipc(void);
void bench_ospert2014(void);

/* virtual memory area to map the sp804 timer I/O for test_timer_irq() */
#define IO_MAP_BASE 0x20000000
#define IO_MAP_SIZE 0x00001000
#define IO_MAP_NAME "sp804_timer"

/* unmapped virtual memory area for test_mapping() */
#define UNMAPPED_BASE 0x30000000
#define UNMAPPED_SIZE 0x10000000

/* test mapping for test_mapping() (same as unmapped area) */
#define TEST_MAP_BASE 0x30000000
#define TEST_MAP_SIZE 0x00004000
#define TEST_MAP_NAME "test"

/* mapping for memory benchmarks (same as unmapped area) */
#define BENCH_MAP_BASE 0x30000000
#define BENCH_MAP_SIZE 0x02000000
#define BENCH_MAP_NAME "membench"

/* virtual memory area to map the SHM for test_partition() */
#define SHM_MAP_BASE 0x40000000
#define SHM_MAP_SIZE 0x00001000
#define SHM_MAP_NAME "shm1"

/* IPC channel */
#define IPC_SERVER_NAME "s1"
#define IPC_CLIENT_NAME "c1"

/* IPC channel for I/O */
#define IPC_IO_SERVER_NAME "dev-test-server"
#define IPC_IO_CLIENT_NAME "/dev/test"

#endif
