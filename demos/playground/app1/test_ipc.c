/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * test_ipc.c
 *
 * Inter-process communication.
 *
 * azuepke, 2022-07-23: initial
 */

#include <marron/api.h>
#include <marron/mapping.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include <fcntl.h>
#include "app.h"


static void test_ipc_port_names(void)
{
	char name[32];
	uint32_t fd;
	err_t err;

	printf("\n* IPC endpoint names\n");

	printf("IPC server names:\n");
	for (unsigned int i = 0; ; i++) {
		err = sys_ipc_port_name(IPC_TYPE_SERVER, i, name, sizeof(name));
		assert((err == EOK) || (err == ELIMIT));
		if (err == EOK) {
			err = sys_ipc_port_fd(IPC_TYPE_SERVER, i, &fd);
			assert(err == EOK);
			printf("- %u: \"%s\": 0x%08x\n", i, name, fd);
		} else {
			break;
		}
	}

	printf("IPC client names:\n");
	for (unsigned int i = 0; ; i++) {
		err = sys_ipc_port_name(IPC_TYPE_CLIENT, i, name, sizeof(name));
		assert((err == EOK) || (err == ELIMIT));
		if (err == EOK) {
			err = sys_ipc_port_fd(IPC_TYPE_CLIENT, i, &fd);
			assert((err == EOK) || (err == ESTATE));
			if (err == EOK) {
				printf("- %u: \"%s\": 0x%08x\n", i, name, fd);
			} else {
				printf("- %u: \"%s\": -1 <not connected>\n", i, name);
			}
		} else {
			break;
		}
	}

	/* test invalid type */
	err = sys_ipc_port_name(2, 0, name, sizeof(name));
	assert_eq(err, EINVAL);
	/* test invalid type */
	err = sys_ipc_port_fd(2, 0, &fd);
	assert_eq(err, EINVAL);
}

static void test_ipc_fds(void)
{
	unsigned int start;
	unsigned int max;
	unsigned int i;
	uint32_t attrib;
	uint32_t fds[2];
	uint32_t fd;
	err_t err;

	/* this test requires at least 4 fds and an even larger fd_table */
	/* initial assumption: no fds are in use, fd_table is empty */

	printf("\n* IPC file descriptor tests\n");

	/* invalid flags */
	err = sys_ipc_pair(0xffffffff, &fds[0], &fds[1]);
	assert_eq(err, EINVAL);

	/* create a first pair of fds */
	printf("- pair 1\n");
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);
	start = fds[0];
	assert_eq(start, 0);
	assert_eq(fds[0], start + 0);
	assert_eq(fds[1], start + 1);

	/* create a second pair of fds -- taking consecutive numbers */
	printf("- pair 2\n");
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);
	assert_eq(fds[0], start + 2);
	assert_eq(fds[1], start + 3);

	/* close fds in the middle */
	printf("- close 1\n");
	err = sys_fd_close(start + 2);
	assert_eq(err, EOK);
	err = sys_fd_close(start + 1);
	assert_eq(err, EOK);

	/* create a third pair, taking the numbers in the middle */
	printf("- pair 3\n");
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);
	assert_eq(fds[0], start + 1);
	assert_eq(fds[1], start + 2);

	/* close all of them */
	printf("- close 2\n");
	err = sys_fd_close(start + 0);
	assert_eq(err, EOK);
	err = sys_fd_close(start + 1);
	assert_eq(err, EOK);
	err = sys_fd_close(start + 2);
	assert_eq(err, EOK);
	err = sys_fd_close(start + 3);
	assert_eq(err, EOK);

	/* close all of them again should fail */
	printf("- close 3\n");
	err = sys_fd_close(start + 0);
	assert_eq(err, EBADF);
	err = sys_fd_close(start + 1);
	assert_eq(err, EBADF);
	err = sys_fd_close(start + 2);
	assert_eq(err, EBADF);
	err = sys_fd_close(start + 3);
	assert_eq(err, EBADF);

	/* create a fourth pair of fds */
	printf("- pair 4\n");
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);
	assert_eq(fds[0], start + 0);
	assert_eq(fds[1], start + 1);

	printf("- dup all: ");
	for (max = start + 2; ; max++) {
		err = sys_fd_dup(start + 0, start + 0, 0, &fd);
		if (err != EOK) {
			break;
		}
		assert_eq(fd, max);
	}
	printf("from %u to %u\n", start, max - 1);

	printf("- dup2 all: ");
	for (fd = start + 1; ; fd++) {
		err = sys_fd_dup2(start + 0, fd, 0);
		if (err != EOK) {
			break;
		}
	}
	printf("from %u to %u\n", start, fd - 1);
	assert_eq(fd, max);

	printf("- close 4\n");
	for (i = start + 0; i < max; i++) {
		err = sys_fd_close(i);
		assert_eq(err, EOK);
	}

	/* consume all possible fd objects */
	printf("- fd objects: ");
	for (max = start; ; max += 2) {
		err = sys_ipc_pair(0, &fds[0], &fds[1]);
		if (err != EOK) {
			break;
		}
		assert_eq(fds[0], max + 0);
		assert_eq(fds[1], max + 1);
	}
	/* even or odd number of fd objects? */
	err = sys_fd_close(max - 1);
	assert_eq(err, EOK);
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	if (err == EOK) {
		/* odd */
		assert_eq(fds[0], max - 1);
		assert_eq(fds[1], max + 0);
		max++;
	}
	printf("from %u to %u\n", start, max - 1);

	printf("- close fd objects\n");
	for (i = start + 0; i < max; i++) {
		err = sys_fd_close(i);
		if (i + 1 == max) {
			assert((err == EOK) || (err == EBADF));
		} else {
			assert_eq(err, EOK);
		}
	}

	/* create a fifth and sixth pair of fds */
	printf("- cloexec\n");
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);
	assert_eq(fds[0], start + 0);
	assert_eq(fds[1], start + 1);
	err = sys_ipc_pair(SYS_FD_CLOEXEC, &fds[0], &fds[1]);
	assert_eq(err, EOK);
	assert_eq(fds[0], start + 2);
	assert_eq(fds[1], start + 3);

	err = sys_fd_cloexec_get(start + 0, &attrib);
	assert_eq(err, EOK);
	assert_eq(attrib, 0);
	err = sys_fd_cloexec_get(start + 1, &attrib);
	assert_eq(err, EOK);
	assert_eq(attrib, 0);
	err = sys_fd_cloexec_get(start + 2, &attrib);
	assert_eq(err, EOK);
	assert_eq(attrib, SYS_FD_CLOEXEC);
	err = sys_fd_cloexec_get(start + 3, &attrib);
	assert_eq(err, EOK);
	assert_eq(attrib, SYS_FD_CLOEXEC);
	err = sys_fd_cloexec_get(start + 4, &attrib);
	assert_eq(err, EBADF);

	err = sys_fd_cloexec_set(start + 0, 0xffffffff);
	assert_eq(err, EINVAL);
	err = sys_fd_cloexec_set(start + 0, SYS_FD_CLOEXEC);
	assert_eq(err, EOK);
	err = sys_fd_cloexec_get(start + 0, &attrib);
	assert_eq(err, EOK);
	assert_eq(attrib, SYS_FD_CLOEXEC);

	err = sys_fd_dup2(start + 0, start + 1, 0);
	assert_eq(err, EOK);
	err = sys_fd_cloexec_get(start + 1, &attrib);
	assert_eq(attrib, 0);

	err = sys_fd_dup2(start + 0, start + 1, SYS_FD_CLOEXEC);
	assert_eq(err, EOK);
	err = sys_fd_cloexec_get(start + 1, &attrib);
	assert_eq(attrib, SYS_FD_CLOEXEC);

	err = sys_fd_close(start + 0);
	assert_eq(err, EOK);
	err = sys_fd_close(start + 1);
	assert_eq(err, EOK);
	err = sys_fd_close(start + 2);
	assert_eq(err, EOK);
	err = sys_fd_close(start + 3);
	assert_eq(err, EOK);
}

////////////////////////////////////////////////////////////////////////////////

static void test_ipc_msg_validation(void)
{
	struct sys_iov vec[SYS_IOV_MAX + 1] = { 0 };
	struct sys_ipc msg = { 0 };
	uint32_t fd;
	err_t err;

	printf("\n* IPC message validation tests\n");

	/* NOTE: We rely on the fact that the kernel first validates a message
	 * before trying to send it. With this, we can test all possible
	 * input combinations before failing with EBADF.
	 */
	fd = 0;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EBADF);

	/* NULL pointer inputs */
	err = sys_ipc_call(fd, NULL, &msg);
	assert_eq(err, EINVAL);

	err = sys_ipc_call(fd, &msg, NULL);
	assert_eq(err, EINVAL);

	/* invalid pointers */
	err = sys_ipc_call(fd, (void*)-1ul, &msg);
	assert_eq(err, EINVAL);

	err = sys_ipc_call(fd, (void*)-16ul, &msg);
	assert_eq(err, EINVAL);

	/* unmapped pointers */
	err = sys_ipc_call(fd, (void*)1ul, &msg);
	assert_eq(err, EFAULT);

	err = sys_ipc_call(fd, (void*)16ul, &msg);
	assert_eq(err, EFAULT);

	/* invalid message flags */
	msg.flags = 0xffff;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping in message -- should work */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EBADF);

	/* mapping: forbidded other message flags */
	msg.flags = SYS_IPC_MAP | SYS_IPC_BUF0R | SYS_IPC_BUF0W | SYS_IPC_FD0 | SYS_IPC_FD1;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: wrong permissions */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = 0xf;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: wrong type in mapping flags */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED | MAP_PRIVATE;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: wrong type in mapping flags */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = 0;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: invalid mapping flags */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = 0xffff;
	msg.map_addr = PAGE_SIZE;
	msg.map_size = PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: unaligned address */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = 1ul*PAGE_SIZE + 1;
	msg.map_size = 1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: unaligned size */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = 1ul*PAGE_SIZE;
	msg.map_size = 1ul*PAGE_SIZE + 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: zero size */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = 1ul*PAGE_SIZE;
	msg.map_size = 0;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: addr in kernel space */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = -2ul*PAGE_SIZE;
	msg.map_size = 1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* mapping: adspace overflow */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ | PROT_WRITE | PROT_EXEC;
	msg.map_flags = MAP_SHARED;
	msg.map_addr = 2ul*PAGE_SIZE;
	msg.map_size = -1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 0: should work */
	msg.flags = SYS_IPC_BUF0R;
	msg.buf0 = (void*)PAGE_SIZE;
	msg.size0 = PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EBADF);

	/* buffer 0: addr in kernel space */
	msg.flags = SYS_IPC_BUF0R;
	msg.buf0 = (void*)(-2ul*PAGE_SIZE);
	msg.size0 = 1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 0: adspace overflow */
	msg.flags = SYS_IPC_BUF0R;
	msg.buf0 = (void*)(2ul*PAGE_SIZE);
	msg.size0 = -1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 0: ssize_t overflow */
	msg.flags = SYS_IPC_BUF0R;
	msg.buf0 = (void*)0;
	msg.size0 = 1ul << (sizeof(long)*8 - 1);
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 1: addr in kernel space */
	msg.flags = SYS_IPC_BUF1R;
	msg.buf1 = (void*)(-2ul*PAGE_SIZE);
	msg.size1 = 1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 1: adspace overflow */
	msg.flags = SYS_IPC_BUF1R;
	msg.buf1 = (void*)(2ul*PAGE_SIZE);
	msg.size1 = -1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 1: ssize_t overflow */
	msg.flags = SYS_IPC_BUF1R;
	msg.buf1 = (void*)0;
	msg.size1 = 1ul << (sizeof(long)*8 - 1);
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 2: addr in kernel space */
	msg.flags = SYS_IPC_BUF2R;
	msg.buf2 = (void*)(-2ul*PAGE_SIZE);
	msg.size2 = 1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 2: adspace overflow */
	msg.flags = SYS_IPC_BUF2R;
	msg.buf2 = (void*)(2ul*PAGE_SIZE);
	msg.size2 = -1ul*PAGE_SIZE;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* buffer 2: ssize_t overflow */
	msg.flags = SYS_IPC_BUF2R;
	msg.buf2 = (void*)0;
	msg.size2 = 1ul << (sizeof(long)*8 - 1);
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* vector: should work */
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EBADF);

	/* vector: NULL vector address */
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = NULL;
	msg.iov_num = 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EFAULT);

	/* vector: unaligned vector address (can be OK) */
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = (void *)((addr_t)vec + 1);
	msg.iov_num = 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert((err == EBADF) || (err == EFAULT));

	/* vector: null-sized vector */
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = 0;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* vector: oversized vector */
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = SYS_IOV_MAX + 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* vector: addr in kernel space */
	vec[0].addr = (void*)(-2ul*PAGE_SIZE);
	vec[0].size = 1ul*PAGE_SIZE;
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* vector: adspace overflow */
	vec[0].addr = (void*)(2ul*PAGE_SIZE);
	vec[0].size = -1ul*PAGE_SIZE;
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* vector: ssize_t overflow */
	vec[0].addr = (void*)0;
	vec[0].size = 1ul << (sizeof(long)*8 - 1);
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = 1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* vector: ssize_t overflow with two vectors */
	vec[0].addr = (void*)0;
	vec[0].size = (1ul << (sizeof(long)*8 - 1)) - 1ul*PAGE_SIZE;
	vec[1].addr = (void*)0;
	vec[1].size = 1ul*PAGE_SIZE;
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = 2;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	/* first file descriptor: should work */
	msg.flags = SYS_IPC_FD0;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EBADF);

	/* both file descriptors: should work */
	msg.flags = SYS_IPC_FD0 | SYS_IPC_FD1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EBADF);

	/* second file descriptors: should NOT work */
	msg.flags =               SYS_IPC_FD1;
	err = sys_ipc_call(fd, &msg, &msg);
	assert_eq(err, EINVAL);

	printf("- OK\n");
}

////////////////////////////////////////////////////////////////////////////////

static volatile int step;

#define STEP(s) do { \
		assert_eq(step, (s)); step = (s) + 1; \
	} while (0)

static void ipc_server(void *arg)
{
	uint32_t *fds = arg;
	struct sys_ipc msg;
	uint32_t rhnd;
	err_t err;

	STEP(3);
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.req, 42);
	msg.req = 43;

	STEP(4);
	err = sys_ipc_reply_wait(rhnd, &msg, 0, NULL, NULL);
	assert_eq(err, EOK);

	STEP(6);
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.req, 44);
	msg.req = 45;

	STEP(10);
	err = sys_ipc_reply_wait(rhnd, &msg, 0, NULL, NULL);
	assert_eq(err, EOK);

	STEP(12);
	sys_thread_exit();
}

static void ipc_client1(void *arg)
{
	uint32_t *fds = arg;
	struct sys_ipc msg = { 0 };
	err_t err;

	STEP(1);
	msg.req = 42;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.req, 43);

	STEP(5);
	sys_thread_exit();
}

static void ipc_client2(void *arg)
{
	uint32_t *fds = arg;
	struct sys_ipc msg = { 0 };
	err_t err;

	STEP(8);
	msg.req = 44;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.req, 45);

	STEP(11);
	sys_thread_exit();
}

static void test_ipc_communication(void)
{
	uint32_t fds[2];
	err_t err;

	printf("\n* IPC communication tests\n");

	step = 0;

	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	/* create IPC client thread first */
	STEP(0);
	create_thread(1, ipc_client1, fds, sys_prio_get());
	sys_yield();

	/* the client is already waiting for send, now start server */
	STEP(2);
	create_thread(2, ipc_server, fds, sys_prio_get()-1);
	sys_sleep(100*1000*1000, CLK_ID_MONOTONIC);

	/* the client finished, the server is waiting. now start another client */
	STEP(7);
	create_thread(1, ipc_client2, fds, sys_prio_get());
	sys_yield();

	/* the client is waiting for a reply. let server run */
	STEP(9);
	sys_sleep(100*1000*1000, CLK_ID_MONOTONIC);

	STEP(13);
	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EOK);
}

////////////////////////////////////////////////////////////////////////////////

static void ipc_clientrw(void *arg)
{
	struct sys_ipc msg = { 0 };
	struct sys_iov vec[2];
	uint32_t *fds = arg;
	char buf[8];
	err_t err;

	/* 8 characters including \0 */
	strcpy(buf, "ipctest");

	/* the first IPC does not enable any reading and writing */
	msg.flags = 0;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);

	/* the second IPC enables reading of buffer 0 */
	msg.flags = SYS_IPC_BUF0R;
	msg.buf0 = buf;
	msg.size0 = sizeof(buf);
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert(strcmp(buf, "ipctest") == 0);

	/* the third IPC enables writing to buffer 0 */
	msg.flags = SYS_IPC_BUF0W;
	msg.buf0 = buf;
	msg.size0 = sizeof(buf);
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert(strcmp(buf, "testipc") == 0);

	/* the fourth IPC enables reading/writing to buffer 0 in vector mode */
	strcpy(buf, "hihello");
	vec[0].addr = &buf[0];
	vec[0].size = 2;
	vec[1].addr = &buf[2];
	vec[1].size = 6;
	msg.flags = SYS_IPC_BUF0R | SYS_IPC_BUF0W | SYS_IPC_IOV;
	msg.iov = vec;
	msg.iov_num = 2;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert(strcmp(buf, "ipctest") == 0);

	sys_thread_exit();
}

static void test_ipc_read_and_write(void)
{
	struct sys_ipc msg = { 0 };
	uint32_t fds[2];
	uint32_t rhnd;
	char buf[16];
	size_t size;
	err_t err;

	printf("\n* IPC reading and writing tests\n");

	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	/* create IPC client thread first */
	create_thread(1, ipc_clientrw, fds, sys_prio_get());
	sys_yield();

	//--------------------------------------------------------------------------

	/* the first IPC does not enable any reading and writing */
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, 0);

	/* check the read and write API */

	/* read not allowed by client */
	err = sys_ipc_read(rhnd, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, ESTATE);

	/* invalid handle */
	err = sys_ipc_read(rhnd + 1, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, ESRCH);

	/* invalid buffer ID */
	err = sys_ipc_read(rhnd, -1u, buf, sizeof(buf), 0, &size);
	assert_eq(err, EINVAL);

	/* invalid buffer addr */
	err = sys_ipc_read(rhnd, 0, buf, -1000ul, 0, &size);
	assert_eq(err, EINVAL);

	/* invalid buffer size */
	err = sys_ipc_read(rhnd, 0, (void*)-1000ul, sizeof(buf), 0, &size);
	assert_eq(err, EINVAL);


	/* write not allowed by client */
	err = sys_ipc_write(rhnd, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, ESTATE);

	/* invalid handle */
	err = sys_ipc_write(rhnd + 1, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, ESRCH);

	/* invalid buffer ID */
	err = sys_ipc_write(rhnd, -1u, buf, sizeof(buf), 0, &size);
	assert_eq(err, EINVAL);

	/* invalid buffer addr */
	err = sys_ipc_write(rhnd, 0, buf, -1000ul, 0, &size);
	assert_eq(err, EINVAL);

	/* invalid buffer size */
	err = sys_ipc_write(rhnd, 0, (void*)-1000ul, sizeof(buf), 0, &size);
	assert_eq(err, EINVAL);

	/* OK write */
	err = sys_ipc_write(rhnd, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, ESTATE);

	//--------------------------------------------------------------------------

	/* the second IPC enables reading of buffer 0 */
	err = sys_ipc_reply_wait(rhnd, &msg, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* write not allowed by client */
	err = sys_ipc_write(rhnd, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, ESTATE);

	/* read OK full */
	err = sys_ipc_read(rhnd, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, EOK);
	assert_eq(size, 8);
	assert(strcmp(buf, "ipctest") == 0);

	/* read OK with offset */
	strcpy(buf, "else");
	err = sys_ipc_read(rhnd, 0, buf, sizeof(buf), 3, &size);
	assert_eq(err, EOK);
	assert_eq(size, 5);
	assert(strcmp(buf, "test") == 0);

	/* read OK beyond offset */
	err = sys_ipc_read(rhnd, 0, buf, sizeof(buf), 32, &size);
	assert_eq(err, EOK);
	assert_eq(size, 0);

	//--------------------------------------------------------------------------

	/* the third IPC enables writing to buffer 0 */
	err = sys_ipc_reply_wait(rhnd, &msg, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* read not allowed by client */
	err = sys_ipc_read(rhnd, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, ESTATE);

	/* write OK partial */
	strcpy(buf, "testipc");
	err = sys_ipc_write(rhnd, 0, buf, 4, 0, &size);
	assert_eq(err, EOK);
	assert_eq(size, 4);

	/* write OK with offset */
	err = sys_ipc_write(rhnd, 0, buf+4, sizeof(buf)-4, 4, &size);
	assert_eq(err, EOK);
	assert_eq(size, 4);

	/* write OK beyond offset */
	err = sys_ipc_write(rhnd, 0, buf, sizeof(buf), 32, &size);
	assert_eq(err, EOK);
	assert_eq(size, 0);

	//--------------------------------------------------------------------------

	/* the fourth IPC enables reading/writing to buffer 0 in vector mode */
	err = sys_ipc_reply_wait(rhnd, &msg, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* read OK */
	err = sys_ipc_read(rhnd, 0, buf, sizeof(buf), 0, &size);
	assert_eq(err, EOK);
	assert_eq(size, 8);
	assert(strcmp(buf, "hihello") == 0);

	/* write OK partial */
	strcpy(buf, "ipctest");
	err = sys_ipc_write(rhnd, 0, buf, 4, 0, &size);
	assert_eq(err, EOK);
	assert_eq(size, 4);

	/* write OK partial, remainder */
	err = sys_ipc_write(rhnd, 0, buf+4, 32-4, 4, &size);
	assert_eq(err, EOK);
	assert_eq(size, 4);

	//--------------------------------------------------------------------------

	/* reply to fourth IPC */
	err = sys_ipc_reply_wait(rhnd, &msg, 0, NULL, NULL);
	assert_eq(err, EOK);

	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EOK);

	/* let client thread terminate */
	sys_yield();

	printf("- OK\n");
}

////////////////////////////////////////////////////////////////////////////////

static void ipc_clientfd(void *arg)
{
	struct sys_ipc msg = { 0 };
	uint32_t *fds = arg;
	err_t err;

	/* the first IPC does not enable reception of file descriptors */
	msg.flags = 0;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, 0);

	/* the second IPC enables receiving one file descriptor */
	msg.flags = SYS_IPC_FD0;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, SYS_IPC_FD0);
	assert_eq(msg.fd0, fds[0] + 2);

	/* the third IPC enables receiving two file descriptors */
	msg.flags = SYS_IPC_FD0 | SYS_IPC_FD1;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, (SYS_IPC_FD0|SYS_IPC_FD1));
	assert_eq(msg.fd0, fds[0] + 3);
	assert_eq(msg.fd1, fds[0] + 4);

	/* the fourth IPC enables receiving two file descriptors, we transfer one */
	msg.flags = SYS_IPC_FD0 | SYS_IPC_FD1;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, SYS_IPC_FD0);
	assert_eq(msg.fd0, fds[0] + 5);

	err = sys_fd_close(fds[0] + 2);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[0] + 3);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[0] + 4);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[0] + 5);
	assert_eq(err, EOK);

	sys_thread_exit();
}

static void test_ipc_fd(void)
{
	struct sys_ipc msg = { 0 };
	uint32_t fds[2];
	uint32_t rhnd;
	err_t err;

	printf("\n* IPC file descriptor sharing tests\n");

	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	/* create IPC client thread first */
	create_thread(1, ipc_clientfd, fds, sys_prio_get());
	sys_yield();

	//--------------------------------------------------------------------------

	/* the first IPC does not enable reception of file descriptors */
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, 0);

	/* test API */

	/* invalid receive handle */
	err = sys_ipc_sharefd(rhnd + 1, fds[0], 0);
	assert_eq(err, ESRCH);

	/* invalid fd */
	err = sys_ipc_sharefd(rhnd, fds[0]+42, 0);
	assert_eq(err, EBADF);

	/* invalid flags */
	err = sys_ipc_sharefd(rhnd, fds[0], -1u);
	assert_eq(err, EINVAL);

	/* try sharing one (no opt-in) */
	err = sys_ipc_sharefd(rhnd, fds[0], 0);
	assert_eq(err, ESTATE);

	//--------------------------------------------------------------------------

	/* the second IPC enables receiving one file descriptor */
	err = sys_ipc_reply_wait(rhnd, &msg, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* try sharing one */
	err = sys_ipc_sharefd(rhnd, fds[0], 0);
	assert_eq(err, EOK);

	/* try sharing another one */
	err = sys_ipc_sharefd(rhnd, fds[1], 0);
	assert_eq(err, ESTATE);

	//--------------------------------------------------------------------------

	/* the third IPC enables receiving two file descriptors */
	err = sys_ipc_reply_wait(rhnd, &msg, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* try sharing one */
	err = sys_ipc_sharefd(rhnd, fds[0], 0);
	assert_eq(err, EOK);

	/* try sharing another one */
	err = sys_ipc_sharefd(rhnd, fds[1], 0);
	assert_eq(err, EOK);

	/* try sharing a third one */
	err = sys_ipc_sharefd(rhnd, fds[0], 0);
	assert_eq(err, ESTATE);

	//--------------------------------------------------------------------------

	/* the fourth IPC enables receiving two file descriptors, we transfer one */
	err = sys_ipc_reply_wait(rhnd, &msg, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* share one and close it -- this is the IPC client_fd! */
	err = sys_ipc_sharefd(rhnd, fds[1], SYS_IPC_CLOSE);
	assert_eq(err, EOK);

	//--------------------------------------------------------------------------

	/* reply to fourth IPC */
	err = sys_ipc_reply_wait(rhnd, &msg, 0, NULL, NULL);
	assert_eq(err, EOK);

	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EBADF);

	/* let client thread terminate */
	sys_yield();

	printf("- OK\n");
}

////////////////////////////////////////////////////////////////////////////////

static void ipc_clientmap(void *arg)
{
	struct sys_ipc msg = { 0 };
	uint32_t *fds = arg;
	err_t err;

	/* the first IPC does not enable reception of a mapping */
	msg.flags = 0;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, 0);

	/* the second IPC enables receiving a mapping (two pages) */
	msg.flags = SYS_IPC_MAP;
	msg.map_prot = PROT_READ;
	msg.map_flags = MAP_SHARED;
	msg.map_start = 0;
	msg.map_addr = 0;
	msg.map_size = 2*PAGE_SIZE;
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, SYS_IPC_MAP);
	assert(msg.map_start > 0);

	/* NOTE: the start address of the mapping changes in each invocation! */
	printf("- msg.map_start: 0x%zx\n", msg.map_start);

	/* cleanup */
	err = sys_vm_unmap(msg.map_start, 2*PAGE_SIZE);
	assert_eq(err, EOK);

	sys_thread_exit();
}

static void test_ipc_map(void)
{
	struct sys_ipc msg = { 0 };
	uint32_t fds[2];
	uint32_t map_fd;
	uint32_t rhnd;
	err_t err;

	printf("\n* IPC mapping tests\n");

	/* get ID of special memory requirement for testing */
	map_fd = __open_memrq(TEST_MAP_NAME, MEMRQ_TYPE_MEM, O_RDWR);
	assert_ne(map_fd, -1u);

	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	/* create IPC client thread first */
	create_thread(1, ipc_clientmap, fds, sys_prio_get());
	sys_yield();

	//--------------------------------------------------------------------------

	/* the first IPC does not enable reception of a mapping */
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);
	assert_eq(msg.flags, 0);

	/* test API */
	/* invalid rhnd */
	err = sys_ipc_sharemap(rhnd + 1, 0, map_fd, 0);
	assert_eq(err, ESRCH);

	/* invalid flags */
	err = sys_ipc_sharemap(rhnd, 0xffffffff, map_fd, 0);
	assert_eq(err, EINVAL);

	/* offset not page aligned */
	err = sys_ipc_sharemap(rhnd, 0, map_fd, 0x1);
	assert_eq(err, EINVAL);

	/* invalid flags */
	err = sys_ipc_sharemap(rhnd, 0xffffffff, map_fd, 0);
	assert_eq(err, EINVAL);

	/* invalid file descriptor */
	err = sys_ipc_sharemap(rhnd, 0, 0xffffffff, 0);
	assert_eq(err, EBADF);

	/* client did not opt-in to receive a mapping */
	err = sys_ipc_sharemap(rhnd, 0, map_fd, 0);
	assert_eq(err, ESTATE);

	//--------------------------------------------------------------------------

	/* the second IPC enables receiving a mapping (two pages) */
	err = sys_ipc_reply_wait(rhnd, &msg, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* test API */
	/* range exceeds memory requirement */
	err = sys_ipc_sharemap(rhnd, 0, map_fd, TEST_MAP_SIZE);
	assert_eq(err, ESTATE);

	/* works -- this installs the mapping */
	err = sys_ipc_sharemap(rhnd, 0, map_fd, 0);
	assert_eq(err, EOK);

	/* client already received a mapping */
	err = sys_ipc_sharemap(rhnd, 0, map_fd, 0);
	assert_eq(err, ESTATE);

	//--------------------------------------------------------------------------

	/* reply to second IPC */
	err = sys_ipc_reply_wait(rhnd, &msg, 0, NULL, NULL);
	assert_eq(err, EOK);

	err = sys_fd_close(map_fd);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EOK);

	/* let client thread terminate */
	sys_yield();

	printf("- OK\n");
}

////////////////////////////////////////////////////////////////////////////////

static void ipc_thread_close(void *arg)
{
	struct sys_ipc msg = { 0 };
	uint32_t *fds = arg;
	uint32_t rhnd;
	err_t err;

	/* the client is cancelled waiting in IPC CALL */
	STEP(1);
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EPIPE);

	/* the client is cancelled waiting in IPC REPLY */
	STEP(4);
	err = sys_ipc_call(fds[1], &msg, &msg);
	assert_eq(err, EPIPE);

	/* the client is now a server and is cancelled waiting in IPC WAIT */
	STEP(7);
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EPIPE);

	STEP(10);
	sys_thread_exit();
}

static void test_ipc_close(void)
{
	struct sys_ipc msg = { 0 };
	uint32_t fds[2];
	uint32_t rhnd;
	err_t err;

	printf("\n* IPC close tests\n");

	step = 0;

	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	/* create IPC client/server thread first */
	STEP(0);
	create_thread(1, ipc_thread_close, fds, sys_prio_get());
	sys_yield();

	/* close fds while the client is waiting in IPC CALL */
	STEP(2);
	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EOK);

	/* create new pair of file descriptors */
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	/* let client run */
	STEP(3);
	err = sys_ipc_reply_wait(0, NULL, fds[0], &rhnd, &msg);
	assert_eq(err, EOK);

	/* close fds while the client is waiting in IPC REPLY */
	STEP(5);
	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EOK);

	/* create new pair of file descriptors */
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	assert_eq(err, EOK);

	/* let client run */
	STEP(6);
	sys_yield();

	/* close fds while the server is waiting in IPC WAIT */
	STEP(8);
	err = sys_fd_close(fds[0]);
	assert_eq(err, EOK);
	err = sys_fd_close(fds[1]);
	assert_eq(err, EOK);

	/* let client thread terminate */
	STEP(9);
	sys_yield();
	STEP(11);

	printf("- OK\n");
}

////////////////////////////////////////////////////////////////////////////////

void test_ipc(void)
{
	print_header("Kernel IPC Implementation");

	test_ipc_port_names();
	test_ipc_fds();
	test_ipc_msg_validation();
	test_ipc_communication();
	test_ipc_read_and_write();
	test_ipc_fd();
	test_ipc_map();
	test_ipc_close();
}
