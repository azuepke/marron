/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * test_timerfd.c
 *
 * Testcases for timerfd.
 *
 * azuepke, 2022-10-24: initial
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"
#include "bench.h"
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/timerfd.h>


#if 0
#define vprintf(x...) printf(x)
#define vvprintf(x...) printf(x)
#else
#define vprintf(x...) (void)0
#define vvprintf(x...) (void)0
#endif

void test_timerfd(void)
{
	uint64_t expiry_count;
	struct itimerspec it;
	sys_time_t start;
	sys_time_t now;
	unsigned int i;
	ssize_t r;
	err_t err;
	int ret;
	int fd2;
	int fd;

	print_header("timerfd");

	printf("- open / close: ");

	/* invalid clock ID */
	fd = timerfd_create(NUM_CLK_IDS, 0);
	assert(fd == -1);
	assert(errno == EINVAL);

	/* invalid flags */
	fd = timerfd_create(CLK_ID_MONOTONIC, ~0);
	assert(fd == -1);
	assert(errno == EINVAL);

	/* should work */
	fd = timerfd_create(CLK_ID_MONOTONIC, 0);
	assert(fd != -1);

	close(fd);

	/* again, should work a second time */
	fd2 = timerfd_create(CLK_ID_MONOTONIC, 0);
	assert(fd2 != -1);
	assert(fd2 == fd);

	close(fd2);

	printf("OK\n");


	printf("- monotonic timer: ");

	/* expected to work */
	fd = timerfd_create(CLK_ID_MONOTONIC, 0);
	assert(fd != -1);

	it.it_value.tv_sec = 0;
	it.it_value.tv_nsec = 100*1000*1000;
	it.it_interval.tv_sec = 0;
	it.it_interval.tv_nsec = 10*1000*1000;

	/* periodic 10ms timer after 100ms */
	ret = timerfd_settime(fd, 0, &it, NULL);
	assert_eq(ret, 0);

	/* expected to work */
	ret = timerfd_gettime(fd, &it);
	assert_eq(ret, 0);
	assert(it.it_value.tv_sec == 0);
	assert(it.it_value.tv_nsec <= 100*1000*1000);
	assert(it.it_interval.tv_sec == 0);
	assert(it.it_interval.tv_nsec == 10*1000*1000);

	/* expected to work */

	expiry_count = 42;
	r = read(fd, &expiry_count, sizeof expiry_count);
	assert_eq(r, sizeof expiry_count);
	assert_eq(expiry_count, 1);

	err = sys_clock_get(CLK_ID_MONOTONIC, &start);
	assert_eq(err, EOK);
	assert3(start, >, 0);

	/* wait for at least 100 timer expirations (1 second overall) */
	i = 0;
	while (i < 100) {
		/* waiting mode && expected to work */
		r = read(fd, &expiry_count, sizeof expiry_count);
		assert_eq(r, sizeof expiry_count);
		/* NOTE: unstable timing on QEMU, accept delays up to 100ms */
		assert3(expiry_count, >=, 1);
		assert3(expiry_count, <=, 10);
		i += expiry_count;
	}

	err = sys_clock_get(CLK_ID_MONOTONIC, &now);
	assert_eq(err, EOK);
	assert3(now, >, 0);
	assert3(start, <, now);
	assert3(now - start, >=, (i-1)*10*1000*1000);
	assert3(now - start, <=, (i+1)*10*1000*1000);

	/* expected to work */
	ret = close(fd);
	assert_eq(ret, 0);

	printf("OK\n");
}
