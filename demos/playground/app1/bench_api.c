/* SPDX-License-Identifier: MIT */
/* Copyright 2014, 2015, 2020 Alexander Zuepke */
/*
 * bench_api.c
 *
 * CPU core benchmark
 *
 * azuepke, 2014-03-04: initial
 * azuepke, 2015-01-26: add low-level syscall benchmarks
 * azuepke, 2020-01-31: adapted for Marron
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <marron/compiler.h>
#include "app.h"
#include "bench.h"
/* this is a benchmark, no not enable assert() unconditionally */
#include <assert_ext.h>
#include <marron/util.h>
#include <marron/arch_tls.h>
#include <stddef.h>


void bench_api(void)
{
	TIME_T before, after;
	unsigned int runs;
	unsigned int prio;
	uint32_t futex;
	int kldd_id;
	int c;

	printf("\n*** core API:\n");

	printf("- GETTS()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		GETTS();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_gettime()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		uint64_t t0;
		sys_clock_get(CLK_ID_MONOTONIC, &t0);
	}
	after = GETTS();
	delta(before, after);


	printf("- null TLS access\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_self();
	}
	after = GETTS();
	delta(before, after);


	printf("- null syscall\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_self_syscall();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_yield()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_yield();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_preempt()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_preempt();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_futex_wait() compare fail\t\t\t\t");
	futex = 0;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_futex_wait(&futex, 1, 0xffffffff, 0, TIMEOUT_NULL, CLK_ID_MONOTONIC);
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_futex_wake(&futex, 0)\t\t\t\t");
	futex = 0;
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_futex_wake(&futex, 0);
	}
	after = GETTS();
	delta(before, after);


	/* fast prio switching */
	printf("- prio fast: set/restore pair inlined\t\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		struct sys_tls *tls = (struct sys_tls *)__tls_base();
		/* NOTE: inlined version of fast priority switching */
		tls->user_prio = prio;
		barrier();
		tls->user_prio = prio - 1;
		if (tls->next_prio >= prio-1) {
			sys_preempt();
		}
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- prio fast: set/restore pair\t\t\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_prio_set(prio);
		sys_prio_set(prio-1);
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- prio fast: set/restore pair + syscall\t\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		/* NOTE: we hack next_prio in TLS to enforce a system call */
		sys_prio_set(prio+1);
		tls_set_1(offsetof(struct sys_tls, next_prio), 0xff);
		sys_prio_set(prio);
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- prio slow: set/restore pair with syscalls\t\t");
	prio = sys_prio_get();
	sys_prio_set(prio-1);
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_prio_set_syscall(prio);
		sys_prio_set_syscall(prio-1);
	}
	after = GETTS();
	delta(before, after);
	sys_prio_set(prio);


	printf("- poll console for input\t\t\t\t");
	kldd_id = config_kldd_id("con");
	if (kldd_id != -1) {

		before = GETTS();
		for (runs = 0; runs < RUNS; runs++) {
			/* get character using the KLDD */
			(void)sys_kldd_call(kldd_id, 1, (unsigned long)&c, 0, 0);
		}
		after = GETTS();
		delta(before, after);
	} else {
		printf("test skipped\n");
	}
}
