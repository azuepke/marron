/* SPDX-License-Identifier: MIT */
/* Copyright 2018 Andreas Werner */
/*
 * test_partition.c
 *
 * Partition testcases
 *
 * awerner, 2018-03-28: initial
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"
/*#define VERBOSE*/
#ifdef VERBOSE
# define vprint(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
# define vprint(fmt, ...)
#endif

static void print_partion_state(unsigned int state)
{
#ifdef VERBOSE
	sys_time_t now;
	sys_clock_get(&now, CLK_ID_MONOTONIC);
	switch (state) {
		case PART_STATE_IDLE:
			vprint("# Part 2 is in IDLE now is %lld\n", now);
			break;
		case PART_STATE_RUNNING:
			vprint("# Part 2 is in RUNNING now is %lld\n", now);
			break;
	}
#else
	(void) state;
#endif
}

void test_partition(volatile uint32_t *shm1)
{
	err_t err;
	unsigned int state;
	unsigned int oldstate;

	print_header("Partition Implementation");

	for (int i = 0; i < 2; i++) {
		/* test get Partition state */
		printf("# Test get partition state: ");
		vprint("\n");
		vprint("# Get State of Partition 2\n");
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_IDLE);
		vprint("# ");
		printf("ok\n");

		/* test start partition */
		printf("# Test start partition: ");
		vprint("\n");
		vprint("# Start Partition 2\n");
		shm1[2] = 0x0; /* reset spinlock */
		shm1[3] = 0x0; /* select first test */
		shm1[4] = 0x0; /* reset state variable */
		err = sys_part_restart_other(2);
		assert_eq(err, EOK);
		/* let run partition */
		sys_sleep(100*1000*1000, CLK_ID_MONOTONIC);
		/* check state */
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_RUNNING);
		/* check Partition execute code */
		assert_eq(shm1[4], 1);
		vprint("# ");
		printf("ok\n");

		/* Test wait for self termination */
		printf("# Test wait for termination: ");
		vprint("\n");
		vprint("# Let Partition 2 run until self termination\n");
		/*start app2*/
		shm1[2] = 0x1;
		/* wait self termination */
		do {
			oldstate = state;
			err = sys_part_state_other(2, &state);
			assert_eq(err, EOK);
			if (oldstate != state) {
				print_partion_state(state);
			}
			sys_sleep(1000*1000, CLK_ID_MONOTONIC);
		} while (state != 0);
		vprint("# ");
		printf("ok\n");

		/* Test restart partition after dead*/
		printf("# Test Restart Partition after termination: ");
		vprint("\n");
		vprint("# Restart Partition 2");
		/* restart Partition */
		shm1[2] = 0x1; /* set partition run throw spinlock */
		shm1[3] = 0x1; /* set part 2 test 2 */
		err = sys_part_restart_other(2);
		assert_eq(err, EOK);
		/* wait a bit */
		sys_sleep(2*1000*1000*1000, CLK_ID_MONOTONIC);
		/* check state */
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_RUNNING);
		vprint("# ");
		printf("ok\n");

		/* test Interrupt Partition and restart while running */
		/* partition is running test 2 (endless loop) */
		printf("# Test Interrupt Partition and restart: ");
		vprint("\n");
		vprint("# Interrupt Partition 2 and restart\n");
		/* Interrupt Partion while running */
		shm1[2] = 0x0; /* clear spinlock */
		shm1[3] = 0x1; /* set part 2 test 2 */
		shm1[4] = 0x0; /* reset state variable */
		err = sys_part_restart_other(2);
		assert_eq(err, EOK);
		sys_sleep(100*1000*1000, CLK_ID_MONOTONIC);
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_RUNNING);
		assert_eq(shm1[4], 1);
		vprint("# ");
		printf("ok\n");

		/* test kill partition while running */
		/* partition is running test 2 (endless loop) */
		printf("# Test Kill Partition while running: ");
		vprint("\n");
		shm1[2] = 0x1; /* start endless loop */
		sys_sleep(2*1000*1000*1000, CLK_ID_MONOTONIC);
		vprint("# Kill Partition 2 \n");
		/* kill Partition */
		err = sys_part_shutdown_other(2);
		assert_eq(err, EOK);
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_IDLE);
		vprint("# ");
		printf("ok\n");

		/* test partition self restart */
		printf("# Test Partition self restart: ");
		vprint("\n");
		vprint("# Restart Partition\n");
		shm1[2] = 0x0;
		shm1[3] = 0x2; /* set part 2 test 3 (self restart) */
		shm1[4] = 0x0;
		err = sys_part_restart_other(2);
		assert_eq(err, EOK);
		sys_sleep(1000*1000*1000, CLK_ID_MONOTONIC);
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_RUNNING);
		/* check Partion is in init */
		assert_eq(shm1[4], 1);
		vprint("# State Varable: %u\n", shm1[4]);
		vprint("# Start self restart test\n");
		shm1[2] = 0x1; /* start test */
		sys_sleep(1000*1000*1000, CLK_ID_MONOTONIC);
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_RUNNING);
		sys_sleep(3*1000*1000*1000ULL, CLK_ID_MONOTONIC);
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_RUNNING);
		vprint("# State Varable: %u\n", shm1[4]);
		/* check Partion is in init */
		assert_eq(shm1[4], 2);
		vprint("# ");
		printf("ok\n");

		/* test inter-partition communication */
		printf("# Test inter-partition communication: ");
		vprint("\n");
		vprint("# Restart Partition");
		shm1[2] = 0x1; /* run to wait */
		shm1[3] = 0x3; /* set part 2 test 4 (ping / pong) */
		shm1[4] = 0x0;
		err = sys_part_restart_other(2);
		assert_eq(err, EOK);
		sys_sleep(1000*1000*1000, CLK_ID_MONOTONIC);
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_RUNNING);
		/* check Partion is in init */
		assert_eq(shm1[4], 1);
		vprint("# send ping\n");
		shm1[5] = 1;
		sys_sleep(10*1000*1000, CLK_ID_MONOTONIC);
		vprint("# wait for pong\n");
		while (shm1[6] == 0) {
			sys_sleep(10*1000*1000, CLK_ID_MONOTONIC);
		}
		shm1[6] = 0;
		vprint("# ");
		printf("ok\n");

		/* Test Partition Permissions */
		shm1[2] = 0x1; /* run to test */
		shm1[3] = 0x4; /* set part 2 test 5 (Permission Test) */
		shm1[4] = 0x0;
		err = sys_part_restart_other(2);
		assert_eq(err, EOK);
		/* Test send event on finish */
		while (shm1[6] == 0) {
			sys_sleep(10*1000*1000, CLK_ID_MONOTONIC);
		}
		shm1[6] = 0;

		/* shutdown partition after last test */
		err = sys_part_shutdown_other(2);
		assert_eq(err, EOK);
		err = sys_part_state_other(2, &state);
		assert_eq(err, EOK);
		print_partion_state(state);
		assert_eq(state, PART_STATE_IDLE);
	}
}
