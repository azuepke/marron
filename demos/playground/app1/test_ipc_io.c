/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2024 Alexander Zuepke */
/*
 * test_ipc_io.c
 *
 * Testcases for IPC-based I/O.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2024-12-28: use epoll
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <marron/ioserver.h>
#include <marron/macros.h>
#include <marron/mapping.h>
#include <marron/epoll.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include <marron/util.h>
#include "app.h"
#include "bench.h"
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>


#if 1
#define vprintf(x...) printf(x)
#define vvprintf(x...) printf(x)
#else
#define vprintf(x...) (void)0
#define vvprintf(x...) (void)0
#endif

static const char content[] = "Hello, World";
static struct ioserver_file file_instance;
static uint32_t filefd = -1;
static uint32_t epollfd;
static int mapfd;

static inline void epoll_watch(uint32_t epoll_fd, uint32_t fd)
{
	struct sys_epoll event = { 0 };
	err_t err;

	event.eventmask = EPOLLIN | EPOLLOUT | EPOLLHUP | EPOLLERR;
	event.data = fd;
	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event);
	assert_eq(err, EOK);
}

static inline void epoll_unwatch(uint32_t epoll_fd, uint32_t fd)
{
	err_t err;

	err = sys_epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, NULL);
	assert_eq(err, EOK);
}

static inline uint32_t epoll_wait(uint32_t epoll_fd)
{
	struct sys_epoll event = { 0 };
	uint32_t num;
	err_t err;

	err = sys_epoll_wait(epoll_fd, 1, TIMEOUT_INFINITE, NULL, &event, &num);
	assert_eq(err, EOK);
	assert_eq(num, 1);

	return event.data;
}

err_t i_open(struct ioserver_fs *s, uint32_t rhnd, const char *pathname, uint32_t oflags, uint32_t mode)
{
	struct ioserver_file *f;
	uint32_t fds[2];
	err_t err;

	vprintf("* i_open\n");

	(void)pathname;
	(void)mode;

	if (filefd != -1u) {
		/* we only allow one open instance; ENOMEM is OK-ish for this */
		return ENOMEM;
	}

	if (oflags & (O_DIRECTORY | O_PATH)) {
		return EINVAL;
	}

	/* get a new file instance */
	f = &file_instance;
	f->ioserver = s;
	f->oflags = oflags | __O_SEEK;
	f->offset = 0;
	f->length = sizeof(content);

	/* create an IPC communication pair */
	err = sys_ipc_pair(0, &fds[0], &fds[1]);
	if (err != EOK) {
		return err;
	}

	/* fd[0] is server side, fd[1] is client side */
	err = sys_ipc_sharefd(rhnd, fds[1], SYS_IPC_CLOSE);
	if (err != EOK) {
		return err;
	}

	filefd = fds[0];
	epoll_watch(epollfd, filefd);

	return EOK;
}

err_t i_read(struct ioserver_file *f, uint32_t rhnd, size_t size, off_t offset, size_t *rsize)
{
	vprintf("* i_read(size=%zu, offset=%lld)\n", size, (long long)offset);

	if (offset >= f->length) {
		*rsize = 0;
		return EOK;
	}

	if ((off_t)size > f->length - offset) {
		size = f->length - offset;
	}

	return sys_ipc_write_exact(rhnd, 0, content, size, offset, rsize);
}

err_t i_write(struct ioserver_file *f, uint32_t rhnd, size_t size, off_t offset, size_t *wsize)
{
	vprintf("* i_write(size=%zu, offset=%lld)\n", size, (long long)offset);

	(void)f;
	(void)rhnd;
	(void)size;
	(void)offset;
	(void)wsize;

	return ENOSPC;
}

err_t i_mmap(struct ioserver_file *f, uint32_t rhnd, off_t offset, size_t size, unsigned int prot, unsigned int flags)
{
	size_t aligned_size;

	vprintf("* i_mmap(size=%zu, offset=%lld, prot=%x, flags=%x)\n", size, (long long)offset, prot, flags);

	/* in this testcase, we map exactly one page,
	 * so offset or size beyond PAGE_SIZE are not supported
	 */
	aligned_size = ALIGN_UP(f->length, PAGE_SIZE);
	if (offset + size > aligned_size) {
		/* ENXIO covers all errors with the mappable range */
		return ENXIO;
	}

	if ((prot & PROT_EXEC) != 0) {
		/* as device driver, we do not support executable mappings */
		return ENOTSUP;
	}

	/* notify server on mprotect and munmap */
	flags |= MAP_IPC_MPROTECT | MAP_IPC_MUNMAP;

	return sys_ipc_sharemap(rhnd, flags, mapfd, offset);
}

/* callbacks for file operations -- see ioserver.h */
static const struct d_callback d_callback = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_mmap = d_mmap,				/* i_mmap() */
	.d_seek = d_seek,				/* i_seek() optional */
}};

/* IPC server loop */
static void ipc_io_server(void *arg __unused)
{
	struct ioserver_fs server_instance;
	struct sys_ipc msg;
	unsigned int openfd;
	unsigned int fd;
	uint32_t rhnd;
	int status;
	err_t err;
	int port;
	int running;

	port = config_ipc_port_id(IPC_IO_SERVER_NAME, IPC_TYPE_SERVER);
	assert3(port, !=, -1);

	err = sys_ipc_port_fd(IPC_TYPE_SERVER, port, &openfd);
	assert_eq(err, EOK);

	err = sys_epoll_create(0, &epollfd);
	assert_eq(err, EOK);

	epoll_watch(epollfd, openfd);

	mapfd = __open_memrq(TEST_MAP_NAME, MEMRQ_TYPE_MEM, O_RDWR);
	assert_ne(mapfd, -1);

	running = 1;
	while (running) {
		fd = epoll_wait(epollfd);

		if (fd == openfd) {
			err = sys_ipc_wait(openfd, &rhnd, &msg);
			assert_eq(err, EOK);

			vvprintf("* ipc_io_server: open: got %u\n", msg.req);
			if (msg.req == 99) {
				/* protocol hack to stop the server; reply to stop request */
				running = 0;
			}

			status = dm_dispatch_open_dev(&server_instance, rhnd, &msg);
			assert(status == IOSERVER_OK);

			err = sys_ipc_reply(rhnd, &msg);
			assert_eq(err, EOK);
		} else if (fd == filefd) {
			err = sys_ipc_wait(filefd, &rhnd, &msg);
			assert((err == EOK) || (err == EPIPE));

			vvprintf("* ipc_io_server: file: got %u\n", msg.req);
			if (err == EPIPE) {
				epoll_unwatch(epollfd, filefd);
				err = sys_fd_close(filefd);
				assert_eq(err, EOK);
				filefd = -1;
			} else {
				status = dm_dispatch_file(&file_instance, rhnd, &msg, &d_callback);
				assert(status == IOSERVER_OK);

				err = sys_ipc_reply(rhnd, &msg);
				assert_eq(err, EOK);
			}
		} else {
			assert(0);
		}
	}

	err = sys_fd_close(mapfd);
	assert_eq(err, EOK);

	err = sys_fd_close(epollfd);
	assert_eq(err, EOK);

	if (filefd != -1u) {
		err = sys_fd_close(filefd);
		assert_eq(err, EOK);
		filefd = -1;
	}

	sys_thread_exit();
}

static void ipc_io_test(void)
{
	phys_addr_t phys;
	unsigned int prot;
	addr_t next;
	char buf[16];
	ssize_t r;
	off_t off;
	err_t err;
	int arg;
	int ret;
	void *m;
	int fd;

	/* open /dev/test */
	fd = open("does-not-exists", O_RDWR);
	assert_eq(fd, -1);
	assert_eq(errno, ENOENT);

	fd = open(IPC_IO_CLIENT_NAME, O_RDWR);
	assert(fd != -1);

	/* set O_NONBLOCK flag */
	ret = fcntl(fd, F_GETFL);
	assert_eq(ret & O_ACCMODE, O_RDWR);

	ret = fcntl(fd, F_SETFL, O_NONBLOCK | ret);
	assert_eq(ret, 0);

	ret = fcntl(fd, F_GETFL);
	assert_eq(ret & (O_NONBLOCK | O_ACCMODE), O_NONBLOCK | O_RDWR);

	/* clear O_NONBLOCK flag again */
	arg = 0;
	ret = ioctl(fd, FIONBIO, &arg);
	assert_eq(ret, 0);

	ret = fcntl(fd, F_GETFL);
	assert_eq(ret & O_ACCMODE, O_RDWR);

	/* read data and seek in file */
	r = read(fd, buf, sizeof(buf));
	assert_eq(r, 13);

	r = read(fd, buf, sizeof(buf));
	assert_eq(r, 0);

	off = lseek(fd, 0, SEEK_CUR);
	assert_eq(off, 13);

	off = lseek(fd, -1, SEEK_CUR);
	assert_eq(off, 12);

	off = lseek(fd, -1, SEEK_END);
	assert_eq(off, 12);

	r = read(fd, buf, sizeof(buf));
	assert_eq(r, 1);

	r = write(fd, buf, 1);
	assert_eq(r, -1);
	assert_eq(errno, ENOSPC);

	/* memory mapping */
	/* error: offset out of range */
	m = mmap(NULL, 123, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0x3000);
	assert_eq(m, MAP_FAILED);
	assert_eq(errno, ENXIO);

	/* error: PROT_EXEC not supported */
	m = mmap(NULL, 123, PROT_EXEC, MAP_SHARED, fd, 0);
	assert_eq(m, MAP_FAILED);
	assert_eq(errno, ENOTSUP);

	m = mmap(NULL, 123, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	assert(m != MAP_FAILED);

	err = sys_vm_v2p((addr_t)m, &phys, &prot, &next);
	assert_eq(err, EOK);
	assert_eq(prot, PROT_READ | PROT_WRITE);

	ret = mprotect(m, 123, PROT_READ);
	assert_eq(ret, 0);

	ret = mprotect(m, 123, PROT_WRITE);
	assert_eq(ret, 0);

	ret = munmap(m, 123);
	assert_eq(ret, 0);


	/* close file */
	ret = close(fd);
	assert_eq(ret, 0);

	ret = close(fd);
	assert_eq(ret, -1);
	assert_eq(errno, EBADF);

	/* kick server thread */
	sys_yield();

	/* open and close again */
	fd = open(IPC_IO_CLIENT_NAME, O_RDWR);
	assert(fd != -1);

	ret = close(fd);
	assert_eq(ret, 0);

	/* kick server thread again */
	sys_yield();
}

void test_ipc_io(void)
{
	struct sys_ipc msg = { 0 };
	unsigned int openfd;
	err_t err;
	int port;

	print_header("IPC I/O operations");

	port = config_ipc_port_id(IPC_IO_CLIENT_NAME, IPC_TYPE_CLIENT);
	assert3(port, !=, -1);

	err = sys_ipc_port_fd(IPC_TYPE_CLIENT, port, &openfd);
	assert_eq(err, EOK);

	create_thread(1, ipc_io_server, NULL, sys_prio_get());
	sys_yield();

	ipc_io_test();

	/* terminate IPC open server */
	msg.req = 99;
	err = sys_ipc_call(openfd, &msg, &msg);
	assert_eq(err, EOK);
	sys_yield();

	printf("- OK!\n");
}
