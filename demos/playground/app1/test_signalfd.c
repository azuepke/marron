/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * test_eventfd.c
 *
 * Testcases for signalfd.
 *
 * azuepke, 2025-02-14: initial
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#undef NDEBUG /* always keep assert() activated */
#include <assert_ext.h>
#include "app.h"
#include "bench.h"
#include <unistd.h>
#include <signal.h>
#include <sys/signalfd.h>
#include <sys/epoll.h>
#include <errno.h>
#include <fcntl.h>


static int mytimer_create(int sig)
{
	struct sys_sig_info info = { 0 };
	unsigned int timer_id;
	err_t ret;

	info.sig = sig;
	ret = sys_timer_create(CLK_ID_MONOTONIC, 0, &info, 0, &timer_id);
	assert_eq(ret, EOK);

	return timer_id;
}

static void mytimer_arm(int timer_id, unsigned int ms)
{
	err_t ret;

	assert(ms < 1000);
	ret = sys_timer_set(timer_id, 0, ms*1000*1000, 0);
	assert_eq(ret, EOK);
}

static void mytimer_delete(int timer_id)
{
	err_t ret;

	ret = sys_fd_close(timer_id);
	assert_eq(ret, EOK);
}

static void test_signalfd_api(void)
{
	struct signalfd_siginfo ssi;
	sigset_t mask;
	ssize_t rw;
	int timer;
	int ret;
	int fd;

	printf("- basic API\n");

	sigemptyset(&mask);
	errno = EOK;
	fd = signalfd(-1, &mask, SFD_NONBLOCK);
	assert_ne(fd, -1);
	assert_eq(errno, EOK);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, -1);
	assert_eq(errno, EAGAIN);

	kill(0, SIGUSR1);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, -1);
	assert_eq(errno, EAGAIN);

	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);
	errno = EOK;
	ret = signalfd(fd, &mask, SFD_NONBLOCK);
	assert_eq(ret, fd);
	assert_eq(errno, EOK);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, sizeof(ssi));
	assert_eq(errno, EOK);
	assert_eq(ssi.ssi_signo, SIGUSR1);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, -1);
	assert_eq(errno, EAGAIN);

	/* the two SIGUSR1 signals will be reported as one */
	kill(0, SIGUSR1);
	kill(0, SIGUSR1);
	kill(0, SIGUSR2);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, sizeof(ssi));
	assert_eq(errno, EOK);
	assert_eq(ssi.ssi_signo, SIGUSR1);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, sizeof(ssi));
	assert_eq(errno, EOK);
	assert_eq(ssi.ssi_signo, SIGUSR2);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, -1);
	assert_eq(errno, EAGAIN);

	/* blocking read test: change fd to blocking mode */
	errno = EOK;
	ret = fcntl(fd, F_SETFL, 0);
	assert_eq(ret, 0);
	assert_eq(errno, EOK);

	timer = mytimer_create(SIGUSR1);

	mytimer_arm(timer, 10);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, sizeof(ssi));
	assert_eq(errno, EOK);
	assert_eq(ssi.ssi_signo, SIGUSR1);

	mytimer_delete(timer);
	close(fd);
}

static void test_signalfd_epoll(void)
{
	struct signalfd_siginfo ssi;
	struct epoll_event e;
	sigset_t mask;
	ssize_t rw;
	int epfd;
	int ret;
	int fd;

	printf("- basic API\n");

	sigemptyset(&mask);
	errno = EOK;
	fd = signalfd(-1, &mask, SFD_NONBLOCK);
	assert_ne(fd, -1);
	assert_eq(errno, EOK);

	epfd = epoll_create1(0);
	assert_ne(epfd, -1);

	e.events = EPOLLIN;
	e.data.fd = fd;
	ret = epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &e);
	assert_eq(ret, 0);

	ret = epoll_wait(epfd, &e, 1, 0);
	assert_eq(ret, 0);

	kill(0, SIGUSR1);

	ret = epoll_wait(epfd, &e, 1, 0);
	assert_eq(ret, 0);

	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	errno = EOK;
	ret = signalfd(fd, &mask, SFD_NONBLOCK);
	assert_eq(ret, fd);
	assert_eq(errno, EOK);

	ret = epoll_wait(epfd, &e, 1, 0);
	assert_eq(ret, 1);
	assert_eq(e.data.fd, fd);

	errno = EOK;
	rw = read(fd, &ssi, sizeof(ssi));
	assert_eq(rw, sizeof(ssi));
	assert_eq(errno, EOK);
	assert_eq(ssi.ssi_signo, SIGUSR1);

	ret = epoll_wait(epfd, &e, 1, 0);
	assert_eq(ret, 0);

	close(epfd);
	close(fd);
}

void test_signalfd(void)
{
	sigset_t mask, old_mask;
	int ret;

	print_header("signalfd implementation");

	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);
	ret = sigprocmask(SIG_BLOCK, &mask, &old_mask);
	assert_eq(ret, 0);

	test_signalfd_api();
	test_signalfd_epoll();

	ret = sigprocmask(SIG_SETMASK, &old_mask, NULL);
	assert_eq(ret, 0);

	printf("- OK!\n");
}
