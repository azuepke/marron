# SPDX-License-Identifier: MIT
# Copyright 2021 Alexander Zuepke

MARRON_DEMO_NAME="Playground testsuite"
MARRON_APPS="app1 app2"
MARRON_LIBS="libsys libc libioserver libcmini libutil"
MARRON_KLDDS="console pmu cpuinfo"
