#!/bin/bash
# SPDX-License-Identifier: MIT
# Copyright 2021, 2023 Alexander Zuepke
#
# azuepke, 2021-01-25: initial
# azuepke, 2023-07-31: use new build script

echo "Building for all targets ..."

function usage {
	echo "error: must specify an operation"
	echo "  $0 all      -- build all targets"
	echo "  $0 list     -- list all targets"
	echo "  $0 clean    -- clean all targets"
}

if [ -z "$1" ]; then
	usage
	exit 1
fi

# bail out on error
set -e
# warn when using uninitialised variables
set -u

if [ "$1" = "clean" ]; then
	rm tmp-buildall-*.log
	exit 0
fi

TARGETS="am335x \
	am57xx \
	am62x \
	bcm2711 \
	bcm2711-64bit \
	bcm2712 \
	bcm2836 \
	bcm2837 \
	bcm2837-64bit \
	fvp \
	imx6 \
	imx8m \
	imx8mp \
	lx2160a \
	polarfire \
	qemu-aarch64 \
	qemu-arm \
	qemu-riscv32 \
	qemu-riscv64 \
	rk3588 \
	s32g274 \
	s32v234 \
	tegra186 \
	tegra194 \
	tegra234 \
	zcu102 \
	zynq"

if [ "$1" = "list" ]; then
	echo $TARGETS
	exit 0
fi

if [ "$1" != "all" ]; then
	usage
	exit 1
fi

# warn when using uninitialised variables
set -u
# stop on first error
set -e

# build(target, debug)
function build {
	echo -e "\n-----------------------------------------------------------\n"
	echo "Building $1 DEBUG=$2"
	if [ -f tmp-buildall-$1-$2.log ]; then
		echo "-- exists, skipped"
		return
	fi
	bash -c "source BUILDENV.sh -b $1 ; make DEBUG=$2 all && make DEBUG=$2 distclean"
	echo "-- done"
	touch tmp-buildall-$1-$2.log
}

# build all targets with DEBUG set to yes or no
for t in $TARGETS; do
	build $t yes
	build $t no
done
