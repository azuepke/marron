/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_fs_mknod.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_fs_mknod(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	// FIXME: IMPLEMENT ME
	(void)fs;
	(void)rhnd;

	return msg_err(msg, ENOSYS);
}
