/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * d_error.c
 *
 * IPC-based I/O server implementation, generic error handler.
 *
 * azuepke, 2025-02-09: initial
 */

#include <marron/ioserver.h>
#include <marron/ipc_io.h>
#include <marron/compiler.h>
#include <marron/error.h>
#include "internal.h"


static const uint8_t d_error_list[] = {
	ENOSYS,		/* IPC_IO_GETOFLAGS */
	ENOSYS,		/* IPC_IO_SETOFLAGS */
	ENOSYS,		/* IPC_IO_FS_OPEN */
	EBADF,		/* IPC_IO_STAT */
	EBADF,		/* IPC_IO_READ */
	EBADF,		/* IPC_IO_WRITE */
	ENOTTY,		/* IPC_IO_IOCTL */
	EACCES,		/* IPC_IO_MMAP */
	ENOSYS,		/* IPC_IO_MPROTECT */
	ENOSYS,		/* IPC_IO_MUNMAP */
	ESPIPE,		/* IPC_IO_SEEK */
	EINVAL,		/* IPC_IO_TRUNCATE */
	EINVAL,		/* IPC_IO_ALLOCATE */
	EINVAL,		/* IPC_IO_SYNC */
	EINVAL,		/* IPC_IO_ADVISE */
	EINVAL,		/* IPC_IO_LOCK */
	ENOSYS,		/* IPC_IO_STATVFS */
	ENOSYS,		/* IPC_IO_GETPATH */
	ENOTDIR,	/* IPC_IO_GETDENTS */
	ENOSYS,		/* IPC_IO_FS_ACCESS */
	ENOSYS,		/* IPC_IO_FS_STAT */
	ENOSYS,		/* IPC_IO_FS_RENAME */
	ENOSYS,		/* IPC_IO_FS_REMOVE */
	ENOSYS,		/* IPC_IO_FS_MKDIR */
	ENOSYS,		/* IPC_IO_FS_MKNOD */
	ENOSYS,		/* IPC_IO_FS_LINK */
	ENOSYS,		/* IPC_IO_FS_SYMLINK */
	ENOSYS,		/* IPC_IO_FS_READLINK */
	ENOSYS,		/* IPC_IO_FS_CHMOD */
	ENOSYS,		/* IPC_IO_FS_CHOWN */
	ENOSYS,		/* IPC_IO_FS_UTIME */
	ENOENT,		/* IPC_IO_CHMOD */
	ENOENT,		/* IPC_IO_CHOWN */
	ENOENT,		/* IPC_IO_UTIME */
	ENOTSOCK,	/* IPC_IO_SEND */
	ENOTSOCK,	/* IPC_IO_RECV */
	ENOTSOCK,	/* IPC_IO_ACCEPT */
	ENOTSOCK,	/* IPC_IO_CONNECT */
	ENOTSOCK,	/* IPC_IO_BIND */
	ENOTSOCK,	/* IPC_IO_GETPEERNAME */
	ENOTSOCK,	/* IPC_IO_GETSOCKNAME */
	ENOTSOCK,	/* IPC_IO_LISTEN */
	ENOTSOCK,	/* IPC_IO_SHUTDOWN */
	ENOTSOCK,	/* IPC_IO_GETSOCKOPT */
	ENOTSOCK,	/* IPC_IO_SETSOCKOPT */
};

int d_error(void *f_ignored, uint32_t rhnd, struct sys_ipc *msg)
{
	uint8_t req;

	(void)f_ignored;
	(void)rhnd;

	req = msg->req;
	if (req < countof(d_error_list)) {
		return msg_err(msg, d_error_list[req]);
	}

	return msg_err(msg, ENOSYS);
}
