/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * d_fs_open_pipe.c
 *
 * IPC-based I/O server implementation, open a pipe pair.
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services (former d_open_pipe.c)
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <fcntl.h>
#include "internal.h"


int d_fs_open_pipe(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	uint32_t oflags;
	err_t err;

	if (msg->h8 != IPC_IO_FS_OPEN_PIPE) {
		/* wrong protocol */
		return msg_err(msg, ENOSYS);
	}
	if ((msg->flags & (SYS_IPC_FD0 | SYS_IPC_FD1)) != (SYS_IPC_FD0 | SYS_IPC_FD1)) {
		/* must receive two file descriptors */
		return msg_err(msg, EINVAL);
	}

	oflags = msg->h32;
	if ((oflags & ~(O_NONBLOCK)) != 0) {
		/* invalid flags */
		return msg_err(msg, EINVAL);
	}

	fs->mountpointname[0] = '\0';

	err = i_pipe(fs, rhnd, oflags);
	return msg_err(msg, err);
}
