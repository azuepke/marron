/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_statvfs.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/arch_defs.h>
#include <fcntl.h>
#include "internal.h"


int d_statvfs(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	struct statvfs statbuf = { 0 };
	size_t size;
	err_t err;

	/* sane defaults for memory file systems */
	statbuf.f_bsize = PAGE_SIZE;
	statbuf.f_frsize = PAGE_SIZE;
	statbuf.f_namemax = 255;

	err = i_statvfs(f, &statbuf);

	if ((err == EOK) && (msg->size0 > 0)) {
		err = sys_ipc_write_exact(rhnd, 0, &statbuf, sizeof(statbuf), 0, &size);
		if (err != EOK) {
			return msg_err(msg, EFAULT);
		}
	}

	err = EOK;
	return msg_err(msg, err);
}
