/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_advise.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_advise(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	unsigned int advice;
	off_t offset;
	off_t len;
	err_t err;

	(void)rhnd;

	if ((f->oflags & O_DIRECTORY) != 0) {
		/* is a directory */
		return msg_err(msg, EISDIR);
	}
	if ((f->oflags & (__O_READ|__O_WRITE)) == 0) {
		/* file not opened for reading or writing */
		return msg_err(msg, EBADF);
	}
	if ((f->oflags & __O_SEEK) == 0) {
		/* not seekable */
		return msg_err(msg, ESPIPE);
	}

	advice = msg->h32;
	if (advice > POSIX_FADV_NOREUSE) {
		/* invalid advice */
		return msg_err(msg, EINVAL);
	}
	offset = msg->h64;
	if (offset < 0) {
		/* negative offset */
		return msg_err(msg, EINVAL);
	}
	len = msg->a64;
	if (len < 0) {
		/* negative length */
		return msg_err(msg, EINVAL);
	}
	if ((uint64_t)offset + len < (uint64_t)offset) {
		/* overflow */
		return msg_err(msg, EINVAL);
	}
	if (offset + len > f->length) {
		/* greater than file size */
		return msg_err(msg, EINVAL);
	}
	if (len == 0) {
		len = f->length - offset;
	}

	err = i_advise(f, offset, len, advice);

	return msg_err(msg, err);
}
