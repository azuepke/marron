/* SPDX-License-Identifier: MIT */
/* Copyright 2025 Alexander Zuepke */
/*
 * d_fs_access.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2025-02-09: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_fs_access(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	// FIXME: IMPLEMENT ME
	(void)fs;
	(void)rhnd;

	return msg_err(msg, ENOSYS);
}
