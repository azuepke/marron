/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * d_getsetoflags_fs.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services (from d_getoflags.c)
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/compiler.h>
#include "internal.h"


static int d_getsetoflags_fs(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	(void)fs;
	(void)rhnd;

	/* static file descriptor instances always have oflags set to zero */
	msg->h32 = 0;
	return msg_err(msg, EOK);
}

int d_getoflags_fs_default(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg) __alias(d_getsetoflags_fs);
int d_setoflags_fs_default(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg) __alias(d_getsetoflags_fs);
