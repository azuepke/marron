/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * i_munmap_default.c
 *
 * IPC-based I/O server implementation, default implemention callbacks.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/compiler.h>
#include "internal.h"


static err_t i_munmap_default(struct ioserver_file *f, off_t offset, size_t size)
{
	(void)f;
	(void)offset;
	(void)size;

	return EOK;
}

err_t i_munmap(struct ioserver_file *f, off_t offset, size_t size) __weakref(i_munmap_default);
