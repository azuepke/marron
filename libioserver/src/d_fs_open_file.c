/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * d_fs_open_file.c
 *
 * IPC-based I/O server implementation, open a file or directory or device.
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services (former d_open_file.c)
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <fcntl.h>
#include "internal.h"


int d_fs_open_file(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	char pathname[256];
	uint32_t oflags;
	size_t size;
	mode_t mode;
	err_t err;
	char *c;

	if (msg->h8 != IPC_IO_FS_OPEN_FILE) {
		/* wrong protocol */
		return msg_err(msg, ENOSYS);
	}
	if ((msg->flags & SYS_IPC_FD0) == 0) {
		/* must receive a file descriptor */
		return msg_err(msg, EINVAL);
	}

	/* retrieve local name of mount point (for IPC_IO_GETPATH) */
	err = sys_ipc_read(rhnd, 0, fs->mountpointname, sizeof(fs->mountpointname), 0, &size);
	if (err != EOK) {
		/* memory access error */
		return msg_err(msg, EFAULT);
	}
	if (size == sizeof(fs->mountpointname)) {
		/* name of mount point too long */
		return msg_err(msg, ENAMETOOLONG);
	}
	fs->mountpointname[size] = '\0';

	/* retrieve file name */
	err = sys_ipc_read(rhnd, 1, pathname, sizeof(pathname), 0, &size);
	if (err != EOK) {
		/* memory access error */
		return msg_err(msg, EFAULT);
	}
	if (size == sizeof(pathname)) {
		/* file name too long */
		return msg_err(msg, ENAMETOOLONG);
	}
	pathname[size] = '\0';
	if (size > 0) {
		c = pathname;
		if (*c != '/') {
			/* must start with "/" */
			return msg_err(msg, EINVAL);
		}

		/* check for non-normalized file names
		 *
		 * bad patterns:
		 * - "//"
		 * - "/./"
		 * - "/.\0"
		 * - "/../"
		 * - "/..\0"
		 */
		while (*c != '\0') {
			if (*c == '/') {
				c++;
				if (*c == '/') {
					return msg_err(msg, EINVAL);
				}
				if (*c == '.') {
					c++;
					if ((*c == '/') || (*c == '\0')) {
						return msg_err(msg, EINVAL);
					}
					if (*c == '.') {
						c++;
						if ((*c == '/') || (*c == '\0')) {
							return msg_err(msg, EINVAL);
						}
					}
				}
			} else {
				c++;
			}
		}

		/* remove trailing "/" and set O_DIRECTORY */
		if ((size > 1) && (pathname[size-1] == '/')) {
			size--;
			pathname[size] = '\0';
			msg->h32 |= O_DIRECTORY;
		}
	}

	oflags = msg->h32;
	if ((oflags & ~(O_ACCMODE | O_DIRECTORY |
	                O_CREAT | O_EXCL | O_TRUNC | O_APPEND |
	                O_NOCTTY | O_NOFOLLOW | O_NONBLOCK |
	                O_SYNC | O_ASYNC)) != 0) {
		/* invalid flags */
		return msg_err(msg, EINVAL);
	}
	switch (oflags & O_ACCMODE) {
	case O_RDONLY:
	case O_WRONLY:
	case O_RDWR:
	case O_PATH:
		break;
	default:
		/* must be O_RDONLY, O_WRONLY, O_RDWR or O_PATH */
		return msg_err(msg, EINVAL);
	}
	if ((oflags & (O_DIRECTORY | O_CREAT)) == (O_DIRECTORY | O_CREAT)) {
		/* invalid flags for a directory */
		return msg_err(msg, EISDIR);
	}
	if ((oflags & O_PATH) != 0) {
		oflags &= ~(O_CREAT | O_EXCL | O_TRUNC | O_APPEND);
	}

	/* invalid mode bits should be ignored */
	mode = msg->h64l & 07777;

	err = i_open(fs, rhnd, pathname, oflags, mode);
	return msg_err(msg, err);
}
