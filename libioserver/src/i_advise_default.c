/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * i_advise_default.c
 *
 * IPC-based I/O server implementation, default implemention callbacks.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/compiler.h>
#include "internal.h"


static err_t i_advise_default(struct ioserver_file *f, off_t offset, off_t length, unsigned int advice)
{
	(void)f;
	(void)offset;
	(void)length;
	(void)advice;

	return EOK;
}

err_t i_advise(struct ioserver_file *f, off_t offset, off_t length, unsigned int advice) __weakref(i_advise_default);
