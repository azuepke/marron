/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_write.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_write(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	off_t offset;
	size_t wsize;
	size_t size;
	err_t err;

	if ((f->oflags & O_DIRECTORY) != 0) {
		/* is a directory */
		return msg_err(msg, EISDIR);
	}
	if ((f->oflags & __O_WRITE) == 0) {
		/* file not opened for writing */
		return msg_err(msg, EBADF);
	}

	/* seek position */
	if (msg->h8 == SEEK_CUR) {
		/* read() */
		offset = f->offset;
	} else if (msg->h8 == SEEK_SET) {
		/* readp() */
		if ((f->oflags & __O_SEEK) == 0) {
			/* not seekable */
			return msg_err(msg, ESPIPE);
		}
		offset = msg->h64;
		if (offset < 0) {
			/* negative offset */
			return msg_err(msg, EINVAL);
		}
	} else {
		/* invalid whence */
		return msg_err(msg, EINVAL);
	}
	/* O_APPEND overrides any given positions */
	if ((f->oflags & O_APPEND) != 0) {
		offset = f->length;
	}

	/* cap request to ~2 GB size */
	size = msg->size0;
	if (size > 0x7ffff000) {
		size = 0x7ffff000;
	}

	wsize = 0;
	err = i_write(f, rhnd, size, offset, &wsize);
	if (err == ESTATE) {
		/* write shall block, do not modify message */
		return IOSERVER_WAIT;
	}

	/* update returned size and file pointer when necessary */
	if (err == EOK) {
		msg->size0 = wsize;
		if ((f->oflags & __O_SEEK) != 0) {
			if (msg->h8 == SEEK_CUR) {
				f->offset += msg->size0;
			}

			/* grow file size accordingly */
			if ((unsigned long long)f->length < (unsigned long long)offset + wsize) {
				f->length = offset + wsize;
			}
		}
	}

	return msg_err(msg, err);
}
