/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * dm_dispatch.c
 *
 * IPC-based I/O server implementation, main dispatcher for IPC file protocol.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/ipc_io.h>
#include <marron/compiler.h>
#include <stddef.h>
#include "internal.h"


static int dm_dispatch(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg, const struct d_callback *cb)
{
	int (*callback)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
	uint8_t req;

	req = msg->req;
	if (req < countof(cb->d_callback)) {
		callback = cb->d_callback[req];
		if (callback != NULL) {
			return callback(f, rhnd, msg);
		}
	}

	return d_error(f, rhnd, msg);
}

int dm_dispatch_file(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg, const struct d_callback *cb) __alias(dm_dispatch);
int dm_dispatch_fs(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg, const struct d_callback *cb) __alias(dm_dispatch);
