/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * dm_dispatch_open_dev.c
 *
 * IPC-based I/O server implementation, open a device node (simplified form).
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services (former d_open_dev.c)
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include "internal.h"


int dm_dispatch_open_dev(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	fs->oflags = 0;

	switch (msg->req) {
	case IPC_IO_GETOFLAGS:
	case IPC_IO_SETOFLAGS:
		/* static file descriptor instances always have oflags set to zero */
		msg->h32 = 0;
		return msg_err(msg, EOK);

	case IPC_IO_FS_OPEN:
		return d_fs_open_dev(fs, rhnd, msg);

	case IPC_IO_STAT:
		return d_stat_fs_default(fs, rhnd, msg);

	default:
		return msg_err(msg, ENOSYS);
	}
}
