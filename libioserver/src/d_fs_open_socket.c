/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * d_fs_open_socket.c
 *
 * IPC-based I/O server implementation, open a socket or socket pair.
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services (former d_open_socket.c)
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <fcntl.h>
#include "internal.h"


int d_fs_open_socket(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	uint32_t oflags;
	uint32_t domain;
	uint32_t type;
	uint32_t protocol;
	err_t err;

	if (msg->h8 == IPC_IO_FS_OPEN_SOCKET) {
		if ((msg->flags & SYS_IPC_FD0) == 0) {
			/* must receive a file descriptor */
			return msg_err(msg, EINVAL);
		}
	} else if (msg->h8 == IPC_IO_FS_OPEN_SOCKETPAIR) {
		if ((msg->flags & (SYS_IPC_FD0 | SYS_IPC_FD1)) != (SYS_IPC_FD0 | SYS_IPC_FD1)) {
			/* must receive two file descriptors */
			return msg_err(msg, EINVAL);
		}
	} else {
		/* wrong protocol */
		return msg_err(msg, ENOSYS);
	}

	oflags = msg->h32;
	if ((oflags & ~(O_NONBLOCK)) != 0) {
		/* invalid flags */
		return msg_err(msg, EINVAL);
	}

	fs->mountpointname[0] = '\0';

	domain = msg->h64l;
	type = msg->h64h;
	protocol = msg->a64l;

	if (msg->h8 == IPC_IO_FS_OPEN_SOCKET) {
		err = i_socket(fs, rhnd, domain, type, protocol, oflags);
	} else {
		err = i_socketpair(fs, rhnd, domain, type, protocol, oflags);
	}
	return msg_err(msg, err);
}
