/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_seek.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_seek(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	off_t offset;
	err_t err;

	(void)rhnd;

	if ((f->oflags & (__O_READ|__O_WRITE)) == 0) {
		/* file not opened for reading or writing */
		return msg_err(msg, EBADF);
	}
	if ((f->oflags & __O_SEEK) == 0) {
		/* not seekable */
		return msg_err(msg, ESPIPE);
	}

	switch (msg->h8) {
	case SEEK_SET:
		offset = msg->h64;
		break;
	case SEEK_CUR:
		offset = f->offset + msg->h64;
		break;
	case SEEK_END:
		offset = f->length + msg->h64;
		break;
	default:
		/* invalid whence */
		return msg_err(msg, EINVAL);
	}
	if (offset < 0) {
		/* negative offset */
		return msg_err(msg, EOVERFLOW);
	}

	err = i_seek(f, offset);

	/* provide new offset in any case */
	msg->h64 = f->offset;

	return msg_err(msg, err);
}
