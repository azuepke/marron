/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * i_seek_default.c
 *
 * IPC-based I/O server implementation, default implemention callbacks.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/compiler.h>
#include "internal.h"


static err_t i_seek_default(struct ioserver_file *f, off_t offset)
{
	f->offset = offset;
	return EOK;
}

err_t i_seek(struct ioserver_file *f, off_t offset) __weakref(i_seek_default);
