/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * i_stat_default.c
 *
 * IPC-based I/O server implementation, default implemention callbacks.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/compiler.h>
#include "internal.h"


static err_t i_stat_default(struct ioserver_file *f, struct stat *statbuf)
{
	(void)f;
	(void)statbuf;

	/* the default setting of the stat buffer is fine for a character device */

	return EOK;
}

err_t i_stat(struct ioserver_file *f, struct stat *statbuf) __weakref(i_stat_default);
