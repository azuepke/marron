/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_lock.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_lock(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	unsigned int operation;
	off_t offset;
	off_t len;
	err_t err;

	(void)rhnd;

	if ((f->oflags & O_DIRECTORY) != 0) {
		/* is a directory */
		return msg_err(msg, EISDIR);
	}
	if ((f->oflags & (__O_READ|__O_WRITE)) == 0) {
		/* file not opened for reading or writing */
		return msg_err(msg, EBADF);
	}
	if ((f->oflags & __O_SEEK) == 0) {
		/* not seekable */
		return msg_err(msg, ESPIPE);
	}

	operation = msg->h32;
	switch (operation) {
	case 0:	// rdlock     (F_RDLCK)
	case 1:	// wrlock     (F_RDLCK)
	case 2:	// unlock     (F_UNLCK)
	case 4:	// rdtrylock  (F_RDLCK | __F_TRYLCK)
	case 5:	// wrtrylock  (F_WRLCK | __F_TRYLCK)
	case 8:	// rdtestlock (F_RDLCK | __F_TSTLCK)
	case 9:	// wrtestlock (F_WRLCK | __F_TSTLCK)
		break;
	default:
		/* invalid operation */
		return msg_err(msg, EINVAL);
	}

	switch (msg->h8) {
	case SEEK_SET:
		offset = msg->h64;
		break;
	case SEEK_CUR:
		offset = f->offset + msg->h64;
		break;
	case SEEK_END:
		offset = f->length + msg->h64;
		break;
	default:
		/* invalid whence */
		return msg_err(msg, EINVAL);
	}
	if (offset < 0) {
		/* negative offset */
		return msg_err(msg, EINVAL);
	}

	len = msg->a64;
	if (len < 0) {
		/* special case: negative length */
		offset -= len;
		len = -len;
		if (offset < 0) {
			len -= 0 - offset;
			offset = 0;
		}
		if (len < 0) {
			/* underflow */
			return msg_err(msg, EINVAL);
		}
	}
	if ((uint64_t)offset + len < (uint64_t)offset) {
		/* overflow */
		return msg_err(msg, EINVAL);
	}
	if (len == 0) {
		len = 0x7fffffffffffffff - offset;
	}

	err = i_lock(f, offset, len, operation);

	if (err == ESTATE) {
		/* lock shall block, do not modify message */
		return IOSERVER_WAIT;
	}

	return msg_err(msg, err);
}
