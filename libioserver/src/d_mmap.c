/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_mmap.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/arch_defs.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "internal.h"


int d_mmap(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	unsigned int flags;
	unsigned int prot;
	off_t offset;
	size_t size;
	err_t err;

	if ((msg->flags & SYS_IPC_MAP) == 0) {
		/* not a mapping IPC */
		return msg_err(msg, EINVAL);
	}
	/* the kernel already validated for us:
	 * - msg->h8: prot
	 * - msg->h32: flags
	 * - msg->buf0: address
	 * - msg->size0: length
	 */

	if ((f->oflags & O_DIRECTORY) != 0) {
		/* is a directory (POSIX requires ENODEV here) */
		return msg_err(msg, ENODEV);
	}
	if ((f->oflags & __O_READ) == 0) {
		/* file not opened for reading (POSIX requires read access for mmap()) */
		return msg_err(msg, EACCES);
	}
	if ((f->oflags & __O_SEEK) == 0) {
		/* not seekable */
		return msg_err(msg, ESPIPE);
	}

	size = msg->map_size;
	offset = msg->h64;
	if ((offset < 0) || ((offset & (PAGE_SIZE - 1)) != 0)) {
		/* negative or unaligned offset */
		return msg_err(msg, EINVAL);
	}
	if ((uint64_t)offset + size < (uint64_t)offset) {
		/* address space overflow */
		return msg_err(msg, EINVAL);
	}
	prot = msg->map_prot;
	if (((prot & PROT_WRITE) != 0) && ((f->oflags & __O_WRITE) == 0)) {
		/* file not open for writing */
		return msg_err(msg, EACCES);
	}
	flags = msg->map_flags;
	if ((prot & PROT_READ) != 0) {
		flags |= MAP_ALLOW_READ;
	}
	if ((prot & PROT_WRITE) != 0) {
		flags |= MAP_ALLOW_WRITE;
	}
	if ((prot & PROT_EXEC) != 0) {
		flags |= MAP_ALLOW_EXEC;
	}

	err = i_mmap(f, rhnd, offset, size, prot, flags);

	return msg_err(msg, err);
}
