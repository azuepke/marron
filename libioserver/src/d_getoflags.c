/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * d_getoflags.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/compiler.h>
#include "internal.h"


int d_getoflags(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	(void)rhnd;

	msg->h32 = f->oflags;

	return msg_err(msg, EOK);
}

int d_getoflags_fs(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg) __alias(d_getoflags);
