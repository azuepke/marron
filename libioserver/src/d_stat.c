/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_stat.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/arch_defs.h>
#include "internal.h"


int d_stat(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	struct stat statbuf = { 0 };
	size_t size;
	err_t err;

	/* default settings for stat for a character device */
	statbuf.st_mode = S_IRUSR | S_IWUSR;
	statbuf.st_nlink = 1;
	statbuf.st_blksize = PAGE_SIZE;

	err = i_stat(f, &statbuf);

	if ((err == EOK) && (msg->size0 > 0)) {
		err = sys_ipc_write_exact(rhnd, 0, &statbuf, sizeof(statbuf), 0, &size);
		if (err != EOK) {
			return msg_err(msg, EFAULT);
		}
	}

	return msg_err(msg, err);
}
