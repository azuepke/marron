/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * i_statvfs_default.c
 *
 * IPC-based I/O server implementation, default implemention callbacks.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/compiler.h>
#include "internal.h"


static err_t i_statvfs_default(struct ioserver_file *f, struct statvfs *statbuf)
{
	(void)f;
	(void)statbuf;

	/* the default setting of the stat buffer is fine for an in-memory file system */

	return EOK;
}

err_t i_statvfs(struct ioserver_file *f, struct statvfs *statbuf) __weakref(i_statvfs_default);
