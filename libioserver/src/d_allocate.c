/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_allocate.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_allocate(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	off_t offset;
	off_t len;
	err_t err;

	(void)rhnd;

	if ((f->oflags & O_DIRECTORY) != 0) {
		/* is a directory */
		return msg_err(msg, EISDIR);
	}
	if ((f->oflags & __O_WRITE) == 0) {
		/* file not opened for writing */
		return msg_err(msg, EBADF);
	}
	if ((f->oflags & __O_SEEK) == 0) {
		/* not seekable */
		return msg_err(msg, ESPIPE);
	}

	offset = msg->h64;
	if (offset < 0) {
		/* negative offset */
		return msg_err(msg, EINVAL);
	}
	len = msg->a64;
	if (len <= 0) {
		/* negative or zero length */
		return msg_err(msg, EINVAL);
	}
	if ((uint64_t)offset + len < (uint64_t)offset) {
		/* overflow */
		return msg_err(msg, EINVAL);
	}

	len = offset + len;
	if (len > f->length) {
		err = i_truncate(f, len);
	} else {
		err = EOK;
	}

	if (err == EOK) {
		err = i_allocate(f, offset, len);
	}

	return msg_err(msg, err);
}
