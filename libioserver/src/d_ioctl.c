/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_ioctl.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_ioctl(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	uint32_t rval;
	err_t err;

	if ((f->oflags & O_DIRECTORY) != 0) {
		/* is a directory (NOTE: Linux and BSD use ENOTTY here) */
		return msg_err(msg, EISDIR);
	}
	if ((f->oflags & (__O_READ|__O_WRITE)) == 0) {
		/* file not opened for reading or writing (NOTE: Linux and BSD use ENOTTY here) */
		return msg_err(msg, EBADF);
	}

	err = i_ioctl(f, rhnd, msg->h32, msg->arg0, &rval);
	if (err == ESTATE) {
		/* ioctl shall block, do not modify message */
		return IOSERVER_WAIT;
	}

	if (err == EOK) {
		msg->h32 = rval;
	}

	return msg_err(msg, err);
}
