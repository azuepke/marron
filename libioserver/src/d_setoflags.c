/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * d_setoflags.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/compiler.h>
#include <fcntl.h>
#include "internal.h"


int d_setoflags(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	uint32_t oflags;
	err_t err;

	(void)rhnd;

	oflags = msg->h32 & (O_APPEND | O_NONBLOCK | O_SYNC | O_ASYNC);
	err = i_setoflags(f, oflags);

	msg->h32 = f->oflags;

	return msg_err(msg, err);
}

int d_setoflags_fs(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg) __alias(d_setoflags);
