/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * d_fs_open_dev.c
 *
 * IPC-based I/O server implementation, open a device node (simplified form).
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services (former d_open_dev.c)
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/ipc_io.h>
#include <fcntl.h>
#include "internal.h"


int d_fs_open_dev(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg)
{
	uint32_t oflags;
	size_t size;
	err_t err;

	if (msg->h8 != IPC_IO_FS_OPEN_FILE) {
		/* wrong protocol */
		return msg_err(msg, ENOSYS);
	}
	if ((msg->flags & SYS_IPC_FD0) == 0) {
		/* must receive a file descriptor */
		return msg_err(msg, EINVAL);
	}

	/* retrieve local name of mount point (for IPC_IO_GETPATH) */
	err = sys_ipc_read(rhnd, 1, fs->mountpointname, sizeof(fs->mountpointname), 0, &size);
	if (err != EOK) {
		/* memory access error */
		return msg_err(msg, EFAULT);
	}
	if (size == sizeof(fs->mountpointname)) {
		/* name of mount point too long */
		return msg_err(msg, ENAMETOOLONG);
	}
	fs->mountpointname[size] = '\0';

	/* retrieve file name -- must be empty */
	if (msg->size1 > 0) {
		/* not a file system */
		return msg_err(msg, ENOENT);
	}

	oflags = msg->h32;
	if ((oflags & ~(O_ACCMODE | O_DIRECTORY |
	                O_CREAT | O_EXCL | O_TRUNC | O_APPEND |
	                O_NOCTTY | O_NOFOLLOW | O_NONBLOCK |
	                O_SYNC | O_ASYNC)) != 0) {
		/* invalid flags */
		return msg_err(msg, EINVAL);
	}
	switch (oflags & O_ACCMODE) {
	case O_RDONLY:
	case O_WRONLY:
	case O_RDWR:
	case O_PATH:
		break;
	default:
		/* must be O_RDONLY, O_WRONLY, O_RDWR or O_PATH */
		return msg_err(msg, EINVAL);
	}
	if ((oflags & (O_DIRECTORY | O_CREAT)) != 0) {
		/* invalid flags for a device */
		return msg_err(msg, ENOTDIR);
	}
	if ((oflags & O_PATH) != 0) {
		oflags &= ~(O_CREAT | O_EXCL | O_TRUNC | O_APPEND);
	}

	err = i_open(fs, rhnd, "", oflags, 0);
	return msg_err(msg, err);
}
