/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * i_setoflags_default.c
 *
 * IPC-based I/O server implementation, default implemention callbacks.
 *
 * azuepke, 2022-09-02: initial
 * azuepke, 2025-02-09: file system services
 */

#include <marron/ioserver.h>
#include <marron/compiler.h>
#include <fcntl.h>
#include "internal.h"


static err_t i_setoflags_default(struct ioserver_file *f, uint32_t oflags)
{
	f->oflags &= ~(O_APPEND | O_NONBLOCK | O_SYNC | O_ASYNC);
	f->oflags |= oflags;
	return EOK;
}

err_t i_setoflags(struct ioserver_file *f, uint32_t oflags) __weakref(i_setoflags_default);
