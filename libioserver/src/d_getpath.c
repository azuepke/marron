/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_getpath.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_getpath(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	size_t size;
	err_t err;

	if ((f->oflags & O_DIRECTORY) == 0) {
		/* not a directory */
		return msg_err(msg, ENOTDIR);
	}

	size = 0;
	err = i_getpath(f, rhnd, msg->size0, &size);
	if (err == EOK) {
		msg->size0 = size;
	}
	return msg_err(msg, err);
}
