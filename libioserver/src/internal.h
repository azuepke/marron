/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * internal.h
 *
 * IPC-based I/O server interface and implementation.
 *
 * azuepke, 2022-08-20: initial
 */

#ifndef INTERNAL_H_
#define INTERNAL_H_

#include <marron/ioserver.h>


static inline int msg_err(struct sys_ipc *msg, err_t err)
{
	msg->err = err;
	return IOSERVER_OK;
}

#endif
