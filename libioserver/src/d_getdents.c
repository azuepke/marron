/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_getdents.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <marron/macros.h>
#include <fcntl.h>
#include "internal.h"
#include <stddef.h>
#include <stdalign.h>


int d_getdents(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	struct dirent dirent = { 0 };
	size_t reclen;
	size_t wsize;
	size_t size;
	size_t pos;
	err_t err;

	if ((f->oflags & O_DIRECTORY) == 0) {
		/* not a directory */
		return msg_err(msg, ENOTDIR);
	}
	if ((f->oflags & __O_READ) == 0) {
		/* directory not opened for reading */
		return msg_err(msg, EBADF);
	}
	if ((f->oflags & __O_SEEK) == 0) {
		/* not seekable */
		return msg_err(msg, ESPIPE);
	}

	size = msg->size0;
	if (size < sizeof dirent) {
		/* buffer too small */
		return msg_err(msg, EINVAL);
	}

	pos = 0;
	while (size >= sizeof dirent) {
		/* the callback fills in all values except "d_reclen" */
		err = i_getdent(f, &dirent, f->offset);
		if (err == ENOENT) {
			/* reached end */
			break;
		} else if (err != EOK) {
			/* I/O error */
			return msg_err(msg, EIO);
		}

		reclen = offsetof(struct dirent, d_name) + dirent.d_namlen + 1;
		reclen += alignof(struct dirent) - 1;
		reclen = ALIGN_UP(reclen, alignof(struct dirent));
		dirent.d_reclen = reclen;

		err = sys_ipc_write_exact(rhnd, 0, &dirent, sizeof(dirent), pos, &wsize);
		if (err != EOK) {
			return msg_err(msg, EFAULT);
		}

		/* update offset for next iteration */
		f->offset = dirent.d_off;

		pos += reclen;
		size -= reclen;
	}

	/* return the actually delivered size */
	msg->size0 = pos;

	err = EOK;
	return msg_err(msg, err);
}
