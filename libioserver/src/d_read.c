/* SPDX-License-Identifier: MIT */
/* Copyright 2022 Alexander Zuepke */
/*
 * d_read.c
 *
 * IPC-based I/O server implementation, IPC file protocol dispatchers.
 *
 * azuepke, 2022-08-20: initial
 */

#include <marron/ioserver.h>
#include <marron/api.h>
#include <fcntl.h>
#include "internal.h"


int d_read(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg)
{
	off_t offset;
	size_t rsize;
	size_t size;
	err_t err;

	if ((f->oflags & O_DIRECTORY) != 0) {
		/* is a directory */
		return msg_err(msg, EISDIR);
	}
	if ((f->oflags & __O_READ) == 0) {
		/* file not opened for reading */
		return msg_err(msg, EBADF);
	}

	/* seek position */
	if (msg->h8 == SEEK_CUR) {
		/* read() */
		offset = f->offset;
	} else if (msg->h8 == SEEK_SET) {
		/* readp() */
		if ((f->oflags & __O_SEEK) == 0) {
			/* not seekable */
			return msg_err(msg, ESPIPE);
		}
		offset = msg->h64;
		if (offset < 0) {
			/* negative offset */
			return msg_err(msg, EINVAL);
		}
	} else {
		/* invalid whence */
		return msg_err(msg, EINVAL);
	}

	/* cap request to ~2 GB size */
	size = msg->size0;
	if (size > 0x7ffff000) {
		size = 0x7ffff000;
	}

	rsize = 0;
	err = i_read(f, rhnd, size, offset, &rsize);
	if (err == ESTATE) {
		/* read shall block, do not modify message */
		return IOSERVER_WAIT;
	}

	/* update returned size and file pointer when necessary */
	if (err == EOK) {
		msg->size0 = rsize;
		if ((f->oflags & __O_SEEK) != 0) {
			if (msg->h8 == SEEK_CUR) {
				f->offset += msg->size0;
			}
		}
	}

	return msg_err(msg, err);
}
