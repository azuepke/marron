/* SPDX-License-Identifier: MIT */
/* Copyright 2022, 2025 Alexander Zuepke */
/*
 * marron/ioserver.h
 *
 * IPC-based I/O server interface and implementation.
 *
 * azuepke, 2022-08-20: initial
 * azuepke, 2025-02-09: file system services
 */

#ifndef MARRON_IOSERVER_H_
#define MARRON_IOSERVER_H_

#include <marron/error.h>
#include <marron/types.h>
#include <marron/ipc_io.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <dirent.h>


/** dispatcher return codes */
#define IOSERVER_OK		0
#define IOSERVER_WAIT	1


/** I/O server file system protocol context (file-system meta data)
 *
 * This data structure comprises the state (meta data) of each instance
 * of the IPC file system protocol, e.g. one per statically assigned
 * file descriptor.
 * We need this information to track the mount point name on the client side.
 * Filled in by the first-level dispatcher functions for the open protocol.
 *
 * Initialization:
 * - oflags must be set to zero (inaccessible file)
 * - fd should be set to the server-side IPC file descriptor
 *
 * \note You can embed this structure as first element in another structures
 * and pass it to the dispatcher functions.
 */
struct ioserver_fs {
	/** open flags from fcntl.h */
	uint32_t oflags;

	/** associated server-side IPC file descriptor */
	uint32_t fd;

	/* --- end of common header --- */

	/** name of mount point on client side */
	char mountpointname[256];
};

/** I/O server file protocol context (file meta data)
 *
 * This data structure comprises the state of each opened file descriptor.
 * We track the most common VFS meta-data information such as open flags,
 * current file offset and length here. This allows us to implement
 * most of the typical VFS functionality and consistency checks
 * in generic dispatcher layer.
 *
 * Initialization:
 * - oflags must be initialized from oflags passed to the d_fs_open() callback
 *   and with further device-specific settings
 * - fd should be set to the server-side IPC file descriptor
 * - offset must be set to zero
 * - length must be set to the file length, if used
 *
 * \note You can embed this structure as first element in another structures
 * and pass it to the dispatcher functions.
 */
struct ioserver_file {
	/** open flags from fcntl.h */
	uint32_t oflags;

	/** associated server-side IPC file descriptor */
	uint32_t fd;

	/* --- end of common header --- */

	/** current file offset (if used) */
	off_t offset;

	/** file length (if used) */
	off_t length;

	/** pointer to I/O server file system context */
	struct ioserver_fs *ioserver;
};

/** I/O server file protocol dispatcher callback structure
 *
 * This structure contains all callbacks to dispatch the IPC file protocol.
 */
struct d_callback {
	union {
		int (*d_callback[IPC_IO_NUM])(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
		struct {
			union {
				int (*d_getoflags)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
				int (*d_getoflags_fs)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			};
			union {
				int (*d_setoflags)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
				int (*d_setoflags_fs)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			};
			int (*d_fs_open)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			union {
				int (*d_stat)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
				int (*d_stat_fs)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			};
			int (*d_read)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_write)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_ioctl)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_mmap)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_mprotect)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_munmap)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_seek)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_truncate)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_allocate)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_sync)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_advise)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_lock)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_statvfs)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_getpath)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_getdents)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_access)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_stat)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_rename)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_remove)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_mkdir)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_mknod)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_link)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_symlink)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_readlink)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_chmod)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_chown)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_fs_utime)(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_chmod)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_chown)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_utime)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_send)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_recv)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_accept)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_connect)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_bind)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_getpeername)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_getsockname)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_listen)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_shutdown)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_getsockopt)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
			int (*d_setsockopt)(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
		};
	};
};


#if 0

/*
 * Fully populated dispatcher callback structures
 *
 * To be copied to and modified by drivers.
 */

/* all operations, not usable */
static const struct d_callback d_callback_all = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_fs_open = d_fs_open_file,	/* i_open() */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_ioctl = d_ioctl,				/* i_ioctl() */
	.d_mmap = d_mmap,				/* i_mmap() */
	.d_mprotect = d_mprotect,		/* i_mprotect() */
	.d_munmap = d_munmap,			/* i_munmap() optional */
	.d_seek = d_seek,				/* i_seek() optional */
	.d_truncate = d_truncate,		/* i_truncate() */
	.d_allocate = d_allocate,		/* i_allocate() optional */
	.d_sync = d_sync,				/* i_sync() optional */
	.d_advise = d_advise,			/* i_advise() optional */
	.d_lock = d_lock,				/* i_lock() */
	.d_statvfs = d_statvfs,			/* i_statvfs() optional */
	.d_getpath = d_getpath,			/* i_getpath() */
	.d_getdents = d_getdents,		/* i_getdent() */
	.d_fs_access = d_fs_access,
	.d_fs_stat = d_fs_stat,
	.d_fs_rename = d_fs_rename,
	.d_fs_remove = d_fs_remove,
	.d_fs_mkdir = d_fs_mkdir,
	.d_fs_mknod = d_fs_mknod,
	.d_fs_link = d_fs_link,
	.d_fs_symlink = d_fs_symlink,
	.d_fs_readlink = d_fs_readlink,
	.d_fs_chmod = d_fs_chmod,
	.d_fs_chown = d_fs_chown,
	.d_fs_utime = d_fs_utime,
	.d_chmod = d_chmod,
	.d_chown = d_chown,
	.d_utime = d_utime,
	.d_send = d_send,
	.d_recv = d_recv,
	.d_accept = d_accept,
	.d_connect = d_connect,
	.d_bind = d_bind,
	.d_getpeername = d_getpeername,
	.d_getsockname = d_getsockname,
	.d_listen = d_listen,
	.d_shutdown = d_shutdown,
	.d_getsockopt = d_getsockopt,
	.d_setsockopt = d_setsockopt,
}};

/* use dm_dispatch_open_pipe() for file system operations */
static const struct d_callback d_callback_pipe = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_ioctl = d_ioctl,				/* i_ioctl() */
}};

/* use dm_dispatch_open_dev() for file system operations */
static const struct d_callback d_callback_mem = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_ioctl = d_ioctl,				/* i_ioctl() */
	.d_mmap = d_mmap,				/* i_mmap() */
	.d_mprotect = d_mprotect,		/* i_mprotect() */
	.d_munmap = d_munmap,			/* i_munmap() optional */
	.d_seek = d_seek,				/* i_seek() optional */
}};

/* use dm_dispatch_open_dev() for file system operations */
static const struct d_callback d_callback_dev = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_ioctl = d_ioctl,				/* i_ioctl() */
	.d_mmap = d_mmap,				/* i_mmap() */
	.d_mprotect = d_mprotect,		/* i_mprotect() */
	.d_munmap = d_munmap,			/* i_munmap() optional */
	.d_seek = d_seek,				/* i_seek() optional */
	.d_truncate = d_truncate,		/* i_truncate() */
	.d_allocate = d_allocate,		/* i_allocate() optional */
	.d_sync = d_sync,				/* i_sync() optional */
	.d_advise = d_advise,			/* i_advise() optional */
	.d_lock = d_lock,				/* i_lock() */
	.d_statvfs = d_statvfs,			/* i_statvfs() optional */
}};

/* use dm_dispatch_open_socket() for file system operations */
static const struct d_callback d_callback_socket = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_ioctl = d_ioctl,				/* i_ioctl() */
	.d_send = d_send,
	.d_recv = d_recv,
	.d_accept = d_accept,
	.d_connect = d_connect,
	.d_bind = d_bind,
	.d_getpeername = d_getpeername,
	.d_getsockname = d_getsockname,
	.d_listen = d_listen,
	.d_shutdown = d_shutdown,
	.d_getsockopt = d_getsockopt,
	.d_setsockopt = d_setsockopt,
}};

/* use dm_dispatch_open_file() for file system operations OR d_callback_sfs_fs */
static const struct d_callback d_callback_sfs_file = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_ioctl = d_ioctl,				/* i_ioctl() */
	.d_mmap = d_mmap,				/* i_mmap() */
	.d_mprotect = d_mprotect,		/* i_mprotect() */
	.d_munmap = d_munmap,			/* i_munmap() optional */
	.d_seek = d_seek,				/* i_seek() optional */
	.d_truncate = d_truncate,		/* i_truncate() */
	.d_allocate = d_allocate,		/* i_allocate() optional */
	.d_sync = d_sync,				/* i_sync() optional */
	.d_advise = d_advise,			/* i_advise() optional */
	.d_lock = d_lock,				/* i_lock() */
	.d_statvfs = d_statvfs,			/* i_statvfs() optional */
	.d_getpath = d_getpath,			/* i_getpath() */
	.d_getdents = d_getdents,		/* i_getdent() */
}};

/* use with d_callback_sfs_file for file operations */
static const struct d_callback d_callback_sfs_fs = {{
	.d_getoflags_fs = d_getoflags_fs_default,	/* required callback, no i_getoflags_fs() */
	.d_setoflags_fs = d_setoflags_fs_default,	/* required callback, no i_setoflags_fs() */
	.d_stat_fs = d_stat_fs_default,				/* required callback, no i_stat_fs() */
	.d_fs_open = d_fs_open_file,	/* i_open() */
	.d_fs_access = d_fs_access,
	.d_fs_stat = d_fs_stat,
}};

/* use with d_callback_ffs_fs for file system operations */
static const struct d_callback d_callback_ffs_file = {{
	.d_getoflags = d_getoflags,		/* required callback, no i_getoflags() */
	.d_setoflags = d_setoflags,		/* required callback, i_setoflags() optional */
	.d_stat = d_stat,				/* required callback, i_stat() optional */
	.d_read = d_read,				/* i_read() */
	.d_write = d_write,				/* i_write() */
	.d_ioctl = d_ioctl,				/* i_ioctl() */
	.d_mmap = d_mmap,				/* i_mmap() */
	.d_mprotect = d_mprotect,		/* i_mprotect() */
	.d_munmap = d_munmap,			/* i_munmap() optional */
	.d_seek = d_seek,				/* i_seek() optional */
	.d_truncate = d_truncate,		/* i_truncate() */
	.d_allocate = d_allocate,		/* i_allocate() optional */
	.d_sync = d_sync,				/* i_sync() optional */
	.d_advise = d_advise,			/* i_advise() optional */
	.d_lock = d_lock,				/* i_lock() */
	.d_statvfs = d_statvfs,			/* i_statvfs() optional */
	.d_getpath = d_getpath,			/* i_getpath() */
	.d_getdents = d_getdents,		/* i_getdent() */
	.d_chmod = d_chmod,
	.d_chown = d_chown,
	.d_utime = d_utime,
}};

/* use with d_callback_ffs_file for file operations */
static const struct d_callback d_callback_ffs_fs = {{
	.d_getoflags_fs = d_getoflags_fs_default,	/* required callback, no i_getoflags_fs() */
	.d_setoflags_fs = d_setoflags_fs_default,	/* required callback, no i_setoflags_fs() */
	.d_stat_fs = d_stat_fs_default,				/* required callback, no i_stat_fs() */
	.d_fs_open = d_fs_open_file,	/* i_open() */
	.d_fs_access = d_fs_access,
	.d_fs_stat = d_fs_stat,
	.d_fs_rename = d_fs_rename,
	.d_fs_remove = d_fs_remove,
	.d_fs_mkdir = d_fs_mkdir,
	.d_fs_mknod = d_fs_mknod,
	.d_fs_link = d_fs_link,
	.d_fs_symlink = d_fs_symlink,
	.d_fs_readlink = d_fs_readlink,
	.d_fs_chmod = d_fs_chmod,
	.d_fs_chown = d_fs_chown,
	.d_fs_utime = d_fs_utime,
}};

#endif

////////////////////////////////////////////////////////////////////////////////

/** generic error handler/dispatcher
 *
 * This dispatcher return an appropiate error code, depending on the request.
 *
 * Handles:
 * - IPC_IO_*
 *
 * Calls:
 * - none
 *
 * Returns IOSERVER_OK
 */
int d_error(void *f_ignored, uint32_t rhnd, struct sys_ipc *msg);

////////////////////////////////////////////////////////////////////////////////

/* dispatch messages for the IPC open protocol
 *
 * These dispatchers dispatch the whole message, no callback tables needed.
 *
 * Handles:
 * - IPC_IO_GETOFLAGS
 * - IPC_IO_SETOFLAGS
 * - IPC_IO_FS_OPEN
 * - IPC_IO_STAT
 * - rest: return EBADF
 */

/** dispatch open file requests (complex version for file systems)
 *
 * The dispatcher function handles complex file systems.
 * The dispatcher expects a mount point name and a valid normalized path name
 * that starts with a "/" character, or an empty path name.
 * If the path name ends in "/", the trailing directory separator is removed
 * and O_DIRECTORY is set in oflags.
 *
 * Handles:
 * - IPC_IO_GETOFLAGS (implemented internally)
 * - IPC_IO_SETOFLAGS (implemented internally)
 * - IPC_IO_FS_OPEN + IPC_IO_FS_OPEN_FILE
 * - IPC_IO_STAT
 *
 * Calls:
 * - d_fs_open_file() -> i_open()
 * - d_stat_fs_default()
 *
 * Returns IOSERVER_OK
 */
int dm_dispatch_open_file(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_open_file(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);

/** dispatch open file requests (simplified version for device drivers)
 *
 * The dispatcher function expects an empty path name
 * (only the mount point name is provided),
 * and no directory flags.
 *
 * Handles:
 * - IPC_IO_GETOFLAGS (implemented internally)
 * - IPC_IO_SETOFLAGS (implemented internally)
 * - IPC_IO_FS_OPEN + IPC_IO_FS_OPEN_FILE
 * - IPC_IO_STAT
 *
 * Calls:
 * - dm_dispatch_open_dev() -> i_open()
 * - d_stat_fs_default()
 *
 * Returns IOSERVER_OK
 */
int dm_dispatch_open_dev(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_open_dev(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);

/** dispatch open pipe requests
 *
 * This dispatcher handles open requests to pipes.
 * Pipes comprise two file descriptors
 *
 * Handles:
 * - IPC_IO_GETOFLAGS (implemented internally)
 * - IPC_IO_SETOFLAGS (implemented internally)
 * - IPC_IO_FS_OPEN + IPC_IO_FS_OPEN_SOCKET
 * - IPC_IO_STAT
 *
 * Calls:
 * - dm_dispatch_open_pipe() -> i_pipe()
 * - d_stat_fs_default()
 *
 * Returns IOSERVER_OK
 */
int dm_dispatch_open_pipe(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_open_pipe(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);

/** dispatch open socket requests
 *
 * This dispatcher handles open requests to sockets or socket pairs.
 *
 * Handles:
 * - IPC_IO_GETOFLAGS (implemented internally)
 * - IPC_IO_SETOFLAGS (implemented internally)
 * - IPC_IO_FS_OPEN + IPC_IO_FS_OPEN_SOCKETPAIR
 * - IPC_IO_STAT
 *
 * Calls:
 * - dm_dispatch_open_socket() -> i_socket()
 * - dm_dispatch_open_socket() -> i_socketpair()
 * - d_stat_fs_default()
 *
 * Returns IOSERVER_OK
 */
int dm_dispatch_open_socket(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_open_socket(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);

/** default dispatchers for required calls on file system file descriptors */
int d_getoflags_fs_default(struct ioserver_fs *fs_ignored, uint32_t rhnd, struct sys_ipc *msg);
int d_setoflags_fs_default(struct ioserver_fs *fs_ignored, uint32_t rhnd, struct sys_ipc *msg);
int d_stat_fs_default(struct ioserver_fs *fs_ignored, uint32_t rhnd, struct sys_ipc *msg);

////////////////////////////////////////////////////////////////////////////////

/* implementation callbacks for the IPC open protocol */

/** open file callback
 *
 * Open a file named "pathname" (NUL-terminated string).
 *
 * On success, the callback must pass one IPC client file descriptor
 * to the IPC caller. The function must also allocate a struct ioserver_file "f"
 * and associate it with the IPC server side file descriptor.
 * The function must initialize f as follows:
 * - set f->oflags to oflags
 * - set O_DIRECTORY in f->oflags for directories
 * - set f->length to the file size for seekable files
 * - set f->offset to 0 for seekable files
 *
 * The dispatcher has validated the message as follows:
 * - s->mountpointname[] set and NUL terminated
 * - pathname: normalized (starting with "/" if not empty, trailing "/" removed)
 * - oflags: O_ACCMODE | O_DIRECTORY | O_CREAT | O_EXCL | O_TRUNC | O_APPEND |
 *           O_NOCTTY | O_NOFOLLOW | O_NONBLOCK | O_SYNC | O_ASYNC
 * - access mode is a combination of O_RDONLY, O_WRONLY or O_RDWR,
 *           or just O_PATH without any further read or write access
 * - invalid oflags combinations of O_DIRECTORY and O_CREAT do not appear
 * - if access mode is O_PATH, then O_CREAT, O_EXCL, O_TRUNC, O_APPEND
 *           are not set in oflags
 * - mode: only valid bits (mask 07777 applied)
 */
err_t i_open(struct ioserver_fs *fs, uint32_t rhnd, const char *pathname, uint32_t oflags, uint32_t mode);

/** open pipe callback
 *
 * Create a pipe of two IPC client file descriptors.
 * The first one is the read end. The second one is the write end.
 *
 * On success, the callback must pass two file descriptors to the IPC caller.
 * The function must also allocate two struct ioserver_file "f" for each end
 * and associate them with the two IPC server side file descriptors.
 * The function must set f->oflags to "oflags" and additionally OR f->oflags
 * with O_RDONLY for the read end and with O_WRONLY for the write end.
 *
 * The dispatcher has validated the message as follows:
 * - s->mountpointname[] set to ""
 * - oflags: O_NONBLOCK
 */
err_t i_pipe(struct ioserver_fs *fs, uint32_t rhnd, uint32_t oflags);

/** open socket callback
 *
 * Create a socket for domain, type and protocol.
 *
 * On success, the callback must pass one IPC client file descriptor
 * to the IPC caller. The function must also allocate a struct ioserver_file "f"
 * and associate it with the IPC server side file descriptor.
 * The function must set f->oflags to "oflags" OR-ed with O_RDWR.
 *
 * The dispatcher has validated the message as follows:
 * - s->mountpointname[] set to ""
 * - oflags: O_NONBLOCK
 */
err_t i_socket(struct ioserver_fs *fs, uint32_t rhnd, uint32_t domain, uint32_t type, uint32_t protocol, uint32_t oflags);

/** open socketpair callback
 *
 * Create a socket pair (two file descriptors) for domain, type and protocol.
 *
 * On success, the callback must pass two file descriptors to the IPC caller.
 * The function must also allocate two struct ioserver_file "f" for each end
 * and associate them with the two IPC server side file descriptors.
 * The function must set f->oflags to "oflags" OR-ed with O_RDWR
 * for both file descriptors.
 *
 * On success, the callback must pass two file descriptors to the IPC caller.
 *
 * The dispatcher has validated the message as follows:
 * - s->mountpointname[] set to ""
 * - oflags: O_NONBLOCK
 */
err_t i_socketpair(struct ioserver_fs *fs, uint32_t rhnd, uint32_t domain, uint32_t type, uint32_t protocol, uint32_t oflags);

////////////////////////////////////////////////////////////////////////////////

/* dispatcher table entries for the IPC file and file system protocol */

// group base (mandatory)
int d_getoflags(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_getoflags_fs(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_setoflags(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_setoflags_fs(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_stat(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);

// group IO 1
int d_read(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_write(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_ioctl(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);

// group IO 2
int d_mmap(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_mprotect(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_munmap(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_seek(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);

// group IO 3
int d_truncate(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_allocate(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_sync(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_advise(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_lock(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);

// group FS 1
int d_statvfs(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_getpath(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_getdents(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_access(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_stat(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);

// group FS 2
int d_fs_rename(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_remove(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_mkdir(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_mknod(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_link(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_symlink(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_readlink(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_chmod(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_chown(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_fs_utime(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg);
int d_chmod(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_chown(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_utime(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);

// group SOCK
int d_send(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_recv(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_accept(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_connect(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_bind(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_getpeername(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_getsockname(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_listen(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_shutdown(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_getsockopt(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);
int d_setsockopt(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg);

////////////////////////////////////////////////////////////////////////////////

/** dispatch IPC file system protocol requests
 *
 * This dispatcher handles all requests for the IPC file system protocol
 * via the given callback table
 *
 * Returns IOSERVER_OK or IOSERVER_WAIT returned from dispatcher callback
 */
int dm_dispatch_fs(struct ioserver_fs *fs, uint32_t rhnd, struct sys_ipc *msg, const struct d_callback *cb);

/** dispatch IPC file protocol requests
 *
 * This dispatcher handles all requests for the IPC file protocol
 * via the given callback table
 *
 * Returns IOSERVER_OK or IOSERVER_WAIT returned from dispatcher callback
 */
int dm_dispatch_file(struct ioserver_file *f, uint32_t rhnd, struct sys_ipc *msg, const struct d_callback *cb);

////////////////////////////////////////////////////////////////////////////////

/* implementation callbacks for the IPC file protocol */

/** change oflags callback
 *
 * Change f->oflags of an open file descriptor, like fcntl(F_SETFL).
 * Only the oflags given below should be changed to the given value.
 *
 * NOTE: If an implementation of i_setoflags() is not provided,
 * d_setoflags() allows to change O_APPEND | O_NONBLOCK | O_SYNC | O_ASYNC
 * and return EOK.
 *
 * oflags: O_APPEND | O_NONBLOCK | O_SYNC | O_ASYNC
 */
err_t i_setoflags(struct ioserver_file *f, uint32_t oflags);

/** stat callback
 *
 * Retrieve file status.
 * The stat buffer is pre-initialized to default values for a character driver.
 *
 * NOTE: If an implementation of i_stat() is not provided,
 * d_stat() uses the default values and returns with EOK.
 */
err_t i_stat(struct ioserver_file *f, struct stat *statbuf);

/** read callback
 *
 * Read up to "size" data from "offset" in file "f".
 * Pass read data to the IPC caller in buffer 0 (write to IPC caller adspace).
 * Return the number of transferred bytes in "rsize".
 * Non-seekable should simply ignore the offset.
 *
 * The calling function updates the offset after successful reading.
 *
 * The callback returns ESTATE (instead of EAGAIN) to indicate blocking.
 *
 * The dispatcher has validated the offset to be positive,
 * but the offset might be beyond the end of the file.
 * The file is a readable file and not a directory.
 */
err_t i_read(struct ioserver_file *f, uint32_t rhnd, size_t size, off_t offset, size_t *rsize);

/** write callback
 *
 * Write up to "size" data to "offset" in file "f".
 * Get the data to write from the IPC caller in buffer 0 (read from IPC caller).
 * Return the number of transferred bytes in "wsize".
 * Non-seekable should simply ignore the offset.
 *
 * The calling function updates the offset after successful writing,
 * and also updates (grows) the file size based on offset and wsize.
 *
 * The callback returns ESTATE (instead of EAGAIN) to indicate blocking.
 *
 * The dispatcher has validated the offset to be positive,
 * but the offset might be beyond the end of the file.
 * The file is a writable file and not a directory.
 */
err_t i_write(struct ioserver_file *f, uint32_t rhnd, size_t size, off_t offset, size_t *wsize);

/** ioctl callback
 *
 * Perform a generic ioctl (encoded in request, see sys/ioctl.h).
 * The request encodes read and/or write permissions
 * to buffer 0 in the IPC caller's address space.
 * The function returns a 32-bit return value in rval.
 *
 * The callback returns ESTATE (instead of EAGAIN) to indicate blocking.
 *
 * The dispatcher has validated that the file is not a directory.
 */
err_t i_ioctl(struct ioserver_file *f, uint32_t rhnd, uint32_t request, unsigned long arg, uint32_t *rval);

/** mmap callback
 *
 * Map memory into the address space of the IPC caller.
 *
 * The dispatcher validated that the IPC message is a mapping request
 * (see sys_ipc_call()) and validates that the open file is not a directory
 * and that the requested access permissions match the opened file.
 * The given offset is positive and page-aligned,
 * but the offset might be beyond the end of the file.
 *
 * The kernel already validated the following properties for mapping requests:
 * - map address and size are page aligned and in user space.
 * - size is greater than zero.
 * - mapping permissions are PROT_NONE or PROT_READ | PROT_WRITE | PROT_EXEC.
 * - mapping flags are valid
 * - mapping flags encode either MAP_PRIV or MAP_SHARED type (to be ignored)
 * - MAP_ANON is not set in the mapping flags
 * - mapping placement rules such as MAP_FIXED and MAP_EXCL are taken care of
 *
 * The dispatcher further sets the remapping flags for mprotect() as follows:
 * - MAP_ALLOW_READ:  if PROT_READ is set
 * - MAP_ALLOW_WRITE: if PROT_WRITE is set
 * - MAP_ALLOW_EXEC:  if PROT_EXEC is set
 * The callback can extend the set of remapping flags, if needed,
 * for later mprotect() calls to succeed.
 */
err_t i_mmap(struct ioserver_file *f, uint32_t rhnd, off_t offset, size_t size, unsigned int prot, unsigned int flags);

/** mprotect callback
 *
 * Map memory with different access permissions into the address space
 * of the IPC caller (IPC message send by the kernel)
 *
 * i_mprotect() has the same semantics as i_mmap(),
 * but also tells the server that an existing mapping shall be changed.
 *
 * This function can be implemented as the same function as i_mmap()
 * if the tracking of individually mapped pages is not relevant.
 *
 * NOTE: If an implementation of i_mprotect() is not provided,
 * d_mprotect() simply returns EOK.
 */
err_t i_mprotect(struct ioserver_file *f, uint32_t rhnd, off_t offset, size_t size, unsigned int prot, unsigned int flags);

/** munmap callback
 *
 * Unmap memory notification (IPC message send by the kernel)
 *
 * The callback will be invoked after the pages have been unmapped
 * in the caller's address space.
 *
 * This function can be implemented as an empty function
 * if the tracking of individually mapped pages is not relevant.
 *
 * NOTE: If an implementation of i_munmap() is not provided,
 * d_munmap() returns EOK.
 */
err_t i_munmap(struct ioserver_file *f, off_t offset, size_t size);

/** seek callback
 *
 * Change offset in open file, potentially beyond the end of the file.
 * The function should update f->offset accordinly.
 *
 * FIXME: TBD
 *
 * The dispatcher validated the offset (not negative).
 *
 * NOTE: If an implementation of i_seek() is not provided,
 * d_seek() sets the offsets and returns EOK.
 * Unless the file is not written to with a non zero-sized write operation,
 * changing the offset beyond the end of the file has no effect.
 */
err_t i_seek(struct ioserver_file *f, off_t offset);

/** truncate callback
 *
 * Truncate or grow the size of a file.
 * The function should update f->length accordinly.
 *
 * FIXME: TBD
 *
 * The dispatcher validated the offset
 * and that the file is not a directory and that the file is opened for writing.
 */
err_t i_truncate(struct ioserver_file *f, off_t length);

/** allocate callback
 *
 * Allocate space in underlying data storage for the given section of a file.
 *
 * FIXME: TBD
 *
 * The dispatcher validated offset and length
 * and that the file is not a directory and that the file is opened for writing.
 *
 * This function can be implemented as an empty function.
 *
 * NOTE: If an implementation of i_allocate() is not provided,
 * d_allocate() simply returns EOK.
 */
err_t i_allocate(struct ioserver_file *f, off_t offset, off_t length);

/** sync callback
 *
 * Write back any changes in the given section of a file back to data storage.
 *
 * FIXME: TBD
 *
 * The dispatcher validated offset and length.
 *
 * This function can be implemented as an empty function
 * or ignore the given section of a file and synchronize the whole file
 * and related meta data.
 *
 * NOTE: If an implementation of i_sync() is not provided,
 * d_sync() simply returns EOK.
 */
err_t i_sync(struct ioserver_file *f, off_t offset, off_t length);

/** advise callback
 *
 * Advise buffer management about future use of the data in a section of a file.
 *
 * FIXME: TBD
 *
 * The dispatcher validated offset, length and advice
 * and that the file is not a directory.
 *
 * This function can be implemented as an empty function.
 *
 * NOTE: If an implementation of i_advise() is not provided,
 * d_advise() simply returns EOK.
 */
err_t i_advise(struct ioserver_file *f, off_t offset, off_t length, unsigned int advice);

/** lock callback
 *
 * Advisory file locking on a section of a file.
 *
 * FIXME: TBD
 *
 * The callback returns ESTATE (instead of EAGAIN) to indicate blocking.
 *
 * The dispatcher validated offset, length and operation
 * and that the file is not a directory.
 * The combination of offset and length can refer to a section
 * beyond the end of the file.
 * An offset of 0 and a length of 0x7fffffffffffffff
 *
 * It is safe to return EINVAL if file locking is not supported.
 */
err_t i_lock(struct ioserver_file *f, off_t offset, off_t length, unsigned int operation);


/** statvfs callback
 *
 * Retrieve status of current file system.
 * The statvfs buffer is pre-initialized to default values
 * for an in-memory file system.
 *
 * NOTE: If an implementation of i_statvfs() is not provided,
 * d_statvfs() uses the default values and returns with EOK.
 */
err_t i_statvfs(struct ioserver_file *f, struct statvfs *statbuf);

/** getpath callback
 *
 * Retrieve the mount point name and the path name of a directory.
 *
 * FIXME: TBD
 */
err_t i_getpath(struct ioserver_file *f, uint32_t rhnd, size_t size, size_t *rsize);

/** get directory entry.
 *
 * Fill in a directory entry in "dirent" for "offset".
 * The parameter "offset" is the opaque value of the requested directory entry
 * related to "dirent.d_off" for the next directory entry.
 * The caller updates the file offset to "dirent.d_off" on success.
 * By convention, the first directory entry has an offset of zero.
 *
 * The callback must fill in the name of the file, NUL-terminate the string,
 * and fill in all other members in "dirent", except "dirent.d_reclen",
 * which is handled by the calling function.
 * The calling function takes care to copy "dirent" to the IPC caller.
 *
 * After all files in a directory have been iterated, the function should
 * return ENOENT.
 *
 * \retval EOK				Success
 * \retval ENOENT			Terminator. Entry beyond the end of the list.
 * \retval EIO				I/O error
 */
err_t i_getdent(struct ioserver_file *f, struct dirent *dirent, off_t offset);

#endif
