# README_DESIGN.TXT
#
# azuepke, 2021-05-01: initial
# azuepke, 2021-05-16: a better way to handle syscall arguments


Kernel Design Considerations
============================

This document lists the main considerations for the kernel design.
The kernel uses a single kernel stack model, i.e. we have a single
kernel stack per processor. This eliminates internal context switching
between threads at kernel level, but also implies that we cannot simply
block in a function in the kernel. Threads must always "run to completion",
as scheduling only happens when a thread leaves the kernel.
This behavior deviates from other kernels such as Linux, BSD or Windows.

This document provides a brief overview on the main topics of system call
arguments and register contexts, blocking and unblocking, continuation
and cancellation handlers, and how preemption is handled internally.


System Call Arguments
---------------------

Like in other operating systems, a system call function in the kernel
is invoked with up to eight parameters from user space, and the function
returns either an error code or other values back to user space.
A typical system call function looks like this:

	err_t sys_some_system_call(ulong_t id)
	{
		if (id >= some_limit) {
			return ELIMIT;
		}

		...
		return EOK;
	}

Note that parameters from user space are "unsanitized" w.r.t. any bits
that exceed the original type of a parameter.
This is important on 64-bit systems, where setting any bits beyond 32-bit
in a 32-bit data type is undefined behavior.
Therefore, all integer types are provided with a long base type (ulong_t)
that matches the register width, and we expect the system call handler
to sanitize 32-bit values or check the value domain.

Note that pointer types are not affected by this. However, directly accessing
pointers from the kernel would cause the kernel to crash if the referenced
memory is not mapped or has different access permissions, e.g. writing
to read-only memory. Therefore, a system call handler uses robust copy
operations to access user space:

	err_t sys_futex_system_call(int *futex_user)
	{
		int val;

		err = user_get_4(futex_user, val);
		if (err == EOK) {
			...
			err = user_put_4(futex_user, val + 1);
		}
		...
	}

In the example above, the kernel safely dereferences the user space pointer
"futex_user" into "val", and then safely writes an incremented value back.


64-bit Parameters on 32-bit Architectures
-----------------------------------------

On 32-bit systems, 64-bit values in function calls are passed in two consecutive
registers, a register pair, or as 64-bit value on the stack. Both the allocation
in registers and the allocation on the stack might honor different alignment
guaranteed by the ABI, i.e. an alignment might also apply to the register pairs.
The rules are:
- x86:
  - 64-bit values are aligned to 32-bit boundaries
  - no arguments in registers
- ARM:
  - 64-bit variables are aligned to 64-bit
  - 64-bit valuess in aligned register pairs starting at r0
  - 64-bit parameters r0+r1 and r2+r3 only (rest on stack)
- PowerPC:
  - 64-bit variables are aligned to 64-bit
  - 64-bit values in aligned register pairs starting at r3
  - 64-bit parameters r3+r4, r5+r6, r7+r8, r9+r10 (rest on stack)
- RISC-V:
  - 64-bit variables are aligned to 64-bit
  - 64-bit values in any register pair (no alignment here)
  - BUT: 64-bit varargs are aligned to 64-bit


System Call Return Values
-------------------------

For security reasons, the return type is limited to native "long"-sized values.
When returning 64-bit values on 32-bit systems, the ABI requires two registers.
However, when a system call only returns a 32-bit value (or has "void" return
type!), it could leak kernel data in the second return register.
Still, the kernel supports returning 64-bit values with a trick that sets the
second return register directly in the register context of the calling thread:

	ulong_t sys_64bit_return_value(...)
	{
		uint64_t val;
		...
		return arch_syscall_return_64bit(current_thread()->regs, val);
	}

Here, a native "long" return type and a call to arch_syscall_return_64bit() in
the tail-recursion position ensures that the 64-bit value is correctly returned.
This function is a nop on 64-bit systems that support 64-bit values natively.

Note that when returning smaller types, any upper bits are implicitly cleared,
and the kernel does not leak any state to user space. Likewise, a void return
type is not supported. In this case, the system call should simply return 0.


Deferred System Calls (Immediate Continuation)
----------------------------------------------

Note that the way described above always enforces a write-back of the return
value into the register context by the low-level system call dispatcher.
But there are system calls that need a consistent register context, for
example to send a signal. For these use cases, the kernel offers a
"deferred system call" mechanism where a second function is invoked
when the system call return type was set. By convention, we name the
deferred handler the same as the system call but with a postfix "_deferred":

	err_t sys_other_system_call(...)
	{
		/* defer to not mess up the register context */
		thread_defer_to(current_thread(), sys_other_system_call_deferred);

		return EOK;
	}

	static void sys_other_system_call_deferred(struct thread *thr)
	{
		... consistent register context in thr->regs ...
	}

The kernel provides the thread_defer_to() service to register the deferred
handler. Actually, the deferred system call is a special type of continuation
that is executed immediately after the system call. The deferred system call
handler is executed only once.

The general design pattern is to check all input parameters in the system call
main function, and defer any work that cannot fail to the deferred handler.
Safely passing parameters to the deferred handler is shown in the next sections.


Blocking, Unblocking and Cancellation
-------------------------------------

For waiting (blocking) in the kernel, a system call function simply
sets the state of the current thread to a waiting state and then returns:

	/* block thread when leaving the kernel */
	thread_wait(thr, THREAD_STATE_WAIT_XXX, timeout);

	return EOK;

Updating the return code (wakeup code) then uses a two-step mechanism.
While the thread is blocked, the wakeup code can be updated all the time
by updating the thread's wakeup_code variable:

	thr->wakeup_code = ETIMEDOUT;

The return code of the thread is finally updated by the thread itself
when the thread is scheduled again. This prevents any race conditions
that could happen when a thread's return code is updated directly
in the register context while the thread is woken up concurrently.
To set the return code, the kernel provides a special continuation
that is run after wakeup but before the thread returns to user space.
This continuation can be activated as follows:

	thread_continue_at_wait_finish(thr);

The model above is sufficient for simple blocking calls. But when the blocked
thread is also enqueued in specific wait queues, we must perform additional
cleanup to remove the blocked from the wait queues. For this, the kernel
defines a concept of so called "cancellation handlers". These handles
are called from timeout processing when waking up the thread, or when the
thread is deleted or otherwise unblocked.

The cancellation handler receives the cancelled thread as argument:

	void xxx_wait_cancel(struct thread *thr);

To register and unregister a cancellation handler, a thread can use:

	/* setup cancellation handler before waiting */
	thread_cancel_at(thr, xxx_wait_cancel);

	/* clear cancellation handler on regular wakeup */
	thread_cancel_clear(thr);

Note that due to fine-grained locking, the decision to cancel waiting
is protected by the scheduler lock (this protects the current thread state).
However, the cancellation handler is called unlocked, and the handler
should check the thread's state again under the specific lock that protects
the wait queue if the thread is still enqueued on the wait queue,
as cancellation can race with other wakeup operations, as described above.
As a consequence, the cancellation handler must not modify a thread's
return code or register context, but can only update "thr->wakeup_code".


Kernel Continuation Design
--------------------------

To make longer running kernel operations preemptible, the kernel uses
so called "continuations" as an in-kernel pick up routine. Continuations
comprise continuation handlers, i.e. functions that continue an operation
after preemption. A contuniation handler is simply a function with a pointer
to its own thread as argument:

	void a_cont_handler(struct thread *thr);

In case a long-running routine detects a request that requires scheduling,
it registers a continuation handler and sets up the necessary parameters
as "continuations arguments" (cont_args)

	/* run continuation before switching to user space */
	thr->cont_args.arg0 = tls;
	thread_continue_at(thr, a_cont_handler);

When scheduling the thread again, the kernel will invoke the provided
continuation handler before returning to user space. In fact,
the continuation handler will be invoked each time the thread is scheduled
until the continuation handler of the thread is explicitly nulled by a call to:

	thread_continue_normal(thr);

This helps to design preemptible functionality in the kernel that likely
require multiple invocations of the preemption handler.

As a guideline, the normal system call routine should evaluate and prepare
all necessary parameters for the continuation function, and then move the
actual preemptive work into the continuation handler.

The continuation handler receives a pointer to the current thread (to pick
up the continuation parameters), but does not provide a return code.
To return an error, the continuation function must explicitly update
the return code register in the thread's register context:

	void a_cont_handler(struct thread *thr)
	{
		... thr->cont_args.args[] ...

		arch_syscall_retval(thr->regs, err);
	}

Note that updating the register context is safe when done in the context
of the current thread.


Special Cases
-------------

Continuations can also be "implanted" into other threads. We use this trick
when starting a partition. After preparing an empty address space for the
partition, we implant a continuation in the first thread of the partition
to setup the rest of the address space, i.e. load the application code
and data from the ELF file and create the according mappings.

However, this only works because we know that the first thread of a partition
is idle when start the partition, and therefore cannot execute a continuation
on its own. The situation is different when we need to inject a continuation
in a thread that is already running, and already has setup a continuation.
In this case, we must first cancel the thread's ongoing operation before
we can implant a new continuation. This requires a cancellation routine
provided by the target thread. The cancellation function has the same type
signature as a continuation. However, the cancellation is performed
in the context of the injecting thread and should be kept as short as possible.
The kernel uses the same callback infrastructure to cancel blocking operation.


Interrupt Handling and Pending Preemption Requests
--------------------------------------------------

Interrupt handling in the kernel is realized as follows: the kernel always
executes with disabled interrupts. This means that only code running
in user space is effectively interrupted by an interrupt.
However, to detect pending interrupts in long-running system calls,
the kernel provides a special polling primitive:

	int arch_irq_pending(void);

On ARM and RISC-V, this simply tests a CPU register for pending interrupts,
so the check is fast. In this case, a long-running system call should simply
setup a continuation handler and return from the system call. The kernel
will then receive the interrupt, i.e. wake up waiting threads and schedules.
If the original thread was not preempted, the kernel immediately return
to this thread by calling its continuation handler. Otherwise, the handler
will be invoked once the thread is again the highest priority one.

Note that the scheduler uses an internal variable "sched->reschedule"
that is set if scheduling is required, but this variable is not a sufficient
test for pending interrupts. System call code should not check this variable.


Lock Contention and Pending Preemption Requests
-----------------------------------------------

Likewise to the check for pending interrupts, the kernel also provides
a fast check if a spinlock has contention:

	int spin_contended(spin_t *spin);

This check tests the ticket counter of a lock for contention,
and its much cheaper than a sequence of spin_unlock() and spin_lock(),
as it does not contain atomic operations. Long-running operations should
therefore keep any locks locked and check for both lock contention
and pending interrupts from time to time to ensure both lock fairness
w.r.t. other processors and preemptibility w.r.t. interrupts:

	spin_lock(&lock);
	while (...) {
		...

		/* fast-path to check for rescheduling */
		if (spin_contended(&lock) || arch_irq_pending()) {
			break;
		}
	}
	spin_unlock(&lock);
